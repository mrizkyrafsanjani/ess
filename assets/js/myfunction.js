
$(function () {
	
	var gl_uangmakan;
	var gl_uangsaku;
	gl_uangmakan = $("#txtBebanHarianUangMakan").val();
	gl_uangsaku = $("#txtBebanHarianUangSaku").val();

	$("#tblUangMuka").hide();
	$("#tblPerkiraanBiaya").hide();
	$("#tblOutstanding").hide();
	$("#tblTotalUM").hide();
	$("#tblBayarUM").hide();
	//$('#txtTanggalBerangkat').datepick({dateFormat: 'yyyy-mm-dd'});
	//$('#txtTanggalKembali').datepick({dateFormat: 'yyyy-mm-dd'});
	//$('#txtTanggalBerangkat').datepick({dateFormat: 'dd-mm-yyyy'});
	//$('#txtTanggalKembali').datepick({dateFormat: 'dd-mm-yyyy'});
	$('.clsTotal').attr("disabled", true);
	$('#txtTanggalBerangkat').attr("readonly", true);
	$('#txtTanggalKembali').attr("readonly", true);
	$('#txtTanggalKembaliKantor').attr("readonly", true);
	$('#txtGrandTotal').attr("readonly", true);
	$('#txtGrandTotal').removeAttr("disabled");
	$(".clsInputNumber").keyup(function () { hitungTotal(); changeFormat(); });
	$("#txtTujuan").focus(function () { changeFormat(); countDay(); });
	setInterval(function () { changeFormat(); countDay(); extend(); checkBtnSubmit();},500);
	
	$("#tanggalkembali").hide();
	$("#tanggalkembalikantor").show();
	$("#hidden").hide();
	$("#hidden2").hide();
	$("#hidden3").hide();
	$("#hidden4").hide();
	$("#alasanextend").hide();

	var tglBerangkat = $('#txtTanggalBerangkat').val();
	var tglKembali = $('#txtTanggalKembali').val();
	var tglKembaliPermohonan = $('#txtTanggalKembaliPermohonan').val();
	var tglKembaliKantor =  $('#txtTanggalKembaliKantor').val();
	
	if ($('#txtTanggalBerangkat').val() !== "") {
		$('#txtTanggalBerangkat').val(tglBerangkat.substr(8, 2) + '-' + tglBerangkat.substr(5, 2) + '-' + tglBerangkat.substr(0, 4));
		$('#txtTanggalKembali').val(tglKembali.substr(8, 2) + '-' + tglKembali.substr(5, 2) + '-' + tglKembali.substr(0, 4));
		$('#txtTanggalKembaliKantor').val(tglKembaliKantor.substr(8, 2) + '-' + tglKembaliKantor.substr(5, 2) + '-' + tglKembaliKantor.substr(0, 4));
		$('#txtTanggalKembaliPermohonan').val(tglKembaliPermohonan.substr(8, 2) + '-' + tglKembaliPermohonan.substr(5, 2) + '-' + tglKembaliPermohonan.substr(0, 4));
		
	}
	
	function changeFormat() {
		$("#txtTotalUangSaku").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalUangSaku").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalUangMakan").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalUangMakan").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalHotel").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalHotel").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalHotelLap").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalHotelLap").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalHotel2").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalHotel2").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalTaksi").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalTaksi").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalAirport").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalAirport").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalLain1").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalLain1").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalLain2").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalLain2").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalLain3").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtTotalLain3").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtGrandTotal").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtGrandTotal").formatNumber({ format: "#,##0", locale: "us" });
		$("#lblTotalPengajuan").parseNumber({ format: "#,##0", locale: "us" });
		$("#lblTotalPengajuan").formatNumber({ format: "#,##0", locale: "us" });
		$("#lblTotalPengajuanUangMuka").parseNumber({ format: "#,##0", locale: "us" });
		$("#lblTotalPengajuanUangMuka").formatNumber({ format: "#,##0", locale: "us" });
		$("#lblJmlhPermintaan").parseNumber({ format: "#,##0", locale: "us" });
		$("#lblJmlhPermintaan").formatNumber({ format: "#,##0", locale: "us" });
		$("#lblSelisih").parseNumber({ format: "#,##0", locale: "us" });
		$("#lblSelisih").formatNumber({ format: "#,##0", locale: "us" });

		$("#txtBebanHarianTaksi").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtBebanHarianTaksi").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtBebanHarianAirport").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtBebanHarianAirport").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtBebanHarianLain1").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtBebanHarianLain1").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtBebanHarianLain2").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtBebanHarianLain2").formatNumber({ format: "#,##0", locale: "us" });
		$("#txtBebanHarianLain3").parseNumber({ format: "#,##0", locale: "us" });
		$("#txtBebanHarianLain3").formatNumber({ format: "#,##0", locale: "us" });
	}

	$(".clsBebanHarian").change(function () {
		$(this).parseNumber({ format: "#,##0", locale: "us" });
		$(this).formatNumber({ format: "#,##0", locale: "us" });
	});
	$("#txtBebanHarianUangSaku").formatNumber({ format: "#,##0", locale: "us" });
	$("#txtBebanHarianUangMakan").formatNumber({ format: "#,##0", locale: "us" });
	
	

	$("#rbYa").click(function () {
		$("#tblPerkiraanBiaya").show(1000);
		$("#tblUangMuka").show(1000);
		$("#txtBebanHarianUangMakan").val(gl_uangmakan);
		$("#txtBebanHarianUangSaku").val(gl_uangsaku);
		$("#tblOutstanding").show(1000);
		$("#tblTotalUM").show(1000);
		$("#tblBayarUM").show(1000);
	});
	$("#rbTidak").click(function () {
		$("#tblPerkiraanBiaya").hide(1000);
		$("#tblUangMuka").hide(1000);
		$("#tblOutstanding").hide(1000);
		$("#tblTotalUM").hide(1000);
		$("#tblBayarUM").hide(1000);
		gl_uangmakan = $("#txtBebanHarianUangMakan").val();
		gl_uangsaku = $("#txtBebanHarianUangSaku").val();
		$("#txtBebanHarianUangMakan").val(0);
		$("#txtBebanHarianUangSaku").val(0);
	});

	$("#rbInternasional").click(function () {
		$("#txtBebanHarianUangSaku").attr("readonly", false);
		$("#txtBebanHarianUangMakan").attr("readonly", false);
	});

	$("#rbNasional").click(function () {
		$("#txtBebanHarianUangSaku").attr("readonly", true);
		$("#txtBebanHarianUangMakan").attr("readonly", true);
	});

	$(".clsInputTanggal").click(function () {
		countDay();
	});

	function countDay(jenis) {
		var strDate1 = $("#txtTanggalBerangkat").val();
		var strDate2 = $("#txtTanggalKembali").val();

		strDate1 = strDate1.substr(6, 4) + '-' + strDate1.substr(3, 2) + '-' + strDate1.substr(0, 2);
		strDate2 = strDate2.substr(6, 4) + '-' + strDate2.substr(3, 2) + '-' + strDate2.substr(0, 2);
		if (strDate1 != '' && strDate2 != '') {
			var date1 = new Date(strDate1);
			var date2 = new Date(strDate2);
			if (jenis === 'uangmakan') {
				// if($("#txtJmlhHariUangMakanDb").val() != ((date2 - date1) / (24 * 60 * 60 * 1000)) + 1){
				// 	$('#txtJmlhHariUangMakan').val(((date2 - date1) / (24 * 60 * 60 * 1000)) + 1);
				// }
			} else if (jenis === 'uangsaku') {
				//$('#txtJmlhHariUangSaku').val(((date2 - date1) / (24 * 60 * 60 * 1000)) + 1);
			} else if (jenis === 'akomodasi') {
				$('#txtJmlhHariHotel').val(((date2 - date1) / (24 * 60 * 60 * 1000)));
			} else {
				// if($("#txtJmlhHariUangMakanDb").val() != ((date2 - date1) / (24 * 60 * 60 * 1000)) + 1){
				// 	  $('#txtJmlhHariUangMakan').val(((date2 - date1) / (24 * 60 * 60 * 1000)) + 1);
				// }
				//$('#txtJmlhHariUangSaku').val(((date2 - date1) / (24 * 60 * 60 * 1000)) + 1);
				$('#txtJmlhHariHotel').val(((date2 - date1) / (24 * 60 * 60 * 1000)));
			}
		
			hitungTotal();
			changeFormat();
			if($("#txtJmlhHariUangMakan").val() > ((date2 - date1) / (24 * 60 * 60 * 1000)) + 1){
			   $('#txtJmlhHariUangMakan').val(((date2 - date1) / (24 * 60 * 60 * 1000)) + 1);
			}
			if($("#txtJmlhHariUangSaku").val() > ((date2 - date1) / (24 * 60 * 60 * 1000)) + 1){
				$('#txtJmlhHariUangSaku').val(((date2 - date1) / (24 * 60 * 60 * 1000)) + 1);
			 }
			if ($('#txtJmlhHariHotel').val() == 0) {
				$('#txtJmlhHariHotel').val("");
			}
		}
	}


	//Function untuk menghitung hari
	function hitungHari() {
		var strDate1 = $("#txtTanggalBerangkat").val();
		var strDate2 = $("#txtTanggalKembali").val();

		strDate1 = strDate1.substr(6, 4) + '-' + strDate1.substr(3, 2) + '-' + strDate1.substr(0, 2);
		strDate2 = strDate2.substr(6, 4) + '-' + strDate2.substr(3, 2) + '-' + strDate2.substr(0, 2);

		if (strDate1 != '' && strDate2 != '') {
			var date1 = new Date(strDate1);
			var date2 = new Date(strDate2);
			return (((date2 - date1) / (24 * 60 * 60 * 1000)) + 1);
			
		}
	}
	
	function extend(){
		var tglKembaliKantor1 = $('#txtTanggalKembaliKantor').val();
		var KembaliKantorVal = tglKembaliKantor1.substr(6, 4) + '-' + tglKembaliKantor1.substr(3, 2) + '-' + tglKembaliKantor1.substr(0, 2);
		$('#txtTanggalKembaliKantor1').val(KembaliKantorVal);

		if ($('#txtTanggalKembaliPermohonan1').val() < $('#txtTanggalKembaliKantor1').val() ) {	
			$('#alasanextend').show();
			;
			if ($('#txtAlasanExtend').val()=='Cuti'){
				$('#lblkembali').html('*Tanggal Selesai Kerja');
				$('#tanggalkembali').show();
				$('#tanggalkembalikantor').show();
				$('#lblkembalikantor').html('*Tanggal Kembali Ke Kantor')			
			}else if($('#txtAlasanExtend').val()=='ExtendTugas'){
				$('#lblkembali').html('*Tanggal Selesai Kerja');
				$('#tanggalkembali').show();
				$('#tanggalkembalikantor').hide();
				$('#lblkembali').html('*Tanggal Kembali Ke Kantor');
			}else{
				$('#tanggalkembalikantor').hide();
				$('#alasanextend').hide();
				$('#lblkembalikantor').html('*Tanggal Kembali');
				$('#txtTanggalKembali').val($('#txtTanggalKembaliPermohonan').val());
				$('#txtAlasanExtend').val('Cuti');
			}
		}else{
			$('#tanggalkembali').hide();
			$('#alasanextend').hide();
			$('#lblkembali').html('*Tanggal Kembali');
			$('#txtTanggalKembali').val($('#txtTanggalKembaliKantor').val())
		}
	}
	function hitungTotal() {
		var uangsaku = 0;
		var uangmakan = 0;
		var hotel = 0;
		var hotel2 = 0;
		var taksi = 0;
		var airport = 0;
		var lain1 = 0;
		var lain2 = 0;
		var lain3 = 0;

		if ($('#txtBebanHarianHotel').parseNumber({ format: "#,##0", locale: "us" }) != "") {
			hotel = $('#txtBebanHarianHotel').parseNumber({ format: "#,##0", locale: "us" });
			$('#txtBebanHarianHotel').formatNumber({ format: "#,##0", locale: "us" });
		}
		if ($('#txtBebanHarianHotel2').parseNumber({ format: "#,##0", locale: "us" }) != "") {
			hotel2 = $('#txtBebanHarianHotel2').parseNumber({ format: "#,##0", locale: "us" });
			$('#txtBebanHarianHotel2').formatNumber({ format: "#,##0", locale: "us" });
		}
		if ($('#txtBebanHarianTaksi').parseNumber({ format: "#,##0", locale: "us" }) != "") {
			taksi = $('#txtBebanHarianTaksi').parseNumber({ format: "#,##0", locale: "us" });
			$('#txtBebanHarianTaksi').formatNumber({ format: "#,##0", locale: "us" });
		}
		if ($('#txtBebanHarianAirport').parseNumber({ format: "#,##0", locale: "us" }) != "") {
			airport = $('#txtBebanHarianAirport').parseNumber({ format: "#,##0", locale: "us" });
			$('#txtBebanHarianAirport').formatNumber({ format: "#,##0", locale: "us" });
		}
		if ($('#txtBebanHarianLain1').parseNumber({ format: "#,##0", locale: "us" }) != "") {
			lain1 = $('#txtBebanHarianLain1').parseNumber({ format: "#,##0", locale: "us" });
			$('#txtBebanHarianLain1').formatNumber({ format: "#,##0", locale: "us" });
		}
		if ($('#txtBebanHarianLain2').parseNumber({ format: "#,##0", locale: "us" }) != "") {
			lain2 = $('#txtBebanHarianLain2').parseNumber({ format: "#,##0", locale: "us" });
			$('#txtBebanHarianLain2').formatNumber({ format: "#,##0", locale: "us" });
		}
		if ($('#txtBebanHarianLain3').parseNumber({ format: "#,##0", locale: "us" }) != "") {
			lain3 = $('#txtBebanHarianLain3').parseNumber({ format: "#,##0", locale: "us" });
			$('#txtBebanHarianLain3').formatNumber({ format: "#,##0", locale: "us" });
		}
		$('#txtTotalUangSaku').val($('#txtBebanHarianUangSaku').parseNumber({ format: "#,##0", locale: "us" }) * $('#txtJmlhHariUangSaku').parseNumber({ format: "#,##0", locale: "us" }));
		$('#txtTotalUangMakan').val($('#txtBebanHarianUangMakan').parseNumber({ format: "#,##0", locale: "us" }) * $('#txtJmlhHariUangMakan').parseNumber({ format: "#,##0", locale: "us" }));
		$('#txtTotalHotel').val(hotel * $('#txtJmlhHariHotel').parseNumber({ format: "#,##0", locale: "us" }));
		$('#txtTotalHotelLap').val(hotel * $('#txtJmlhHariHotelLap').parseNumber({ format: "#,##0", locale: "us" }));
		$('#txtTotalHotel2').val(hotel2 * $('#txtJmlhHariHotel2').parseNumber({ format: "#,##0", locale: "us" }))
		$('#txtTotalTaksi').val(taksi);
		$('#txtTotalAirport').val(airport);
		$('#txtTotalLain1').val(lain1);
		$('#txtTotalLain2').val(lain2);
		$('#txtTotalLain3').val(lain3);
		$('#txtGrandTotal').val(parseInt($('#txtTotalUangSaku').parseNumber({ format: "#,##0", locale: "us" }), 10) + parseInt($('#txtTotalUangMakan').parseNumber({ format: "#,##0", locale: "us" }), 10) + parseInt($('#txtTotalHotel').parseNumber({ format: "#,##0", locale: "us" }), 10) + parseInt($('#txtTotalTaksi').parseNumber({ format: "#,##0", locale: "us" }), 10) + parseInt($('#txtTotalAirport').parseNumber({ format: "#,##0", locale: "us" }), 10) + parseInt($('#txtTotalLain1').parseNumber({ format: "#,##0", locale: "us" }), 10) + parseInt($('#txtTotalLain2').parseNumber({ format: "#,##0", locale: "us" }), 10) + parseInt($('#txtTotalLain3').parseNumber({ format: "#,##0", locale: "us" }), 10) + parseInt($('#txtTotalHotelLap').parseNumber({ format: "#,##0", locale: "us" }), 10) + parseInt($('#txtTotalHotel2').parseNumber({ format: "#,##0", locale: "us" }), 10));
		$('#lblTotalPengajuan').val($('#txtGrandTotal').val());
		$('#lblJmlhPermintaan').html($('#txtGrandTotal').val());

		var totalRealisasi = $('#txtGrandTotal').parseNumber({ format: "#,##0", locale: "us" });
		var totalPengajuan = $('#lblTotalPengajuanUangMuka').parseNumber({ format: "#,##0", locale: "us" });
		$('#lblSelisih').val(totalRealisasi - totalPengajuan);
		$('#txtBebanHarianUangSaku').formatNumber({ format: "#,##0", locale: "us" });
		
		$('#txtBebanHarianUangMakan').formatNumber({ format: "#,##0", locale: "us" })
		$('#lblkembalikantor').html('*Tanggal Kembali');
		
		
		if ($('#radbDpaDua').is(':checked') == true) {
		if ($('#lblSelisih').val() < 0) {
	
				$('#statusLebih').html('Total Kelebihan Uang Muka');
				$('#nmbank').html('Permata');
				$('#norek').html('0701158113');
				$('#nmpenerima').html('DPA Dua - Opex');
				$('#psn').html('Kelebihan Uang Muka harap di transfer ke');
				document.getElementById("tablepembayaran").style.display = 'block';
				document.getElementById("tablepembayarankurang").style.display = 'none';
				var selisih = $('#lblSelisih').val();
				$('#lblSelisih').val(selisih * -1);
				$('#txtBankKurang').val($('#nmbank').html());
				$('#txtNoRekKurang').val($('#norek').html());
				$('#txtPenerimaKurang').val($('#nmpenerima').html());

		} else
		if ($('#lblSelisih').val() > 0) {
				$('#txtBankKurang').val();
				$('#txtNoRekKurang').val();
				$('#txtPenerimaKurang').val();
			$('#statusLebih').html('Total Kekurangan Uang Muka');
			document.getElementById("tablepembayaran").style.display = 'none';
			document.getElementById("tablepembayarankurang").style.display = 'block';
			var selisih = $('#lblSelisih').val();
			$('#lblSelisih').val(selisih * -1);
		} else 
		{
			$('#statusLebih').html('Total Kelebihan/Kekurangan Uang Muka');
			document.getElementById("tablepembayaran").style.display = 'none';
			document.getElementById("tablepembayarankurang").style.display = 'none';
		}
		}else if ($('#radbDpaSatu').is(':checked') == true) { 
			if ($('#lblSelisih').val() < 0) {
	
				$('#statusLebih').html('Total Kelebihan Uang Muka');
				$('#nmbank').html('Permata');
				$('#norek').html('0701607104');
				$('#nmpenerima').html('DPA Satu - Opex');
				$('#psn').html('Kelebihan Uang Muka harap di transfer ke');
				document.getElementById("tablepembayaran").style.display = 'block';
				document.getElementById("tablepembayarankurang").style.display = 'none';
				var selisih = $('#lblSelisih').val();
				$('#lblSelisih').val(selisih * -1);
				$('#txtBankKurang').val($('#nmbank').html());
				$('#txtNoRekKurang').val($('#norek').html());
				$('#txtPenerimaKurang').val($('#nmpenerima').html());

		} else
		if ($('#lblSelisih').val() > 0) {
			$('#txtBankKurang').val();
			$('#txtNoRekKurang').val();
			$('#txtPenerimaKurang').val();
			$('#statusLebih').html('Total Kekurangan Uang Muka');
			document.getElementById("tablepembayaran").style.display = 'none';
			document.getElementById("tablepembayarankurang").style.display = 'block';
			var selisih = $('#lblSelisih').val();
			$('#lblSelisih').val(selisih * -1);
		} else 
		{
			$('#statusLebih').html('Total Kelebihan/Kekurangan Uang Muka');
			document.getElementById("tablepembayaran").style.display = 'none';
			document.getElementById("tablepembayarankurang").style.display = 'none';
		}	
		}
		if($('#txtUangMuka').val() ==0){
			document.getElementById("tablepembayaran").style.display = 'none';
			document.getElementById("tablepembayarankurang").style.display = 'none';
		}
	}

	
	//Function untuk memilih title halaman
	function wherePage(){
		if(document.title === "ESS DPA - Input Laporan SPD"){
			window.onload = hitungTotal(); changeFormat();
		}else if(document.title === "ESS DPA - Input Form SPD"){
			window.onload = changeFormat();
		}
	}


	//document.getElementById('btnSubmit').disabled = true;
	checkBtnSubmit();
	$('form').click(function () { checkBtnSubmit(); });

	function checkBtnSubmit() {
		//countDay('uangsaku');
		hitungTotal(); changeFormat();
		$('#divError').show();
		if ($('#rbDpaSatu').is(':checked') == false && $('#rbDpaDua').is(':checked') == false) {
			//alert('Harap pilih DPA 1 atau 2');
			$('#divError').html('Tolong tentukan DPA 1 atau 2 pada bagian atas');
			document.getElementById('btnSubmit').disabled = true;
		} else if ($('#txtTanggalBerangkat').val() == '' || $('#txtTanggalKembali').val() == '') {
			//alert('Harap masukkan tanggal berangkat dan tanggal kembali');
			$('#divError').html('Harap masukkan tanggal berangkat dan tanggal kembali');
			document.getElementById('btnSubmit').disabled = true;
		} else if ($('#txtTujuan').val() == '' || $('#txtAlasan').val() == '') {
			//alert('Harap mengisi tujuan dan alasan perjalanan');
			$('#divError').html('Harap mengisi tujuan dan alasan perjalanan');
			document.getElementById('btnSubmit').disabled = true;
		} else if ($('#rbYa').is(':checked') == false && $('#rbTidak').is(':checked') == false) {
			//alert('Harap menentukan apakah ingin mengajukan uang muka atau tidak');
			$('#divError').html('Harap menentukan apakah ingin mengajukan uang muka atau tidak');
			document.getElementById('btnSubmit').disabled = true;
		} else if ($('#txtJmlhHariUangSaku').parseNumber({ format: "#,##0", locale: "us" }) > hitungHari()) {
			//jumlah hari yang dimasukkan tidak boleh lebih besar dari jumlah hari spd
			$('#divError').html('Jumlah hari uang saku yang anda masukkan terlalu besar');
			document.getElementById('btnSubmit').disabled = true;
		} else if ($("#lblTotalPengajuanUangMuka").val() != 0){
			if($('#txtKegiatan').val() == ''){
				$('#divError').html('Harap memilih jenis kegiatan');
				document.getElementById('btnSubmit').disabled = true;
				} else if ($('#txtBank').val() == ''){
					$('#divError').html('Harap mengisi nama bank penerima');
					document.getElementById('btnSubmit').disabled = true;
				} else if ($('#txtNoRek').val() == ''){
					$('#divError').html('Harap mengisi nomor rekening penerima');
					document.getElementById('btnSubmit').disabled = true;
				} else if ($('#txtPenerima').val() == ''){
					$('#divError').html('Harap mengisi nama penerima');
					document.getElementById('btnSubmit').disabled = true;
				}else{
					$('#divError').hide();
					document.getElementById('btnSubmit').disabled = false;
				}
		}
			else if (tglBerangkat >  tglKembali ) {
				//alert('Harap masukkan tanggal berangkat dan tanggal kembali');
				$('#divError').html('Tanggal Kembali harus lebih BESAR dari Tanggal Berangkat');
				document.getElementById('btnSubmit').disabled = true;  
		}

			/*else if($('#rbYa').is(':checked') == true &&($('#txtJmlhHariHotel').val() != '') && ($('#txtBebanHarianHotel').val() == '' || $('#txtBebanHarianHotel').val() == 0)){
				//alert('Harap isi beban harian hotel');
				$('#divError').html('Harap isi beban harian hotel/akomodasi');
				document.getElementById('btnSubmit').disabled = true; 
			}else if($('#rbYa').is(':checked') == true && $('#txtJmlhHariHotel').val() == '' && $('#txtBebanHarianHotel').val() != ''){
				//alert('Harap isi jumlah hari hotel');
				$('#divError').html('Harap isi jumlah hari hotel/akomodasi');
				document.getElementById('btnSubmit').disabled = true; 
			}*/else {
			document.getElementById('btnSubmit').disabled = false;
			$('#divError').html('');
			$('#divError').hide();
		}
	}

	$('#txtAlasan').keyup(function () { copyIsi(); });
	$('#txtTujuan').keyup(function () { copyIsi(); });
	function copyIsi() {
		//alert ('Uang Muka SPD '+ $('#txtAlasan').val() + ' ' + $('#txtTujuan').val() + '( ' + $('#txtTanggalBerangkat').val() + ' s/d ' + $('#txtTanggalKembali').val() +')');
		$('#lblKeperluan').html($('#txtAlasan').val());
		$('#lblTujuan').html($('#txtTujuan').val());
		$('#txtKeterangan').val('Uang Muka SPD '+ $('#txtAlasan').val() + ' ' + $('#txtTujuan').val() + ' ( ' + $('#txtTanggalBerangkat').val() + ' s/d ' + $('#txtTanggalKembali').val() +') an ' + $('#txtNama').val());
	}



	function kirimData(filePHP) {
		$(document).ready(function () {
			//$('#statusData').html("<img src=\'./images/ProgressAnim.gif\' /> sedang mengirim...");
			//document.getElementById('btnKirim').disabled = true; 
			if (confirm('Apakah Anda yakin untuk menyimpan data ini ke dalam database?')) {
				$.ajax({
					type: 'POST',
					url: filePHP + '.php',
					data: 'txtTanggalBerangkat=' + $('#txtTanggalBerangkat').val() +
						'&txtTanggalKembali=' + $('#txtTanggalKembali').val() +
						'&txtTujuan=' + $('#txtTujuan').val() +
						'&txtAlasan=' + $('#txtAlasan').val() +
						'&rbDPA=' + $('#rbDPA').val() +
						'&rbUangMuka=' + $('#rbUangMuka').val() +
						'&txtGrandTotal=' + $('#txtGrandTotal').val()
				}).done(function (msg) {
					//message done
					alert('data sudah tersimpan');
				});
			}

		});
	}

	wherePage();
});

	/*
	function countDay(){
		var strDate1 = document.getElementById('txtTanggalBerangkat').value;
		var strDate2 = document.getElementById('txtTanggalKembali').value;
		var date1 = new Date(strDate1);
		var date2 = new Date(strDate2);
		if(strDate1 != '' && strDate2 != ''){
			document.getElementById('txtJmlhHariUangMakan').value = ((date2 - date1)/(24*60*60*1000)) + 1;
			document.getElementById('txtJmlhHariUangSaku').value = ((date2 - date1)/(24*60*60*1000)) + 1;
			document.getElementById('txtJmlhHariHotel').value = ((date2 - date1)/(24*60*60*1000));
			hitungTotal();
		}
	}
	*/
	/*
	function copyIsi(){
		document.getElementById('lblKeperluan').value = document.getElementById('txtAlasan').value;
		document.getElementById('lblTujuan').value = document.getElementById('txtTujuan').value;
	}
	*/
	/*
	function hitungTotal(){
		var uangsaku = 0;
		var uangmakan = 0;
		var hotel = 0;
		var taksi = 0;
		var airport = 0;
		var lain1 = 0;
		var lain2= 0;
		var lain3 = 0;
		
		if(document.getElementById('txtBebanHarianHotel').value != ""){
			hotel = document.getElementById('txtBebanHarianHotel').value.replace(/\,/g,"");
		}
		if(document.getElementById('txtBebanHarianTaksi').value.replace(/\,/g,"") != ""){
			taksi = document.getElementById('txtBebanHarianTaksi').value.replace(/\,/g,"");
		}
		if(document.getElementById('txtBebanHarianAirport').value.replace(/\,/g,"") != ""){
			airport = document.getElementById('txtBebanHarianAirport').value.replace(/\,/g,"");
		}
		if(document.getElementById('txtBebanHarianLain1').value.replace(/\,/g,"") != ""){
			lain1 = document.getElementById('txtBebanHarianLain1').value.replace(/\,/g,"");
		}
		if(document.getElementById('txtBebanHarianLain2').value.replace(/\,/g,"") != ""){
			lain2 = document.getElementById('txtBebanHarianLain2').value.replace(/\,/g,"");
		}
		if(document.getElementById('txtBebanHarianLain3').value.replace(/\,/g,"")!= ""){
			lain3 = document.getElementById('txtBebanHarianLain3').value.replace(/\,/g,"");
		}
		document.getElementById('txtTotalUangSaku').value = document.getElementById('txtBebanHarianUangSaku').value.replace(/\,/g,"")*document.getElementById('txtJmlhHariUangSaku').value.replace(/\,/g,"");
		document.getElementById('txtTotalUangMakan').value = document.getElementById('txtBebanHarianUangMakan').value.replace(/\,/g,"")*document.getElementById('txtJmlhHariUangMakan').value.replace(/\,/g,"");
		document.getElementById('txtTotalHotel').value = hotel*document.getElementById('txtJmlhHariHotel').value.replace(/\,/g,"");
		document.getElementById('txtTotalTaksi').value = taksi;
		document.getElementById('txtTotalAirport').value = airport;
		document.getElementById('txtTotalLain1').value = lain1;
		document.getElementById('txtTotalLain2').value = lain2;
		document.getElementById('txtTotalLain3').value = lain3;
		document.getElementById('txtGrandTotal').value = parseInt(document.getElementById('txtTotalUangSaku').value.replace(/\,/g,""),10) + parseInt(document.getElementById('txtTotalUangMakan').value.replace(/\,/g,""),10) + parseInt(document.getElementById('txtTotalHotel').value.replace(/\,/g,""),10) + parseInt(document.getElementById('txtTotalTaksi').value.replace(/\,/g,""),10) + parseInt(document.getElementById('txtTotalAirport').value.replace(/\,/g,""),10) + parseInt(document.getElementById('txtTotalLain1').value.replace(/\,/g,""),10) + parseInt(document.getElementById('txtTotalLain2').value.replace(/\,/g,""),10) + parseInt(document.getElementById('txtTotalLain3').value.replace(/\,/g,""),10);
		document.getElementById('lblTotalPengajuan').value = document.getElementById('txtGrandTotal').value;
		document.getElementById('lblJmlhPermintaan').value = document.getElementById('txtGrandTotal').value;
	}*/

