
$(function () {
	


	//document.getElementById('btnSubmit').disabled = true;
	checkBtnSubmit();
	$('form').click(function () { checkBtnSubmit(); });

	function checkBtnSubmit() {
		//countDay('uangsaku');
	//	hitungTotal(); changeFormat();
		$('#divError').show();
		if ($('#rbDpaSatu').is(':checked') == false && $('#rbDpaDua').is(':checked') == false) {
			//alert('Harap pilih DPA 1 atau 2');
			$('#divError').html('Tolong tentukan DPA 1 atau 2 pada bagian atas');
			document.getElementById('btnSubmit').disabled = true;
		}  else if ($('#txtTujuan').val() == '' ) {
			//alert('Harap mengisi tujuan dan alasan perjalanan');
			$('#divError').html('Harap mengisi tujuan');
			document.getElementById('btnSubmit').disabled = true;
		} else if ($('#txtAlasan').val() == '' ) {
			//alert('Harap mengisi tujuan dan alasan perjalanan');
			$('#divError').html('Harap jenis Alasan');
			document.getElementById('btnSubmit').disabled = true;
		} else if ($('#txtTanggalCheckIn').val() == '' || $('#txtTanggalCheckOut').val() == '') {
			//alert('Harap masukkan tanggal berangkat dan tanggal kembali');
			$('#divError').html('Harap masukkan tanggal Check In dan tanggal Check Out');
			document.getElementById('btnSubmit').disabled = true;
		} else if ($('#txtBerangkatPesawat').val() == '' || $('#txtPulangPesawat').val() == '') {
			//alert('Harap masukkan tanggal berangkat dan tanggal kembali');
			$('#divError').html('Harap masukkan tanggal keberangkatan pesawat dan tanggal kepulangan pesawat');
			document.getElementById('btnSubmit').disabled = true;
		}
		else if ($('#txtTanggalBerangkat').val() > $('#txtTanggalKembali').val() ) {
			//alert('Harap masukkan tanggal berangkat dan tanggal kembali');
			$('#divError').html('Tanggal Kembali harus lebih BESAR dari Tanggal Berangkat');
			document.getElementById('btnSubmit').disabled = true;
		} 
		else if ($('#txtTanggalCheckIn').val() > $('#txtTanggalCheckOut').val() ) {
			//alert('Harap masukkan tanggal berangkat dan tanggal kembali');
			$('#divError').html('Tanggal Check Out harus lebih BESAR dari Tanggal Check In');
			document.getElementById('btnSubmit').disabled = true;
		}
		else if ($('#txtBerangkatPesawat').val() > $('#txtPulangPesawat').val() ) {
			//alert('Harap masukkan tanggal berangkat dan tanggal kembali');
			$('#divError').html('Tanggal Kepulangan harus lebih BESAR dari Tanggal Keberangkatan');
			document.getElementById('btnSubmit').disabled = true;
		}    
		else {
			document.getElementById('btnSubmit').disabled = false;
			$('#divError').html('');
			$('#divError').hide();
		}
	}



});

	
