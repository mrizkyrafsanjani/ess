DROP PROCEDURE IF EXISTS dpageneraless.generateCutiIzinSpdLibur;
CREATE PROCEDURE dpageneraless.`generateCutiIzinSpdLibur`(INOUT list varchar(4000))
BEGIN
	DECLARE finished INTEGER DEFAULT 0;
  DECLARE v_Kode varchar(255) DEFAULT '';
  DECLARE v_NPK varchar(100);
  DECLARE v_TanggalMulai date;
  DECLARE v_TanggalSelesai date;
  DECLARE v_Tanggal date;
  DECLARE v_Hari varchar(100);
  DECLARE v_Keterangan varchar(255);
    
  -- declare cursor for CutiIzin
    DECLARE kodeCuti_cursor CURSOR FOR 
  SELECT c.KodeCuti,NPK,cj.TanggalMulai,cj.TanggalSelesai,Deskripsi  from cuti c join cutijenistidakhadir cj on c.KodeCuti = cj.KodeCuti join jenistidakhadir jt on cj.KodeJenisTidakHadir = jt.KodeJenisTidakHadir
  WHERE c.TanggalMulai > DATE_ADD(curdate(), INTERVAL -60 DAY) AND StatusApproval = 'APR' AND c.Deleted = 0;
    
  -- declare cursor for SPD
    DECLARE kodeSPD_cursor CURSOR FOR
  SELECT NoSPD,NPK,TanggalBerangkatLap,TanggalKembaliLap FROM trkspd
  WHERE TanggalBerangkatLap > DATE_ADD(curdate(), INTERVAL -60 DAY) AND StatusLaporan = 'S' AND Deleted = 0;
  
  -- declare cursor for libur dan cuti bersama
    DECLARE kodeLibur_cursor CURSOR FOR
  SELECT KodeHariLibur,Tanggal,Hari FROM harilibur; 



    
    DECLARE kodeNPK_cursor CURSOR FOR
  SELECT NPK FROM mstruser
  WHERE Deleted = 0 AND (TanggalBerhenti = '0000-00-00' or TanggalBerhenti IS NULL);  
  

    DECLARE CONTINUE HANDLER 
    FOR NOT FOUND SET finished = 1;


  -- generate all hari --
    
  SET finished = 0;
  OPEN kodeNPK_cursor;
  get_user: LOOP
    FETCH kodeNPK_cursor INTO v_NPK;
    IF finished = 1 THEN 
      LEAVE get_user;
    END IF;
    
    SET v_Tanggal = DATE_ADD(curdate(), INTERVAL -10 DAY);
    insert_AllTgl: LOOP
      IF v_Tanggal <= CURDATE() THEN
        CASE DAYNAME(v_Tanggal)
          WHEN 'Monday' THEN SET v_Hari ='Senin';
          WHEN 'Tuesday' THEN SET v_Hari ='Selasa';
          WHEN 'Wednesday' THEN SET v_Hari ='Rabu';
          WHEN 'Thursday' THEN SET v_Hari ='Kamis';
          WHEN 'Friday' THEN SET v_Hari ='Jumat';
          WHEN 'Saturday' THEN SET v_Hari ='Sabtu';
          WHEN 'Sunday' THEN SET v_Hari ='Minggu';
        END CASE;
        
        SET @v_Jumlah := 0;
        SELECT @v_Jumlah := COUNT(0) FROM absensi WHERE Tanggal = v_Tanggal and NPK = v_NPK;
        
        IF @v_Jumlah = 0 THEN        
          INSERT INTO absensi(NPK, Tanggal, Hari, CreatedOn, CreatedBy) VALUES
          (v_NPK,v_Tanggal,v_Hari,NOW(),'System');          
        END IF;
        


                
        SET v_Tanggal = DATE_ADD(v_Tanggal, INTERVAL +1 DAY);      
        ITERATE insert_AllTgl;
      END IF;
      LEAVE insert_AllTgl;
    END LOOP insert_AllTgl;
  


  
  END LOOP get_user;
  CLOSE kodeNPK_cursor;
  -- end of generate all hari --
 
  -- start untuk bagian cuti
   
     SET finished = 0;
  OPEN kodeCuti_cursor;  
  get_cuti: LOOP
    FETCH kodeCuti_cursor INTO v_Kode,v_NPK,v_TanggalMulai,v_TanggalSelesai,v_Keterangan;
    
    IF finished  = 1 THEN
      LEAVE get_cuti;
    END IF;
    

        SET v_Tanggal = v_TanggalMulai;
    
    get_perTanggal: LOOP      
      IF v_Tanggal <= v_TanggalSelesai THEN
        
        CASE DAYNAME(v_Tanggal)
          WHEN 'Monday' THEN SET v_Hari ='Senin';
          WHEN 'Tuesday' THEN SET v_Hari ='Selasa';
          WHEN 'Wednesday' THEN SET v_Hari ='Rabu';
          WHEN 'Thursday' THEN SET v_Hari ='Kamis';
          WHEN 'Friday' THEN SET v_Hari ='Jumat';
          WHEN 'Saturday' THEN SET v_Hari ='Sabtu';
          WHEN 'Sunday' THEN SET v_Hari ='Minggu';
        END CASE;
        
        SET @v_Jumlah := 0;
        SELECT @v_Jumlah := COUNT(0) FROM absensi WHERE Tanggal = v_Tanggal AND NPK = v_NPK;
        
        IF @v_Jumlah > 0 THEN
          UPDATE absensi SET Hari = v_Hari, Keterangan = v_Keterangan WHERE Tanggal = v_Tanggal AND NPK = v_NPK;
        ELSE
          INSERT INTO absensi(NPK, Tanggal, Hari, Keterangan, CreatedOn, CreatedBy) VALUES
          (v_NPK,v_Tanggal,v_Hari,v_Keterangan,NOW(),'System');
        END IF;
        


                
        SET v_Tanggal = DATE_ADD(v_Tanggal, INTERVAL +1 DAY);
        ITERATE get_perTanggal;
      END IF;
      LEAVE get_perTanggal;
    END LOOP get_perTanggal;
    
  END LOOP get_cuti;
  CLOSE kodeCuti_cursor;
  -- end bagian cuti
  
  -- ---- start untuk bagian SPD -------
    
    SET finished = 0;
  OPEN kodeSPD_cursor;
  get_spd: LOOP 
    FETCH kodeSPD_cursor INTO v_Kode,v_NPK,v_TanggalMulai,v_TanggalSelesai;
    IF finished  = 1 THEN
      LEAVE get_spd;
    END IF;
    

        SET v_Tanggal = v_TanggalMulai;
    
    get_perTanggalSPD: LOOP      
      IF v_Tanggal <= v_TanggalSelesai THEN
        
        CASE DAYNAME(v_Tanggal)
          WHEN 'Monday' THEN SET v_Hari ='Senin';
          WHEN 'Tuesday' THEN SET v_Hari ='Selasa';
          WHEN 'Wednesday' THEN SET v_Hari ='Rabu';
          WHEN 'Thursday' THEN SET v_Hari ='Kamis';
          WHEN 'Friday' THEN SET v_Hari ='Jumat';
          WHEN 'Saturday' THEN SET v_Hari ='Sabtu';
          WHEN 'Sunday' THEN SET v_Hari ='Minggu';
        END CASE;
        
        SET @v_Jumlah := 0;
        SELECT @v_Jumlah := COUNT(0) FROM absensi WHERE Tanggal = v_Tanggal AND NPK = v_NPK;
        
        IF @v_Jumlah > 0 THEN
          UPDATE absensi SET Hari = v_Hari, Keterangan = 'Perjalanan Dinas' WHERE Tanggal = v_Tanggal AND NPK = v_NPK;
        ELSE
          INSERT INTO absensi(NPK, Tanggal, Hari, Keterangan, CreatedOn, CreatedBy) VALUES
          (v_NPK,v_Tanggal,v_Hari,'Perjalanan Dinas',NOW(),'System');
        END IF;
        
        SET v_Tanggal = DATE_ADD(v_Tanggal, INTERVAL +1 DAY);
        ITERATE get_perTanggalSPD;
      END IF;
      LEAVE get_perTanggalSPD;
    END LOOP get_perTanggalSPD;
    
  END LOOP get_spd;
  CLOSE kodeSPD_cursor;
  -- end bagian SPD --
  
  -- start untuk bagian hari Libur --
    
    SET finished = 0;
  OPEN kodeLibur_cursor;
  get_libur: LOOP
    FETCH kodeLibur_cursor INTO v_Kode,v_Tanggal,v_Keterangan;
    IF finished  = 1 THEN
      LEAVE get_libur;
    END IF;
    
    IF v_Keterangan = 'cb' THEN
      SET v_Keterangan = 'Cuti Bersama';
    END IF;
    
    CASE DAYNAME(v_Tanggal)
      WHEN 'Monday' THEN SET v_Hari ='Senin';
      WHEN 'Tuesday' THEN SET v_Hari ='Selasa';
      WHEN 'Wednesday' THEN SET v_Hari ='Rabu';
      WHEN 'Thursday' THEN SET v_Hari ='Kamis';
      WHEN 'Friday' THEN SET v_Hari ='Jumat';
      WHEN 'Saturday' THEN SET v_Hari ='Sabtu';
      WHEN 'Sunday' THEN SET v_Hari ='Minggu';
    END CASE;
    
    SET @v_Jumlah := 0;
    SELECT @v_Jumlah := COUNT(0) FROM absensi WHERE Tanggal = v_Tanggal;
    
    IF @v_Jumlah > 0 THEN
      UPDATE absensi SET Hari = v_Hari, Keterangan = v_Keterangan WHERE Tanggal = v_Tanggal;
    ELSE
      INSERT INTO absensi(NPK, Tanggal, Hari, Keterangan, CreatedOn, CreatedBy) VALUES
      ('Libur',v_Tanggal,v_Hari,v_Keterangan,NOW(),'System');
    END IF;
    
  END LOOP get_libur;
  CLOSE kodeLibur_cursor;
  -- end bagian Libur --

  
  -- update view karyawan lembur bagian realisasi 

  update lembur b
    JOIN absensi a ON a.NPK = b.NPK and a.Tanggal = b.Tanggal
  set b.JamMulaiRealisasi = 
    case when a.WaktuKeluar > b.JamMulaiRencana then b.JamMulaiRencana
    else null
    end , b.JamSelesaiRealisasi =
    case when a.WaktuKeluar > b.JamMulaiRencana then a.WaktuKeluar
    else null
    end 
  WHERE b.Deleted = 0 and a.deleted = 0 and b.StatusLembur = 'APP' and a.Tanggal > DATE_ADD(curdate(), INTERVAL -60 DAY);

  -- end of update view realisasi karyawan lembur

    -- update total jam lembur
  update lembur b
  set b.TotalJamRealisasi = TIME_TO_SEC(timediff(b.JamSelesaiRealisasi,b.JamMulaiRealisasi))/3600
  where b.Deleted = 0 and b.JamMulaiRealisasi is not null and 
    b.JamSelesaiRealisasi is not null;
  
END;
