<html>
	<!--
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/myfunction.js"></script>
	-->
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-ui.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function lookUpUsername(){
			name = document.getElementById('txtNPK').value;
			if(name !== ''){
				$.post( 
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ajax_lookUpNPK',
					 { NPK: name },
					 function(response) {  
						if (response == 0) {					   
						   alert('Sudah ada NPK '+ name +', mohon diganti dengan NPK lain.');
						   $('#txtNPK').focus();
						}
					 }
				);
			}
		}
		
		function btnBatal_click(){
			if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ajax_batalEdit',
					{},
					function(response){
						alert(response);
						location.reload();
					}
				);
			}
		}
		
		
		$(function() {
			
		  });
		  
		$(document).ready(function(){
			<?php if($this->session->flashdata('msg')){ ?>
			alert('<?php echo $this->session->flashdata('msg'); ?>');
			<?php } ?>
		});
	</script>
	<head>
		<title>Update NPK PKWT Menjadi Tetap</title>
		<!--site css -->
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Data User Terkait</h3>
		</div>
		<div class="panel-body">
		<form action="<?php echo $this->config->base_url(); ?>index.php/Pengaturan/updatePKWTtoTetap/do_updatePKWTtoTetap/<?php echo $npkLama; ?>" method="post" accept-charset="utf-8">

<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2"> <?php //echo $this->config->base_url(); ?></td>
	  <input id="txtJenisProses" name="txtJenisProses" type="text" hidden value="edit">
	  
	  <input id="txtTanggalMulaiBekerjaLama" name="txtTanggalMulaiBekerjaLama" type="text" hidden value="<?php if(set_value('txtTanggalMulaiBekerjaLama')!=''){ echo set_value('txtTanggalMulaiBekerjaLama'); }else { echo $TanggalMulaiBekerjaLama; } ?>" readonly><?php echo form_error('txtTanggalMulaiBekerjaLama'); ?></td>
	</tr>
	<!-- <tr>
	  <td align="center"><img alt="Foto Karyawan" src="<?php echo $this->config->base_url() . $urlfoto; ?>" width="150px" height="200px"></td>
	  
	</tr> -->
	
	
	
	</tbody>
</table>

<fieldset>
	<!-- <legend>Data Kepegawaian</legend> -->
	<table border=0>
		<tbody>
		<tr>
		  <td class="lbl">Nama Lengkap *</td>
		  <td><input id="txtNamaLengkap" class="form-control" name="txtNamaLengkap" type="text" size="40%" 
		  		value="<?php if(set_value('txtNamaLengkap')!=''){ echo set_value('txtNamaLengkap'); }else { echo $nama; } ?>" readonly><?php echo form_error('txtNamaLengkap'); ?></td>
		</tr>
		<tr>
		  <td class="lbl">DPA *</td>
		  <td>
		  

		  <input id="txtNamaLengkap" class="form-control" name="txtNamaLengkap" type="text" size="40%" 
		  		value="<?php if(set_value('txtNamaLengkap')!=''){ echo set_value('txtNamaLengkap'); }else { echo $dpa; } ?>" readonly><?php echo form_error('txtNamaLengkap'); ?>

		  </td>	  
		</tr>
		<tr>
		  <td class="lbl">NPK Lama*</td>
		  <td><input id="txtNPKLama" class="form-control"  name="txtNPKLama" type="text" size="25" maxlength="6" readonly value= "<?php echo $npkLama; ?>" ><?php echo form_error('txtNPKLama'); ?></td>
		</tr>
		
		<tr>
		  <td class="lbl">Tanggal Mulai Bekerja *</td>
		  <td><input class="form-control"  id="
		  " name='txtTanggalMulaiBekerjaBaru' type="date" size="15" value="<?php if(set_value('txtTanggalMulaiBekerjaBaru')!=''){ echo set_value('txtTanggalMulaiBekerjaBaru'); }else { echo $TanggalMulaiBekerjaBaru; } ?>"><?php echo form_error('txtTanggalMulaiBekerjaBaru'); ?></td>

		</tr>
		<!--<tr>
		  <td class="lbl">Tanggal Berhenti</td>
		  <td></td>
		</tr>
		<tr>
		  <td class="lbl">Keterangan Berhenti</td>
		  <td><input id="txtKeteranganBerhenti" name="txtKeteranganBerhenti" type="text" size="25" ></td>
		</tr>	-->
		<!-- <tr>
		  <td class="lbl">Status Karyawan *</td>
		  <td>
			<?php
				$selectedTetap = "";
				$selectedPKWT = "";
				$selectedOutsource = "";
				$selectedAI = "";
				
				switch($statuskaryawan)
				{
					case "Tetap": $selectedTetap = "selected"; break;
					case "PKWT": $selectedPKWT = "selected"; break;
					case "Outsource": $selectedOutsource = "selected"; break;
					case "Assignment AI": $selectedAI = "selected"; break;
				}
			?>
		    <select class="form-control"  name="cmbStatusKaryawan">
				<option value=''></option>
				<option value='Tetap' <?php echo $selectedTetap; ?> >Tetap</option>
				<option value='PKWT' <?php echo $selectedPKWT; ?> >PKWT</option>
				<option value='Outsource' <?php echo $selectedOutsource; ?> >Outsource</option>
				<option value='Assignment AI' <?php echo $selectedAI; ?> >Assignment AI</option>
			</select>
			<?php echo form_error('cmbStatusKaryawan'); ?>
		  </td>
		</tr> -->
		<tr>
		  <td class="lbl">NPK Baru *</td>
		  <td><input id="txtNPKBaru" class="form-control"  name="txtNPKBaru" type="text" size="25" maxlength="6"  value= "<?php echo $npkBaru; ?>" ><?php echo form_error('txtNPKBaru'); ?></td>
		</tr>
		
		</tbody>
	</table>
	
</fieldset>


		<div id="note">
		</div>
		<table class="tblPureNoBorder" style="float:right;">
			<tr>
				<td>
				<br/><input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin mengupdate data ini ke dalam database?')" id="btnSubmit" name="submitForm" value="Submit">
				
				<input id="btnBatal" class="btn" type="button" value="Batal" onClick="btnBatal_click()">
				<div id="divError"></div>
				</td>
			</tr>
		</table>
			<br/>
		</form>
		</div>
		</div>
	</body>
</html>