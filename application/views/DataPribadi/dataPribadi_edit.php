<html>
	<!--
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/myfunction.js"></script>
	-->
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-ui.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function lookUpUsername(){
			name = document.getElementById('txtNPK').value;
			if(name !== ''){
				$.post( 
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ajax_lookUpNPK',
					 { NPK: name },
					 function(response) {  
						if (response == 0) {					   
						   alert('Sudah ada NPK '+ name +', mohon diganti dengan NPK lain.');
						   $('#txtNPK').focus();
						}
					 }
				);
			}
		}
		
		function btnBatal_click(){
			if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ajax_batalEdit',
					{},
					function(response){
						alert(response);
						location.reload();
					}
				);
			}
		}
		
		function alertsizeRwyPendidikan(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatPendidikan').style.height=pixels+"px";
		}
		
		function alertsizeRwyTraining(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatTraining').style.height=pixels+"px";
		}
		
		function alertsizeRwyPekerjaan(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatPekerjaan').style.height=pixels+"px";
		}
		
		function alertsizeRwyRotasi(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatRotasi').style.height=pixels+"px";
		}
		
		function alertsizeKeluargaBesar(pixels){
			pixels+=32;
			document.getElementById('iFrameKeluargaBesar').style.height=pixels+"px";
		}
		
		function alertsizeKeluargaInti(pixels){
			pixels+=32;
			document.getElementById('iFrameKeluargaInti').style.height=pixels+"px";
		}
		
		$(function() {
			$( "#dialogKTP" ).dialog({
			  autoOpen: false			  
			});
			$( "#dialogNPWP" ).dialog({
			  autoOpen: false			  
			});
			$( "#dialogManulife" ).dialog({
			  autoOpen: false			  
			});
		 
			$( "#popupManulife" ).click(function() {
			  $( "#dialogManulife" ).dialog( "open" );
			});
			
			$( "#dialogJamsostek" ).dialog({
			  autoOpen: false			  
			});
			
			$( "#popupKTP" ).click(function() {
			  $( "#dialogKTP" ).dialog( "open" );
			});
			$( "#popupNPWP" ).click(function() {
			  $( "#dialogNPWP" ).dialog( "open" );
			});
			$( "#popupJamsostek" ).click(function() {
			  $( "#dialogJamsostek" ).dialog( "open" );
			});
			
			$( "#dialogGardaMedika" ).dialog({
			  autoOpen: false			  
			});
		 
			$( "#popupGardaMedika" ).click(function() {
			  $( "#dialogGardaMedika" ).dialog( "open" );
			});
		  });
		  
		$(document).ready(function(){
			<?php if($this->session->flashdata('msg')){ ?>
			alert('<?php echo $this->session->flashdata('msg'); ?>');
			<?php } ?>
		});
	</script>
	<head>
		<title>Data Pribadi Karyawan</title>
		<!--site css -->
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Data Pribadi</h3>
		</div>
		<div class="panel-body">
		<form action="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/TambahDataPribadi/<?php echo $npk; ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2"> <?php //echo $this->config->base_url(); ?></td>
	  <input id="txtJenisProses" name="txtJenisProses" type="text" hidden value="edit">
	  </td>
	</tr>
	<tr>
	  <td align="center"><img alt="Foto Karyawan" src="<?php echo $this->config->base_url() . $urlfoto; ?>" width="150px" height="200px"></td>
	  <td><input id="inputFileFoto" name="inputFileFoto" type="file"></td>	  
	</tr>
	<tr>
	  <td class="lbl">Nama Lengkap *</td>
	  <td><input id="txtNamaLengkap" class="form-control" name="txtNamaLengkap" type="text" size="40%" value="<?php if(set_value('txtNamaLengkap')!=''){ echo set_value('txtNamaLengkap'); }else { echo $nama; } ?>"><?php echo form_error('txtNamaLengkap'); ?></td>
	</tr>
	<tr>
	  <td class="lbl">Tempat Lahir *</td>
	  <td><input id="txtTempatLahir" class="form-control" name="txtTempatLahir" type="text" size ="40%" value="<?php if(set_value('txtTempatLahir')!=''){ echo set_value('txtTempatLahir'); }else { echo $tempatlahir; } ?>"><?php echo form_error('txtTempatLahir'); ?></td>
	</tr>
	<tr>
	  <td class="lbl">Tanggal Lahir *</td>
	  <td><input class="form-control" id="txtTanggalLahir" name='txtTanggalLahir' type="date" size="15" value="<?php if(set_value('txtTanggalLahir')!=''){ echo set_value('txtTanggalLahir'); }else { echo $tanggallahir; } ?>"><?php echo form_error('txtTanggalLahir'); ?></td>
	</tr>
	<tr>
	  <td class="lbl">Jenis Kelamin *</td>
	  <td>
	  <?php 
		$checked11 = "";
		$checked22 = "";
		if(set_radio('rbJenisKelamin', 'L') != '' || set_radio('rbJenisKelamin', 'P') != '')
		{
			if(set_radio('rbJenisKelamin', 'L') != ''){
				$checked11 = "checked";
			}else{
				$checked22 = "checked";
			}
		}
		else
		{
			if($jeniskelamin == "L"){
				$checked11 = "checked";
			}else{
				$checked22 = "checked";
			}
		}
	  ?>
	  <input type="radio" id="rbLaki" name="rbJenisKelamin" value="L" <?php echo $checked11; ?> >Laki-laki
	  <input type="radio" id="rbPerempuan" name="rbJenisKelamin" value="P" <?php echo $checked22; ?> >Perempuan	  
	  <?php echo form_error('rbJenisKelamin'); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">Alamat KTP *</td>
	  <td><textarea id="txtAlamatKTP" class="form-control" name='txtAlamatKTP' type="text" cols="60" rows="3" ><?php if(set_value('txtAlamatKTP')!=''){ echo set_value('txtAlamatKTP'); }else { echo $alamatktp; } ?></textarea><?php echo form_error('txtAlamatKTP'); ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Alamat Tinggal</td>
	  <td><textarea id="txtAlamatTinggal" class="form-control" name='txtAlamatTinggal' type="text" cols="60" rows="3" ><?php if(set_value('txtAlamatTinggal')!=''){ echo set_value('txtAlamatTinggal'); }else { echo $alamattinggal; } ?></textarea></td>
	  
	</tr>
	<tr>
	  <td class="lbl">No. Telp</td>
	  <td><input id="txtNoTelp" class="form-control" name="txtNoTelp" type="tel" size="20" value="<?php if(set_value('txtNoTelp')!=''){ echo set_value('txtNoTelp'); }else { echo $notelp; } ?>" ></td>
	  
	</tr>
	<tr>
	  <td class="lbl">No. HP</td>
	  <td><input id="txtNoHP" class="form-control" name="txtNoHP" type="tel" size="20" value="<?php if(set_value('txtNoHP')!=''){ echo set_value('txtNoHP'); }else { echo $nohp; } ?>" ></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Status Kawin *</td>
	  <td>
	  <?php 
		$checkedB = "";
		$checkedS = "";
		if(set_radio('rbStatusKawin', 'S') != '' || set_radio('rbStatusKawin', 'B') != '')
		{
			if(set_radio('rbStatusKawin', 'B') != ''){
				$checkedB = "checked";
			}else{
				$checkedS = "checked";
			}
		}
		else
		{
			if($statuskawin == "B"){
				$checkedB = "checked";
			}else{
				$checkedS = "checked";
			}
		}
	  ?>
	  <input type="radio" id="rbKawin" name="rbStatusKawin" value="S" <?php echo $checkedS; ?> >Kawin
	  <input type="radio" id="rbBelumKawin" name="rbStatusKawin" value="B" <?php echo $checkedB; ?> >Belum Kawin
	  <?php echo form_error('rbStatusKawin'); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">KTP</td>
	  <td>
			<div class="col-md-6"><input id="txtNoKTP" class="form-control" name="txtNoKTP" type="text" size="25" value = "<?php if(set_value('txtNoKTP')!=''){ echo set_value('txtNoKTP'); }else { echo $noktp; } ?>" > 
			<?php if($urlktp != ''){ ?> 
			<div id="popupKTP" class="btn btn-link">Scan KTP</div> 
			<div id="dialogKTP" title="KTP">
			  <p><img alt="Scan KTP" src="<?php echo $this->config->base_url().$urlktp ?>"></p>
			</div>
			<?php } ?>
			</div>
			<div class="col-md-6"><input id="inputFileKTP" class="form-control" name="inputFileKTP" type="file"> </div>
	  </td>	  
	</tr>
	<tr>
	  <td class="lbl">NPWP</td>
	  <td>
		<div class="col-md-6"><input id="txtNoNPWP" class="form-control" name="txtNoNPWP" type="text" size="25" value= "<?php if(set_value('txtNoNPWP')!=''){ echo set_value('txtNoNPWP'); }else { echo $npwp; } ?>" > 
		<?php if($urlnpwp != ''){ ?>   
		<div id="popupNPWP" class="btn btn-link">Scan NPWP</div>
		<div id="dialogNPWP" title="NPWP">
		  <p><img alt="Scan NPWP" src="<?php echo $this->config->base_url().$urlnpwp ?>"></p>
		</div>		
		<?php } ?>
		</div>
		<div class="col-md-6"><input id="inputFileNPWP" class="form-control" name="inputFileNPWP" type="file"> </div>
	  </td>
	</tr>
	</tbody>
</table>

<fieldset>
	<legend>Data Kepegawaian</legend>
	<table border=0>
		<tbody>
		<tr>
		  <td class="lbl">DPA *</td>
		  <td>
		  <?php 
			$checkedDPA1 = "";
			$checkedDPA2 = "";
			if($dpa == 1){
				$checkedDPA1 = "checked";
			}else{
				$checkedDPA2 = "checked";
			}
		  ?> 
		  <input type="radio" id="rbDPA1" name="rbDPA" value="1" <?php echo $checkedDPA1; ?> >Satu
		  <input type="radio" id="rbDPA2" name="rbDPA" value="2" <?php echo $checkedDPA2; ?> >Dua
		  <?php echo form_error('rbDPA'); ?></td>	  
		</tr>
		<tr>
		  <td class="lbl">NPK *</td>
		  <td><input id="txtNPK" class="form-control"  name="txtNPK" type="text" size="25" maxlength="6" readonly value= "<?php echo $npk; ?>" ><?php echo form_error('txtNPK'); ?></td>
		</tr>
		<!--
		<tr>
		  <td class="lbl">Departemen *</td>
		  <td>
			<select name="cmbDepartemen">
			  <option value=''></option>
			<?php 
			  
			  for($i=0;$i<count($dataDepartemen);$i++)
			  {
				$selectedstr = "";
				if($departemen == $dataDepartemen[$i]['KodeDepartemen'])
				{
					$selectedstr = "selected";
				}
				echo "<option value='".$dataDepartemen[$i]['KodeDepartemen']."' ". $selectedstr .">".$dataDepartemen[$i]['NamaDepartemen']."</option>";				
			  }
			  ?>
			</select>
		  </td>
		</tr>
		<tr>
		  <td class="lbl">Jabatan *</td>
		  <td><input id="txtJabatan" name="txtJabatan" type="text" size="25" value= "<?php echo $jabatan; ?>"k ></td>
		</tr>
		<tr>
		  <td class="lbl">Golongan *</td>
		  <td>
			<select name="cmbGolongan">
			  <option value=''></option>
			<?php 			  
			  for($i=0;$i<count($dataGolongan);$i++)
			  {
				$selectedstr = "";
				if($golongan == $dataGolongan[$i]['Golongan']){ $selectedstr = "selected"; }
				echo "<option value='".$dataGolongan[$i]['Golongan']."' ".$selectedstr." >".$dataGolongan[$i]['Golongan']."</option>";				
			  }
			  ?>
			</select>
		  </td>
		</tr>
		-->
		<tr>
		  <td class="lbl">Tanggal Bekerja *</td>
		  <td><input class="form-control"  id="txtTanggalBekerja" name='txtTanggalBekerja' type="date" size="15" value="<?php if(set_value('txtTanggalBekerja')!=''){ echo set_value('txtTanggalBekerja'); }else { echo $tanggalbekerja; } ?>"><?php echo form_error('txtTanggalBekerja'); ?></td>
		</tr>
		<!--<tr>
		  <td class="lbl">Tanggal Berhenti</td>
		  <td></td>
		</tr>
		<tr>
		  <td class="lbl">Keterangan Berhenti</td>
		  <td><input id="txtKeteranganBerhenti" name="txtKeteranganBerhenti" type="text" size="25" ></td>
		</tr>	-->
		<tr>
		  <td class="lbl">Status Karyawan *</td>
		  <td>
			<?php
				$selectedTetap = "";
				$selectedPKWT = "";
				$selectedOutsource = "";
				$selectedAI = "";
				
				switch($statuskaryawan)
				{
					case "Tetap": $selectedTetap = "selected"; break;
					case "PKWT": $selectedPKWT = "selected"; break;
					case "Outsource": $selectedOutsource = "selected"; break;
					case "Assignment AI": $selectedAI = "selected"; break;
				}
			?>
		    <select class="form-control"  name="cmbStatusKaryawan">
				<option value=''></option>
				<option value='Tetap' <?php echo $selectedTetap; ?> >Tetap</option>
				<option value='PKWT' <?php echo $selectedPKWT; ?> >PKWT</option>
				<option value='Outsource' <?php echo $selectedOutsource; ?> >Outsource</option>
				<option value='Assignment AI' <?php echo $selectedAI; ?> >Assignment AI</option>
			</select>
			<?php echo form_error('cmbStatusKaryawan'); ?>
		  </td>
		</tr>
		<tr>
		  <td class="lbl">No. Ext.</td>
		  <td><input id="txtNoExt" class="form-control"  name="txtNoExt" type="text" size="25" value= "<?php if(set_value('txtNoExt')!=''){ echo set_value('txtNoExt'); }else { echo $noext; } ?>" ></td>
		</tr>
		<tr>
		  <td class="lbl">Email Internal</td>
		  <td><input id="txtEmailInternal" class="form-control"  name="txtEmailInternal" type="email" size="25" value= "<?php if(set_value('txtEmailInternal')!=''){ echo set_value('txtEmailInternal'); }else { echo $emailinternal; } ?>" ></td>
		</tr>
		<tr>
		  <td class="lbl">Manulife</td>
		  <td>
			<div class="col-md-6"><input id="txtIDManulife" class="form-control"  name="txtIDManulife" type="text" size="25" value= "<?php if(set_value('txtIDManulife')!=''){ echo set_value('txtIDManulife'); }else { echo $iddanapensiun; } ?>" ><div id="popupManulife" class="btn btn-link">Scan Manulife</div></div>
			<div class="col-md-6"><input id="inputFileManulife" class="form-control" name="inputFileManulife" type="file"></div>
		  </td>
		</tr>
		<tr>
		  <td class="lbl">Jamsostek</td>
		  <td>
			<div class="col-md-6"><input id="txtIDJamsostek" class="form-control"  name="txtIDJamsostek" type="text" size="25" value= "<?php if(set_value('txtIDJamsostek')!=''){ echo set_value('txtIDJamsostek'); }else { echo $idjamsostek; } ?>" >
			<div class="btn btn-link" id="popupJamsostek">Scan Jamsostek</div></div>
			<div class="col-md-6"><input id="inputFileJamsostek" class="form-control" name="inputFileJamsostek" type="file"></div>
		  </td>
		</tr>
		<tr>
		  <td class="lbl">Garda Medika</td>
		  <td>
			<div class="col-md-6"><input id="txtIDGardaMedika" class="form-control"  name="txtIDGardaMedika" type="text" size="25" value="<?php if(set_value('txtIDGardaMedika')!=''){ echo set_value('txtIDGardaMedika'); }else { echo $idgardamedika; } ?>" >
			<div class="btn btn-link" id="popupGardaMedika">Scan Garda Medika</div></div>
			<div class="col-md-6"><input id="inputFileGardaMedika" class="form-control" name="inputFileGardaMedika" type="file"></div>
		  </td>
		</tr>
		</tbody>
	</table>
	<div id="dialogManulife" title="Manulife">
	  <p><img alt="Scan Manulife" src="<?php echo $this->config->base_url().$urldanapensiun; ?>"></p>
	</div>
	<div id="dialogJamsostek" title="Jamsostek">
	  <p><img alt="Scan Jamsostek" src="<?php echo $this->config->base_url().$urljamsostek; ?>"></p>
	</div>
	<div id="dialogGardaMedika" title="Garda Medika">
	  <p><img alt="Scan Garda Medika" src="<?php echo $this->config->base_url().$urlgardamedika; ?>"></p>
	</div>
</fieldset>

<fieldset>
	<legend>Data Rekening</legend>
	<table border=0>
		<tbody>
		<tr>
		  <td class="lbl">No. Rekening *</td>
		  <td><input id="txtNoRekening" class="form-control" name="txtNoRekening" type="text" size="25" value="<?php if(set_value('txtNoRekening')!=''){ echo set_value('txtNoRekening'); }else { echo $norek; } ?>"><?php echo form_error('txtNoRekening'); ?></td>
		</tr>
		<tr>
		  <td class="lbl">Atas Nama *</td>
		  <td><input id="txtAtasNama" class="form-control" name="txtAtasNama" type="text" size="25" value="<?php if(set_value('txtAtasNama')!=''){ echo set_value('txtAtasNama'); }else { echo $atasnama; } ?>"><?php echo form_error('txtAtasNama'); ?></td>
		</tr>
		<tr>
		  <td class="lbl">Bank *</td>
		  <td><input id="txtBank" class="form-control" name="txtBank" type="text" size="25" value="<?php if(set_value('txtBank')!=''){ echo set_value('txtBank'); }else { echo $namabank; } ?>"><?php echo form_error('txtBank'); ?></td>
		</tr>
		<tr>
		  <td class="lbl">Cabang *</td>
		  <td><input id="txtCabang" class="form-control" name="txtCabang" type="text" size="25" value="<?php if(set_value('txtCabang')!=''){ echo set_value('txtCabang'); }else { echo $cabang; } ?>"><?php echo form_error('txtCabang'); ?></td>
		</tr>
		</tbody>
	</table>
</fieldset>

<fieldset>
	<legend>Data Kontak Keluarga</legend>
	<table border=0>
		<tbody>
			<tr>
			  <td class="lbl">Nama Kontak</td>
			  <td><input id="txtNamaKontak" class="form-control" name="txtNamaKontak" type="text" size="25" value= "<?php if(set_value('txtNamaKontak')!=''){ echo set_value('txtNamaKontak'); }else { echo $namakontakkeluarga; } ?>" ></td>
			</tr>
			<tr>
			  <td class="lbl">Hubungan Keluarga</td>
			  <td><input id="txtHubunganKeluarga" class="form-control" name="txtHubunganKeluarga" type="text" size="25" value= "<?php if(set_value('txtHubunganKeluarga')!=''){ echo set_value('txtHubunganKeluarga'); }else { echo $hubungankeluarga; } ?>" ></td>
			</tr>
			<tr>
			  <td class="lbl">No. Telp</td>
			  <td><input id="txtNoTelpKontakKeluarga" class="form-control" name="txtNoTelpKontakKeluarga" type="tel" size="25" value= "<?php if(set_value('txtNoTelpKontakKeluarga')!=''){ echo set_value('txtNoTelpKontakKeluarga'); }else { echo $notelpkontakkeluarga; } ?>" ></td>
			</tr>
			<tr>
			  <td class="lbl">No. HP</td>
			  <td><input id="txtNoHPKontakKeluarga" class="form-control" name="txtNoHPKontakKeluarga" type="tel" size="25" value= "<?php if(set_value('txtNoHPKontakKeluarga')!=''){ echo set_value('txtNoHPKontakKeluarga'); }else { echo $nohpkontakkeluarga; } ?>" ></td>
			</tr>
		</tbody>
	</table>
</fieldset>

<fieldset>
	<legend>Medical dan Cuti</legend>
	<table border=0>
		<tbody>
			<!--<tr>
			  <td class="lbl">*Medical</td>
			  <td><?php echo "Rp. ".number_format($medical); ?>
			  </td>
			</tr>-->
			<tr>
			  <td class="lbl">*Cuti Tahunan</td>
			  <td><?php echo $cutiTahunan; ?>
			  <!--<input id="txtCutiTahunan" class="form-control" name="txtCutiTahunan" type="text" size="25" value="<?php echo $cutiTahunan; ?>">-->
			  </td>
			</tr>
			<tr>
			  <td class="lbl">*Cuti Besar</td>
			  <td><?php echo $cutiBesar; ?>
			  <!--<input id="txtCutiBesar" class="form-control" name="txtCutiBesar" type="text" size="25" value="<?php echo $cutiBesar; ?>">-->
			  </td>
			</tr>
		</tbody>
	</table>
</fieldset>

<fieldset>
	<legend>Karyawan Berhenti</legend>
	<table border=0>
		<tbody>
			<tr>
			  <td class="lbl">Tanggal Berhenti</td>
			  <td><input id="txtTanggalBerhenti" class="form-control" name="txtTanggalBerhenti" type="text" size="25"  value= "<?php if(set_value('txtTanggalBerhenti')!=''){ echo set_value('txtTanggalBerhenti'); }else { echo $tanggalberhenti; } ?>" ></td>
			</tr>
			<tr>
			  <td class="lbl">Alasan Berhenti</td>
			  <td><textarea class="form-control" rows="3" cols="50" id="txtAlasanBerhenti" name="txtAlasanBerhenti" ><?php if(set_value('txtAlasanBerhenti')!=''){ echo set_value('txtAlasanBerhenti'); }else { echo $alasanberhenti; } ?></textarea></td>
			</tr>
		</tbody>
	</table>
</fieldset>
<br/>
<fieldset>
<legend>Riwayat Pendidikan</legend>
<iframe id="iFrameRiwayatPendidikan" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/RiwayatPendidikan/index/<?php echo $npk; ?>" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>

<fieldset>
<legend>Riwayat Training</legend>
<iframe id="iFrameRiwayatTraining" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/RiwayatTraining/index/<?php echo $npk; ?>" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Riwayat Pekerjaan</legend>
<iframe id="iFrameRiwayatPekerjaan" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/RiwayatPekerjaan/index/<?php echo $npk; ?>" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Riwayat Mutasi/Rotasi/Demosi/Promosi Pekerjaan DPA</legend>
<iframe id="iFrameRiwayatRotasi" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/RiwayatRotasiPekerjaan/index/<?php echo $npk; ?>" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Keluarga Besar</legend>
<iframe id="iFrameKeluargaBesar" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/Keluarga/KeluargaBesar/<?php echo $npk; ?>" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Keluarga Inti</legend>
<iframe id="iFrameKeluargaInti" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/Keluarga/KeluargaInti/<?php echo $npk; ?>" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<div id="note">
</div>
<table class="tblPureNoBorder" style="float:right;">
	<tr>
		<td>
		<br/><input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin mengedit data ini ke dalam database?')" id="btnSubmit" name="submitForm" value="Edit">
		
		<input id="btnBatal" class="btn" type="button" value="Batal" onClick="btnBatal_click()">
		<div id="divError"></div>
		</td>
	</tr>
</table>
			<br/>
		</form>
		</div>
		</div>
	</body>
</html>