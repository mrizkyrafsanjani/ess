<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/myfunction.js"></script>-->
	<script type="text/javascript">
		
		function btnApprove_click(){
			if(confirm('Apakah Anda yakin ingin approval data ini?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ProsesApprovalDataPribadiUser/<?php echo $databaru['kodetempmstruser']; ?>/AP',
					{},
					function(response){
						alert(response);
						window.location.replace('<?php echo $this->config->base_url(); ?>index.php/UserTask');
					}
				);
			}
		}
		
		function btnDecline_click(){
			if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ProsesApprovalDataPribadiUser/<?php echo $databaru['kodetempmstruser']; ?>/DE',
					{},
					function(response){
						alert(response);
						window.location.replace('<?php echo $this->config->base_url(); ?>index.php/UserTask');
					}
				);
			}
		}
		
		function alertsizeRwyPendidikan(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatPendidikan').style.height=pixels+"px";
		}
		
		function alertsizeKeluargaInti(pixels){
			pixels+=32;
			document.getElementById('iFrameKeluargaInti').style.height=pixels+"px";
		}
		
		function alertsizeTempRwyPendidikan(pixels){
			pixels+=32;
			document.getElementById('iFrameTempRiwayatPendidikan').style.height=pixels+"px";
		}
		
		function alertsizeTempKeluargaInti(pixels){
			pixels+=32;
			document.getElementById('iFrameTempKeluargaInti').style.height=pixels+"px";
		}
		
	</script>
	<head>
		<title>Data Pribadi Karyawan</title>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div id="DataLama">
<fieldset><legend>Data Lama</legend>
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2">Data Pribadi <?php //echo $this->config->base_url(); ?></td>
	  <input id="txtJenisProses" name="txtJenisProses" type="text" hidden value="edit">
	  </td>
	</tr>
	<tr>
	  <td class="lbl">Nama Lengkap *</td>
	  <td><?php echo $datalama['nama']; ?></td>	  
	</tr>	
	<tr>
	  <td class="lbl">Alamat Tinggal</td>
	  <td><textarea id="txtAlamatTinggal" name='txtAlamatTinggal' type="text" cols="60" rows="3" ><?php echo $datalama['alamattinggal']; ?></textarea></td>
	  
	</tr>
	<tr>
	  <td class="lbl">No. Telp</td>
	  <td><input id="txtNoTelp" name="txtNoTelp" type="text" size="20" value="<?php echo $datalama['notelp']; ?>" ></td>
	  
	</tr>
	<tr>
	  <td class="lbl">No. HP</td>
	  <td><input id="txtNoHP" name="txtNoHP" type="text" size="20" value="<?php echo $datalama['nohp']; ?>" ></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Status Kawin *</td>
	  <td>
	  <?php 
		$checkedB = "";
		$checkedS = "";
		if($datalama['statuskawin'] == "B"){
			$checkedB = "checked";
		}else{
			$checkedS = "checked";
		}
	  ?>
	  <input type="radio" id="rbKawinLama" name="rbStatusKawinLama" value="S" <?php echo $checkedS; ?> >Kawin
	  <input type="radio" id="rbBelumKawinLama" name="rbStatusKawinLama" value="B" <?php echo $checkedB; ?> >Belum Kawin
	  </td>	  
	</tr>
	
	<tr>
	  <td class="lbl">Nama Kontak</td>
	  <td><input id="txtNamaKontak" name="txtNamaKontak" type="text" size="25" value= "<?php echo $datalama['namakontakkeluarga']; ?>" ></td>
	</tr>
	<tr>
	  <td class="lbl">Hubungan Keluarga</td>
	  <td><input id="txtHubunganKeluarga" name="txtHubunganKeluarga" type="text" size="25" value= "<?php echo $datalama['hubungankeluarga']; ?>" ></td>
	</tr>
	<tr>
	  <td class="lbl">No. Telp</td>
	  <td><input id="txtNoTelpKontakKeluarga" name="txtNoTelpKontakKeluarga" type="text" size="25" value= "<?php echo $datalama['notelpkontakkeluarga']; ?>" ></td>
	</tr>
	<tr>
	  <td class="lbl">No. HP</td>
	  <td><input id="txtNoHPKontakKeluarga" name="txtNoHPKontakKeluarga" type="text" size="25" value= "<?php echo $datalama['nohpkontakkeluarga']; ?>" ></td>
	</tr>
	
	</tbody>
</table>



<iframe id="iFrameRiwayatPendidikan" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/RiwayatPendidikan/index/<?php echo $datalama['npk']; ?>/approve" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>

<iframe id="iFrameKeluargaInti" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/Keluarga/KeluargaInti/<?php echo $datalama['npk']; ?>/user" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
		</div>
		
		<!------------------pemisah data lama dan baru---------------------->
		
		<div id="DataBaru">
<fieldset><legend>Data Baru</legend>
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2">Data Pribadi <?php //echo $this->config->base_url(); ?></td>
	  <input id="txtJenisProses" name="txtJenisProses" type="text" hidden value="edit">
	  </td>
	</tr>
	<tr>
	  <td class="lbl">Nama Lengkap *</td>
	  <td><?php echo $datalama['nama']; ?></td>	  
	</tr>	
	<tr>
	  <td class="lbl">Alamat Tinggal</td>
	  <td><textarea id="txtAlamatTinggal" name='txtAlamatTinggal' type="text" cols="60" rows="3" ><?php echo $databaru['alamattinggal']; ?></textarea></td>
	  
	</tr>
	<tr>
	  <td class="lbl">No. Telp</td>
	  <td><input id="txtNoTelp" name="txtNoTelp" type="text" size="20" value="<?php echo $databaru['notelp']; ?>" ></td>
	  
	</tr>
	<tr>
	  <td class="lbl">No. HP</td>
	  <td><input id="txtNoHP" name="txtNoHP" type="text" size="20" value="<?php echo $databaru['nohp']; ?>" ></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Status Kawin *</td>
	  <td>
	  <?php 
		$checkedB = "";
		$checkedS = "";
		if($databaru['statuskawin'] == "B"){
			$checkedB = "checked";
		}else{
			$checkedS = "checked";
		}
	  ?>
	  <input type="radio" id="rbKawin" name="rbStatusKawin" value="S" <?php echo $checkedS; ?> >Kawin
	  <input type="radio" id="rbBelumKawin" name="rbStatusKawin" value="B" <?php echo $checkedB; ?> >Belum Kawin
	  </td>	  
	</tr>
	
	<tr>
	  <td class="lbl">Nama Kontak</td>
	  <td><input id="txtNamaKontak" name="txtNamaKontak" type="text" size="25" value= "<?php echo $databaru['namakontakkeluarga']; ?>" ></td>
	</tr>
	<tr>
	  <td class="lbl">Hubungan Keluarga</td>
	  <td><input id="txtHubunganKeluarga" name="txtHubunganKeluarga" type="text" size="25" value= "<?php echo $databaru['hubungankeluarga']; ?>" ></td>
	</tr>
	<tr>
	  <td class="lbl">No. Telp</td>
	  <td><input id="txtNoTelpKontakKeluarga" name="txtNoTelpKontakKeluarga" type="text" size="25" value= "<?php echo $databaru['notelpkontakkeluarga']; ?>" ></td>
	</tr>
	<tr>
	  <td class="lbl">No. HP</td>
	  <td><input id="txtNoHPKontakKeluarga" name="txtNoHPKontakKeluarga" type="text" size="25" value= "<?php echo $databaru['nohpkontakkeluarga']; ?>" ></td>
	</tr>
	
	</tbody>
</table>



<iframe id="iFrameTempRiwayatPendidikan" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/TempRiwayatPendidikan/index/<?php echo $databaru['kodetempmstruser']; ?>/approval" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>

<iframe id="iFrameTempKeluargaInti" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/TempKeluarga/KeluargaInti/<?php echo $databaru['kodetempmstruser']; ?>/approval" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>

		</fieldset>
		</div>
		<input id="btnBatal" class="buttonSubmit" type="button" value="Decline" onClick="btnDecline_click()">
		
		<input id="btnBatal" class="buttonSubmit" type="button" value="Approve" onClick="btnApprove_click()">
		
	</body>
</html>