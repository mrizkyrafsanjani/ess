<html>
	<!--
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/myfunction.js"></script>
	-->	
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-ui.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function lookUpUsername(){
			name = document.getElementById('txtNPK').value;
			if(name !== ''){
				$.post( 
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ajax_lookUpNPK',
					 { NPK: name },
					 function(response) {  
						if (response == 0) {					   
						   alert('Sudah ada NPK '+ name +', mohon diganti dengan NPK lain.');
						   $('#txtNPK').focus();
						}
					 }
				);
			}
		}
		
		function btnEdit_click(){
			if(confirm('Apakah Anda yakin ingin mengedit data ini ke dalam database?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/SubmitEditDataPribadiUser/<?php echo $npk; ?>',
					$('#frmEditUser').serialize(),
					function(response){
						alert(response);
						location.reload();
					}
				);
			}
		}
		
		function btnBatal_click(){
			if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ajax_batalEdit',
					{},
					function(response){
						alert(response);
						location.reload();
					}
				);
			}
		}
		
		function alertsizeRwyPendidikan(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatPendidikan').style.height=pixels+"px";
		}
		
		function alertsizeRwyTraining(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatTraining').style.height=pixels+"px";
		}
		
		function alertsizeRwyPekerjaan(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatPekerjaan').style.height=pixels+"px";
		}
		
		function alertsizeRwyRotasi(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatRotasi').style.height=pixels+"px";
		}
		
		function alertsizeKeluargaBesar(pixels){
			pixels+=32;
			document.getElementById('iFrameKeluargaBesar').style.height=pixels+"px";
		}
		
		function alertsizeKeluargaInti(pixels){
			pixels+=32;
			document.getElementById('iFrameKeluargaInti').style.height=pixels+"px";
		}

		$(function() {
			$( "#dialogKTP" ).dialog({
			  autoOpen: false			  
			});
			$( "#dialogNPWP" ).dialog({
			  autoOpen: false			  
			});
			$( "#dialogManulife" ).dialog({
			  autoOpen: false			  
			});
		 
			$( "#popupManulife" ).click(function() {
			  $( "#dialogManulife" ).dialog( "open" );
			});
			
			$( "#dialogJamsostek" ).dialog({
			  autoOpen: false			  
			});
			
			$( "#popupKTP" ).click(function() {
			  $( "#dialogKTP" ).dialog( "open" );
			});
			$( "#popupNPWP" ).click(function() {
			  $( "#dialogNPWP" ).dialog( "open" );
			});
			$( "#popupJamsostek" ).click(function() {
			  $( "#dialogJamsostek" ).dialog( "open" );
			});
			
			$( "#dialogGardaMedika" ).dialog({
			  autoOpen: false			  
			});
		 
			$( "#popupGardaMedika" ).click(function() {
			  $( "#dialogGardaMedika" ).dialog( "open" );
			});
			
			$("#btnSaveTelp").click(function(){
				$.post( 
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ajax_SaveLangsung',
					 { NPK: '<?php echo $npk; ?>', AlamatTinggal: $("#txtAlamatTinggal").val(), NoTelp: $("#txtNoTelp").val(),NoHP:$("#txtNoHP").val() },
					 function(response) {
						alert(response);
					 }
				);
			});
		  });
		
		$(document).ready(function(){
			<?php if($message != ""){ ?>
			alert('<?php echo $message ?>');
			<?php } ?>
			<?php if($this->session->flashdata('msg')){ ?>
			alert('<?php echo $this->session->flashdata('msg'); ?>');
			<?php } ?>
		});
	</script>
	<head>
		<title>Data Pribadi Karyawan</title>
		<!--site css -->
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}	

		</style>
	</head>
	<body>		
		<?php echo validation_errors(); ?>
		<div style="width:100%;text-align:right;display:block;"><a href="#" onclick="window.open('<?php echo $this->config->base_url(); ?>assets/images/wf/DataPribadiWorkFlow.jpg','', 'width=600, height=400')" >Proses Flow Pada Data Pribadi</a></div>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Data Pribadi</h3>
		</div>
		<div class="panel-body">
		<form id="frmEditUser" action="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/SubmitEditDataPribadiUser/<?php echo $npk; ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2"> <?php //echo $this->config->base_url(); ?></td>
	  <input id="txtJenisProses" name="txtJenisProses" type="text" hidden value="edit">
	  </td>
	</tr>
	<tr>
	  <td align="center"><img alt="Foto Karyawan" src="<?php echo $this->config->base_url() . $urlfoto; ?>" width="150px" height="200px"></td>
	  <td></td>	  
	</tr>
	<tr>
	  <td class="lbl">Nama Lengkap *</td>
	  <td><?php echo $nama; ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">Tempat Lahir *</td>
	  <td><?php echo $tempatlahir; ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">Tanggal Lahir *</td>
	  <td><?php $date = new DateTime($tanggallahir); echo $date->format('d F Y'); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">Jenis Kelamin *</td>
	  <td>
	  <?php 
		$checked11 = "";
		$checked22 = "";
		if($jeniskelamin == "L"){
			$checked11 = "checked";
			echo "Laki-laki";
		}else{
			$checked22 = "checked";
			echo "Perempuan";
		}
	  ?>	  
	  </td>	  
	</tr>
	<tr>
	  <td class="lbl">Alamat KTP *</td>
	  <td><?php echo nl2br($alamatktp); ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Alamat Tinggal</td>
	  <td><textarea id="txtAlamatTinggal" class="form-control" name='txtAlamatTinggal' type="text" cols="60" rows="3" ><?php echo $alamattinggal; ?></textarea></td>
	  
	</tr>
	<tr>
	  <td class="lbl">No. Telp</td>
	  <td><input id="txtNoTelp" class="form-control" name="txtNoTelp" type="text" size="20" value="<?php echo $notelp; ?>" ></td>
	  
	</tr>
	<tr>		
	  <td class="lbl">No. HP</td>
	  <td>
		<div style="float:left;"> 
			<input id="txtNoHP" class="form-control" name="txtNoHP" type="text" size="20" value="<?php echo $nohp; ?>" >
		</div>
		<div style="position: relative;right: -10px;float:left;" >
			<button class="btn btn-primary" type="button" id="btnSaveTelp" 
				data-toggle="tooltip" data-placement="right" title="Simpan langsung data alamat, no hp, dan nomor telepon" 
				data-original-title="Simpan langsung data alamat, no hp, dan nomor telepon">Simpan</button>			
		</div>
	  </td>
	  
	</tr>
	<tr>
		<td></td>
		<td>Note: Tombol Simpan ini berguna untuk menyimpan langsung data alamat, no hp, dan nomor telepon. Jika ingin menyimpan data selain data tersebut, silahkan tekan tombol Simpan yang ada di bagian paling bawah.</td>
	</tr>
	<tr>
	  <td class="lbl">Status Kawin *</td>
	  <td>
	  <?php 
		$checkedB = "";
		$checkedS = "";
		if($statuskawin == "B"){
			$checkedB = "checked";
		}else{
			$checkedS = "checked";
		}
	  ?>
	  <input type="radio" id="rbKawin" name="rbStatusKawin" value="S" <?php echo $checkedS; ?> >Kawin
	  <input type="radio" id="rbBelumKawin" name="rbStatusKawin" value="B" <?php echo $checkedB; ?> >Belum Kawin
	  </td>	  
	</tr>
	<tr>
	  <td class="lbl">KTP</td>
	  <td>
	  <?php echo $noktp;  if($urlktp != ''){ ?> 
		<div id="popupKTP" class="btn btn-link">Scan KTP</div> 
		<div id="dialogKTP" title="KTP">
		  <p><img alt="Scan KTP" src="<?php echo $this->config->base_url().$urlktp ?>"></p>
		</div>
		<?php } ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">NPWP</td>
	  <td><?php echo $npwp; if($urlnpwp != ''){ ?>   
		<div id="popupNPWP" class="btn btn-link">Scan NPWP</div>
		<div id="dialogNPWP" title="NPWP">
		  <p><img alt="Scan NPWP" src="<?php echo $this->config->base_url().$urlnpwp ?>"></p>
		</div>		
		<?php } ?> </td>
	</tr>
	</tbody>
</table>

<fieldset>
	<legend>Data Kepegawaian</legend>
	<table border=0>
		<tbody>
		<tr>
		  <td class="lbl">DPA *</td>
		  <td>
		  <?php 
			$checkedDPA1 = "";
			$checkedDPA2 = "";
			if($dpa == 1){
				$checkedDPA1 = "checked";
				echo "Satu";
			}else{
				$checkedDPA2 = "checked";
				echo "Dua";
			}
		  ?> 		 
		  </td>	  
		</tr>
		<tr>
		  <td class="lbl">NPK *</td>
		  <td><?php echo $npk; ?></td>
		</tr>
		<!--
		<tr>
		  <td class="lbl">Departemen *</td>
		  <td>
			<?php 
			  
			  for($i=0;$i<count($dataDepartemen);$i++)
			  {
				$selectedstr = "";
				if($departemen == $dataDepartemen[$i]['KodeDepartemen'])
				{
					$selectedstr = "selected";
					echo $dataDepartemen[$i]['NamaDepartemen'];
				}				
			  }
			  ?>
		  </td>
		</tr>
		<tr>
		  <td class="lbl">Jabatan *</td>
		  <td><?php echo $jabatan; ?></td>
		</tr>
		<tr>
		  <td class="lbl">Golongan *</td>
		  <td>
			<?php 			  
			  for($i=0;$i<count($dataGolongan);$i++)
			  {
				$selectedstr = "";
				if($golongan == $dataGolongan[$i]['Golongan']){ $selectedstr = "selected"; echo $dataGolongan[$i]['Golongan']; }
				
			  }
			  ?>
		  </td>
		</tr>
		-->
		<tr>
		  <td class="lbl">Tanggal Bekerja *</td>
		  <td><?php $date = new DateTime($tanggalbekerja); echo $date->format('d F Y'); ?></td>	  
		</tr>
		<!--<tr>
		  <td class="lbl">Tanggal Berhenti</td>
		  <td></td>
		</tr>
		<tr>
		  <td class="lbl">Keterangan Berhenti</td>
		  <td><input id="txtKeteranganBerhenti" name="txtKeteranganBerhenti" type="text" size="25" ></td>
		</tr>	-->
		<tr>
		  <td class="lbl">Status Karyawan *</td>
		  <td>
			<?php
				echo $statuskaryawan;
			?>
		    
		  </td>
		</tr>
		<tr>
		  <td class="lbl">No. Ext.</td>
		  <td><?php echo $noext; ?></td>
		</tr>
		<tr>
		  <td class="lbl">Email Internal</td>
		  <td><?php echo $emailinternal; ?></td>
		</tr>
		<tr>
		  <td class="lbl">Manulife</td>
		  <td><?php echo $iddanapensiun; ?><div id="popupManulife" class="btn btn-link">Scan Manulife</div></td>
		</tr>
		<tr>
		  <td class="lbl">Jamsostek</td>
		  <td><?php echo $idjamsostek; ?><div class="btn btn-link" id="popupJamsostek">Scan Jamsostek</div></td>
		</tr>
		<tr>
		  <td class="lbl">Garda Medika</td>
		  <td><?php echo $idgardamedika; ?><div class="btn btn-link" id="popupGardaMedika">Scan Garda Medika</div></td>
		</tr>
		</tbody>
	</table>
	<div id="dialogManulife" title="Manulife">
	  <p><img alt="Scan Manulife" src="<?php echo $this->config->base_url().$urldanapensiun; ?>"></p>
	</div>
	<div id="dialogJamsostek" title="Jamsostek">
	  <p><img alt="Scan Jamsostek" src="<?php echo $this->config->base_url().$urljamsostek; ?>"></p>
	</div>
	<div id="dialogGardaMedika" title="Garda Medika">
	  <p><img alt="Scan Garda Medika" src="<?php echo $this->config->base_url().$urlgardamedika; ?>"></p>
	</div>
</fieldset>

<fieldset>
	<legend>Data Rekening</legend>
	<table border=0>
		<tbody>
		<tr>
		  <td class="lbl">No. Rekening *</td>
		  <td><?php echo $norek; ?></td>
		</tr>
		<tr>
		  <td class="lbl">Atas Nama *</td>
		  <td><?php echo $atasnama; ?></td>
		</tr>
		<tr>
		  <td class="lbl">Bank *</td>
		  <td><?php echo $namabank; ?></td>
		</tr>
		<tr>
		  <td class="lbl">Cabang *</td>
		  <td><?php echo $cabang; ?></td>
		</tr>
		</tbody>
	</table>
</fieldset>

<fieldset>
	<legend>Data Kontak Keluarga</legend>
	<table border=0>
		<tbody>
			<tr>
			  <td class="lbl">Nama Kontak</td>
			  <td><input id="txtNamaKontak" class="form-control" name="txtNamaKontak" type="text" size="25" value= "<?php echo $namakontakkeluarga; ?>" ></td>
			</tr>
			<tr>
			  <td class="lbl">Hubungan Keluarga</td>
			  <td><input id="txtHubunganKeluarga" class="form-control" name="txtHubunganKeluarga" type="text" size="25" value= "<?php echo $hubungankeluarga; ?>" ></td>
			</tr>
			<tr>
			  <td class="lbl">No. Telp</td>
			  <td><input id="txtNoTelpKontakKeluarga" class="form-control" name="txtNoTelpKontakKeluarga" type="text" size="25" value= "<?php echo $notelpkontakkeluarga; ?>" ></td>
			</tr>
			<tr>
			  <td class="lbl">No. HP</td>
			  <td><input id="txtNoHPKontakKeluarga" class="form-control" name="txtNoHPKontakKeluarga" type="text" size="25" value= "<?php echo $nohpkontakkeluarga; ?>" ></td>
			</tr>
		</tbody>
	</table>
</fieldset>

<fieldset>
	<legend>Cuti & Medical</legend>
	<table border=0>
		<tbody>
			<!--<tr>
			  <td class="lbl">*Medical</td>
			  <td><?php
				echo "Rp. ".number_format($medical);
			  ?>			  
			  </td>
			</tr>-->
			<tr>
			  <td class="lbl">*Cuti Tahunan</td>
			  <td><?php echo $cutiTahunan; ?>
			  <!--<input id="txtCutiTahunan" class="form-control" name="txtCutiTahunan" type="text" size="25" value="<?php echo $cutiTahunan; ?>">-->
			  </td>
			</tr>
			<tr>
			  <td class="lbl">*Cuti Besar</td>
			  <td><?php echo $cutiBesar; ?>
			  <!--<input id="txtCutiBesar" class="form-control" name="txtCutiBesar" type="text" size="25" value="<?php echo $cutiBesar; ?>">-->
			  </td>
			</tr>
		</tbody>
	</table>
</fieldset>

<br/>

<fieldset>
<legend>Riwayat Pendidikan</legend>
<iframe id="iFrameRiwayatPendidikan" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/TempEditUserRiwayatPendidikan/index/<?php echo $npk; ?>" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>

<fieldset>
<legend>Riwayat Training</legend>
<iframe id="iFrameRiwayatTraining" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/RiwayatTraining/index/<?php echo $npk; ?>/user" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Riwayat Pekerjaan</legend>
<iframe id="iFrameRiwayatPekerjaan" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/RiwayatPekerjaan/index/<?php echo $npk; ?>/user" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Riwayat Mutasi/Rotasi/Demosi/Promosi Pekerjaan DPA</legend>
<iframe id="iFrameRiwayatRotasi" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/RiwayatRotasiPekerjaan/index/<?php echo $npk; ?>/user" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Keluarga Besar</legend>
<iframe id="iFrameKeluargaBesar" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/Keluarga/KeluargaBesar/<?php echo $npk; ?>/user" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Keluarga Inti</legend>
<iframe id="iFrameKeluargaInti" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/TempEditUserKeluarga/KeluargaInti/<?php echo $npk; ?>" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<div id="note">
</div>
<table class="tblPureNoBorder" style="float:right;">
	<tr>
		<td>
		<br/>
		<input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin mengedit data ini ke dalam database?')" id="btnSubmit" name="submitForm" value="Simpan">
		
		<!--<input class="buttonSubmit" type="button" onClick="btnEdit_click()" id="btnSubmit" name="submitForm" value="Edit">
		-->
		<input id="btnBatal" class="btn" type="button" value="Batal" onClick="btnBatal_click()">
		<div id="divError"></div>
		</td>
	</tr>
</table>
			<br/>
		</form>
		</div>
		</div>
	</body>
</html>