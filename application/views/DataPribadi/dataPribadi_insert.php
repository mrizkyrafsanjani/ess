<html>
	<!--
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/myfunction.js"></script>
	-->
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-ui.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	
		function lookUpUsername(){
			name = document.getElementById('txtNPK').value;
			if(name !== ''){
				$.post( 
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ajax_lookUpNPK',
					 { NPK: name },
					 function(response) {  
						if (response == 0) {					   
						   alert('Sudah ada NPK '+ name +', mohon diganti dengan NPK lain.');
						   $('#txtNPK').focus();
						}
					 }
				);
			}
		}
		
		function btnBatal_click(){
			if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/ajax_batalSimpan',
					{},
					function(response){
						alert(response);
						location.reload();
					}
				);
			}
		}
		
		function alertsizeRwyPendidikan(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatPendidikan').style.height=pixels+"px";
		}
		
		function alertsizeRwyTraining(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatTraining').style.height=pixels+"px";
		}
		
		function alertsizeRwyPekerjaan(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatPekerjaan').style.height=pixels+"px";
		}
		
		function alertsizeRwyRotasi(pixels){
			pixels+=32;
			document.getElementById('iFrameRiwayatRotasi').style.height=pixels+"px";
		}
		
		function alertsizeKeluargaBesar(pixels){
			pixels+=32;
			document.getElementById('iFrameKeluargaBesar').style.height=pixels+"px";
		}
		
		function alertsizeKeluargaInti(pixels){
			pixels+=32;
			document.getElementById('iFrameKeluargaInti').style.height=pixels+"px";
		}
	</script>
	<head>
		<title>Data Pribadi Karyawan</title>
		<!--site css -->
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<!--<?php echo validation_errors(); ?>-->
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Data Pribadi</h3>
		</div>
		<div class="panel-body">
		<form action="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/TambahDataPribadi" method="post" accept-charset="utf-8" enctype="multipart/form-data">
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2"> <?php //echo $this->config->base_url(); ?></td>
	  <input id="txtJenisProses" name="txtJenisProses" type="text" hidden value="input">
	  </td>
	</tr>
	<tr>
	  <td class="lbl">
		<!--<img alt="Foto Karyawan" src="<?php echo $this->config->base_url(); ?>assets\uploads\files\fotoDPA.jpg" width="150px" height="200px">
		-->
		Upload Foto
	  </td>
	  <td><input id="inputFileFoto" name="inputFileFoto" type="file"></td>	  
	</tr>
	<tr>
	  <td class="lbl">Nama Lengkap *</td>
	  <td><input id="txtNamaLengkap" class="form-control" name="txtNamaLengkap" type="text" size="40%" value="<?php echo set_value('txtNamaLengkap'); ?>"><?php echo form_error('txtNamaLengkap'); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">Tempat Lahir *</td>
	  <td><input id="txtTempatLahir" class="form-control" name="txtTempatLahir" type="text" size ="40%" value="<?php echo set_value('txtTempatLahir'); ?>" ><?php echo form_error('txtTempatLahir'); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">Tanggal Lahir *</td>
	  <td><input class="form-control" id="txtTanggalLahir" name='txtTanggalLahir' type="date" size="15" value="<?php echo set_value('txtTanggalLahir'); ?>" ><?php echo form_error('txtTanggalLahir'); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">Jenis Kelamin *</td>
	  <td>
	  <input type="radio" id="rbLaki" name="rbJenisKelamin" value="L" <?php echo set_radio('rbJenisKelamin', 'L', TRUE); ?>>Laki-laki
	  <input type="radio" id="rbPerempuan" name="rbJenisKelamin" value="P" <?php echo set_radio('rbJenisKelamin', 'P'); ?>>Perempuan	  
	  <?php echo form_error('rbJenisKelamin'); ?></td>
	</tr>
	<tr>
	  <td class="lbl">Alamat KTP *</td>
	  <td><textarea id="txtAlamatKTP" class="form-control" name='txtAlamatKTP' type="text" cols="60" rows="3" ><?php echo set_value('txtAlamatKTP'); ?></textarea><?php echo form_error('txtAlamatKTP'); ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Alamat Tinggal</td>
	  <td><textarea id="txtAlamatTinggal" class="form-control"  name='txtAlamatTinggal' type="text" cols="60" rows="3" ><?php echo set_value('txtAlamatTinggal'); ?></textarea></td>
	  
	</tr>
	<tr>
	  <td class="lbl">No. Telp</td>
	  <td><input id="txtNoTelp" class="form-control"  name="txtNoTelp" type="tel" size="20" value="<?php echo set_value('txtNoTelp'); ?>"></td>
	  
	</tr>
	<tr>
	  <td class="lbl">No. HP</td>
	  <td><input id="txtNoHP" class="form-control"  name="txtNoHP" type="tel" size="20" value="<?php echo set_value('txtNoHP'); ?>"></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Status Kawin *</td>
	  <td>
	  <input type="radio" id="rbKawin" name="rbStatusKawin" value="S" <?php echo set_radio('rbStatusKawin', 'S'); ?>>Kawin
	  <input type="radio" id="rbBelumKawin" name="rbStatusKawin" value="B" <?php echo set_radio('rbStatusKawin', 'B'); ?>>Belum Kawin
	  <?php echo form_error('rbStatusKawin'); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">KTP</td>
	  <td>
		  <div class="col-md-6"><input id="txtNoKTP" class="form-control"  name="txtNoKTP" type="text" size="25" value="<?php echo set_value('txtNoKTP'); ?>"> </div>
		  <!--<a href="#">Scan KTP</a> -->
		  <div class="col-md-6"><input id="inputFileKTP" class="form-control" name="inputFileKTP" type="file"> </div>
	  </td>	  
	</tr>
	<tr>
	  <td class="lbl">NPWP</td>
	  <td>
		  <div class="col-md-6"><input id="txtNoNPWP" class="form-control"  name="txtNoNPWP" type="text" size="25" value="<?php echo set_value('txtNoNPWP'); ?>"> </div>
		  <!--<a href="#">Scan NPWP</a>-->
		  <div class="col-md-6"><input id="inputFileNPWP" class="form-control" name="inputFileNPWP" type="file"> </div>
	  </td>
	</tr>
	</tbody>
</table>

<fieldset>
	<legend>Data Kepegawaian</legend>
	<table border=0>
		<tbody>
		<tr>
		  <td class="lbl">DPA *</td>
		  <td>
		  <input type="radio" id="rbDPA1" name="rbDPA" value="1" <?php echo set_radio('rbDPA', '1'); ?>>Satu
		  <input type="radio" id="rbDPA2" name="rbDPA" value="2" <?php echo set_radio('rbDPA', '2'); ?>>Dua
		  <?php echo form_error('rbDPA'); ?></td>	  
		</tr>
		<tr>
		  <td class="lbl">NPK *</td>
		  <td><input id="txtNPK" class="form-control"  name="txtNPK" type="text" size="25" maxlength="6" onblur="lookUpUsername()" value="<?php echo set_value('txtNPK'); ?>"><?php echo form_error('txtNPK'); ?></td>
		</tr>
		<!--
		<tr>
		  <td class="lbl">Departemen *</td>
		  <td>
			<select class="form-control" name="cmbDepartemen">
			  <option value=''></option>
			<?php 
			  
			  for($i=0;$i<count($dataDepartemen);$i++)
			  {
				echo "<option value='".$dataDepartemen[$i]['KodeDepartemen']."'>".$dataDepartemen[$i]['NamaDepartemen']."</option>";				
			  }
			  ?>
			</select>
		  </td>
		</tr>
		<tr>
		  <td class="lbl">Jabatan *</td>
		  <td><input id="txtJabatan" class="form-control"  name="txtJabatan" type="text" size="25" ></td>
		</tr>
		<tr>
		  <td class="lbl">Golongan *</td>
		  <td>
			<select class="form-control" name="cmbGolongan">
			  <option value=''></option>
			<?php 			  
			  for($i=0;$i<count($dataGolongan);$i++)
			  {
				echo "<option value='".$dataGolongan[$i]['Golongan']."'>".$dataGolongan[$i]['Golongan']."</option>";				
			  }
			  ?>
			</select>
		  </td>
		</tr>
		-->
		<tr>
		  <td class="lbl">Tanggal Bekerja *</td>
		  <td><input  class="form-control"  id="txtTanggalBekerja" name='txtTanggalBekerja' type="date" size="15" value="<?php echo set_value('txtTanggalBekerja'); ?>"><?php echo form_error('txtTanggalBekerja'); ?></td>
		</tr>
		<!--<tr>
		  <td class="lbl">Tanggal Berhenti</td>
		  <td></td>
		</tr>
		<tr>
		  <td class="lbl">Keterangan Berhenti</td>
		  <td><input id="txtKeteranganBerhenti" name="txtKeteranganBerhenti" type="text" size="25" ></td>
		</tr>	-->
		<tr>
		  <td class="lbl">Status Karyawan *</td>
		  <td>
		    <select  class="form-control"  name="cmbStatusKaryawan">
				<option value='' <?php echo set_select('cmbStatusKaryawan', '', TRUE); ?>></option>
				<option value='Tetap' <?php echo set_select('cmbStatusKaryawan', 'Tetap'); ?>>Tetap</option>
				<option value='PKWT' <?php echo set_select('cmbStatusKaryawan', 'PKWT'); ?>>PKWT</option>
				<option value='Outsource' <?php echo set_select('cmbStatusKaryawan', 'Outsource'); ?>>Outsource</option>
				<option value='Assignment AI' <?php echo set_select('cmbStatusKaryawan', 'Assignment AI'); ?>>Assignment AI</option>
			</select>
		  <?php echo form_error('cmbStatusKaryawan'); ?></td>
		</tr>
		<tr>
		  <td class="lbl">No. Ext.</td>
		  <td><input id="txtNoExt" class="form-control" name="txtNoExt" type="text" size="25" value="<?php echo set_value('txtNoExt'); ?>"></td>
		</tr>
		<tr>
		  <td class="lbl">Email Internal</td>
		  <td><input id="txtEmailInternal" class="form-control" name="txtEmailInternal" type="email" size="25" value="<?php echo set_value('txtEmailInternal'); ?>"></td>
		</tr>
		<tr>
		  <td class="lbl">Manulife</td>
		  <td>
			  <div class="col-md-6"><input id="txtIDManulife" class="form-control" name="txtIDManulife" type="text" size="25" value="<?php echo set_value('txtIDManulife'); ?>"></div>
			  <!--<a href="#">Scan Manulife</a>-->
			  <div class="col-md-6"><input id="inputFileManulife" class="form-control" name="inputFileManulife" type="file"></div>
		  </td>
		</tr>
		<tr>
		  <td class="lbl">Jamsostek</td>
		  <td>
			  <div class="col-md-6"><input id="txtIDJamsostek" class="form-control" name="txtIDJamsostek" type="text" size="25" value="<?php echo set_value('txtIDJamsostek'); ?>"></div>
			  <!--<a href="#">Scan Jamsostek</a>-->
			  <div class="col-md-6"><input id="inputFileJamsostek" class="form-control" name="inputFileJamsostek" type="file"></div>
		  </td>
		</tr>
		<tr>
		  <td class="lbl">Garda Medika</td>
		  <td>
			  <div class="col-md-6"><input id="txtIDGardaMedika" class="form-control" name="txtIDGardaMedika" type="text" size="25" value="<?php echo set_value('txtIDGardaMedika'); ?>"></div>
			  <!--<a href="#">Scan Garda Medika</a>-->
			  <div class="col-md-6"><input id="inputFileGardaMedika" class="form-control" name="inputFileGardaMedika" type="file"></div>
		  </td>
		</tr>
		</tbody>
	</table>
</fieldset>

<fieldset>
	<legend>Data Rekening</legend>
	<table border=0>
		<tbody>
		<tr>
		  <td class="lbl">No. Rekening *</td>
		  <td><input id="txtNoRekening" class="form-control" name="txtNoRekening" type="text" size="25" value="<?php echo set_value('txtNoRekening'); ?>"><?php echo form_error('txtNoRekening'); ?></td>
		</tr>
		<tr>
		  <td class="lbl">Atas Nama *</td>
		  <td><input id="txtAtasNama" class="form-control" name="txtAtasNama" type="text" size="25" value="<?php echo set_value('txtAtasNama'); ?>"><?php echo form_error('txtAtasNama'); ?></td>
		</tr>
		<tr>
		  <td class="lbl">Bank *</td>
		  <td><input id="txtBank" class="form-control" name="txtBank" type="text" size="25" value="<?php echo set_value('txtBank'); ?>"><?php echo form_error('txtBank'); ?></td>
		</tr>
		<tr>
		  <td class="lbl">Cabang *</td>
		  <td><input id="txtCabang" class="form-control" name="txtCabang" type="text" size="25" value="<?php echo set_value('txtCabang'); ?>"><?php echo form_error('txtCabang'); ?></td>
		</tr>
		</tbody>
	</table>
</fieldset>

<fieldset>
	<legend>Data Kontak Keluarga</legend>
	<table border=0>
		<tbody>
			<tr>
			  <td class="lbl">Nama Kontak</td>
			  <td><input id="txtNamaKontak" class="form-control" name="txtNamaKontak" type="text" size="25" value="<?php echo set_value('txtNamaKontak'); ?>"></td>
			</tr>
			<tr>
			  <td class="lbl">Hubungan Keluarga</td>
			  <td><input id="txtHubunganKeluarga" class="form-control" name="txtHubunganKeluarga" type="text" size="25" value="<?php echo set_value('txtHubunganKeluarga'); ?>"></td>
			</tr>
			<tr>
			  <td class="lbl">No. Telp</td>
			  <td><input id="txtNoTelpKontakKeluarga" class="form-control" name="txtNoTelpKontakKeluarga" type="tel" size="25" value="<?php echo set_value('txtNoTelpKontakKeluarga'); ?>"></td>
			</tr>
			<tr>
			  <td class="lbl">No. HP</td>
			  <td><input id="txtNoHPKontakKeluarga" class="form-control" name="txtNoHPKontakKeluarga" type="tel" size="25" value="<?php echo set_value('txtNoHPKontakKeluarga'); ?>"></td>
			</tr>
		</tbody>
	</table>
</fieldset>
<!--
<fieldset>
	<legend>Medical dan Cuti</legend>
	<table border=0>
		<tbody>
			<tr>
			  <td class="lbl">*Medical</td>
			  <td><input id="txtMedical" name="txtMedical" type="text" size="25" ></td>
			</tr>
			<tr>
			  <td class="lbl">*Cuti Tahunan</td>
			  <td><input id="txtCutiTahunan" name="txtCutiTahunan" type="text" size="25" ></td>
			</tr>
			<tr>
			  <td class="lbl">*Cuti Besar</td>
			  <td><input id="txtCutiBesar" name="txtCutiBesar" type="text" size="25" ></td>
			</tr>
		</tbody>
	</table>
</fieldset>
-->
<br/>

<fieldset>
<legend>Riwayat Pendidikan</legend>
<iframe id="iFrameRiwayatPendidikan" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/TempRiwayatPendidikan/addTempRiwayatPendidikan" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>

<fieldset>
<legend>Riwayat Training</legend>
<iframe id="iFrameRiwayatTraining" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/TempRiwayatTraining" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Riwayat Pekerjaan</legend>
<iframe id="iFrameRiwayatPekerjaan" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/TempRiwayatPekerjaan" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Riwayat Mutasi/Rotasi/Demosi/Promosi Pekerjaan DPA</legend>
<iframe id="iFrameRiwayatRotasi" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/TempRiwayatRotasiPekerjaan" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Keluarga Besar</legend>
<iframe id="iFrameKeluargaBesar" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/TempKeluarga/KeluargaBesar" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<fieldset>
<legend>Keluarga Inti</legend>
<iframe id="iFrameKeluargaInti" src="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/TempKeluarga/addKeluargaInti" width="100%" height="200px" seamless frameBorder="0">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<div id="note">
</div>
<table class="tblPureNoBorder" style="float:right;">
	<tr>
		<td>
		<br/><input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin menyimpan data ini ke dalam database?')" id="btnSubmit" name="submitForm" value="Simpan">
		
		<input id="btnBatal" class="btn" type="button" value="Batal" onClick="btnBatal_click()">
		<div id="divError"></div>
		</td>
	</tr>
</table>
			<br/>
		</form>
		</div>
		</div>
	</body>
</html>