<html>
	<style type="text/css">
	@import "../assets/css/jquery-ori.datepick.css";
	@import "../assets/css/cetak.css";
	</style>
	<!--<script type="text/javascript" src="../assets/js/jquery.js"></script>-->
	<script type="text/javascript" src="../assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="../assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="../assets/js/myfunction.js?v=2.17"></script>
	<script>
    	$(document).ready(function(){
			$(".example1").delay(4500).fadeOut('slow');	
		});
    </script>
    
	<head>
		<title>Laporan SPD</title>
	</head>
	<body>
  <?php echo validation_errors(); ?>
  
  <?php if ( $trkUangMuka_array[0]['TglTerimaHRGAPermohonan']=='' ) {?> 
    
	<form action="submitUpdateSPD" method="post" accept-charset="utf-8">
    
    <input id="txtMode" name='txtMode' type="hidden" size="30" value="edit">
    <h1>
    <center><span class="example1"><b><font color="#777777" face="Segoe UI, Helvetica, Arial, Sans-Serif">
	<?php  
		if(isset($msg)){
			echo "$msg" ;			
		}
	?></font></b></span>
    </center>
    </h1>
    </br>
	<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr  class="tblHeader">
	  <td colspan="2">Laporan Perjalanan Dinas</td>
          <td colspan="3"><image id="logoDPA" src='../assets/images/logoDPA.png'>*DANA PENSIUN ASTRA<br/>
		  <?php 
			$checked1 = "";
			$checked2 = "";
			if($trkSPD_array[0]['DPA'] == 1){
				$checked1 = "checked";
			}else{
				$checked2 = "checked";
			}
		  ?>
          <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked1; ?> >SATU
          <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked2; ?> >DUA
		  
		  <input class="clsUangMuka" type="radio" id="rbYa" name="rbUangMuka" value="1" checked hidden>
          </td>
	</tr>
	<tr>
	  <td class="lbl">No SPD</td>
	  <td><input id="txtNoSPD" name='txtNoSPD' type="text" size="30" value="<?php echo $trkSPD_array[0]['nospd']; ?>" readonly="true"></td>
	  
	  <td colspan="3" rowspan="10" align="center">
		<div id="catatan">
			<div class="lblTop">Catatan Penting</div>
				<div id="txtCatatan"><ol><li>Pertanggungjawaban atas uang muka untuk biaya perjalanan dinas wajib dilaporkan selambat-lambatnya 3 (tiga) hari setelah tiba di tempat kerja.</li>
<li>Pengembalian kelebihan ataupun kekurangan uang muka diterima/diserahkan 2 hari kerja setelah dokumen lengkap dan disetujui oleh HRD.</li>
<li>Semua bukti pengeluaran harus dilampirkan:</li>
</ol>
				<ul>
					<li>-Invoice Biaya penginapan (Hotel) Asli</li>
					<li>-Airport tax</li>
					<li>-Kwitansi deklarasi Taksi</li>
					<li>-dll (Berkas yang terkait)</li>
				</ul>
				</div>
		</div>
      </td>
	</tr>
	<tr>
	  <td class="lbl">Tanggal Laporan</td>
	  <td><?php echo date("j F Y"); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">NPK</td>
	  <td><?php echo $trkSPD_array[0]['npk']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Golongan</td>
	  <td><?php echo $trkSPD_array[0]['golongan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Nama</td>
	  <td><?php echo $trkSPD_array[0]['nama']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Jabatan</td>
	  <td><?php echo $trkSPD_array[0]['jabatan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Departemen</td>
	  <td><?php echo $trkSPD_array[0]['departemen']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Atasan</td>
	  <td><?php echo $trkSPD_array[0]['atasan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">*Tanggal Keberangkatan</td>
	  <td><input id="txtTanggalBerangkat" name='txtTanggalBerangkat' type="text" size="15" value="<?php echo $trkSPD_array[0]['tanggalberangkatspd']; ?>" ></td>
	  
	</tr>
	<tr>
	  <td class="lbl">*Tanggal Kembali</td>
	  <td>
    <input id="txtTanggalKembali" name='txtTanggalKembali' type="text" size="15" value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>" >
    <input type="hidden" id="txtTanggalKembaliKantor" name='txtTanggalKembaliKantor' type="text" size="15" value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>" >
    <input type="hidden" id="txtTanggalKembaliPermohonan" name='txtTanggalKembaliKantor' type="text" size="15" value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>" >
    </td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tujuan</td>
	  <td colspan="4"><input id="txtTujuan" name='txtTujuan' type="text" size="15" value="<?php echo $trkSPD_array[0]['tujuan']; ?>"></td>
	</tr>
	<tr>
	  <td class="lbl">Alasan Perjalanan</td>
	  <td colspan="4" valign="top"><textarea id="txtAlasan" name='txtAlasan' type="text" cols="60" rows="3" ><?php echo $trkSPD_array[0]['alasan']; ?></textarea></td>
	</tr>
  </tbody>
</table>
<br/>
<!-- part untuk perkiraaan biaya -->
<table border=0 >
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Perkiraan Biaya
	  <input class="clsUangMuka" type="radio" id="rbInternasional" name="rbRegion" value="1">Internasional
		<input class="clsUangMuka" type="radio" id="rbNasional" name="rbRegion" value="0">Nasional
	  </td>
    </tr>
    <tr>
      <td class="lbl" width="170px">Jenis Biaya</td>
      <td id="keterangan" class="lblTop">Keterangan</td>
      <td class="lblTop">Beban Harian</td>
      <td class="lblTop">#Jml Hari</td>
      <td class="lblTop">Total Perkiraan Biaya</td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Saku</td>
      <td><input id="txtKetUangSaku" name='txtKetUangSaku' type="text" size="40" value="<?php echo $trkSPD_array[0]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianUangSaku" name='txtBebanHarianUangSaku' type="text" size="15" value="<?php 
		if($trkSPD_array[0]['uangmuka'] == 0){
			echo $dataUser['uangsaku'];
		}else{
			echo $trkSPD_array[0]['bebanharianspd']; 
		}
	?>"   readonly></td>
      <td class='clsHari'><input id="txtJmlhHariUangSaku" name='txtJmlhHariUangSaku' type="text" size="5" value="<?php echo $trkSPD_array[0]['jumlahharispd']; ?>" ></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalUangSaku" name='txtTotalUangSaku' type="text" size="15" value="<?php echo $trkSPD_array[0]['bebanharianspd']*$trkSPD_array[0]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Makan</td>
      <td><input id="txtKetUangMakan" name='txtKetUangMakan' type="text" size="40" value="<?php echo $trkSPD_array[1]['keteranganspd']; ?>" ></td>
	  <?php 
		$readonly = "";
		if($trkSPD_array[0]['golongan'] <= 5){
			$readonly = "readonly";
		}
	  ?>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianUangMakan" name='txtBebanHarianUangMakan' type="text" size="15" value="<?php 
		if($trkSPD_array[0]['uangmuka'] == 0){
			echo $dataUser['uangmakan'];
		}else{
			echo $trkSPD_array[1]['bebanharianspd']; 
		}		
	?>"  <?php echo $readonly; ?> ></td>
      <td class='clsHari'><input id="txtJmlhHariUangMakan" name='txtJmlhHariUangMakan' type="text" size="5" value="<?php echo $trkSPD_array[1]['jumlahharispd']; ?>" ></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalUangMakan" name='txtTotalUangMakan' type="text" size="15" value="<?php echo $trkSPD_array[1]['bebanharianspd']*$trkSPD_array[1]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;*Akomodasi</td>
      <td><input id="txtKetHotel" name='txtKetHotel' type="text" size="40" value="<?php echo $trkSPD_array[2]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianHotel" name='txtBebanHarianHotel' type="text" size="15" value="<?php echo $trkSPD_array[2]['bebanharianspd']; ?>" ></td>
      <td class='clsHari'><input id="txtJmlhHariHotelLap" name="txtJmlhHariHotelLap" type="text" size="5" value="<?php echo $trkSPD_array[2]['jumlahharispd']; ?>" ></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalHotelLap" type="text" size="15" value="<?php echo $trkSPD_array[2]['bebanharianspd']*$trkSPD_array[2]['jumlahharispd']; ?>" ></td>
    </tr>
	<tr class='clsInputNumber'>
      <td class="lbl"></td>
      <td><input id="txtKetHotel2" name="txtKetHotel2" type="text" size="40" value="" ></td>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianHotel2" name="txtBebanHarianHotel2" type="text" size="15" value="" ></td>
      <td class='clsHari'><input id="txtJmlhHariHotel2" name="txtJmlhHariHotel2" type="text" size="5" value="" ></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalHotel2" name="txtTotalHotel2" type="text" size="15" value="" ></td>
    </tr>
    <!--<tr>
      <td><br/></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>-->
    <tr>
      <td class="lbl"><i>Transportasi</i></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr class='clsInputNumber'>
      <td  class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Taksi</td>
      <td><input id="txtKetTaksi" name='txtKetTaksi' type="text" size="40" value="<?php echo $trkSPD_array[4]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianTaksi" name='txtBebanHarianTaksi' type="text" size="15" value="<?php echo $trkSPD_array[4]['bebanharianspd']; ?>" ></td>
      <td></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalTaksi" name='txtTotalTaksi' type="text" size="15" value="<?php echo $trkSPD_array[4]['bebanharianspd']*$trkSPD_array[4]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Airport Tax</td>
      <td><input id="txtKetAirport" name='txtKetAirport' type="text" size="40" value="<?php echo $trkSPD_array[5]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianAirport" name='txtBebanHarianAirport' type="text" size="15" value="<?php echo $trkSPD_array[5]['bebanharianspd']; ?>" ></td>
      <td></td></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalAirport" name='txtTotalAirport' type="text" size="15" value="<?php echo $trkSPD_array[5]['bebanharianspd']*$trkSPD_array[5]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl"><i>Lain-lain</i></td>
	  <?php 
		$counter = 1;
		for($i=6;$i<count($trkSPD_array);$i++){
			echo "<td><input id=\"txtKetLain".$counter."\" name='txtKetLain".$counter."' type=\"text\" size=\"40\" value='". $trkSPD_array[$i]['keteranganspd'] ."' ></td>
			  <td class='clsUang'><input class='clsBebanHarian' id=\"txtBebanHarianLain".$counter."\" name='txtBebanHarianLain".$counter."' type=\"text\" size=\"15\" value='". $trkSPD_array[$i]['bebanharianspd'] ."' ></td>
			  <td></td>
			  <td class='clsUang'><input class='clsTotal' id=\"txtTotalLain".$counter."\" name='txtTotalLain".$counter."' type=\"text\" size=\"15\" value='" . $trkSPD_array[$i]['bebanharianspd']*$trkSPD_array[$i]['jumlahharispd'] . "' ></td>
			</tr>
			<tr class='clsInputNumber'>
				<td></td>";
			$counter++;
		}
		$counter = 1;
		if(count($trkSPD_array)==6){
			for($i=1;$i<=3;$i++){
				echo "<td><input id=\"txtKetLain".$counter."\" name='txtKetLain".$counter."' type=\"text\" size=\"40\" ></td>
				  <td class='clsUang'><input class='clsBebanHarian' id=\"txtBebanHarianLain".$counter."\" name='txtBebanHarianLain".$counter."' type=\"text\" size=\"15\" ></td>
				  <td></td>
				  <td class='clsUang'><input class='clsTotal' id=\"txtTotalLain".$counter."\" name='txtTotalLain".$counter."' type=\"text\" size=\"15\" value='' ></td>
				</tr>
				<tr class='clsInputNumber'>
					<td></td>";
				$counter++;
			}
		}
	  ?>
      <td></td>
      <td class="lbl" colspan="2"><b>Grand Total</b></td>
      <td class='clsUang'><input class='clsTotal' id="txtGrandTotal" name='txtGrandTotal' type="text" size="15" ></td>
    </tr>
	<tr>
      <td class="lbl">Total Pengeluaran Perjalanan Dinas</td>
      <td colspan="4">Rp. <input type="text" class='clsTotal' id="lblTotalPengajuan"></td>
    </tr>
    <tr>
      <td class="lbl">Total Pengajuan Uang Muka</td>
      <td colspan="4">Rp. <input type="text" class='clsTotal' id="lblTotalPengajuanUangMuka" value="<?php	
		if ($trkSPD_array[0]['uangmuka'] == 0){
			echo "0";
		}else{
			echo $trkSPD_array[0]['totalSPD']; 
		}
	  ?>" readonly>
	  </td>
    </tr>
	<tr>
      <td class="lbl"><label id="statusLebih"></label></td>
      <td colspan="4">Rp. <input type="text" class='clsTotal' id="lblSelisih"></td>
    </tr>
	<!--<tr>
      <td>Terbilang</td>
      <td colspan="4"><label id="lblTerbilang"></label></td>
    </tr>-->
  </tbody>
</table>
<br/>

<!-- Permintaan Uang Muka -->
<table id="" border=0>
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Permintaan Uang Muka</td>
    </tr>
    <tr>
      <td colspan="5"><br/></td>
    </tr>
    <tr>
    <td class="lbl">No Uang Muka</td>
    <td bgcolor="#D3D3D3"> <input id="txtNoUM" name='txtNoUM' type="text" size="30" value="<?php echo $trkUangMuka_array[0]['NoUangMuka']; ?>" readonly="true"> </td>	 
    </tr>
    <tr>
    <td class="lbl">Keterangan</td>
    <td bgcolor="#D3D3D3"><input id="txtKeterangan" class="form-control" name='txtKeterangan' type="text" size="50%" value="<?php echo $trkUangMuka_array[0]['KeteranganPermohonan']; ?>"></td>	 
    </tr>
    <tr>
                <td class="lbl">Tanggal Permohonan Uang Muka</td>
                <td><?php echo date("j F Y"); ?></td>	  
                </tr>
    <tr>
                <td class="lbl">Pilih No. Persetujuan</td>
                <td colspan="4" valign="top"><input class="form-control" type="text" id="txtNoPP"/>
												<select class="form-control select2" id="txtDisplayPP"  name="txtDisplayPP">
												<option value =''></option>
												<?php foreach($dataNoPP as $row){
														if ($row->NoPP==$trkUangMuka_array[0]['NoPP'])
														echo "<option selected value='".$row->NoPP."'>".$row->display." </option>";
														else
														echo "<option value='".$row->NoPP."'>".$row->display." </option>";
												} ?>
												</select>
                </td>
    </tr>
    <tr>
                <td class="lbl">Jenis Kegiatan Uang Muka</td>
                <td colspan="3" valign="top">
														<select class="form-control"  id="txtKegiatan" name="txtKegiatan"> 
														<option value =''>Pilih salah satu</option>                               
														<?php foreach($dataJenisKeterangan as $row){
																if ($row->IdKegiatan==$trkUangMuka_array[0]['JenisKegiatanUangMuka'])
																echo "<option selected value='".$row->IdKegiatan."'>".$row->JenisKegiatan." </option>";
																else
																echo "<option value='".$row->IdKegiatan."'>".$row->JenisKegiatan." </option>";
														} ?>
														</select>
                </td>
                <td>					
                  <div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
                </td>
      </tr>
                
      <tr>
                <td class="lbl">Outstanding Uang Muka</td>
                <td><?php 
                        if($dataOutstanding)
                        {  echo "Ada"; 
                           echo "<input id='txtOutstanding'  name='txtOutstanding' type='hidden' value='Ada' >";
                        }
                        else
                        {  echo "Tidak Ada";
                           echo "<input id='txtOutstanding'  name='txtOutstanding' type='hidden' value='TidakAda' >";
                        }
                    ?></td>	  
        </tr>
    </tbody>
    </table>
    <br>   
   
    
    
    <table id="">
    <tr>
      <td class="lbl">Total Permintaan</td>
      <td colspan="2"><label class='clsUang' id="lblJmlhPermintaan"></label></td>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td>Terbilang</td>
      <td colspan="2"><input type="text" id="lblTerbilang2" disabled></td>
      <td></td>
    </tr>
  </table>
<br/>
<table id="" border=0>
            <tbody>
                <!-- Results table headers -->
                
                <tr>
                    <td class="lbl">Pembayaran Uang Muka</td>
                    <td ><input class="form-control" id="txtPilihBayar" name="txtPilihBayar" value="<?php echo $trkUangMuka_array[0]['TipeBayarUangMuka'] ?>" readonly>              
                    </td>	
                    <td class="lbl">Waktu Pembayaran Tercepat</td>
                    <td><input type="text" class='bordered' id="txtWaktuBayarCepat" name="txtWaktuBayarCepat" value="<?php echo $trkUangMuka_array[0]['WaktuBayarTercepat']; ?>" readonly/></td>
                </tr>
                <tr>
                    <td class="lbl">Bank</td>
                    <td><input type="text" class="form-control" id="txtBank" name="txtBank" value="<?php echo $trkUangMuka_array[0]['BankBayarUangMuka']; ?>" /></td>
                    <td class="lbl">Paling Lambat Waktu Penyelesaian</td>
                    <td><input type="text" class='bordered' id="txtWaktuSelesaiUM" name="txtWaktuSelesaiUM" value="<?php echo $trkUangMuka_array[0]['WaktuPenyelesaianTerlambat']; ?>" readonly /></td>
                </tr>
                <tr>
                    <td class="lbl">No Rekening</td>
                    <td ><input id="txtNoRek" class="form-control" name='txtNoRek' type="text" value="<?php echo $trkUangMuka_array[0]['NoRekBayarUangMuka']; ?>"></td>
                </tr>
                <tr>
                    <td class="lbl">Nama Penerima</td>
                    <td ><input id="txtPenerima" class="form-control" name='txtPenerima' type="text" value="<?php echo $trkUangMuka_array[0]['NmPenerimaBayarUangMuka']; ?>"></td>
                </tr>
            </tbody>
</table>
<!-- tanda tangan -->
<table id="tblTandaTanganDIC"  border=0>
  <tbody>
  
    <tr class="ttd">
      <td rowspan="2">Pemohon</td>
      <td colspan="3">Menyetujui</td>
    </tr>
    <tr class="ttd">
      <td>Atasan</td>
      <td>HRGA Dept Head</td>
      <?php   if($dataOutstanding) { ?>
        <td>DIC Accounting Tax & Control</td>
      <?php }?>
    </tr>
    <tr class="ttd">
      <td><br/><br/><br/><br/><br/></td>
      <td></td>
      <td></td>
    </tr>
    <tr class="ttd">
      <td><?php echo $trkSPD_array[0]['nama']; ?></td>
      <td><?php echo $trkSPD_array[0]['atasan']; ?></td>
      <td>Eviyati</td>
      <?php   if($dataOutstanding) { ?>
        <td>Fredyanto Manalu</td>
      <?php }?>
      
    </tr>
  </tbody>
</table>
<br>
<?php   if($dataOutstanding) { ?>
    
            <table  border=0>
            <tbody>
                <tr class="ttd">
                <td >No Uang Muka</td>
                <td >Keterangan</td>
                <td >Total</td>
                <td >Alasan Outstanding</td>
                </tr>
                <?php 
                        for($i=0;$i<count($dataOutstanding);$i++){
                          if($dataOutstanding[$i]['Total']<>'' or $dataOutstanding[$i]['Total']<>null or $dataOutstanding[$i]['Total']<>0){
                            echo "<tr><td>".$dataOutstanding[$i]['NoUangMuka'] ." </td>
                                    <td>". $dataOutstanding[$i]['KeteranganPermohonan'] ."</td>
                                    <td>Rp ". number_format($dataOutstanding[$i]['Total'],2,',','.') ."</td>   
                                    <td>". $dataOutstanding[$i]['ReasonOutstanding'] ."</td>                
                                    </tr> ";    
                          }else{
                            echo "<tr><td>".$dataOutstanding[$i]['NoUangMuka'] ." </td>
                                    <td>". $dataOutstanding[$i]['KeteranganPermohonan'] ."</td>
                                    <td>Rp ". number_format($dataOutstanding[$i]['TotalSPD'],2,',','.') ."</td>   
                                    <td>". $dataOutstanding[$i]['ReasonOutstanding'] ."</td>                
                                    </tr> ";            
                        }
                      }
                ?>    
            </tbody>
            </table>
      <?php }?>

            <br/>   
<div id="note">
</div><br/>
<table class="tblPureNoBorder"><tr><td>
	<input class="btn btn-primary"  type="submit" onClick="return confirm ('Apakah Anda yakin ingin mengupdate / menambah Laporan SPD ini ke dalam database?')" id="btnSubmit" name="submitUpdateSPD" value="Update">
	<a href="RekapSPDnew"><input class="btn btn-default"  type="button" value="Batal" ></a>
	<div id="divError" class="alert alert-danger"></div>
</tr></td></table>
</form>
<?php } 
else {?>
            SUDAH DITERIMA HRGA TIDAK BOLEH EDIT KONTEN
            <BR>
            <form action="submitUpdateSPD" method="post" accept-charset="utf-8">
    
              <input id="txtMode" name='txtMode' type="hidden" size="30" value="inputreason">
              <input id="txtNoSPD" name='txtNoSPD' type="hidden" size="30" value="<?php echo $_GET['NoSPD']; ?>" >
              <table border=0>
              <tr>
                <td class="lbl">Alasan Outstanding</td>
                <td colspan="4"><input id="txtReason" name='txtReason' type="text" size="15" value=""></td>
              </tr>
              </table>

              <table class="tblPureNoBorder"><tr><td>
              <input class="buttonSubmit"  type="submit" onClick="return confirm ('Apakah Anda yakin ingin update Alasan ini?')" id="" name="submitUpdateSPD" value="Update">
              </tr></td></table>
          </form>
<?php }?>
	</body>
  <script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
  <script type="text/javascript">
    
      
      $('#txtNoPP').hide();
      $('#txtNoPP').keyup(function(){
        var kodePP = document.getElementById('txtNoPP').value;
        $('#txtDisplayPP').val(kodePP);			
      });
      
      $('#txtDisplayPP').change(function(){
        $('#txtNoPP').val($('#txtDisplayPP').val());
        var kodePP = $('#txtNoPP').val();			
      });
      
      $(".select2").select2();

      $('#txtKegiatan').change(function(){				
        loadtanggalPembayaranPenyelesaianUM();
      });

      function loadtanggalPembayaranPenyelesaianUM(){
        $('#divLoadingSubmit').show();
        var idKegiatan = document.getElementById('txtKegiatan').value;  
        var tglMulai = document.getElementById("txtTanggalBerangkat").value;
        var tglSelesai = document.getElementById("txtTanggalKembali").value;

        tglMulai = tglMulai.substring(6, 10)+"-"+tglMulai.substring(3, 5)+"-"+tglMulai.substring(0, 2);
        tglSelesai = tglSelesai.substring(6, 10)+"-"+tglSelesai.substring(3, 5)+"-"+tglSelesai.substring(0, 2);

        if (tglSelesai=='' && tglMulai=='')
        {
            alert("Harap mengisi Tanggal Mulai dan Tanggal Selesai terlebih dulu"); 
        }
        else
        { 
            $.ajax({
                url: '<?php echo $this->config->base_url(); ?>index.php/UangMuka/formUMLain/ajax_loadWaktuJenisKegiatanDPA',
                type: "POST",             
                data: {idKegiatan:idKegiatan,tglMulai:tglMulai,tglSelesai:tglSelesai},
                dataType: 'json',
                cache: false,                
                success: function(data)
                {	
                console.log(data);   
                //alert('testttt');                 
                    $('#txtWaktuBayarCepat').val(data[0].WaktuBayarUangMuka);
                    $('#txtWaktuSelesaiUM').val(data[0].WaktuSelesaiUangMuka);
                    $('#divLoadingSubmit').hide();
                },
                error: function (request, status, error) {
                    console.log(error);
                    $('#divLoadingSubmit').hide();
                }
            });
        }
      }

      $('#txtTanggalBerangkat').change(function () { loadtanggalPembayaranPenyelesaianUM(); });
      $('#txtTanggalKembali').change(function () { loadtanggalPembayaranPenyelesaianUM(); });
    </script>
      <script>
          $(function(){
        //Date picker
              $('#txtTanggalMulai').datepicker({
                  autoclose: true,
                  format: 'yyyy-mm-dd'
              });
              $('#txtTanggalSelesai').datepicker({
                  autoclose: true,
                  format: 'yyyy-mm-dd'
              });
          });
      </script>    
    <script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
</html>