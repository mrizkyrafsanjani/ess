<html>
	<style type="text/css">
	</style>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function alertsizeATK(pixels){
			pixels+=32;
			document.getElementById('iFrameAtk').style.height=pixels+"px";
		}
		
		function search(){
			var namaKaryawan = document.getElementById('txtNamaKaryawan').value;
			var tahun = document.getElementById('cmbTahun').value;
			var bulan = document.getElementById('cmbBulan').value;
			var npk = '<?php echo $npk; ?>';
			
			if(namaKaryawan === '' || namaKaryawan === null)
				namaKaryawan = 'non';
			
			var iframe = document.getElementById('iFrameAtk');
			if(npk === '')
			{
				iframe.src = '<?php echo $this->config->base_url(); ?>index.php/Atk/PoAtk/viewAdmin/'+tahun+'/'+bulan+'/'+namaKaryawan;
			}
			else
			{
				iframe.src = '<?php echo $this->config->base_url(); ?>index.php/Atk/PoAtk/viewUser/'+tahun+'/'+bulan+'/'+namaKaryawan+'/'+npk;
			}				
		}
		
		function generateATK()
	{
		var cmbTahun = $('#cmbTahun').val();
		var cmbBulan = $('#cmbBulan').val();
		$.post( 			
			'<?php echo $this->config->base_url(); ?>index.php/Atk/AtkController/ajax_prosesGenerateATK',
			 { tahun: cmbTahun, bulan: cmbBulan },
			 function(response) {
				if(response != '')
					alert(response);
				window.location.replace('<?php echo site_url('Atk/AtkController/LaporanPoAtk'); ?>');
			 }
		);
	}
	
	
	
	$(function() {
		$('.buttonSubmit').click(function (){
				$('#printArea').printElement({
					//leaveOpen: true,
					printMode:'popup',
					overrideElementCSS:[
						'<?php echo $this->config->base_url(); ?>assets/css/cetak.css','<?php echo $this->config->base_url(); ?>assets/css/style-common.css']
				});
			});
			
			
			
		});
	</script>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Laporan PO ATK</h3>
		</div>
		<div class="panel-body">
		<?php echo validation_errors(); ?>
		<table border=0>
			<?php 
				if($npk == ''){
			?>
			
			<tr>
				<td>Nama Karyawan *</td>
				<td><select class="form-control" id="txtNamaKaryawan">
				<?php foreach($databawahan as $row){
					echo "<option value='".$row->NPK."'>".$row->Nama."</option>";
				} ?>
				</select></td>
			</tr>
			<?php }else{ ?>
			<tr>
				<td></td>
				<td><input type="hidden" class="form-control" id="txtNamaKaryawan"></td>
			</tr>
			<?php } ?>
			<tr>
				<td>Tahun</td>
				<td>
					<select class="form-control" id="cmbTahun" name="cmbTahun" >
					<?php 
						for($i=-1;$i<6;$i++)
					{
						$tahun = date('Y') + $i;
						echo "<option value='$tahun'>$tahun</option>";
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Bulan</td>
				<td>
					<select class="form-control" id="cmbBulan" name="cmbBulan">
					<?php 
						for($i=1;$i<=12;$i++)
						{
							$selected = '';
							if(date('n') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari">
					<input class="btn btn-primary" type="button" onClick="generateATK()" id="btnGenerate" name="" value="Generate PO ATK">
				</td>
			</tr>
			
			<div id='printArea'>
		</table>
		<?php 
			if($npk == '' && $admin == '1'){
		?>
		<iframe id="iFrameAtk" src="<?php echo $this->config->base_url(); ?>index.php/Atk/PoAtk/viewAdmin" width="100%" height="200px" seamless="seamless">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		<?php }else{ ?>
		<iframe id="iFrameAtk" src="<?php echo $this->config->base_url(); ?>index.php/Atk/PoAtk/viewUser/non/non/non/<?php echo $npk; ?>/<?php echo $kodeusertask; ?>" width="100%" height="300px" seamless="seamless">
		  <p>Your browser does not support iframes.</p>
		</iframe>		
		<?php } ?>
		
		</div>
		</div>
	</body>
</html>
