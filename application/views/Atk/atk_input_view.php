<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	
	function alertsizeATK(pixels){
			pixels+=132;
			document.getElementById('iFrameATKDetail').style.height=pixels+"px";
		}
	
	function btnSubmit_click(){
			
			if(confirm('Apakah Anda yakin ingin menyimpan data ini?')==true)
			{
				$.post(
				'<?php echo $this->config->base_url(); ?>index.php/Atk/AtkController/ajax_submitAtk',
				{ npk: '<?php echo $npk; ?>', kodeusertask: '<?php echo $kodeusertask; ?>', editmode: '<?php 
					if($editmode=='0')
					{ 
						echo 'false'; 
					}
					else if($editmode == '1')
					{
						echo 'true';
					}
					
					?>' },
				function(response){
					alert(response);
					<?php if($editmode=='0'){ ?>
						window.location.replace('<?php echo site_url('Atk/AtkController/InputAtk'); ?>');
					<?php }else{ ?>
						window.location.replace('<?php echo site_url('UserTask'); ?>');
					<?php } ?>
					
				}
			);
			}
		}
	
	function btnCancel_click(){
				if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
				{
					$.post(
				'<?php echo $this->config->base_url(); ?>index.php/Atk/AtkController/ajax_cancelAtk',
				{ npk: '<?php echo $npk; ?>', editmode: 'false' },
				function(response){
					alert(response);
					<?php if($editmode=='0'){ ?>
						window.location.replace('<?php echo site_url('Atk/AtkController/InputAtk'); ?>');
					<?php }else{ ?>
						window.location.replace('<?php echo site_url('UserTask'); ?>');
					<?php } ?>
					
				}
			);
				}
			}
		
		
	</script>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Input Request ATK</h3>
			</div>
			<div class="panel-body">
				<br/>
				<div class="row">
					<div class="col-md-3">
						Tanggal Request
					</div>
					<div class="col-md-6">
						<?php echo $tanggal; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Departemen 
					</div>
					<div class="col-md-6">
						<?php echo $departemen; ?>
					</div>
				</div>
				<br/>
				<iframe id="iFrameATKDetail" src="<?php echo $this->config->base_url(); ?>index.php/Atk/detailAtk/index/<?php
				if($editmode == '0')
				{ 
					echo $npk;
				} 
				else if($editmode == '1')
				{
					echo $kodeusertask."/editmode";
				}
				?>" width="100%" height="200px" seamless="seamless">
					<p>Your browser does not support iframes.</p>
				</iframe>
				<div class="row">
					<div class="col-md-3">
						History Catatan :
					</div>
					<div class="col-md-9">
						<?php echo $historycatatan; ?>
					</div>
				</div>
				<div class="row">
					<div align="right">
						<button id="btnSubmit" onClick="btnSubmit_click()" type="button" class="btn btn-primary">Simpan</button>
						<button id="btnCancel" onClick="btnCancel_click()" type="button" class="btn ">Batal</button>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
</html>
