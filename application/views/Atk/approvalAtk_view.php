<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	
	function alertsizeATK(pixels){
			pixels+=32;
			document.getElementById('iFrameATKDetail').style.height=pixels+"px";
		}
		
	function btnApprove_click(){
			var txtCatatan = $('#txtCatatan');
			if(confirm('Apakah Anda yakin ingin approval data ini?')==true)
			{
				$.post( 
				<?php if($realisasi == '1'){ ?>
				'<?php echo $this->config->base_url(); ?>index.php/Atk/AtkController/ajax_prosesApprovalRealisasi',
				<?php }else{ ?>
				'<?php echo $this->config->base_url(); ?>index.php/Atk/AtkController/ajax_prosesApproval',
				<?php } ?>
				 { kodeusertask: '<?php echo $kodeusertask; ?>', status: 'AP',catatan: txtCatatan.val() },
				 function(response) {
					alert(response);
					
					window.location.replace('<?php echo site_url('UserTask'); ?>');
				 }
			);
			}
		}
	
	function btnDecline_click(){
			var txtCatatan = $('#txtCatatan');
			
			if(txtCatatan.val() === '')
			{
				alert('Catatan harus diisi!');
			}
			else
			{
				if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
				{
					$.post(
						<?php if($realisasi == '1'){ ?>
						'<?php echo $this->config->base_url(); ?>index.php/Atk/AtkController/ajax_prosesApprovalRealisasi',
						<?php }else{ ?>
						'<?php echo $this->config->base_url(); ?>index.php/Atk/AtkController/ajax_prosesApproval',
						<?php } ?>
						 { kodeusertask: '<?php echo $kodeusertask; ?>', status: 'DE', catatan: txtCatatan.val() },
						 function(response) {
							alert(response);
							
							window.location.replace('<?php echo site_url('UserTask'); ?>');
						 }
					);
				}
			}
		}
	</script>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						Kode Request :
					</div>
					<div class="col-md-6">
						<?php echo $koderequestatk; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Tanggal Request
					</div>
					<div class="col-md-6">
						<?php echo $tanggal; ?>
					</div>
				</div>
				<br/>
				<iframe id="iFrameATKDetail" src="<?php echo $this->config->base_url(); ?>index.php/Atk/detailAtk/index/<?php echo $kodeusertask;?>/approve" width="100%" height="200px" seamless="seamless">
					<p>Your browser does not support iframes.</p>
				</iframe>
				<br/><br/><br/><br/>
				<div class="row">
				<?php //if($realisasi == '0'){ ?>
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				<?php //} ?>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						History Catatan :
					</div>
					<div class="col-md-9">
						<?php echo $historycatatan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div align="right">
						<button id="btnApprove" type="button" onClick="btnApprove_click()" class="btn btn-primary">Approve</button>
						<button id="btnDecline" type="button" onClick="btnDecline_click()" class="btn ">Decline</button>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
</html>
