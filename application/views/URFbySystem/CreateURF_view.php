<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
    
	<script type="text/javascript">
        
		$(function(event) {
            
            $("#npkURFUser").attr("disabled", true);
            $("#namaPKL").attr("disabled", true);
            $("#tempDate").attr("disabled", true);
            var disable = false;
            disableAll();
            function disableAll(){
                $("#colApp :input").attr("disabled", true);
                $("#colUser :input").attr("disabled", true);
                $("#colEmail :input").attr("disabled", true);
                $("#colOthers :input").attr("disabled", true);
                $("#colAccount :input").attr("disabled", true);


                $("#colApp :input").removeAttr('checked');
                $("#colUser :input").removeAttr('checked');
                $("#colEmail :input").removeAttr('checked');
                $("#colOthers :input").removeAttr('checked');
                $("#colAccount :input").removeAttr('checked');
            }
            function enableAll(){
                $("#colApp :input").attr("disabled", false);
                $("#colUser :input").attr("disabled", false);
                $("#colEmail :input").attr("disabled", false);
                $("#colOthers :input").attr("disabled", false);
                $("#colAccount :input").attr("disabled", false);


                $("#colApp :input").removeAttr('checked');
                $("#colUser :input").removeAttr('checked');
                $("#colEmail :input").removeAttr('checked');
                $("#colOthers :input").removeAttr('checked');
                $("#colAccount :input").removeAttr('checked');
            }
            $("#rbPermanent").click(function(){
                $("#tempDate").attr("disabled", true);
            });
            $("#rbTemporary").click(function(){
                $("#tempDate").attr("disabled", false);
            });
            $("#rbNoAssistant").click(function(){
                $("#npkURFUser").attr("disabled", true);
            });
            $("#rbYesAssistant").click(function(){
                $("#npkURFUser").attr("disabled", false);
            });
            $("#selectTemplate").change(function(){
                disableAll();
                var val1 = $('#selectTemplate option:selected').val();
                if(val1 == "Install"){
                    $("#colApp :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                }else if(val1 == "Create"){
                    $("#colUser :input").attr("disabled", false);
                    $("#colAccount :input").attr("disabled", false);
                    $("#chkdeleteUserAccount").attr("disabled", true);
                    $("#chkresetPassword").attr("disabled", true);
                    $("#chkcreateUserAccount").attr('checked','checked');
                    $("#Deskripsi").focus();
                }else if(val1 == "Delete"){
                    $("#colUser :input").attr("disabled", false);
                    $("#colAccount :input").attr("disabled", false);
                    $("#chkcreateUserAccount").attr("disabled", true);
                    $("#chkresetPassword").attr("disabled", true);
                    $("#chkdeleteUserAccount").attr('checked','checked');
                    $("#Deskripsi").focus();
                }else if(val1 == "Reset"){
                    $("#colUser :input").attr("disabled", false);
                    $("#colAccount :input").attr("disabled", false);
                    $("#chkresetPassword").attr('checked','checked');
                    $("#chkdeleteUserAccount").attr("disabled", true);
                    $("#chkcreateUserAccount").attr("disabled", true);
                    $("#Deskripsi").focus();
                }else if(val1 == "File Sharing"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#chkFile").attr('checked','checked');
                    $("#Deskripsi").focus();
                    
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkIT").attr("disabled", true);
                    $("#chkInternet").attr("disabled", true);
                    $("#chkBackup").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "IT Device"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#chkIT").attr('checked','checked');
                    $("#Deskripsi").focus();
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkFile").attr("disabled", true);
                    $("#chkInternet").attr("disabled", true);
                    $("#chkBackup").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "Internet"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#chkInternet").attr('checked','checked');
                    $("#Deskripsi").focus();
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkFile").attr("disabled", true);
                    $("#chkIT").attr("disabled", true);
                    $("#chkBackup").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "Backup"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#chkBackup").attr('checked','checked');
                    $("#Deskripsi").focus();
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkFile").attr("disabled", true);
                    $("#chkIT").attr("disabled", true);
                    $("#chkInternet").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "Email"){
                    $("#colEmail :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                }else if(val1 == "Lainnya"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                }else if(val1 == "Custom"){
                    enableAll();
                    $("#Deskripsi").focus();
                }else{
                    disableAll();
                }
            });
            
            $('#chkPKL').click(function() {
                $("#namaPKL").attr("disabled", !this.checked);
                $("#namaPKL").val("");
            });
            
            $("#rbNoAssistant").click(function(){
                $("#npkURFUser").attr("disabled", this.checked);
                $("#npkURFUser").val("");
            });
            $("#rbYesAssistant").click(function(){
                $("#npkURFUser").attr("disabled", !this.checked);
                $("#npkURFUser").val("");
            });
            
		});
		
	</script>
    
    <script type="text/javascript">
        
        function cetakSubmitURF() {
            var selectTemplate = document.getElementById("selectTemplate").value;
            var tempDate = document.getElementById("tempDate").value;
            var flag = true;
            var rbPermanent = document.getElementById('rbPermanent').checked;
            var rbTemporary = document.getElementById('rbTemporary').checked;
            var rbNoAssistant = document.getElementById('rbNoAssistant').checked;
            var rbYesAssistant = document.getElementById('rbYesAssistant').checked;
            <?php 
                if($departemen[0]->departemen == 'HRGA'){
            ?>
                var chkPKL = document.getElementById('chkPKL').checked;
                var namaPKL = document.getElementById("namaPKL").value;
            <?php
                }else{
            ?>
                var chkPKL = false;
                var namaPKL = '';
            <?php
                }
            ?>
            var npkURFUser = document.getElementById("npkURFUser").value;
            var deskripsi = document.getElementById('Deskripsi').value;
            
            var chkAppSiDP = document.getElementById("chkAppSiDP").checked;
            var chkAppsiDAPEN1 = document.getElementById("chkAppsiDAPEN1").checked;
            var chkAppsiDAPEN2 = document.getElementById("chkAppsiDAPEN2").checked;
            var chkAppApisoft = document.getElementById("chkAppApisoft").checked;
            var chkAppCORE = document.getElementById("chkAppCORE").checked;
            var chkAppAccpac = document.getElementById("chkAppAccpac").checked;
            var chkAppSIAP = document.getElementById("chkAppSIAP").checked;
            var chkAppCARE = document.getElementById("chkAppCARE").checked;
            var chkAppOther = document.getElementById("chkAppOther").checked;
            var chkuserIDWindows = document.getElementById("chkuserIDWindows").checked;
            var chkuserIDSiDP = document.getElementById("chkuserIDSiDP").checked;
            var chkuserIDsiDAPEN1 = document.getElementById("chkuserIDsiDAPEN1").checked;
            var chkuserIDsiDAPEN2 = document.getElementById("chkuserIDsiDAPEN2").checked;
            var chkuserIDApisoft = document.getElementById("chkuserIDApisoft").checked;
            var chkuserIDCORE = document.getElementById("chkuserIDCORE").checked;
            var chkuserIDAccpac = document.getElementById("chkuserIDAccpac").checked;
            var chkuserIDSIAP = document.getElementById("chkuserIDSIAP").checked;
            var chkuserIDCARE = document.getElementById("chkuserIDCARE").checked;
            var chkuserIDOther = document.getElementById("chkuserIDOther").checked;
            
            var chkemail_aiastracoid = document.getElementById("chkemail_aiastracoid").checked;
            var chkemail_dapenastracom = document.getElementById("chkemail_dapenastracom").checked;
            var chkemail_dpacoid = document.getElementById("chkemail_dpacoid").checked;
            var chkcreateUserAccount = document.getElementById("chkcreateUserAccount").checked;
            var chkdeleteUserAccount = document.getElementById("chkdeleteUserAccount").checked;
            var chkresetPassword = document.getElementById("chkresetPassword").checked;
            var chkFile = document.getElementById("chkFile").checked;
            var chkIT = document.getElementById("chkIT").checked;
            var chkInternet = document.getElementById("chkInternet").checked;
            var chkBackup = document.getElementById("chkBackup").checked;
            
            if(!rbPermanent && !rbTemporary){
                alert('Mohon isi Durasi');
                flag = false;
            }else if(!rbYesAssistant && !rbNoAssistant){
                alert('Mohon isi apakah URF untuk orang lain');
                flag = false;
            }else if(deskripsi == ""){
                alert('Mohon isi Deskripsi singkat mengenai URF ini');
                flag = false;
            }else if(rbTemporary && (tempDate == "")){
                alert('Mohon isi Expired Period Date');
                flag = false;
            }
            else if(selectTemplate == ""){
                alert('Mohon pilih template yang tersedia, jika permintaan khusus, pilih Custom');
                flag = false;
            }else if(chkcreateUserAccount && chkdeleteUserAccount){ 
                alert('Mohon pilih salah satu, apakah ingin menghapus, atau mendelete user ID');
                flag = false;
            }
            else if(selectTemplate == "Install"){
                if( !chkAppSiDP && !chkAppsiDAPEN1 && !chkAppsiDAPEN2 && !chkAppApisoft && !chkAppCORE && !chkAppAccpac && !chkAppSIAP && !chkAppCARE && !chkAppOther){
                    alert('Mohon pilih salah satu aplikasi yang ingin di install, jika aplikasi khusus, pilih template Custom');
                    flag = false;
                }
            }
            else if(selectTemplate == "Create"){
                if (!chkcreateUserAccount){
                    alert('Mohon checklist Create User ID di centang');
                    flag = false;
                }else{
                    
                    if( !chkuserIDWindows && !chkuserIDSiDP && !chkuserIDsiDAPEN1 && !chkuserIDsiDAPEN2 && !chkuserIDApisoft && !chkuserIDCORE && !chkuserIDAccpac && !chkuserIDSIAP && !chkuserIDCARE && !chkuserIDOther){
                        alert('Mohon pilih salah satu user aplikasi yang ingin dibuat, jika aplikasi khusus, pilih template Custom');
                        flag = false;
                    }
                }
            }
            else if(selectTemplate == "Delete"){
                
                if (!chkdeleteUserAccount){
                    alert('Mohon checklist Delete user ID di centang');
                    flag = false;
                }else{
                    
                    if( !chkuserIDWindows && !chkuserIDSiDP && !chkuserIDsiDAPEN1 && !chkuserIDsiDAPEN2 && !chkuserIDApisoft && !chkuserIDCORE && !chkuserIDAccpac && !chkuserIDSIAP && !chkuserIDCARE && !chkuserIDOther){
                        alert('Mohon pilih salah satu user aplikasi yang ingin dihapus, jika aplikasi khusus, pilih template Custom');
                        flag = false;
                    }
                }
            }
            else if(selectTemplate == "Reset"){
                
                if (!chkresetPassword){
                    alert('Mohon checklist Reset user ID di centang');
                    flag = false;
                }else{
                    if( !chkuserIDWindows && !chkuserIDSiDP && !chkuserIDsiDAPEN1 && !chkuserIDsiDAPEN2 && !chkuserIDApisoft && !chkuserIDCORE && !chkuserIDAccpac && !chkuserIDSIAP && !chkuserIDCARE && !chkuserIDOther){
                        alert('Mohon pilih salah satu user aplikasi yang ingin di reset / lupa password, jika aplikasi khusus, pilih template Custom');
                        flag = false;
                        event.preventDefault();
                    }
                }

            }
            else if(selectTemplate == "File Sharing"){
                
                if (!chkFile){
                    alert('Mohon checklist File Sharing di centang dan jelaskan di Deskripsi');
                    flag = false;
                }
            }
            else if(selectTemplate == "IT Device"){
               
                if (!chkIT){
                    alert('Mohon checklist IT Device di centang dan jelaskan di Deskripsi');
                    flag = false;
                }
            }
            else if(selectTemplate == "Internet"){
                
                if (!chkInternet){
                    alert('Mohon checklist Internet di centang dan jelaskan di Deskripsi');
                    flag = false;
                }
            }
            else if(selectTemplate == "Backup"){
                
                if (!chkBackup){
                    alert('Mohon checklist Internet di centang dan jelaskan di Deskripsi');
                    flag = false;
                }
            }
            else if(selectTemplate == "Email"){
                
                if(!chkemail_aiastracoid && !chkemail_dapenastracom && !chkemail_dpacoid){
                    alert('Mohon pilih salah satu email yang di inginkan, jika email khusus, pilih template Custom');
                    flag = false;
                }
            }
            else if(selectTemplate == "Lainnya"){

            }
            else if(selectTemplate == "Custom"){

            }
            
            if(!rbNoAssistant && !rbYesAssistant){
                alert('Mohon isi Durasi');
                flag = false;
            } 
            if(rbYesAssistant && (npkURFUser == "") && !chkPKL){
                alert('Mohon isi NPK User yang ingin dibuatkan URF nya');
                flag = false;
            } 
            if(rbYesAssistant && chkPKL){
                alert('Mohon pilih salah satu, untuk PKL atau karyawan tetap lainnya');
                flag = false;
            } 
            if(chkPKL){
                if(namaPKL == ''){
                    alert('Mohon isi Nama User PKL yang ingin dibuatkan URF nya');
                    flag = false;
                }
            }
            if(flag){
                var x = window.confirm("Cetak URF ini?");
                if(x){
                    $("#CreateURF").attr("action"); //will retrieve it
                    $("#CreateURF").attr("target", "_blank"); //will retrieve it
                    $("#CreateURF").attr("action", "<?php echo site_url('URFbySystem/URFbySystemController/CreateURF') ?>");
                    window.alert("Data sudah tersimpan, silahkan Tutup halaman ini");
                }else{
                    event.preventDefault();
                }
            }else{
                event.preventDefault();
            }
        }
    </script>
    <style>
    #error{
        color: red;
    }
    </style>
	<head>
		<title>Create URF </title>
	</head>
	<body>
           
        <?php echo $this->session->flashdata('message'); ?>

		<div style="width:100%;text-align:right;display:block;"><a href="<?php echo $this->config->base_url(); ?>assets/uploads/Referensi Table URF.xlsx" download>Referensi Table Akses User</a></div>
        <div class="container" style="width:80%">
            <form method="Post" onsubmit="cetakSubmitURF()" name="CreateURF" id="CreateURF">
                <div class="form-group">
                    <div class = "row">
                        <div class ="col">
                            <label for="sel1">Saya mau request</label>
                            <select class="form-control" id="selectTemplate" name="selectTemplate">
                                <option value="">--Pilih Template Request--</option>
                                <option value="Install">Install Aplikasi</option>
                                <option value="Create">Create User Aplikasi</option>
                                <option value="Delete">Delete User Aplikasi</option>
                                <option value="Reset">Reset / Lupa Password</option>
                                <option value="File Sharing">File Sharing</option>
                                <option value="IT Device">IT Device</option>
                                <option value="Internet">Internet</option>
                                <option value="Backup">Backup / Restore Database</option>
                                <option value="Email">Email</option>
                                <option value="Lainnya">Lainnya</option>
                                <option value="Custom">Custom</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="radio">
                                <label><input type="radio" id="rbNew" name="rdbNewUpdate" value="New" checked>New</label>
                                <label><input type="radio" id="rbUpdate" name="rdbNewUpdate" value="Update" >Update</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                        <label for="Lainnya">Deskripsi</label>
                        <textarea class="form-control" rows="2" id="Deskripsi" name="Deskripsi"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="Period">Expired Period</label>
                            <div class="radio">
                                <label><input type="radio" id="rbPermanent" name="rdbPeriod" value="Permanent" >Permanent</label>
                                <label><input type="radio" id="rbTemporary" name="rdbPeriod" value="Temporary" >Temporary Until</label>
                                <input type="date" class="form-control" id="tempDate" name="tempDate" min = <?php echo date("Y-m-d"); ?> style="width:20%">
                            </div>
                        </div>
                    </div>
                    <!-- 
                    <div class="row">
                        <div class="col">
                        </div>
                    </div>
                    -->
                    <div class="row">
                        <!-- Application -->
                        <div class="col-sm-2" id="colApp">
                            <label for="chk1">Aplikasi</label>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppSiDP" name="chkAppSiDP" value="chkSiDP">SiDP</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppsiDAPEN1" name="chkAppsiDAPEN1" value="chksiDAPEN1">siDAPEN1</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppsiDAPEN2" name="chkAppsiDAPEN2" value="chksiDAPEN2">siDAPEN2</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppApisoft" name="chkAppApisoft" value="chkApisoft">Apisoft</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppCORE" name="chkAppCORE" value="chkCORE">CORE</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppAccpac" name="chkAppAccpac" value="chkAccpac">Accpac</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppSIAP" name="chkAppSIAP" value="chkSIAP">SIAP</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppCARE" name="chkAppCARE" value="chkCARE">CARE</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppOther" name="chkAppOther" value="chkAppOther">Other</label></div>
                        </div>
                        <!-- User ID -->
                        <div class="col-sm-2" id="colUser">
                            <label for="chk1">User ID Aplikasi</label>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDWindows" name="chkuserIDWindows" value="userIDWindows">Windows</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDSiDP" name="chkuserIDSiDP" value="userIDSiDP">SiDP</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDsiDAPEN1" name="chkuserIDsiDAPEN1" value="userIDsiDAPEN1">siDAPEN1</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDsiDAPEN2" name="chkuserIDsiDAPEN2" value="userIDsiDAPEN2">siDAPEN2</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDApisoft" name="chkuserIDApisoft" value="userIDApisoft">Apisoft</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDCORE" name="chkuserIDCORE" value="userIDCORE">CORE</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDAccpac" name="chkuserIDAccpac" value="userIDAccpac">Accpac</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDSIAP" name="chkuserIDSIAP" value="userIDSIAP">SIAP</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDCARE" name="chkuserIDCARE" value="userIDCARE">CARE</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDOther" name="chkuserIDOther" value="userIDOther">Other</label></div>
                        </div>
                        <div class="col-sm-2" id="colAccount">
                            <label for="chk1"></label>
                            <div class="checkbox"><label><input type="checkbox" id="chkcreateUserAccount" name="chkcreateUserAccount" value="createUserAccount">Create User ID</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkdeleteUserAccount" name="chkdeleteUserAccount" value="deleteUserAccount">Delete User ID</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkresetPassword" name="chkresetPassword" value="resetPassword">Reset/Lupa Password</label></div>
                        </div>
                        <!-- Email-->
                        <div class="col-sm-2" id="colEmail">
                            <label for="chk1">Email</label>
                            <div class="checkbox"><label><input type="checkbox" id="chkemail_aiastracoid" name="chkemail_aiastracoid" value="email_aiastracoid">@ai.astra.co.id</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkemail_dapenastracom" name="chkemail_dapenastracom" value="email_dapenastracom">@dapenastra.com</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkemail_dpacoid" name="chkemail_dpacoid" value="email_dpacoid">@dpa.co.id</label></div>
                        </div>
                        <!-- Others-->
                        <div class="col-sm-2" id="colOthers">
                            <label for="chk1">Lainnya</label>
                            <div class="checkbox"><label><input type="checkbox" id="chkSoftware" name="chkSoftware" value="Software License">Software License</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkFile" name="chkFile" value="File Sharing">File Sharing</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkIT" name="chkIT" value="IT Device">IT Device</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkInternet" name="chkInternet" value="Internet">Internet</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkBackup" name="chkBackup" value="Backup">Backup / Restore</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkUpload" name="chkUpload" value="Upload">Upload</label></div>
                        </div>
                    </div>
                    <!-- <div class = "row">
                        <div class ="col">
                            <label for="sel1">Ada Lampiran</label>
                            <select class="form-control" id="selectLampiran" name="selectLampiran">
                                <option value="0">Tidak ada</option>
                                <option value="1">Ada</option>
                            </select>
                        </div>
                    </div> -->
                    <div class = "row">
                        <div class ="col-sm-6">
                            <label for="Period">Request untuk orang lain</label>
                            <div class="radio">
                                <label><input type="radio" id="rbNoAssistant" name="rdbAssistant" value="No" checked >Tidak</label>
                                <label><input type="radio" id="rbYesAssistant" name="rdbAssistant" value="Yes" >Ya</label>
                                <input type="text" class="form-control" id="npkURFUser" name="npkURFUser" style="width:100%" placeholder="Masukkan NPK User tersebut">
                            </div>
                        </div>
                        <?php 
                            if($departemen[0]->departemen == 'HRGA'){
                        ?>
                        <div class ="col-sm-6">
                            <label for="Period">Request untuk PKL</label>
                            <div class="radio">
                                <label><input type="checkbox" id="chkPKL" name="chkPKL" value="PKL" >PKL</label>   
                                <input type="text" class="form-control" id="namaPKL" name="namaPKL" style="width:100%" placeholder="Masukkan Nama PKL tersebut">
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col"><br/>
                            <input type="submit" class="btn btn-success" value="Print and Submit">
                        </div>
                    </div>
                </div>
            </form> 
        </div>
        
	</body>
</html> 