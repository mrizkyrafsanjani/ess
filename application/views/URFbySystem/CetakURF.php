<html lang="en">

<head>
    <title><?php echo $URF_Number; ?></title>
    <style type="text/css">
         ::selection {
            background-color: #E13300;
            color: white;
        }
         ::moz-selection {
            background-color: #E13300;
            color: white;
        }
         ::webkit-selection {
            background-color: #E13300;
            color: white;
        }
        #wrapper {
            width: 100%;
            height: 100%;
            margin: auto;
        }
        #body {
            float: right;
            width: 100%;
            height: 100%;
        }
        #sidebar {
            float: left;
            width: 30%;
            height: 100%;
        }
                
        hr.style2 {
            border-top: 3px double #8c8b8b;
        }
        html {
            margin: 20px
        }
        
        #RightLine {
            text-align: right;
        }
        body {
            background-color: #fff;
            font: 10px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }
        /*
        table tr td {
            padding-top: 10px;
            padding-bottom: 10px;
            cellspacing: 0px;
        }*/
        
        table.gridtable {
            font-family: normal Helvetica, Arial, sans-serif;
            ;
            font-size: 10px/20px;
            color: #333333;
            border-width: 1px;
            border-color: #666666;
            border-spacing: 0;
        }
        
        table.gridtable th {
            border-width: 1px;
            border-style: solid;
            border-color: #666666;
            background-color: #ffffff;
        }
        
        table, tr, th, td{
            
            border-collapse: collapse;
            border: 1px solid black;
        }
        table.gridtable td {
            border-width: 1px;
            border-style: solid;
            border-color: #666666;
            background-color: #ffffff;
            text-align: center;
        }
        #insideTable{
            border-collapse: collapse;
            border: 1px solid black;
        }
        .title{
            font-weight: bold;
        }
        ul {
          margin: 0;
        }
        ul.dashed {
          list-style-type: none;
        }
        ul.dashed > li {
          text-indent: -5px;
        }
        ul.dashed > li:before {
          content: "-";
          text-indent: -5px;
        }

    </style>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:5px 10px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:5px 10px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
        .tg .tg-uqo3{background-color:#efefef;text-align:center;vertical-align:top}
        .tg .tg-n1xd{background-color:#efefef;color:#333333;vertical-align:top}
        .tg .tg-baqh{text-align:center;vertical-align:top}
        .tg .tg-yzt1{background-color:#efefef;vertical-align:top}
        .tg .tg-my2k{background-color:#efefef;text-align:right;vertical-align:top}
        .tg .tg-yw4l{vertical-align:top}
        ul {
                
            list-style:none;
            padding-left:0;
        }
        #appname, #userID{
            float:left;
        }
    </style>
	<script>    
		function cetak(){ 
            var css = '@page { size: landscape; }',
            head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');

            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet){
            style.styleSheet.cssText = css;
            } else {
            style.appendChild(document.createTextNode(css));
            }

            head.appendChild(style);

            setTimeout(function () { window.print(); }, 500);
            setTimeout(function () { 
            alert('Klik dimanapun untuk menutup halaman ini'); }, 1000);
            //window.onfocus = function () { setTimeout(function () { window.close(); }, 0); }
		}
	</script>
</head>

<body onload = "cetak()" onclick="window.close()">
    <div id="wrapper">
        <div id="body">
            <image id="logoDPA" src='<?php echo $this->config->base_url(); ?>assets/images/logoDPA.png'>DANA PENSIUN ASTRA <?php echo $DPA; ?>
            <div id="table">
                <table class="tg" style="undefined;table-layout: fixed; width: 1040px">
                <colgroup>
                    <col style="width: 209px">
                    <col style="width: 208px">
                    <col style="width: 208px">
                    <col style="width: 278px">
                    <col style="width: 187px">
                </colgroup>
                <tr>
                    <th class="tg-baqh" colspan="4">USER REQUEST IT FORM</th>
                    <th class="tg-baqh" colspan="1"><?php if($URF_Number != '') echo $URF_Number;?></th>
                </tr>
                <tr>
                    <td class="tg-yzt1" colspan="4">Filled by User</td>
                    <td class="tg-my2k"><?php echo date('l, M d, Y'); ?></td>
                </tr>
                <tr>
                    <td class="tg-yw4l"></td>
                    <td class="tg-yw4l">Name</td>
                    <td class="tg-yw4l">Dept./Unit</td>
                    <td class="tg-yw4l">Email</td>
                    <td class="tg-yw4l">Ext.</td>
                </tr>
                <tr>
                    <td class="tg-yw4l">User/Initiator</td>
                    <td class="tg-yw4l"><?php if($namaUser != '') echo $namaUser;?></td>
                    <td class="tg-yw4l"><?php if($departemenUser != '') echo $departemenUser;?></td>
                    <td class="tg-yw4l"><?php if($emailUser != '') echo $emailUser;?></td>
                    <td class="tg-yw4l"><?php if($extUser != '') echo $extUser;?></td>
                </tr>
                <tr>
                    <td class="tg-yw4l"><?php if($jabatanAtasan != '') echo $jabatanAtasan;?></td>
                    <td class="tg-yw4l"><?php if($namaAtasan != '') echo $namaAtasan;?></td>
                    <td class="tg-yw4l"><?php if($departemenAtasan != '') echo $departemenAtasan;?></td>
                    <td class="tg-yw4l"><?php if($emailAtasan != '') echo $emailAtasan;?></td>
                    <td class="tg-yw4l"><?php if($extAtasan != '') echo $extAtasan;?></td>
                </tr>
                </table>
            <div id="table">
                <table class="tg" style="undefined;table-layout: fixed; width: 1040px">
                <colgroup>
                    <col style="width: 209px">
                    <col style="width: 208px">
                    <col style="width: 208px">
                    <col style="width: 238px">
                    <col style="width: 227px">
                </colgroup>
                <tr>
                    <td class="tg-n1xd" colspan="5">SYSTEM APPLICATION; USER ID; HARDWARE &amp; SOFTWARE RELATED</td>
                </tr>
                <tr>
                    <td class="tg-yw4l">Type of Request:</td>
                    <td class="tg-yw4l"><input type="checkbox" <?php if($New != '') echo "checked";?>>New</td>
                    <td class="tg-yw4l" colspan="3"><input type="checkbox" <?php if($Update != '') echo "checked";?>>Update</td>
                </tr>
                <tr>
                    <td class="tg-yzt1"><input type="checkbox" <?php if($APPLICATION != '') echo "checked";?>>APPLICATION</td>
                    <td class="tg-yzt1"><input type="checkbox" <?php if($USERID != '') echo "checked";?>>USER ID</td>
                    <td class="tg-yzt1"></td>
                    <td class="tg-yzt1"></td>
                    <td class="tg-yzt1"><input type="checkbox" <?php if($OTHERS != '') echo "checked";?>>OTHERS</td>
                </tr>
                <tr>
                    <td class="tg-yw4l"><i>Application Name at :</i></td>
                    <td class="tg-yw4l"><i>Application Name at :</i></td>
                    <td class="tg-yw4l"><i>Item Request :</i></td>
                    <td class="tg-yw4l"><i>Expired Period :</i></td>
                    <td class="tg-yw4l"><i>Item Request :</i></td>
                </tr>
                <tr>
                    <td class="tg-yw4l">
                        <ul id="appname">
                            <li><input type="checkbox" <?php if($chkAppSiDP != '') echo "checked";?>>SIDP</li>
                            <li><input type="checkbox" <?php if($chkAppsiDAPEN1 != '') echo "checked";?>>siDAPEN1</li>
                            <li><input type="checkbox" <?php if($chkAppsiDAPEN2 != '') echo "checked";?>>siDAPEN2</li>
                            <li><input type="checkbox" <?php if($chkAppApisoft != '') echo "checked";?>>Apisoft</li>
                            <li><input type="checkbox" <?php if($chkAppOther != '') echo "checked";?>>Other</li>
                        </ul>
                        <ul>
                            <li><input type="checkbox" <?php if($chkAppCORE != '') echo "checked";?>>CORE</li>
                            <li><input type="checkbox" <?php if($chkAppAccpac != '') echo "checked";?>>Accpac</li>
                            <li><input type="checkbox" <?php if($chkAppSIAP != '') echo "checked";?>>SIAP</li>
                            <li><input type="checkbox" <?php if($chkAppCARE != '') echo "checked";?>>CARE</li>
                            <li>&nbsp;</li>
                        </ul>
                    </td>
                    <td class="tg-yw4l">
                        <ul id="userID">
                            <li><input type="checkbox" <?php if($chkuserIDWindows != '') echo "checked";?>>Windows</li>
                            <li><input type="checkbox" <?php if($chkuserIDSiDP != '') echo "checked";?>>SIDP</li>
                            <li><input type="checkbox" <?php if($chkuserIDsiDAPEN1 != '') echo "checked";?>>siDAPEN1</li>
                            <li><input type="checkbox" <?php if($chkuserIDsiDAPEN2 != '') echo "checked";?>>siDAPEN2</li>
                            <li><input type="checkbox" <?php if($chkuserIDApisoft != '') echo "checked";?>>Apisoft</li>
                            <li><input type="checkbox" <?php if($chkuserIDOther != '') echo "checked";?>>Other</li>
                        </ul>
                        <ul>
                            <li><input type="checkbox" <?php if($chkuserIDCORE != '') echo "checked";?>>CORE</li>
                            <li><input type="checkbox" <?php if($chkuserIDAccpac != '') echo "checked";?>>Accpac</li>
                            <li><input type="checkbox" <?php if($chkuserIDSIAP != '') echo "checked";?>>SIAP</li>
                            <li><input type="checkbox" <?php if($chkuserIDCARE != '') echo "checked";?>>CARE</li>
                            <li>&nbsp;</li>
                            <li>&nbsp;</li>
                        </ul>
                    </td>
                    <td class="tg-yw4l">
                        <ul>
                            <li><input type="checkbox" <?php if($chkcreateUserAccount != '') echo "checked";?>>Created User ID</li>
                            <li><input type="checkbox" <?php if($chkdeleteUserAccount != '') echo "checked";?>>Delete User ID</li>
                            <li><input type="checkbox" <?php if($chkresetPassword != '') echo "checked";?>>Change/Unlock Password</li>
                        </ul>
                    </td>
                    <td class="tg-yw4l">
                        <ul>
                            <li><input type="checkbox" <?php if($Permanent == 'Permanent') echo "checked";?>>Permanent</li>
                            <li><input type="checkbox" <?php if($Temporary == 'Temporary') echo "checked";?>>Temporary Until</li>
                            <li><?php if($expiredPeriod != '') echo $expiredPeriod;?></li>
                        </ul>
                    </td>
                    <td class="tg-yw4l">
                        <ul>
                            <li><input type="checkbox" <?php if($chkSoftware != '') echo "checked";?>>Software License</li>
                            <li><input type="checkbox" <?php if($chkFile != '') echo "checked";?>>File Sharing</li>
                            <li><input type="checkbox" <?php if($chkIT != '') echo "checked";?>>IT Device</li>
                            <li><input type="checkbox" <?php if($chkInternet != '') echo "checked";?>>Internet</li>
                            <li><input type="checkbox" <?php if($chkBackup != '') echo "checked";?>>Restore</li>
                            <li><input type="checkbox" <?php if($chkUpload != '') echo "checked";?>>Upload</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="tg-yw4l">Request Description :</td>
                    <td class="tg-yw4l" colspan="4"><?php if($Deskripsi != "") { echo $Deskripsi; } ?></td>
                </tr>
                <tr>
                    <td class="tg-yzt1" colspan="4"><input type="checkbox" <?php if($urfEMAIL != '') echo "checked";?>>EMAIL</td>
                    <td class="tg-yw4l">Resolve Date:</td>
                </tr>
                <tr>
                    <td class="tg-yw4l" colspan="4">
                        <input type="checkbox" <?php if($chkemail_aiastracoid != '') echo "checked";?>>@ai.astra.co.id
                        <input type="checkbox" <?php if($chkemail_dapenastracom != '') echo "checked";?>>@dapenastra.com
                        <input type="checkbox" <?php if($chkemail_dpacoid != '') echo "checked";?>>@dpa.co.id
                    </td>
                    <td class="tg-yw4l" rowspan="2"></td>
                </tr>
                <tr>
                    <td class="tg-uqo3" colspan="4">Approval</td>
                </tr>
                <tr>
                    <td class="tg-baqh">User/Initiator</td>
                    <td class="tg-baqh"><?php if($jabatanAtasan != '') echo $jabatanAtasan;?></td>
                    <td class="tg-baqh"><?php if($jabatanHeadIT != '') echo $jabatanHeadIT;?></td>
                    <td class="tg-baqh"><?php if($jabatanDICIT != '') echo 'IT DIC';?></td>
                    <td class="tg-yw4l">Receive Date:</td>
                </tr>
                <tr height="80px">
                    <td class="tg-yw4l" rowspan="2"></td>
                    <td class="tg-yw4l" rowspan="2"></td>
                    <td class="tg-yw4l" rowspan="2"></td>
                    <td class="tg-yw4l" rowspan="2"></td>
                    <td class="tg-yw4l" rowspan="3"></td>
                </tr>
                <tr>
                </tr>
                <tr height="30px">
                    <td class="tg-baqh"><?php echo $namaUser;?></td>
                    <td class="tg-baqh"><?php echo $namaAtasan;?></td>
                    <td class="tg-baqh"><?php if($namaHeadIT != '') echo $namaHeadIT;?></td>
                    <td class="tg-baqh"><?php if($namaDICIT != '') echo $namaDICIT;?></td>
                </tr>
                </table>
            </div>

        </div>
        
    </div>

</body>

</html>