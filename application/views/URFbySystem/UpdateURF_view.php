<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
    
	<script type="text/javascript">
        
		$(function(event) {
            
            $("#tempDate").attr("disabled", true);
            $("#npkURFUser").attr("disabled", true);
            //disableAll();
            
            function disableAll(){
                $("#colApp :input").attr("disabled", true);
                $("#colUser :input").attr("disabled", true);
                $("#colEmail :input").attr("disabled", true);
                $("#colOthers :input").attr("disabled", true);
                $("#colAccount :input").attr("disabled", true);


                $("#colApp :input").removeAttr('checked');
                $("#colUser :input").removeAttr('checked');
                $("#colEmail :input").removeAttr('checked');
                $("#colOthers :input").removeAttr('checked');
                $("#colAccount :input").removeAttr('checked');
            }
            function enableAll(){
                $("#colApp :input").attr("disabled", false);
                $("#colUser :input").attr("disabled", false);
                $("#colEmail :input").attr("disabled", false);
                $("#colOthers :input").attr("disabled", false);
                $("#colAccount :input").attr("disabled", false);


                $("#colApp :input").removeAttr('checked');
                $("#colUser :input").removeAttr('checked');
                $("#colEmail :input").removeAttr('checked');
                $("#colOthers :input").removeAttr('checked');
                $("#colAccount :input").removeAttr('checked');
            }
            $("#rbPermanent").click(function(){
                $("#tempDate").attr("disabled", true);
            });
            $("#rbTemporary").click(function(){
                $("#tempDate").attr("disabled", false);
            });
            $("#selectTemplate").change(function(){
                disableAll();
                var val1 = $('#selectTemplate option:selected').val();
                if(val1 == "Install"){
                    $("#colApp :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                }else if(val1 == "Create"){
                    $("#colUser :input").attr("disabled", false);
                    $("#colAccount :input").attr("disabled", false);
                    $("#chkdeleteUserAccount").attr("disabled", true);
                    $("#chkresetPassword").attr("disabled", true);
                    $("#chkcreateUserAccount").attr('checked','checked');
                    $("#Deskripsi").focus();
                }else if(val1 == "Delete"){
                    $("#colUser :input").attr("disabled", false);
                    $("#colAccount :input").attr("disabled", false);
                    $("#chkcreateUserAccount").attr("disabled", true);
                    $("#chkresetPassword").attr("disabled", true);
                    $("#chkdeleteUserAccount").attr('checked','checked');
                    $("#Deskripsi").focus();
                }else if(val1 == "Reset"){
                    $("#colUser :input").attr("disabled", false);
                    $("#colAccount :input").attr("disabled", false);
                    $("#chkresetPassword").attr('checked','checked');
                    $("#chkdeleteUserAccount").attr("disabled", true);
                    $("#chkcreateUserAccount").attr("disabled", true);
                    $("#Deskripsi").focus();
                }else if(val1 == "File Sharing"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#chkFile").attr('checked','checked');
                    $("#Deskripsi").focus();
                    
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkIT").attr("disabled", true);
                    $("#chkInternet").attr("disabled", true);
                    $("#chkBackup").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "IT Device"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#chkIT").attr('checked','checked');
                    $("#Deskripsi").focus();
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkFile").attr("disabled", true);
                    $("#chkInternet").attr("disabled", true);
                    $("#chkBackup").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "Internet"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#chkInternet").attr('checked','checked');
                    $("#Deskripsi").focus();
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkFile").attr("disabled", true);
                    $("#chkIT").attr("disabled", true);
                    $("#chkBackup").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "Backup"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#chkBackup").attr('checked','checked');
                    $("#Deskripsi").focus();
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkFile").attr("disabled", true);
                    $("#chkIT").attr("disabled", true);
                    $("#chkInternet").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "Email"){
                    $("#colEmail :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                }else if(val1 == "Lainnya"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                }else if(val1 == "Custom"){
                    enableAll();
                    $("#Deskripsi").focus();
                }else{
                    disableAll();
                }
            });
            

		});
		$(document).ready(function (event) {
            function disableAll(){
                $("#colApp :input").attr("disabled", true);
                $("#colUser :input").attr("disabled", true);
                $("#colEmail :input").attr("disabled", true);
                $("#colOthers :input").attr("disabled", true);
                $("#colAccount :input").attr("disabled", true);
            }
            function enableAll(){
                $("#colApp :input").attr("disabled", false);
                $("#colUser :input").attr("disabled", false);
                $("#colEmail :input").attr("disabled", false);
                $("#colOthers :input").attr("disabled", false);
                $("#colAccount :input").attr("disabled", false);
            }
            function refresh(){
                disableAll();
                var val1 = $('#selectTemplate option:selected').val();
                if(val1 == "Install"){
                    $("#colApp :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                }else if(val1 == "Create"){
                    $("#colUser :input").attr("disabled", false);
                    $("#colAccount :input").attr("disabled", false);
                    $("#chkdeleteUserAccount").attr("disabled", true);
                    $("#chkresetPassword").attr("disabled", true);
                    $("#Deskripsi").focus();
                }else if(val1 == "Delete"){
                    $("#colUser :input").attr("disabled", false);
                    $("#colAccount :input").attr("disabled", false);
                    $("#chkcreateUserAccount").attr("disabled", true);
                    $("#chkresetPassword").attr("disabled", true);
                    $("#Deskripsi").focus();
                }else if(val1 == "Reset"){
                    $("#colUser :input").attr("disabled", false);
                    $("#colAccount :input").attr("disabled", false);
                    $("#chkdeleteUserAccount").attr("disabled", true);
                    $("#chkcreateUserAccount").attr("disabled", true);
                    $("#Deskripsi").focus();
                }else if(val1 == "File Sharing"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkIT").attr("disabled", true);
                    $("#chkInternet").attr("disabled", true);
                    $("#chkBackup").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "IT Device"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkFile").attr("disabled", true);
                    $("#chkInternet").attr("disabled", true);
                    $("#chkBackup").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "Internet"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkFile").attr("disabled", true);
                    $("#chkIT").attr("disabled", true);
                    $("#chkBackup").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "Backup"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                    $("#chkSoftware").attr("disabled", true);
                    $("#chkFile").attr("disabled", true);
                    $("#chkIT").attr("disabled", true);
                    $("#chkInternet").attr("disabled", true);
                    $("#chkUpload").attr("disabled", true);
                }else if(val1 == "Email"){
                    $("#colEmail :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                }else if(val1 == "Lainnya"){
                    $("#colOthers :input").attr("disabled", false);
                    $("#Deskripsi").focus();
                }else if(val1 == "Custom"){
                    enableAll();
                    $("#Deskripsi").focus();
                }else{
                    disableAll();
                }
            }
            
            $('#chkPKL').click(function() {
                $("#namaPKL").attr("disabled", !this.checked);
                $("#namaPKL").val("");
            });
            
            $("#rbNoAssistant").click(function(){
                $("#npkURFUser").attr("disabled", this.checked);
                $("#npkURFUser").val("");
            });
            $("#rbYesAssistant").click(function(){
                $("#npkURFUser").attr("disabled", !this.checked);
                $("#npkURFUser").val("");
            });
            refresh();
        });
	</script>
    
    <script type="text/javascript">
        function SubmitUpdateURF(URF_ID) {
            var selectTemplate = document.getElementById("selectTemplate").value;
            var tempDate = document.getElementById("tempDate").value;
            var flag = true;
            var rbPermanent = document.getElementById('rbPermanent').checked;
            var rbTemporary = document.getElementById('rbTemporary').checked;
            var rbNoAssistant = document.getElementById('rbNoAssistant').checked;
            var rbYesAssistant = document.getElementById('rbYesAssistant').checked;
            <?php 
                if($departemen[0]->departemen == 'HRGA'){
            ?>
                var chkPKL = document.getElementById('chkPKL').checked;
                var namaPKL = document.getElementById("namaPKL").value;
            <?php
                }else{
            ?>
                var chkPKL = false;
                var namaPKL = '';
            <?php
                }
            ?>  
            var deskripsi = document.getElementById('Deskripsi').value;
            
            var chkAppSiDP = document.getElementById("chkAppSiDP").checked;
            var chkAppsiDAPEN1 = document.getElementById("chkAppsiDAPEN1").checked;
            var chkAppsiDAPEN2 = document.getElementById("chkAppsiDAPEN2").checked;
            var chkAppApisoft = document.getElementById("chkAppApisoft").checked;
            var chkAppCORE = document.getElementById("chkAppCORE").checked;
            var chkAppAccpac = document.getElementById("chkAppAccpac").checked;
            var chkAppSIAP = document.getElementById("chkAppSIAP").checked;
            var chkAppCARE = document.getElementById("chkAppCARE").checked;
            var chkAppOther = document.getElementById("chkAppOther").checked;
            var chkuserIDWindows = document.getElementById("chkuserIDWindows").checked;
            var chkuserIDSiDP = document.getElementById("chkuserIDSiDP").checked;
            var chkuserIDsiDAPEN1 = document.getElementById("chkuserIDsiDAPEN1").checked;
            var chkuserIDsiDAPEN2 = document.getElementById("chkuserIDsiDAPEN2").checked;
            var chkuserIDApisoft = document.getElementById("chkuserIDApisoft").checked;
            var chkuserIDCORE = document.getElementById("chkuserIDCORE").checked;
            var chkuserIDAccpac = document.getElementById("chkuserIDAccpac").checked;
            var chkuserIDSIAP = document.getElementById("chkuserIDSIAP").checked;
            var chkuserIDCARE = document.getElementById("chkuserIDCARE").checked;
            var chkuserIDOther = document.getElementById("chkuserIDOther").checked;
            
            var chkemail_aiastracoid = document.getElementById("chkemail_aiastracoid").checked;
            var chkemail_dapenastracom = document.getElementById("chkemail_dapenastracom").checked;
            var chkemail_dpacoid = document.getElementById("chkemail_dpacoid").checked;
            var chkcreateUserAccount = document.getElementById("chkcreateUserAccount").checked;
            var chkdeleteUserAccount = document.getElementById("chkdeleteUserAccount").checked;
            var chkresetPassword = document.getElementById("chkresetPassword").checked;
            var chkFile = document.getElementById("chkFile").checked;
            var chkIT = document.getElementById("chkIT").checked;
            var chkInternet = document.getElementById("chkInternet").checked;
            var chkBackup = document.getElementById("chkBackup").checked;
            
            if(!rbPermanent && !rbTemporary){
                alert('Mohon isi Durasi');
                flag = false;
            }else if(deskripsi == ""){
                alert('Mohon isi Deskripsi singkat mengenai URF ini');
                flag = false;
            }else if(rbTemporary && (tempDate == "")){
                alert('Mohon isi Expired Period Date');
                flag = false;
            }
            else if(selectTemplate == ""){
                alert('Mohon pilih template yang tersedia, jika permintaan khusus, pilih Custom');
                flag = false;
            }else if(chkcreateUserAccount && chkdeleteUserAccount){ 
                alert('Mohon pilih salah satu, apakah ingin menghapus, atau mendelete user ID');
                flag = false;
            }
            else if(selectTemplate == "Install"){
                if( !chkAppSiDP && !chkAppsiDAPEN1 && !chkAppsiDAPEN2 && !chkAppApisoft && !chkAppCORE && !chkAppAccpac && !chkAppSIAP && !chkAppCARE && !chkAppOther){
                    alert('Mohon pilih salah satu aplikasi yang ingin di install, jika aplikasi khusus, pilih template Custom');
                    flag = false;
                }
            }
            else if(selectTemplate == "Create"){
                if (!chkcreateUserAccount){
                    alert('Mohon checklist Create User ID di centang');
                    flag = false;
                }else{
                    if( !chkuserIDWindows && !chkuserIDSiDP && !chkuserIDsiDAPEN1 && !chkuserIDsiDAPEN2 && !chkuserIDApisoft && !chkuserIDCORE && !chkuserIDAccpac && !chkuserIDSIAP && !chkuserIDCARE && !chkuserIDOther){
                        alert('Mohon pilih salah satu user aplikasi yang ingin dibuat, jika aplikasi khusus, pilih template Custom');
                        flag = false;
                    }
                }
            }
            else if(selectTemplate == "Delete"){
                if (!chkdeleteUserAccount){
                    alert('Mohon checklist Delete user ID di centang');
                    flag = false;
                }else{
                    if( !chkuserIDWindows && !chkuserIDSiDP && !chkuserIDsiDAPEN1 && !chkuserIDsiDAPEN2 && !chkuserIDApisoft && !chkuserIDCORE && !chkuserIDAccpac && !chkuserIDSIAP && !chkuserIDCARE && !chkuserIDOther){
                        alert('Mohon pilih salah satu user aplikasi yang ingin dihapus, jika aplikasi khusus, pilih template Custom');
                        flag = false;
                    }
                }
            }
            else if(selectTemplate == "Reset"){
                if (!chkresetPassword){
                    alert('Mohon checklist Reset user ID di centang');
                    flag = false;
                }else{
                    if( !chkuserIDWindows && !chkuserIDSiDP && !chkuserIDsiDAPEN1 && !chkuserIDsiDAPEN2 && !chkuserIDApisoft && !chkuserIDCORE && !chkuserIDAccpac && !chkuserIDSIAP && !chkuserIDCARE && !chkuserIDOther){
                        alert('Mohon pilih salah satu user aplikasi yang ingin di reset / lupa password, jika aplikasi khusus, pilih template Custom');
                        flag = false;
                    }
                }
            }
            else if(selectTemplate == "File Sharing"){
                if (!chkFile){
                    alert('Mohon checklist File Sharing di centang dan jelaskan di Deskripsi');
                    flag = false;
                }
            }
            else if(selectTemplate == "IT Device"){
                if (!chkIT){
                    alert('Mohon checklist IT Device di centang dan jelaskan di Deskripsi');
                    flag = false;
                }
            }
            else if(selectTemplate == "Internet"){
                if (!chkInternet){
                    alert('Mohon checklist Internet di centang dan jelaskan di Deskripsi');
                    flag = false;
                }
            }
            else if(selectTemplate == "Backup"){
                if (!chkBackup){
                    alert('Mohon checklist Internet di centang dan jelaskan di Deskripsi');
                    flag = false;
                }
            }
            else if(selectTemplate == "Email"){
                if(!chkemail_aiastracoid && !chkemail_dapenastracom && !chkemail_dpacoid){
                    alert('Mohon pilih salah satu email yang di inginkan, jika email khusus, pilih template Custom');
                    flag = false;
                }
            }
            else if(selectTemplate == "Lainnya"){
            }
            else if(selectTemplate == "Custom"){
            }
            if(!rbNoAssistant && !rbYesAssistant){
                alert('Mohon isi Durasi');
                flag = false;
            }else 
            if(rbYesAssistant && (npkURFUser == "") && !chkPKL){
                alert('Mohon isi NPK User yang ingin dibuatkan URF nya');
                flag = false;
            }else 
            if(rbYesAssistant && chkPKL){
                alert('Mohon pilih salah satu, untuk PKL atau karyawan tetap lainnya');
                flag = false;
            }else 
            if(chkPKL){
                if(namaPKL == ''){
                    alert('Mohon isi Nama User PKL yang ingin dibuatkan URF nya');
                    flag = false;
                }
            }
            if(flag){
                var x = window.confirm("Update URF ini?");
                if(x){
                    $("#UpdateURF").attr("action"); //will retrieve it
                    $("#UpdateURF").attr("target", "_blank"); //will retrieve it
                    $("#UpdateURF").attr("action", "<?php echo site_url('URFbySystem/URFbySystemController/UpdateURF');?>/"+URF_ID);
                }else{
                    event.preventDefault();
                }
            }else{
                event.preventDefault();
            }
        }
        function BatalUpdateURF(){
            var x = window.confirm("Batalkan Update URF ini?");
            if(x){
                $("#UpdateURF").attr("action"); //will retrieve it
                $("#UpdateURF").attr("target", "_self"); //will retrieve it
                $("#UpdateURF").attr("action", "<?php echo site_url('URFbySystem/URFbySystemController/ViewURFUser');?>");
            }else{
                event.preventDefault();
            }
        }
    </script>
    <style>
    #error{
        color: red;
    }
    </style>
	<head>
		<title>Create URF</title>
	</head>
    <body>
        <?php echo $this->session->flashdata('message'); ?>
        
        <?php foreach($data as $data){ ?>
        <div class="container" style="width:80%">
            <h2>Ubah data URF</h2>
            <form method="POST" name="UpdateURF" id="UpdateURF">
                <div class="form-group">
                    <div class = "row">
                        <div class ="col">
                            <label for="sel1">Saya mau request</label>
                            <select class="form-control" id="selectTemplate" name="selectTemplate">
                                <option value="">--Pilih Template Request--</option>
                                <option <?php if($data->Template == "Install") { ?> selected = "Selected" <?php } ?> value="Install">Install Aplikasi</option>
                                <option <?php if($data->Template == "Create") { ?> selected = "Selected" <?php } ?> value="Create">Create User Aplikasi</option>
                                <option <?php if($data->Template == "Delete") { ?> selected = "Selected" <?php } ?> value="Delete">Delete User Aplikasi</option>
                                <option <?php if($data->Template == "Reset") { ?> selected = "Selected" <?php } ?> value="Reset">Reset / Lupa Password</option>
                                <option <?php if($data->Template == "File Sharing") { ?> selected = "Selected" <?php } ?> value="File Sharing">File Sharing</option>
                                <option <?php if($data->Template == "IT Device") { ?> selected = "Selected" <?php } ?> value="IT Device">IT Device</option>
                                <option <?php if($data->Template == "Internet") { ?> selected = "Selected" <?php } ?> value="Internet">Internet</option>
                                <option <?php if($data->Template == "Backup") { ?> selected = "Selected" <?php } ?> value="Backup">Backup / Restore Database</option>
                                <option <?php if($data->Template == "Email") { ?> selected = "Selected" <?php } ?> value="Email">Email</option>
                                <option <?php if($data->Template == "Lainnya") { ?> selected = "Selected" <?php } ?> value="Lainnya">Lainnya</option>
                                <option <?php if($data->Template == "Custom") { ?> selected = "Selected" <?php } ?> value="Custom">Custom</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="radio">
                                <label><input type="radio" id="rbNew" name="rdbNewUpdate" value="New" <?php if($data->NewOrUpdate == "New") { echo 'checked'; } ?> >New</label>
                                <label><input type="radio" id="rbUpdate" name="rdbNewUpdate" value="Update" <?php if($data->NewOrUpdate == "Update") { echo 'checked'; } ?> >Update</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                        <label for="Lainnya">Deskripsi</label>
                        <textarea class="form-control" rows="2" id="Deskripsi" name="Deskripsi" style="resize:none;"><?php if($data->Deskripsi != "") { echo $data->Deskripsi; } ?></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label for="Period">Expired Period</label>
                            <div class="radio">
                                <label><input type="radio" id="rbPermanent" name="rdbPeriod" value="Permanent" <?php if($data->ExpiredPeriod == "0000-00-00") { echo 'checked'; } ?> >Permanent</label>
                                <label><input type="radio" id="rbTemporary" name="rdbPeriod" value="Temporary" <?php if($data->ExpiredPeriod != "0000-00-00") { echo 'checked'; } ?>  >Temporary Until</label>
                                <input type="date" class="form-control" id="tempDate" name="tempDate" min = <?php echo date("Y-m-d"); ?> style="width:20%" value = "<?php if($data->ExpiredPeriod != "0000-00-00") { echo $data->ExpiredPeriod; } ?>">
                                <?php if($data->ExpiredPeriod != "0000-00-00"){
                                    echo '<script>$(function() { $("#tempDate").attr("disabled", false)});</script>';
                                }?>
                            </div>
                        </div>
                    </div>
                    <!-- 
                    <div class="row">
                        <div class="col">
                        </div>
                    </div>
                    -->
                    <div class="row">
                        <!-- Application -->
                        <div class="col-sm-2" id="colApp">
                            <label for="chk1">Aplikasi</label>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppSiDP" name="chkAppSiDP" value="chkSiDP" <?php if(strpos($data->AksesAplikasi, 'chkSiDP') !== false){echo "checked";} ?>>SiDP</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppsiDAPEN1" name="chkAppsiDAPEN1" value="chksiDAPEN1" <?php if(strpos($data->AksesAplikasi, 'chksiDAPEN1') !== false){echo "checked";} ?>>siDAPEN1</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppsiDAPEN2" name="chkAppsiDAPEN2" value="chksiDAPEN2" <?php if(strpos($data->AksesAplikasi, 'chksiDAPEN2') !== false){echo "checked";} ?>>siDAPEN2</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppApisoft" name="chkAppApisoft" value="chkApisoft" <?php if(strpos($data->AksesAplikasi, 'chkApisoft') !== false){echo "checked";} ?>>Apisoft</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppCORE" name="chkAppCORE" value="chkCORE" <?php if(strpos($data->AksesAplikasi, 'chkCORE') !== false){echo "checked";} ?>>CORE</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppAccpac" name="chkAppAccpac" value="chkAccpac" <?php if(strpos($data->AksesAplikasi, 'chkAccpac') !== false){echo "checked";} ?>>Accpac</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppSIAP" name="chkAppSIAP" value="chkSIAP" <?php if(strpos($data->AksesAplikasi, 'chkSIAP') !== false){echo "checked";} ?>>SIAP</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppCARE" name="chkAppCARE" value="chkCARE" <?php if(strpos($data->AksesAplikasi, 'chkCARE') !== false){echo "checked";} ?>>CARE</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkAppOther" name="chkAppOther" value="chkAppOther" <?php if(strpos($data->AksesAplikasi, 'chkOther') !== false){echo "checked";} ?>>Other</label></div>
                        </div>
                        <!-- User ID -->
                        <div class="col-sm-2" id="colUser">
                            <label for="chk1">User ID Aplikasi</label>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDWindows" name="chkuserIDWindows" value="userIDWindows" <?php if(strpos($data->UserAplikasi, 'userIDWindows') !== false ){ echo "checked"; } ?>>Windows</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDSiDP" name="chkuserIDSiDP" value="userIDSiDP" <?php if(strpos($data->UserAplikasi, 'userIDSiDP') !== false ){ echo "checked"; } ?>>SiDP</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDsiDAPEN1" name="chkuserIDsiDAPEN1" value="userIDsiDAPEN1" <?php if(strpos($data->UserAplikasi, 'userIDsiDAPEN1') !== false ){ echo "checked"; } ?>>siDAPEN1</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDsiDAPEN2" name="chkuserIDsiDAPEN2" value="userIDsiDAPEN2" <?php if(strpos($data->UserAplikasi, 'userIDsiDAPEN2') !== false ){ echo "checked"; } ?>>siDAPEN2</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDApisoft" name="chkuserIDApisoft" value="userIDApisoft" <?php if(strpos($data->UserAplikasi, 'userIDApisoft') !== false ){ echo "checked"; } ?>>Apisoft</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDCORE" name="chkuserIDCORE" value="userIDCORE" <?php if(strpos($data->UserAplikasi, 'userIDCORE') !== false ){ echo "checked"; } ?>>CORE</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDAccpac" name="chkuserIDAccpac" value="userIDAccpac" <?php if(strpos($data->UserAplikasi, 'userIDAccpac') !== false ){ echo "checked"; } ?>>Accpac</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDSIAP" name="chkuserIDSIAP" value="userIDSIAP" <?php if(strpos($data->UserAplikasi, 'userIDSIAP') !== false ){ echo "checked"; } ?>>SIAP</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDCARE" name="chkuserIDCARE" value="userIDCARE" <?php if(strpos($data->UserAplikasi, 'userIDCARE') !== false ){ echo "checked"; } ?>>CARE</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkuserIDOther" name="chkuserIDOther" value="userIDOther" <?php if(strpos($data->UserAplikasi, 'userIDOther') !== false ){ echo "checked"; } ?>>Other</label></div>
                        </div>
                        <div class="col-sm-2" id="colAccount">
                            <label for="chk1"></label>
                            <div class="checkbox"><label><input type="checkbox" id="chkcreateUserAccount" name="chkcreateUserAccount" value="createUserAccount" <?php if(strpos($data->UserID, 'createUserAccount') !== false ){ echo "checked"; } ?> >Create User ID</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkdeleteUserAccount" name="chkdeleteUserAccount" value="deleteUserAccount" <?php if(strpos($data->UserID, 'deleteUserAccount') !== false ){ echo "checked"; } ?> >Delete User ID</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkresetPassword" name="chkresetPassword" value="resetPassword" <?php if(strpos($data->UserID, 'resetPassword') !== false ){ echo "checked"; } ?> >Reset/Lupa Password</label></div>
                        </div>
                        <!-- Email-->
                        <div class="col-sm-2" id="colEmail">
                            <label for="chk1">Email</label>
                            <div class="checkbox"><label><input type="checkbox" id="chkemail_aiastracoid" name="chkemail_aiastracoid" value="email_aiastracoid" <?php if(strpos($data->Email, 'email_aiastracoid') !== false ){ echo "checked"; } ?>>@ai.astra.co.id</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkemail_dapenastracom" name="chkemail_dapenastracom" value="email_dapenastracom" <?php if(strpos($data->Email, 'email_dapenastracom') !== false ){ echo "checked"; } ?>>@dapenastra.com</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkemail_dpacoid" name="chkemail_dpacoid" value="email_dpacoid" <?php if(strpos($data->Email, 'email_dpacoid') !== false ){ echo "checked"; } ?>>@dpa.co.id</label></div>
                        </div>
                        <!-- Others-->
                        <div class="col-sm-2" id="colOthers">
                            <label for="chk1">Lainnya</label>
                            <div class="checkbox"><label><input type="checkbox" id="chkSoftware" name="chkSoftware" value="Software License" <?php if(strpos($data->ItemRequest, 'Software' ) !== false) { echo "checked"; } ?> >Software License</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkFile" name="chkFile" value="File Sharing" <?php if(strpos($data->ItemRequest, 'File' ) !== false) { echo "checked"; } ?> >File Sharing</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkIT" name="chkIT" value="IT Device" <?php if(strpos($data->ItemRequest, 'IT' ) !== false) { echo "checked"; } ?> >IT Device</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkInternet" name="chkInternet" value="Internet" <?php if(strpos($data->ItemRequest, 'Internet' ) !== false) { echo "checked"; } ?> >Internet</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkBackup" name="chkBackup" value="Backup" <?php if(strpos($data->ItemRequest, 'Backup' ) !== false) { echo "checked"; } ?> >Backup / Restore</label></div>
                            <div class="checkbox"><label><input type="checkbox" id="chkUpload" name="chkUpload" value="Upload" <?php if(strpos($data->ItemRequest, 'Upload' ) !== false) { echo "checked"; } ?> >Upload</label></div>
                        </div>
                    </div>
                    <!-- <div class = "row">
                        <div class ="col">
                            <label for="sel1">Ada Lampiran</label>
                            <select class="form-control" id="selectLampiran" name="selectLampiran">
                                <option value="0">Tidak ada</option>
                                <option value="1">Ada</option>
                            </select>
                        </div>
                    </div> -->
                    <div class = "row">
                        <div class ="col-sm-6">
                            <label for="Period">Request untuk orang lain</label>
                            <div class="radio">
                                <label><input type="radio" id="rbNoAssistant" name="rdbAssistant" value="No" <?php if($data->NPK_User == $data->CreatedBy or $data->NamaPKL != "" ) { echo "checked"; } ?> >Tidak</label>
                                <label><input type="radio" id="rbYesAssistant" name="rdbAssistant" value="Yes" <?php if($data->NPK_User != $data->CreatedBy and $data->NamaPKL == "" ) { echo "checked"; } ?> >Ya</label>
                                <input type="text" class="form-control" id="npkURFUser" name="npkURFUser" style="width:40%" placeholder="Masukkan NPK User tersebut" value="<?php if($data->NPK_User != $data->CreatedBy and $data->NamaPKL == ""  ) { echo $data->NPK_User; } ?>">
                                <?php  if($data->NPK_User != $data->CreatedBy and $data->NamaPKL == "" ) {
                                    echo '<script>$(function() { $("#npkURFUser").attr("disabled", false)});</script>';
                                }?>
                            </div>
                            
                        </div>
                        <?php 
                            if($departemen[0]->departemen == 'HRGA'){
                        ?>
                        <div class ="col-sm-6">
                            <label for="Period">Request untuk PKL</label>
                            <div class="radio">
                                <label><input type="checkbox" id="chkPKL" name="chkPKL" value="PKL" <?php if($data->NamaPKL != "" ) { echo "checked"; } ?>>PKL</label>   
                                <input type="text" class="form-control" id="namaPKL" name="namaPKL" style="width:100%" placeholder="Masukkan Nama PKL tersebut" value="<?php if($data->NamaPKL != "" ) { echo $data->NamaPKL; } ?>">
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                    </div>
                    <div class="row">
                        <div class="col"><br/>
                            <input type="submit" class="btn btn-success" onclick = "SubmitUpdateURF('<?php echo $data->URF_ID?>')" value="Print and Submit">
                            <input type="submit" class="btn btn-danger" onclick = "BatalUpdateURF()" value="Batal Ubah">
                        </div>
                    </div>
        <?php } ?>
                </div>
            <!-- </form>  -->
        </div>
        
	</body>
</html> 