<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title><?php $title ?></title>
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">URF Semua User</h3>
		</div>
		
        <?php echo $this->session->flashdata('message'); ?>
		<div class="panel-body">
			<div class="col-sm-2"><label for="sel1">Search By Status </label></div>
			<div class="col-sm-8">
				<select class="form-control" id="selectStatus" name="selectStatus">
					<option <?php if($status == "NeedAction") { ?> selected = "Selected" <?php } ?>value="NeedAction">Butuh Tindakan</option>
					<option <?php if($status == "All") { ?> selected = "Selected" <?php } ?>value="All">All</option>
					<option <?php if($status == "NotReceived") { ?> selected = "Selected" <?php } ?>value="NotReceived">Belum Diterima IT</option>
					<option <?php if($status == "OnReview") { ?> selected = "Selected" <?php } ?>value="OnReview">Dalam Proses Review IT</option>
					<option <?php if($status == "Waiting") { ?> selected = "Selected" <?php } ?>value="Waiting">Menunggu pengerjaan oleh IT</option>
					<option <?php if($status == "Declined") { ?> selected = "Selected" <?php } ?>value="Declined">URF Tidak disetujui</option>
					<option <?php if($status == "Resolved") { ?> selected = "Selected" <?php } ?>value="Resolved">Sudah dikerjakan IT</option>
					<option <?php if($status == "NeedRevert") { ?> selected = "Selected" <?php } ?>value="NeedRevert">Sudah dikerjakan IT dan akan dikembalikan pada waktunya</option>
				</select>
			</div>
			<div class="col-sm-2">
				<button class="btn btn-primary" onclick="search()"><i class="glyphicon glyphicon-search"></i> Search</button>
			</div>
			<br/><br/>
			<table class="table">
				<thead>
				<tr>
					<th>Nomor</th>
					<th>NPK</th>
					<th>Nama</th>
					<th>Status</th>
					<th>Deskripsi</th>
					<th>Durasi</th>
					<th>Tindakan</th>
				</tr>
				</thead>
				<tbody>
				<?php
					foreach($data as $value){
						$class = "";
						$status = "";
						$receiveAble = true;
						$rejectAble = true;
						$approveAble = false;
						$declineAble = true;
						$resolveAble = false;
						$revertAble = false;
						if($value->StatusReceived == 1){
							$class = "active";
							$status = "Diterima IT";
							$receiveAble = false;
							$rejectAble = false;
							$approveAble = true;
							$resolveAble = false;
							if($value->StatusApproved == 1){
								$class = "success";
								$status = "Sudah di approve IT";
								$approveAble = false;
								$resolveAble = true;
								$declineAble = false;
								if($value->StatusResolved == 2){
									$class = "success";
									$status = "Sudah dikerjakan IT";
									$resolveAble = false;
								}else if($value->StatusResolved == 1){
									$class = "success";
									$status = "Sudah dikerjakan IT dan akan dikembalikan pada waktunya";
									$resolveAble = false;
									$revertAble = true;
								}else{
									$status = "Menunggu pengerjaan oleh IT";
									$resolveAble = true;
								}
							}else if($value->StatusApproved == 2){
								$status = "URF Tidak disetujui";
								$rejectAble = false;
								$approveAble = false;
							}else{
								$status = "Dalam proses review IT";
							}
						}else if($value->StatusReceived == 2){
							$class = "danger";
							$status = "URF Ditolak oleh IT";
							$receiveAble = false;
							$rejectAble = false;
							$approveAble = false;
							$declineAble = true;
							$resolveAble = false;
							$revertAble = false;
						}else{
							$class = "info";
							$status = "Belum diterima IT";
						}
						$now = new DateTime(date('Y-m-d h:i:s'));
						$expiredPeriod = new DateTime($value->ExpiredPeriod);
						$interval = $now->diff($expiredPeriod);
						$sisaWaktu = $interval->format('%R%a hari');
						$flagExp = false;
						if($expiredPeriod<=$now){
							$flagExp = true;
						}
						$permanent = true;
						if($value->StatusResolved != 2 && $value->ExpiredPeriod != '0000-00-00' && ($value->ExpiredPeriod)!= NULL && $value->ExpiredPeriod > date('Y-m-d h:i:s') && $sisaWaktu < 8){
							$class = "warning";
							$status = "Akses Temporary Kurang dari 1 minggu dan akan dikembalikan segera";
						}if($value->ExpiredPeriod != '0000-00-00' and $value->ExpiredPeriod != NULL){
							$permanent = false;
						}
						echo '<tr class ="'.$class.'">';
						echo '<td>'.$value->URF_Number.'</td>';
						echo '<td>'.$value->NPK_User.'</td>';
						echo '<td>'.$value->Nama.'</td>';
						echo '<td>'.$status.'</td>';
						echo '<td>'.$value->Deskripsi.'</td>';
						if($permanent){
							echo '<td>Permanent</td>';
						}else{
							if($flagExp){
								echo '<td><span style="color:red;">'.$sisaWaktu.', '.$value->ExpiredPeriodFmt.'</span></td>';
							}else{
								echo '<td>'.$sisaWaktu.', '.$value->ExpiredPeriodFmt.'</td>';
							}
						}
						echo '<td>';
						
						if($receiveAble){
						?>
								<button class="btn btn-info" onclick="receive('<?php echo $value->URF_ID; ?>')"><i class="glyphicon glyphicon-chevron-down"></i> Receive</button>
						<?php
						}
						if($rejectAble){
						?>
								<button class="btn btn-warning" onclick="reject('<?php echo $value->URF_ID; ?>')"><i class="glyphicon glyphicon-remove"></i> Reject</button>
						<?php
						}
						if($approveAble){
						?>
								<button class="btn btn-primary" onclick="approve('<?php echo $value->URF_ID; ?>')"><i class="glyphicon glyphicon-ok"></i> Approve</button>
						<?php
						}
						if($approveAble){
						?>
								<button class="btn btn-danger" onclick="decline('<?php echo $value->URF_ID; ?>')"><i class="glyphicon glyphicon-remove"></i> Decline</button>
						<?php
						}
						if($resolveAble){
						?>
								<button class="btn btn-success" onclick="resolve('<?php echo $value->URF_ID; ?>')"><i class="glyphicon glyphicon-wrench"></i> Resolve</button>
						<?php
						}
						if($revertAble){
						?>
								<button class="btn btn-info" onclick="revert('<?php echo $value->URF_ID; ?>')"><i class="glyphicon glyphicon-retweet"></i> Revert</button>
						<?php
						}
						echo '</td>';
						?>
						</tr>
				<?php
					}
				?>
				</tbody>
			</table>
		</div>
	</body>
	<script type="text/javascript">
		function search(){
			var Filter = document.getElementById('selectStatus').value;
			window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/ViewURFAdmin');?>/"+Filter;
		}
		function receive(URF_ID){
			var x = window.confirm('Apakah anda yakin URF sudah diterima?');
			if(x){
				window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/Receive');?>/"+URF_ID;
			}else{
				event.preventDefault();
			}
		}
		function reject(URF_ID){
			var x = window.confirm('Apakah anda yakin URF ini ditolak?');
			if(x){
				window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/Reject');?>/"+URF_ID;
			}else{
				event.preventDefault();
			}
		}
		function approve(URF_ID){
			var x = window.confirm('Apakah anda yakin URF sudah disetujui?');
			if(x){
				window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/Approve');?>/"+URF_ID;
			}else{
				event.preventDefault();
			}
		}
		function decline(URF_ID){
			var x = window.confirm('Apakah anda yakin URF tidak disetujui?');
			if(x){
				window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/Decline');?>/"+URF_ID;
			}else{
				event.preventDefault();
			}
		}
		function resolve(URF_ID){
			var x = window.confirm('Apakah anda yakin URF sudah dikerjakan?');
			if(x){
				window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/Resolve');?>/"+URF_ID;
			}else{
				event.preventDefault();
			}
		}
		function revert(URF_ID){
			var x = window.confirm('Apakah anda yakin Akses URF sudah dikembalikan?');
			if(x){
				window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/Revert');?>/"+URF_ID;
			}else{
				event.preventDefault();
			}
		}
	</script>
</html>
