<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title><?php $title ?></title>
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">URF User</h3>
		</div>
		<div class="panel-body">
			<table class="table">
				<thead>
				<tr>
					<th>Nomor</th>
					<th>Status</th>
					<th>Template</th>
					<th>Deskripsi</th>
					<th>Durasi</th>
					<th>Tindakan</th>
				</tr>
				</thead>
				<tbody>
				<?php
					foreach($data as $value){
						$class = "";
						$status = "";
						$ubahAble = true;
						$batalAble = true;
						$renewAble = false;
						if($value->StatusReceived == 1){
							$class = "active";
							$status = "Diterima IT";
							$ubahAble = false;
							$batalAble = false;
							if($value->StatusApproved == 1){
								$class = "success";
								$status = "Sudah di approve IT";
								$ubahAble = false;
								$batalAble = false;
								if($value->StatusResolved == 2){
									$class = "success";
									$status = "Sudah dikerjakan IT";
									$ubahAble = false;
									$batalAble = false;
								}else if($value->StatusResolved == 1){
									$class = "success";
									$status = "Sudah dikerjakan IT akan dikembalikan pada waktunya";
									$ubahAble = false;
									$batalAble = false;
								}else{
									$status = "Menunggu pengerjaan oleh IT";
									$ubahAble = false;
									$batalAble = false;
								}
							}else if($value->StatusApproved == 2){
								$status = "URF Tidak disetujui";
								$rejectAble = false;
								$approveAble = false;
							}else{
								$status = "Dalam proses review IT";
								$ubahAble = true;
								$batalAble = true;
							}
						}else if($value->StatusReceived == 2){
							$class = "danger";
							$status = "URF Ditolak oleh IT";
							$ubahAble = false;
							$batalAble = false;
						}else{
							$class = "info";
							$status = "Belum diterima IT";
							$ubahAble = true;
							$batalAble = true;
						}
						$now = new DateTime(date('Y-m-d h:i:s'));
						$expiredPeriod = new DateTime($value->ExpiredPeriod);
						$interval = $now->diff($expiredPeriod);
						$sisaWaktu = $interval->format('%R%a hari');
						$flagExp = false;
						if($expiredPeriod<=$now){
							$flagExp = true;
						}
						$permanent = true;
						if($value->StatusResolved != 2 && $value->ExpiredPeriod != '0000-00-00' && ($value->ExpiredPeriod)!= NULL && $value->ExpiredPeriod > date('Y-m-d h:i:s') && $sisaWaktu < 8){
							$class = "warning";
							$status = "Akses Temporary Kurang dari 1 minggu dan akan dikembalikan segera";
							$renewAble = true;
						}
						if($value->ExpiredPeriod != '0000-00-00' && ($value->ExpiredPeriod)!= NULL){
							$permanent = false;
						}
						echo '<tr class ="'.$class.'">';
						echo '<td>'.$value->URF_Number.'</td>';
						echo '<td>'.$status.'</td>';
						echo '<td>'.$value->Template.'</td>';
						echo '<td>'.$value->Deskripsi.'</td>';
						if($permanent){
							echo '<td>Permanent</td>';
						}else{
							if($flagExp && $value->StatusResolved != 2){
								echo '<td><span style="color:red;">'.$sisaWaktu.', '.$value->ExpiredPeriodFmt.'</span></td>';
							}else if($flagExp && $value->StatusResolved == 2){
								echo '<td>Done</td>';
							}
							else{
								echo '<td>'.$sisaWaktu.', '.$value->ExpiredPeriodFmt.'</td>';
							}
						}
						echo '<td>';
						if($ubahAble){
						?>
								<button class="btn btn-warning" onclick="ubah('<?php echo $value->URF_ID; ?>')"><i class="glyphicon glyphicon-pencil"></i> Ubah</button>
						<?php
						}
						if($batalAble){
						?>
								<button class="btn btn-danger" onclick="batal('<?php echo $value->URF_ID; ?>')"><i class="glyphicon glyphicon-remove"></i> Batal</button>
						<?php
						}
						if($renewAble){
						?>
								<!-- <button class="btn btn-primary" onclick="renew('<?php echo $value->URF_ID; ?>')"><i class="glyphicon glyphicon-ok-circle"></i> Recreate URF</button> -->
						<?php
						}
						echo '</td>';
						?>
						</tr>
				<?php
					}
				?>
				</tbody>
			</table>
		</div>
	</body>
	<script type="text/javascript">
		function ubah(URF_ID){
			window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/Ubah');?>/"+URF_ID;
		}
		function renew(URF_ID){
			window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/Renew');?>/"+URF_ID;
		}
		function batal(URF_ID){
			var x = window.confirm('Apakah anda yakin ingin membatalkan URF ini?');
			if(x){
				window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/Batal');?>/"+URF_ID;
			}else{
				event.preventDefault();
			}
		}
	</script>
</html>
