<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
    <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
    
    <script type="text/javascript">
        function SubmitUploadPPB(ID) {
            var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png", ".pdf"];
            var flag = true;
            var arrInputs = UploadPPB.getElementsByTagName("input");
            var countFile = 0;
            for (var i = 0; i < arrInputs.length; i++) {
                var HtmlInput = arrInputs[i];
                if (HtmlInput.type == "file") {
                    var sFileName = HtmlInput.value;
                    if (sFileName.length > 0) {
                        countFile++;
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                            flag = false;
                        }
                    }
                }
            }
            if(countFile==0){
                alert("Tidak ada file yang diupload?");
                flag = false;
            }
            if(flag){
                var x = window.confirm("Upload PPB?");
                if(x){
                    $("#UploadPPB").attr("action"); //will retrieve it
                    $("#UploadPPB").attr("target", "_self"); //will retrieve it
                    $("#UploadPPB").attr("action", "<?php echo site_url('PersetujuanPengeluaranBudget/PPBController/UploadPPB');?>/"+ID);
                }else{
                    event.preventDefault();
                }
            }else{
                event.preventDefault();
            }
        }
        function BatalUploadPPB(){
            var x = window.confirm("Batalkan Upload Scan Dokumen Persetujuan Pengeluaran Budget ini?");
            if(x){
                $("#UploadPPB").attr("action"); //will retrieve it
                $("#UploadPPB").attr("target", "_self"); //will retrieve it
                $("#UploadPPB").attr("action", "<?php echo site_url('PersetujuanPengeluaranBudget/PPBController/ListPP');?>");
            }else{
                event.preventDefault();
            }
        }
    </script>
    <style>
    #error{
        color: red;
    }
    </style>
	<head>
		<title>Upload Scan Dokumen</title>
	</head>
    <body>
        <?php echo $this->session->flashdata('message'); ?>
        
        <?php if(isset($error)) VAR_DUMP($error);?>
        <?php foreach($data as $data){ ?>
        <div class="container" style="width:80%">
            <h2>Upload Scan Dokumen <?php echo ($data->Tipe == 'PRPS')? 'Proposal':'Purchase Request';?></h2>
            <form method="POST" name="UploadPPB" id="UploadPPB" enctype="multipart/form-data">
                <div class="form-group">
                    <div class = "row">
                        <div class ="col">
                            <label for="uploadScanPPB">Upload Scan Dokumen</label>
                            <input type="file" multiple class="form-control form-control-file" id="uploadScanPPB" name="uploadScanPPB">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col"><br/>
                            <input type="submit" class="btn btn-success" onclick = "SubmitUploadPPB('<?php echo $data->ID?>')" value="Upload">
                            <input type="submit" class="btn btn-danger" onclick = "BatalUploadPPB()" value="Batal Upload">
                        </div>
                    </div>
                </div>
            </form>
            <?php if (!empty($data->FilePath)): ?>
            <div class="row">
                <div class="col-sm-6">
                    <h4><?php echo $data->NoPP; ?></h4>
                </div>
                <div class="col-sm-6">
                    <h5><a href="<?php echo $this->config->base_url().'assets/uploads/files/PPB/'. $data->FilePath; ?>" download>download</a></h5>
                </div>
            </div>
            <div class="row">    
                <div class="col-md-12"> 
                    <iframe src="<?php echo $this->config->base_url().'assets/uploads/files/PPB/'. $data->FilePath; ?>" width="90%" height="720px">
                    This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->config->base_url().'assets/uploads/files/ppb/'. $data->FilePath; ?>">Download PDF</a>
                    </iframe>   
                </div>
            </div>
            <?php endif; ?>
        </div>
        <?php } ?>
        
	</body>
</html> 