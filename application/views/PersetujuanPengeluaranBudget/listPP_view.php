<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title><?php $title ?></title>
		<style>
		td {padding:5px 5px 5px 5px;}
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">List Persetujuan Pengeluaran Budget</h3>
		</div>
        <?php echo $this->session->flashdata('message'); ?>
		<div class="panel-body">
			<table>
				<tr>
					<td>Keterangan</td>
						<td>
							<input type="text" class="form-control" id="txtKeterangan" name="txtKeterangan" placeholder="Keterangan"></input>
						</td>
					</tr>
				</tr>
				<tr>
					<td></td>
					<td>
						<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari">
						<input class="btn btn-danger" type="button" onClick="clearfilter()" id="btnClear" name="" value="Clear Filter">
					</td>
				</tr>
			</table>
			<table class="table table-condensed">
				<thead>
				<tr>
					<th>Nomor</th>
					<th>Keterangan</th>
					<th>Tipe</th>
					<th>Pengeluaran DPA 1</th>
					<th>Pengeluaran DPA 2</th>
					<th>Total</th>
					<th>Tindakan</th>
				</tr>
				</thead>
				<tbody>
				<?php
					if (!empty($data)):
						foreach($data as $value){
							$uploaded = false;
							$bayarAble = false;
							$editAble = false;
							$deleteAble = false;
							if($value["Tipe"] == 'PRPS'){
								$Tipe = 'Proposal';
								$class = 'info';
							}else{
								$Tipe = 'PR';
								$class = 'default';
							}
							if($value["FilePath"] == ""){
								$bayarAble = false;
								$editAble = true;
								$uploaded = false;
							}else{
								$editAble = false;
								$uploaded = true;
								$bayarAble = true;
							}
							echo '<tr class="'.$class.'">';
							echo '<td style="word-wrap: break-word">'.$value["NoPP"].'</td>';
							echo '<td style="word-wrap: break-word">'.$value["Keterangan"].'</td>';
							echo '<td style="word-wrap: break-word">'.$Tipe.'</td>';
							echo '<td style="word-wrap: break-word">'.$value["PengeluaranDPA1"].'</td>';
							echo '<td style="word-wrap: break-word">'.$value["PengeluaranDPA2"].'</td>';
							echo '<td style="word-wrap: break-word">'.$value["TotalPengeluaran"].'</td>';
							echo '<td>';
							if($uploaded == true){
							?>
									<button class="btn btn-success" onclick="upload('<?php echo $value["ID"]; ?>')"><i class="glyphicon glyphicon-cloud-upload"></i> Upload</button>
							<?php
							}else{
							?>
									<button class="btn btn-default" onclick="upload('<?php echo $value["ID"]; ?>')"><i class="glyphicon glyphicon-cloud-upload"></i> Upload</button>
							<?php
							}
							if($editAble){
							?>
									<button class="btn btn-info" onclick="edit('<?php echo $value["ID"]; ?>')"><i class="glyphicon glyphicon-edit"></i> Edit</button>
							<?php
							}
							if($bayarAble){
							?>
									<button class="btn btn-danger" onclick="kartubayar('<?php echo $value["ID"]; ?>')"><i class="glyphicon glyphicon-credit-card"></i> Kartu Bayar</button>
							<?php
							}
							?>
							</tr>
				<?php
						}
					endif;
				?>
				</tbody>
			</table>
		</div>
	</body>
	<script type="text/javascript">
		function upload(NoPP){
			window.location.href = "<?php echo site_url('PersetujuanPengeluaranBudget/PPBController/Upload');?>/"+NoPP;
		}
		function edit(NoPP){
			window.location.href = "<?php echo site_url('PersetujuanPengeluaranBudget/PPBController/Edit');?>/"+NoPP;
		}
		function kartubayar(NoPP){
			window.location.href = "<?php echo site_url('PersetujuanPengeluaranBudget/PPBController/KartuBayar');?>/"+NoPP;
		}
		
	</script>

	<script type="text/javascript">
		function search(){
			var Keterangan = document.getElementById('txtKeterangan').value;
			if(Keterangan == ''){
				var src = '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ListPP/';
			}else{
				var src = '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ListPP/'+Keterangan;
			}

			window.location.replace(src);
		}
		function clearfilter(){
			var src2 = '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ListPP/';
			window.location.replace(src2);
		}
	</script>
</html>
