<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title><?php $title ?></title>
		<script src="<?php echo $this->config->base_url(); ?>assets/js/autoNumeric.js" type="text/javascript"></script>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->		
		<style>
			td {padding:5px 5px 5px 5px;}
		</style>
		<script type="text/javascript">
			var isChangedDPA1 = false;
			var isChangedDPA2 = false;
			var pengeluaranDPA1beforeChanged = 0;
			var pengeluaranDPA2beforeChanged = 0;
			function submitUpdateProposal(ID){
				var DPA = document.getElementById('txtDPA').value;
				var NPK = '<?php echo $npk; ?>';
				var KodeActivity = document.getElementById('txtKodeActivity').value;
				if(isChangedDPA1 || isChangedDPA2){
					flag = true;
				}else{
					var x = window.confirm("Yakin tidak ada yang diubah?");
						if(x){
							flag = true;
						}else{
							flag = false;
							event.preventDefault();
						}
				}
				if(flag){
					if(DPA == 1){
						ambilValueTxtBudgetDPA1();
					}else if(DPA == 2){
						ambilValueTxtBudgetDPA2();
					}else{
						ambilValueTxtBudgetDPA1();
						ambilValueTxtBudgetDPA2();
					}
					var x = window.confirm("Cetak Proposal Ini?");
					if(x){
						$("#UpdateProposal").attr("action"); //will retrieve it
						$("#UpdateProposal").attr("target", "_blank"); //will retrieve it
						$("#UpdateProposal").attr("action", "<?php echo site_url('PersetujuanPengeluaranBudget/PPBController/UpdateProposal') ?>/"+ID);
						window.alert("Data sudah tersimpan, silahkan Tutup halaman ini");
					}else{
						event.preventDefault();
					}
				}else{
					event.preventDefault();
				}
			}
		</script>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Proposal</h3>
			</div>
			<div class="panel-body">
				<?php echo validation_errors(); ?>
				<form method="POST" name="UpdateProposal" id="UpdateProposal">
					<table border=0 class="table table-condensed" >
						<tr>
							<td>No. Persetujuan Pengeluaran *</td>
							<td>
								<input class="form-control" type="text" id="txtNoPP" name="txtNoPP" value="<?php echo $data['NoPP']; ?>" readonly />
							</td>
						</tr>
						<tr>
							<td>DPA *</td>
							<td>
								<input class="form-control" type="text" id="txtDPA" name="txtDPA" value="<?php echo $data['DPA']; ?>" readonly />
							</td>
						</tr>
						<tr>
							<td>Nama Activity</td>
							<td>
								<input class="form-control" type="text" id="txtKodeActivity" name="txtKodeActivity" value="<?php echo $data['KodeActivity'].' - '.$data['NamaActivity']; ?>" readonly />
							</td>
						</tr>
						<tr>
							<td>Tahun Budget</td>
							<td>
								<input class="form-control" type="text" id="txtTahunBudget" name="txtTahunBudget" value="<?php echo $data['TahunBudget']; ?>" readonly />
							</td>
						</tr>
						<tr>
							<td>Jenis Pengeluaran</td>
							<td>
								<input class="form-control" type="hidden" id="hidKodeCoaDPA1" name="hidKodeCoaDPA1" readonly/>
								<input class="form-control" type="hidden" id="hidKodeCoaDPA2" name="hidKodeCoaDPA2" readonly/>
								<input class="form-control" type="text" id="txtOpexCapex" name="txtOpexCapex"value="<?php echo $data['JenisPengeluaran']; ?>" readonly/>
							</td>
						</tr>
						<tr>
							<td>Keterangan</td>
							<td><input class="form-control" type="text" id="txtKeterangan" name="txtKeterangan" value="<?php echo $data['Keterangan']; ?>"/>
							</td>
						</tr>
						<tr class="rowDPA1">
							<td>Pengeluaran DPA 1</td>
							<td>
								<input class="form-control" type="text" id="txtPengeluaranDPA1" name="txtPengeluaranDPA1" value="<?php echo $data['PengeluaranDPA1']; ?>"/>
							</td>
						</tr>
						<tr class="rowDPA2">
							<td>Pengeluaran DPA 2</td>
							<td>
								<input class="form-control" type="text" id="txtPengeluaranDPA2" name="txtPengeluaranDPA2"value="<?php echo $data['PengeluaranDPA2']; ?>"/>
							</td>
						</tr>
						<tr class="rowDPA1">
							<td>BUDGET DPA 1</td>
							<td>
								<input class="form-control" type="text" id="txtBudgetDPA1" name="txtBudgetDPA1" readonly/>
							</td>
						</tr>
						<tr class="rowDPA1">
							<td>REALISASI DPA 1</td>
							<td>
								<input class="form-control" type="text" id="txtRealisasiDPA1" name="txtRealisasiDPA1" readonly/>
							</td>
						</tr>
						<tr class="rowDPA1">
							<td>REALISASI BERJALAN DPA 1</td>
							<td>
								<input class="form-control" type="text" id="txtRealisasiBerjalanDPA1" name="txtRealisasiBerjalanDPA1" readonly/>
							</td>
						</tr>
						<tr class="rowDPA1">
							<td>PENGAJUAN DPA 1</td>
							<td>
								<input class="form-control" type="text" id="txtPengajuanDPA1" name="txtPengajuanDPA1" readonly/>
							</td>
						</tr>
						<tr class="rowDPA1">
							<td>SISA BUDGET DPA 1</td>
							<td>
								<input class="form-control" type="text" id="txtSisaBudgetDPA1" name="txtSisaBudgetDPA1" readonly/>
							</td>
						</tr>
						<tr class="rowDPA2">
							<td>BUDGET DPA 2</td>
							<td>
								<input class="form-control" type="text" id="txtBudgetDPA2" name="txtBudgetDPA2" readonly/>
							</td>
						</tr>
						<tr class="rowDPA2">
							<td>REALISASI DPA 2</td>
							<td>
								<input class="form-control" type="text" id="txtRealisasiDPA2" name="txtRealisasiDPA2" readonly/>
							</td>
						</tr>
						<tr class="rowDPA2">
							<td>REALISASI BERJALAN DPA 2</td>
							<td>
								<input class="form-control" type="text" id="txtRealisasiBerjalanDPA2" name="txtRealisasiBerjalanDPA2" readonly/>
							</td>
						</tr>
						<tr class="rowDPA2">
							<td>PENGAJUAN DPA 2</td>
							<td>
								<input class="form-control" type="text" id="txtPengajuanDPA2" name="txtPengajuanDPA2"  readonly/>
							</td>
						</tr>
						<tr class="rowDPA2">
							<td>SISA BUDGET DPA 2</td>
							<td>
								<input class="form-control bg-success" type="text" id="txtSisaBudgetDPA2" name="txtSisaBudgetDPA2" readonly/>
							</td>
						</tr>
						<tr >
							<td>STATUS</td>
							<td>
								<select class="form-control select2" id="txtSelStatus" name="txtSelStatus">
								<option <?php if($data['Status'] == "Budgeted") { ?> selected = "Selected" <?php } ?> value='Budgeted'>Budgeted</option>
								<option <?php if($data['Status'] == "Unbudgeted") { ?> selected = "Selected" <?php } ?> value='Unbudgeted'>Unbudgeted</option>
								<option <?php if($data['Status'] == "Overbudget") { ?> selected = "Selected" <?php } ?> value='Overbudget'>Overbudget</option>
							</select></td>
						</tr>
						<tr>
							<td colspan = "2">
							<input class="btn btn-primary col-sm-2" type="submit" id="btnSubmit" onclick="submitUpdateProposal('<?php echo $data['ID']; ?>')" name="" value="Simpan dan Cetak">
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
		$( window ).load(function() {

			var DPA = document.getElementById('txtDPA').value;
			var TahunBudget = document.getElementById('txtTahunBudget').value;
			if(DPA == 1){
				$('.rowDPA1').show();
				$('.rowDPA2').hide();
			}else if(DPA == 2){
				$('.rowDPA1').hide();
				$('.rowDPA2').show();
			}else{
				$('.rowDPA1').show();
				$('.rowDPA2').show();
			}
			if(DPA == '1' || DPA == '2'){
				var KodeActivity = (document.getElementById('txtKodeActivity').value).substring(0,6);
				loadActivityBasedOnDPA(KodeActivity, DPA,TahunBudget);
				if(DPA == '1'){
					$('#txtPengeluaranDPA1').prop('disabled','');
					$('#txtPengeluaranDPA2').prop('disabled','true');
					pengeluaranDPA1beforeChanged = document.getElementById('txtPengeluaranDPA1').value;
				}else{
					$('#txtPengeluaranDPA1').prop('disabled','true');
					$('#txtPengeluaranDPA2').prop('disabled','');
					pengeluaranDPA2beforeChanged = document.getElementById('txtPengeluaranDPA2').value;
				}
			}else{
				var KodeActivity = (document.getElementById('txtKodeActivity').value).substring(0,5);
				loadActivityBasedOnDPA(KodeActivity+'1', '1',TahunBudget);
				loadActivityBasedOnDPA(KodeActivity+'2', '2',TahunBudget);
				$('#txtPengeluaranDPA1').prop('disabled','');
				$('#txtPengeluaranDPA2').prop('disabled','');
				pengeluaranDPA1beforeChanged = document.getElementById('txtPengeluaranDPA1').value;
				pengeluaranDPA2beforeChanged = document.getElementById('txtPengeluaranDPA2').value;
			}
		});
		function ambilValueTxtBudgetDPA1(){
			var PengeluaranDPA1 = document.getElementById('txtPengeluaranDPA1').value;
			if(PengeluaranDPA1 == ''){
				alert("Pengeluaran DPA 1 harus di isi!");
				flag = false;
			}
			var BudgetDPA1 = document.getElementById('txtBudgetDPA1').value;
			var RealisasiDPA1 = document.getElementById('txtRealisasiDPA1').value;
			var RealisasiBerjalanDPA1 = document.getElementById('txtRealisasiBerjalanDPA1').value;
		}
		function ambilValueTxtBudgetDPA2(){
			var PengeluaranDPA2 = document.getElementById('txtPengeluaranDPA2').value;
			if(PengeluaranDPA2 == ''){
				alert("Pengeluaran DPA 2 harus di isi!");
				flag = false;
			}
			var BudgetDPA2 = document.getElementById('txtBudgetDPA2').value;
			var RealisasiDPA2 = document.getElementById('txtRealisasiDPA2').value;
			var RealisasiBerjalanDPA2 = document.getElementById('txtRealisasiBerjalanDPA2').value;
		}
		function loadActivity(){
			$('#divLoadingSubmit').show();
			clear();
			var DPA = document.getElementById('txtDPA').value;
			var TahunBudget = document.getElementById('txtTahunBudget').value;
			
			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/BudgetMonitoringController/ajax_loadActivityBasedDPA',
				type: "POST",
				data: {DPA : DPA, admin: <?php echo $admin; ?>, TahunBudget: TahunBudget},
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					if(DPA == 1){
						$('.rowDPA1').show();
						$('.rowDPA2').hide();
					}else if(DPA == 2){
						$('.rowDPA1').hide();
						$('.rowDPA2').show();
					}else{
						$('.rowDPA1').show();
						$('.rowDPA2').show();
					}
					var newOption = $('<option value=""></option>');
					$('#txtNamaActivity').append(newOption);
					for(opsi in data)
					{
						$('#txtNamaActivity').append($('<option value="'+data[opsi].KodeActivity+'">'+data[opsi].NamaActivity+' ('+ data[opsi].KodeActivity +')</option>'));
					}
					$('#txtNamaActivity').trigger("chosen:updated");
					$('#divLoadingSubmit').hide();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
		$('#txtDPA').change(function(){
			loadActivity();
		});
		$('#txtKodeActivity').keyup(function(){
			var KodeActivity = document.getElementById('txtKodeActivity').value;
			$('#txtNamaActivity').val(KodeActivity);
		});

		$('#txtNamaActivity').change(function(){
			$('#txtKodeActivity').val($('#txtNamaActivity').val());
			var KodeActivity = $('#txtKodeActivity').val();
			var DPA = document.getElementById('txtDPA').value;
			var TahunBudget = document.getElementById('txtTahunBudget').value;
			clear();
			$('#divLoadingSubmit').show();
			if(DPA == '1' || DPA == '2'){
				loadActivityBasedOnDPA(KodeActivity, DPA,TahunBudget);
				if(DPA == '1'){
					$('#txtPengeluaranDPA1').prop('disabled','');
					$('#txtPengeluaranDPA2').prop('disabled','true');
				}else{
					$('#txtPengeluaranDPA1').prop('disabled','true');
					$('#txtPengeluaranDPA2').prop('disabled','');
				}
			}else{
				loadActivityBasedOnDPA(KodeActivity+'1', '1',TahunBudget);
				loadActivityBasedOnDPA(KodeActivity+'2', '2',TahunBudget);
				$('#txtPengeluaranDPA1').prop('disabled','');
				$('#txtPengeluaranDPA2').prop('disabled','');
			}
			//$('#txtDPA').val(KodeActivity.substr(KodeActivity.length - 1));
		});

		function loadActivityBasedOnDPA(KodeActivity,DPA, TahunBudget){
			var DPA = DPA;
			var KodeActivity = KodeActivity;
			var TahunBudget = TahunBudget;
			var NoPP = document.getElementById("txtNoPP").value;
			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_kalkulasiBudgetRealisasi',
				type: "POST",
				data: { KodeActivity: KodeActivity, DPA: DPA, TahunBudget: TahunBudget },
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_getSummaryBudget',
						type: "POST",
						data: { KodeActivity: KodeActivity, DPA: DPA, NoPP: NoPP, Source: "EditPRPS", TahunBudget: TahunBudget },
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log(data);
							if(data[2] == "NoBudget"){
								alert('Tidak ada budget DPA '+DPA+' untuk activity ini');
								$('#txtOpexCapex').val(data[1].JenisPengeluaran);
								$('#hidKodeCoaDPA1').val(data[1].KodeCoa);
								if(DPA == 1){
									$('#txtBudgetDPA1').val(0);
									$('#txtRealisasiDPA1').val(0);
									$('#txtRealisasiBerjalanDPA1').val(data[0].TotalRealisasiPengajuan);
									var nominalPengeluaran = document.getElementById('txtPengeluaranDPA1').value;
									document.getElementById('txtPengajuanDPA1').value = nominalPengeluaran;
									nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
									var realisasiBerjalanDPA1 = document.getElementById('txtRealisasiBerjalanDPA1').value;
									realisasiBerjalanDPA1 = realisasiBerjalanDPA1.replace(/,/g, "");
									var sisaBudgetDPA1 = 0 - 0 - realisasiBerjalanDPA1 - nominalPengeluaran;
									document.getElementById('txtSisaBudgetDPA1').value = nf.format(sisaBudgetDPA1);
									if(sisaBudgetDPA1 < 0){
										alert('Sisa Budget DPA 1 kurang dari 0');
										var $select = jQuery( '#txtSelStatus' );
										$select.val("Overbudget").change();
									}
								}else if(DPA == 2){
									$('#txtBudgetDPA2').val(0);
									$('#txtRealisasiDPA2').val(0);
									$('#txtRealisasiBerjalanDPA2').val(data[0].TotalRealisasiPengajuan);
									var nominalPengeluaran = document.getElementById('txtPengeluaranDPA2').value;
									document.getElementById('txtPengajuanDPA2').value = nominalPengeluaran;
									nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
									var realisasiBerjalanDPA2 = document.getElementById('txtRealisasiBerjalanDPA2').value;
									realisasiBerjalanDPA2 = realisasiBerjalanDPA1.replace(/,/g, "");
									var sisaBudgetDPA2 = 0 - 0 - realisasiBerjalanDPA2 - nominalPengeluaran;
									document.getElementById('txtSisaBudgetDPA2').value = nf.format(sisaBudgetDPA2);
									if(sisaBudgetDPA2 < 0){
										alert('Sisa Budget DPA 2 kurang dari 0');
										var $select = jQuery( '#txtSelStatus' );
										$select.val("Overbudget").change();
									}
								}
							}else{
								if(DPA == 1){
									$('#txtBudgetDPA1').val(data[0].BudgetFullYear);
									$('#txtRealisasiDPA1').val(data[0].RealisasiYTD);
									$('#hidKodeCoaDPA1').val(data[0].KodeCoa);
									$('#txtRealisasiBerjalanDPA1').val(data[1].TotalRealisasiPengajuan);
									$('#txtOpexCapex').val(data[0].JenisPengeluaran);
									var budgetDPA1 = document.getElementById('txtBudgetDPA1').value;
									var realisasiDPA1 = document.getElementById('txtRealisasiDPA1').value;
									var realisasiBerjalanDPA1 = document.getElementById('txtRealisasiBerjalanDPA1').value;
									var nominalPengeluaran = document.getElementById('txtPengeluaranDPA1').value;
									document.getElementById('txtPengajuanDPA1').value = nominalPengeluaran;
									budgetDPA1 = budgetDPA1.replace(/,/g, "");
									realisasiDPA1 = realisasiDPA1.replace(/,/g, "");
									realisasiBerjalanDPA1 = realisasiBerjalanDPA1.replace(/,/g, "");
									nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
									var sisaBudgetDPA1 = budgetDPA1 - realisasiDPA1 - realisasiBerjalanDPA1 - nominalPengeluaran;
									document.getElementById('txtSisaBudgetDPA1').value = nf.format(sisaBudgetDPA1);
									if(sisaBudgetDPA1 < 0){
										alert('Sisa Budget DPA 1 kurang dari 0');
										var $select = jQuery( '#txtSelStatus' );
										$select.val("Overbudget").change();
									}
								}else if(DPA == 2){
									$('#txtBudgetDPA2').val(data[0].BudgetFullYear);
									$('#txtRealisasiDPA2').val(data[0].RealisasiYTD);
									$('#hidKodeCoaDPA2').val(data[0].KodeCoa);
									$('#txtRealisasiBerjalanDPA2').val(data[1].TotalRealisasiPengajuan);
									var budgetDPA2 = document.getElementById('txtBudgetDPA2').value;
									var realisasiDPA2 = document.getElementById('txtRealisasiDPA2').value;
									var realisasiBerjalanDPA2 = document.getElementById('txtRealisasiBerjalanDPA2').value;
									var nominalPengeluaran = document.getElementById('txtPengeluaranDPA2').value;
									document.getElementById('txtPengajuanDPA2').value = nominalPengeluaran;
									budgetDPA2 = budgetDPA2.replace(/,/g, "");
									realisasiDPA2 = realisasiDPA2.replace(/,/g, "");
									realisasiBerjalanDPA2 = realisasiBerjalanDPA2.replace(/,/g, "");
									nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
									var sisaBudgetDPA2 = budgetDPA2 - realisasiDPA2 - realisasiBerjalanDPA2 - nominalPengeluaran;
									document.getElementById('txtSisaBudgetDPA2').value = nf.format(sisaBudgetDPA2);
									if(sisaBudgetDPA2 < 0){
										alert('Sisa Budget DPA 2 kurang dari 0');
										var $select = jQuery( '#txtSelStatus' );
										$select.val("Overbudget").change();
									}
								}
							}
							$('#divLoadingSubmit').hide();
						},
						error: function (request, status, error) {
							console.log(error);
							$('#divLoadingSubmit').hide();
						}
					});
					$('#divLoadingSubmit').hide();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
	</script>
	<script type="text/javascript">
		function clear(){
			$('#txtPengeluaranDPA1').prop('disabled','true');
			$('#txtPengeluaranDPA2').prop('disabled','true');
			$('#txtPengeluaranDPA1').val('');
			$('#txtPengeluaranDPA2').val('');
			$('#txtBudgetDPA1').val('');
			$('#txtBudgetDPA2').val('');
			$('#txtRealisasiDPA1').val('');
			$('#txtRealisasiDPA2').val('');
			$('#txtPengajuanDPA1').val('');
			$('#txtPengajuanDPA2').val('');
			$('#txtRealisasiBerjalanDPA1').val('');
			$('#txtRealisasiBerjalanDPA2').val('');
			$('#txtSisaBudgetDPA1').val('');
			$('#txtSisaBudgetDPA2').val('');
		}
		$(function($) {
			$('#txtPengeluaranDPA1').autoNumeric('init', { lZero: 'deny', aSep: ',', mDec: 0 });
			$('#txtPengeluaranDPA2').autoNumeric('init', { lZero: 'deny', aSep: ',', mDec: 0 });
		});
		var nf = new Intl.NumberFormat();
		$("#txtPengeluaranDPA1").on('change', function() {
			var budgetDPA1 = document.getElementById('txtBudgetDPA1').value;
			var realisasiDPA1 = document.getElementById('txtRealisasiDPA1').value;
			var realisasiBerjalanDPA1 = document.getElementById('txtRealisasiBerjalanDPA1').value;
			var nominalPengeluaran = document.getElementById('txtPengeluaranDPA1').value;
			document.getElementById('txtPengajuanDPA1').value = nominalPengeluaran;
			if(pengeluaranDPA1beforeChanged != nominalPengeluaran){
				isChangedDPA1 = true;
			}else{
				isChangedDPA1 = false;
			}
			budgetDPA1 = budgetDPA1.replace(/,/g, "");
			realisasiDPA1 = realisasiDPA1.replace(/,/g, "");
			realisasiBerjalanDPA1 = realisasiBerjalanDPA1.replace(/,/g, "");
			nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
			var sisaBudgetDPA1 = budgetDPA1 - realisasiDPA1 - realisasiBerjalanDPA1 - nominalPengeluaran;
			if(sisaBudgetDPA1 < 0){
				alert('Sisa Budget kurang dari 0');
				var $select = jQuery( '#txtSelStatus' );
				$select.val("Overbudget").change();
			}
			document.getElementById('txtSisaBudgetDPA1').value = nf.format(sisaBudgetDPA1);
		});
		$("#txtPengeluaranDPA2").on('change', function() {
			var budgetDPA2 = document.getElementById('txtBudgetDPA2').value;
			var realisasiDPA2 = document.getElementById('txtRealisasiDPA2').value;
			var realisasiBerjalanDPA2 = document.getElementById('txtRealisasiBerjalanDPA2').value;
			var nominalPengeluaran = document.getElementById('txtPengeluaranDPA2').value;
			document.getElementById('txtPengajuanDPA2').value = nominalPengeluaran;
			if(pengeluaranDPA2beforeChanged != nominalPengeluaran){
				isChangedDPA2 = true;
			}else{
				isChangedDPA2 = false;
			}
			budgetDPA2 = budgetDPA2.replace(/,/g, "");
			realisasiDPA2 = realisasiDPA2.replace(/,/g, "");
			realisasiBerjalanDPA2 = realisasiBerjalanDPA2.replace(/,/g, "");
			nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
			var sisaBudgetDPA2 = budgetDPA2 - realisasiDPA2 - realisasiBerjalanDPA2 - nominalPengeluaran;
			if(sisaBudgetDPA2 < 0){
				alert('Sisa Budget kurang dari 0');
				var $select = jQuery( '#txtSelStatus' );
				$select.val("Overbudget").change();
			}
			document.getElementById('txtSisaBudgetDPA2').value = nf.format(sisaBudgetDPA2);
		});
	</script>
	
</html>
