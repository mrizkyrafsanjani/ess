<html lang="en">

<head>
    <title><?php echo $NoPP; ?></title>
    <style type="text/css">
       .wrapper{
            height:180mm;
            width:297mm;
            font-family: Arial;
        }
        .title {
            font-weight: bold;
            font-family: Arial;
            font-size: 12px;
        }
        .miniComment {
            text-align: right;
            font-family: arial;
            font-size: 8px;
        }
        #tableKeterangan{
            margin-bottom: 5%;
        }
        .td1{width: 100px !important;}
        .td2{width: 100px !important;}
        .td3{width: 100px !important;}
        .td4{width: 100px !important;}
        .td5{width: 100px !important;}
        .td6{width: 100px !important;}
        .td7{width: 100px !important;}
        .td8{width: 100px !important;}
        .td9{width: 100px !important;}
        table > tbody > tr > th {
            border-top: 1px solid black !important;
        }
        table > tbody > tr > td {
            border-top: 1px solid black !important;
        }
        table.table-bordered{
            border:1px solid black !important;
        }
        table.table-bordered > tbody > tr > th{
            border:1px solid black !important;
        }
        table.table-bordered > tbody > tr > td{
            border:1px solid black !important;
        }

        .rowttd{

        }
    </style>
    <script src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
    <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
	<script>
		function cetak(){
            var css = '@page { size: landscape; }',
            head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');

            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet){
            style.styleSheet.cssText = css;
            } else {
            style.appendChild(document.createTextNode(css));
            }

            head.appendChild(style);

            setTimeout(function () { window.print(); }, 500);
            setTimeout(function () { 
            alert('Klik dimanapun untuk menutup halaman ini'); }, 1000);
            //window.onfocus = function () { setTimeout(function () { window.close(); }, 0); }
		}
	</script>
</head>

<body onload="cetak()" onclick="window.close()">
    <div class="wrapper">
        <div class="row">
            <div class="col-xs-2">
                <image id="logoDPA" src='<?php echo $this->config->base_url(); ?>assets/images/logoDPA.png'>
            </div>
            <div class="col-xs-10">
                <label>DANA PENSIUN ASTRA</label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p><center>PURCHASE REQUEST</center></p>
            </div>
        </div>
        <div class="row form-inline">
            <div class="col-xs-2 form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" <?php if($DPA == '1') echo "checked";?>>
                <label class="form-check-label" for="inlineCheckbox1">DPA 1</label>
            </div>
            <div class="col-xs-2 form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" <?php if($DPA == '2') echo "checked";?>>
                <label class="form-check-label" for="inlineCheckbox2">DPA 2</label>
            </div>
            <div class="col-xs-4">
            </div>
            <div class="col-xs-2 form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox3" <?php if($JenisPengeluaran == 'Opex') echo "checked";?>>
                <label class="form-check-label" for="inlineCheckbox3">Opex</label>
            </div>
            <div class="col-xs-2 form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" <?php if($JenisPengeluaran == 'Capex') echo "checked";?>>
                <label class="form-check-label" for="inlineCheckbox4">Capex</label>
            </div>
        </div>
        <div class="row form-inline">
            <div class="col-xs-3">
                No: <?php if($NoPP != '') echo $NoPP; ?>
            </div>
            <div class="col-xs-3">
                Tgl: <?php echo date('d F Y') ?>
            </div>
            <div class="col-xs-6">
                Departemen: <?php if($departemenUser != '') echo $departemenUser; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table class="table" id="tableKeterangan">
                    <tr>
                        <th>No</th>
                        <th>Keterangan</th>
                        <th>COA</th>
                        <th>Quantity</th>
                        <th>Harga Satuan</th>
                        <th>Total</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td><?php if($Keterangan != '') echo $Keterangan; ?></td>
                        <td><?php if($KodeCoa != '') echo $KodeCoa; ?></td>
                        <td><?php if($Quantity != '') echo $Quantity; ?></td>
                        <td><?php if($HargaSatuan != '') echo $HargaSatuan; ?></td>
                        <td><?php if($TotalPengeluaran != '') echo $TotalPengeluaran; ?></td>
                    </tr>
                    <tr>
                        <td colspan="5">Grand Total</td>
                        <td><?php if($TotalPengeluaran != '') echo $TotalPengeluaran; ?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-2 form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox4" <?php if($Status == 'Budgeted') echo "checked";?>>
                <label class="form-check-label" for="inlineCheckbox4">Budgeted</label>
            </div>
            <div class="col-xs-2 form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox5" <?php if($Status == 'Overbudget') echo "checked";?>>
                <label class="form-check-label" for="inlineCheckbox5">Overbudget</label>
            </div>
            <div class="col-xs-2 form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox6" <?php if($Status == 'Unbudgeted') echo "checked";?>>
                <label class="form-check-label" for="inlineCheckbox6">Unbudgeted</label>
            </div>
        </div>
        <input class="form-control" type="hidden" id="hidDepartemenUser" name="hidDepartemenUser" value="<?php if($departemenUser != '') echo $departemenUser; ?>" readonly/>
        <input class="form-control" type="hidden" id="hidTipePRST" name="hidTipePRST" value="<?php if($TipePRST != '') echo $TipePRST; ?>" readonly/>
        <div class="row">
            <div id="rowttd" class="col-xs-12">
                <table class="table table-bordered">
                    <tr>
                        <td class="td1"><span class="title">Pemohon</span></td>
                        <td class="td2"><span class="title">Dept Head Pemohon/Leader</span></td>
                        <td class="td3"><span class="title">Acc & Tax Dept Head</span></td>
                        <td class="td4"><span class="title">DIC Pemohon</span></td>
                        <td class="td5"><span class="title">HRGA Dept Head</span></td>
                        <td class="td6"><span class="title">DIC Fin & Investment, HRGA / Direktur DPA 1</span></td>
                        <td class="td7"><span class="title">Chief / Presiden Direktur DPA 1</span></td>
                        <td class="td8"><span class="title">DIC Membership / Direktur DPA 2</span></td>
                        <td class="td9"><span class="title">DIC Acc & Tax, IT / Presiden Direktur DPA 2</span></td>
                    </tr>
                    <tr>
                        <td class="td1" style="height: 100px;"></td>
                        <td class="td2" style="height: 100px;"></td>
                        <td class="td3" style="height: 100px;"></td>
                        <td class="td4" style="height: 100px;"></td>
                        <td class="td5" style="height: 100px;"></td>
                        <td class="td6" style="height: 100px;"></td>
                        <td class="td7" style="height: 100px;"></td>
                        <td class="td8" style="height: 100px;"></td>
                        <td class="td9" style="height: 100px;"></td>
                    </tr>
                    <tr>
                        <td class="td1" style=""><span class="title"><?php if($namaUser != '') echo $namaUser;?></span></td>
                        <td class="td2" style=""><span class="title"><?php if($namaAtasan != '') echo $namaAtasan;?></span></td>
                        <td class="td3" style=""><span class="title">Ratna Ikawati</span></td>
                        <td class="td4" style=""><span class="title"><?php if($namaAtasannyaatasan != '') echo $namaAtasannyaatasan;?></span></td>
                        <td class="td5" style=""><span class="title">Eviyati</span></td>
                        <td class="td6" style=""><span class="title">Chairi Pitono</span></td>
                        <td class="td7" style=""><span class="title">Suheri</span></td>
                        <td class="td8" style=""><span class="title">Purwaningsih</span></td>
                        <td class="td9" style=""><span class="title">Fredyanto Manalu</span></td>
                    </tr>
                    <tr>
                        <td class="td1" style="height:70px;"><span class="miniComment">comment:</span></td>
                        <td class="td2" style="height:70px;"><span class="miniComment">comment:</span></td>
                        <td class="td3" style="height:70px;"><span class="miniComment">comment:</span></td>
                        <td class="td4" style="height:70px;"><span class="miniComment">comment:</span></td>
                        <td class="td5" style="height:70px;"><span class="miniComment">comment:</span></td>
                        <td class="td6" style="height:70px;"><span class="miniComment">comment:</span></td>
                        <td class="td7" style="height:70px;"><span class="miniComment">comment:</span></td>
                        <td class="td8" style="height:70px;"><span class="miniComment">comment:</span></td>
                        <td class="td9" style="height:70px;"><span class="miniComment">comment:</span></td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-12">                
                <p><i>Kode Activity: <?php echo $KodeActivity; ?> | Nama Activity: <?php echo $NamaActivity; ?>| Tahun Budget: <?php echo $TahunBudget; ?><i></p>
             </div>
        </div>
    </div>
</body>
<script type="text/javascript">
$(document).ready(function() {
    $("#rowttd").attr('class','col-xs-4 text-right text-xs-center');
    var TipePRST = document.getElementById("hidTipePRST").value;
    var departemenUser = document.getElementById("hidDepartemenUser").value;
    $(".td4").hide();
    $(".td5").hide();
    $(".td6").hide();
    $(".td7").hide();
    $(".td8").hide();
    $(".td9").hide();
    /*
    "td4" DIC Pemohon
    "td5" HRGA Dept Head
    "td6" DIC Fin & Investment, HRGA / Direktur DPA 1
    "td7" DIC Chief / Presiden Direktur DPA 1
    "td8" DIC Membership / Direktur DPA 2
    "td9" DIC Acc & Tax, IT / Presiden Direktur DPA 2
    */
    if(TipePRST == "BOPEX510" || TipePRST == "BCAPEX525"){
        $(".td4").show();
        $("#rowttd").attr('class','col-xs-6');
    }else if(TipePRST == "1BOPEX100" || TipePRST == "1BCAPEX250"){
        if(departemenUser == "IT" 
        || departemenUser == "Mitra Relation & Communication" 
        || departemenUser == "Customer Relation" 
        || departemenUser == "Kepesertaan" 
        || departemenUser == "Claim & Actuary"
        || departemenUser == "Actuary & Administration Support"
        || departemenUser == "Accounting Tax & Control"){
            $(".td4").show();
        };
        $(".td6").show();
        $(".td7").show();
        $("#rowttd").attr('class','col-xs-9 text-right text-xs-center');
    }else if(TipePRST == "2BOPEX100" || TipePRST == "2BCAPEX250"){
        if(departemenUser == "HRGA" 
        || departemenUser == "Finance & Investment" 
        || departemenUser == "Corporate Secretary"){
            $(".td4").show();
        }
        $(".td8").show();
        $(".td9").show();
        $("#rowttd").attr('class','col-xs-9 text-right text-xs-center');
    }else if(TipePRST == "UOOPEX"){
        $(".td6").show();
        $(".td7").show();
        $(".td8").show();
        $(".td9").show();
        $("#rowttd").attr('class','col-xs-11 text-right text-xs-center');
    }else if(TipePRST == "UOCAPEX"){
        if(departemenUser != "HRGA"){
            $(".td5").show();
        }
        $(".td6").show();
        $(".td7").show();
        $(".td8").show();
        $(".td9").show();
        $("#rowttd").attr('class','col-xs-12 text-right text-xs-center');
    }
});
</script>

</html>