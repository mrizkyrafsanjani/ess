<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
 
<?php 
foreach($body->css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<?php foreach($body->js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
</head>
<body>
<!-- Beginning header -->

<!-- End of header-->
    <div style='height:20px;'></div>  
    <div>
       <iframe id="iFrameLog" src="<?php echo $this->config->base_url(); ?>index.php/LogMonitoring/LogMonitoring/view/<?php echo $idLogSelected; ?>" width="100%" height="550px" seamless >
			<p>Your browser does not support iframes.</p>
		</iframe>
    </div>
  
<!-- Beginning footer -->
<div></div>
<!-- End of Footer -->
</body>
</html>
