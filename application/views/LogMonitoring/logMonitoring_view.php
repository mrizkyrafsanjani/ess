<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function alertsizeLogMonitoring(pixels){
			pixels+=32;
			document.getElementById('iFrameLog').style.height=pixels+"px";
		}
		function search(){
						
			var tahun = document.getElementById('cmbTahun').value;
			var bulan = document.getElementById('cmbBulan').value;
			
			var iframe = document.getElementById('iFrameLog');
		
			iframe.src = '<?php echo $this->config->base_url(); ?>index.php/LogMonitoring/LogMonitoring/user/'+tahun+'/'+bulan;
		
		}
	</script>
	<head>
		<title><?php $title ?></title>
		
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Log Monitoring</h3>
			</div>
			<div class="panel-body">
				<?php echo validation_errors(); ?>
				<table border=0>
					
					<tr>
						<td>Tahun</td>
						<td>
							<select class="form-control" id="cmbTahun" name="cmbTahun">
							<?php 
								for($i=date('Y');$i>date('Y')-4;$i--)
								{
									echo "<option value='".$i."'>".$i."</option>";
								}
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Bulan</td>
						<td>
							<select class="form-control" id="cmbBulan" name="cmbBulan">
							<?php 
								for($i=1;$i<=12;$i++)
								{
									$selected = '';
									if(date('n') == $i )
										$selected = 'selected';
									echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
								}
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari">
						</td>
					</tr>
				</table>
				<iframe id="iFrameLog" src="<?php echo $this->config->base_url(); ?>index.php/LogMonitoring/LogMonitoring/index/non/non" width="100%" height="400px" seamless >
				  <p>Your browser does not support iframes.</p>
				</iframe>
			
			
			</div>
		</div>
	</body>
</html>
