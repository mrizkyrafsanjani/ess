<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title><?php $title ?></title>
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">BPKB User</h3>
			</div>
			<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>

			<div class="panel-body">
				<table class="table">
					<thead>
					<tr>
						<th>No Polisi</th>
						<th>No BPKB</th>
						<th>Jenis Kendaraan</th>
						<th>Status Peminjaman</th>
						<th>Tanggal Peminjaman</th>
						<th>Tanggal Pengembalian</th>
						<th>Tanggal Pengambilan</th>
						<th>Kepentingan</th>
					</tr>
					</thead>
					<tbody>
					<?php
						foreach($data as $value){
							$pinjamAble = false;
							$ambilAble = false;
							$Status;
							if($value->Status==0){
								$Status = 'Tidak Dapat Dipinjam';
							}else if($value->Status == 1){
								$pinjamAble = true;
								$Status = 'Dapat Dipinjam';
							}else if($value->Status == 2){
								$Status = 'Sedang Dalam Proses Peminjaman';
							}else if($value->Status == 3){
								$Status = 'Sedang Dipinjam';
							}else if($value->Status == 4){
								$ambilAble = true;
								$Status = 'Dapat Diambil';
							}else if($value->Status == 5){
								$Status = 'Sedang Dalam Proses Pengambilan';
							}else if($value->Status == 6){
								$Status = 'Sudah Diambil';
							}else if($value->Status == 7){
								$Status = 'Tidak Dapat Diambil';
							}else if($value->Status == 8){
								$Status = 'Sudah Diterima User';
							}
							$now = new DateTime(date('Y-m-d h:i:s'));
							if($value->TanggalPeminjaman == '00/00/0000'){
								$value->TanggalPeminjaman = '-';
							}
							if($value->TanggalPengembalian == '00/00/0000'){
								$value->TanggalPengembalian = '-';
							}
							if($value->TanggalPengambilan == '00/00/0000'){
								$value->TanggalPengambilan = '-';
							}
							// $expiredPeriod = new DateTime($value->ExpiredPeriod);
							// $interval = $now->diff($expiredPeriod);
							// $sisaWaktu = $interval->format('%R%a hari');
							// $flagExp = false;
							// if($expiredPeriod<=$now){
							// 	$flagExp = true;
							// }
							// $permanent = true;
							// if($value->StatusResolved != 2 && $value->ExpiredPeriod != '0000-00-00' && $value->ExpiredPeriod > date('Y-m-d h:i:s') && $sisaWaktu < 8){
							// 	$class = "warning";
							// 	$status = "Akses Temporary Kurang dari 1 minggu dan akan dikembalikan segera";
							// 	$ambilAble = true;
							// }
							// if($value->ExpiredPeriod != '0000-00-00'){
							// 	$permanent = false;
							// }
							echo '<tr>';
							echo '<td>'.$value->NoPolisi.'</td>';
							echo '<td>'.$value->NoBPKB.'</td>';
							echo '<td>'.$value->JenisKendaraan.'</td>';
							echo '<td>'.$Status.'</td>';
							echo '<td>'.$value->TanggalPeminjaman.'</td>';
							echo '<td>'.$value->TanggalPengembalian.'</td>';
							echo '<td>'.$value->TanggalPengambilan.'</td>';
							echo '<td>';
							if($pinjamAble){
							?>
									<?php
									if($isHead){
									?>
									<button class="btn btn-success" onclick="pinjamHead('<?php echo $value->IdBPKB; ?>')"><i class="glyphicon glyphicon-triangle-top"></i> Pinjam</button>
									<?php
									}else{
									?>
									<button class="btn btn-success" onclick="pinjam('<?php echo $value->IdBPKB; ?>')"><i class="glyphicon glyphicon-triangle-top"></i> Pinjam</button>

									<?php
									}
									?>
							<?php
							}
							if($ambilAble){
							?>
									<?php
										if($isHead){
										?>
										<button class="btn btn-primary" onclick="ambilHead('<?php echo $value->IdBPKB; ?>')"><i class="glyphicon glyphicon-triangle-bottom"></i> Ambil</button>
										<?php
										}else{
										?>
										<button class="btn btn-primary" onclick="ambil('<?php echo $value->IdBPKB; ?>')"><i class="glyphicon glyphicon-triangle-bottom"></i> Ambil</button>
										<?php
										}
									?>
							<?php
							}
							echo '</td>';
							?>
							</tr>
					<?php
						}
					?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">List Transaksi BPKB</h3>
			</div>
			<div class="panel-body">
				<table class="table">
					<thead>
					<tr>
						<th>No Transaksi</th>
						<th>No BPKB</th>
						<th>Kepentingan</th>
						<th>Status Approval</th>
						<th>Tanggal Transaksi</th>
						<th>Tanggal Pengembalian</th>
						<th>Tanggal Pengambilan</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php
						foreach($dataTrk as $value){
							$terimaPinjamAble = false;
							$terimaAmbilAble = false;
							if($value->StatusApproval == 'AP2' && $value->StatusTerima == 0){
								if($value->Kepentingan=='Peminjaman' || $value->Kepentingan == 'PeminjamanHead'){
									$terimaPinjamAble = true;
								}else{
									$terimaAmbilAble = true;
								}
							}
							echo '<tr>';
							echo '<td>'.$value->KodeTrk.'</td>';
							echo '<td>'.$value->NoBPKB.'</td>';
							echo '<td>'.$value->Kepentingan.'</td>';
							echo '<td>'.$value->StatusApproval.'</td>';
							echo '<td>'.$value->TanggalTransaksi.'</td>';
							echo '<td>'.$value->TanggalPengembalian.'</td>';
							echo '<td>'.$value->TanggalPengambilan.'</td>';
							echo '<td>';
								if($terimaPinjamAble){
								?>
										<button class="btn btn-danger" onclick="konfirmasiTerimaPeminjaman('<?php echo $value->IdBPKB; ?>','<?php echo $value->KodeTrk; ?>' )"><i class="glyphicon glyphicon-triangle-left"></i> Konfirmasi Terima Peminjaman</button>
								<?php
								}
								if($terimaAmbilAble){
								?>
										<button class="btn btn-danger" onclick="konfirmasiTerimaPengambilan('<?php echo $value->IdBPKB; ?>','<?php echo $value->KodeTrk; ?>')"><i class="glyphicon glyphicon-triangle-left"></i> Konfirmasi Terima Pengambilan</button>
								<?php
								}
								echo '</td>';
							echo '</tr>';
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</body>
	
	<script type="text/javascript">
		function pinjam(IdBPKB){
			flag = true;
			if(flag){
				var x = window.confirm("Pinjam BPKB ini?");
				if(x){
					$('#divLoadingSubmit').show();
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/BPKBController/ajax_pinjamAmbilBPKB',
						type: "POST",
						data: { IdBPKB: IdBPKB, Kepentingan: "Peminjaman"},
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log(data);
							alert(data.message);
							$('#divLoadingSubmit').hide();
							location.reload();

						},
						error: function (request, status, error) {
							console.log(error);
							alert('Error');
						}
					});
				}else{
					event.preventDefault();
				}
			}else{
				event.preventDefault();
			}
		}
		function pinjamHead(IdBPKB){
			flag = true;
			if(flag){
				var x = window.confirm("Pinjam BPKB ini?");
				if(x){
					$('#divLoadingSubmit').show();
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/BPKBController/ajax_pinjamAmbilBPKB',
						type: "POST",
						data: { IdBPKB: IdBPKB, Kepentingan: "PeminjamanHead"},
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log(data);
							alert(data.message);
							$('#divLoadingSubmit').hide();
							print(IdBPKB);
						},
						error: function (request, status, error) {
							console.log(error);
							alert('Error');
						}
					});
				}else{
					event.preventDefault();
				}
			}else{
				event.preventDefault();
			}
		}
		function ambil(IdBPKB){
			flag = true;
			if(flag){
				var x = window.confirm("Ambil BPKB ini?");
				if(x){
					$('#divLoadingSubmit').show();
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/BPKBController/ajax_pinjamAmbilBPKB',
						type: "POST",
						data: { IdBPKB: IdBPKB, Kepentingan: "Pengambilan"},
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log(data);
							alert(data.message);
							$('#divLoadingSubmit').hide();
							location.reload();

						},
						error: function (request, status, error) {
							console.log(error);
							alert('Error');
						}
					});
				}else{
					event.preventDefault();
				}
			}else{
				event.preventDefault();
			}
		}
		function ambilHead(IdBPKB){
			flag = true;
			if(flag){
				var x = window.confirm("Ambil BPKB ini?");
				if(x){
					$('#divLoadingSubmit').show();
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/BPKBController/ajax_pinjamAmbilBPKB',
						type: "POST",
						data: { IdBPKB: IdBPKB, Kepentingan: "PengambilanHead"},
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log(data);
							alert(data.message);
							$('#divLoadingSubmit').hide();
							print(IdBPKB);

						},
						error: function (request, status, error) {
							console.log(error);
							alert('Error');
						}
					});
				}else{
					event.preventDefault();
				}
			}else{
				event.preventDefault();
			}
		}
		function konfirmasiTerimaPengambilan(IdBPKB, KodeTrk){
			flag = true;
			if(flag){
				var x = window.confirm("Terima BPKB ini?");
				if(x){
					$('#divLoadingSubmit').show();
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/BPKBController/ajax_terimaBPKB',
						type: "POST",
						data: { IdBPKB: IdBPKB, Kepentingan: "Pengambilan", KodeTrk: KodeTrk},
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log(data);
							alert(data.message);
							$('#divLoadingSubmit').hide();
							location.reload();

						},
						error: function (request, status, error) {
							console.log(error);
							alert('Error');
						}
					});
				}else{
					event.preventDefault();
				}
			}else{
				event.preventDefault();
			}
		}
		function konfirmasiTerimaPeminjaman(IdBPKB, KodeTrk){
			flag = true;
			if(flag){
				var x = window.confirm("Terima BPKB ini?");
				if(x){
					$('#divLoadingSubmit').show();
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/BPKBController/ajax_terimaBPKB',
						type: "POST",
						data: { IdBPKB: IdBPKB, Kepentingan: "Peminjaman", KodeTrk: KodeTrk},
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log(data);
							alert(data.message);
							$('#divLoadingSubmit').hide();
							location.reload();

						},
						error: function (request, status, error) {
							console.log(error);
							alert('Error');
						}
					});
				}else{
					event.preventDefault();
				}
			}else{
				event.preventDefault();
			}
		}
		function print(IdBPKB){
			window.location.href = "<?php echo site_url('PeminjamanDokumen/BPKBController/printbpkb');?>?IdBPKB="+IdBPKB;
		}
	</script>
</html>
