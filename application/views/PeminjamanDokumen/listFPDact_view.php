<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function iframeHeight(pixels){
			pixels+=32;
			document.getElementById('iFrameListFPD_Done').style.height=pixels+"px";
		}
	</script>
	<head>
		<title><?php $title ?></title>
		<style>
		td {padding:5px 5px 5px 5px;}
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">List All Peminjaman Dokumen
			<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
			</h3>
		</div>
        <?php echo $this->session->flashdata('message'); ?>
		<div class="panel-body">
			<table class="table table-condensed">
				<thead>
				<tr>
					<th>No</th>
					<th>Peminjam</th>
					<th>Kode Voucher / Nomor Peserta</th>
					<th>Status</th>
					<th>Deadline</th>
					<th>Tanggal Terbit Voucher</th>
					<th>Tindakan</th>
				</tr>
				</thead>
				<tbody>
				<?php
					if (!empty($data)):
						foreach($data as $value){
							$tolakAble = true;
							$returnAble = true;
							$status = "Belum Diberikan";
							if($value->IsReceived == '0'){
								$tolakAble = true;
								$returnAble = false;
								$status = "Belum Diberikan";
							}else if ($value->IsBorrowed == '1'){
								$status = "Sedang dipinjam";
								$tolakAble = false;
								$returnAble = true;
							}else if($value->IsBack == '1'){
								$status = "Sudah Dikembalikan";
								$tolakAble = false;
								$returnAble = false;
							}
							echo '<tr>';
							echo '<td style="word-wrap: break-word">'.$value->KodeFPD.'</td>';
							echo '<td style="word-wrap: break-word">'.$value->Peminjam.'</td>';
							echo '<td style="word-wrap: break-word">'.$value->KodeVoucherNomorPeserta.'</td>';
							echo '<td style="word-wrap: break-word">'.$status.'</td>';
							echo '<td style="word-wrap: break-word">'.$value->TanggalDeadline.'</td>';
							echo '<td style="word-wrap: break-word">'.$value->TanggalTerbitVoucher.'</td>';
							echo '<td>';
							if($tolakAble){
							?>
								<button class="btn btn-danger" onclick="reject('<?php echo $value->KodeFPD; ?>')"><i class="glyphicon glyphicon-cloud-upload"></i> Tolak</button>
							<?php
							}
							if($returnAble){
							?>
								<button class="btn btn-success" onclick="return_('<?php echo $value->KodeFPD; ?>')"><i class="glyphicon glyphicon-edit"></i> Konfirmasi Pengembalian</button>
							<?php
							}
							?>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
							</td></tr>
					<?php
						}
					endif;
				?>
				</tbody>
			</table>
		</div>

		<iframe id="iFrameListFPD_Done" src="<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/PDController/ListFPD_Done" width="100%" height="200px" seamless >
		  <p>Your browser does not support iframes.</p>
		</iframe>
	</body>
	<script type="text/javascript">
		
		$(document).ready(function(){
			<?php if($this->session->flashdata('msg')){ ?>
			alert('<?php echo $this->session->flashdata('msg'); ?>');
			<?php } ?>
		});
		function search(){
			var Filter = document.getElementById('selectStatus').value;
			window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/ViewURFAdmin');?>/"+Filter;
		}
		function reject(KodeFPD){
			$('#divLoadingSubmit').show();

			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/PDController/ajax_reject',
				type: "POST",
				data: { KodeFPD: KodeFPD },
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					$('#divLoadingSubmit').hide();
					location.reload();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
		function return_(KodeFPD){
			$('#divLoadingSubmit').show();

			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/PDController/ajax_return',
				type: "POST",
				data: { KodeFPD: KodeFPD },
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					$('#divLoadingSubmit').hide();
					location.reload();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
	</script>
</html>
