<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title><?php $title ?></title>
		<script src="<?php echo $this->config->base_url(); ?>assets/js/autoNumeric.js" type="text/javascript"></script>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->		
		<style>
		td {padding:5px 5px 5px 5px;}
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Form Peminjaman Dokumen</h3>
			</div>
			<div class="panel-body">
				<?php echo validation_errors(); ?>
				<form method="Post" onsubmit="cetakSubmitFPD()" name="CreateFPD" id="CreateFPD">
					<table border=0 class="table table-condensed" >
						<tr>
							<td>DPA *</td>
							<td colspan="3">
								<select class="form-control select2" style="width:100%" id="txtDPA" name="txtDPA">
									<option value='1'>1</option>
									<option value='2'>2</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Nomor Voucher</td>
							<td><input class="form-control" type="text" id="txtNomorVoucher" name="txtNomorVoucher"/>
							</td>
							<td>Nomor Peserta</td>
							<td><input class="form-control" type="text" id="txtNomorPeserta" name="txtNomorPeserta"/>
							</td>
						</tr>
						<tr>
						<td colspan ="4">
							<input class="btn btn-primary col-sm-2" type="button" onclick="cariDetailDokumen()"id="btnCari" name="" value="Cari!">
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
						</tr>
						<tr>
						<td>Tanggal Terbit Voucher</td>
							<td colspan ="3">
							<input class="form-control" type="text" id="txtTanggalTerbit" name="txtTanggalTerbit" readonly/>
							</td>
						</tr>
						<tr>
							<td>Keterangan</td>
							<td colspan ="3"><input class="form-control" type="text" id="txtKeterangan" name="txtKeterangan" readonly/>
							</td>
						</tr>
						<tr>
							<td>Nominal</td>
							<td colspan ="3"><input class="form-control" type="text" id="txtNominal" name="txtNominal" readonly/>
							</td>
						</tr>
						<tr>
							<td>Status</td>
							<td colspan ="3"><input class="form-control" type="text" id="txtStatus" name="txtStatus" readonly/>
							</td>
						</tr>
						<tr>
							<td>Bank</td>
							<td colspan ="3"><input class="form-control" type="text" id="txtBank" name="txtBank" readonly/>
							</td>
						</tr>
						<tr>
							<td>Keperluan</td>
							<td colspan ="3">
								<select class="form-control select2" style="width:100%" id="selKeperluan" name="selKeperluan">
									<option value='Retur MP'>Retur MP</option>
									<option value='Bukan Retur MP'>Bukan Retur MP</option>
								</select>
							</td>
						</tr>
						<td>Tanggal Pinjam</td>
							<td colspan ="3">
							<input class="form-control" type="text" id="txtTanggalPinjam" name="txtTanggalPinjam" readonly/>
							</td>
						</tr>
						<td>Tanggal Tanggal Pengembalian</td>
							<td colspan ="3">
							<input class="form-control" type="text" id="txtTanggalPengembalian" name="txtTanggalPengembalian" readonly/>
							</td>
						</tr>
						<tr>
							<td colspan = "4">
							<input class="btn btn-primary col-sm-2" type="submit" id="btnSubmit" name="" value="Submit Peminjaman">
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
		$( window ).load(function() {
		});
		function cariDetailDokumen(){
			$('#divLoadingSubmit').show();
			var NomorVoucher = document.getElementById("txtNomorVoucher").value;
			var NomorPeserta = document.getElementById("txtNomorPeserta").value;
			var DPA = document.getElementById("txtDPA").value;
			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/PDController/ajax_cariDetail',
				type: "POST",
				data: { NomorVoucher: NomorVoucher, NomorPeserta: NomorPeserta, DPA: DPA },
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					if(data.msg == 'Tidak ditemukan'){
						alert(data.msg);
						$('#txtTanggalTerbit').val("");
						$('#txtKeterangan').val("");
						$('#txtNominal').val("");
						$('#txtStatus').val("");
						$('#txtBank').val("");
						$('#txtTanggalPinjam').val("");
						$('#txtTanggalPengembalian').val("");
					}else{
						$('#txtNomorVoucher').val(data.NomorVoucher.trim());
						if((data.NomorPeserta.trim().length)!= 6){
							$('#txtNomorPeserta').val("-");
						}else if($.isNumeric(data.NomorPeserta)){
							$('#txtNomorPeserta').val(data.NomorPeserta.trim());
						}
						$('#txtTanggalTerbit').val(data.TanggalTerbit);
						$('#txtKeterangan').val(data.Keterangan.trim());
						$('#txtNominal').val(data.Nominal);
						$('#txtStatus').val(data.Status);
						if(data.Status == 'Sedang Dipinjam'){
							$('#btnSubmit').hide();
						}else{
							$('#btnSubmit').show();

						}
						// if(data.Status == 1 || data.Status == 'S' ){
						// 	$('#txtStatus').val("Completed");
						// }else{
						// 	$('#txtStatus').val("Pending");
						// }
						$('#txtBank').val(data.Bank);
						$('#txtTanggalPinjam').val(data.TanggalPinjam);
						$('#txtTanggalPengembalian').val(data.TanggalPengembalian);
					}
				
					$('#divLoadingSubmit').hide();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
	</script>
	<script type="text/javascript">
		function cetakSubmitFPD(){
			var DPA = document.getElementById('txtDPA').value;
			var NPK = '<?php echo $npk; ?>';
			var flag = false;
			var NomorVoucher = document.getElementById('txtNomorVoucher').value;
			var NomorPeserta = document.getElementById('txtNomorPeserta').value;
			var Keterangan = document.getElementById('txtKeterangan').value;
			var Nominal = document.getElementById('txtNominal').value;
			var Status = document.getElementById('txtStatus').value;
			var Bank = document.getElementById('txtBank').value;
			var Keperluan = document.getElementById('selKeperluan').value;
			if(NomorVoucher == ''){
				alert('Harap Mengisi NomorVoucher yang akan di submit!');
				flag = false;
			}else if(NomorPeserta == ''){
				alert('Harap Mengisi NomorPeserta yang akan di submit!');
				flag = false;
			} else{
				flag = true;
				if(flag){
					var x = window.confirm("Yakin Mau Pinjam Dokumen Ini?");
					if(x){
						$("#CreateFPD").attr("action"); //will retrieve it
						$("#CreateFPD").attr("target", "_self"); //will retrieve it
						$("#CreateFPD").attr("action", "<?php echo site_url('PeminjamanDokumen/PDController/CreateFPD') ?>");
					}else{
						event.preventDefault();
					}
				}else{
					event.preventDefault();
				}
			}
		}
		</script>
</html>
