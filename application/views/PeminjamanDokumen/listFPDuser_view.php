<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title><?php $title ?></title>
		<style>
		td {padding:5px 5px 5px 5px;}
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">List Peminjaman Dokumen
				<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
			</h3>
		</div>
        <?php echo $this->session->flashdata('message'); ?>
		<div class="panel-body">
			<table class="table table-condensed">
				<thead>
				<tr>
					<th>No</th>
					<th>Kode Voucher / Nomor Peserta</th>
					<th>Status</th>
					<th>Deadline</th>
					<th>Tindakan</th>
				</tr>
				</thead>
				<tbody>
				<?php
					if (!empty($data)):
						foreach($data as $value){
							$batalAble = true;
							$terimaAble = true;
							$status = "";
							if($value->IsReceived == 0){
								$batalAble = true;
								$terimaAble = true;
								$status = "Belum Diberikan";
							}else if ($value->IsBorrowed == 1){
								$status = "Sedang dipinjam";
								$batalAble = false;
								$terimaAble = false;
							}else if($value->IsBack == 1){
								$status = "Sudah Dikembalikan";
								$batalAble = false;
								$terimaAble = false;
							}
							echo '<tr>';
							echo '<td style="word-wrap: break-word">'.$value->KodeFPD.'</td>';
							echo '<td style="word-wrap: break-word">'.$value->KodeVoucherNomorPeserta.'</td>';
							echo '<td style="word-wrap: break-word">'.$status.'</td>';
							echo '<td style="word-wrap: break-word">'.$value->TanggalDeadline.'</td>';
							echo '<td>';
							if($batalAble){
							?>
								<button class="btn btn-danger" onclick="cancel('<?php echo $value->KodeFPD; ?>')"><i class="glyphicon glyphicon-cloud-upload"></i> Batal Pinjam</button>
							<?php
							}
							if($terimaAble){
							?>
								<button class="btn btn-success" onclick="receive('<?php echo $value->KodeFPD; ?>')"><i class="glyphicon glyphicon-edit"></i> Konfirmasi Penerimaan</button>
							<?php
							}
							?>
							</td></tr>
					<?php
						}
					endif;
				?>
				</tbody>
			</table>
		</div>
	</body>
	<script type="text/javascript">
		
		$(document).ready(function(){
			<?php if($this->session->flashdata('msg')){ ?>
			alert('<?php echo $this->session->flashdata('msg'); ?>');
			<?php } ?>
		});
		function search(){
			var Filter = document.getElementById('selectStatus').value;
			window.location.href = "<?php echo site_url('URFbySystem/URFbySystemController/ViewURFAdmin');?>/"+Filter;
		}
		function cancel(KodeFPD){
			$('#divLoadingSubmit').show();
			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/PDController/ajax_cancel',
				type: "POST",
				data: { KodeFPD: KodeFPD },
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					$('#divLoadingSubmit').hide();
					location.reload();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
		function receive(KodeFPD){
			$('#divLoadingSubmit').show();
			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/PDController/ajax_receive',
				type: "POST",
				data: { KodeFPD: KodeFPD },
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					$('#divLoadingSubmit').hide();
					location.reload();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
	</script>
</html>
