<html lang="en">

<head>
    <title><?php echo $nama; ?></title>
    <style type="text/css">
         ::selection {
            background-color: #E13300;
            color: white;
        }
         ::moz-selection {
            background-color: #E13300;
            color: white;
        }
         ::webkit-selection {
            background-color: #E13300;
            color: white;
        }
        #wrapper {
            width: 100%;
            height: 100%;
            margin: auto;
        }
        #body {
            float: right;
            width: 100%;
            height: 100%;
        }
        #sidebar {
            float: left;
            width: 30%;
            height: 100%;
        }
                
        hr.style2 {
            border-top: 3px double #8c8b8b;
        }
        html {
            margin: 20px
        }
        
        #RightLine {
            text-align: right;
        }
        body {
            background-color: #fff;
            font: 10px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }
        /*
        table tr td {
            padding-top: 10px;
            padding-bottom: 10px;
            cellspacing: 0px;
        }*/

        .tablex{
            text-align: left;
            border-collapse: collapse;
        }
        
        

    </style>
	<script>    
		function cetak(){ 
            var css = '@page { size: landscape; }',
            head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');

            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet){
            style.styleSheet.cssText = css;
            } else {
            style.appendChild(document.createTextNode(css));
            }

            head.appendChild(style);

            setTimeout(function () { window.print(); }, 500);
            setTimeout(function () { 
            alert('Klik dimanapun untuk menutup halaman ini'); }, 1000);
            //window.onfocus = function () { setTimeout(function () { window.close(); }, 0); }
		}
	</script>
</head>

<body onload = "cetak()" onclick="window.close()">
<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Form BPKB</h3>
			</div>
			<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>

			<div class="panel-body">
				<table class="tablex" border="1">
					<thead>
					<tr>
						<th>No Polisi</th>
						<th>No BPKB</th>
						<th>Jenis Kendaraan</th>
						<th>Status Peminjaman</th>
						<th>Tanggal Peminjaman</th>
						<th>Tanggal Pengembalian</th>
						<th>Tanggal Pengambilan</th>
					</tr>
					</thead>
					<tbody>
					<?php
						foreach($data as $value){
							$pinjamAble = false;
							$ambilAble = false;
							$Status;
							if($value->Status==0){
								$Status = 'Tidak Dapat Dipinjam';
							}else if($value->Status == 1){
								$pinjamAble = true;
								$Status = 'Dapat Dipinjam';
							}else if($value->Status == 2){
								$Status = 'Sedang Dalam Proses Peminjaman';
							}else if($value->Status == 3){
								$Status = 'Sedang Dipinjam';
							}else if($value->Status == 4){
								$ambilAble = true;
								$Status = 'Dapat Diambil';
							}else if($value->Status == 5){
								$Status = 'Sedang Dalam Proses Pengambilan';
							}else if($value->Status == 6){
								$Status = 'Sudah Diambil';
							}else if($value->Status == 7){
								$Status = 'Tidak Dapat Diambil';
							}else if($value->Status == 8){
								$Status = 'Sudah Diterima User';
							}
							$now = new DateTime(date('Y-m-d h:i:s'));
							if($value->TanggalPeminjaman == '00/00/0000'){
								$value->TanggalPeminjaman = '-';
							}
							if($value->TanggalPengembalian == '00/00/0000'){
								$value->TanggalPengembalian = '-';
							}
							if($value->TanggalPengambilan == '00/00/0000'){
								$value->TanggalPengambilan = '-';
							}
							// $expiredPeriod = new DateTime($value->ExpiredPeriod);
							// $interval = $now->diff($expiredPeriod);
							// $sisaWaktu = $interval->format('%R%a hari');
							// $flagExp = false;
							// if($expiredPeriod<=$now){
							// 	$flagExp = true;
							// }
							// $permanent = true;
							// if($value->StatusResolved != 2 && $value->ExpiredPeriod != '0000-00-00' && $value->ExpiredPeriod > date('Y-m-d h:i:s') && $sisaWaktu < 8){
							// 	$class = "warning";
							// 	$status = "Akses Temporary Kurang dari 1 minggu dan akan dikembalikan segera";
							// 	$ambilAble = true;
							// }
							// if($value->ExpiredPeriod != '0000-00-00'){
							// 	$permanent = false;
							// }
							echo '<tr>';
							echo '<td>'.$value->NoPolisi.'</td>';
							echo '<td>'.$value->NoBPKB.'</td>';
							echo '<td>'.$value->JenisKendaraan.'</td>';
							echo '<td>'.$Status.'</td>';
							echo '<td>'.$value->TanggalPeminjaman.'</td>';
							echo '<td>'.$value->TanggalPengembalian.'</td>';
							echo '<td>'.$value->TanggalPengambilan.'</td>';
							?>
							</tr>
					<?php
						}
					?>
					</tbody>
				</table >
                <br/>
                <br/>
                <br/>
                <div>
                <table>
                    <tr>
                        <td>Tertanda</td>
                        <td width="100px"></td>
                        <td>Disetujui</td>

                    </tr>
                    <tr>
                        <td height="100px"></td>
                        <td width="100px"></td>
                        <td height="100px"></td>
                    </tr>
                    <tr>
                        <td>
                        <?php echo $nama; ?>
                        </td>
                        <td width="100px"></td>
                        <td>
                        <?php echo $atasan; ?>
                        </td>
                    </tr>
                </table>
                </div>
			</div>
		</div>
		
</body>

</html>