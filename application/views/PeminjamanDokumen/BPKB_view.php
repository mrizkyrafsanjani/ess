<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <script src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/js/autoNumeric.js" type="text/javascript"></script>
    
    <script>

    $(function($) {
        $('#field-Nominal').autoNumeric('init', { lZero: 'deny', aSep: ',', mDec: 0 });
    });

    </script>
<?php 
foreach($body->css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<?php foreach($body->js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>



</head>
<body onload="parent.alertSizeDPA(document.body.clientHeight);" >
<!-- Beginning header -->

<!-- End of header-->
    <div style='height:20px;'></div>  
    <div>
        <?php echo $body->output; ?>
    </div>
    <?php if(isset($dataTrk)): ?>
    <hr/>
    <div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">List Transaksi BPKB</h3>
			</div>
			<div class="panel-body">
				<table class="table">
					<thead>
					<tr>
						<th>No Transaksi</th>
						<th>No BPKB</th>
						<th>Kepentingan</th>
						<th>Status Approval</th>
						<th>Tanggal Transaksi</th>
						<th>Tanggal Pengembalian</th>
						<th>Tanggal Pengambilan</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php
						foreach($dataTrk as $value){
							$kembaliAble = false;
							$terimaAmbilAble = false;
							if($value->StatusApproval == 'AP2' && $value->Status == 3 && $value->StatusTerima != 2){
								if($value->Kepentingan=='Peminjaman' ||$value->Kepentingan=='PeminjamanHead'){
									$kembaliAble = true;
								}
							}
							echo '<tr>';
							echo '<td>'.$value->KodeTrk.'</td>';
							echo '<td>'.$value->NoBPKB.'</td>';
							echo '<td>'.$value->Kepentingan.'</td>';
							echo '<td>'.$value->StatusApproval.'</td>';
							echo '<td>'.$value->TanggalTransaksi.'</td>';
							echo '<td>'.$value->TanggalPengembalian.'</td>';
							echo '<td>'.$value->TanggalPengambilan.'</td>';
							echo '<td>';
								if($kembaliAble){
								?>
										<button class="btn btn-danger" onclick="konfirmasiKembaliPeminjaman('<?php echo $value->IdBPKB; ?>','<?php echo $value->KodeTrk; ?>' )"><i class="glyphicon glyphicon-triangle-left"></i> Konfirmasi Kembali Peminjaman</button>
								<?php
								}
								echo '</td>';
							echo '</tr>';
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
        <?php endif; ?>

<!-- Beginning footer -->
<div></div>
<script tipe="text/javascript">
    function konfirmasiKembaliPeminjaman(IdBPKB, KodeTrk){
			flag = true;
			if(flag){
				var x = window.confirm("Sudah Menerima Kembali BPKB ini?");
				if(x){
					$('#divLoadingSubmit').show();
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PeminjamanDokumen/BPKBController/ajax_kembaliBPKB',
						type: "POST",
						data: { IdBPKB: IdBPKB, Kepentingan: "Peminjaman", KodeTrk: KodeTrk},
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log(data);
							alert(data.message);
							$('#divLoadingSubmit').hide();
							location.reload();

						},
						error: function (request, status, error) {
							console.log(error);
							alert('Error');
						}
					});
				}else{
					event.preventDefault();
				}
			}else{
				event.preventDefault();
			}
		}
</script>
<!-- End of Footer -->
</body>
</html>