<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function alertsizeLDArchive(pixels){
			pixels+=32;
			document.getElementById('iFrameLDArchive').style.height=pixels+"px";
		}
		function copyAll(){
			var password = prompt("Konfirmasi Copy semua Reminder");
			if(password == 'yasayamaucopyuntuktahundepan'){
				alert("Semua Reminder Laporan akan di copy ke tahun depan");
				window.location.href = "<?php echo $this->config->base_url(); ?>index.php/LDArchive/ReminderLaporan/copyAllReminder";
			
			}else{
				alert("Password salah");
			}
		}
		function search(){
			var DPASelected = document.getElementById('cmbDPA').value;
			var Tahun = document.getElementById('cmbTahun').value;
			var NamaLaporan = document.getElementById('txtNamaLaporan').value;
			if(DPASelected == ''){
				DPASelected = 'non';
			}
			if(Tahun == ''){
				Tahun = 'non';
			}
			
			if(NamaLaporan == ''){
				NamaLaporan = 'non';
			}
			

			var iframe = document.getElementById('iFrameLDArchive');
			var src = '<?php echo $this->config->base_url(); ?>index.php/LDArchive/ReminderLaporan/searchAndViewReminderLaporan/'+DPASelected+'/'+Tahun+'/'+NamaLaporan;
			iframe.src = src;
		}
	</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Reminder Laporan List</h3>
		</div>
		<div class="panel-body">
		<?php echo validation_errors(); ?>
		<table border=0>
			<tr>
				<td>DPA</td>
				<td><select class="form-control" id="cmbDPA">
				<?php
					echo "<option value=''>Semua</option>";
					echo "<option value='1'>Satu</option>";
					echo "<option value='2'>Dua</option>";
				?>
				</select>
				</td>
				
			</tr>
			<tr>
				<td>Tahun</td>
				<td>
					<select class="form-control" id="cmbTahun" name="cmbTahun">
					<?php 
						echo "<option value=''>Semua</option>";
						for($i=date('Y');$i>date('Y')-2;$i--)
						{
							echo "<option value='".$i."'>".$i."</option>";
						}
					?>
					</select>
				</td>
				</td>
				<td>Nama Laporan</td>
				<td>
					<input type="text" class="form-control" id="txtNamaLaporan" name="txtNamaLaporan" placeholder="Nama Laporan"></input>
				</td>
			</tr>
			
			<tr>
				<td></td>
				<td>
					<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari">
					<input class="btn btn-light" type="button" onClick="clear()" id="btnClear" name="" value="Clear Filter">
				</td>
			</tr>
		</table>
		<iframe id="iFrameLDArchive" src="<?php echo $this->config->base_url(); ?>index.php/LDArchive/ReminderLaporan/searchAndViewReminderLaporan" width="100%" height="200px" seamless >
		  <p>Your browser does not support iframes.</p>
		</iframe>
		</div>
		<?php 
			$month = date("m");
			if($month == 12){
		?>
		<table>
			<tr>
				<td></td>
				<td>
					
					<input class="btn btn-danger" type="button" onClick="copyAll()" id="btnCopyAllEntry" name="" value="Copy All Entry">
				</td>
			</tr>
		</table>
		<?php
			}
		?>
	</body>
</html>