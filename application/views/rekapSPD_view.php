<html>
	<style type="text/css">
	@import "../assets/css/jquery-ori.datepick.css";
	@import "../assets/css/cetak.css";
	@import "../assets/css/tablesorter.css";
	</style>
	<!--<script type="text/javascript" src="../assets/js/jquery.js"></script>-->
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="../assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.numberformatter.js"></script>
	<!--<script type="text/javascript" src="../assets/js/myfunction.js"></script>-->
	<script type="text/javascript" src="../assets/js/jquery.tablesorter.js"></script> 
	<script type="text/javascript" src="../assets/js/jquery.tablesorter.pager.js"></script> 

	<script type="text/javascript">
		$(document).ready(function() {
			$(".clsUang").formatNumber({format:"#,##0", locale:"us"});
			$("#myTable")
				.tablesorter({widthFixed: true, widgets: ['zebra']})
				.tablesorterPager({container: $("#pager")});
			
			
		});
		

		function popup(url) {
			popupWindow = window.open(
				url,'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no')
		}
		</script>
	<head>
		
	</head>
	<body>
		<h1 align="center">Rekap Surat Perjalanan Dinas</h1><br/>
		<form action="rekapSPD" method="get">
		Filter berdasarkan departemen : 
		<select id="cmbDepartemen" name="cmbDepartemen">
		  <?php 
		  if($this->input->get("cmbDepartemen") === ""){
			$selected = "selected";
		  }
		  echo "<option value='0' $selected>All Departemen</option>";
		  foreach($departemen_array as $row){
		  if($this->input->get("cmbDepartemen") === $row->departemen){
			$selected = "selected";
		  }else{
			$selected = "";
		  }
			echo "<option value='".$row->departemen."' $selected>".$row->departemen."</option>";
		  }
		  ?>
          
 
             
		</select>
		 tahun 
		<select id="cmbTahun" name="cmbTahun">
			<?php 
			  if($this->input->get("cmbTahun") === ""){
				$selected = "selected";
			  }
			  echo "<option value='0' $selected>semua</option>";
			  foreach($year_array as $row){
			  if($this->input->get("cmbTahun") == $row->year){
				$selected = "selected";
			  }else{
				$selected = "";
			  }
				echo "<option value='".$row->year."' $selected>".$row->year."</option>";
			  }
			?>
		</select>
		<input class="buttonSubmitBebas" type="submit" value="Cari">
		</form>
		<br/>
		<table id="myTable" class="tablesorter" >
			<thead><tr>
				<th class="lblTop">No.</th>
				<th class="lblTop" width="150px">No. Register Surat</th>
				<th class="lblTop">Tujuan</th>
				<th class="lblTop">Alasan</th>
				<th class="lblTop" width="65px">Tanggal Berangkat</th>
				<th class="lblTop" width="65px">Tanggal Pulang</th>
				<th class="lblTop">PIC yg Berangkat</th>
				<th class="lblTop">Departemen</th>
				<th class="lblTop" width="5px">Status Lap</th>
				<th class="lblTop">Total Uang Muka</th>
				<th class="lblTop">Total Realisasi</th>
				<th class="lblTop" colspan="4" width="10px"></th>
				
			</tr></thead><tbody>
			<?php
			$counter=1;
			$total=0;
			
			foreach($table as $row){
				echo "<tr>
					<td>". $counter ."</td>
					<td>". $row->NoSPD ."</td>
					<td>". $row->Tujuan ."</td>
					<td>". $row->AlasanPerjalanan ."</td>
					<td>". $row->TanggalBerangkatSPD ."</td>
					<td>". $row->TanggalKembaliSPD ."</td>
					<td>". $row->nama ."</td>
					<td>". $row->departemen ."</td>
					<td>". $row->StatusLaporan ."</td>
					<td class='clsUang'>". $row->TotalSPD ."</td>
					<td class='clsUang'>". $row->TotalLap ."</td>
					<td> <!--<td><a href=\"JavaScript:popup('cetakFormSPDnew?NoSPD=".$row->NoSPD."');\">Cetak SPD</a></td>-->
					<td><a href=\"cetakFormSPDnew?NoSPD=".$row->NoSPD."\">Lihat SPD</a></td>";
				
				if($row->StatusLaporan == "B"){
					
					echo "<td>". anchor('EditRekapSPDnew?NoSPD='.$row->NoSPD,'Edit Rekap SPD') ."</td>"; 
					echo "<td>". anchor('laporanSPD?cmbNoSPD='.$row->NoSPD,'Buat Laporan') ."</td>
						";
					$session_data = $this->session->userdata('logged_in');

					if($session_data['koderole'] == 3 OR $session_data['koderole'] == 1 OR $session_data['koderole'] == 13){
						echo "<td>". anchor('deleteSPD?NoSPD='.$row->NoSPD,'Delete SPD',"onclick=\"return confirm('Apakah Anda yakin untuk menghapus SPD ini?')\"") ."</td>
							</tr>";
						
					}				
				}else{
					//echo "<td><a href=\"JavaScript:popup('cetakLaporanSPD?NoSPD=".$row->NoSPD."');\">Cetak Laporan</a></td>
					//	</tr>";
					//echo "<td>". anchor('EditRekapSPD?NoSPD='.$row->NoSPD,'Edit Rekap SPD') ."</td>"; 
					echo "<td colspan=\"2\"><a href=\"cetakLaporanSPD?NoSPD=".$row->NoSPD."\">Lihat Laporan</a></td>
					";
						
					$session_data = $this->session->userdata('logged_in');
						
					if($session_data['koderole'] == 3 OR $session_data['koderole'] == 1 OR $session_data['koderole'] == 13){
							echo "<td>". anchor('deleteLaporanSPD?NoSPD='.$row->NoSPD,'Batal Laporan',"onclick=\"return confirm('Apakah Anda yakin untuk membatalkan laporan ini?')\"") ."</td>
							</tr>";						
					}	
				}
				$total+=$row->TotalLap;
				$counter++;
			}
			echo "<tr><td colspan='10' align='right'><b>Grand Total  </b></td><td class='clsUang'>". $total ."</td>
			<td colspan='3' ></td></tr>";
			
			?></tbody>
		</table>
		<div id="pager" class="pager" style="top: 0px; position: inline;">
			<form>
				<img src="../assets/images/first.png" class="first">
				<img src="../assets/images/prev.png" class="prev">
				<input type="text" class="pagedisplay" size="5">
				<img src="../assets/images/next.png" class="next">
				<img src="../assets/images/last.png" class="last">
				<select class="pagesize">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="30">30</option>
					<option value="40">40</option>
					<option selected="selected" value="50">50</option>
					
				</select>
			</form>
		</div>
	</body>
</html>