<html>
	<style type="text/css">
	@import "../assets/css/jquery-ori.datepick.css";
	@import "../assets/css/cetak.css";
	</style>
	<link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
	<!--<script type="text/javascript" src="../assets/js/jquery.datepick.js"></script>-->
	<script type="text/javascript" src="../assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="../assets/js/myfunction.js"></script>
	<head>
		<title>Form SPD</title>
	<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		
</head>
	<body>
		<?php echo validation_errors(); ?>
		
		<form action="submitFormSPD" method="post" accept-charset="utf-8">
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2">Surat Perjalanan Dinas</td>
	  <td colspan="3"><image id="logoDPA" src='../assets/images/logoDPA.png'>*DANA PENSIUN ASTRA<br/>
	  <input type="radio" id="rbDpaSatu" name="rbDPA" value="1">SATU
	  <input type="radio" id="rbDpaDua" name="rbDPA" value="2">DUA
	  </td>
	</tr>
	<tr>
	  <td class="lbl">No SPD</td>
	  <td bgcolor="#D3D3D3"></td>
	  <td colspan="3" rowspan="8" align="center">
		<div id="catatan">
			<div class="lblTop">Catatan Penting</div>
			<div id="txtCatatan"><ol><li>Permintaan uang muka perjalanan dinas diajukan selambat-lambatnya 5 hari kerja sebelum melakukan perjalanan dinas.
<li>Pengeluaran uang muka paling lambat diterima 1 hari kerja sebelum keberangkatan (SPD).
<li>Pertanggungjawaban atas uang muka untuk biaya-biaya perjalanan dinas wajib dilaporkan selambat-lambatnya 3 (tiga) hari setelah tiba di tempat kerja.
</ol>
</div>
      </td>
	</tr>
	<tr>
	  <td class="lbl">Tanggal Pengajuan SPD</td>
	  <td><?php echo date("j F Y"); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">NPK</td>
	  <td><input type="text" class='bordered' id="txtNPK" name="txtNPK" value="<?php echo $npk ?>" readonly /></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Golongan</td>
	  <td><?php echo $golongan ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Nama</td>
	  <td><?php echo $nama ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Jabatan</td>
	  <td><?php echo $jabatan ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Departemen</td>
	  <td><?php echo $departemen ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Atasan </td> 
	  <td> <?php echo $atasan ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">*Tanggal Keberangkatan</td>
	  <td colspan="4">
    <!--<input  class='bordered' id="txtTanggalBerangkat" name='txtTanggalBerangkat' type="text" size="15" >-->
	    
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalBerangkat" name='txtTanggalBerangkat'>
        </div>
        <!-- /.input group -->
      
    </td>
	  
	</tr>
	<tr>
	  <td class="lbl">*Tanggal Kembali</td>
	  <td colspan="4"><!--<input  class='bordered'  id="txtTanggalKembali" name='txtTanggalKembali' type="text" size="15" >-->
      <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembali" name='txtTanggalKembali'>
      </div>
    </td>
	</tr>
	<tr>
	  <td class="lbl">*Tujuan</td>
	  <td colspan="4"><input id="txtTujuan" class="form-control" name='txtTujuan' type="text" size="50%" ></td>
	</tr>
	<tr>
	  <td class="lbl">*Alasan Perjalanan</td>
	  <td colspan="4" valign="top"><textarea class="form-control" id="txtAlasan" name='txtAlasan' type="text" cols="60" rows="3" ></textarea></td>
	</tr>
  </tbody>
</table>
<br/>
*Apakah ingin mengajukan uang muka 

<input class="clsUangMuka" type="radio" id="rbYa" name="rbUangMuka" value="1">Ya
<input class="clsUangMuka" type="radio" id="rbTidak" name="rbUangMuka" value="0">Tidak

<!-- part untuk perkiraaan biaya -->
<table id="tblPerkiraanBiaya" border=0>
  <tbody>
    <tr class="tblHeader">
		<td colspan="5">Perkiraan Biaya 
		<input class="clsUangMuka" type="radio" id="rbInternasional" name="rbRegion" value="1">Internasional
		<input class="clsUangMuka" type="radio" id="rbNasional" name="rbRegion" value="0">Nasional
		</td>      
    </tr>
    <tr>
      <td class="lbl">Jenis Biaya</td>
      <td id="keterangan" class="lblTop">Keterangan</td>
      <td class="lblTop">Beban Harian</td>
      <td class="lblTop">#Jml Hari</td>
      <td class="lblTop">Total Perkiraan Biaya</td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Saku</td>
      <td><input id="txtKetUangSaku"  class='bordered' name='txtKetUangSaku' type="text" size="40"></td>
      
      <td class='clsUang'><input class='bordered' id="txtBebanHarianUangSaku" name='txtBebanHarianUangSaku' type="text" size="15" value="<?php echo $uangsaku ?>"   readonly></td>
      <td class='clsHari' bgcolor="#D3D3D3" ><input class='defult' id="txtJmlhHariUangSaku" name='txtJmlhHariUangSaku' type="text" size="5"></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtTotalUangSaku" name='txtTotalUangSaku' type="text" size="15"></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Makan</td>
      <td><input id="txtKetUangMakan"  class='bordered' name='txtKetUangMakan' type="text" size="40"></td>
	  <?php 
		$readonly = "";
		if($golongan <= 5){
			$readonly = "readonly";
		}
	  ?>
      <td class='clsUang'><input  class='bordered' id="txtBebanHarianUangMakan" name='txtBebanHarianUangMakan' type="text" size="15" value=<?php echo $uangmakan ?>  <?php echo $readonly; ?> ></td>
      <td class='clsHari' bgcolor="#D3D3D3" ><input  class='defult' id="txtJmlhHariUangMakan" name='txtJmlhHariUangMakan' type="text" size="5"></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtTotalUangMakan" name='txtTotalUangMakan' type="text" size="15"></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;*Akomodasi</td>
      <td><input id="txtKetHotel"   class='bordered' name='txtKetHotel' type="text" size="40"></td>
      <td class='clsUang'><input class='bordered' id="txtBebanHarianHotel" name='txtBebanHarianHotel' type="text" size="15"  ></td>
      <td class='clsHari'><input class='bordered' id="txtJmlhHariHotel" name='txtJmlhHariHotel' type="text" size="5" ></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtTotalHotel" name='txtTotalHotel' type="text" size="15"></td>
    </tr>
    <tr>
      <td><br/></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="lbl"><i>Transportasi</i></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Taksi</td>
      <td><input id="txtKetTaksi" class='bordered' name='txtKetTaksi' type="text" size="40"></td>
      <td class='clsUang'><input class='bordered' id="txtBebanHarianTaksi" name='txtBebanHarianTaksi' type="text" size="15"  ></td>
      <td></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtTotalTaksi" name='txtTotalTaksi' type="text" size="15"></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Airport Tax</td>
      <td><input id="txtKetAirport"  class='bordered' name='txtKetAirport' type="text" size="40"></td>
      <td class='clsUang'><input class='bordered' id="txtBebanHarianAirport" name='txtBebanHarianAirport' type="text" size="15"  ></td>
      <td></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtTotalAirport" name='txtTotalAirport' type="text" size="15"></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl"><i>Lain-lain</i></td>
      <td><input id="txtKetLain1" class='bordered'  name='txtKetLain1' type="text" size="40"></td>
      <td class='clsUang'><input class='bordered' id="txtBebanHarianLain1" name='txtBebanHarianLain1' type="text" size="15"  ></td>
      <td></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtTotalLain1" name='txtTotalLain1' type="text" size="15"></td>
    </tr>
    <tr class='clsInputNumber'>
      <td></td>
      <td><input id="txtKetLain2"  class='bordered' name='txtKetLain2' type="text" size="40"></td>
      <td class='clsUang'><input class='bordered' id="txtBebanHarianLain2" name='txtBebanHarianLain2' type="text" size="15"  ></td>
      <td></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtTotalLain2" name='txtTotalLain2' type="text" size="15"></td>
    </tr>
    <tr class='clsInputNumber'>
      <td></td>
      <td><input id="txtKetLain3" class='bordered' name='txtKetLain3' type="text" size="40"></td>
      <td class='clsUang'><input class='bordered' id="txtBebanHarianLain3" name='txtBebanHarianLain3' type="text" size="15"  ></td>
      <td></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtTotalLain3" name='txtTotalLain3' type="text" size="15"></td>
    </tr>
    <tr class='clsInputNumber'>
      <td></td>
      <td></td>
      <td class='lbl' colspan="2"><b>Grand Total</b></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtGrandTotal" name='txtGrandTotal' type="text" size="15"></td>
    </tr>
    <tr>
      <td class="lbl">Total Pengajuan</td>
      <td colspan="4" class='clsUang' bgcolor="#D3D3D3" ><input class='defult'  type="text" id="lblTotalPengajuan"></td>
    </tr>
	<!--<tr>
      <td>Terbilang</td>
      <td colspan="4"><label id="lblTerbilang"></label></td>
    </tr>-->
  </tbody>
</table>
<br/>
<!-- Permintaan Uang Muka -->
<table id="tblUangMuka" border=0>
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Permintaan Uang Muka</td>
    </tr>
    <tr>
      <td colspan="5"><br/></td>
    </tr>
    <tr>
      <td width="200px" class="lbl">Nama Pemohon</td>
      <td colspan="2" width="400px"><?php echo $nama; ?></td>
	  <td align="right" colspan="2" width="200px"><b>Tanggal : </b><?php echo date("j F Y"); ?></td>
    </tr>
    <tr>
      <td class="lbl">Departemen</td>
      <td colspan="2"><?php echo $departemen; ?></td>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td class="lbl">Jumlah Permintaan</td>
      <td colspan="2"><label class='clsUang' id="lblJmlhPermintaan"></label></td>
      <td colspan="2"></td>
    </tr>
    <!--<tr>
      <td>Terbilang</td>
      <td colspan="2"><input type="text" id="lblTerbilang2" disabled></td>
      <td></td>
    </tr>-->
    <tr>
      <td class="lbl">Keperluan</td>
      <td colspan="2"><label id="lblKeperluan"></label></td>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td class="lbl">Tujuan</td>
      <td colspan="2"><label id="lblTujuan"></label></td>
      <td colspan="2"></td>
    </tr>
  </tbody>
</table>
<br/>
<!-- tanda tangan -->
<table id="tblTandaTangan" border=0>
  <tbody>
    <tr class="ttd">
    <td rowspan ="2">Pemohon</td>
      <td colspan="3">Menyetujui</td>
    </tr>
    <tr class="ttd">
      <td><?php 
		if($atasan==$nama)
		{
			echo "DIC HRGA";
		}
		else if($atasan=='')
		{ 
			echo "Chief"; 
		}
		else
		{ 
			echo "Atasan"; 
		}
		?></td>
      <td>HRGA Dept Head</td>
    </tr>
    <tr class="ttd">
      <td><br/><br/><br/><br/><br/></td>
      <td></td>
      <td></td>
    </tr>
    <tr class="ttd">
      <td><?php echo $nama ?></td>
      <td><?php echo $atasan==$nama?"":$atasan; ?></td>
      <td>Eviyati</td>
    </tr>
  </tbody>
</table>
<div id="note">
</div>

<!--pelimpahan wewenang -->
        <BR><BR>
        *Apakah disertai dengan Pelimpahan Wewenang?

				<input class="clsPelimpahanWewenang" type="radio" id="rbYaPW" name="rbPelimpahanWewenang" value="1">Ya
				<input class="clsPelimpahanWewenang" type="radio" id="rbTidakPW" name="rbPelimpahanWewenang" value="0">Tidak				
				
				<table><tr><td>
        <iframe id="iFrameSPDPelimpahanWewenang" src="<?php echo $this->config->base_url(); ?>index.php/PelimpahanWewenang/PelimpahanWewenang/index/" width="100%" height="400px" seamless frameBorder="0">
					<p>Your browser does not support iframes.</p>
				</iframe>
        </td></tr>
        </table>

				<div id=txtPelimpahanWewenang>
				*
				Mohon dijelaskan secara singkat dan jelas dasar dari adanya pelimpahan wewenang, dan jangka waktu 
				dari pelimpahan wewenang tersebut. <BR> Apabila dibutuhkan dokumen pendukung lainnya, mohon dilampirkan 
				bersamaan dengan formulir ini (ex : URF, dll)
				<br>
				*Apakah disertai dengan pemberian notifikasi pada email dan/atau pelimpahan untuk email yang masuk?
				<input class="clsEmail" type="radio" id="rbEmailYa" name="rbEmailPelimpahanWewenang" value="1">Ya
				<input class="clsEmail" type="radio" id="rbEmailTidak" name="rbEmailPelimpahanWewenang" value="0">Tidak				

				</div>

				<div id=txtPelimpahanWewenangEmail>
				<br>
				Selama saya Perjalanan Dinas, untuk semua email yang masuk pada email saya, dialihkan kepada:
				<table id="tblEmail" border=0>
				<tbody>
				    <tr>					
					<td width="80px" class="lbl">Nama </td>
					<td width="300px"><input id="txtNamaEmail1" class="form-control" name='txtNamaEmail1' type="text" size="20" ></td>					
					<td width="80px" class="lbl">Email </td>
					<td width="400px"><input id="txtAlamatEmail1" class="form-control" name='txtAlamatEmail1' type="text" size="20" ></td>						
					</tr>
					<tr>					
					<td width="80px" class="lbl">Nama </td>
					<td width="300px"><input id="txtNamaEmail2" class="form-control" name='txtNamaEmail2' type="text" size="20" ></td>					
					<td width="80px" class="lbl">Email </td>
					<td width="400px"><input id="txtAlamatEmail2" class="form-control" name='txtAlamatEmail2' type="text" size="20" ></td>						
					</tr>
					<tr>					
					<td width="80px" class="lbl">Nama </td>
					<td width="300px"><input id="txtNamaEmail3" class="form-control" name='txtNamaEmail3' type="text" size="20" ></td>					
					<td width="80px" class="lbl">Email </td>
					<td width="400px"><input id="txtAlamatEmail3" class="form-control" name='txtAlamatEmail3' type="text" size="20" ></td>						
					</tr>
					<tr>					
					<td width="80px" class="lbl">Nama </td>
					<td width="300px"><input id="txtNamaEmail4" class="form-control" name='txtNamaEmail4' type="text" size="20" ></td>					
					<td width="80px" class="lbl">Email </td>
					<td width="400px"><input id="txtAlamatEmail4" class="form-control" name='txtAlamatEmail4' type="text" size="20" ></td>						
					</tr>
					<tr>					
					<td width="80px" class="lbl">Nama </td>
					<td width="300px"><input id="txtNamaEmail5" class="form-control" name='txtNamaEmail5' type="text" size="20" ></td>					
					<td width="80px" class="lbl">Email </td>
					<td width="400px"><input id="txtAlamatEmail5" class="form-control" name='txtAlamatEmail5' type="text" size="20" ></td>						
					</tr>					
				</tbody>
				</table>
				</div>

<!-- END -->


    <table class="tblPureNoBorder">
      <tr><td>
        <input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin menyimpan SPD ini ke dalam database?')" id="btnSubmit" name="submitFormSPD" value="Simpan">
        <a href="formSPD"><input class="btn btn-default" type="button" value="Batal"></a>			
			</td></tr>
      <tr><td><div id="divError" class="alert alert-danger"></div></td></tr>
    </table>
			<br/>
		</form>
    <script>
      $(function(){
      //Date picker
          $('#txtTanggalBerangkat').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
          });
          $('#txtTanggalKembali').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
          });

          $("#iFrameSPDPelimpahanWewenang").hide(1000);
          $("#txtPelimpahanWewenang").hide(1000);
          $("#txtPelimpahanWewenangEmail").hide(1000);

          //pelimpahan wewenang
          $("#rbYaPW").click(function(){
            $("#iFrameSPDPelimpahanWewenang").show(1000);
            $("#txtPelimpahanWewenang").show(1000);			
          });
          
          $("#rbTidakPW").click(function(){
              $("#iFrameSPDPelimpahanWewenang").hide(1000);
              $("#txtPelimpahanWewenang").hide(1000);
              $("#txtPelimpahanWewenangEmail").hide(1000);
          });

          $("#rbEmailYa").click(function(){
              $("#txtPelimpahanWewenangEmail").show(1000);					
          });
          
          $("#rbEmailTidak").click(function(){
              $("#txtPelimpahanWewenangEmail").hide(1000);
          });

      });      
    </script>
    <script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
    
	</body>
</html>