<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetakFormCuti.css";
	</style>



	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript">
	
		function printDiv(divName) {
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
		//	window.print();
			document.body.innerHTML = originalContents;
		}

		$(function() {
			
			$('.buttonSubmit').click(function (){				
				$('#printArea').printElement({
					//leaveOpen: true,
					pageTitle:'Form Cuti',
					printMode:'popup',
					overrideElementCSS:[
						'<?php echo $this->config->base_url(); ?>assets/css/cetak.css','<?php echo $this->config->base_url(); ?>assets/css/style-common.css']
				});
			});
			
			
			
		});
		
		

	</script>
	<head>
		<title>Form Pelimpahan Wewenang</title>
	</head>
	<body >
	<br/>
	

	<BR><BR>
	<div id='printArea'>
		
			<table border=0>
				<tbody>
				<!-- Results table headers -->
				<tr class="tblHeader">	  
					<td colspan="1"><image id="header" height="100" width="920" src='<?php echo $this->config->base_url(); ?>assets/images/header_dpa.jpg'><br/>	
				</tr>
				
				</tbody>
			</table>
			<br>
			<div class=perihal>
				<p><B>Pelimpahan Wewenang</B><BR>
				<B><?php echo $KodePelimpahanWewenang ;?></B></p>
			</div>
			<BR><BR>
				Sehubungan dengan pengajuan izin/cuti saya selama <u><?php echo $jumlahHariGlobal; ?></u> hari,terhitung sejak tanggal <u><?php echo $TanggalMulai;?> </u> sampai dengan <u><?php echo $TanggalSelesai; ?></u> <br> 
				sebagaimana dibuktikan dengan formulir izin/cuti terlampir, maka wewenang atas : <br>
					<?php for($i=0;$i<count($dataPelimpahanWewenangDetail);$i++){					 									
							echo $dataPelimpahanWewenangDetail[$i]['Keterangan'] .'<br />' ; 	
							echo 'Akan dilimpahkan kepada : '. $dataPelimpahanWewenangDetail[$i]['NamaYangDilimpahkan'] . '<br />' ;										  
					}?> 
				<br>
				<!--Mohon dijelaskan secara singkat dan jelas dasar dari adanya pelimpahan wewenang, penerima pelimpahan wewenang, 
				dan jangka waktu dari pelimpahan wewenang tersebut.-->

				<?php 
				if($flagemail=='2'){
				?>
						Selama saya izin/cuti, mohon disediakan fitur balas otomatis pada email saya, dengan format :<br>
						<i>
						Thank you for your email.<br>
						I am leave till <u><?php echo $TanggalSelesai; ?></u>. I will read and reply the email when return to office. <br>
						For urgent matter, please contact 	
						<?php for($i=0;$i<count($dataEmailPelimpahanWewenangDetail);$i++){
							if ( $i==count($dataEmailPelimpahanWewenangDetail)-1) 								
								echo $dataEmailPelimpahanWewenangDetail[$i]['Nama'] . ' at ' . $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju'] . '.';
							else
								echo $dataEmailPelimpahanWewenangDetail[$i]['Nama'] . ' at ' . $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju'] . ' or '; 
							}
						?> <br>
						Thanks and Regards <br>
						<?php echo $NamaYangMelimpahkan; ?> 
						</i> <br><br>

						Selama saya izin/cuti, untuk semua email yang masuk pada email saya, dialihkan kepada : <br>
						<?php for($i=0;$i<count($dataEmailPelimpahanWewenangDetail);$i++){
							if ( $i==count($dataEmailPelimpahanWewenangDetail)-1)
								echo  $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju']  ;
							else
							echo  $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju'] . ',' ;
						}
						?>
				<?php 
				}
				?>
				<BR><BR>
				<!-- tanda tangan -->
				<table border="1">			
					<tr class="ttdPW">
					<td rowspan="2">Dibuat </td>
					<td >Diketahui</td>
					<td >Disetujui</td>
					</tr>
					<tr class="ttdPW">
					<td>Penerima Pelimpahan Wewenang</td>
					<td>Atasan</td>
					</tr>
					<tr class="ttdPW">
					<td><br/><br/><br/><br/><br/></td>
					<td></td>
					<td></td>
					</tr>
					<tr class="ttdPW">
					<td><?php echo $NamaYangMelimpahkan; ?></td>
					<td>
					<?php for($i=0;$i<count($dataPelimpahanWewenangDetail);$i++){						 									
						if ( $i == count($dataPelimpahanWewenangDetail)-1) 
							echo $dataPelimpahanWewenangDetail[$i]['NamaYangDilimpahkan']   ;	
						else
							echo $dataPelimpahanWewenangDetail[$i]['NamaYangDilimpahkan'] . ' , ' ;											  
					}?> 
					</td>
					<td><?php echo $dataAtasan['Atasan']; ?></td>
					</tr>	
				</table>

				<BR><BR>
				*Mohon untuk dilampirkan formulir URF terkait pelimpahan wewenang ini (jika diperlukan).
		
	</div>

	</body>
</html>