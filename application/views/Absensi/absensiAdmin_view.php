<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function alertsizeAbsensi(pixels){
			pixels+=32;
			document.getElementById('iFrameAbsensi').style.height=pixels+"px";
		}
		
		function search(){
			var npkKaryawanSelected = document.getElementById('txtNamaKaryawan').value;
			var tahun = document.getElementById('cmbTahun').value;
			var bulan = document.getElementById('cmbBulan').value;
			var npk = '<?php echo $npk; ?>';
			
			$.post(
				'<?php echo $this->config->base_url(); ?>index.php/Absensi/AbsensiController/ajax_GetKehadiran',
				 { npkKaryawan: npkKaryawanSelected, tahun:tahun, bulan:bulan, npk:npk },
				 function(response) {  					
					   $('#kehadiran').html(response['kehadiran']);
					   $('#izin').html(response['izin']);
					   $('#cuti').html(response['cuti']);
					   $('#sakit').html(response['sakit']);
					   $('#perjalanandinas').html(response['perjalanandinas']);
				 },
				 "json"
			);
			
			if(npkKaryawanSelected === '' || npkKaryawanSelected === null)
				npkKaryawanSelected = 'non';
			
			var iframe = document.getElementById('iFrameAbsensi');
			if(npk === '')
			{
				iframe.src = '<?php echo $this->config->base_url(); ?>index.php/Absensi/Absensi/index/'+tahun+'/'+bulan+'/'+npkKaryawanSelected;
			}
			else
			{
				iframe.src = '<?php echo $this->config->base_url(); ?>index.php/Absensi/Absensi/user/'+tahun+'/'+bulan+'/'+npkKaryawanSelected+'/'+npk;
			}
		}
	</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Absensi Karyawan</h3>
		</div>
		<div class="panel-body">
		<?php echo validation_errors(); ?>
		<table border=0>
			<?php 
				if($npk == ''){
			?>
			<tr>
				<td>Nama Karyawan *</td>
				<td><select class="form-control select2" id="txtNamaKaryawan">
				<?php foreach($databawahan as $row){
					echo "<option value='".$row->NPK."'>".$row->Nama."</option>";
				} ?>
				</select></td>
			</tr>
			<?php }else{ ?>
			<tr>
				<td></td>
				<td><input type="hidden" class="form-control" id="txtNamaKaryawan"></td>
			</tr>
			<?php } ?>
			<tr>
				<td>Tahun</td>
				<td>
					<select class="form-control" id="cmbTahun" name="cmbTahun">
					<?php 
						for($i=date('Y');$i>date('Y')-2;$i--)
						{
							echo "<option value='".$i."'>".$i."</option>";
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Bulan</td>
				<td>
					<select class="form-control" id="cmbBulan" name="cmbBulan">
					<?php 
						for($i=1;$i<=12;$i++)
						{
							$selected = '';
							if(date('n') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari">
				</td>
			</tr>
		</table>
		<?php 
			if($npk == ''){
		?>
		<iframe id="iFrameAbsensi" src="<?php echo $this->config->base_url(); ?>index.php/Absensi/Absensi/index/non/non/non" width="100%" height="200px" seamless >
		  <p>Your browser does not support iframes.</p>
		</iframe>
		<?php }else{ ?>
		<iframe id="iFrameAbsensi" src="<?php echo $this->config->base_url(); ?>index.php/Absensi/Absensi/index/non/non/non/<?php echo $npk; ?>" width="100%" height="200px" seamless >
		  <p>Your browser does not support iframes.</p>
		</iframe>		
		<?php } ?>
		<table border=0>
			<tr>
				<td>Kehadiran</td>
				<td><div id = 'kehadiran'></div><?php echo $kehadiran; ?></td>
			</tr>
			<tr>
				<td>Izin</td>
				<td><div id = 'izin'></div><?php echo $izin; ?></td>
			</tr>
			<tr>
				<td>Cuti</td>
				<td><div id = 'cuti'></div><?php echo $cuti; ?></td>
			</tr>
			<tr>
				<td>Sakit</td>
				<td><div id = 'sakit'></div><?php echo $sakit; ?></td>
			</tr>
			<tr>
				<td>Perjalanan Dinas</td>
				<td><div id = 'perjalanandinas'></div><?php echo $perjalanandinas; ?></td>
			</tr>
		</table>
		</div>
		</div>
	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $(".select2").select2();
    </script>
</html>
