<html>


	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url(); ?>assets/css/style-common.css">
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.js"></script>

	<script type="text/javascript">
		$(function() {
			$('#lupaPassword').click(function(){
				if($('#npk').val()==""){
					alert('Harap isi NPK terlebih dahulu');
				}else{
					$('#divError').html("<img src='<?php echo $this->config->base_url(); ?>assets/images/ProgressAnim.gif'> <font size='2px'>Tunggu sebentar...</font>");
					$.ajax({
					  type: "POST",
					  url: "<?php echo $this->config->base_url(); ?>index.php/lupaPassword",
					  data: { npk: $('#npk').val() }
					}).done(function( msg ) {
						$('#divError').html(msg);
					  //alert("Email reset password sudah terkirim ke alamat email Anda");
					});
				}
			});
		});
	</script>
	<head>
		<title>Enterprise Self Service Login</title>
		<link rel="shortcut icon" href=<?php echo str_replace('index.php/','',site_url("favicon.ico")); ?> />
	</head>
	<body>
		<div id="tv-bg-frame"></div>
		<div id="tv-topbar"></div>
		<div id="container" style="border: none !important;">
			<div id="leftcontent">
			<img src=<?php echo str_replace('index.php/','',site_url("assets/images/ess_image.jpg")); ?> alt="Enterprise Self Service Image">
			</div>
			<div id="rightcontent">
				<div id='tv-top-logo'><br/><br/><div style="float:right; color:grey !important;">Enterprise Self Service</div></div>
				<!--<image id="logoDPA" src='../assets/images/LogoDPA.png' style="vertical-align: middle;">
				<span style="vertical-align: middle;"><font size="4px" color="grey">Dana Pensiun Astra</font></span>-->
				<br/><br/><br/><br/>
				<?php echo validation_errors(); ?>				
				<form action="<?php echo $this->config->base_url(); ?>index.php/verifylogin?u=<?php echo $urlTujuan; ?>" method="post" accept-charset="utf-8" autocomplete="off">
				
					<table id="tblLogin">
						<tr>
							<td align="right">
								<label for="npk">NPK :&nbsp;</label><br/>
							</td>
							<td>
								<input class="textbox" type="text" size="20" id="npk" name="npk" autocomplete="off" /><br/>
							</td>
						</tr>
						<tr>
							<td align="right">
								<label for="password">Password :&nbsp;</label>
							</td>
							<td>
								<input class="textbox" type="password" size="20" id="password" name="password" autocomplete="off" />
							</td>
						</tr>
						<tr>
							<td colspan="2" align="right" style="font-size:12px; color:grey; font-style:none;">
								<a id="lupaPassword" href="#">Lupa Password?</a>
							</td>
						</tr>
						<tr>
							<td  colspan="2">
								
								<input class="buttonSubmit" type="submit" value="Login" />
							</td>
						</tr>
						<tr>
							<td  colspan="2">
								<div id="divError"></div>
							</td>
						</tr>
						<td colspan="2" align="right" style="font-size:12px; color:grey; font-style:none;">
							<br/><br/><br/><br/><br/><br/><br/><br/>
							<a id="resetPasswordWindows" href="<?php echo $this->config->base_url(); ?>index.php/ResetPasswordWindows/resetPasswordWindows">Reset Password Windows</a>
						</td>
					</table>
				</form>
				
			</div>
		</div>
		<div id="footer" style="text-align: center"><div id='copyright'>BPIT DPA &copy;2013 - <?php echo strftime("%Y"); ?></div></div>
	</body> 	
</html>