<html>
	<style type="text/css">
	@import "../assets/css/jquery-ori.datepick.css";
	@import "../assets/css/cetak.css";
	</style>
	<!--<script type="text/javascript" src="../assets/js/jquery.js"></script>-->
	<script type="text/javascript" src="../assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="../assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="../assets/js/myfunction.js?v=2.17"></script>
	
	<head>
		<title>Laporan SPD</title>
	</head>
	<body>
	<?php echo validation_errors(); ?>
	<form action="submitUpdateSPD" method="post" accept-charset="utf-8">
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr  class="tblHeader">
	  <td colspan="2">Laporan Perjalanan Dinas</td>
          <td colspan="3"><image id="logoDPA" src='../assets/images/logoDPA.png'>*DANA PENSIUN ASTRA<br/>
		  <?php 
			$checked1 = "";
			$checked2 = "";
			if($trkSPD_array[0]['DPA'] == 1){
				$checked1 = "checked";
			}else{
				$checked2 = "checked";
			}
		  ?>
          <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked1; ?> >SATU
          <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked2; ?> >DUA
		  
		  <input class="clsUangMuka" type="radio" id="rbYa" name="rbUangMuka" value="1" checked hidden>
          </td>
	</tr>
	<tr>
	  <td class="lbl">No SPD</td>
	  <td><input id="txtNoSPD" name='txtNoSPD' type="text" size="30" value="<?php echo $trkSPD_array[0]['nospd']; ?>" readonly="true"></td>
	  
	  <td colspan="3" rowspan="10" align="center">
		<div id="catatan">
			<div class="lblTop">Catatan Penting</div>
				<div id="txtCatatan"><ol><li>Pertanggungjawaban atas uang muka untuk biaya perjalanan dinas wajib dilaporkan selambat-lambatnya 3 (tiga) hari setelah tiba di tempat kerja.</li>
<li>Pengembalian kelebihan ataupun kekurangan uang muka diterima/diserahkan 2 hari kerja setelah dokumen lengkap dan disetujui oleh HRD.</li>
<li>Semua bukti pengeluaran harus dilampirkan:</li>
</ol>
				<ul>
					<li>-Invoice Biaya penginapan (Hotel) Asli</li>
					<li>-Airport tax</li>
					<li>-Kwitansi deklarasi Taksi</li>
					<li>-dll (Berkas yang terkait)</li>
				</ul>
				</div>
		</div>
      </td>
	</tr>
	<tr>
	  <td class="lbl">Tanggal Laporan</td>
	  <td><?php echo date("j F Y"); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">NPK</td>
	  <td><?php echo $trkSPD_array[0]['npk']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Golongan</td>
	  <td><?php echo $trkSPD_array[0]['golongan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Nama</td>
	  <td><?php echo $trkSPD_array[0]['nama']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Jabatan</td>
	  <td><?php echo $trkSPD_array[0]['jabatan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Departemen</td>
	  <td><?php echo $trkSPD_array[0]['departemen']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Atasan</td>
	  <td><?php echo $trkSPD_array[0]['atasan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">*Tanggal Keberangkatan</td>
	  <td><input id="txtTanggalBerangkat" name='txtTanggalBerangkat' type="text" size="15" value="<?php echo $trkSPD_array[0]['tanggalberangkatspd']; ?>" ></td>
	  
	</tr>
	<tr>
	  <td class="lbl">*Tanggal Kembali</td>
	  <td><input id="txtTanggalKembali" name='txtTanggalKembali' type="text" size="15" value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>" ></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tujuan</td>
	  <td colspan="4"><input id="tujuan" name='txtTujuan' type="text" size="15" value="<?php echo $trkSPD_array[0]['tujuan']; ?>"></td>
	</tr>
	<tr>
	  <td class="lbl">Alasan Perjalanan</td>
	  <td colspan="4" valign="top"><input id="tujuan" name='txtAlasan' type="text" size="15" value="<?php echo $trkSPD_array[0]['alasan']; ?>"></td>
	</tr>
  </tbody>
</table>
<br/>
<!-- part untuk perkiraaan biaya -->
<table border=0>
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Perkiraan Biaya
	  <input class="clsUangMuka" type="radio" id="rbInternasional" name="rbRegion" value="1">Internasional
		<input class="clsUangMuka" type="radio" id="rbNasional" name="rbRegion" value="0">Nasional
	  </td>
    </tr>
    <tr>
      <td class="lbl" width="170px">Jenis Biaya</td>
      <td id="keterangan" class="lblTop">Keterangan</td>
      <td class="lblTop">Beban Harian</td>
      <td class="lblTop">#Jml Hari</td>
      <td class="lblTop">Total Perkiraan Biaya</td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Saku</td>
      <td><input id="txtKetUangSaku" name='txtKetUangSaku' type="text" size="40" value="<?php echo $trkSPD_array[0]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianUangSaku" name='txtBebanHarianUangSaku' type="text" size="15" value="<?php 
		if($trkSPD_array[0]['uangmuka'] == 0){
			echo $dataUser['uangsaku'];
		}else{
			echo $trkSPD_array[0]['bebanharianspd']; 
		}
	?>"   readonly></td>
      <td class='clsHari'><input id="txtJmlhHariUangSaku" name='txtJmlhHariUangSaku' type="text" size="5" value="<?php echo $trkSPD_array[0]['jumlahharispd']; ?>" ></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalUangSaku" name='txtTotalUangSaku' type="text" size="15" value="<?php echo $trkSPD_array[0]['bebanharianspd']*$trkSPD_array[0]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Makan</td>
      <td><input id="txtKetUangMakan" name='txtKetUangMakan' type="text" size="40" value="<?php echo $trkSPD_array[1]['keteranganspd']; ?>" ></td>
	  <?php 
		$readonly = "";
		if($trkSPD_array[0]['golongan'] <= 5){
			$readonly = "readonly";
		}
	  ?>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianUangMakan" name='txtBebanHarianUangMakan' type="text" size="15" value="<?php 
		if($trkSPD_array[0]['uangmuka'] == 0){
			echo $dataUser['uangmakan'];
		}else{
			echo $trkSPD_array[1]['bebanharianspd']; 
		}		
	?>"  <?php echo $readonly; ?> ></td>
      <td class='clsHari'><input id="txtJmlhHariUangMakan" name='txtJmlhHariUangMakan' type="text" size="5" value="<?php echo $trkSPD_array[1]['jumlahharispd']; ?>" ></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalUangMakan" name='txtTotalUangMakan' type="text" size="15" value="<?php echo $trkSPD_array[1]['bebanharianspd']*$trkSPD_array[1]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;*Akomodasi</td>
      <td><input id="txtKetHotel" name='txtKetHotel' type="text" size="40" value="<?php echo $trkSPD_array[2]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianHotel" name='txtBebanHarianHotel' type="text" size="15" value="<?php echo $trkSPD_array[2]['bebanharianspd']; ?>" ></td>
      <td class='clsHari'><input id="txtJmlhHariHotelLap" name="txtJmlhHariHotelLap" type="text" size="5" value="<?php echo $trkSPD_array[2]['jumlahharispd']; ?>" ></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalHotelLap" type="text" size="15" value="<?php echo $trkSPD_array[2]['bebanharianspd']*$trkSPD_array[2]['jumlahharispd']; ?>" ></td>
    </tr>
	<tr class='clsInputNumber'>
      <td class="lbl"></td>
      <td><input id="txtKetHotel2" name="txtKetHotel2" type="text" size="40" value="" ></td>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianHotel2" name="txtBebanHarianHotel2" type="text" size="15" value="" ></td>
      <td class='clsHari'><input id="txtJmlhHariHotel2" name="txtJmlhHariHotel2" type="text" size="5" value="" ></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalHotel2" name="txtTotalHotel2" type="text" size="15" value="" ></td>
    </tr>
    <!--<tr>
      <td><br/></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>-->
    <tr>
      <td class="lbl"><i>Transportasi</i></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr class='clsInputNumber'>
      <td  class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Taksi</td>
      <td><input id="txtKetTaksi" name='txtKetTaksi' type="text" size="40" value="<?php echo $trkSPD_array[4]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianTaksi" name='txtBebanHarianTaksi' type="text" size="15" value="<?php echo $trkSPD_array[4]['bebanharianspd']; ?>" ></td>
      <td></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalTaksi" name='txtTotalTaksi' type="text" size="15" value="<?php echo $trkSPD_array[4]['bebanharianspd']*$trkSPD_array[4]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Airport Tax</td>
      <td><input id="txtKetAirport" name='txtKetAirport' type="text" size="40" value="<?php echo $trkSPD_array[5]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='clsBebanHarian' id="txtBebanHarianAirport" name='txtBebanHarianAirport' type="text" size="15" value="<?php echo $trkSPD_array[5]['bebanharianspd']; ?>" ></td>
      <td></td></td>
      <td class='clsUang'><input class='clsTotal' id="txtTotalAirport" name='txtTotalAirport' type="text" size="15" value="<?php echo $trkSPD_array[5]['bebanharianspd']*$trkSPD_array[5]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl"><i>Lain-lain</i></td>
	  <?php 
		$counter = 1;
		for($i=6;$i<count($trkSPD_array);$i++){
			echo "<td><input id=\"txtKetLain".$counter."\" name='txtKetLain".$counter."' type=\"text\" size=\"40\" value='". $trkSPD_array[$i]['keteranganspd'] ."' ></td>
			  <td class='clsUang'><input class='clsBebanHarian' id=\"txtBebanHarianLain".$counter."\" name='txtBebanHarianLain".$counter."' type=\"text\" size=\"15\" value='". $trkSPD_array[$i]['bebanharianspd'] ."' ></td>
			  <td></td>
			  <td class='clsUang'><input class='clsTotal' id=\"txtTotalLain".$counter."\" name='txtTotalLain".$counter."' type=\"text\" size=\"15\" value='" . $trkSPD_array[$i]['bebanharianspd']*$trkSPD_array[$i]['jumlahharispd'] . "' ></td>
			</tr>
			<tr class='clsInputNumber'>
				<td></td>";
			$counter++;
		}
		$counter = 1;
		if(count($trkSPD_array)==6){
			for($i=1;$i<=3;$i++){
				echo "<td><input id=\"txtKetLain".$counter."\" name='txtKetLain".$counter."' type=\"text\" size=\"40\" ></td>
				  <td class='clsUang'><input class='clsBebanHarian' id=\"txtBebanHarianLain".$counter."\" name='txtBebanHarianLain".$counter."' type=\"text\" size=\"15\" ></td>
				  <td></td>
				  <td class='clsUang'><input class='clsTotal' id=\"txtTotalLain".$counter."\" name='txtTotalLain".$counter."' type=\"text\" size=\"15\" value='' ></td>
				</tr>
				<tr class='clsInputNumber'>
					<td></td>";
				$counter++;
			}
		}
	  ?>
      <td></td>
      <td class="lbl" colspan="2"><b>Grand Total</b></td>
      <td class='clsUang'><input class='clsTotal' id="txtGrandTotal" name='txtGrandTotal' type="text" size="15" ></td>
    </tr>
	<tr>
      <td class="lbl">Total Pengeluaran Perjalanan Dinas</td>
      <td colspan="4">Rp. <input type="text" class='clsTotal' id="lblTotalPengajuan"></td>
    </tr>
    <tr>
      <td class="lbl">Total Pengajuan Uang Muka</td>
      <td colspan="4">Rp. <input type="text" class='clsTotal' id="lblTotalPengajuanUangMuka" value="<?php	
		if ($trkSPD_array[0]['uangmuka'] == 0){
			echo "0";
		}else{
			echo $trkSPD_array[0]['totalSPD']; 
		}
	  ?>" readonly>
	  </td>
    </tr>
	<tr>
      <td class="lbl"><label id="statusLebih"></label></td>
      <td colspan="4">Rp. <input type="text" class='clsTotal' id="lblSelisih"></td>
    </tr>
	<!--<tr>
      <td>Terbilang</td>
      <td colspan="4"><label id="lblTerbilang"></label></td>
    </tr>-->
  </tbody>
</table>
<br/>
<!-- tanda tangan -->
<table id="tblTandaTangan"  border=0>
  <tbody>
  
    <tr class="ttd">
      <td rowspan="2">Pemohon</td>
      <td colspan="2">Menyetujui</td>
    </tr>
    <tr class="ttd">
      <td>Atasan</td>
      <td>HRGA Dept Head</td>
    </tr>
    <tr class="ttd">
      <td><br/><br/><br/><br/><br/></td>
      <td></td>
      <td></td>
    </tr>
    <tr class="ttd">
      <td><?php echo $trkSPD_array[0]['nama']; ?></td>
      <td><?php echo $trkSPD_array[0]['atasan']; ?></td>
      <td>Eviyati</td>
    </tr>
  </tbody>
</table>
<div id="note">
</div><br/>
<table class="tblPureNoBorder"><tr><td>
	<input class="buttonSubmit"  type="submit" onClick="return confirm('Apakah Anda yakin ingin menyimpan Laporan SPD ini ke dalam database?')" id="btnSubmit" name="submitUpdateSPD" value="Update">
	<a href="submitUpdateSPD"><input class="buttonSubmit"  type="button" value="Batal"></a>
	<div id="divError"></div>
</tr></td></table>
	</body>
</html>