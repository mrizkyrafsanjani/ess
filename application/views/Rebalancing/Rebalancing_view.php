<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
    
	<!--<script type="text/javascript" src="../assets/js/jquery.datepick.js"></script>-->
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
   <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/myfunctionUM.js?V=1"></script>
	<head>
		<title>Rebalancing</title>
        <script src="<?php echo $this->config->base_url(); ?>assets/js/autoNumeric.js" type="text/javascript"></script>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/ttable.css">         
</head>
	<body >
        <div class="container">
		
            <table class="responsive-table">
                <thead>
                    <tr> 
                        <th scope="col" rowspan="2">JCI</th> 
                        <th scope="col" rowspan="2"><?php echo $IndexToday ; ?></th> 
                        <th scope="col" > MTD : <?php echo $MTDIndex ; ?> </th>
                        <th scope="col" rowspan="2">LQ45</th>  
                        <th rowspan="2"><?php echo $LQ45Today ; ?></th>    
                        <th scope="col" > MTD : <?php echo $MTDLQ45 ; ?> </th>     
                        <th scope="col" > Yield Gov Bond 10 Year </th>
                        <th scope="col" > <?php echo $Yield10Today ; ?> </th>   
                        <th scope="col" > MTD : <?php echo $MTDYield10 ; ?> YTD : <?php echo $YTDYield10 ; ?>  </th>    
                        <th scope="col" > BI Rate </th>  
                        <th scope="col" > <?php echo $RateBIToday ; ?> </th>
                        <th scope="col" > MTD : <?php echo $MTDRateBI ; ?> YTD : <?php echo $YTDRateBI ; ?>  </th>         
                    </tr>
                    <tr>
                        <th scope="col"> YTD : <?php echo $YTDIndex ; ?> </th> 
                        <th scope="col"> YTD : <?php echo $YTDLQ45 ; ?> </th> 
                        <th scope="col"> Yield Gov Bond 3 Year </th>
                        <th scope="col"> <?php echo $Yield3Today ; ?> </th>   
                        <th scope="col"> MTD : <?php echo $MTDYield3 ; ?> YTD : <?php echo $YTDYield3 ; ?>  </th> 
                        <th scope="col"> IDR/USD </th>  
                        <th scope="col"> <?php echo $RateKursToday ; ?> </th>  
                        <th scope="col"> MTD : <?php echo $MTDRateKurs ; ?> YTD : <?php echo $YTDRateKurs ; ?>  </td> 
                    </tr>
                </thead>
            </table>
        </div> 

        <div class="container">
  <table class="responsive-table">
    <caption>Top 10 Grossing Animated Films of All Time</caption>
    <thead>
      <tr>
        <th scope="col">Film Title</th>
        <th scope="col">Released</th>
        <th scope="col">Studio</th>
        <th scope="col">Worldwide Gross</th>
        <th scope="col">Domestic Gross</th>
        <th scope="col">Foreign Gross</th>
        <th scope="col">Budget</th>
      </tr>
    </thead>
   
    <tbody>
      <tr>
        <th scope="row">Frozen</th>
        <td data-title="Released">2013</td>
        <td data-title="Studio">Disney</td>
        <td data-title="Worldwide Gross" data-type="currency">$1,276,480,335</td>
        <td data-title="Domestic Gross" data-type="currency">$400,738,009</td>
        <td data-title="Foreign Gross" data-type="currency">$875,742,326</td>
        <td data-title="Budget" data-type="currency">$150,000,000</td>
      </tr>
       <tr>
        <th scope="row">Incredibles 2</th>
        <td data-title="Released">2018</td>
        <td data-title="Studio">Disney Pixar</td>
        <td data-title="Worldwide Gross" data-type="currency">$1,210,072,582</td>
        <td data-title="Domestic Gross" data-type="currency">$606,782,977</td>
        <td data-title="Foreign Gross" data-type="currency">$602,369,069</td>
        <td data-title="Budget" data-type="currency">$200,000,000</td>
      </tr>
      <tr>
        <th scope="row">Minions</th>
        <td data-title="Released">2015</td>
        <td data-title="Studio">Universal</td>
        <td data-title="Worldwide Gross" data-type="currency">$1,159,398,397</td>
        <td data-title="Domestic Gross" data-type="currency">$336,045,770</td>
        <td data-title="Foreign Gross" data-type="currency">$823,352,627</td>
        <td data-title="Budget" data-type="currency">$74,000,000</td>
      </tr>
      <tr>
        <th scope="row">Toy Story 3</th>
        <td data-title="Released">2010</td>
        <td data-title="Studio">Disney Pixar</td>
        <td data-title="Worldwide Gross" data-type="currency">$1,066,969,703</td>
        <td data-title="Domestic Gross" data-type="currency">$415,004,880</td>
        <td data-title="Foreign Gross" data-type="currency">$651,964,823</td>
        <td data-title="Budget" data-type="currency">$200,000,000</td>
      </tr>
      <tr>
        <th scope="row">Despicable Me 3</th>
        <td data-title="Released">2017</td>
        <td data-title="Studio">Universal</td>
        <td data-title="Worldwide Gross" data-type="currency">$1,034,799,409</td>
        <td data-title="Domestic Gross" data-type="currency">$264,624,300</td>
        <td data-title="Foreign Gross" data-type="currency">$770,175,109</td>
        <td data-title="Budget" data-type="currency">$80,000,000</td>
      </tr>
      <tr>
        <th scope="row">Finding Dory</th>
        <td data-title="Released">2016</td>
        <td data-title="Studio">Disney Pixar</td>
        <td data-title="Worldwide Gross" data-type="currency">$1,028,570,889</td>
        <td data-title="Domestic Gross" data-type="currency">$486,295,561</td>
        <td data-title="Foreign Gross" data-type="currency">$542,275,328</td>
        <td data-title="Budget" data-type="currency">$175,000,000</td>
      </tr>
      <tr>
        <th scope="row">Zootopia</th>
        <td data-title="Released">2016</td>
        <td data-title="Studio">Disney</td>
        <td data-title="Worldwide Gross" data-type="currency">$1,023,227,498</td>
        <td data-title="Domestic Gross" data-type="currency">$341,268,248</td>
        <td data-title="Foreign Gross" data-type="currency">$681,959,250</td>
        <td data-title="Budget" data-type="currency">$150,000,000</td>
      </tr>
      <tr>
        <th scope="row">Despicable Me 2</th>
        <td data-title="Released">2013</td>
        <td data-title="Studio">Universal</td>
        <td data-title="Worldwide Gross" data-type="currency">$970,761,885</td>
        <td data-title="Domestic Gross" data-type="currency">$368,061,265</td>
        <td data-title="Foreign Gross" data-type="currency">$602,700,620</td>
        <td data-title="Budget" data-type="currency">$76,000,000</td>
      </tr>
      <tr>
        <th scope="row">The Lion King</th>
        <td data-title="Released">1994</td>
        <td data-title="Studio">Disney</td>
        <td data-title="Worldwide Gross" data-type="currency">$987,483,777</td>
        <td data-title="Domestic Gross" data-type="currency">$422,783,777</td>
        <td data-title="Foreign Gross" data-type="currency">$564,700,000</td>
        <td data-title="Budget" data-type="currency">$45,000,000</td>
      </tr>
      <tr>
        <th scope="row">Finding Nemo</th>
        <td data-title="Released">2003</td>
        <td data-title="Studio">Pixar</td>
        <td data-title="Worldwide Gross" data-type="currency">$936,743,261</td>
        <td data-title="Domestic Gross" data-type="currency">$380,843,261</td>
        <td data-title="Foreign Gross" data-type="currency">$555,900,000</td>
        <td data-title="Budget" data-type="currency">$94,000,000</td>
      </tr>
    </tbody>
  </table>
</div> 
    
    
	</body>
    <script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
	
		
		$('#txtNoPP').hide();
		$('#txtNoPP').keyup(function(){
			var kodePP = document.getElementById('txtNoPP').value;
			$('#txtDisplayPP').val(kodePP);			
		});
		
		$('#txtDisplayPP').change(function(){
			$('#txtNoPP').val($('#txtDisplayPP').val());
			var kodePP = $('#txtNoPP').val();			
		});
		
		$(".select2").select2();

        $('#txtKegiatan').change(function(){				
			$('#divLoadingSubmit').show();
            var idKegiatan = document.getElementById('txtKegiatan').value;  
            var tglMulai = document.getElementById("txtTanggalMulai").value;
            var tglSelesai = document.getElementById("txtTanggalSelesai").value; 

            if (tglSelesai=='' && tglMulai=='')
            {
                alert("Harap mengisi Tanggal Mulai dan Tanggal Selesai terlebih dulu"); 
            }
            else
            { 
                $.ajax({
                    url: '<?php echo $this->config->base_url(); ?>index.php/UangMuka/formUMLain/ajax_loadWaktuJenisKegiatanDPA',
                    type: "POST",             
                    data: {idKegiatan:idKegiatan,tglMulai:tglMulai,tglSelesai:tglSelesai},
                    dataType: 'json',
                    cache: false,                
                    success: function(data)
                    {	
                    console.log(data);   
                    //alert('testttt');                 
                                $('#txtWaktuBayarCepat').val(data[0].WaktuBayarUangMuka);
                                    $('#txtWaktuSelesaiUM').val(data[0].WaktuSelesaiUangMuka);												
                        
                        $('#divLoadingSubmit').hide();
                    },
                    error: function (request, status, error) {
                        console.log(error);
                        $('#divLoadingSubmit').hide();
                    }
                });
            }
		});
	</script>
    <script>
        $(function(){
      //Date picker
            $('#txtTanggalMulai').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
            $('#txtTanggalSelesai').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
        });
    </script>   
    <script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
</html>