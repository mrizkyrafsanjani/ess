<html>
<style type="text/css">
@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
</style>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body >
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Rebalancing Investasi</h3>
		</div>
		<div class="panel-body">
		<?php echo validation_errors(); ?>
		<table border=0>			
			
		
			<tr>			
			<td>Data As Of</td>
				<td colspan=3>
					<div class="input-group date">
					<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
					</div>
					<input type="text" class="form-control pull-right" id="txtTanggal" name='txtTanggal'>
				</div>
				</td>
			</tr>
						
			
			<tr>
				<td></td>
				<td>
				<div class="col-sm-4">
					<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari"></div>
					<div id="divLoadingCari" hidden class="col-sm-7" style="padding-top:8px"><i id="spinner" class="fa fa-refresh fa-spin"></i>Loading...</div>
				</td>
				</td>
			</tr>
		</table>
		
		<div id="divContent" hidden>
			<H4><b>Data</b></H4>
			<iframe id="iFrameRebalancing" src="<?php echo $this->config->base_url(); ?>index.php/Rebalancing/RebalancingController" width="1200px" height="1000px" seamless frameBorder="0">
			<p>Your browser does not support iframes.</p>
			</iframe>
		</div>
		</div>
	</body>
	<script>
      $(function(){
      //Date picker
          $('#txtTanggal').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
          });
      });      
    </script>
    <script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		
		
		function search(){
			$('#divLoadingCari').show();	
				var Tgl = document.getElementById('txtTanggal').value;		
							
				var iframe1 = document.getElementById('iFrameRebalancing');				
				iframe1.src = '<?php echo $this->config->base_url(); ?>index.php/Rebalancing/RebalancingController/ViewData'+ '/'+ Tgl ;				
				$('#divLoadingCari').hide();
				$('#divContent').show();				  
					
		}
		
		
	
		
	</script>
</html>
