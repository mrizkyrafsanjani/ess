<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}
		th {text-align:center;}
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>
			</div>
			<div class="panel-body">
					<div class="">
						<div class="col-sm-6">
							<label for="dtTanggal" class="control-label">Date As Of</label>
							<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right" id="dtTanggal" name="dtTanggal">
							</div>
						</div>
					</div>
			</div>


			<div class="panel-footer clearfix">
				<div class="col-sm-1">
				<button id="btnSubmit" type="submit"  onClick="loadData()" class="btn btn-primary">Submit</button>
				</div>
				<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i>Processing</div>
			</div>
		</div>

		<div class="row">
		<div class="col-md-12">
			<div id="divReportRebalancing" class="box box-primary" hidden>
				<iframe id="iFrameRebalancing" width="100%" height="1300px" seamless frameBorder="0">
				<p>Your browser does not support iframes.</p>
				</iframe>
			</div>
		</div>
		</div>

		<div class="row">
		<div class="col-md-12">


			<div id="divContent" class="box box-primary col-sm-4">
				<!-- <div class="box-header with-border">
					<h3 class="box-title">Market Indicator</h3>
				</div>
					<?php if ($IndexToday) { ?>
					<table class="table table-bordered" border=2 bgcolor="black" align="right" >
						<thead>
							<tr>
								<th valign="middle" rowspan="2">JCI</th>
								<th valign="middle" rowspan="2"><?php echo $IndexToday ; ?></th>
								<th  > MTD : <?php echo $MTDIndex ; ?> </th>
								<th valign="middle" rowspan="2">LQ45</th>
								<th valign="middle" rowspan="2"><?php echo $LQ45Today ; ?></th>
								<th  > MTD : <?php echo $MTDLQ45 ; ?> </th>
								<th  > Yield Gov Bond 10 Year </th>
								<th  > <?php echo $Yield10Today ; ?> </th>
								<th  > MTD : <?php echo $MTDYield10 ; ?> YTD : <?php echo $YTDYield10 ; ?>  </th>
								<th  > BI Rate </th>
								<th  > <?php echo $RateBIToday ; ?> </th>
								<th  > MTD : <?php echo $MTDRateBI ; ?> YTD : <?php echo $YTDRateBI ; ?>  </th>
							</tr>
							<tr>
								<th > YTD : <?php echo $YTDIndex ; ?> </th>
								<th > YTD : <?php echo $YTDLQ45 ; ?> </th>
								<th > Yield Gov Bond 3 Year </th>
								<th > <?php echo $Yield3Today ; ?> </th>
								<th > MTD : <?php echo $MTDYield3 ; ?> YTD : <?php echo $YTDYield3 ; ?>  </th>
								<th > IDR/USD </th>
								<th > <?php echo $RateKursToday ; ?> </th>
								<th > MTD : <?php echo $MTDRateKurs ; ?> YTD : <?php echo $YTDRateKurs ; ?>  </td>
							</tr>
						</thead>
					</table>
					<?php } ?> -->
				<div class="box-header with-border">
					<h3 class="box-title">Investment Plan</h3>
				</div>
				<div class="box-body">
					<!-- <div id="divAssetDPA" class="col-md-6">
						<h4>DPA 1</h4>
						<?php echo var_dump($sprdata); ?>
					</div> -->
					<div id="divKontenInputDPA" class="col-md-6">
						<h4>DPA 1</h4>
						<table border="1">
							<tr><td rowspan="2">Equity</td><td>Inject</td>
								<td><input id="nominalEquityInject" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalEquityInject" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksEquityInject" type="text" class="form-input" placeholder="remarks"></td></tr>
							<tr><td>Withdraw</td>
							<td><input id="nominalEquityWithdraw" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalEquityWithdraw" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksEquityWithdraw" type="text" class="form-input" placeholder="remarks"></td>
							</tr>

							<tr><td rowspan="2">Corp Bond</td><td>Inject</td><td><input id="nominalCorpBondInject" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalCorpBondInject" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksCorpBondInject" type="text" class="form-input" placeholder="remarks"></td></tr>
							<tr><td>Withdraw</td><td><input id="nominalCorpBondWithdraw" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalCorpBondWithdraw" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksCorpBondWithdraw" type="text" class="form-input" placeholder="remarks"></td></tr>

							<tr><td colspan="4" ></td>								
								<td><input type="checkbox"  class="form-input" id="chkastra1" name="chkastra1" value="T">Astra</input></td></tr>
														

							<tr><td rowspan="2">Gov Bond</td><td>Inject</td><td><input id="nominalGovBondInject" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalGovBondInject" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksGovBondInject" type="text" class="form-input" placeholder="remarks"></td></tr>
							<tr><td>Withdraw</td><td><input id="nominalGovBondWithdraw" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalGovBondWithdraw" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksGovBondWithdraw" type="text" class="form-input" placeholder="remarks"></td></tr>
						</table>
					</div>
					<div id="divKontenInputDPA2" class="col-md-6">
						<h4>DPA 2</h4>
						<table border="1">
							<tr><td rowspan="2">Equity</td><td>Inject</td>
								<td><input id="nominalEquityInjectDPA2" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalEquityInjectDPA2" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksEquityInjectDPA2" type="text" class="form-input" placeholder="remarks"></td></tr>
							<tr><td>Withdraw</td>
							<td><input id="nominalEquityWithdrawDPA2" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalEquityWithdrawDPA2" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksEquityWithdrawDPA2" type="text" class="form-input" placeholder="remarks"></td>
							</tr>

							<tr><td rowspan="2">Corp Bond</td><td>Inject</td><td><input id="nominalCorpBondInjectDPA2" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalCorpBondInjectDPA2" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksCorpBondInjectDPA2" type="text" class="form-input" placeholder="remarks"></td></tr>
							<tr><td>Withdraw</td><td><input id="nominalCorpBondWithdrawDPA2" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalCorpBondWithdrawDPA2" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksCorpBondWithdrawDPA2" type="text" class="form-input" placeholder="remarks"></td></tr>

							<tr><td colspan="4" ></td>								
								<td><input type="checkbox"  class="form-input" id="chkastra2" name="chkastra2" value="T">Astra</input></td></tr>

							<tr><td rowspan="2">Gov Bond</td><td>Inject</td><td><input id="nominalGovBondInjectDPA2" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalGovBondInjectDPA2" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksGovBondInjectDPA2" type="text" class="form-input" placeholder="remarks"></td></tr>
							<tr><td>Withdraw</td><td><input id="nominalGovBondWithdrawDPA2" type="number" class="form-input" placeholder="nominal"></td>
								<td><input id="dtTanggalGovBondWithdrawDPA2" type="text" class="form-control pull-right" placeholder="tanggal"></td>
								<td><input id="remarksGovBondWithdrawDPA2" type="text" class="form-input" placeholder="remarks"></td></tr>
						</table>
					</div>
				</div>
				<div class="box-footer">
					<button id="btnPreview" type="submit" onClick="preview()" class="btn btn-warning">Preview</button>
					<button id="btnSimpan" type="submit" onClick="simpan()" class="btn btn-primary pull-right">Simpan</button>
					<div id="divLoadingSimpan" class="pull-right" style="padding-top:7px;padding-right:7px"><i class="fa fa-refresh fa-spin"></i>Processing</div>
				</div>
			</div>
		</div>
		</div>

	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".select2").select2();
			$("#dtTanggal").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalEquityInject").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalEquityWithdraw").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalCorpBondInject").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalCorpBondWithdraw").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalGovBondInject").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalGovBondWithdraw").datepicker({ autoclose: true, format: "yyyy-mm-dd"});

			$("#dtTanggalEquityInjectDPA2").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalEquityWithdrawDPA2").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalCorpBondInjectDPA2").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalCorpBondWithdrawDPA2").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalGovBondInjectDPA2").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$("#dtTanggalGovBondWithdrawDPA2").datepicker({ autoclose: true, format: "yyyy-mm-dd"});
			$('#divLoadingSimpan').hide();
		});
	</script>
	<script type="text/javascript">
		function search(){
			var dtTanggal = document.getElementById('dtTanggal').value;
			if(dtTanggal == ''){
				var src = '<?php echo $this->config->base_url(); ?>index.php/Rebalancing/RebalancingController/ViewData/';
			}else{
				var src = '<?php echo $this->config->base_url(); ?>index.php/Rebalancing/RebalancingController/ViewData/'+dtTanggal;
			}

			window.location.replace(src);
		}

		function clearfilter(){
			var src2 = '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ListPP/';
			window.location.replace(src2);
		}

		function loadData(){
			var dtTanggal = $('#dtTanggal').val();
		//	window.alert (dtTanggal);
			if(dtTanggal != ""){
				var iframe = document.getElementById('iFrameRebalancing');
				iframe.src = "http://sqldpa2app/ReportServer/Pages/ReportViewer.aspx?%2fESS%2fRebalancingSave&rc:Parameters=Collapsed&tanggal="+dtTanggal;
				$('#divReportRebalancing').show();
			}else{
				alert("Mohon pilih tanggal!");
			}
		}

		function simpan(){
			if(confirm("Apakah Anda yakin untuk menyimpan data-data ini?"))
			{
				console.log("proses simpan berjalan");
				var data = [];
				var nominalEquityInject = $('#nominalEquityInject').val();
				var dtTanggalEquityInject = $('#dtTanggalEquityInject').val();
				var remarksEquityInject = $('#remarksEquityInject').val();
				var nominalEquityWithdraw    = $('#nominalEquityWithdraw').val();
				var dtTanggalEquityWithdraw  = $('#dtTanggalEquityWithdraw').val();
				var remarksEquityWithdraw 	 = $('#remarksEquityWithdraw').val();
				var chkastravalue1 = $('#chkastra1').val();
				if($("#chkastra1").is(':checked')) chkastravalue1='T'; else  chkastravalue1='F' ;

				data.push({"tipe": "EQUITY", "jenis":"INJECT", "nominal": nominalEquityInject, "tanggal": dtTanggalEquityInject, "remark": remarksEquityInject, "dpa":1,"isastra":chkastravalue1});
				data.push({"tipe": "EQUITY", "jenis":"WITHDRAW", "nominal": nominalEquityWithdraw, "tanggal": dtTanggalEquityWithdraw, "remark": remarksEquityWithdraw, "dpa":1,"isastra":chkastravalue1});

				var nominalCorpBondInject 	 = $('#nominalCorpBondInject').val();
				var dtTanggalCorpBondInject  = $('#dtTanggalCorpBondInject').val();
				var remarksCorpBondInject 	 = $('#remarksCorpBondInject').val();

				var nominalCorpBondWithdraw  = $('#nominalCorpBondWithdraw').val();
				var dtTanggalCorpBondWithdraw = $('#dtTanggalCorpBondWithdraw').val();
				var remarksCorpBondWithdraw  = $('#remarksCorpBondWithdraw').val();

				data.push({"tipe": "CORPBOND", "jenis":"INJECT", "nominal": nominalCorpBondInject, "tanggal": dtTanggalCorpBondInject, "remark": remarksCorpBondInject, "dpa":1,"isastra":chkastravalue1});
				data.push({"tipe": "CORPBOND", "jenis":"WITHDRAW", "nominal": nominalCorpBondWithdraw, "tanggal": dtTanggalCorpBondWithdraw, "remark": remarksCorpBondWithdraw, "dpa":1,"isastra":chkastravalue1});

				var nominalGovBondInject 	 = $('#nominalGovBondInject').val();
				var dtTanggalGovBondInject 	 = $('#dtTanggalGovBondInject').val();
				var remarksGovBondInject 	 = $('#remarksGovBondInject').val();

				var nominalGovBondWithdraw 	 = $('#nominalGovBondWithdraw').val();
				var dtTanggalGovBondWithdraw = $('#dtTanggalGovBondWithdraw').val();
				var remarksGovBondWithdraw 	 = $('#remarksGovBondWithdraw').val();

				data.push({"tipe": "GOVBOND", "jenis":"INJECT", "nominal": nominalGovBondInject, "tanggal": dtTanggalGovBondInject, "remark": remarksGovBondInject, "dpa":1});
				data.push({"tipe": "GOVBOND", "jenis":"WITHDRAW", "nominal": nominalGovBondWithdraw, "tanggal": dtTanggalGovBondWithdraw, "remark": remarksGovBondWithdraw, "dpa":1});

				var nominalEquityInjectDPA2 = $('#nominalEquityInjectDPA2').val();
				var dtTanggalEquityInjectDPA2 = $('#dtTanggalEquityInjectDPA2').val();
				var remarksEquityInjectDPA2 = $('#remarksEquityInjectDPA2').val();
				var nominalEquityWithdrawDPA2    = $('#nominalEquityWithdrawDPA2').val();
				var dtTanggalEquityWithdrawDPA2  = $('#dtTanggalEquityWithdrawDPA2').val();
				var remarksEquityWithdrawDPA2 	 = $('#remarksEquityWithdrawDPA2').val();
				var nominalCorpBondInjectDPA2 	 = $('#nominalCorpBondInjectDPA2').val();
				var dtTanggalCorpBondInjectDPA2  = $('#dtTanggalCorpBondInjectDPA2').val();
				var remarksCorpBondInjectDPA2 	 = $('#remarksCorpBondInjectDPA2').val();
				var nominalCorpBondWithdrawDPA2  = $('#nominalCorpBondWithdrawDPA2').val();
				var dtTanggalCorpBondWithdrawDPA2 = $('#dtTanggalCorpBondWithdrawDPA2').val();
				var remarksCorpBondWithdrawDPA2  = $('#remarksCorpBondWithdrawDPA2').val();
				var nominalGovBondInjectDPA2 	 = $('#nominalGovBondInjectDPA2').val();
				var dtTanggalGovBondInjectDPA2 	 = $('#dtTanggalGovBondInjectDPA2').val();
				var remarksGovBondInjectDPA2 	 = $('#remarksGovBondInjectDPA2').val();
				var nominalGovBondWithdrawDPA2 	 = $('#nominalGovBondWithdrawDPA2').val();
				var dtTanggalGovBondWithdrawDPA2 = $('#dtTanggalGovBondWithdrawDPA2').val();
				var remarksGovBondWithdrawDPA2 	 = $('#remarksGovBondWithdrawDPA2').val();
				var chkastravalue2 = $('#chkastra2').val();
				if($("#chkastra2").is(':checked')) chkastravalue2='T'; else  chkastravalue2='F' ;

				data.push({"tipe": "EQUITY", "jenis":"INJECT", "nominal": nominalEquityInjectDPA2, "tanggal": dtTanggalEquityInjectDPA2, "remark": remarksEquityInjectDPA2, "dpa":2,"isastra":chkastravalue2});
				data.push({"tipe": "EQUITY", "jenis":"WITHDRAW", "nominal": nominalEquityWithdrawDPA2, "tanggal": dtTanggalEquityWithdrawDPA2, "remark": remarksEquityWithdrawDPA2, "dpa":2,"isastra":chkastravalue2});
				data.push({"tipe": "CORPBOND", "jenis":"INJECT", "nominal": nominalCorpBondInjectDPA2, "tanggal": dtTanggalCorpBondInjectDPA2, "remark": remarksCorpBondInjectDPA2, "dpa":2,"isastra":chkastravalue2});
				data.push({"tipe": "CORPBOND", "jenis":"WITHDRAW", "nominal": nominalCorpBondWithdrawDPA2, "tanggal": dtTanggalCorpBondWithdrawDPA2, "remark": remarksCorpBondWithdrawDPA2, "dpa":2,"isastra":chkastravalue2});
				data.push({"tipe": "GOVBOND", "jenis":"INJECT", "nominal": nominalGovBondInjectDPA2, "tanggal": dtTanggalGovBondInjectDPA2, "remark": remarksGovBondInjectDPA2, "dpa":2});
				data.push({"tipe": "GOVBOND", "jenis":"WITHDRAW", "nominal": nominalGovBondWithdrawDPA2, "tanggal": dtTanggalGovBondWithdrawDPA2, "remark": remarksGovBondWithdrawDPA2, "dpa":2});

				var valid = true;
				var pesanError = "";
				var count = 0;
				data.forEach(function(row){
					if((row["nominal"] > 0 && row["tanggal"] == "") || (row["nominal"] == 0 && row["tanggal"] != ""))
					{
						valid = false;
						pesanError += "Pastikan nominal dan tanggal pada field "+row["tipe"]+ " "+ row["jenis"] + " DPA "+ row["dpa"] + " terisi!! \n";
					}

					if(row["nominal"] == 0 && row["tanggal"] == ""){
						count++;
					}
				});

				if(!valid){
					alert(pesanError);
				}

				console.log(data);
				if(valid && count != 12){
					$('#divLoadingSimpan').show();
					$.post(
						'<?php echo $this->config->base_url(); ?>index.php/Rebalancing/RebalancingController/simpanInvestmentPlan',
						{ data: data },
						function(response) {
							$('#divLoadingSimpan').hide();
							var r = confirm(response);
							if (r == true && response == "Data berhasil disimpan!"){
								window.location.reload();
							}
						},
						"html"
					);
				}else{
					alert("Mohon masukkan data dengan benar!");
				}

			}
		}

		function preview(){
			var dtTanggal = $('#dtTanggal').val();

			var nominalEquityInjectDPA = $('#nominalEquityInject').val();
			var dtTanggalEquityInjectDPA = $('#dtTanggalEquityInject').val();
			var remarksEquityInjectDPA = $('#remarksEquityInject').val();
			var nominalEquityWithdrawDPA    = $('#nominalEquityWithdraw').val();
			var dtTanggalEquityWithdrawDPA  = $('#dtTanggalEquityWithdraw').val();
			var remarksEquityWithdrawDPA 	 = $('#remarksEquityWithdraw').val();
			var nominalCorpBondInjectDPA 	 = $('#nominalCorpBondInject').val();
			var dtTanggalCorpBondInjectDPA  = $('#dtTanggalCorpBondInject').val();
			var remarksCorpBondInjectDPA 	 = $('#remarksCorpBondInject').val();
			var nominalCorpBondWithdrawDPA  = $('#nominalCorpBondWithdraw').val();
			var dtTanggalCorpBondWithdrawDPA = $('#dtTanggalCorpBondWithdraw').val();
			var remarksCorpBondWithdrawDPA  = $('#remarksCorpBondWithdraw').val();
			var nominalGovBondInjectDPA 	 = $('#nominalGovBondInject').val();
			var dtTanggalGovBondInjectDPA 	 = $('#dtTanggalGovBondInject').val();
			var remarksGovBondInjectDPA 	 = $('#remarksGovBondInject').val();
			var nominalGovBondWithdrawDPA 	 = $('#nominalGovBondWithdraw').val();
			var dtTanggalGovBondWithdrawDPA = $('#dtTanggalGovBondWithdraw').val();
			var remarksGovBondWithdrawDPA 	 = $('#remarksGovBondWithdraw').val();

			var nominalEquityInjectDPA2 = $('#nominalEquityInjectDPA2').val();
			var dtTanggalEquityInjectDPA2 = $('#dtTanggalEquityInjectDPA2').val();
			var remarksEquityInjectDPA2 = $('#remarksEquityInjectDPA2').val();
			var nominalEquityWithdrawDPA2    = $('#nominalEquityWithdrawDPA2').val();
			var dtTanggalEquityWithdrawDPA2  = $('#dtTanggalEquityWithdrawDPA2').val();
			var remarksEquityWithdrawDPA2 	 = $('#remarksEquityWithdrawDPA2').val();
			var nominalCorpBondInjectDPA2 	 = $('#nominalCorpBondInjectDPA2').val();
			var dtTanggalCorpBondInjectDPA2  = $('#dtTanggalCorpBondInjectDPA2').val();
			var remarksCorpBondInjectDPA2 	 = $('#remarksCorpBondInjectDPA2').val();
			var nominalCorpBondWithdrawDPA2  = $('#nominalCorpBondWithdrawDPA2').val();
			var dtTanggalCorpBondWithdrawDPA2 = $('#dtTanggalCorpBondWithdrawDPA2').val();
			var remarksCorpBondWithdrawDPA2  = $('#remarksCorpBondWithdrawDPA2').val();
			var nominalGovBondInjectDPA2 	 = $('#nominalGovBondInjectDPA2').val();
			var dtTanggalGovBondInjectDPA2 	 = $('#dtTanggalGovBondInjectDPA2').val();
			var remarksGovBondInjectDPA2 	 = $('#remarksGovBondInjectDPA2').val();
			var nominalGovBondWithdrawDPA2 	 = $('#nominalGovBondWithdrawDPA2').val();
			var dtTanggalGovBondWithdrawDPA2 = $('#dtTanggalGovBondWithdrawDPA2').val();
			var remarksGovBondWithdrawDPA2 	 = $('#remarksGovBondWithdrawDPA2').val();

			var chkastravalue2 = $('#chkastra2').val();
			if($("#chkastra2").is(':checked')) chkastravalue2='T'; else  chkastravalue2='F' ;

			var chkastravalue1 = $('#chkastra1').val();
			if($("#chkastra1").is(':checked')) chkastravalue1='T'; else  chkastravalue1='F' ;

			var strLink = nominalEquityInjectDPA2 == "" ? "" : "&EquityInject="+nominalEquityInjectDPA2;
			strLink += dtTanggalEquityInjectDPA2 == "" ? "" : "&TglEquityInject="+dtTanggalEquityInjectDPA2;			
			strLink += nominalEquityWithdrawDPA2 == "" ? "" : "&EquityWithdraw="+nominalEquityWithdrawDPA2;
			strLink += dtTanggalEquityWithdrawDPA2 == "" ? "" : "&TglEquityWithDraw="+dtTanggalEquityWithdrawDPA2;
			strLink += nominalCorpBondInjectDPA2 == "" ? "" : "&CorpBondInject="+nominalCorpBondInjectDPA2;
			strLink += nominalCorpBondWithdrawDPA2 == "" ? "" : "&CorpBondWithdraw="+nominalCorpBondWithdrawDPA2;
			strLink += dtTanggalGovBondInjectDPA2 == "" ? "" : "&TglCorpBondInject="+dtTanggalGovBondInjectDPA2;
			strLink += dtTanggalCorpBondWithdrawDPA2 == "" ? "" : "&TglCorpBondWithDraw="+dtTanggalCorpBondWithdrawDPA2;
			strLink += nominalGovBondInjectDPA2 == "" ? "" : "&GovBondInject="+nominalGovBondInjectDPA2;
			strLink += nominalGovBondWithdrawDPA2 == "" ? "" : "&GovBondWithdraw="+nominalGovBondWithdrawDPA2;
			strLink += remarksGovBondInjectDPA2 == "" ? "" : "&TglGovBondInject="+remarksGovBondInjectDPA2;
			strLink += dtTanggalGovBondWithdrawDPA2 == "" ? "" : "&TglGovBondWithDraw="+dtTanggalGovBondWithdrawDPA2;
			strLink += chkastravalue2 == "" ? "" : "&astra="+chkastravalue2;

			strLink += nominalEquityInjectDPA == "" ? "" : "&EquityInject1="+nominalEquityInjectDPA;
			strLink += dtTanggalEquityInjectDPA == "" ? "" : "&TglEquityInject1="+dtTanggalEquityInjectDPA;			
			strLink += nominalEquityWithdrawDPA == "" ? "" : "&EquityWithdraw1="+nominalEquityWithdrawDPA;
			strLink += dtTanggalEquityWithdrawDPA == "" ? "" : "&TglEquityWithDraw1="+dtTanggalEquityWithdrawDPA;
			strLink += nominalCorpBondInjectDPA == "" ? "" : "&CorpBondInject1="+nominalCorpBondInjectDPA;
			strLink += nominalCorpBondWithdrawDPA == "" ? "" : "&CorpBondWithdraw1="+nominalCorpBondWithdrawDPA;
			strLink += dtTanggalGovBondInjectDPA == "" ? "" : "&TglCorpBondInject1="+dtTanggalGovBondInjectDPA;
			strLink += dtTanggalCorpBondWithdrawDPA == "" ? "" : "&TglCorpBondWithDraw1="+dtTanggalCorpBondWithdrawDPA;
			strLink += nominalGovBondInjectDPA == "" ? "" : "&GovBondInject1="+nominalGovBondInjectDPA;
			strLink += nominalGovBondWithdrawDPA == "" ? "" : "&GovBondWithdraw1="+nominalGovBondWithdrawDPA;
			strLink += remarksGovBondInjectDPA == "" ? "" : "&TglGovBondInject1="+remarksGovBondInjectDPA;
			strLink += dtTanggalGovBondWithdrawDPA == "" ? "" : "&TglGovBondWithDraw1="+dtTanggalGovBondWithdrawDPA;
			strLink += chkastravalue1 == "" ? "" : "&astra1="+chkastravalue1;

			if(dtTanggal != ""){
				var iframe = document.getElementById('iFrameRebalancing');
				alert (strLink);
				//console.log ("http://sqldpa2app/ReportServer/Pages/ReportViewer.aspx?%2fESS%2fRebalancingPrev&rc:Parameters=Collapsed&tanggal="+dtTanggal+"&EquityInject="+nominalEquityInjectDPA2"&EquityWithdraw="+nominalEquityWithdrawDPA2"&TglEquityInject="+dtTanggalEquityInjectDPA2"&TglEquityWithDraw="+dtTanggalEquityWithdrawDPA2"&CorpBondInject="+nominalCorpBondInjectDPA2"&CorpBondWithdraw="+nominalCorpBondWithdrawDPA2"&TglCorpBondInject="+dtTanggalGovBondInjectDPA2"&TglCorpBondWithDraw="+dtTanggalCorpBondWithdrawDPA2"&GovBondInject="+nominalGovBondInjectDPA2"&GovBondWithdraw="+nominalGovBondWithdrawDPA2"&TglGovBondInject="+remarksGovBondInjectDPA2"&TglGovBondWithDraw="+dtTanggalGovBondWithdrawDPA2"&astra="+chkastravalue2+"&EquityInject1="+nominalEquityInjectDPA+"&EquityWithdraw1="+nominalEquityWithdrawDPA+"&TglEquityInject1="+dtTanggalEquityInjectDPA+"&TglEquityWithDraw1="+dtTanggalEquityWithdrawDPA+"&CorpBondInject1="+nominalCorpBondInjectDPA+"&CorpBondWithdraw1="+nominalCorpBondWithdrawDPA+"&TglCorpBondInject1="+dtTanggalGovBondInjectDPA+"&TglCorpBondWithDraw1="+dtTanggalCorpBondWithdrawDPA+"&GovBondInject1="+nominalGovBondInjectDPA+"&GovBondWithdraw1="+nominalGovBondWithdrawDPA+"&TglGovBondInject1="+remarksGovBondInjectDPA+"&TglGovBondWithDraw1="+dtTanggalGovBondWithdrawDPA+"&astra1="+chkastravalue1)
				iframe.src = "http://sqldpa2app/ReportServer/Pages/ReportViewer.aspx?%2fESS%2fRebalancingPrev&rc:Parameters=Collapsed&tanggal="+dtTanggal+strLink;
			
				$('#divReportRebalancing').show();
			}else{
				alert("Mohon pilih tanggal!");
			}
		}
		
	</script>
</html>
