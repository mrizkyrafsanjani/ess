<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<style>
		@page { size 21cm 29.7cm; margin: 2cm }
		div.page { page-break-after: always }
		@bottom-right { 
     	content: counter(page) " of " counter(pages); 
 		}
}
		</style>
	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript">

		$(function() {
			$('#txtTanggalBerangkat').datepick({dateFormat: 'yyyy-mm-dd'});
			$('#txtTanggalKembali').datepick({dateFormat: 'yyyy-mm-dd'});
			$(".clsUang").formatNumber({format:"#,##0", locale:"us"});
			if($("input:radio[name='rbUangMuka']:checked").val()  == 1){
				$("#tblUangMuka").show();
			}
			
			$('.buttonSubmit').click(function (){
				$('#printArea').printElement({
					//leaveOpen:true,
					pageTitle:'Laporan SPD <?php echo $trkSPD_array[0]['nospd']; ?>',
					printMode:'popup',
					overrideElementCSS:[
						'<?php echo $this->config->base_url(); ?>assets/css/cetak.css','<?php echo $this->config->base_url(); ?>assets/css/style-common.css']
				});
			});
		});
		
	</script>
	<head>
		<title>Laporan SPD</title>
	</head>
	<body>
	<table class="tblPureNoBorder"><tr><td>
	<input class='buttonSubmit' type="button" value="Cetak" id="btnCetak">
	</tr></td></table><br/>
<div id='printArea'>
<?php if($UangMuka<>0){?>
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2">Laporan Perjalanan Dinas</td>
          <td colspan="3"><image id="logoDPA" src='../assets/images/logoDPA.png'>DANA PENSIUN ASTRA<br/>
		  <?php 
			$checked1 = "";
			$checked2 = "";
			if($trkSPD_array[0]['DPA'] == 1){
				$checked1 = "checked";
			}else{
				$checked2 = "checked";
			}
		  ?>
          
          <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked1; ?> disabled="disabled" >SATU
          <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked2; ?> disabled="disabled" >DUA
          </td>
	</tr>
	<tr>
	  <td class="lbl">No SPD</td>
	  <td><?php echo $trkSPD_array[0]['nospd']; ?></td>
	  
	  <td colspan="3" rowspan="10" align="center">
		<div id="catatan">
			<div class="lblTop">Catatan Penting</div>
				<div id="txtCatatan"><ol><li>Pertanggungjawaban atas uang muka untuk biaya perjalanan dinas wajib dilaporkan selambat-lambatnya 3 (tiga) hari setelah tiba di tempat kerja.</li>
<li>Pengembalian kelebihan ataupun kekurangan uang muka diterima/diserahkan 2 hari kerja setelah dokumen lengkap dan disetujui oleh HRD.</li>
<li>Semua bukti pengeluaran harus dilampirkan:</li>
</ol>
				<ul>
					<li>-Invoice Biaya penginapan (Hotel) Asli</li>
					<li>-Airport tax</li>
					<li>-Kwitansi deklarasi Taksi</li>
					<li>-dll (Berkas yang terkait)</li>
				</ul>
				</div>
		</div>
      </td>
	</tr>
	<tr>
	  <td class="lbl">Tanggal Laporan</td>
	  <td><?php echo date("j F Y"); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">NPK</td>
	  <td><?php echo $trkSPD_array[0]['npk']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Golongan</td>
	  <td><?php echo $trkSPD_array[0]['golongan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Nama</td>
	  <td><?php echo $trkSPD_array[0]['nama']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Jabatan</td>
	  <td><?php echo $trkSPD_array[0]['jabatan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Departemen</td>
	  <td><?php echo $trkSPD_array[0]['departemen']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Atasan</td>
	  <td><?php echo $trkSPD_array[0]['atasan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tanggal Keberangkatan</td>
	  <td><?php echo $trkSPD_array[0]['tanggalberangkatlap']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tanggal Kembali</td>
	  <td><?php echo $trkSPD_array[0]['tanggalkembalikantorlap']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tujuan</td>
	  <td colspan="4"><?php echo $trkSPD_array[0]['tujuan']; ?></td>
	</tr>
	<tr>
	  <td class="lbl">Alasan Perjalanan</td>
	  <td colspan="4" valign="top"><?php echo $trkSPD_array[0]['alasan']; ?></td>
	</tr>
  </tbody>
</table>

							
<br/>
<!-- part untuk perkiraaan biaya -->
<table id="tblPerkiraanBiaya" border=0>
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Perkiraan Biaya</td>      
    </tr>
    <tr>
      <td class="lblWidth" >Jenis Biaya</td>
      <td id="keterangan" class="lblTop">Keterangan</td>
      <td class="lblTop">Beban Harian</td>
      <td class="lblTop">#Jml Hari</td>
      <td class="lblTop">Total Perkiraan Biaya</td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Saku</td>
      <td><?php echo $trkSPD_array[0]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[0]['bebanharianlap']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[0]['jumlahharilap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[0]['bebanharianlap']*$trkSPD_array[0]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Makan</td>
      <td><?php echo $trkSPD_array[1]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[1]['bebanharianlap']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[1]['jumlahharilap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[1]['bebanharianlap']*$trkSPD_array[1]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td  class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Akomodasi</td>
      <td><?php echo $trkSPD_array[2]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[2]['bebanharianlap']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[2]['jumlahharilap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[2]['bebanharianlap']*$trkSPD_array[2]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td  class="lbl"></td>
      <td><?php echo $trkSPD_array[3]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[3]['bebanharianlap']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[3]['jumlahharilap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[3]['bebanharianlap']*$trkSPD_array[3]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td class="lbl"><i>Transportasi</i></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Taksi</td>
      <td><?php echo $trkSPD_array[4]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[4]['bebanharianlap']; ?></td>
      <td></td>
      <td class='clsUang'><?php echo $trkSPD_array[4]['bebanharianlap']*$trkSPD_array[4]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Airport Tax</td>
      <td><?php echo $trkSPD_array[5]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[5]['bebanharianlap']; ?></td>
      <td></td>
      <td class='clsUang'><?php echo $trkSPD_array[5]['bebanharianlap']*$trkSPD_array[5]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td class="lbl"><i>Lain-lain</i></td>
	  <?php 
		
		for($i=6;$i<count($trkSPD_array);$i++){
			echo "<td>". $trkSPD_array[$i]['keteranganlap'] ."</td>
			  <td class='clsUang'>". $trkSPD_array[$i]['bebanharianlap'] ." </td>
			  <td></td>
			  <td class='clsUang'> " . $trkSPD_array[$i]['bebanharianlap']*$trkSPD_array[$i]['jumlahharilap'] . "</td>
			</tr>
			<tr>
				<td></td>";
		}
		if(count($trkSPD_array)==6){
			echo "<td><br/></td>
			  <td></td>
			  <td></td>
			  <td></td>
			</tr>
			<tr>
				<td></td></tr>";
		}
	  ?>
		<tr>
      <td  class="lblWidth" colspan="4"><b>Grand Total</b></td>
      <td class='clsUang'><?php echo $trkSPD_array[0]['totallap']; ?></td>
    </tr>
  </tbody>
</table>
<br/>

<div style="page-break-before: always;">
<table bordered="0">
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Realisasi Uang Muka</td>
		</tr>
		<tr>
			<td>No Permohonan Uang Muka</td><td><?php echo $joinUangMuka_array['Nouangmuka']; ?></td>
		</tr>
		<tr>
			<td>Keterangan</td><td><?php echo $joinUangMuka_array['KeteranganPermohonan']; ?></td>
		</tr>
		<tr>
			<td>No Persetujuan Pengeluaran</td><td><?php echo $joinUangMuka_array['noPP']; ?></td>
		</tr>
		<tr>
			<td>Tanggal Permohonan Uang Muka</td><td><?php echo $joinUangMuka_array['TanggalPermohonan']; ?></td>
		</tr>
		<tr>
			<td>Tanggal Realisasi Uang Muka</td><td><?php echo date("Y-m-d"); ?></td>
		</tr>
		<tr>
			<td>Paling Lambat Waktu Penyelesaian</td><td><?php echo $joinUangMuka_array['WaktuPenyelesaianTerlambat']; ?></td>
		</tr>
		<tr>
      <td class="lblWidth">Total Pengeluaran Perjalanan Dinas</td>
      <td colspan="4"><label class='clsUang' id="lblTotalPengeluaran"><?php echo $trkSPD_array[0]['totallap']; ?></label></td>
    </tr>
    <tr>
      <td  class="lblWidth">Total Pengajuan Uang Muka</td>
      <td colspan="4"><label  class='clsUang' id="lblTotalPengajuanUangMuka">
		<?php  echo $trkSPD_array[0]['totalspd']; ?></label></td>
    </tr>
	<?php 
	$kata = "Kekurangan Uang Muka";
	  if($trkSPD_array[0]['uangmuka']=="1"){ 
		$selisih = $trkSPD_array[0]['totallap']-$trkSPD_array[0]['totalspd'];
		if($selisih < 0){
			$selisih = $selisih*-1;
			$kata = "Kelebihan Uang Muka";
		}
	}else{
		$selisih = $trkSPD_array[0]['totallap'];
	}
	?>
	<tr>
      <td  class="lblWidth"><?php echo $kata; ?></td>
      <td colspan="4"><label class='clsUang' id="lblSelisih"><?php echo $selisih; ?></label></td>
    </tr>
	<tr>
      <td class="lbl">Terbilang</td>
      <td colspan="4"><label id="lblTerbilang" class="clsTerbilang"><?php if($terbilang==''){ echo 'Nol'; } else { echo ucwords($terbilang); }  ?></label></td>
    </tr>
	</tbody>
</table>

<br/>

<table id ="tablepembayaran" bordered="0">
  <tbody>
		<?php
				if($selisih<0 && $trkSPD_array[0]['DPA'] == 1){?>
		<tr>
			<td colspan="2">Kelebihan uang muka harap ditransfer ke</td>
		</tr>
		<tr>
			<td >Bank</td><td>: Permata</td>
		</tr>
		<tr>
			<td>No Rekening</td><td>: 0701607104</td>
		</tr>
		<tr>
			<td>Nama Penerima</td><td>: DPA Satu - Opex</td>
		</tr>
		<?php }else if($selisih<0 && $trkSPD_array[0]['DPA'] == 2){?>
			<tr>
			<td colspan="2">Kelebihan uang muka harap ditransfer ke</td>
		</tr>
			<tr>
			<td >Bank</td><td>: Permata</td>
		</tr>
		<tr>
			<td>No Rekening</td><td>: 0701158113</td>
		</tr>
		<tr>
			<td>Nama Penerima</td><td>: DPA Dua - Opex</td>
		</tr>
		<?php }else if($selisih>0){?>
			<tr>
			<td>Pilih Pembayaran Kekurangan Uang Muka</td>
			<td><?php echo $joinUangMuka_array['TipeKembaliUangMuka']?></td>
		</tr>
			<tr>
			<td >Bank</td><td>: <?php echo $joinUangMuka_array['BankKembaliUangMuka']?></td>
		</tr>
		<tr>
			<td>No Rekening</td><td>: <?php echo $joinUangMuka_array['NoRekKembaliUangMuka']?></td>
		</tr>
		<tr>
			<td>Nama Penerima</td><td>: <?php echo $joinUangMuka_array['NmPenerimaUangMuka']?></td>
		</tr>
		<?php }else{

		}?>
	</tbody>
</table>
<br/>

<!-- tanda tangan -->
<table id="tblTandaTanganDIC"  border=0>
  <tbody>
  
    <tr class="ttd">
      <td rowspan="2">Pemohon</td>
      <td colspan="3">Menyetujui</td>
    </tr>
    <tr class="ttd">
      <td><?php 
		if($trkSPD_array[0]['atasan']== $trkSPD_array[0]['nama'])
		{
			echo "DIC HRGA";
		}
		else if($trkSPD_array[0]['atasan']=='')
		{ 
			echo "Chief"; 
		}
		else
		{ 
			echo "Atasan"; 
		}
	  ?></td>
      <td>HRGA Dept Head</td>
			<?php if($flagOutStanding!='0' ){ echo "<td >DIC Accounting, Tax & Control</td>";} else  { echo "";}?>
    </tr>
    <tr class="ttd">
      <td><br/><br/><br/><br/><br/></td>
      <td></td>
      <td></td>
			<?php if($flagOutStanding!='0' ){ echo "<td ></td>";} else  { echo "";}?>
    </tr>
    <tr class="ttd">
      <td><?php echo $trkSPD_array[0]['nama']; ?></td>
      <td><?php echo $trkSPD_array[0]['atasan']==$trkSPD_array[0]['nama']?"":$trkSPD_array[0]['atasan']; ?></td>
      <td><?php echo $HeadHRGA ?></td>
			<?php if($flagOutStanding!='0' ){ echo "<td >".$DICACC."</td>";} else  { echo "";}?>
    </tr>
  </tbody>
</table>
<br>
<?php   if($dataOutstanding!='0') { ?>
						Outstanding Uang Muka <br>
            <table id="tblOutstanding" border=0>
            <tbody>
								<tr class="tblHeader">
								<td colspan="5">Outstanding Uang Muka</td>
								</tr>
                <tr class="ttd">
                <td >No Uang Muka</td>
                <td >Keterangan</td>
                <td >Total</td>
                <td >Alasan Outstanding</td>
                </tr>
                <?php 
                        for($i=0;$i<count($dataOutstanding);$i++){
                          if($dataOutstanding[$i]['Total']<>'' or $dataOutstanding[$i]['Total']<>null or $dataOutstanding[$i]['Total']<>0){
                            echo "<tr><td>".$dataOutstanding[$i]['NoUangMuka'] ." </td>
                                    <td>". $dataOutstanding[$i]['KeteranganPermohonan'] ."</td>
                                    <td>Rp ". number_format($dataOutstanding[$i]['Total'],2,',','.') ."</td>   
                                    <td>". $dataOutstanding[$i]['ReasonOutStanding'] ."</td>                
                                    </tr> ";    
                          }else{
                            echo "<tr><td>".$dataOutstanding[$i]['NoUangMuka'] ." </td>
                                    <td>". $dataOutstanding[$i]['KeteranganPermohonan'] ."</td>
                                    <td>Rp ". number_format($dataOutstanding[$i]['TotalSPD'],2,',','.') ."</td>   
                                    <td>". $dataOutstanding[$i]['ReasonOutStanding'] ."</td>                
                                    </tr> "; 
                          }             
                        }
                ?>    
								</table>
								<?php } ?>
</div>
<div id="note">
<table class="tblPureNoBorder"><tr><td>
	<input class='buttonSubmit' type="button" value="Cetak" id="btnCetak2">
	</tr></td></table>
</div>
	<?php }else{?>
		<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2">Laporan Perjalanan Dinas</td>
          <td colspan="3"><image id="logoDPA" src='../assets/images/logoDPA.png'>DANA PENSIUN ASTRA<br/>
		  <?php 
			$checked1 = "";
			$checked2 = "";
			if($trkSPD_array[0]['DPA'] == 1){
				$checked1 = "checked";
			}else{
				$checked2 = "checked";
			}
		  ?>
          
          <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked1; ?> disabled="disabled" >SATU
          <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked2; ?> disabled="disabled" >DUA
          </td>
	</tr>
	<tr>
	  <td class="lbl">No SPD</td>
	  <td><?php echo $trkSPD_array[0]['nospd']; ?></td>
	  
	  <td colspan="3" rowspan="10" align="center">
		<div id="catatan">
			<div class="lblTop">Catatan Penting</div>
				<div id="txtCatatan"><ol><li>Pertanggungjawaban atas uang muka untuk biaya perjalanan dinas wajib dilaporkan selambat-lambatnya 3 (tiga) hari setelah tiba di tempat kerja.</li>
<li>Pengembalian kelebihan ataupun kekurangan uang muka diterima/diserahkan 2 hari kerja setelah dokumen lengkap dan disetujui oleh HRD.</li>
<li>Semua bukti pengeluaran harus dilampirkan:</li>
</ol>
				<ul>
					<li>-Invoice Biaya penginapan (Hotel) Asli</li>
					<li>-Airport tax</li>
					<li>-Kwitansi deklarasi Taksi</li>
					<li>-dll (Berkas yang terkait)</li>
				</ul>
				</div>
		</div>
      </td>
	</tr>
	<tr>
	  <td class="lbl">Tanggal Laporan</td>
	  <td><?php echo date("j F Y"); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">NPK</td>
	  <td><?php echo $trkSPD_array[0]['npk']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Golongan</td>
	  <td><?php echo $trkSPD_array[0]['golongan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Nama</td>
	  <td><?php echo $trkSPD_array[0]['nama']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Jabatan</td>
	  <td><?php echo $trkSPD_array[0]['jabatan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Departemen</td>
	  <td><?php echo $trkSPD_array[0]['departemen']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Atasan</td>
	  <td><?php echo $trkSPD_array[0]['atasan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tanggal Keberangkatan</td>
	  <td><?php echo $trkSPD_array[0]['tanggalberangkatlap']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tanggal Kembali</td>
	  <td><?php echo $trkSPD_array[0]['tanggalkembalikantorlap']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tujuan</td>
	  <td colspan="4"><?php echo $trkSPD_array[0]['tujuan']; ?></td>
	</tr>
	<tr>
	  <td class="lbl">Alasan Perjalanan</td>
	  <td colspan="4" valign="top"><?php echo $trkSPD_array[0]['alasan']; ?></td>
	</tr>
  </tbody>
</table>
<br/>
<!-- part untuk perkiraaan biaya -->
<table id="tblPerkiraanBiaya" border=0>
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Perkiraan Biaya</td>      
    </tr>
    <tr>
      <td class="lblWidth" >Jenis Biaya</td>
      <td id="keterangan" class="lblTop">Keterangan</td>
      <td class="lblTop">Beban Harian</td>
      <td class="lblTop">#Jml Hari</td>
      <td class="lblTop">Total Perkiraan Biaya</td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Saku</td>
      <td><?php echo $trkSPD_array[0]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[0]['bebanharianlap']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[0]['jumlahharilap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[0]['bebanharianlap']*$trkSPD_array[0]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Makan</td>
      <td><?php echo $trkSPD_array[1]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[1]['bebanharianlap']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[1]['jumlahharilap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[1]['bebanharianlap']*$trkSPD_array[1]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td  class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Akomodasi</td>
      <td><?php echo $trkSPD_array[2]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[2]['bebanharianlap']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[2]['jumlahharilap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[2]['bebanharianlap']*$trkSPD_array[2]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td  class="lbl"></td>
      <td><?php echo $trkSPD_array[3]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[3]['bebanharianlap']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[3]['jumlahharilap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[3]['bebanharianlap']*$trkSPD_array[3]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td class="lbl"><i>Transportasi</i></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Taksi</td>
      <td><?php echo $trkSPD_array[4]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[4]['bebanharianlap']; ?></td>
      <td></td>
      <td class='clsUang'><?php echo $trkSPD_array[4]['bebanharianlap']*$trkSPD_array[4]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Airport Tax</td>
      <td><?php echo $trkSPD_array[5]['keteranganlap']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[5]['bebanharianlap']; ?></td>
      <td></td>
      <td class='clsUang'><?php echo $trkSPD_array[5]['bebanharianlap']*$trkSPD_array[5]['jumlahharilap']; ?></td>
    </tr>
    <tr>
      <td class="lbl"><i>Lain-lain</i></td>
	  <?php 
		
		for($i=6;$i<count($trkSPD_array);$i++){
			echo "<td>". $trkSPD_array[$i]['keteranganlap'] ."</td>
			  <td class='clsUang'>". $trkSPD_array[$i]['bebanharianlap'] ." </td>
			  <td></td>
			  <td class='clsUang'> " . $trkSPD_array[$i]['bebanharianlap']*$trkSPD_array[$i]['jumlahharilap'] . "</td>
			</tr>
			<tr>
				<td></td>";
		}
		if(count($trkSPD_array)==6){
			echo "<td><br/></td>
			  <td></td>
			  <td></td>
			  <td></td>
			</tr>
			<tr>
				<td></td></tr>";
		}
	  ?>
		<tr>
      <td  class="lblWidth" colspan="4"><b>Grand Total</b></td>
      <td class='clsUang'><?php echo $trkSPD_array[0]['totallap']; ?></td>
    </tr>
  </tbody>
</table>
<br/>

<table bordered="0">
  <tbody>
   
		<tr>
      <td class="lblWidth">Total Pengeluaran Perjalanan Dinas</td>
      <td colspan="4"><label class='clsUang' id="lblTotalPengeluaran"><?php echo $trkSPD_array[0]['totallap']; ?></label></td>
    </tr>
    <tr>
      <td  class="lblWidth">Total Pengajuan Uang Muka</td>
      <td colspan="4"><label  class='clsUang' id="lblTotalPengajuanUangMuka">
		<?php  echo $trkSPD_array[0]['totalspd']; ?></label></td>
    </tr>
	<?php 
	$kata = "Kekurangan Uang Muka";
	  if($trkSPD_array[0]['uangmuka']=="1"){ 
		$selisih = $trkSPD_array[0]['totallap']-$trkSPD_array[0]['totalspd'];
		if($selisih < 0){
			$selisih = $selisih*-1;
			$kata = "Kelebihan Uang Muka";
		}
	}else{
		$selisih = $trkSPD_array[0]['totallap'];
	}
	?>
	<tr>
      <td  class="lblWidth"><?php echo $kata; ?></td>
      <td colspan="4"><label class='clsUang' id="lblSelisih"><?php echo $selisih; ?></label></td>
    </tr>
	<tr>
      <td class="lbl">Terbilang</td>
      <td colspan="4"><label id="lblTerbilang" class="clsTerbilang"><?php if($terbilang==''){ echo 'Nol'; } else { echo ucwords($terbilang); }  ?></label></td>
    </tr>
	</tbody>
</table>

<br/>
	</tbody>
</table>
<br/>

<!-- tanda tangan -->
<table id="tblTandaTanganDIC"  border=0>
  <tbody>
  
    <tr class="ttd">
      <td rowspan="2">Pemohon</td>
      <td colspan="3">Menyetujui</td>
    </tr>
    <tr class="ttd">
      <td><?php 
		if($trkSPD_array[0]['atasan']== $trkSPD_array[0]['nama'])
		{
			echo "DIC HRGA";
		}
		else if($trkSPD_array[0]['atasan']=='')
		{ 
			echo "Chief"; 
		}
		else
		{ 
			echo "Atasan"; 
		}
	  ?></td>
      <td>HRGA Dept Head</td>
    </tr>
    <tr class="ttd">
      <td><br/><br/><br/><br/><br/></td>
      <td></td>
      <td></td>
    </tr>
    <tr class="ttd">
      <td><?php echo $trkSPD_array[0]['nama']; ?></td>
      <td><?php echo $trkSPD_array[0]['atasan']==$trkSPD_array[0]['nama']?"":$trkSPD_array[0]['atasan']; ?></td>
      <td><?php echo $HeadHRGA ?></td>
    </tr>
  </tbody>
</table>
</div>
<div id="note">
<table class="tblPureNoBorder"><tr><td>
	<input class='buttonSubmit' type="button" value="Cetak" id="btnCetak2">
	</tr></td></table>
</div>
	<?php } ?>
	</body>
</html>