<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
    <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
    
    <script type="text/javascript">
        function SubmitUploadUM(ID,Status) {
            var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png", ".pdf"];
            var flag = true;
            var arrInputs = UploadUM.getElementsByTagName("input");
            var countFile = 0;
            for (var i = 0; i < arrInputs.length; i++) {
                var HtmlInput = arrInputs[i];
                if (HtmlInput.type == "file") {
                    var sFileName = HtmlInput.value;
                    if (sFileName.length > 0) {
                        countFile++;
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                            flag = false;
                        }
                    }
                }
            }
            if(countFile==0){
                alert("Tidak ada file yang diupload?");
                flag = false;
            }
            if(flag){
                var x = window.confirm("Upload Permohonan Uang Muka?");
                if(x){
                    $("#UploadUM").attr("action"); //will retrieve it
                    $("#UploadUM").attr("target", "_self"); //will retrieve it
                    $("#UploadUM").attr("action", "<?php echo site_url('UangMuka/UangMukaController/UploadUM');?>/"+ID+"/"+Status);
                }else{
                    event.preventDefault();
                }
            }else{
                event.preventDefault();
            }
        }
        function BatalUploadUM(){
            var x = window.confirm("Batalkan Upload Scan Dokumen Persetujuan Permohonan Uang Muka ini?");
            if(x){
                $("#UploadUM").attr("action"); //will retrieve it
                $("#UploadUM").attr("target", "_self"); //will retrieve it
                $("#UploadUM").attr("action", "<?php echo site_url('UangMuka/UangMukaController/ViewListUangMukaUser');?>");
            }else{
                event.preventDefault();
            }
        }
    </script>
    <style>
    #error{
        color: red;
    }
    </style>
	<head>
		<title>Upload Scan Dokumen</title>
	</head>
    <body>
        <?php echo $this->session->flashdata('message'); ?>
        
        <?php if(isset($error)) VAR_DUMP($error);?>
        <?php foreach($data as $data){ ?>
        <div class="container" style="width:80%">
            <h2>Upload Scan Dokumen <?php echo ($data->Status == 'P')? 'Pengajuan Uang Muka '. $data->NoUangMuka  :'Realisasi Uang Muka '. $data->NoUangMuka;;?></h2>
            <form method="POST" name="UploadUM" id="UploadUM" enctype="multipart/form-data">
                <div class="form-group">
                    <div class = "row">
                        <div class ="col">
                            <label for="uploadScanUM">Upload Scan Dokumen</label>
                            <input type="file" class="form-control form-control-file" id="uploadScanUM" name="uploadScanUM">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col"><br/>
                            <input type="submit" class="btn btn-success" onclick = "SubmitUploadUM('<?php echo $data->id?>','<?php echo $data->Status?>')" value="Upload">
                            <input type="submit" class="btn btn-danger" onclick = "BatalUploadUM()" value="Batal Upload">
                        </div>
                    </div>
                </div>
            </form>
            <?php if (!empty($data->FilePathPermohonan)): ?>
            <div class="row">
                <div class="col-sm-6">
                    <h4><?php echo $data->NoUangMuka; ?></h4>
                </div>
                <div class="col-sm-6">
                    <h5><a href="<?php echo $this->config->base_url().'assets/uploads/files/UangMuka/'. $data->FilePathPermohonan; ?>" download>download</a></h5>
                </div>
            </div>
            <div class="row">    
                <div class="col-md-12"> 
                    <iframe src="<?php echo $this->config->base_url().'assets/uploads/files/UangMuka/'. $data->FilePathPermohonan; ?>" width="90%" height="720px">
                    This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->config->base_url().'assets/uploads/files/UangMuka/'. $data->FilePathPermohonan; ?>">Download</a>
                    </iframe>   
                </div>
            </div>
            <?php endif; ?>
            <?php if (!empty($data->FilePathRealisasi)): ?>
            <div class="row">
                <div class="col-sm-6">
                    <h4><?php echo $data->NoUangMuka; ?></h4>
                </div>
                <div class="col-sm-6">
                    <h5><a href="<?php echo $this->config->base_url().'assets/uploads/files/UangMuka/'. $data->FilePathRealisasi; ?>" download>download</a></h5>
                </div>
            </div>
            <div class="row">    
                <div class="col-md-12"> 
                    <iframe src="<?php echo $this->config->base_url().'assets/uploads/files/UangMuka/'. $data->FilePathRealisasi; ?>" width="90%" height="720px">
                    This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->config->base_url().'assets/uploads/files/UangMuka/'. $data->FilePathRealisasi; ?>">Download</a>
                    </iframe>   
                </div>
            </div>
            <?php endif; ?>
        </div>
        <?php } ?>
        
	</body>
</html> 