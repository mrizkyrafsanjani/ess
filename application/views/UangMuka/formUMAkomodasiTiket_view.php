<html>
    <style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>/plugins/datepicker/bootstrap-datepicker.css">
	<!--<script type="text/javascript" src="../assets/js/jquery.datepick.js"></script>-->
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
   <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/myfunctionUMAkomodasiTiket.js"></script>
	<head>
		<title>Form Akomodasi Tiket</title>		
    </head>
	<body>
		<?php echo validation_errors(); ?>
		
		<form action="submitFormUMAkomodasiTiket" method="post" accept-charset="utf-8">
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2">Permintaan Akomodasi Tiket</td>
	  <td colspan="3"><image id="logoDPA" src='../assets/images/logoDPA.png'>*DANA PENSIUN ASTRA<br/>
	  <input type="radio" id="rbDpaSatu" name="rbDPA" value="1">SATU
	  <input type="radio" id="rbDpaDua" name="rbDPA" value="2">DUA
	  </td>
	</tr>
	<tr>
	  <td class="lbl">No Permohonan</td>
	  <td bgcolor="#D3D3D3"></td>
	  <td colspan="3" rowspan="8" align="center">
		<div id="catatan">
			<div class="lblTop">Catatan Penting</div>
			<div id="txtCatatan"><ol><li>Permintaan uang muka perjalanan dinas diajukan selambat-lambatnya 5 hari kerja sebelum melakukan perjalanan dinas.
<li>Pengeluaran uang muka paling lambat diterima 1 hari kerja sebelum keberangkatan (SPD).
<li>Pertanggungjawaban atas uang muka untuk biaya-biaya perjalanan dinas wajib dilaporkan selambat-lambatnya 3 (tiga) hari setelah tiba di tempat kerja.
</ol>
</div>
      </td>
	</tr>
	<tr>
	  <td class="lbl">Tanggal Pengajuan </td>
	  <td><?php echo date("j F Y"); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">NPK</td>
	  <td><input type="text" class='bordered' id="txtNPK" name="txtNPK" value="<?php echo $npk ?>" readonly />
    <input type="hidden" class='bordered' id="txtNPKatasan" name="txtNPKatasan" value="<?php echo $NPKAtasan; ?>" readonly />
    </td>   
	  
	</tr>
	<tr>
	  <td class="lbl">Golongan</td>
	  <td><?php echo $dataUser['golongan'] ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Nama</td>
	  <td><?php echo $nama ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Jabatan</td>
	  <td><?php echo $dataUser['jabatan'] ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Departemen</td>
	  <td><?php echo $dataUser['departemen'] ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Atasan </td> 
	  <td> <?php echo $dataUser['atasan'] ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">*Tanggal Keberangkatan</td>
	  <td colspan="4">
    <!--<input  class='bordered' id="txtTanggalBerangkat" name='txtTanggalBerangkat' type="text" size="15" >-->
	    
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalBerangkat" name='txtTanggalBerangkat'>
        </div>
        <!-- /.input group -->
      
    </td>
	  
	</tr>
	<tr>
	  <td class="lbl">*Tanggal Kembali</td>
	  <td colspan="4"><!--<input  class='bordered'  id="txtTanggalKembali" name='txtTanggalKembali' type="text" size="15" >-->
      <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembali" name='txtTanggalKembali'>
      </div>
    </td>
	</tr>
	<tr>
	  <td class="lbl">*Tujuan</td>
	  <td colspan="4"><input id="txtTujuan" class="form-control" name='txtTujuan' type="text" size="50%" ></td>
	</tr>
	<tr>
	  <td class="lbl">*Alasan Perjalanan</td>
	  <td colspan="4" valign="top"><textarea class="form-control" id="txtAlasan" name='txtAlasan' type="text" cols="60" rows="3" ></textarea></td>
	</tr>
  </tbody>
</table>
<br/>

<table border=0>
<tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="4">Permintaan Akomodasi </td>	
	</tr>
    <tr>
	  <td class="lbl">Hotel</td>
      <td ><input id="txtHotel" class="form-control" name='txtHotel' type="text" ></td>
    </tr>
    <tr>
	  <td class="lbl">   Tanggal Check In :</td>
      <td >
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right" id="txtTanggalCheckIn" name='txtTanggalCheckIn'>
        </div>
      </td>
    </tr>
    <tr>
	  <td class="lbl">   Tanggal Check Out :</td>
      <td >
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right" id="txtTanggalCheckOut" name='txtTanggalCheckOut'>
        </div>
      </td>
    </tr>
    <tr>
	  <td class="lbl">Tiket</td>
      <td ><input id="txtTiketPesawat" class="form-control" name='txtTiket' type="text" ></td>
    </tr>
    <tr>
	  <td class="lbl">   Tanggal Keberangkatan Pesawat :</td>
      <td >
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right" id="txtBerangkatPesawat" name='txtBerangkatPesawat'>
        </div>
      </td>
      <td class="lbl">   Jam :</td>
      <td > <input name='txtJamBerangkatPesawat' id='txtJamBerangkatPesawat' type='time' value='$value'></td>      
    </tr>
    <tr>
	  <td class="lbl">   Tanggal Kepulangan Pesawat :</td>
      <td >
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right" id="txtPulangPesawat" name='txtPulangPesawat'>
        </div>
      </td>
      <td class="lbl">   Jam :</td>
      <td > <input name='txtJamPulangPesawat' id='txtJamPulangPesawat' type='time' value='$value'></td>      
    </tr>

</tbody>
</table>


    <table class="tblPureNoBorder">
      <tr><td>
        <input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin menyimpan permohonan ini ke dalam database?')" id="btnSubmit" name="submitFormUMAkomodasiTiket" value="Simpan">
        <a href="formUMAkomodasiTiket"><input class="btn btn-default" type="button" value="Batal"></a>			
			</td></tr>
      <tr><td><div id="divError" class="alert alert-danger"></div></td></tr>
    </table>
			<br/>
		</form>
    <script>
      $(function(){
      //Date picker
          $('#txtTanggalCheckIn').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
          }).on('changeDate', function (selected) {
              var minDate = new Date(selected.date.valueOf());
              $('#txtTanggalCheckOut').datepicker('setStartDate', minDate);
          });
          $('#txtTanggalCheckOut').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
          }).on('changeDate', function (selected) {
              var minDate = new Date(selected.date.valueOf());
              $('#txtTanggalCheckIn').datepicker('setEndDate', minDate);
          });

          $('#txtTanggalBerangkat').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
          }).on('changeDate', function (selected) {
              var minDate = new Date(selected.date.valueOf());
              $('#txtTanggalKembali').datepicker('setStartDate', minDate);
          });
          $('#txtTanggalKembali').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
          }).on('changeDate', function (selected) {
              var minDate = new Date(selected.date.valueOf());
              $('#txtTanggalBerangkat').datepicker('setEndDate', minDate);
          });

          $('#txtBerangkatPesawat').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
          }).on('changeDate', function (selected) {
              var minDate = new Date(selected.date.valueOf());
              $('#txtPulangPesawat').datepicker('setStartDate', minDate);
          });
          $('#txtPulangPesawat').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
          }).on('changeDate', function (selected) {
              var minDate = new Date(selected.date.valueOf());
              $('#txtBerangkatPesawat').datepicker('setEndDate', minDate);
          });
      });      
    </script>
    <script src="<?php echo $this->config->base_url(); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
    
	</body>
</html>