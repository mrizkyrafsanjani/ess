<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>

		<style>
		@page { size 21cm 29.7cm; margin: 2cm }
		div.page { page-break-after: always }
		@media print {
    #btnCetak {
        display :  none;
		}
		#tblCetak {
        display :  none;
    }
}
		</style>

	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript">

		$(function() {
		
			$(".clsUang").formatNumber({format:"#,##0", locale:"us"});
			$('.buttonSubmit').click(function (){
				$('#printArea').printElement({
					//leaveOpen: true,
					pageTitle:'Form Uang Muka <?php echo $trxuangmuka_array[0]['NoUangMuka']; ?>',
					printMode:'popup',
					overrideElementCSS:[
						'<?php echo $this->config->base_url(); ?>assets/css/cetak.css','<?php echo $this->config->base_url(); ?>assets/css/style-common.css']
				});
			});
			
			
			
		});
		
		

	</script>
	<head>
		<title>Form Uang Muka</title>
	</head>
	<body>
	<table class="tblPureNoBorder" id ="tblCetak"><tr><td>
	<input class='buttonSubmit' type="button" value="Cetak" id="btnCetak">
	</tr></td></table><br/>
<div id='printArea'>
<div class="page" >
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2">Permintaan Uang Muka</td>
	  <td colspan="2"><image id="logoDPA" src='<?php echo $this->config->base_url(); ?>assets/images/logoDPA.png'>DANA PENSIUN ASTRA<br/>
	  <?php 
		$checked11 = "";
		$checked22 = "";
		if($trxuangmuka_array[0]['DPA'] == 1){
			$checked11 = "checked";
		}else{
			$checked22 = "checked";
		}
	  ?> 
	  <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked11; ?> disabled="disabled" >SATU
	  <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked22; ?> disabled="disabled" >DUA
	  </td>
	</tr>
	<tr>
	  <td class="lbl">No Permohonan Uang Muka</td>
	  <td><?php echo $trxuangmuka_array[0]['NoUangMuka']; ?></td>
		<td class="lbl">NPK Pemohon </td>
	  <td><?php echo $npk; ?></td>
	</tr>
	<tr>
	  <td colspan="1" class="lbl">Keterangan</td>
	  <td colspan="3"><?php echo $trxuangmuka_array[0]['Keterangan']; ?></td>		
	</tr>
	<tr>
	  <td colspan="1" class="lbl">Tanggal Permohonan Uang Muka</td>
	  <td colspan="3"><?php echo $trxuangmuka_array[0]['TanggalUangMuka']; ?></td>	  
	</tr>
	<tr>
	  <td colspan="1" class="lbl">Tanggal Mulai</td>
	  <td colspan="3"><?php echo $trxuangmuka_array[0]['TanggalMulai']; ?></td>	  
	</tr>
	<tr>
	  <td colspan="1" class="lbl">Tanggal Selesai</td>
	  <td colspan="3"><?php echo $trxuangmuka_array[0]['TanggalSelesai']; ?></td>	  
	</tr>
	<tr>
	  <td colspan="1" class="lbl">No Persetujuan Pengeluaran</td>
	  <td colspan="3"><?php echo $trxuangmuka_array[0]['NoPP']; ?></td>
	  
	</tr>
	<tr>
	  <td colspan="1" class="lbl">Outstanding Uang Muka</td>
	  <td colspan="3">	<?php if($flagoutstanding=='1' ){ echo "Ada";} else  { echo "-";}?></td>	  
	</tr>	
  </tbody>
</table>
<br>

<?php 
 if($trxAT_array ){  ?>
	<table border=0>
<tbody>
<!-- Results table headers -->
<tr class="tblHeader">
<td colspan="4">Permintaan Akomodasi </td>	
</tr>
<tr>
<td class="lbl">Hotel</td>
	<td ><?php echo $trxAT_array['NamaHotel']; ?></td>
</tr>
<tr>
<td class="lbl">   Tanggal Check In :</td>
	<td >	<?php echo $trxAT_array['TanggalCheckIn']; ?>	</td>
</tr>
<tr>
<td class="lbl">   Tanggal Check Out :</td>
	<td ><?php echo $trxAT_array['TanggalCheckOut']; ?></td>    
</tr>
<tr>
<td class="lbl">Tiket</td>
	<td ><?php echo $trxAT_array['TiketPesawat']; ?></td>
</tr>
<tr>
<td class="lbl">   Tanggal Keberangkatan Pesawat :</td>
	<td ><?php echo $trxAT_array['TanggalBerangkatPesawat']; ?>
	</td>
	<td class="lbl">   Jam :</td>
	<td ><?php echo $trxAT_array['JamBerangkatPesawat']; ?></td>      
</tr>
<tr>
<td class="lbl">   Tanggal Kepulangan Pesawat :</td>
	<td ><?php echo $trxAT_array['TanggalKepulanganPesawat']; ?>
	</td>
	<td class="lbl">   Jam :</td>
	<td > <?php echo $trxAT_array['JamKepulanganPesawat']; ?></td>      
</tr>

</tbody>
</table>
<?php }  ?>

<br>
<table id="tblPerkiraanBiaya" border=0>
  <tbody>
    
    <tr class="tblHeader">

      <td class="lbl">Jenis Biaya</td>
      <td id="keterangan" class="lblTop">Keterangan</td>
      <td class="lblTop">Quantity</td>
      <td class="lblTop">Price</td>
      <td class="lblTop">Total</td>
    </tr>
	<?php 
		$subtotal = 0;
		for($i=0;$i<count($detailuangmuka_array);$i++){		
			ini_set('error_reporting',0);
			if ($detailuangmuka_array[$i]['TotalPermohonan']==0){
			}else{?>
			<tr>
				<td ><?php echo $detailuangmuka_array[$i]['JenisBiayaPermohonan']; ?></td>
				<td><?php echo $detailuangmuka_array[$i]['KeteranganPermohonan']; ?></td>
				<td><?php echo $detailuangmuka_array[$i]['JumlahPermohonan']; ?></td>
				<td class='clsUang'><?php echo $detailuangmuka_array[$i]['HargaPermohonan']; ?></td>
				<td class='clsUang'><?php echo $detailuangmuka_array[$i]['TotalPermohonan']; ?></td>
				
				</tr>
				<?php 

				$j=$i+1;
				if($j>count($detailuangmuka_array)){
				$j=$i;
				}else{
				$subtotal = $subtotal + $detailuangmuka_array[$i]['TotalPermohonan'];
				if($detailuangmuka_array[$j]['JenisBiayaPermohonan']  == $detailuangmuka_array[$i]['JenisBiayaPermohonan']){
				}else{ 
				?>
				<tr>
				<td colspan=4>Subtotal</td>
				<td class='clsUang'><?php echo $subtotal; ?></td>
				</tr>
				<?php 
				$subtotal =0;
				$j++;
				
		}
		}
		
		}}?>
	
				<tr>
				<td colspan=4 >Total Permohonan</td>				
				<td class='clsUang'><?php echo $trxuangmuka_array[0]['TotalPermohonan']; ?></td>
		</tr>   
		<tr>
				<td colspan=2 >Terbilang</td>				
				<td colspan=3><?php if($trxuangmuka_array[0]['terbilang']==''){ echo 'Nol'; } else { echo ucwords($trxuangmuka_array[0]['terbilang']);} ?> Rupiah</td>
		</tr> 
  </tbody>
</table>

<br>

<table border=0>
            <tbody>
              
                <tr>
                    <td class="lbl">Pembayaran Uang Muka</td>
                    <td> <?php echo $trxuangmuka_array[0]['TipeBayarUangMuka']; ?>  </td>	
                    <td class="lbl">Waktu Pembayaran Tercepat</td>
                    <td> <?php echo $trxuangmuka_array[0]['WaktuBayarTercepat']; ?> 	</td>
                </tr>
                <tr>
                    <td class="lbl">Bank</td>
                    <td><?php echo $trxuangmuka_array[0]['BankBayarUangMuka']; ?> </td>
                    <td class="lbl">Paling Lambat Waktu Penyelesaian</td>
                    <td><?php echo $trxuangmuka_array[0]['WaktuPenyelesaianTerlambat']; ?> </td>
                </tr>
                <tr>
                    <td class="lbl">No Rekening</td>
                    <td ><?php echo $trxuangmuka_array[0]['NoRekBayarUangMuka']; ?> </td>
                </tr>
                <tr>
                    <td class="lbl">Nama Penerima</td>
                    <td ><?php echo $trxuangmuka_array[0]['NmPenerimaBayarUangMuka']; ?> </td>
                </tr>
            </tbody>
</table>

<br/>

<br/>
<?php if($trxuangmuka_array[0]['NoAkomodasiTiket'] != "") { ?>
<!-- jika akomodasi -->
	<table id="tblTandaTangan"  border=0>
		<tbody>
			<tr class="ttd">
				<td >Pemohon</td>
				<td >DIC Pemohon</td>
				<?php if($flagoutstanding=='1' ){ echo "<td >DIC Accounting, Tax & Control</td>";} else  { echo "";}?>
			</tr>
			<tr class="ttd">
				<td><br/><br/><br/><br/><br/></td>
				<td></td>
				<?php if($flagoutstanding=='1' ){ echo "<td ></td>";} else  { echo "";}?>
			</tr>
			<tr class="ttd">
				<td><?php echo $HeadHRGA; ?></td>
				<td><?php echo $DICHRGA ?></td>
				<?php if($flagoutstanding=='1' ){ echo "<td >$DICACC</td>";} else  { echo "";}?>
			</tr>
		</tbody>
	</table>
<?php } else { ?>
<!-- tanda tangan -->
<?php if($DICPemohon_array[0]['NamaDIC']<>$DICACC){?>
	<table id="tblTandaTangan"  border=0>
		<tbody>
			<tr class="ttd">
				<td >Pemohon</td>
				<td >DIC Pemohon</td>
				<?php if($flagoutstanding=='1' ){ echo "<td >DIC Accounting, Tax & Control</td>";} else  { echo "";}?>
			</tr>
			<tr class="ttd">
				<td><br/><br/><br/><br/><br/></td>
				<td></td>
				<?php if($flagoutstanding=='1' ){ echo "<td ></td>";} else  { echo "";}?>
			</tr>
			<tr class="ttd">
				<td><?php echo $trxuangmuka_array[0]['namaNPKPemohon']; ?></td>
				<td><?php echo $DICPemohon_array[0]['NamaDIC'];?></td>
				<?php if($flagoutstanding=='1' ){ echo "<td >$DICACC</td>";} else  { echo "";}?>
			</tr>
		</tbody>
	</table>
<?php }else{?>
	<table id="tblTandaTangan"  border=0>
		<tbody>
			<tr class="ttd">
				<td >Pemohon</td>
				<td >
				<?php if($flagoutstanding=='1' ){?>
				DIC Pemohon/DIC Accounting, Tax & Control
				<?php }else{?>
				DIC Pemohon
				<?php } ?>
				</td>
			</tr>
			<tr class="ttd">
				<td><br/><br/><br/><br/><br/></td>
				<td></td>
			</tr>
			<tr class="ttd">
				<td><?php echo $trxuangmuka_array[0]['namaNPKPemohon']; ?></td>
				<td><?php echo $DICPemohon_array[0]['NamaDIC'];?></td>
			</tr>
		</tbody>
	</table>
<?php }} ?>

<br>

<?php if($flagoutstanding=='1' ){  ?>
	Outstanding Uang Muka
	<table id="tblPerkiraanBiaya" border=0>
		<tbody>
			<tr class="tblHeader">
				<td >No Permohonan Uang Muka</td>     
				<td >Keterangan</td> 
				<td >Nominal</td> 
				<td >Penyebab Outstanding</td>       
			</tr>
			<?php for($i=0;$i<count($dataOutstandingDetail);$i++){		
				if($dataOutstandingDetail[$i]['Total']<>'' or $dataOutstandingDetail[$i]['Total']<>null or $dataOutstandingDetail[$i]['Total']<>0){?>
				<tr>
					<td ><?php echo $dataOutstandingDetail[$i]['NoUangMuka']; ?></td>
					<td><?php echo $dataOutstandingDetail[$i]['KeteranganPermohonan']; ?></td>
					<td class='clsUang'><?php echo $dataOutstandingDetail[$i]['Total']; ?></td>
					<td ><?php echo $dataOutstandingDetail[$i]['ReasonOutStanding']; ?></td>
					</tr>
			<?php }else{?>
				<td ><?php echo $dataOutstandingDetail[$i]['NoUangMuka']; ?></td>
					<td><?php echo $dataOutstandingDetail[$i]['KeteranganPermohonan']; ?></td>
					<td class='clsUang'><?php echo $dataOutstandingDetail[$i]['TotalSPD']; ?></td>
					<td ><?php echo $dataOutstandingDetail[$i]['ReasonOutStanding']; ?></td>
					</tr>

			<?php }
			}?>
		</tbody>
	</table>
<br>
<?php }  ?>

</div></div>

<BR><BR>

<!-- <div id="note">
<table class="tblPureNoBorder"><tr><td>
	<input class='buttonSubmit' type="button" value="Cetak" id="btnCetak2">
	</tr></td></table>
</div>-->
	</body>
</html>