<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
	<!--<script type="text/javascript" src="../assets/js/jquery.datepick.js"></script>-->
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
   <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/myfunctionUM.js"></script>
	<head>
		<title>Form Uang Muka Lain</title>
        <script src="<?php echo $this->config->base_url(); ?>assets/js/autoNumeric.js" type="text/javascript"></script>
		
    </head>
	<body>
		<?php echo validation_errors(); ?>

        <?php if ( $trkUangMuka_array[0]['TglTerimaHRGAPermohonan']=='' ) {?> 
        <!--belum di terima HRGA , jadi bisa edit-->
		
                      
                        <!--Untuk Uang Muka Lain-->
                            <form action="submitUpdateUMLain" method="post" accept-charset="utf-8">
                            <input id="txtMode" name='txtMode' type="hidden" size="30" value="edit">
                                    <table border=0>
                                    <tbody>
                                        <!-- Results table headers -->
                                        <tr class="tblHeader">
                                        <td colspan="2">Edit Permintaan Uang Muka</td>
                                        <td colspan="3"><image id="logoDPA" src='<?php echo $this->config->base_url(); ?>assets/images/logoDPA.png'>*DANA PENSIUN ASTRA<br/>
                                        <?php 
                                            $checked1 = "";
                                            $checked2 = "";
                                            if($trkUangMuka_array[0]['DPA'] == 1){
                                                $checked1 = "checked";
                                            }else{
                                                $checked2 = "checked";
                                            }
                                        ?>
                                        <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked1; ?> >SATU
                                        <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked2; ?> >DUA
                                        </td>
                                        </tr>
                                        <tr>
                                        <td class="lbl">No Uang Muka</td>
                                        <td > <input id="txtNoUM" name='txtNoUM' type="text" size="30" value="<?php echo $trkUangMuka_array[0]['NoUangMuka']; ?>" readonly="true"> </td>	 
                                        </tr>
                                        <tr>
                                        <td class="lbl">NPK Pemohon</td>
                                        <td><input type="text" class='bordered' id="txtNPK" name="txtNPK" value="<?php echo $trkUangMuka_array[0]['NPKPemohon'];  ?>" readonly />
                                            <input type="hidden" class='bordered' id="txtNPKLogin" name="txtNPKLogin" value="<?php echo $trkUangMuka_array[0]['CreatedBy'];  ?>"  />
                                        </td>                
                                        </tr>
                                        <tr>
                                        <td class="lbl">Keterangan</td>
                                        <td colspan="4"><input id="txtKeterangan" class="form-control" name='txtKeterangan' type="text" size="50%" value="<?php echo $trkUangMuka_array[0]['KeteranganPermohonan']; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td class="lbl">*Tanggal Mulai</td>
                                            <td >
                                                                
                                                <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="txtTanggalMulai" name="txtTanggalMulai"  value="<?php echo $trkUangMuka_array[0]['TanggalMulai']; ?>" >
                                                </div>
                                                <!-- /.input group -->
                                            
                                            </td>
                                            
                                            </tr>
                                            <tr>
                                            <td class="lbl">*Tanggal Selesai</td>
                                            <td ><!--<input  class='bordered'  id="txtTanggalSelesai" name='txtTanggalSelesai' type="text" size="15" >-->
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="txtTanggalSelesai" name="txtTanggalSelesai" value="<?php echo $trkUangMuka_array[0]['TanggalSelesai']; ?>">
                                            </div>
                                            </td>
                                            </tr>
                                        <tr>
                                        <td class="lbl">Tanggal Permohonan Uang Muka</td>
                                        <td><?php echo $trkUangMuka_array[0]['TanggalPermohonan']; ?></td>	  
                                        </tr>
                                        
                                        <tr>
                                        <td class="lbl">Pilih No. Persetujuan</td>
                                        <td colspan="4" valign="top"><input class="form-control" type="text" id="txtNoPP"/>
                                                        <select class="form-control select2" id="txtDisplayPP"  name="txtDisplayPP">
                                                        <option value =''></option>
                                                        <?php foreach($dataNoPP as $row){
                                                             if ($row->NoPP==$trkUangMuka_array[0]['NoPP'])
                                                             echo "<option selected value='".$row->NoPP."'>".$row->display." </option>";
                                                             else
                                                             echo "<option value='".$row->NoPP."'>".$row->display." </option>";
                                                        } ?>
                                                        </select>
                                        </td>
                                        </tr>

                                        <tr>
                                        <td class="lbl">Jenis Kegiatan Uang Muka</td>
                                        <td colspan="3" valign="top">
                                                        <select class="form-control"  id="txtKegiatan" name="txtKegiatan"> 
                                                        <option value =''>Pilih salah satu</option>                               
                                                        <?php foreach($dataJenisKeterangan as $row){
                                                            if ($row->IdKegiatan==$trkUangMuka_array[0]['JenisKegiatanUangMuka'])
                                                            echo "<option selected value='".$row->IdKegiatan."'>".$row->JenisKegiatan." </option>";
                                                            else
                                                            echo "<option value='".$row->IdKegiatan."'>".$row->JenisKegiatan." </option>";
                                                        } ?>
                                                        </select>
                                        </td>
                                        <td>					
                                            <div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
                                        </td>
                                        </tr>
                                        
                                        <tr>
                                        <td class="lbl">Outstanding Uang Muka</td>
                                        <td><?php 
                                                if($dataOutstanding!='0')
                                                {  echo "Ada"; 
                                                echo "<input id='txtOutstanding'  name='txtOutstanding' type='hidden' value='Ada' >";
                                                }
                                                else
                                                {  echo "Tidak Ada";
                                                echo "<input id='txtOutstanding'  name='txtOutstanding' type='hidden' value='TidakAda' >";
                                                }
                                            ?></td>	  
                                        </tr>
                                    </tbody>
                                    </table>
                                    <br/>

                                    <!-- out standing -->
                                    <?php   if($dataOutstanding!='0') { ?>
                                    <table id="tblOutstanding" border=0>
                                    <tbody>
                                        <tr class="ttd">
                                        <td >No Uang Muka</td>
                                        <td >Keterangan</td>
                                        <td >Total</td>
                                        <td >Alasan Outstanding</td>
                                        </tr>
                                        <?php 
                                                for($i=0;$i<count($dataOutstanding);$i++){
                                                    if($dataOutstanding[$i]['Total']<>'' or $dataOutstanding[$i]['Total']<>null or $dataOutstanding[$i]['Total']<>0){
                                                      echo "<tr><td>".$dataOutstanding[$i]['NoUangMuka'] ." </td>
                                                              <td>". $dataOutstanding[$i]['KeteranganPermohonan'] ."</td>
                                                              <td>Rp ". number_format($dataOutstanding[$i]['Total'],2,',','.') ."</td>   
                                                              <td>". $dataOutstanding[$i]['ReasonOutstanding'] ."</td>                
                                                              </tr> ";    
                                                    }else{
                                                      echo "<tr><td>".$dataOutstanding[$i]['NoUangMuka'] ." </td>
                                                              <td>". $dataOutstanding[$i]['KeteranganPermohonan'] ."</td>
                                                              <td>Rp ". number_format($dataOutstanding[$i]['TotalSPD'],2,',','.') ."</td>   
                                                              <td>". $dataOutstanding[$i]['ReasonOutstanding'] ."</td>                
                                                              </tr> "; 
                                                    }             
                                                  }
                                        ?>    
                                    </tbody>
                                    </table>
                                    <?php }?>

                                    <br/>

                                    <table><tr><td>
                                            <iframe id="iFrameDetailUangMuka" src="<?php echo $this->config->base_url(); ?>index.php/UangMuka/DetailUangMuka/Edit/<?php echo $trkUangMuka_array[0]['id']; ?>" width="100%" height="400px" seamless frameBorder="0">
                                                        <p>Your browser does not support iframes.</p>
                                                    </iframe>
                                            </td></tr>
                                    </table>

                                    <br>
                                    <table border=0>
                                   
                                    <tbody>
                                        <!-- Results table headers -->
                                        
                                        <tr>
                                            <td class="lbl">Pembayaran Uang Muka</td>
                                            <td > <select  id="txtPilihBayar" name="txtPilihBayar">
                                                <option <?php if ($trkUangMuka_array[0]['TipeBayarUangMuka'] == 'CASH' ) echo 'selected' ; ?> value ='CASH'>CASH</option> 
                                                <option <?php if ($trkUangMuka_array[0]['TipeBayarUangMuka'] == 'TRANSFER' ) echo 'selected' ; ?> value ='TRANSFER'>TRANSFER</option>                                     
                                                </select>
                                            </td>	
                                            <td class="lbl">Waktu Pembayaran Tercepat</td>
                                            <td><input type="text" class='bordered' id="txtWaktuBayarCepat" name="txtWaktuBayarCepat" value="<?php echo $trkUangMuka_array[0]['WaktuBayarTercepat']; ?>" readonly="true" ></td>
                                        </tr>
                                        <tr>
                                            <td class="lbl">Bank</td>
                                            <td><input type="text" class="form-control" id="txtBank" name="txtBank" value="<?php echo $trkUangMuka_array[0]['BankBayarUangMuka']; ?>" /></td>
                                            <td class="lbl">Paling Lambat Waktu Penyelesaian</td>
                                            <td><input type="text" class='bordered' id="txtWaktuSelesaiUM" name="txtWaktuSelesaiUM" value="<?php echo $trkUangMuka_array[0]['WaktuPenyelesaianTerlambat']; ?>" readonly /></td>
                                        </tr>
                                        <tr>
                                            <td class="lbl">No Rekening</td>
                                            <td ><input id="txtNoRek" class="form-control" name='txtNoRek' type="text" value="<?php echo $trkUangMuka_array[0]['NoRekBayarUangMuka']; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td class="lbl">Nama Penerima</td>
                                            <td ><input id="txtPenerima" class="form-control" name='txtPenerima' type="text" value="<?php echo $trkUangMuka_array[0]['NmPenerimaBayarUangMuka']; ?>"></td>
                                        </tr>
                                    </tbody>
                                    </table>
                                        


                                    <table class="tblPureNoBorder">
                                    <tr><td>
                                        <input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin update Pengajuan ini ke dalam database?')" id="btnSubmit" name="submitUpdateUMLain" value="Update">
                                        <a href="UangMuka/UangMukaController/Edit/<?php echo $trkUangMuka_array[0]['id'] ?>"><input class="btn btn-default" type="button" value="Batal"></a>			
                                            </td></tr>
                                    <tr><td><div id="divError" class="alert alert-danger"></div></td></tr>
                                    </table>
                                    <br/>
                            
                            

                            </form>
                        <!--Selesai Uang Muka Lain-->
                        
        <!--selesai edit-->                                        
        <?php } else {?>
            SUDAH DITERIMA HRGA TIDAK BOLEH EDIT
            <br>
            <form action="submitUpdateUMLain" method="post" accept-charset="utf-8">
    
              <input id="txtMode" name='txtMode' type="hidden" size="30" value="inputreason">
              <input id="txtNoUM" name='txtNoUM' type="hidden" size="30" value="<?php echo $trkUangMuka_array[0]['NoUangMuka']; ?>" >
              <table border=0>
              <tr>
                <td class="lbl">Alasan Outstanding</td>
                <td colspan="4"><input id="txtReason" name='txtReason' type="text" size="15" value=""></td>
              </tr>
              </table>

              <table class="tblPureNoBorder"><tr><td>
              <input class="buttonSubmit"  type="submit" onClick="return confirm ('Apakah Anda yakin ingin update Alasan ini?')" id="" name="submitUpdateUMLain" value="Update">
              </tr></td></table>
          </form>

        <?php }?> 
    
    
	</body>
    <script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
	
		
		$('#txtNoPP').hide();
		$('#txtNoPP').keyup(function(){
			var kodePP = document.getElementById('txtNoPP').value;
			$('#txtDisplayPP').val(kodePP);			
		});
		
		$('#txtDisplayPP').change(function(){
			$('#txtNoPP').val($('#txtDisplayPP').val());
			var kodePP = $('#txtNoPP').val();			
		});
		
		$(".select2").select2();

        $('#txtKegiatan').change(function(){				
			$('#divLoadingSubmit').show();
            var idKegiatan = document.getElementById('txtKegiatan').value;  
            var tglMulai = document.getElementById("txtTanggalMulai").value;
            var tglSelesai = document.getElementById("txtTanggalSelesai").value; 

            if (tglSelesai=='' && tglMulai=='')
            {
                alert("Harap mengisi Tanggal Mulai dan Tanggal Selesai terlebih dulu"); 
            }
            else
            { 
                $.ajax({
                    url: '<?php echo $this->config->base_url(); ?>index.php/UangMuka/formUMLain/ajax_loadWaktuJenisKegiatanDPA',
                    type: "POST",             
                    data: {idKegiatan:idKegiatan,tglMulai:tglMulai,tglSelesai:tglSelesai},
                    dataType: 'json',
                    cache: false,                
                    success: function(data)
                    {	
                    console.log(data);   
                    //alert('testttt');                 
                                $('#txtWaktuBayarCepat').val(data[0].WaktuBayarUangMuka);
                                    $('#txtWaktuSelesaiUM').val(data[0].WaktuSelesaiUangMuka);												
                        
                        $('#divLoadingSubmit').hide();
                    },
                    error: function (request, status, error) {
                        console.log(error);
                        $('#divLoadingSubmit').hide();
                    }
                });
            }
		});
	</script>
    <script>
        $(function(){
      //Date picker
            $('#txtTanggalMulai').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
            $('#txtTanggalSelesai').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
        });
    </script>   
    <script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
</html>