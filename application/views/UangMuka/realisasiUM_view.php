<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
	<!--<script type="text/javascript" src="../assets/js/jquery.datepick.js"></script>-->
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
   <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/myfunctionUM.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <script type="text/javascript">
$(document).ready(function() {
   
});


</script>


    <head>
		<title>Form Uang Muka Lain</title>
        <script src="<?php echo $this->config->base_url(); ?>assets/js/autoNumeric.js" type="text/javascript"></script>
		<script>
        totalRealisasiAwal = 0;
        function RefreshTable() {
            var iduangmuka = "<?php echo $id; ?>";
            $.post(
                    '<?php echo $this->config->base_url(); ?>index.php/UangMuka/UangMukaController/ajax_getTotalRealisasi',
                        { ID: iduangmuka },
                        function(response) {
                            totalRealisasi = Number(response["totalRealisasi"]);
                            console.log('total realisasi:'+response["totalRealisasi"]);
                        },
                        "json"
                );
            //console.log('total realisasi awal '+ totalRealisasiAwal );
            if(totalRealisasiAwal != totalRealisasi){
                //console.log('masuk if');
                $( "#mytable" ).load( "<?php echo $this->config->base_url(); ?>index.php/UangMuka/UangMukaController/Realisasi/" + iduangmuka + " #mytable" );
                totalRealisasiAwal = totalRealisasi;
            }
        }

        //$("#refresh-btn").on("click", RefreshTable);

        function hitungTotalRealisasi(){
            console.log('hitungTotalRealisasi');
            $.post(
                    '<?php echo $this->config->base_url(); ?>index.php/UangMuka/UangMukaController/ajax_getTotalRealisasi',
                        { ID: "<?php echo $id; ?>" },
                        function(response) {
                            totalRealisasi = Number(response["totalRealisasi"]);
                            totalPermohonan = Number(response["totalPermohonan"]);

                            $('#divTotalRealisasi').html(totalRealisasi.toLocaleString());
                            $('#divTotalPermohonan').html(totalPermohonan.toLocaleString());
                            $('#divTotalKelebihanKekurangan').html((totalRealisasi-totalPermohonan).toLocaleString());

                            if(totalRealisasi > totalPermohonan){
                                $('#divDeskripsiTotalKelebihanKekurangan').html('Total Kekurangan Uang Muka');
                            }else{
                                $('#divDeskripsiTotalKelebihanKekurangan').html('Total Kelebihan Uang Muka');
                            }
                            console.log('total permohonan:'+response["totalRealisasi"]);
                        },
                        "json"
                );
        }


        </script>
    </head>
	<body onload="setInterval(RefreshTable, 500);">
		<?php echo validation_errors(); ?>

        <?php if ( $trkUangMuka_array[0]['TglTerimaHRGAPermohonan']<>'' && $trkUangMuka_array[0]['TglTerimaFinancePermohonan']<>'' && $trkUangMuka_array[0]['TglBayarFinancePermohonan']<>'' ) {?> 
        <!--belum di terima HRGA , jadi bisa edit-->
		    <?php if ( $trkUangMuka_array[0]['TglTerimaHRGARealisasi']=='' ) {?> 
                        <!--Untuk Uang Muka Lain-->
                            <form action="submitRealisasiUMLain" method="post" accept-charset="utf-8">
                                    <table border=0>
                                    <tbody>
                                        <!-- Results table headers -->
                                        <tr class="tblHeader">
                                        <td colspan="2">Realisasi Permintaan Uang Muka</td>
                                        <td colspan="3"><image id="logoDPA" src='<?php echo $this->config->base_url(); ?>assets/images/logoDPA.png'>*DANA PENSIUN ASTRA<br/>
                                        <?php 
                                            $checked1 = "";
                                            $checked2 = "";
                                            if($trkUangMuka_array[0]['DPA'] == 1){
                                                $checked1 = "checked";
                                            }else{
                                                $checked2 = "checked";
                                            }
                                        ?>
                                        <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked1; ?> disabled />SATU
                                        <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked2; ?> disabled />DUA
                                        </td>
                                        </tr>
                                        <tr>
                                        <td class="lbl">No Uang Muka</td>
                                        <td > <input id="txtNoUM" name='txtNoUM' type="text" size="30" value="<?php echo $trkUangMuka_array[0]['NoUangMuka']; ?>" readonly="true"> </td>	 
                                        </tr>
                                        
                                        <tr>
                                        <td class="lbl">Keterangan</td>
                                        <td colspan="4" id="txtKeterangan" name="txtKeterangan"><?php echo $trkUangMuka_array[0]['KeteranganPermohonan']; ?></td>
                                        </tr>

                                        <tr>
                                        <td class="lbl"> No. Persetujuan</td>
                                        <td colspan="4" valign="top"> <?php echo $trkUangMuka_array[0]['NoPP']; ?>
                                        </td>
                                        </tr>
                                        
                                        <td class="lbl">Tanggal Permohonan Uang Muka</td>
                                        <td><?php echo $trkUangMuka_array[0]['TanggalPermohonan']; ?></td>	  
                                        </tr>
                                     
                                        <tr>
                                        <td class="lbl">Paling Lambat Waktu Penyelesaian</td>
                                        <td colspan="3" id ="txtWaktuPenyelesaianTerlambat" name="txtWaktuPenyelesaianTerlambat" valign="top"><?php echo $trkUangMuka_array[0]['WaktuPenyelesaianTerlambat']; ?></td>
                                        </tr>
                                        
                                        <tr>
                                        <td class="lbl">Tanggal Realisasi</td>
                                        <td colspan="3" valign="top"><p id="date"><?php echo date("Y-m-d") ?></p>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td class="lbl">Outstanding Uang Muka</td>
                                        <td><?php 
                                                if($dataOutstanding!='0')
                                                {  echo "Ada"; 
                                                echo "<input id='txtOutstanding'  name='txtOutstanding' type='hidden' value='Ada' >";
                                                }
                                                else
                                                {  echo "Tidak Ada";
                                                echo "<input id='txtOutstanding'  name='txtOutstanding' type='hidden' value='TidakAda' >";
                                                }
                                            ?></td>	  
                                        </tr>
                                    </tbody>
                                    </table>
                                    <br/>
                                    <?php if($dataOutstanding){  ?>
                                    <table id="tblOutStanding" border=0>
                                        <tbody>
                                            <tr class="tblHeader">
                                                <td >No Permohonan Uang Muka</td>     
                                                <td >Keterangan</td> 
                                                <td >Nominal</td> 
                                                <td >Penyebab Outstanding</td>       
                                            </tr>
                                            <?php for($i=0;$i<count($dataOutstanding);$i++){		
                                                if($dataOutstanding[$i]['Total']<>'' or $dataOutstanding[$i]['Total']<>null or $dataOutstanding[$i]['Total']<>0){?>
                                                <tr>
                                                    <td ><?php echo $dataOutstanding[$i]['NoUangMuka']; ?></td>
                                                    <td><?php echo $dataOutstanding[$i]['KeteranganPermohonan']; ?></td>
                                                    <td class='clsUang'><?php echo $dataOutstanding[$i]['Total']; ?></td>
                                                    <td><?php echo $dataOutstanding[$i]['ReasonOutstanding']; ?></td>
                                                    </tr>
                                            <?php }else{?>
                                                <td ><?php echo $dataOutstanding[$i]['NoUangMuka']; ?></td>
                                                    <td><?php echo $dataOutstanding[$i]['KeteranganPermohonan']; ?></td>
                                                    <td class='clsUang'><?php echo $dataOutstanding[$i]['TotalSPD']; ?></td>
                                                    <td><?php echo $dataOutstanding[$i]['ReasonOutstanding']; ?></td>
                                                    </tr>

                                            <?php }
                                            }?>
                                        </tbody>
                                    </table>
                                    <?php } ?>
                                    
                                    <br/>

                                    <table><tr><td>
                                            <iframe id="iFrameDetailUangMuka" src="<?php echo $this->config->base_url(); ?>index.php/UangMuka/DetailUangMuka/prerealisasi/<?php echo $trkUangMuka_array[0]['id']; ?>"  width="100%" height="400px" seamless frameBorder="0">
                                                        <p>Your browser does not support iframes.</p>
                                                    </iframe>
                                            </td></tr>
                                            <tr>
                                            <!-- <input class="btn btn-primary" type="button" id="refresh-btn" name="refresh-btn" value="Hitung Realisasi"> -->
                                            </tr>
                                    </table>

                                    <br>
                                    <div id="mytable">
                                    <table bordered="0">
                                   
                                    <tbody>
                                        <!-- Results table headers -->
                                        <tr>
                                            <td colspan="4" >Total Realisasi</td>				
                                            <td class='clsUang'><div id="divTotalRealisasi"><?php echo number_format($trkUangMuka_array[0]['TotalRealisasi']); ?></div></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" >Total Permohonan</td>				
                                            <td class='clsUang'><div id="divTotalPermohonan"><?php echo number_format($trkUangMuka_array[0]['TotalPermohonan']); ?></div></td>
                                        </tr>   
                                        <tr>
                                            <td colspan="4" ><div id="divDeskripsiTotalKelebihanKekurangan">Total Kelebihan/Kekurangan Uang Muka</div></td>				
                                            <td class='clsUang'><div id="divTotalKelebihanKekurangan"><?php echo number_format($trkUangMuka_array[0]['Selisih']); ?></div></td>
                                        </tr>  

                                        <?php
                                        if ( $trkUangMuka_array[0]['Selisih']> 0)
                                        {?>
                                            <tr>
                                            <td class="lbl">Pembayaran Sisa Uang Muka</td>
                                            <td > <select  id="txtPilihBayar" name="txtPilihBayar">
                                                <!--<option value ='CASH'>CASH</option> -->
                                                <option selected="selected" value ='TRANSFER'>TRANSFER</option>                                     
                                                </select>
                                            </td>	
                                        </tr>
                                        <tr>
                                        <td class="lbl">Bank</td>
                                        <td><input type="text" class="form-control" id="txtBank" name="txtBank" value="PERMATA BANK" readonly /></td> 
                                        </tr>
                                        <tr>
                                        <td class="lbl">No Rekening</td>
                                        <td ><input id="txtNoRek" class="form-control" name='txtNoRek' type="text" value="<?php
                                        if ( $trkUangMuka_array[0]['Selisih']> 0 && $trkUangMuka_array[0]['DPA'] == 1)
                                        {
                                            echo '0701607104' ;
                                        }else{
                                            echo '0701158113';
                                        }
                                        
                                        ?>
                                        " readonly />
                                        </td>
                                        </tr>
                                        <tr>
                                            <td class="lbl">Nama Penerima</td>
                                            <td ><input id="txtPenerima" class="form-control" name='txtPenerima' type="text" value="<?php
                                                if ( $trkUangMuka_array[0]['Selisih']> 0 && $trkUangMuka_array[0]['DPA'] == 1)
                                                {
                                                    echo 'DPA Satu - Opex' ;
                                                }else{
                                                    echo 'DPA Dua - Opex';
                                                }
                                                ?>
                                            "readonly/></td>
                                        </tr>
                                        <?php
                                        }else if ( $trkUangMuka_array[0]['Selisih']< 0){
                                            ?>
                                            <tr>
                                            <td class="lbl">Pembayaran Kekurangan Uang Muka</td>
                                            <td >
                                            <?php if ( $action=='edit'){ ?>
                                                <select  id="txtPilihBayar" name="txtPilihBayar">
                                                <option <?php if ($trkUangMuka_array[0]['TipeKembaliUangMuka'] == 'CASH' ) echo 'selected' ; ?> value ='CASH'>CASH</option> 
                                                <option <?php if ($trkUangMuka_array[0]['TipeKembaliUangMuka'] == 'TRANSFER' ) echo 'selected' ; ?> value ='TRANSFER'>TRANSFER</option>                                     
                                                </select>
                                            <?php } else { ?>    
                                                <select  id="txtPilihBayar" name="txtPilihBayar">
                                                <option value ='CASH'>CASH</option> 
                                                <option value ='TRANSFER'>TRANSFER</option>                                     
                                                </select>
                                            <?php }  ?> 
                                            </td>	
                                        </tr>
                                        <tr>
                                        <td class="lbl">Bank</td>
                                        <td><input type="text" class="form-control" id="txtBank" name="txtBank" value="
                                            <?php
                                                 if ( $action=='edit'){ echo $trkUangMuka_array[0]['BankKembaliUangMuka']; }
                                                 else echo "";
                                            ?>
                                            " /></td> 
                                        </tr>
                                        <tr>
                                        <td class="lbl">No Rekening</td>
                                        <td ><input id="txtNoRek" class="form-control" name='txtNoRek' type="text" value="
                                            <?php
                                                    if ( $action=='edit'){ echo $trkUangMuka_array[0]['NoRekKembaliUangMuka']; }
                                                    else echo "";
                                            ?>
                                            " />
                                        </td>
                                        </tr>
                                        <tr>
                                            <td class="lbl">Nama Penerima</td>
                                            <td ><input id="txtPenerima" class="form-control" name='txtPenerima' type="text" value="
                                            <?php
                                                    if ( $action=='edit'){ echo $trkUangMuka_array[0]['NmPenerimaUangMuka']; }
                                                    else echo "";
                                            ?>
                                            "/></td>
                                        </tr>
                                        <?php
                                        }else{
                                                                }                                        
                                        ?>
                        
                                    </tbody>
                                    </table>
                                    </div>    


                                    <table class="tblPureNoBorder">
                                    <tr><td>
                                        <input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin melakukan realisasi?'" id="submitRealisasiUMLain" name="submitRealisasiUMLain" 
                                        value="<?php if ( $action=='edit'){ echo "Update";}else{echo "Realisasi";} ?>">
                                        <a href="UangMuka/UangMukaController/Realisasi/<?php echo $trkUangMuka_array[0]['id'] ?>"><input class="btn btn-default" type="button" value="Batal"></a>			
                                            </td></tr>
                                    <tr><td><div id="divError" class="alert alert-danger"></div></td></tr>
                                    </table>
                                    <br/>
                            
                            

                            </form>
                        <!--Selesai Uang Muka Lain-->
                        <?php } else {?>
                            SUDAH DITERIMA OLEH HRGA, TIDAK DAPAT DIEDIT
                        <?php } ?>
        <?php } else {?>
            PROSES PEMBAYARAN BELUM DILAKUKAN, TRANSAKSI TIDAK DAPAT DI REALISASI
        <?php }?> 
    
    
	</body>
    <script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
        function hitungTotalDetil(){
            $.post("<?php echo $this->config->base_url(); ?>index.php/UangMuka/UangMukaController/ajax_KalkulasiTotalDetilUM",
                {idUangMuka:<?php echo $id; ?>},
                function(response){
                    alert(response);
                });
        }

		$('#txtNoPP').hide();
		$('#txtNoPP').keyup(function(){
			var kodePP = document.getElementById('txtNoPP').value;
			$('#txtDisplayPP').val(kodePP);			
		});
		
		$('#txtDisplayPP').change(function(){
			$('#txtNoPP').val($('#txtDisplayPP').val());
			var kodePP = $('#txtNoPP').val();			
		});
		
		$(".select2").select2();

        $('#txtKegiatan').change(function(){				
			$('#divLoadingSubmit').show();
            var idKegiatan = document.getElementById('txtKegiatan').value;  
            var tglMulai = document.getElementById("txtTanggalMulai").value;
            var tglSelesai = document.getElementById("txtTanggalSelesai").value; 

            if (tglSelesai=='' && tglMulai=='')
            {
                alert("Harap mengisi Tanggal Mulai dan Tanggal Selesai terlebih dulu"); 
            }
            else
            { 
                $.ajax({
                    url: '<?php echo $this->config->base_url(); ?>index.php/UangMuka/formUMLain/ajax_loadWaktuJenisKegiatanDPA',
                    type: "POST",             
                    data: {idKegiatan:idKegiatan,tglMulai:tglMulai,tglSelesai:tglSelesai},
                    dataType: 'json',
                    cache: false,                
                    success: function(data)
                    {	
                    console.log(data);   
                    //alert('testttt');                 
                                $('#txtWaktuBayarCepat').val(data[0].WaktuBayarUangMuka);
                                    $('#txtWaktuSelesaiUM').val(data[0].WaktuSelesaiUangMuka);												
                        
                        $('#divLoadingSubmit').hide();
                    },
                    error: function (request, status, error) {
                        console.log(error);
                        $('#divLoadingSubmit').hide();
                    }
                });
            }
		});
	</script>
    <script>
        $(function(){
      //Date picker
            $('#txtTanggalMulai').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
            $('#txtTanggalSelesai').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            });
        });
    </script>   
    <script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
   
</body>
</html>
 

</html>