<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">List Uang Muka</h3>
		</div>

		<div class="panel-body">
			<?php echo validation_errors(); ?>
			<table border=0>			
				
				<!--<tr>
					<td>Rekening *</td>
					<td colspan=3><select class="form-control" id="cmbRekening">
						<option value='BPT01DPA1'>Rekening Iuran DPA1</option>
						<option value='BPT02DPA1'>Rekening Investasi DPA1</option>
						<option value='BPT03DPA1'>Rekening Opex DPA1</option>
						<option value='BPT04DPA1'>Rekening MP DPA1</option>
						<option value='BPTDPA1'>Rekening CTD DPA1</option>
					</select></td>
				</tr>-->
				<tr>			
				<td>Tahun</td>
					<td colspan=3>
						<select class="form-control" id="cmbTahun" name="cmbTahun">
						<?php 
							for($i=date('Y')+1;$i>date('Y')-2;$i--)
							{
								$selected = '';
								if(date('Y') == $i )
									$selected = 'selected';
								echo "<option value='".$i."' $selected>".$i."</option>";								
							}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Bulan</td>
					<td colspan=3>
						<select class="form-control" id="cmbBulan" name="cmbBulan">
						<?php 
							for($i=1;$i<=12;$i++)
							{
								$selected = '';
								if(date('n') == $i )
									$selected = 'selected';
								echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
							}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Status</td>
					<td colspan=3>
						<select class="form-control" id="cmbStatus" name="cmbStatus">
							<option value="">Semua</option>
							<option value="1">Tgl Terima HRGA Kosong</option>
							<option value="2">Tgl Terima Finance Kosong</option>
							<option value="3">Tgl Bayar Finance Kosong</option>
							<option value="4">No BE Kosong</option>
						</select>
					</td>
				</tr>
				
				
				<tr>
					<td></td>
					<td>
					<div class="col-sm-4">
						<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari"></div>
						<div id="divLoadingCari" hidden class="col-sm-7" style="padding-top:8px"><i id="spinner" class="fa fa-refresh fa-spin"></i>Loading...</div>
					</td>
					</td>
				</tr>
			</table>
		
			<div id="divContent" hidden>
			<H4><b>List Pengajuan Uang Muka</b></H4>
			<iframe id="iFramePengajuanUangMuka" src="<?php echo $this->config->base_url(); ?>index.php/UangMuka/ListUangMuka" width="100%" height="500px" seamless frameBorder="0">
			<p>Your browser does not support iframes.</p>
			</iframe>
			

			<H4><b>List Realisasi Uang Muka</b></H4>
			<iframe id="iFrameRealisasiUangMuka" src="<?php echo $this->config->base_url(); ?>index.php/UangMuka/ListUangMuka" width="100%" height="500px" seamless frameBorder="0">
			<p>Your browser does not support iframes.</p>
			</iframe>
			

			
			
			</div>
		</div>
	</body>
	<script type="text/javascript">
		
		
		function search(){
			$('#divLoadingCari').show();
			var Tahun = document.getElementById('cmbTahun').value;
			var Bulan = document.getElementById('cmbBulan').value;
			var Status = document.getElementById('cmbStatus').value;
			var TanggalAwal = 1;
			var TanggalAkhir = 0;

			//alert (Bulan);

			if (Bulan == 1 ||  Bulan == 3 || Bulan == 5 || Bulan == 7 || Bulan == 8 || Bulan == 10 || Bulan == 12)
				TanggalAkhir=31;
			else if (Bulan == 2)
			TanggalAkhir=28;
			else
				TanggalAkhir=30;
			
			if (TanggalAwal<10 ) TanggalAwal= '0'+ TanggalAwal;
			if (TanggalAkhir<10 ) TanggalAkhir= '0'+ TanggalAkhir;
			
			if (Bulan<10 ) Bulan= '0'+ Bulan;
			
			
			var PilihanTglAwal = Tahun + "-" + Bulan + "-" + TanggalAwal;
			var PilihanTglAkhir = Tahun + "-" + Bulan + "-" + TanggalAkhir;
			
			
			
			
			//alert ( 'index.php/Cashflow/DPA1CashFlow/search/'+ Rekening +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir);
			var iframe1 = document.getElementById('iFramePengajuanUangMuka');
			iframe1.src = '<?php echo $this->config->base_url(); ?>index.php/UangMuka/ListUangMuka/search_pengajuan_user'+ '/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir+'/'+Status;
			

			var iframe2 = document.getElementById('iFrameRealisasiUangMuka');
			iframe2.src = '<?php echo $this->config->base_url(); ?>index.php/UangMuka/ListUangMuka/search_realisasi_user'+ '/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir+'/'+Status;
			$('#divLoadingCari').hide();
			$('#divContent').show();
		}
		
		
	
		
	</script>
</html>
