<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		
		function btnApprove_click(){
			if(confirm('Apakah Anda yakin ingin approval data ini?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/UangMuka/AkomodasiTiketController/ProsesApprovalATUser/<?php echo $data['KodeAkomodasiTiket']; ?>/AP/<?php echo $data['npkpemohon']; ?>',
					{},
					function(response){
						alert(response);
						window.location.replace('<?php echo $this->config->base_url(); ?>index.php/UserTask');
					}
				);
			}
		}
		
		function btnDecline_click(){
			if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/UangMuka/AkomodasiTiketController/ProsesApprovalATUser/<?php echo $data['KodeAkomodasiTiket']; ?>/DE/<?php echo $data['npkpemohon']; ?>',
					{},
					function(response){
						alert(response);
						window.location.replace('<?php echo $this->config->base_url(); ?>index.php/UserTask');
					}
				);
			}
		}
		
		
	</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						No Permohonan :
					</div>
					<div class="col-md-6">
						<?php echo $data['NoAkomodasiTiket']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Nama Karyawan :
					</div>
					<div class="col-md-6">
						<?php echo $data['namapemohon']; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Tanggal Berangkat 
					</div>
					<div class="col-md-6">
						<?php echo $data['TanggalBerangkat']; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Tanggal Kembali :
					</div>
					<div class="col-md-6">
						<?php echo $data['TanggalKembali']; ?>
					</div>
				</div>
				
				<br/>
				<div class="row">
					<div class="col-md-3">
						Tujuan :
					</div>
					<div class="col-md-6">
						<?php echo $data['Tujuan']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Alasan Perjalanan:
					</div>
					<div class="col-md-6">
						<?php echo $data['Alasan']; ?>
					</div>
					
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Hotel
					</div>
					<div class="col-md-2">		
					<?php echo $data['NamaHotel']; ?>				
					</div>					
				</div>
				<br/>				
				<div class="row">
					<div class="col-md-3">
						Tanggal Check In :
					</div>
					<div class="col-md-2">
						<?php echo $data['TanggalCheckIn']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Tanggal Check Out :
					</div>
					<div class="col-md-2">
						<?php echo $data['TanggalCheckOut']; ?> 
					</div>
				</div>
				<br/> 
				
				<br/>
				<div class="row">
					<div class="col-md-3">
						Tiket
					</div>
					<div class="col-md-2">	
					<?php echo $data['TiketPesawat']; ?>					
					</div>					
				</div>
				<br/>				
				<div class="row">
					<div class="col-md-3">
						Tanggal Keberangkatan Pesawat :
					</div>
					<div class="col-md-2">
						<?php echo $data['TanggalBerangkatPesawat']; ?>  :
					</div>
					<div class="col-md-2">
						<?php echo $data['JamBerangkatPesawat']; ?> 
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Tanggal Kepulangan Pesawat :
					</div>
					<div class="col-md-2">
						<?php echo $data['TanggalKepulanganPesawat']; ?> 
					</div>
					<div class="col-md-2">
						<?php echo $data['JamKepulanganPesawat']; ?> 
					</div>
				</div>
				<br/>
				<div class="row">
				
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				
				</div>
				<br/>
				
				<div class="row">
					<div align="right">
						
						<input id="btnBatal" class="btn btn-primary" type="button" value="Approve" onClick="btnApprove_click()">
						<input id="btnBatal" class="btn" type="button" value="Decline" onClick="btnDecline_click()">
		
						
					</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
</html>
