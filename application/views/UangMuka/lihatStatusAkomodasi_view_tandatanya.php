<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class CetakFormUangMuka extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('number');
			$this->load->model('menu','',TRUE);
			$this->load->model('uangmuka_model','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}else{
					echo 'gagal';
				}
			
				$get_NoUangMuka = $this->input->get('NoUangMuka');
				$trxuangmuka = $this->uangmuka_model->getHeaderTrxUangMukabyNoUM($get_NoUangMuka);
				if($trxuangmuka){
					$trxuangmuka_array = array();
					foreach($trxuangmuka as $row){
						$trxuangmuka_array[] = array(
							'DPA' => $row->DPA,
							'NoUangMuka' => $row->NoUangMuka,
							'TanggalUangMuka' => $row->TanggalUangMuka,
							'TanggalMulai' => $row->TanggalMulai,
							'TanggalSelesai' => $row->TanggalSelesai,
							 'NPKPemohon' => $row->NPKPemohon,
							 'namaCreatedBy'=> $row->nama,
							 'namaNPKPemohon'=> $row->atasan,
							 'TipeBayarUangMuka'=> $row->TipeBayarUangMuka,
							 'BankBayarUangMuka'=> $row->BankBayarUangMuka,
							 'NoRekBayarUangMuka'=> $row->NoRekBayarUangMuka,
							 'NmPenerimaBayarUangMuka'=> $row->NmPenerimaBayarUangMuka,
							 'WaktuBayarTercepat'=> $row->WaktuBayarTercepat,
							 'WaktuPenyelesaianTerlambat'=> $row->WaktuPenyelesaianTerlambat,
							 'NoPP'=> $row->NoPP,
							 'Keterangan'=> $row->KeteranganPermohonan
						);
					}
				}else{
					echo 'gagalheader';
				}

				$detailuangmuka = $this->uangmuka_model->getDetailTrxUangMukabyNoUM($get_NoUangMuka);
				if($detailuangmuka){
					$detailuangmuka_array = array();
					foreach($detailuangmuka as $row){
						$detailuangmuka_array[] = array(
							'iddtluangmuka' => $row->iddtluangmuka,
							'JenisBiayaPermohonan' => $row->JenisBiayaPermohonan,
							'KeteranganPermohonan' => $row->KeteranganPermohonan,
							'JumlahPermohonan' => $row->JumlahPermohonan,
							'HargaPermohonan' => $row->HargaPermohonan,
							 'TotalPermohonan' => $row->TotalPermohonan							 
						);
					}
				}else{
					echo 'gagaldetil';
				}

				/*data Outstanding jika ada*/
						
				$NPKAtasan = $this->uangmuka_model->getAtasan($session_data['npk']);
				$dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
				
				if ($dataOutstanding)
				{
					foreach($dataOutstanding as $row)
					{	
						$dataOutstandingDetail[] = array(
							'NoUangMuka' => $row->NoUangMuka,
							'KeteranganPermohonan' => $row->KeteranganPermohonan,
							'Total' => $row->Total,
							'TotalSPD' => $dataOutstanding->TotalSPD,
							'ReasonOutStanding' => $row->ReasonOutStanding
						);		
					}
				}

				

						
				if($trxuangmuka && $dataOutstanding) //// UangMuka & Outstanding
				{
							$data2 = array(
								'title' => 'Cetak Form Uang Muka',
								'trxuangmuka_array' => $trxuangmuka_array,
								'detailuangmuka_array' => $detailuangmuka_array,								
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'dataOutstandingDetail'=>$dataOutstandingDetail,
								'flagoutstanding' => '1'
							);
				}			
				else
				{ // Uang Muka Tanpa Outsttanding							

							$data2 = array(
								'title' => 'Cetak Form Uang Muka',
								'trxuangmuka_array' => $trxuangmuka_array,
								'detailuangmuka_array' => $detailuangmuka_array,								
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'flagoutstanding' => '0'
							);
				}					
				

				$this->load->helper(array('form','url'));
				$this->load->view('UangMuka/cetakFormUangMuka_view',$data2);
				//$this->template->load('default','cetakFormUangMuka_view',$data2);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
	}
?>