<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">  
    <meta charset="utf-8" />
 
<?php 
foreach($body->css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<?php foreach($body->js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<script>

	$(document).ready(function(){
		<?php if($this->session->flashdata('msg')){ ?>
		alert('<?php echo $this->session->flashdata('msg'); ?>');
		<?php } ?>
	});
</script>

</head>
<!-- <body onload="parent.alertsizeBudgetMonitoring(document.body.clientHeight);"> -->
<body onload="">
<!-- Beginning header -->

<!-- End of header-->
    
    <div>
        <?php echo $body->output; ?>
    </div>
<!-- Beginning footer -->
<div></div>
<!-- End of Footer -->
</body>
<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
    <script type="text/javascript">
        $(".select2").select2();
    </script>

</html>