
<html>
<style type="text/css">
@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
</style>
<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
<!--<script type="text/javascript" src="../assets/js/jquery.datepick.js"></script>-->
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/myfunctionUMAkomodasiTiket.js?v=1"></script>

<head>
<title>Form Akomodasi Tiket</title>		
</head>
<body>
<?php echo validation_errors(); ?>

<form action="submitUangMukaAT" method="post" accept-charset="utf-8">
<table border=0>
<tbody>
<!-- Results table headers -->
<tr class="tblHeader">
<td colspan="2">Permintaan Akomodasi Tiket</td>
<td colspan="3"><image id="logoDPA" src='<?php echo $this->config->base_url(); ?>assets/images/logoDPA.png'>*DANA PENSIUN ASTRA<br/>
<?php 
                                            $checked1 = "";
                                            $checked2 = "";
                                            if($trxAT_array['DPA'] == 1){
                                                $checked1 = "checked";
                                            }else{
                                                $checked2 = "checked";
                                            }
                                        ?>
 <input type="radio" id="rbDpaSatu_" name="rbDPA_" value="1" <?php echo $checked1; ?> disabled >SATU
 <input type="radio" id="rbDpaDua_" name="rbDPA_" value="2" <?php echo $checked2; ?> disabled>DUA
</td>
</tr>
<tr>
<td class="lbl">No Permohonan</td>
<td bgcolor="#D3D3D3"><input id="txtNoAT_" class="form-control" name='txtNoAT_' type="text" size="50%" value="<?php echo $trxAT_array['NoAkomodasiTiket']?>" readonly>
<input class = "form-control" id="txtNoAT" name='txtNoAT' type="hidden"  size="50%" value="<?php echo $trxAT_array['NoAkomodasiTiket']?>" >
</td>
<td colspan="3" rowspan="8" align="center">
<div id="catatan">
	<div class="lblTop">Catatan Penting</div>
	<div id="txtCatatan"><ol><li>Permintaan uang muka perjalanan dinas diajukan selambat-lambatnya 5 hari kerja sebelum melakukan perjalanan dinas.
<li>Pengeluaran uang muka paling lambat diterima 1 hari kerja sebelum keberangkatan (SPD).
<li>Pertanggungjawaban atas uang muka untuk biaya-biaya perjalanan dinas wajib dilaporkan selambat-lambatnya 3 (tiga) hari setelah tiba di tempat kerja.
</ol>
</div>
	</td>
</tr>
<tr>
<td class="lbl">Tanggal Pengajuan </td>
<td><?php echo date("j F Y"); ?></td>	  
</tr>
<tr>
<td class="lbl">NPK</td>
<td><input type="text" class='bordered' id="txtNPK" name="txtNPK" value="<?php echo $npk ?>" readonly />
<input type="hidden" class='bordered' id="txtNPKatasan" name="txtNPKatasan" value="<?php echo $dataUserDetail['NPKAtasan']; ?>" readonly />
</td>   

</tr>
<tr>
<td class="lbl">Golongan</td>
<td><?php echo $dataUserDetail['golongan'] ?></td>

</tr>
<tr>
<td class="lbl">Nama</td>
<td><?php echo $nama ?></td>

</tr>
<tr>
<td class="lbl">Jabatan</td>
<td><?php echo $dataUserDetail['jabatan'] ?></td>

</tr>
<tr>
<td class="lbl">Departemen</td>
<td><?php echo $dataUserDetail['departemen'] ?></td>

</tr>
<tr>
<td class="lbl">Atasan </td> 
<td> <?php echo $dataUserDetail['atasan'] ?></td>

</tr>
<tr>
<td class="lbl">*Tanggal Keberangkatan</td>
<td colspan="4"><?php echo $trxAT_array['TanggalBerangkat']; ?></td>

</tr>
<tr>
<td class="lbl">*Tanggal Kembali</td>
<td colspan="4"><?php echo $trxAT_array['TanggalKembali']; ?></td>
</tr>
<tr>
<td class="lbl">*Tujuan</td>
<td colspan="4"><?php echo $trxAT_array['Tujuan']; ?></td>
</tr>
<tr>
<td class="lbl">*Alasan Perjalanan</td>
<td colspan="4"><?php echo $trxAT_array['Alasan']; ?></td>
</tr>
</tbody>
</table>
<br/>

<table border=0>
<tbody>
<!-- Results table headers -->
<tr class="tblHeader">
<td colspan="4">Permintaan Akomodasi </td>	
</tr>
<tr>
<td class="lbl">Hotel</td>
	<td ><?php echo $trxAT_array['NamaHotel']; ?></td>
</tr>
<tr>
<td class="lbl">   Tanggal Check In :</td>
	<td >	<?php echo $trxAT_array['TanggalCheckIn']; ?>	</td>     
</tr>
<tr>
<td class="lbl">   Tanggal Check Out :</td>
	<td ><?php echo $trxAT_array['TanggalCheckOut']; ?></td> 
</tr>
<tr>
<td class="lbl">Tiket</td>
	<td ><?php echo $trxAT_array['TiketPesawat']; ?></td>
</tr>
<tr>
<td class="lbl">   Tanggal Keberangkatan Pesawat :</td>
	<td ><?php echo $trxAT_array['TanggalBerangkatPesawat']; ?>
	</td>
	<td class="lbl">   Jam :</td>
	<td ><?php echo $trxAT_array['JamBerangkatPesawat']; ?></td>      
</tr>
<tr>
<td class="lbl">   Tanggal Kepulangan Pesawat :</td>
	<td ><?php echo $trxAT_array['TanggalKepulanganPesawat']; ?>
	</td>
	<td class="lbl">   Jam :</td>
	<td > <?php echo $trxAT_array['JamKepulanganPesawat']; ?></td>      
</tr>

</tbody>
</table>

<br><br>
<?php if($npk==$NPKGA) {?>
                                    <table border=0>
                                    <tbody>
                                        <!-- Results table headers -->
                                        <tr class="tblHeader">
                                        <td colspan="2">Create Permintaan Uang Muka</td>
                                        <td colspan="3"><image id="logoDPA" src='<?php echo $this->config->base_url(); ?>assets/images/logoDPA.png'>*DANA PENSIUN ASTRA<br/>
                                        <?php 
                                            $checked1 = "";
                                            $checked2 = "";
                                            if($trxAT_array['DPA'] == 1){
                                                $checked1 = "checked";
                                            }else{
                                                $checked2 = "checked";
                                            }
                                        ?>
                                        <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked1; ?> >SATU
                                        <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked2; ?>  >DUA
                                        </td>
                                        </tr>
                                        <tr>
                                        <td class="lbl">No Uang Muka</td>
                                        <td bgcolor="#D3D3D3"></td>	 
                                        </tr>
                                        <tr>
                                        <td class="lbl">NPK Pemohon</td>
                                        <td><input type="text" class='bordered' id="txtNPK" name="txtNPK" value="<?php echo $trxAT_array['npkpemohon'];  ?>" readonly />
                                            <input type="hidden" class='bordered' id="txtNPKLogin" name="txtNPKLogin" value="<?php echo $trxAT_array['npkpemohon'];  ?>"  />
                                        </td>                
                                        </tr>
                                        <tr>
                                        <td class="lbl">Keterangan</td>
                                        <td colspan="4"><input id="txtKeterangan" class="form-control" name='txtKeterangan' type="text" size="50%" value="<?php echo 'Uang Muka Akomodasi & Tiket atas'. $trxAT_array['Alasan'].' '.$trxAT_array['Tujuan']. ' an/ '.$trxAT_array['namapemohon'] ; ?>"></td>
                                        </tr>
                                        <tr>
                                            <td class="lbl">*Tanggal Mulai</td>
                                            <td >
                                                                
                                                <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="txtTanggalMulai" name="txtTanggalMulai"  value="<?php echo $trxAT_array['TanggalBerangkat']; ?>" >
                                                </div>
                                                <!-- /.input group -->
                                            
                                            </td>
                                            
                                            </tr>
                                            <tr>
                                            <td class="lbl">*Tanggal Selesai</td>
                                            <td ><!--<input  class='bordered'  id="txtTanggalSelesai" name='txtTanggalSelesai' type="text" size="15" >-->
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="txtTanggalSelesai" name="txtTanggalSelesai" value="<?php echo $trxAT_array['TanggalKembali']; ?>">
                                            </div>
                                            </td>
                                            </tr>
                                        <tr>
                                        <td class="lbl">Tanggal Permohonan Uang Muka</td>
                                        <td><?php echo date("j F Y"); ?></td>	  
                                        </tr>
                                        
                                        <tr>
                                        <td class="lbl">Pilih No. Persetujuan</td>
                                        <td colspan="4" valign="top"><input class="form-control" type="text" id="txtNoPP"/>
											<select class="form-control select2" id="txtDisplayPP"  name="txtDisplayPP">
											<option value =''></option>
											<?php foreach($dataNoPP as $row){
												echo "<option value='".$row->NoPP."'>".$row->display." </option>";
											} ?>
											</select>
                                        </td>
                                        </tr>

                                        <tr>
                                        <td class="lbl">Jenis Kegiatan Uang Muka</td>
                                        <td colspan="3" valign="top">
											<select class="form-control"  id="txtKegiatan" name="txtKegiatan"> 
											<option value =''>Pilih salah satu</option>                               
											<?php foreach($dataJenisKeterangan as $row){
												echo "<option value='".$row->IdKegiatan."'>".$row->JenisKegiatan." </option>";
											} ?>
											</select>
                                        </td>
                                        <td>					
                                            <div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
                                        </td>
                                        </tr>
                                        
                                        <tr>
                                        <td class="lbl">Outstanding Uang Muka</td>
                                        <td><?php 
                                                if($dataOutstanding!='0')
                                                {  echo "Ada"; 
                                                echo "<input id='txtOutstanding'  name='txtOutstanding' type='hidden' value='Ada' >";
                                                }
                                                else
                                                {  echo "Tidak Ada";
                                                echo "<input id='txtOutstanding'  name='txtOutstanding' type='hidden' value='TidakAda' >";
                                                }
                                            ?></td>	  
                                        </tr>
                                    </tbody>
                                    </table>
                                    <br/>

                                    
                                    <table><tr><td>
									<iframe id="iFrameDetailUangMuka" src="<?php echo $this->config->base_url(); ?>index.php/UangMuka/DetailUangMuka/index/" width="100%" height="400px" seamless frameBorder="0">
										<p>Your browser does not support iframes.</p>
									</iframe>
                                            </td></tr>
                                    </table>

                                    <br>
                                    <table border=0>
                                   
                                    <tbody>
                                        <!-- Results table headers -->
                                        
										<tr>
											<td class="lbl">Pembayaran Uang Muka</td>
											<td > <select  id="txtPilihBayar" name="txtPilihBayar">
												<option value ='CASH'>CASH</option> 
												<option value ='TRANSFER'>TRANSFER</option>                                     
												</select>
											</td>	
											<td class="lbl">Waktu Pembayaran Tercepat</td>
											<td><input type="text" class='bordered' id="txtWaktuBayarCepat" name="txtWaktuBayarCepat" value=""  readonly/></td>
										</tr>
                                        <tr>
											<td class="lbl">Bank</td>
											<td><input type="text" class="form-control" id="txtBank" name="txtBank" value="" /></td>
											<td class="lbl">Paling Lambat Waktu Penyelesaian</td>
											<td><input type="text" class='bordered' id="txtWaktuSelesaiUM" name="txtWaktuSelesaiUM" value="" readonly /></td>
										</tr>
										<tr>
											<td class="lbl">No Rekening</td>
											<td ><input id="txtNoRek" class="form-control" name='txtNoRek' type="text" value=""></td>
										</tr>
										<tr>
											<td class="lbl">Nama Penerima</td>
											<td ><input id="txtPenerima" class="form-control" name='txtPenerima' type="text" value=""></td>
										</tr>
                                    </tbody>
                                    </table>

                                    <br>
                                    <!-- out standing -->
                                    <?php   if($dataOutstanding!='0') { ?>
                                    Outstanding Uang Muka
                                    <table id="tblOutstanding" border=0>
                                    <tbody>
                                        <tr class="ttd">
                                        <td >No Uang Muka</td>
                                        <td >Keterangan</td>
                                        <td >Total</td>
                                        <td >Alasan Outstanding</td>
                                        </tr>
                                        <?php 
                                          for($i=0;$i<count($dataOutstanding);$i++){
                                            if($dataOutstanding[$i]['Total']<>'' or $dataOutstanding[$i]['Total']<>null or $dataOutstanding[$i]['Total']<>0){
                                              echo "<tr><td>".$dataOutstanding[$i]['NoUangMuka'] ." </td>
                                                      <td>". $dataOutstanding[$i]['KeteranganPermohonan'] ."</td>
                                                      <td>Rp ". number_format($dataOutstanding[$i]['Total'],2,',','.') ."</td>   
                                                      <td>". $dataOutstanding[$i]['ReasonOutstanding'] ."</td>                
                                                      </tr> ";    
                                            }else{
                                              echo "<tr><td>".$dataOutstanding[$i]['NoUangMuka'] ." </td>
                                                      <td>". $dataOutstanding[$i]['KeteranganPermohonan'] ."</td>
                                                      <td>Rp ". number_format($dataOutstanding[$i]['TotalSPD'],2,',','.') ."</td>   
                                                      <td>". $dataOutstanding[$i]['ReasonOutstanding'] ."</td>                
                                                      </tr> "; 
                                            }             
                                          }
                                        ?>    
                                    </tbody>
                                    </table>
                                    <?php }?>
                                    <br> 


                                    <table class="tblPureNoBorder">
									<tr><td>
										<input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin menyimpan Pengajuan ini ke dalam database?')" id="btnSubmit" name="submitUangMukaAT" value="Simpan">
										<a href="UangMuka/AkomodasiTiketController/CreateAkomodasiTiket/<?php echo $trxAT_array['NoAkomodasiTiket']; ?>"><input class="btn btn-default" type="button" value="Batal"></a>			
											</td></tr>
									<tr><td><div id="divError" class="alert alert-danger"></div></td></tr>
									</table>
									<br/>
<?php } else
if ($trxAT_array['KeteranganStatus']=='PE') {?>
Permohonan sedang diajukan ke atasan. Silahkan menunggu tindakan.
<a href="UangMukaController/ViewListUangMukaUser">Kembali Ke Menu Awal</a> 
<?php }?>
</form>

</body>
<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
	
		
		$('#txtNoPP').hide();
		$('#txtNoPP').keyup(function(){
			var kodePP = document.getElementById('txtNoPP').value;
			$('#txtDisplayPP').val(kodePP);			
		});
		
		$('#txtDisplayPP').change(function(){
			$('#txtNoPP').val($('#txtDisplayPP').val());
			var kodePP = $('#txtNoPP').val();			
		});
		
		$(".select2").select2();

        $('#txtKegiatan').change(function(){				
			$('#divLoadingSubmit').show();
            var idKegiatan = document.getElementById('txtKegiatan').value;  
            var tglMulai = document.getElementById("txtTanggalMulai").value;
            var tglSelesai = document.getElementById("txtTanggalSelesai").value; 

            if (tglSelesai=='' && tglMulai=='')
            {
                alert("Harap mengisi Tanggal Mulai dan Tanggal Selesai terlebih dulu"); 
            }
            else
            { 
                $.ajax({
                    url: '<?php echo $this->config->base_url(); ?>index.php/UangMuka/formUMLain/ajax_loadWaktuJenisKegiatanDPA',
                    type: "POST",             
                    data: {idKegiatan:idKegiatan,tglMulai:tglMulai,tglSelesai:tglSelesai},
                    dataType: 'json',
                    cache: false,                
                    success: function(data)
                    {	
                    console.log(data);   
                    //alert('testttt');                 
                                $('#txtWaktuBayarCepat').val(data[0].WaktuBayarUangMuka);
                                    $('#txtWaktuSelesaiUM').val(data[0].WaktuSelesaiUangMuka);												
                        
                        $('#divLoadingSubmit').hide();
                    },
                    error: function (request, status, error) {
                        console.log(error);
                        $('#divLoadingSubmit').hide();
                    }
                });
            }
		});
	</script>

<script>
      $(function(){
      //Date picker
          $('#txtTanggalMulai').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
          });
          $('#txtTanggalSelesai').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
          });
          
      });      
    </script>
   
       
    <script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
</html>