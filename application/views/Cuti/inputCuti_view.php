<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	function alertsizeCutiJenisTidakHadir(pixels){
		pixels+=32;
		document.getElementById('iFrameCutiJenisTidakHadir').style.height=pixels+"px";
	}
	
	$(function() {
		$("#iFrameCutiPelimpahanWewenang").hide(1000);
		$("#txtPelimpahanWewenang").hide(1000);
		$("#txtPelimpahanWewenangEmail").hide(1000);

		$("#btnCancel").click(function(){
			$.post(
				'<?php echo $this->config->base_url(); ?>index.php/Cuti/CutiController/ajax_cancelCuti',
				{ npk: '<?php echo $npk; ?>',  kodeusertask: '<?php echo $kodeusertask; ?>', editmode: 'false' },
				function(response){
					alert(response);
					<?php if($editmode=='0'){ ?>
						window.location.replace('<?php echo site_url('Cuti/CutiController/ViewCutiUser'); ?>');
					<?php }else{ ?>
						window.location.replace('<?php echo site_url('UserTask'); ?>');
					<?php } ?>
				}
			);
		});
	

		$("#btnSubmit").click(function(){	
			var NamaEmail1val =''; var AlamatEmail1val = '';
			var NamaEmail2val =''; var AlamatEmail2val = '';
			var NamaEmail3val =''; var AlamatEmail3val = '';
			var NamaEmail4val =''; var AlamatEmail4val = '';
			var NamaEmail5val =''; var AlamatEmail5val = '';
			
					
				
			if(document.getElementById('rbYa').checked)
			{
				NamaEmail1 =$('#txtNamaEmail1');  AlamatEmail1 = $('#txtAlamatEmail1');
				NamaEmail2 =$('#txtNamaEmail2');  AlamatEmail2 = $('#txtAlamatEmail2');
				NamaEmail3 =$('#txtNamaEmail3');  AlamatEmail3 = $('#txtAlamatEmail3');
				NamaEmail4 =$('#txtNamaEmail4');  AlamatEmail4 = $('#txtAlamatEmail4');
				NamaEmail5 =$('#txtNamaEmail5');  AlamatEmail5 = $('#txtAlamatEmail5');

				NamaEmail1val=NamaEmail1.val();AlamatEmail1val=AlamatEmail1.val();
				NamaEmail2val=NamaEmail2.val();AlamatEmail2val=AlamatEmail2.val();
				NamaEmail3val=NamaEmail3.val();AlamatEmail3val=AlamatEmail3.val();
				NamaEmail4val=NamaEmail4.val();AlamatEmail4val=AlamatEmail4.val();
				NamaEmail5val=NamaEmail5.val();AlamatEmail5val=AlamatEmail5.val();
			}

		//	alert (NamaEmail1val);
			$('#divLoadingSubmit').show();
			$.post(
				'<?php echo $this->config->base_url(); ?>index.php/Cuti/CutiController/ajax_submitCuti',
				{ npk: '<?php echo $npk; ?>', kodeusertask: '<?php echo $kodeusertask; ?>', editmode: '<?php 
					if($editmode=='0')
					{ 
						echo 'false'; 
					}
					else if($editmode == '1')
					{
						echo 'true';
					}
					
					?>', NamaEmail1: NamaEmail1val,AlamatEmail1: AlamatEmail1val,
						NamaEmail2: NamaEmail2val,AlamatEmail2: AlamatEmail2val,
						NamaEmail3: NamaEmail3val,AlamatEmail3: AlamatEmail3val,
						NamaEmail4: NamaEmail4val,AlamatEmail4: AlamatEmail4val,
						NamaEmail5: NamaEmail5val,AlamatEmail5: AlamatEmail5val
						 },
				function(response){
					alert(response);
					$('#divLoadingSubmit').hide();
					<?php if($editmode=='0'){ ?>
						window.location.replace('<?php echo site_url('Cuti/CutiController/ViewCutiUser'); ?>');
					<?php }else{ ?>
						window.location.replace('<?php echo site_url('UserTask'); ?>');
					<?php } ?>
				}
			);
		});

		$("#rbYa").click(function(){
			$("#iFrameCutiPelimpahanWewenang").show(1000);
			$("#txtPelimpahanWewenang").show(1000);			
		});
		
		$("#rbTidak").click(function(){
				$("#iFrameCutiPelimpahanWewenang").hide(1000);
				$("#txtPelimpahanWewenang").hide(1000);
				$("#txtPelimpahanWewenangEmail").hide(1000);
		});

		$("#rbEmailYa").click(function(){
				$("#txtPelimpahanWewenangEmail").show(1000);					
		});
		
		$("#rbEmailTidak").click(function(){
				$("#txtPelimpahanWewenangEmail").hide(1000);
		});
	});

	

	</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Input Cuti</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						NPK 
					</div>
					<div class="col-md-6">
						<?php echo $npk; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Nama 
					</div>
					<div class="col-md-6">
						<?php echo $nama; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Departemen 
					</div>
					<div class="col-md-6">
						<?php echo $departemen; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Sisa Cuti Tahunan 
					</div>
					<div class="col-md-6">
						<?php echo $sisacutitahunan; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Sisa Cuti Besar 
					</div>
					<div class="col-md-6">
						<?php echo $sisacutibesar; ?>
					</div>
				</div>
				<br/>
				
				<iframe id="iFrameCutiJenisTidakHadir" src="<?php echo $this->config->base_url(); ?>index.php/Cuti/cutiJenisTidakHadir/index/<?php 
				if($editmode == '0')
				{ 
					echo $npk;
				} 
				else if($editmode == '1')
				{
					echo $kodeusertask."/editmode";
				}
				
				?>" width="100%" height="200px" seamless frameBorder="0">
					<p>Your browser does not support iframes.</p>
				</iframe>	
				<br>
				<div class="row">
					<div class="col-md-3">
						History Catatan :
					</div>
					<div class="col-md-9">
						<?php echo $historycatatan; ?>
					</div>
				</div>
				<br>

				*Apakah disertai dengan Pelimpahan Wewenang?

				<input class="clsPelimpahanWewenang" type="radio" id="rbYa" name="rbPelimpahanWewenang" value="1">Ya
				<input class="clsPelimpahanWewenang" type="radio" id="rbTidak" name="rbPelimpahanWewenang" value="0">Tidak				
				
				<iframe id="iFrameCutiPelimpahanWewenang" src="<?php echo $this->config->base_url(); ?>index.php/PelimpahanWewenang/PelimpahanWewenang/index/" width="100%" height="400px" seamless frameBorder="0">
					<p>Your browser does not support iframes.</p>
				</iframe>

				<div id=txtPelimpahanWewenang>
				*
				Mohon dijelaskan secara singkat dan jelas dasar dari adanya pelimpahan wewenang, dan jangka waktu 
				dari pelimpahan wewenang tersebut. Apabila dibutuhkan dokumen pendukung lainnya, mohon dilampirkan 
				bersamaan dengan formulir ini (ex : URF, dll)
				<br>
				*Apakah disertai dengan pemberian notifikasi pada email dan/atau pelimpahan untuk email yang masuk?
				<input class="clsEmail" type="radio" id="rbEmailYa" name="rbEmailPelimpahanWewenang" value="1">Ya
				<input class="clsEmail" type="radio" id="rbEmailTidak" name="rbEmailPelimpahanWewenang" value="0">Tidak				

				</div>

				<div id=txtPelimpahanWewenangEmail>
				<br>
				Selama saya Izin/Cuti, untuk semua email yang masuk pada email saya, dialihkan kepada:
				<table id="tblEmail" border=0>
				<tbody>
				    <tr>					
					<td width="80px" class="lbl">Nama </td>
					<td width="300px"><input id="txtNamaEmail1" class="form-control" name='txtNamaEmail1' type="text" size="20" ></td>					
					<td width="80px" class="lbl">Email </td>
					<td width="400px"><input id="txtAlamatEmail1" class="form-control" name='txtAlamatEmail1' type="text" size="20" ></td>						
					</tr>
					<tr>					
					<td width="80px" class="lbl">Nama </td>
					<td width="300px"><input id="txtNamaEmail2" class="form-control" name='txtNamaEmail2' type="text" size="20" ></td>					
					<td width="80px" class="lbl">Email </td>
					<td width="400px"><input id="txtAlamatEmail2" class="form-control" name='txtAlamatEmail2' type="text" size="20" ></td>						
					</tr>
					<tr>					
					<td width="80px" class="lbl">Nama </td>
					<td width="300px"><input id="txtNamaEmail3" class="form-control" name='txtNamaEmail3' type="text" size="20" ></td>					
					<td width="80px" class="lbl">Email </td>
					<td width="400px"><input id="txtAlamatEmail3" class="form-control" name='txtAlamatEmail3' type="text" size="20" ></td>						
					</tr>
					<tr>					
					<td width="80px" class="lbl">Nama </td>
					<td width="300px"><input id="txtNamaEmail4" class="form-control" name='txtNamaEmail4' type="text" size="20" ></td>					
					<td width="80px" class="lbl">Email </td>
					<td width="400px"><input id="txtAlamatEmail4" class="form-control" name='txtAlamatEmail4' type="text" size="20" ></td>						
					</tr>
					<tr>					
					<td width="80px" class="lbl">Nama </td>
					<td width="300px"><input id="txtNamaEmail5" class="form-control" name='txtNamaEmail5' type="text" size="20" ></td>					
					<td width="80px" class="lbl">Email </td>
					<td width="400px"><input id="txtAlamatEmail5" class="form-control" name='txtAlamatEmail5' type="text" size="20" ></td>						
					</tr>					
				</tbody>
				</table>
				</div>
				
				
				<br/>
				<div class="row">
					<div align="right">
						<button id="btnSubmit" type="button" class="btn btn-primary">Simpan</button>
						<button id="btnCancel" type="button" class="btn ">Batal</button>
					</div>
					<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i>Processing</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
	

</html>
