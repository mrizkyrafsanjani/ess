<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
 
<?php 
foreach($body->css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<?php foreach($body->js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<script>

	$(document).ready(function(){
		<?php if($this->session->flashdata('msg')){ ?>
		alert('<?php echo $this->session->flashdata('msg'); ?>');
		<?php } ?>
	});
	
	function generateCuti()
	{
		var confirm = prompt("Tolong ketik KONFIRMASI untuk melanjutkan proses", "Konfirmasi");
		if(confirm === "KONFIRMASI"){
			var cmbTahun = $('#cmbTahun').val();
			$.post( 			
				'<?php echo $this->config->base_url(); ?>index.php/Cuti/CutiController/ajax_prosesGenerateCuti',
				 { tahun: cmbTahun },
				 function(response) {
					alert(response);
					window.location.replace('<?php echo site_url('Cuti/TempMasterCutiKaryawan'); ?>');
				 }
			);
		}else{
			alert('INPUT YANG ANDA MASUKKAN SALAH! PROSES TIDAK AKAN BERJALAN!');
		}
	}
</script>

</head>
<body>
<!-- Beginning header -->

<!-- End of header-->
    <div style='height:20px;'></div>  
    <div>
        <?php echo $body->output; ?>
    </div>
	<div>
		<fieldset>
			<legend>Generate Cuti All Karyawan</legend>
			Tahun 
			<select class="form-control" name="cmbTahun" id="cmbTahun">
				<?php 
					for($i=-1;$i<2;$i++)
					{
						$tahun = date('Y') + $i;
						echo "<option value='$tahun'>$tahun</option>";
					}
				?>
				
			</select>
			<input class="btn btn-primary" type="button" onClick="generateCuti()" id="btnGenerate" name="" value="Generate Cuti All Karyawan">
		</fieldset>
	</div>
<!-- Beginning footer -->
<div></div>
<!-- End of Footer -->
</body>
</html>