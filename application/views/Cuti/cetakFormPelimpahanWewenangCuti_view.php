<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetakFormCuti.css";
	</style>

	<style>
	@page { size 21cm 29.7cm; margin: 2cm }
	div.page { page-break-after: always }
	</style>

	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript">

		var afterPrint = function () {
            window.history.back();
        };

		function printDiv(divName) {
			 var printContents = document.getElementById(divName).innerHTML;
			 var originalContents = document.body.innerHTML;
			 document.body.innerHTML = printContents;
			 window.print();
			 document.body.innerHTML = originalContents;
			 window.onafterprint = afterPrint;
		}

		$(function() {

			$('.buttonSubmit').click(function (){
				$('#printArea').printElement({
					//leaveOpen: true,
					pageTitle:'Form Cuti <?php echo $dataUserYangCuti['Nama'];  ?>',
					printMode:'popup',
					overrideElementCSS:[
						'<?php echo $this->config->base_url(); ?>assets/css/cetak.css','<?php echo $this->config->base_url(); ?>assets/css/style-common.css']
				});
			});



		});



	</script>
	<head>
		<title>Form Cuti</title>
	</head>
	<body onload="printDiv('printArea')">
	<br/>
	<div id='printArea'>
	<?php  if ($peruntukan == 'Head') { ?>
		<div class="page" >
	<?php  } else { ?>
		<div class="page"  style="display: none;" >
	<?php  }  ?>
		<table border=0>
			<tbody>
			<!-- Results table headers -->
			<tr class="tblHeader">
				<td colspan="1"><image id="header" height="100" width="920" src='<?php echo $this->config->base_url(); ?>assets/images/header_dpa.jpg'><br/>
			</tr>

			</tbody>
		</table>
		<br>
		<B>Kepada : HRGA </B>
		<div class=perihal>
			<p><B>Perihal : Permohonan Cuti</B></p>
		</div>
		Dengan hormat,
		<br><br>
		Mohon dapat dicatat cuti/ijin meninggalkan pekerjaan kami,
		<br><br>

		<table class="aTable">
		<tr><td>Nama</td><td>:</td><td><b><u><?php echo $dataUserYangCuti['Nama']; ?></b></u></td><td>Dept</td><td>:</td><td><u><b><?php echo $dataUserYangCuti['NamaDepartemen']; ?></b></u></td></tr>
		<tr><td>NPK</td><td>:</td><td><b><u><?php echo $dataUserYangCuti['NPK']; ?></b></u></td><td colspan="3"></td></tr>
		</table>




		Sebanyak <u><?php echo $jumlahHariGlobal; ?></u> hari kerja,
		yaitu pada tanggal <u><?php echo $TanggalMulai;?> </u> sampai dengan <u><?php echo $TanggalSelesai; ?></u>
		<br>
		Cuti/izin meninggalkan pekerjaan ini kami ambil dari :
		<br><br>



		<table width="500" border="1" >
			<tr >
				<th   colspan="4" scope="col">Cuti</th>
				<th  colspan="4" scope="col">Ijin </th>
			</tr>
			<tr >
				<tr>
					<td width="100">1. Tahunan </td>
				<td>:</td>
				<td width="250">
				<?php
					$jumlahcuti = 0;
					for($i=0;$i<count($datacutiuserdetail);$i++){
						if ( $datacutiuserdetail[$i]['KodeJenisTidakHadir']=='1')
							$jumlahcuti += $datacutiuserdetail[$i]['jumlahHariDetail'];
					}
					echo $jumlahcuti == 0 ? "" : $jumlahcuti;
				?>
				</td>
				<td class="borderedright">Hari Kerja</td>
					<td width="250">1. Pernikahan </td>
				<td>:</td>
				<td width="100"> <?php for($i=0;$i<count($datacutiuserdetail);$i++){
										if ( $datacutiuserdetail[$i]['KodeJenisTidakHadir']=='3')
											echo $datacutiuserdetail[$i]['jumlahHariDetail'];
										else
											echo "";  }?> </td>
				<td >Hari Kerja</td>
				</tr>
			<tr>
					<td width="100">2. Besar </td>
				<td>:</td>
				<td width="250">
				<?php
					$jumlahcuti = 0;
					for($i=0;$i<count($datacutiuserdetail);$i++){
					if ( $datacutiuserdetail[$i]['KodeJenisTidakHadir']=='2')
						$jumlahcuti += $datacutiuserdetail[$i]['jumlahHariDetail'];
					}
					echo $jumlahcuti == 0 ? "" : $jumlahcuti;
				?>
				</td>
				<td class="borderedright">Hari Kerja</td>
					<td width="250">2. Pasangan melahirkan/keguguran  </td>
				<td>:</td>
				<td width="100"> <?php for($i=0;$i<count($datacutiuserdetail);$i++){
										if ( $datacutiuserdetail[$i]['KodeJenisTidakHadir']=='4')
											echo $datacutiuserdetail[$i]['jumlahHariDetail'];
										else
											echo "";  }?></td>
				<td >Hari Kerja</td>
				</tr>
			<tr>
					<td width="100">3. Bersalin</td>
				<td>:</td>
				<td  width="250"> <?php for($i=0;$i<count($datacutiuserdetail);$i++){
										if ( $datacutiuserdetail[$i]['KodeJenisTidakHadir']=='9')
											echo $datacutiuserdetail[$i]['jumlahHariDetail'];
										else
											echo "";  }?></td>
				<td class="borderedright">Hari Kerja</td>
					<td width="250">3. Kematian </td>
				<td>:</td>
				<td width="100"><?php for($i=0;$i<count($datacutiuserdetail);$i++){
										if ( $datacutiuserdetail[$i]['KodeJenisTidakHadir']=='5')
											echo $datacutiuserdetail[$i]['jumlahHariDetail'];
										else
											echo "";  }?></td>
				<td >Hari Kerja</td>
				</tr>
			<tr>
					<td  colspan="4" class="borderedright"></td>
					<td width="150">4. Khitanan/Pembabtisan </td>
				<td>:</td>
				<td width="100"> <?php for($i=0;$i<count($datacutiuserdetail);$i++){
										if ( $datacutiuserdetail[$i]['KodeJenisTidakHadir']=='6')
											echo $datacutiuserdetail[$i]['jumlahHariDetail'];
										else
											echo "";  }?> </td>
				<td >Hari Kerja</td>
				</tr>
				<tr>
					<td  colspan="4" class="borderedright"></td>
					<td width="250">5. Ujian Keserjanaan </td>
				<td>:</td>
				<td width="100">  <?php for($i=0;$i<count($datacutiuserdetail);$i++){
										if ( $datacutiuserdetail[$i]['KodeJenisTidakHadir']=='7')
											echo $datacutiuserdetail[$i]['jumlahHariDetail'];
										else
											echo "";  }?> </td>
				<td > Hari Kerja</td>
				</tr>
				<tr>
					<td class="borderedright" colspan="4"></td>
					<td width="250">6. Wisuda </td>
				<td>:</td>
				<td width="100"> <?php for($i=0;$i<count($datacutiuserdetail);$i++){
										if ( $datacutiuserdetail[$i]['KodeJenisTidakHadir']=='8')
											echo $datacutiuserdetail[$i]['jumlahHariDetail'];
										else
											echo "";  }?> </td>
				<td > Hari Kerja</td>
				</tr>
			</tr>
		</table>


		<br/>
		Adapun, sisa cuti kami sampai saat ini adalah :
		<br>
		<table class="aTable">
		<tr><td>Sisa Cuti Tahunan</td><td>:</td><td><u><?php echo $sisacutitahunan ;?></u></td></tr>
		<tr><td>Sisa Cuti Besar</td><td>:</td><td><u><?php echo $sisacutibesar; ?></u></td></tr>
		</table>
		<br>
		Atas perhatian dan bantuannya kami ucapkan terima kasih.
		<br>

		<br/>
		<!-- tanda tangan -->
		<table border="0">
			<tbody>

			<tr class="ttd">
					<td ></td>
				<td ></td>
					<td >Jakarta, <?php $tgl=date('d-m-Y'); echo $tgl;?></td>
				</tr>
			<tr class="ttd">
					<td >Menyetujui</td>
				<td ></td>
					<td >Pemohon</td>
				</tr>

				<tr class="ttd">
					<td><br/><br/><br/><br/><br/></td>
					<td></td>
					<td></td>
				</tr>
				<tr class="ttd">
					<td >( <?php echo $dataAtasan['Atasan']; ?> )</td>
					<td></td>
				<td >( <?php echo $dataUserYangCuti['Nama']; ?> )</td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
		if($flagemail=='1' or $flagemail=='2'  ){
	?>
	<BR><BR>
		<div class="page"> <!-- buat pelimpahan wewenenang kalau ada-->
			<table border=0>
				<tbody>
				<!-- Results table headers -->
				<tr class="tblHeader">
					<td colspan="1"><image id="header" height="100" width="920" src='<?php echo $this->config->base_url(); ?>assets/images/header_dpa.jpg'><br/>
				</tr>

				</tbody>
			</table>
			<br>
			<div class=perihal>
				<p><B>Pelimpahan Wewenang</B><BR>
				<B><?php echo $KodePelimpahanWewenang ;?></B></p>
			</div>
			<BR><BR>
				Sehubungan dengan pengajuan izin/cuti saya selama <u><?php echo $jumlahHariGlobal; ?></u> hari,terhitung sejak tanggal <u><?php echo $TanggalMulai;?> </u> sampai dengan <u><?php echo $TanggalSelesai; ?></u> <br>
				sebagaimana dibuktikan dengan formulir izin/cuti terlampir, maka wewenang atas : <br>
					<?php for($i=0;$i<count($dataPelimpahanWewenangDetail);$i++){
							echo $dataPelimpahanWewenangDetail[$i]['Keterangan'] .'<br />' ;
							echo 'Akan dilimpahkan kepada : '. $dataPelimpahanWewenangDetail[$i]['NamaYangDilimpahkan'] . '<br /><br />' ;
					}?>


				<?php
				if($flagemail=='2'){
				?>
						Selama saya izin/cuti, mohon disediakan fitur balas otomatis pada email saya, dengan format :<br>
						<i>
						Thank you for your email.<br>
						I am leave till <u><?php echo $TanggalSelesai; ?></u>. I will read and reply the email when return to office. <br>
						For urgent matter, please contact
						<?php for($i=0;$i<count($dataEmailPelimpahanWewenangDetail);$i++){
							if ( $i==count($dataEmailPelimpahanWewenangDetail)-1)
								echo $dataEmailPelimpahanWewenangDetail[$i]['Nama'] . ' at ' . $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju'] . '.';
							else
								echo $dataEmailPelimpahanWewenangDetail[$i]['Nama'] . ' at ' . $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju'] . ' or ';
							}
						?> <br>
						Thanks and Regards <br>
						<?php echo $dataUserYangCuti['Nama']; ?>
						</i> <br><br>

						Selama saya izin/cuti, untuk semua email yang masuk pada email saya, dialihkan kepada : <br>
						<?php for($i=0;$i<count($dataEmailPelimpahanWewenangDetail);$i++){
							if ( $i==count($dataEmailPelimpahanWewenangDetail)-1)
								echo  $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju']  ;
							else
							echo  $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju'] . ','  ;
						}
						?>
				<?php
				}
				?>

				<BR><BR>
				<!-- tanda tangan -->
				<table border="1">
					<tr class="ttdPW">
					<td rowspan="2">Dibuat </td>
					<td >Diketahui</td>
					<td >Disetujui</td>
					</tr>
					<tr class="ttdPW">
					<td>Penerima Pelimpahan Wewenang</td>
					<td>Atasan</td>
					</tr>
					<tr class="ttdPW">
					<td><br/><br/><br/><br/><br/></td>
					<td></td>
					<td></td>
					</tr>
					<tr class="ttdPW">
					<td><?php echo $dataUserYangCuti['Nama']; ?></td>
					<td>
					<?php for($i=0;$i<count($dataPelimpahanWewenangDetail);$i++){
						if ( $i==count($dataPelimpahanWewenangDetail)-1)
							echo $dataPelimpahanWewenangDetail[$i]['NamaYangDilimpahkan']  ;
						else
							echo $dataPelimpahanWewenangDetail[$i]['NamaYangDilimpahkan'] . ' , ' ;
					}?>

					</td>
					<td><?php echo $dataAtasan['Atasan']; ?></td>
					</tr>
				</table>

				<BR><BR>
				*Mohon untuk dilampirkan formulir URF terkait pelimpahan wewenang ini (jika diperlukan).

		</div>
	</div>
	<?php
		}
	?>
	</body>
</html>