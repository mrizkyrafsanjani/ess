<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		
		function btnApprove_click(){
			if(confirm('Apakah Anda yakin ingin approval data ini?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cuti/cutiController/ProsesApprovalMasterCutiUser/<?php echo $databaru['KodeTempMasterCutiUser']; ?>/AP/<?php echo $databaru['TempNPK']; ?>/<?php echo $databaru['tahun']; ?>',
					{},
					function(response){
						alert(response);
						window.location.replace('<?php echo $this->config->base_url(); ?>index.php/UserTask');
					}
				);
			}
		}
		
		function btnDecline_click(){
			if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
			{
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cuti/cutiController/ProsesApprovalMasterCutiUser/<?php echo $databaru['KodeTempMasterCutiUser']; ?>/DE/<?php echo $databaru['TempNPK']; ?>/<?php echo $databaru['tahun']; ?>',
					{},
					function(response){
						alert(response);
						window.location.replace('<?php echo $this->config->base_url(); ?>index.php/UserTask');
					}
				);
			}
		}
		
		
	</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						Nama Karyawan :
					</div>
					<div class="col-md-6">
						<?php echo $databaru['nama']; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Tahun 
					</div>
					<div class="col-md-6">
						<?php echo $databaru['tahun']; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Jumlah Cuti Tahunan Awal :
					</div>
					<div class="col-md-6">
						<?php echo $databaru['JumlahCutiTahunanAwal']; ?>
					</div>
				</div>
				
				<br/>
				<div class="row">
					<div class="col-md-3">
						Jumlah Cuti Besar Awal :
					</div>
					<div class="col-md-6">
						<?php echo $databaru['JumlahCutiBesarAwal']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Akumulasi Cuti Tahunan <br>
						(Data Lama):
					</div>
					<div class="col-md-2">
						<?php echo $datalama['AkumulasiCutiTahunan']; ?>
					</div>
					
				</div>
				<div class="row">
					<div class="col-md-3">
						Akumulasi Cuti Tahunan <br> 
						(Data Baru):
					</div>
					<div class="col-md-2">
						<?php echo $databaru['AkumulasiCutiTahunan']; ?>
					</div>
					
				</div>
				<br/>				
				<div class="row">
					<div class="col-md-3">
						Akumulasi Cuti Besar <br>
						(Data Lama):
					</div>
					<div class="col-md-3">
						<?php echo $datalama['AkumulasiCutiBesar']; ?> 
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						Akumulasi Cuti Besar <br>
						(Data Baru):
					</div>
					<div class="col-md-2">
						<?php echo $databaru['AkumulasiCutiBesar']; ?> 
					</div>
				</div>
				<br/>
				
				<div class="row">
				
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						History Catatan :
					</div>
					<div class="col-md-9">
						
					</div>
				</div>
				<br/>
				<div class="row">
					<div align="right">
						
						<input id="btnBatal" class="btn btn-primary" type="button" value="Approve" onClick="btnApprove_click()">
						<input id="btnBatal" class="btn" type="button" value="Decline" onClick="btnDecline_click()">
		
						
					</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
</html>
