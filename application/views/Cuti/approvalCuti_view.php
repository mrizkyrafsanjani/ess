<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	function alertsizeCutiJenisTidakHadir(pixels){
		pixels+=32;
		document.getElementById('iFrameCutiJenisTidakHadir').style.height=pixels+"px";
	}
	$(function() {
		$("#btnDecline").click(function(){
			var txtCatatan = $('#txtCatatan');
			
			if(txtCatatan.val() === '')
			{
				alert('Catatan harus diisi!');
			}
			else
			{
			
				$.post(
					<?php if($realisasi == '1'){ ?>
					'<?php echo $this->config->base_url(); ?>index.php/Cuti/CutiController/ajax_prosesApprovalRealisasi',
					<?php }else{ ?>
					'<?php echo $this->config->base_url(); ?>index.php/Cuti/CutiController/ajax_prosesApproval',
					<?php } ?>
					 { kodeusertask: '<?php echo $kodeusertask; ?>', status: 'DE', catatan: txtCatatan.val() },
					 function(response) {
						alert(response);
						/*if (response == 1) {
						   alert('Decline berhasil');						   
						}else{
						   alert('Sistem gagal.');
						}*/
						window.location.replace('<?php echo site_url('UserTask'); ?>');
					 }
				);
			
			}
		});
		
		$("#btnApprove").click(function(){
			var txtCatatan = $('#txtCatatan');
			$.post( 
				<?php if($realisasi == '1'){ ?>
				'<?php echo $this->config->base_url(); ?>index.php/Cuti/CutiController/ajax_prosesApprovalRealisasi',
				<?php }else{ ?>
				'<?php echo $this->config->base_url(); ?>index.php/Cuti/CutiController/ajax_prosesApproval',
				<?php } ?>
				 { kodeusertask: '<?php echo $kodeusertask; ?>', status: 'AP',catatan: txtCatatan.val() },
				 function(response) {
					alert(response);
					/*
					if (response == 1) {
					   alert('Approve berhasil');						   
					}else{
					   alert('Sistem gagal.');
					}
					*/
					window.location.replace('<?php echo site_url('UserTask'); ?>');
				 }
			);
		});
	});
	</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						Nama Karyawan :
					</div>
					<div class="col-md-6">
						<?php echo $nama; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Sisa Cuti Tahunan 
					</div>
					<div class="col-md-6">
						<?php echo $sisacutitahunan; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Sisa Cuti Besar 
					</div>
					<div class="col-md-6">
						<?php echo $sisacutibesar; ?>
					</div>
				</div>
				<!--
				<br/>
				<div class="row">
					<div class="col-md-3">
						Pada Tanggal :
					</div>
					<div class="col-md-6">
						<?php //echo $tanggalmulai; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Sampai Tanggal :
					</div>
					<div class="col-md-2">
						<?php //echo $tanggalselesai; ?>
					</div>
					
				</div>
				<br/>				
				<div class="row">
					<div class="col-md-3">
						Sebanyak :
					</div>
					<div class="col-md-6">
						<?php //echo $jumlahhari; ?> Hari Kerja
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Jenis Ketidakhadiran :
					</div>
					<div class="col-md-6">
						<?php //echo $jenistidakhadir; ?>
					</div>
				</div>
				<br/>
				-->
				<iframe id="iFrameCutiJenisTidakHadir" src="<?php echo $this->config->base_url(); ?>index.php/Cuti/cutiJenisTidakHadir/index/<?php echo $kodeusertask;?>/approve" width="100%" height="200px" seamless="seamless">
					<p>Your browser does not support iframes.</p>
				</iframe>
				<div class="row">
				<?php if($realisasi == '0'){ ?>
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				<?php } ?>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						History Catatan :
					</div>
					<div class="col-md-9">
						<?php echo $historycatatan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div align="right">
						<button id="btnApprove" type="button" class="btn btn-primary">Approve</button>
						<button id="btnDecline" type="button" class="btn ">Decline</button>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
</html>
