<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<style>
	table {
		border-collapse: collapse;
  		width: 100%;
	}
	</style>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
	<?php //echo $KodeRequestMOM_array[0]['NoTransaksi']; ?>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Edit MOM PROMISE Request </h3>
			</div>
			<br/>
				<?php 
				if(count($MOM_array)<>''){ ?>
				<div class="row">
				<div class="col-md-3">
						MOM:
					</div>
					<div class="col-md-6">
				<table border="1">
								<tr>
								<th>Judul</th>
								<th>Deskripsi</th>
								<th>Dibuat Pada</th>   
								<th>Dibuat Oleh</th>  
								<th>Action</th>      
								</tr>
								
				<?php 
					
                        for($i=0;$i<count($MOM_array);$i++){
							if($MOM_array[$i]['Updates']<>0){
								$Action = "Edit";						
								}
							if($MOM_array[$i]['Deletes']<>0){
								$Action = "Delete";						
								}
							if($Action=="Delete"){
                            echo "<tr><td>".$MOM_array[$i]['Judul'] ." </td>
                                    <td>". $MOM_array[$i]['Deskripsi'] ."</td>
                                    <td>". $MOM_array[$i]['CreatedOn'] ."</td>   
									<td>". $MOM_array[$i]['CreatedBy'] ."</td>
									<td>". $Action ."</td>

									</tr> ";    
							}else{
								echo "<tr><td>".$MOM_array[$i]['KoreksiJudul'] ." </td>
                                    <td>". $MOM_array[$i]['KoreksiDeskripsi'] ."</td>
                                    <td>". $MOM_array[$i]['CreatedOn'] ."</td>   
									<td>". $MOM_array[$i]['CreatedBy'] ."</td>
									<td>". $Action ."</td>
									</tr> ";   
							}   
						
						}
				
					echo "</table>";
                ?>   
				</div>
				</div>
				<?php } ?>
				<br/>
				<div class="row">
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				</div>
				<br/>
				<div class="row">
					<div align="right">
						<button id="btnApprove" type="button"  onClick="btnApprove_click()" class="btn btn-primary">Approve</button>
						<button id="btnDecline" type="button" onClick="btnDecline_click()" class="btn ">Decline</button>
						
					</div>
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
	
	function btnApprove_click(){
			var txtCatatan = $('#txtCatatan');
			if(confirm('Apakah Anda yakin ingin approval data ini?')==true)
			{
			
				$.post("<?php echo $this->config->base_url(); ?>index.php/PROMISE/PROMISEController/ajax_prosesApprovalMOMPromise",
					{
					kodeusertask: "<?php echo $KodeRequestMOM_array[0]['KodeUserTask']; ?>",
					koderequestmom: "<?php echo $KodeRequestMOM_array[0]['NoTransaksi']; ?>",
					status: "AP",
					catatan: txtCatatan.val(),
					action: "<?php echo $Action; ?>",
					projectID: "<?php echo $MOM_array[0]['ProjectID'];?>",
					},
					function(response){
					alert(response);
					window.location.replace('<?php echo site_url('UserTask'); ?>');
					});

			}
		}
	
	function btnDecline_click(){
			var txtCatatan = $('#txtCatatan');
	
			if(txtCatatan.val() == '')
			{
				alert('Catatan harus diisi!');
			}
			else
			{
				if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
				{
					$.post("<?php echo $this->config->base_url(); ?>index.php/PROMISE/PROMISEController/ajax_prosesApprovalMOMPromise",
					{
					kodeusertask: "<?php echo $KodeRequestMOM_array[0]['KodeUserTask']; ?>",
					koderequestdetail: "<?php echo $KodeRequestMOM_array[0]['NoTransaksi']; ?>",
					status: "DE",
					catatan: txtCatatan.val(),
					action: "<?php echo $Action; ?>",
					projectID: "<?php echo $MOM_array[0]['ProjectID'];?>",
					},
					function(response){
					alert(response);
					window.location.replace('<?php echo site_url('UserTask'); ?>');
					});
				}
			}
		}
	
	
	
	
	
	</script>
</html>
