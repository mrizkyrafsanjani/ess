<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
    <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
    
    <script type="text/javascript">
        function SubmitUploadFinalPROMISE(ID,KodeUserTask,KodeRequestDetail) {

            var _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf"];
            var flag = true;
            var arrInputs = UploadFinalPROMISE.getElementsByTagName("input");
            var countFile = 0;
            for (var i = 0; i < arrInputs.length; i++) {
                var HtmlInput = arrInputs[i];
                if (HtmlInput.type == "file") {
                    var sFileName = HtmlInput.value;
                    if (sFileName.length > 0) {
                        countFile++;
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                            flag = false;
                        }
                    }
                }
            }
            if(countFile==0){
                alert("Tidak ada file yang diupload?");
                flag = false;
            }
            if(flag){
                var x = window.confirm("Upload Dokumen Progress?");
                if(x){
                    $("#UploadFinalPROMISE").attr("action"); //will retrieve it
                    $("#UploadFinalPROMISE").attr("target", "_self"); //will retrieve it
                var y = $("#UploadFinalPROMISE").attr("action", "<?php echo site_url('PROMISE/PROMISEController/UploadFinalPROMISE');?>/"+ID);
                    if(y){
                        alert("Your File Was Successfully Uploaded");
                    }
                }else{
                    event.preventDefault();
                }
            }else{
                event.preventDefault();
            }
        }
        function BatalUploadFinalPROMISE(){
            var x = window.confirm("Batalkan Upload Scan Dokumen Persetujuan Progress ini?");
            if(x){
                $("#UploadFinalPROMISE").attr("action"); //will retrieve it
                $("#UploadFinalPROMISE").attr("target", "_self"); //will retrieve it
            
                $("#UploadFinalPROMISE").attr("action", "<?php echo site_url('PROMISE/PROMISEController/ViewListPROMISEHead');?>");
            }else{
                event.preventDefault();
            }
        }
    </script>
    <style>
    #error{
        color: red;
    }
    </style>
	<head>
		<title>Upload Scan Dokumen</title>
	</head>
    <body>
        <?php echo $this->session->flashdata('message'); ?>
        
        <?php if(isset($error)) VAR_DUMP($error);?>
        <div class="container" style="width:80%">
            <h2>Upload Scan Dokumen Progress <?php echo $data_array['NamaProject'];?></h2>
            <form method="POST" name="UploaPROMISE" id="UploadFinalPROMISE" enctype="multipart/form-data">
                <div class="form-group">
                    <div class = "row">
                        <div class ="col">
                            <label for="uploadScanPROMISE">Upload Scan Dokumen</label>
                            <input type="file" class="form-control form-control-file" id="uploadScanPROMISE" name="uploadScanPROMISE">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col"><br/>
                            <input type="submit" class="btn btn-success" onclick = "SubmitUploadFinalPROMISE('<?php echo $data_array['ProjectID']?>/<?php echo $KodeRequestDetail_array[0]['KodeUserTask']?>/<?php echo $KodeRequestDetail_array[0]['NoTransaksi']?>')" value="Upload">
                            <input type="submit" class="btn btn-danger" onclick = "BatalUploadFinalPROMISE()" value="Batal Upload">
                        </div>
                    </div>
                </div>
            </form>
            <input type="hidden" id="kodeUsertask" name="kodeUsertask" value="<?php echo $KodeRequestDetail_array[0]['KodeUserTask']; ?>">
            <input type="hidden" id="kodeRequestDetail" name="kodeRequestDetail" value="<?php echo $KodeRequestDetail_array[0]['NoTransaksi']; ?>">
            
            <?php if (!empty($data_array['FilePath'])): ?>
            <div class="row">
                <div class="col-sm-6">
                    <h4><?php echo $data_array['FilePath']; ?></h4>
                </div>
                <div class="col-sm-6">
                    <h5><a href="<?php echo $this->config->base_url().'assets/uploads/files/PROMISE/'. $data_array['FilePath']; ?>" download>download</a></h5>
                </div>
            </div>
            <div class="row">    
                <div class="col-md-12"> 
                
                
                    <iframe src="<?php echo $this->config->base_url().'assets/uploads/files/PROMISE/'. $data_array['FilePath']; ?>" width="90%" height="720px">
                    This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->config->base_url().'assets/uploads/files/PROMISE/'. $data_array['FilePath']; ?>">Download</a>
                    </iframe>   
                </div>
            </div>
            <?php endif; ?>
        </div>
        
	</body>
</html> 