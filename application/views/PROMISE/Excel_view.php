<html>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">	
<head>
    <title>Import Dokumen Excel</title>
    <style>
		td {padding:5px 5px 5px 5px;}		
    </style>
</head>
<body>	

    <div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Import Dokumen Excel</h3>			
			</div>
			<div class="panel-body">
				<div class="col-md-6">
						<div class="box-header">
						<h3 class="box-title">Import Dokumen Excel</h3>
						</div>
						<div class="box-body">
                                <form action="../PROMISE/excel/upload/" method="post" enctype="multipart/form-data">
			
									<input class="form-control" type="file" name="file" accept=".xlsx,.xls,.csv"/>
                                    <input type="submit" class="btn btn-info btn-flat" value="Import"/>
					
                                </form>
								</div>
							</div>
						</div>
						</div>
						<!-- /.box-body -->
						<!-- Loading (remove the following to stop the loading)-->
</div>
</body>
</form>
</div>