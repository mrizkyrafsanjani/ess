<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
    <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
    
    <script type="text/javascript">
        function SubmitUploadPROMISE(ID,Status) {
            <?php 
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadHRGA'));
            foreach($query->result() as $row)
            {
                $headHRGA = $row->Value;
            }
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadMRC'));
            foreach($query->result() as $row)
            {
                $headMRC = $row->Value;
            }
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
            foreach($query->result() as $row)
            {
                $headIT = $row->Value;
            }
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadFinInv'));
            foreach($query->result() as $row)
            {
                $headFIINV = $row->Value;
            }
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadACT'));
            foreach($query->result() as $row)
            {
                $headACC = $row->Value;
            }
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadCA'));
            foreach($query->result() as $row)
            {
                $headCA = $row->Value;
            }
            $PresDirDPA1 =$this->user->getSingleUserBasedJabatan("President Director DPA 1")->NPK;
            $PresDirDPA2 =$this->user->getSingleUserBasedJabatan("President Director DPA 2")->NPK;
            $DirDPA1 =$this->user->getSingleUserBasedJabatan("Director DPA 1")->NPK;
            $DirDPA2 =$this->user->getSingleUserBasedJabatan("Director DPA 2")->NPK;
            $CorpSec =$this->user->getSingleUserBasedJabatan("Corporate Secretary Leader")->NPK;
            $JenisUser = '';
            if($this->npkLogin==$CorpSec || $this->npkLogin==$PresDirDPA1 || $this->npkLogin==$PresDirDPA2 || $this->npkLogin==$DirDPA1 || $this->npkLogin==$DirDPA2){
                $JenisUser = 'Admin';
            }else if($this->npkLogin==$headHRGA || $this->npkLogin==$headACC || $this->npkLogin==$headCA || $this->npkLogin==$headFIINV || $this->npkLogin==$headIT || $this->npkLogin==$headMRC){
                $JenisUser = 'Head';
            }else{
                $JenisUser = 'User';
            }
            ?>
            var JenisUser="<?php echo $JenisUser ?>";
            var _validFileExtensions = [".jpg", ".jpeg", ".png", ".pdf"];
            var flag = true;
            var arrInputs = UploadPROMISE.getElementsByTagName("input");
            var countFile = 0;
            for (var i = 0; i < arrInputs.length; i++) {
                var HtmlInput = arrInputs[i];
                if (HtmlInput.type == "file") {
                    var sFileName = HtmlInput.value;
                    if (sFileName.length > 0) {
                        countFile++;
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }
                        if (!blnValid) {
                            alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                            flag = false;
                        }
                    }
                }
            }
            if(countFile==0){
                alert("Tidak ada file yang diupload?");
                flag = false;
            }
            if(flag){
                var x = window.confirm("Upload Dokumen Progress?");
                if(x){
                    $("#UploadPROMISE").attr("action"); //will retrieve it
                    $("#UploadPROMISE").attr("target", "_self"); //will retrieve it
                    var y = $("#UploadPROMISE").attr("action", "<?php echo site_url('PROMISE/PROMISEController/UploadPROMISE');?>/"+JenisUser+"/"+ID+"/"+Status);
                    if(y){
                        alert("Your File Was Successfully Uploaded");
                    }
                }else{
                    event.preventDefault();
                }
            }else{
                event.preventDefault();
            }
        }
        function BatalUploadPROMISE(){
            
            <?php 
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadHRGA'));
            foreach($query->result() as $row)
            {
                $headHRGA = $row->Value;
            }
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadMRC'));
            foreach($query->result() as $row)
            {
                $headMRC = $row->Value;
            }
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
            foreach($query->result() as $row)
            {
                $headIT = $row->Value;
            }
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadFinInv'));
            foreach($query->result() as $row)
            {
                $headFIINV = $row->Value;
            }
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadACT'));
            foreach($query->result() as $row)
            {
                $headACC = $row->Value;
            }
            $query = $this->db->get_where('globalparam',array('Name'=>'HeadCA'));
            foreach($query->result() as $row)
            {
                $headCA = $row->Value;
            }
            $PresDirDPA1 =$this->user->getSingleUserBasedJabatan("President Director DPA 1")->NPK;
            $PresDirDPA2 =$this->user->getSingleUserBasedJabatan("President Director DPA 2")->NPK;
            $DirDPA1 =$this->user->getSingleUserBasedJabatan("Director DPA 1")->NPK;
            $DirDPA2 =$this->user->getSingleUserBasedJabatan("Director DPA 2")->NPK;
            $CorpSec =$this->user->getSingleUserBasedJabatan("Corporate Secretary Leader")->NPK;
            $JenisUser = '';
            if($this->npkLogin==$CorpSec || $this->npkLogin==$PresDirDPA1 || $this->npkLogin==$PresDirDPA2 || $this->npkLogin==$DirDPA1 || $this->npkLogin==$DirDPA2){
                $JenisUser = 'Admin';
            }else if($this->npkLogin==$headHRGA || $this->npkLogin==$headACC || $this->npkLogin==$headCA || $this->npkLogin==$headFIINV || $this->npkLogin==$headIT || $this->npkLogin==$headMRC){
                $JenisUser = 'Head';
            }else{
                $JenisUser = 'User';
            }
            ?>
            var JenisUser="<?php echo $JenisUser ?>";
            var x = window.confirm("Batalkan Upload Scan Dokumen Persetujuan Progress ini?");
            if(x){
                $("#UploadPROMISE").attr("action"); //will retrieve it
                $("#UploadPROMISE").attr("target", "_self"); //will retrieve it
                if(JenisUser=='Admin'){
                    $("#UploadPROMISE").attr("action", "<?php echo site_url('PROMISE/PROMISEController/ViewListPROMISEAdmin');?>");
                }else if(JenisUser=='Head'){
                    $("#UploadPROMISE").attr("action", "<?php echo site_url('PROMISE/PROMISEController/ViewListPROMISEHead');?>");
                }else{
                    $("#UploadPROMISE").attr("action", "<?php echo site_url('PROMISE/PROMISEController/ViewListPROMISEUser');?>");
                }
            }else{
                event.preventDefault();
            }
        }
    </script>
    <style>
    #error{
        color: red;
    }
    </style>
	<head>
		<title>Upload Scan Dokumen</title>
	</head>
    <body>
        <?php echo $this->session->flashdata('message'); ?>
        
        <?php if(isset($error)) VAR_DUMP($error);?>
        <?php foreach($data as $data){ ?>
        <div class="container" style="width:80%">
            <h2>Upload Scan Dokumen Progress <?php echo $data->Parameter?></h2>
            <form method="POST" name="UploaPROMISE" id="UploadPROMISE" enctype="multipart/form-data">
                <div class="form-group">
                    <div class = "row">
                        <div class ="col">
                            <label for="uploadScanPROMISE">Upload Scan Dokumen</label>
                            <input type="file" class="form-control form-control-file" id="uploadScanPROMISE" name="uploadScanPROMISE">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col"><br/>
                            <input type="submit" class="btn btn-success" onclick = "SubmitUploadPROMISE('<?php echo $data->ParameterID?>','<?php echo $data->Status?>')" value="Upload">
                            <input type="submit" class="btn btn-danger" onclick = "BatalUploadPROMISE()" value="Batal Upload">
                        </div>
                    </div>
                </div>
            </form>
            <?php if (!empty($data->FilePathParameter)): ?>
            <div class="row">
                <div class="col-sm-6">
                    <h4><?php echo $data->Parameter; ?></h4>
                </div>
                <div class="col-sm-6">
                    <h5><a href="<?php echo $this->config->base_url().'assets/uploads/files/PROMISE/'. $data->FilePathParameter; ?>" download>download</a></h5>
                </div>
            </div>
            <div class="row">    
                <div class="col-md-12"> 
                    <iframe src="<?php echo $this->config->base_url().'assets/uploads/files/PROMISE/'. $data->FilePathParameter; ?>" width="90%" height="720px">
                    This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->config->base_url().'assets/uploads/files/PROMISE/'. $data->FilePathParameter; ?>">Download</a>
                    </iframe>   
                </div>
            </div>
            <?php endif; ?>
        </div>
        <?php } ?>
        
	</body>
</html> 