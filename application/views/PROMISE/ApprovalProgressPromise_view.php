<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<style>
	table {
		border-collapse: collapse;
  		width: 100%;
	}
	</style>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Progress Approval</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						Nama Project:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['NamaProject']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Indikator Keberhasilan:
					</div>
					<div class="col-md-6">
						<?php echo $progress_array['Parameter']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Presentase:
					</div>
					<div class="col-md-6">
						<?php echo $progress_array['Presentase']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Deadline Indikator Keberhasilan:
					</div>
					<div class="col-md-6">
						<?php  echo $progress_array['DeadlinePara']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Dokumen:
					</div>
					<div class="col-sm-6">
                    <p><a href="<?php echo $this->config->base_url().'assets/uploads/files/PROMISE/'. $progress_array['FilePathParameter']; ?>" download><b><u>Unduh Dokumen</u></b></a></p>
                </div>
				</div>
				<br/>
				<div class="row">
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				</div>
				<br/>
				<div class="row">
					<div align="right">
						<button id="btnApprove" type="button"  onClick="btnApprove_click()" class="btn btn-primary">Approve</button>
						<button id="btnDecline" type="button" onClick="btnDecline_click()" class="btn ">Decline</button>
						
					</div>
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
	
	function btnApprove_click(){
			var txtCatatan = $('#txtCatatan');
			if(confirm('Apakah Anda yakin ingin approval data ini?')==true)
			{
			
				$.post("<?php echo $this->config->base_url(); ?>index.php/PROMISE/PROMISEController/ajax_prosesParameterApproval",
					{
					kodeusertask: "<?php echo $KodeRequestDetail_array[0]['KodeUserTask']; ?>",
					koderequestdetail: "<?php echo $KodeRequestDetail_array[0]['NoTransaksi']; ?>",
					status: "AP",
					catatan: txtCatatan.val(),
					projectID: "<?php echo $progress_array['ProjectID'];?>",
					},
					function(response){
					alert(response);
					window.location.replace('<?php echo site_url('UserTask'); ?>');
					});

			}
		}
	
	function btnDecline_click(){
			var txtCatatan = $('#txtCatatan');
	
			if(txtCatatan.val() == '')
			{
				alert('Catatan harus diisi!');
			}
			else
			{
				if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
				{
					$.post("<?php echo $this->config->base_url(); ?>index.php/PROMISE/PROMISEController/ajax_prosesParameterApproval",
					{
					kodeusertask: "<?php echo $KodeRequestDetail_array[0]['KodeUserTask']; ?>",
					koderequestdetail: "<?php echo $KodeRequestDetail_array[0]['NoTransaksi']; ?>",
					status: "DE",
					catatan: txtCatatan.val(),
					projectID: "<?php echo $progress_array['ProjectID'];?>",
					},
					function(response){
					alert(response);
					window.location.replace('<?php echo site_url('UserTask'); ?>');
					});
				}
			}
		}
	
	
	
	
	
	</script>
</html>
