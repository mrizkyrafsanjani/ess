<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	function btnApprove_click(){
			var txtCatatan = $('#txtCatatan');
			if(confirm('Apakah Anda yakin ingin approval data ini?')==true)
			{
			
				$.post("<?php echo $this->config->base_url(); ?>index.php/PROMISE/PROMISEController/ajax_prosesApprovalNewPromise",
					{
					kodeusertask: "<?php echo $KodeRequestDetail_array[0]['KodeUserTask']; ?>",
					koderequestdetail: "<?php echo $KodeRequestDetail_array[0]['NoTransaksi']; ?>",
					status: "AP",
					catatan: txtCatatan.val(),
					},
					function(response){
					alert(response);
					window.location.replace('<?php echo site_url('UserTask'); ?>');
					});

			}
		}
	
	function btnDecline_click(){
			var txtCatatan = $('#txtCatatan');
	
			if(txtCatatan.val() == '')
			{
				alert('Catatan harus diisi!');
			}
			else
			{
				if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
				{
					$.post("<?php echo $this->config->base_url(); ?>index.php/PROMISE/PROMISEController/ajax_prosesApprovalNewPromise",
					{
					kodeusertask: "<?php echo $KodeRequestDetail_array[0]['KodeUserTask']; ?>",
					koderequestdetail: "<?php echo $KodeRequestDetail_array[0]['NoTransaksi']; ?>",
					status: "DE",
					catatan: txtCatatan.val(),
					},
					function(response){
					alert(response);
					window.location.replace('<?php echo site_url('UserTask'); ?>');
					});
				}
			}
		}
	
	
	
	
	
	</script>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
	<?php //echo $KodeRequestDetail_array[0]['NoTransaksi']; ?>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Add New PROMISE Request </h3>
			</div>
			<div class="panel-body">
						<div class="row">
					<div class="col-md-3">
						Nama:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['NamaProject']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						KPI:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['KPI']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Target:
					</div>
					<div class="col-md-6">
						<?php  echo $data_array['Target']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Start Date:
					</div>
					<div class="col-md-6">
						<?php  echo $data_array['StartDate']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Deadline UREQ:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['DeadlineUREQ']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Deadline :
					</div>
					<div class="col-md-6">
						<?php echo $data_array['Deadline']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						PIC:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['PIC']; ?>
					</div>
				</div>
				<br/>
				<?php if($data_array['Budget']<>''){?>
				<div class="row">
					<div class="col-md-3">
						Budget:
					</div>
					<div class="col-md-6">
						<?php echo 'Rp ' .number_format($data_array['Budget'],2,',','.'); ?>
					</div>
				</div>
				<br/>
				<?php } ?>
				<div class="row">
					<div class="col-md-3">
						Departemen:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['Departemen']; ?>
					</div>
				</div>
				<br/>
				<?php if($data_array['Keterangan']<>''){?>
				<div class="row">
					<div class="col-md-3">
						Keterangan:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['Keterangan']; ?>
					</div>
				</div>
				<br/>
				<?php } ?>
				<div class="row">
					<div class="col-md-3">
						Dibuat Pada:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['CreatedOn']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				</div>
				<br/>
				<div class="row">
					<div align="right">
						<button id="btnApprove" type="button"  onClick="btnApprove_click()" class="btn btn-primary">Approve</button>
						<button id="btnDecline" type="button" onClick="btnDecline_click()" class="btn ">Decline</button>
						
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
