<html>
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	@import "../assets/css/jquery-ori.datepick.css";
	</style>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>/plugins/datepicker/bootstrap-datepicker.css">
		<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.tablesorter.js"></script> 
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.tablesorter.pager.js"></script> 
	
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
		<style>
		td {padding:10px 10px 10px 10px;}	
		input.size {height:40px;
				width:700px;}	
		</style>
		<style>
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  font-size:20px;
  background-color:#e1f1f2;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  font-size:20px;
}

input[type=submit]:hover {
  background-color: #45a049;
}


</style>
		<title>PROMISE Form</title>
	</head>
	
	<body>
	
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">PROMISE Form</h3>
		</div>

		<div class="panel-body">
			<?php echo validation_errors(); ?>

	<div class="form">
	<form action="submitPROMISEForm" method="post" accept-charset="utf-8">
    <table border=1 id="tbleditdetail">
	<tr>
	<input type="hidden" type="text" id="txtProjectID" name="txtProjectID" size="50%" value="<?php echo $LastProjectID_array[0]['ProjectID']+1;?>">
	<td colspan="2"><p style="font-size:20px; font-family:bold">Nama Project</p></td>
	<td colspan="4"><input class="size" type="text" id="txtNmProject" name="txtNmProject" size="50%" value=""></td>
	</tr>
	<tr>
	<td colspan="2"><p style="font-size:20px; font-family:bold">Key Performance Indicator</p></td>
	<td colspan="4"><input class="size" type="text" id="txtKPI" name="txtKPI" size="50%" value=""></td>
	</tr>
	<tr>
	<td colspan="2"><p style="font-size:20px; font-family:bold">Target</p></td>
	<td colspan="4"><input class="size" type="text" id="txtTarget" name="txtTarget" size="50%" value=""></td>
	</tr>
	<tr>
	<td colspan="2"><p style="font-size:20px; font-family:bold">Start Date</p></td>
	<td colspan="4"><div class="input-group date">
          <input type="text" class="size" id="txtStartDate" name='txtStartDate'>
      </div></td>
	</tr>
	<tr>
	<td colspan="2"><p style="font-size:20px; font-family:bold">Deadline UREQ</p></td>
	<td colspan="4"><div class="input-group date">
          <input type="text" class="size" id="txtDeadlineUREQ" name='txtDeadlineUREQ'>
      </div></td>
	</tr>
	</tr>
	<tr>
	<td colspan="2"><p style="font-size:20px; font-family:bold">Deadline</p></td>
	<td colspan="4"><input class="size" type="text" id="txtDeadline" name="txtDeadline" size="50%" value=""></td>
	</tr>
	<tr>
	<td colspan="2"><p style="font-size:20px; font-family:bold">Person In Charge</p></td>
	<td colspan="4"><input class="size" type="text" id="txtPIC" name="txtPIC" size="50%" value=""></td>
	</tr>
	<tr>
	<td colspan="2"><p style="font-size:20px; font-family:bold">Budget</p></td>
	<td colspan="4"><input class="size" type="number" id="txtBudget" name="txtBudget" size="50%" value=""></td>
	</tr>
	<tr>
	<td colspan="2"><p style="font-size:20px; font-family:bold">Departemen</p></td>
	<td colspan=3>
						<select class="form-control" id="txtDepartemen" name="txtDepartemen">
						<?php 
							for($i=0;$i<count($getDepartemen_array);$i++)
							{
								$selected = '';
								//if(date('n') == $i )
									$selected = 'selected';
									if($getDepartemen_array[$i]['NamaDepartemen']=='IT'){
										$getDepartemen_array[$i]['NamaDepartemen']="Information Technology";
									}
								echo "<option value='".$getDepartemen_array[$i]['NamaDepartemen']."' $selected>".$getDepartemen_array[$i]['NamaDepartemen']."</option>";
							}
							echo "<option value='' $selected></option>";
						?>
						</select>
					</td>
	</tr>
	<tr>
	<td colspan="2"><p style="font-size:20px; font-family:bold">Keterangan</p></td>
	<td colspan="4"><input class="size" type="text" id="txtKeterangan" name="txtKeterangan" size="50%" value=""></td>
	</tr>

    </table>

	<table class="tblPureNoBorder">
        <tr><td>
            <input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin menyimpan Data ini ke dalam database?')" id="btnSubmit" name="submitUbahPROMISE" value="Simpan">
            </td></tr>
    </table>
	<br/>
	</form>
</div>
	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<script>
          $(function(){
        //Date picker
              $('#txtStartDate').datepicker({
                  autoclose: true,
                  format: 'yyyy-mm-dd'
              });
							$('#txtDeadlineUREQ').datepicker({
                  autoclose: true,
                  format: 'yyyy-mm-dd'
              });
					});
	</script>
</html>