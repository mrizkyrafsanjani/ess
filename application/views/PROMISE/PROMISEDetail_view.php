<html>
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
		<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.tablesorter.js"></script> 
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.tablesorter.pager.js"></script> 
	<script type="text/javascript">
	$(function () {
		
	$("#tbllihatdetail").hide();
	
	function changeFormat(){}
	$("#LihatYa").click(function () {
		$("#tbllihatdetail").show(1000);

	});
	$("#LihatTidak").click(function () {
		$("#tbllihatdetail").hide(1000);
		
	});
	
	function search(){
			var ProjectID = <?php echo $ProjectDetail_array['ProjectID'];?>;
			var JenisUser = "<?php echo $User; ?>";
			var Page = "<?php echo $PageOrder; ?>";
			var iframe1 = document.getElementById('iFrameListPROMISEAdmin');	
			var iframe2 = document.getElementById('iFrameListPROMISEMOMAdmin');	
			if(Page == "Lihat")	{	
			iframe1.src = '<?php echo $this->config->base_url(); ?>index.php/PROMISE/ListPROMISE/search_PROMISE_adminlihat' + '/' + JenisUser + '/' +ProjectID;
			}else{
			iframe1.src = '<?php echo $this->config->base_url(); ?>index.php/PROMISE/ListPROMISE/search_PROMISE_adminedit'+ '/' + JenisUser + '/' + ProjectID;
			}
			iframe2.src = '<?php echo $this->config->base_url(); ?>index.php/PROMISE/ListPROMISE/search_PROMISE_MOMAdmin'+ '/'+ JenisUser + '/' + ProjectID;

			$('#divLoadingCari').hide();
			$('#divContent').show();
			
		}

	setInterval(function () { changeFormat();},500);
    search();
	

});

</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}	
		input#btnSubmitTambahMOM {
		width: 100%;
		background-color: #4CAF50;
		color: white;
		padding: 14px 20px;
		margin: 8px 0;
		border: none;
		border-radius: 4px;
		cursor: pointer;
		font-size:20px;
		}

		input#btnSubmitTambahMOM:hover {
		background-color: #45a049;
		}	
		
		.progress-bar{
		background:  rgb(80, 168, 70);
			-webkit-animation-name: pulse; /* Chrome, Safari, Opera */
			-webkit-animation-duration: 2s; /* Chrome, Safari, Opera */
			-webkit-animation-iteration-count: infinite;
			animation-name: pulse;
			animation-duration: 2s;
			animation-iteration-count: infinite;
		}


		@-webkit-keyframes pulse {
			0%   {background: rgb(80, 168, 70);}
			25%   {background: rgb(80, 168, 70);}
			50%   {background: rgb(80, 168, 70);}
			75%  {background: rgb(191, 255, 0);}
			85%{background: rgb(80, 168, 70);}
			100% {background: rgb(80, 168, 70);}
		}

		@keyframes pulse {
			0%   {background: rgb(80, 168, 70);}
			25%   {background: rgb(80, 168, 70);}
			50%   {background: rgb(80, 168, 70);}
			75%  {background: rgb(191, 255, 0);}
			85%{background: rgb(80, 168, 70);}
			100% {background: rgb(80, 168, 70);}
		}
		</style>
		<title>Detail Project dan Aktivitas</title>
	</head>
	
	<body>
	
	<?php 
			$Today = date("Y/m/d");
			$TestDate = '2019/04/30';
			$EndDate = date('Y/m/d', strtotime($ProjectDetail_array['EndDate']));
			if(substr($ProjectDetail_array['Deadline'],0,2)=="Q1"){ 
				$Deadline="20".substr($ProjectDetail_array['Deadline'],3,2). "/03/31";
			}else if(substr($ProjectDetail_array['Deadline'],0,2)=="Q2"){ 
				$Deadline="20".substr($ProjectDetail_array['Deadline'],3,2). "/06/30";
			}else if(substr($ProjectDetail_array['Deadline'],0,2)=="Q3"){ 
				$Deadline="20".substr($ProjectDetail_array['Deadline'],3,2). "/09/30";
			}else if(substr($ProjectDetail_array['Deadline'],0,2)=="Q4"){ 
				$Deadline="20".substr($ProjectDetail_array['Deadline'],3,2). "/12/31";
			}
			
			$Deadline14= date('Y/m/d', strtotime("+14 days", strtotime($Deadline)));
			$Deadline30= date('Y/m/d', strtotime("+30 days", strtotime($Deadline)));
			$Deadline45= date('Y/m/d', strtotime("+45 days", strtotime($Deadline)));
			$Deadline60= date('Y/m/d', strtotime("+60 days", strtotime($Deadline)));
			$datetime1 = strtotime($Deadline);
			$datetime2 = strtotime($Today);
			$DeadlinePara = date('Y/m/d', strtotime($Parameter_array['DeadlinePara']));
			
			$secs = $datetime2 - $datetime1;// == <seconds between the two times>
			$MasaLewat = $secs / 86400;
			$PesanDeadline='';
			$DeadlineProject='';
			$ProgressColor = '#4CAF50';
			if($Today >= $Deadline14){
				$ProgressColor = '#faff00';
				$PesanDeadline = 'Project anda telah melewati batas waktu selama ' .$MasaLewat. ' hari';
				$DeadlineProject = 'Deadline Project = ' .$Deadline;
			}
			if($Today >= $Deadline30){
				$ProgressColor = '#f90409';
				$PesanDeadline = 'Project anda telah melewati batas waktu selama ' .$MasaLewat. ' hari';
				$DeadlineProject = 'Deadline Project = ' .$Deadline;
			}
			if($Today >= $Deadline45){
				$ProgressColor = '#820306';
				$PesanDeadline = 'Project anda telah melewati batas waktu selama ' .$MasaLewat. ' hari';
				$DeadlineProject = 'Deadline Project = ' .$Deadline;
			}
			if($Today >= $Deadline60){
				$ProgressColor = '#000000';
				$PesanDeadline = 'Project anda telah melewati batas waktu selama ' .$MasaLewat. ' hari';
				$DeadlineProject = 'Deadline Project = ' .$Deadline;
			}
			if($EndDate < $Deadline14 && $ProjectProgress_array['Presentase']=='100'){
				$ProgressColor = '#4CAF50';
			}
			if($EndDate >= $Deadline14 && $ProjectProgress_array['Presentase']=='100'){
				$ProgressColor = '#f90409';
			}
			if($EndDate >= $Deadline45 && $ProjectProgress_array['Presentase']=='100'){
				$ProgressColor = '#820306';
			}
			if($EndDate >= $Deadline60 && $ProjectProgress_array['Presentase']=='100'){
				$ProgressColor = '#000000';
			}
			

			?>
	<?php 
	if($PageOrder == "Lihat"){?>
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Detail Project dan Aktivitas</h3>
		</div>

		<div class="panel-body">
			<?php echo validation_errors(); ?>
			<p style="font-size:20px; font-weight: bold;"><?php echo $ProjectDetail_array['NamaProject'];?></p>	
			<?php if($Today < $DeadlinePara && $Today <= $Deadline14 && $ProjectProgress_array['Presentase']<>'100'){?>
			<div id="progressdeadline">
			<div class="progress" style="background-color:#dd;width:920px;height:20px" id="new-parser-progress">
			<div class="progress-bar" style="width:<?php echo $ProjectProgress_array['Presentase']; ?>%;">
			<p style="font-size:20px; font-weight: bold;"><?php echo $ProjectProgress_array['Presentase']; ?>%</p>
			</div>
			</div>
			</div>
			<?php }else{?>
			<div id="progressall">
			<div class="progress" style="background-color:#dd;width:920px;height:20px">
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $ProjectProgress_array['Presentase']; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $ProjectProgress_array['Presentase']; ?>%;background-color:<?php echo $ProgressColor?>; height:30px">
					<p style="font-size:20px; font-weight: bold;"><?php echo $ProjectProgress_array['Presentase']; ?>%</p>
					</div>
			</div>
			</div>
			<?php }?>
			
			<p style="font-weight: bold"><?php echo $PesanDeadline?></p>
			<p style="font-weight: bold"><?php echo $DeadlineProject ?></p>
    <table border=1 id="tbllihatdetail">
	<p style="font-size:15px; font-weight: bold;">Lihat Detail :
    <input type="radio" id="LihatYa" name="LihatDet" value="1"> Ya
    <input  type="radio" id="LihatTidak" name="LihatDet" value="0" checked="checked"> Tidak</p>
	<tr>
	<td colspan="2">Key Performance Indicator</td>
	<td colspan="4"><?php echo $ProjectDetail_array['KPI'];?></td>
	</tr>
	<tr>
	<td colspan="2">Target</td>
	<td colspan="4"><?php echo $ProjectDetail_array['Target'];?></td>
	</tr>
	<tr>
	<td colspan="2">Start Date</td>
	<td colspan="4"><?php echo $ProjectDetail_array['StartDate'];?></td>
	</tr>
	<tr>
	<td colspan="2">Deadline UREQ</td>
	<td colspan="4"><?php echo $ProjectDetail_array['DeadlineUREQ'];?></td>
	</tr>
	<tr>
	<td colspan="2">Deadline</td>
	<td colspan="4"><?php echo $ProjectDetail_array['Deadline'];?></td>
	</tr>
	<tr>
	<td colspan="2">Person In Charge</td>
	<td colspan="4"><?php echo $ProjectDetail_array['PIC'];?></td>
	</tr>
	<tr>
	<td colspan="2">Budget</td>
	<td colspan="4">Rp <?php echo number_format($ProjectDetail_array['Budget'],2,',','.');?></td>
	</tr>
	<tr>
	<td colspan="2">Departemen</td>
	<td colspan="4"><?php echo $ProjectDetail_array['Departemen'];?></td>
	</tr>
	<tr>
	<td colspan="2">Keterangan</td>
	<td colspan="4"><?php echo $ProjectDetail_array['Keterangan'];?></td>
	</tr>
	<tr>
	<td colspan="2">Tujuan</td>
	<td colspan="4"><?php echo $ProjectDetail_array['DetilInfo'];?></td>
	</tr>
	<tr>
	<td colspan="2">Keterangan Admin</td>
	<td colspan="4"><?php echo $ProjectDetail_array['KetAdmin'];?></td>
	</tr>
	<tr>
	<td colspan="2">Status</td>
	<td colspan="4"><?php echo $ProjectDetail_array['Status'];?></td>
	</tr>
    </table>

	<div id="divContent">
			<H4><b>List Parameter</b></H4>
			<iframe id="iFrameListPROMISEAdmin" src="<?php echo $this->config->base_url(); ?>index.php/PROMISE/ListPROMISE" width="100%" height="500px" seamless frameBorder="0">
			<p>Your browser does not support iframes.</p>
			</iframe>
			<form action="submitTambahMOM" method="post" accept-charset="utf-8">
			<input type="hidden" id="txtProjectID" name="txtProjectID" size="50%" value="<?php echo $ProjectDetail_array['ProjectID'];?>">
			<div id="divContent">
			<H4><b>Minutes of Meeting</b></H4>
			<iframe id="iFrameListPROMISEMOMAdmin" src="<?php echo $this->config->base_url(); ?>index.php/PROMISE/ListPROMISE" width="100%" height="500px" seamless frameBorder="0">
			<p>Your browser does not support iframes.</p>
			</iframe>
			<input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin menyimpan Data ini ke dalam database?')" id="btnSubmitTambahMOM" name="submitTambahMOM" value="Simpan Minutes of Meeting">
			</form>
	</div>
	<?php } else {?>
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">Ubah Detail Project dan Aktivitas</h3>
		</div>

		<div class="panel-body">
			<?php echo validation_errors(); ?>

	
	<form action="submitUbahPROMISE" method="post" accept-charset="utf-8">
    <table border=1 id="tbleditdetail">
	<tr>
	<td colspan="2">Nama Project</td>
	<input type="hidden" id="txtProjectID" name="txtProjectID" size="50%" value="<?php echo $ProjectDetail_array['ProjectID'];?>">
	<td colspan="4"><input type="text" id="txtNmProject" name="txtNmProject" size="50%" value="<?php echo $ProjectDetail_array['NamaProject'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Key Performance Indicator</td>
	<td colspan="4"><input type="text" id="txtKPI" name="txtKPI" size="50%" value="<?php echo $ProjectDetail_array['KPI'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Target</td>
	<td colspan="4"><input type="text" id="txtTarget" name="txtTarget" size="50%" value="<?php echo $ProjectDetail_array['Target'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Start Date</td>
	<td colspan="4"><input type="text" id="txtStartDate" name="txtStartDate" size="50%" value="<?php echo $ProjectDetail_array['StartDate'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Deadline UREQ</td>
	<td colspan="4"><input type="text" id="txtDeadlineUREQ" name="txtDeadlineUREQ" size="50%" value="<?php echo $ProjectDetail_array['DeadlineUREQ'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Deadline</td>
	<td colspan="4"><input type="text" id="txtDeadline" name="txtDeadline" size="50%" value="<?php echo $ProjectDetail_array['Deadline'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Person In Charge</td>
	<td colspan="4"><input type="text" id="txtPIC" name="txtPIC" size="50%" value="<?php echo $ProjectDetail_array['PIC'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Budget</td>
	<td colspan="4"><input type="text" id="txtBudget" name="txtBudget" size="50%" value="<?php echo $ProjectDetail_array['Budget'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Departemen</td>
	<td colspan="4"><input type="text" id="txtDepartemen" name="txtDepartemen" size="50%" value="<?php echo $ProjectDetail_array['Departemen'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Keterangan</td>
	<td colspan="4"><input type="text" id="txtKeterangan" name="txtKeterangan" size="50%" value="<?php echo $ProjectDetail_array['Keterangan'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Tujuan</td>
	<td colspan="4"><input type="text" id="txtTujuan" name="txtTujuan" size="50%" value="<?php echo $ProjectDetail_array['DetilInfo'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Keterangan Admin</td>
	<td colspan="4"><input type="text" id="txtKetAdmin" name="txtKetAdmin" size="50%" value="<?php echo $ProjectDetail_array['KetAdmin'];?>"></td>
	</tr>
	<tr>
	<td colspan="2">Status</td>
	<td colspan="4"><input type="text" id="txtStatus" name="txtStatus" size="50%" value="<?php echo $ProjectDetail_array['Status'];?>" readonly></td>
	</tr>
    </table>

	<div id="divContent">
			<H4><b>List Parameter</b></H4>
			<iframe id="iFrameListPROMISEAdmin" src="<?php echo $this->config->base_url(); ?>index.php/PROMISE/ListPROMISE" width="100%" height="500px" seamless frameBorder="0">
			<p>Your browser does not support iframes.</p>
			</iframe>
	</div>

            <input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin menyimpan Data ini ke dalam database?')" id="btnSubmit" name="submitUbahPROMISE" value="Simpan">

	<br/>
	</form>
	<?php }?>
	</body>
</html>