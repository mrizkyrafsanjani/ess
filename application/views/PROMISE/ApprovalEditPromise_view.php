<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<style>
	table {
		border-collapse: collapse;
  		width: 100%;
	}
	</style>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
	<?php //echo $parameter_array[0]['Status']?>
	<?php //echo $KodeRequestDetail_array[0]['NoTransaksi']; ?>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Edit PROMISE Request </h3>
			</div>
			<div class="panel-body">
						<div class="row">
					<div class="col-md-3">
						Nama:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['NamaProject']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						KPI:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['KPI']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Target:
					</div>
					<div class="col-md-6">
						<?php  echo $data_array['Target']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Start Date:
					</div>
					<div class="col-md-6">
						<?php  echo $data_array['StartDate']; ?>
					</div>
				</div>
				<?php if($data_array['DeadlineUREQ']<>''){?>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Deadline UREQ:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['DeadlineUREQ']; ?>
					</div>
				</div>
				<?php } ?>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Deadline :
					</div>
					<div class="col-md-6">
						<?php echo $data_array['Deadline']; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						PIC:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['PIC']; ?>
					</div>
				</div>
				<br/>
				<?php if($data_array['Budget']<>''){?>
				<div class="row">
					<div class="col-md-3">
						Budget:
					</div>
					<div class="col-md-6">
						<?php echo 'Rp ' .number_format($data_array['Budget'],2,',','.'); ?>
					</div>
				</div>
				<br/>
				<?php } ?>
				<div class="row">
					<div class="col-md-3">
						Departemen:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['Departemen']; ?>
					</div>
				</div>
				<br/>
				<?php if($data_array['Keterangan']<>''){?>
				<div class="row">
					<div class="col-md-3">
						Keterangan:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['Keterangan']; ?>
					</div>
				</div>
				<br/>
				<?php } ?>
				<div class="row">
					<div class="col-md-3">
						Dibuat Pada:
					</div>
					<div class="col-md-6">
						<?php echo $data_array['CreatedOn']; ?>
					</div>
				</div>
				<br/>
				<?php 
				if(count($parameter_array)<>''){ ?>
				<div class="row">
				<div class="col-md-3">
						Parameter:
					</div>
					<div class="col-md-6">
				<table border="1">
								<tr>
								<th>Indikator Keberhasilan</th>
								<th>Presentase</th>
								<th>Deadline</th>   
								<th>Created By</th> 
								<th>Action</th>          
								</tr>
								
				<?php 
					
                        for($i=0;$i<count($parameter_array);$i++){
							if($parameter_array[$i]['Inserts']<>0){
								$Action = "New";						
								}
							if($parameter_array[$i]['Updates']<>0){
								$Action = "Edit";						
								}
							if($parameter_array[$i]['Deletes']<>0){
								$Action = "Delete";						
								}
							if($parameter_array[$i]['Inserts']<>0 && $parameter_array[$i]['Updates']<>0){
								$Action = "New and Edit";						
								}
							if($Action=="New" || $Action=="Delete"){
                            echo "<tr><td>".$parameter_array[$i]['Parameter'] ." </td>
                                    <td>". $parameter_array[$i]['Presentase'] ."%</td>
                                    <td>". $parameter_array[$i]['DeadlinePara'] ."</td>   
									<td>". $parameter_array[$i]['CreatedBy'] ."</td>
									<td>". $Action ."</td> 
									</tr> ";    
							}else{
								echo "<tr><td>".$parameter_array[$i]['KoreksiParameter'] ." </td>
                                    <td>". $parameter_array[$i]['KoreksiPresentase'] ."%</td>
                                    <td>". $parameter_array[$i]['KoreksiDeadline'] ."</td>   
									<td>". $parameter_array[$i]['CreatedBy'] ."</td>
									<td>". $Action ."</td> 
									</tr> ";   
							}   
						
						}
				
					echo "</table>";
                ?>   
				</div>
				</div>
				<?php } ?>
				<br/>
				<div class="row">
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				</div>
				<br/>
				<div class="row">
					<div align="right">
						<button id="btnApprove" type="button"  onClick="btnApprove_click()" class="btn btn-primary">Approve</button>
						<button id="btnDecline" type="button" onClick="btnDecline_click()" class="btn ">Decline</button>
						
					</div>
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
	
	function btnApprove_click(){
			var txtCatatan = $('#txtCatatan');
			if(confirm('Apakah Anda yakin ingin approval data ini?')==true)
			{
			
				$.post("<?php echo $this->config->base_url(); ?>index.php/PROMISE/PROMISEController/ajax_prosesApprovalEditPromise",
					{
					kodeusertask: "<?php echo $KodeRequestDetail_array[0]['KodeUserTask']; ?>",
					koderequestdetail: "<?php echo $KodeRequestDetail_array[0]['NoTransaksi']; ?>",
					status: "AP",
					catatan: txtCatatan.val(),
					action: "<?php echo $Action; ?>",
					projectID: "<?php echo $data_array['ProjectID'];?>",
					},
					function(response){
					alert(response);
					window.location.replace('<?php echo site_url('UserTask'); ?>');
					});

			}
		}
	
	function btnDecline_click(){
			var txtCatatan = $('#txtCatatan');
	
			if(txtCatatan.val() == '')
			{
				alert('Catatan harus diisi!');
			}
			else
			{
				if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
				{
					$.post("<?php echo $this->config->base_url(); ?>index.php/PROMISE/PROMISEController/ajax_prosesApprovalEditPromise",
					{
					kodeusertask: "<?php echo $KodeRequestDetail_array[0]['KodeUserTask']; ?>",
					koderequestdetail: "<?php echo $KodeRequestDetail_array[0]['NoTransaksi']; ?>",
					status: "DE",
					catatan: txtCatatan.val(),
					action: "<?php echo $Action; ?>",
					projectID: "<?php echo $data_array['ProjectID'];?>",
					},
					function(response){
					alert(response);
					window.location.replace('<?php echo site_url('UserTask'); ?>');
					});
				}
			}
		}
	
	
	
	
	
	</script>
</html>
