<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/tablesorter.css";
	</style>

	<!--<script type="text/javascript" src="../assets/js/jquery.js"></script>-->
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<!--<script type="text/javascript" src="../assets/js/myfunction.js"></script>-->
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.tablesorter.js"></script> 
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.tablesorter.pager.js"></script> 

	<script type="text/javascript">
		$(document).ready(function() {
			$('#hid').hide();	
			$(".clsUang").formatNumber({format:"#,##0", locale:"us"});
			$("#mytable")
				.tablesorter({widthFixed: true, widgets: ['zebra']})
				.tablesorterPager({container: $("#pager")});
				
			
		});
		

		function popup(url) {
			popupWindow = window.open(
				url,'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=no')
		}
		</script>
						
	<head>
	
	</head>
	<body>
	<?php error_reporting(0);?>
		<h1 align="center">Rekap Project dan Aktivitas</h1><br/>
		<form action="rekapPROMISE" method="get">
		Filter berdasarkan departemen : 
		<select id="cmbDepartemen" name="cmbDepartemen">
		  <?php 
		  if($this->input->get("cmbDepartemen") === ""){
			$selected = "selected";
		  }
		  echo "<option value='Semua' $selected>".Semua."</option>";
		  foreach($departemen_array as $row){
		  if($this->input->get("cmbDepartemen") === $row->departemen){
			$selected = "selected";
		  }else{
			$selected = "";
		  }
		  if($row->departemen=='IT'){
			$row->departemen="Information Technology";
		}
			echo "<option value='".$row->departemen."'>".$row->departemen."</option>";
		}

		  ?>
          
 
             
		</select>
		 Tahun 
		<select id="cmbTahun" name="cmbTahun">
			<?php 
			  if($this->input->get("cmbTahun") === ""){
				$selected = "selected";
			  }
			  echo "<option value='Semua' $selected>Semua</option>";
			  foreach($year_array as $row){
			  if($this->input->get("cmbTahun") == $row->year){
				$selected = "selected";
			  }else{
				$selected = "";
			  }
				echo "<option value='".$row->year."' $selected>".$row->year."</option>";
			  }
			?>
		</select>
		<table id="mytable" class="tablesorter" >
		<thead><tr>
				<th class="lblTop">Nama Project dan Aktivitas</th>
				<th class="lblTop" width="150px">Detil Informasi</th>
				<th class="lblTop">Deadline</th>
				<th class="lblTop">PIC</th>
				<th class="lblTop">Departemen</th>
				<th class="lblTop">Progress</th>
				<th class="lblTop">Status</th>
				<th class="lblTop">Keterangan</th>
				<th class="lblTop" id="hid">Tahun</th>
				<th class="lblTop">Lokasi File</th>
				<th class="lblTop">Lihat Detail</th>
				
			</tr></thead><tbody>
		<?php
		for($i=0;$i<count($getRekapAdmin_array);$i++){
			//ini_set('error_reporting',0);
		 //if ($detailuangmuka_array[$i]['TotalRealisasi']==0){
		//	}else{
				?>
			<?php 
			$Today = date("Y/m/d");
			$EndDate = $getRekapAdmin_array[$i]['EndDate']; 
			$TestDate = '2019/04/30';
			if(substr($getRekapAdmin_array[$i]['Deadline'],0,2)=="Q1"){ 
				$Deadline="20".substr($getRekapAdmin_array[$i]['Deadline'],3,2)."/03/31";
			}else if(substr($getRekapAdmin_array[$i]['Deadline'],0,2)=="Q2"){ 
				$Deadline="20".substr($getRekapAdmin_array[$i]['Deadline'],3,2)."/06/30";
			}else if(substr($getRekapAdmin_array[$i]['Deadline'],0,2)=="Q3"){ 
				$Deadline="20".substr($getRekapAdmin_array[$i]['Deadline'],3,2)."/09/30";
			}else if(substr($getRekapAdmin_array[$i]['Deadline'],0,2)=="Q4"){ 
				$Deadline="20".substr($getRekapAdmin_array[$i]['Deadline'],3,2)."/12/31";
			}
			$Deadline14= date('Y/m/d', strtotime("+14 days", strtotime($Deadline)));
			$Deadline30= date('Y/m/d', strtotime("+30 days", strtotime($Deadline)));
			$Deadline45= date('Y/m/d', strtotime("+45 days", strtotime($Deadline)));
			$Deadline60= date('Y/m/d', strtotime("+60 days", strtotime($Deadline)));
			$ProgressColor = '#4CAF50';
			if($Today >= $Deadline14){
				$ProgressColor = '#faff00';
			}
			if($Today >= $Deadline30){
				$ProgressColor = '#f90409';
			}
			if($Today >= $Deadline45){
				$ProgressColor = '#820306';
			}
			if($Today >= $Deadline60){
				$ProgressColor = '#000000';
			}
			if($EndDate <= $Deadline14 && $getProgress_array[$i]['Presentase']=='100'){
				$ProgressColor = '#4CAF50';
			}
			?>
			<tr>
				<td ><?php echo $getRekapAdmin_array[$i]['NamaProject']; ?></td>
				<td><?php echo $getRekapAdmin_array[$i]['DetilInfo']; ?></td>
				<td><?php echo $getRekapAdmin_array[$i]['Deadline']; ?></td>
				<td><?php echo $getRekapAdmin_array[$i]['PIC']; ?></td>
				<td><?php echo $getRekapAdmin_array[$i]['Departemen']; ?></td>
				<td width="50"><div class="progress" style=background-color:#dd;width:200px;height:20px>
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow=<?php echo $getProgress_array[$i]['Presentase']; ?> aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $getProgress_array[$i]['Presentase']; ?>%;background-color:<?php echo $ProgressColor?>; height:30px">
					<?php echo $getProgress_array[$i]['Presentase']; ?>%
					</div>
				</div> 
				</td>
				<td><?php echo $getRekapAdmin_array[$i]['Status']; ?></td>
				<td><?php echo $getRekapAdmin_array[$i]['KetAdmin']; ?></td>
				<td style="display:none;"><?php echo $getRekapAdmin_array[$i]['CreatedOn']; ?></td>
				<?php if($getRekapAdmin_array[$i]['FilePath'] <> ''){ ?>
					<td><a href="<?php echo $this->config->base_url();?>assets/uploads/files/PROMISE/<?php echo $getRekapAdmin_array[$i]['FilePath'];?>">Lihat File</a></td>
				<?php } else { ?>
					<td><a href="<?php echo $this->config->base_url();?>assets/uploads/files/PROMISE/<?php echo $getRekapAdmin_array[$i]['FilePath'];?>"></a></td>
				<?php }
				?>
				<td><a href="<?php echo $this->config->base_url();?>index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/<?php echo $getRekapAdmin_array[$i]['ProjectID'];?>">Lihat Detail</a></td>
				</tr>
				<?php 
				}?>
		
		</table>
		<div id="pager" class="pager" style="top: 0px; position: inline;">
			<form>
				<img src="<?php echo $this->config->base_url(); ?>assets/images/first.png" class="first">
				<img src="<?php echo $this->config->base_url(); ?>assets/images/prev.png" class="prev">
				<input type="text" class="pagedisplay" size="5">
				<img src="<?php echo $this->config->base_url(); ?>assets/images/next.png" class="next">
				<img src="<?php echo $this->config->base_url(); ?>assets/images/last.png" class="last">
				<select class="pagesize">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="30">30</option>
					<option value="40">40</option>
					<option selected="selected" value="50">50</option>
					
				</select>
			</form>
		</div>
	</body>
	<script type="text/javascript">
			$( document ).ready(function() {
			$('#hid').hide();

			$('#cmbDepartemen').change(function() {
				Filter();
			});
			$('#cmbTahun').change(function() {
				Filter();
			});

			function FilterDept() {

			var input, filter, table, tr, td, i, txtValue, Dept;
			Dept = document.getElementById("cmbDepartemen").value;
			input = document.getElementById("cmbDepartemen");
			filter = input.value.toUpperCase();
			table = document.getElementById("mytable");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[4];
				tdYear = tr[i].getElementsByTagName("td")[8];
				if (td) {
				txtValue = td.textContent || td.innerText;
				if (Dept=="Semua"){
					tr[i].style.display = "";
				}else{
				if (txtValue.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
				tdYear.style.display = "none";
				}
				}  
				     
			}

			}
			function FilterYear() {
			var input, filter, table, tr, td, i, txtValue, Year;
			Year = document.getElementById("cmbTahun").value;
			input = document.getElementById("cmbTahun");
			filter = input.value.toUpperCase();
			table = document.getElementById("mytable");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[8];
				if (td) {
				txtValue = td.textContent || td.innerText;
				if (Year=="Semua"){
					tr[i].style.display = "";
				}else{
				if (txtValue.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
				td.style.display = "none";
				}
				}  
				     
			}
		}
			function FilterDeptYear() {
			
			var input, filter, table, tr, td, i, txtValue, Status, Dept, inputDept;
			Dept = document.getElementById("cmbDepartemen").value;
			inputDept = document.getElementById("cmbDepartemen");
			Year = document.getElementById("cmbTahun").value;
			inputYear= document.getElementById("cmbTahun");
			filter2 = inputDept.value.toUpperCase();
			filter3 = inputYear.value.toUpperCase();
			table = document.getElementById("mytable");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				tdDept = tr[i].getElementsByTagName("td")[4];
				tdYear = tr[i].getElementsByTagName("td")[8];
				if (tdDept && tdYear) {
				txtValueDept = tdDept.textContent || tdDept.innerText;
				txtValueYear = tdYear.textContent || tdYear.innerText;
				if (Year=="Semua" && Dept=="Semua"){
					tr[i].style.display = "";
				}else{
				if (txtValueDept.toUpperCase().indexOf(filter2) > -1 && txtValueYear.toUpperCase().indexOf(filter3) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
				tdYear.style.display = "none";
				} 
			}
			}
			}
			function Filter(){
				var Dept = document.getElementById("cmbDepartemen").value;
				var Year = document.getElementById("cmbTahun").value;
				if (Dept == 'Semua' && Year=='Semua'){
					FilterDeptYear();
				}else if(Dept=='Semua' && Year!='Semua' ){
					FilterYear();
				}else if(Dept!='Semua' && Year =='Semua'){
					FilterDept();
				}else if(Dept!='Semua' && Year!='Semua'){
					FilterDeptYear();
				}
			}
		});
	</script>
</html>