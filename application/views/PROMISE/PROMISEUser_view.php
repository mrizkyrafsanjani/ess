<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/tablesorter.css";
	
	</style>
		<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.tablesorter.js"></script> 
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.tablesorter.pager.js"></script> 
	

	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
		<style>
		#mytable td {
			border-right: 1px solid #ccc;
		border-bottom: 1px solid #ccc;
		border-left: 1px solid #ccc;
		border-top: 1;
		background: #efefefe;
		}
		#mytable th {
			border-right: 1px solid #ccc;
		border-bottom: 1px solid #ccc;
		border-left: 1px solid #ccc;
		border-top:  1px solid #ccc;
		background: #efefefe;
		}
		
		td {
			padding:2px 2px 2px 2px;
			font-size:13px;
		}		
		th {
			padding:2px 2px 2px 2px;
			font-size:20px;
		}		
		.progress-bar{
		background:  rgb(80, 168, 70);
			-webkit-animation-name: pulse; /* Chrome, Safari, Opera */
			-webkit-animation-duration: 2s; /* Chrome, Safari, Opera */
			-webkit-animation-iteration-count: infinite;
			animation-name: pulse;
			animation-duration: 2s;
			animation-iteration-count: infinite;
		}


		@-webkit-keyframes pulse {
			0%   {background: rgb(80, 168, 70);}
			25%   {background: rgb(80, 168, 70);}
			50%   {background: rgb(80, 168, 70);}
			75%  {background: rgb(191, 255, 0);}
			85%{background: rgb(80, 168, 70);}
			100% {background: rgb(80, 168, 70);}
		}

		@keyframes pulse {
			0%   {background: rgb(80, 168, 70);}
			25%   {background: rgb(80, 168, 70);}
			50%   {background: rgb(80, 168, 70);}
			75%  {background: rgb(191, 255, 0);}
			85%{background: rgb(80, 168, 70);}
			100% {background: rgb(80, 168, 70);}
		}
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
			<h3 class="panel-title">PROMISE <?php echo $NamaUser_array[0]['nama']; ?></h3>
		</div>

		<div class="panel-body">
			<?php echo validation_errors(); ?>
			<input type=hidden id=cmbDept name=cmbDept value=<?php echo $NamaUser_array[0]['nama']?>>
			<table bordered='0'>			
				
				<!--<tr>
					<td>Rekening *</td>
					<td colspan='3'><select class="form-control" id="cmbRekening">
						<option value='BPT01DPA1'>Rekening Iuran DPA1</option>
						<option value='BPT02DPA1'>Rekening Investasi DPA1</option>
						<option value='BPT03DPA1'>Rekening Opex DPA1</option>
						<option value='BPT04DPA1'>Rekening MP DPA1</option>
						<option value='BPTDPA1'>Rekening CTD DPA1</option>
					</select></td>
				</tr>-->
				<tr>			
				<td>Tahun</td>
					<td colspan='3'>
						<select class="form-control" id="cmbTahun" name="cmbTahun">
						<?php 
							for($i=date('Y')+1;$i>date('Y')-2;$i--)
							{
								$selected = '';
								if(date('Y') == $i )
									$selected = 'selected';
								echo "<option value='".$i."' $selected>".$i."</option>";								
							}
							echo "<option value='Semua' selected>".Semua."</option>";
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Status</td>
					<td colspan=3>
						<select class="form-control" id="cmbStatus" name="cmbStatus">
							<option value="Semua">Semua</option>
							<option value="Done">Done</option>
							<option value="On Process">On Process</option>
						</select>
					</td>
				</tr>
				
				
				<tr>
					<td></td>
					<td>
					<div class="col-sm-4">
						<input class="btn btn-primary" type="button" onClick="Filter()" id="btnCari" name="" value="Cari"></div>
						<div id="divLoadingCari" hidden class="col-sm-7" style="padding-top:8px"><i id="spinner" class="fa fa-refresh fa-spin"></i>Loading...</div>
					</td>
					</td>
				</tr>
			</table>
		
			<div id="divContent" hidden>
			<H4><b>List Project dan Aktivitas
			<!--<div style="margin-top:20px"></div>-->
			<a href="../PROMISEForm"><img height="30px" width="100px" align="right" src="<?php echo $this->config->base_url(); ?>assets/images/insert.png"></a>
		
	
			</b></H4>					
			<form id="LihatDetail" name="LihatDetail" method="post" accept-charset="utf-8">
		<table id="mytable" class="tablesorter" border=0.5 >
		<thead>	
		<tr>
				<th class="lblTop" style="width:600px">Nama Project</th>
				<th class="lblTop">Deadline</th>
				<th class="lblTop">Departemen</th>
				<th class="lblTop">Progress</th>
				<th class="lblTop">Status</th>
				<th class="lblTop" id="hid">Tahun</th>
				<th class="lblTop" id="hide">PIC</th>
				<th class="lblTop">Pilihan</th>
				
			</tr></thead><tbody>
		<?php
		for($i=0;$i<count($getRekapAdmin_array);$i++){
			//ini_set('error_reporting',0);
		 //if ($detailuangmuka_array[$i]['TotalRealisasi']==0){
		//	}else{
			//echo $getRekapAdmin_array[$i]['ProjectID']; 
			//echo $getProgress_array[$i]['ProjectID'];
			//break;
				?>
			
			<?php 
			$Today = date("Y/m/d");
			$TestDate = '2019/04/30';
			$EndDate = date('Y/m/d', strtotime($getRekapAdmin_array[$i]['EndDate']));
			if(substr($getRekapAdmin_array[$i]['Deadline'],0,2)=="Q1"){ 
				$Deadline="20".substr($getRekapAdmin_array[$i]['Deadline'],3,2)."/03/31";
			}else if(substr($getRekapAdmin_array[$i]['Deadline'],0,2)=="Q2"){ 
				$Deadline="20".substr($getRekapAdmin_array[$i]['Deadline'],3,2)."/06/30";
			}else if(substr($getRekapAdmin_array[$i]['Deadline'],0,2)=="Q3"){ 
				$Deadline="20".substr($getRekapAdmin_array[$i]['Deadline'],3,2)."/09/30";
			}else if(substr($getRekapAdmin_array[$i]['Deadline'],0,2)=="Q4"){ 
				$Deadline="20".substr($getRekapAdmin_array[$i]['Deadline'],3,2)."/12/31";
			}
			$Deadline14= date('Y/m/d', strtotime("+14 days", strtotime($Deadline)));
			$Deadline30= date('Y/m/d', strtotime("+30 days", strtotime($Deadline)));
			$Deadline45= date('Y/m/d', strtotime("+45 days", strtotime($Deadline)));
			$Deadline60= date('Y/m/d', strtotime("+60 days", strtotime($Deadline)));
			$ProgressColor = '#4CAF50';
			if($Today >= $Deadline14){
				$ProgressColor = '#faff00';
			}
			if($Today >= $Deadline30){
				$ProgressColor = '#f90409';
			}
			if($Today >= $Deadline45){
				$ProgressColor = '#820306';
			}
			if($Today >= $Deadline60){
				$ProgressColor = '#000000';
			}
			if($EndDate < $Deadline14 && $getProgress_array[$i]['Presentase']=='100'){
				$ProgressColor = '#4CAF50';
			}
			if($EndDate >= $Deadline14 && $getProgress_array[$i]['Presentase']=='100'){
				$ProgressColor = '#f90409';
			}
			if($EndDate >= $Deadline45 && $getProgress_array[$i]['Presentase']=='100'){
				$ProgressColor = '#820306';
			}
			if($EndDate >= $Deadline60 && $getProgress_array[$i]['Presentase']=='100'){
				$ProgressColor = '#000000';
			}
			
			?>
			<tr>
				<td><?php echo $getRekapAdmin_array[$i]['NamaProject']; ?><input type="hidden" id="txtNamaProject" name="txtNamaProject" value="<?php echo $getRekapAdmin_array[$i]['NamaProject']; ?>"></td>
				<td><?php echo $getRekapAdmin_array[$i]['Deadline']; ?></td>
				<td><?php echo $getRekapAdmin_array[$i]['Departemen']; ?></td>
				<?php if ($getRekapAdmin_array[$i]['ProjectID'] == $getProgress_array[$i]['ProjectID']){?>
					
					<td width="50"><div class="progress" style=background-color:#dd;width:200px;height:20px>
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow=
					<?php $getProgress_array[$i]['Presentase']; ?> 
					aria-valuemin="0" aria-valuemax="100" style="width: 
					<?php echo $getProgress_array[$i]['Presentase']; ?>%;
					background-color:<?php echo $ProgressColor?>; height:30px">
					<?php echo $getProgress_array[$i]['Presentase']; ?>%
					</div>
				</div>
				</td>
				<?php } else { ?>
					<td width="50"><div class="progress" style=background-color:#dd;width:200px;height:20px>
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0"
					aria-valuemin="0" aria-valuemax="100" style="width:0%;
					background-color:#4CAF50; height:30px">0%
					</div>
				</div>
				</td>
				<?php } ?>
				<?php $NamaProject = $getRekapAdmin_array[$i]['NamaProject'];?>
				<td><?php echo $getRekapAdmin_array[$i]['Status']; ?></td>
				<td class="hid"><?php echo $getRekapAdmin_array[$i]['CreatedOn']; ?></td>
				<td class="hid"><?php echo $getRekapAdmin_array[$i]['PIC']; ?></td>
				<td align="right">
				<a href="../PROMISEDetail/getNamaProject/User/Lihat/<?php echo $getRekapAdmin_array[$i]['ProjectID']; ?>" title="Lihat Detail"><img height="25" width="30" src="<?php echo $this->config->base_url(); ?>assets/images/view1.png"> 
				<a href="../PROMISEDetail/getNamaProject/User/Edit/<?php echo $getRekapAdmin_array[$i]['ProjectID']; ?>" title="Ubah Detail"><img height="15" width="20" src="<?php echo $this->config->base_url(); ?>assets/images/pen1.png">

				
				</tr>
				<?php 
}?>
		</table>
		</form>
		
		<div id="pager" class="pager" style="top: 0px; position: inline;">
			<form>
				<img src="<?php echo $this->config->base_url(); ?>assets/images/first.png" class="first">
				<img src="<?php echo $this->config->base_url(); ?>assets/images/prev.png" class="prev">
				<input type="text" class="pagedisplay" size="5">
				<img src="<?php echo $this->config->base_url(); ?>assets/images/next.png" class="next">
				<img src="<?php echo $this->config->base_url(); ?>assets/images/last.png" class="last">
				<select class="pagesize">
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="30">30</option>
					<option value="40">40</option>
					<option selected="selected" value="50">50</option>
					
				</select>
			</form>
		
	</body>
	<script type="text/javascript">

			function FilterDeptStat() {
			
			var input, filter, table, tr, td, i, txtValue, Status, Dept, inputDept;
			Dept = document.getElementById("cmbDept").value;
			inputDept = document.getElementById("cmbDept");
			Status = document.getElementById("cmbStatus").value;
			input = document.getElementById("cmbStatus");
			filter1 = input.value.toUpperCase();
			filter2 = inputDept.value.toUpperCase();
			table = document.getElementById("mytable");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[4];
				tdDept = tr[i].getElementsByTagName("td")[6];
				tdYear = tr[i].getElementsByTagName("td")[5];
				if (td && tdDept) {
				txtValue = td.textContent || td.innerText 
				txtValueDept = tdDept.textContent || tdDept.innerText;
				if (txtValue.toUpperCase().indexOf(filter1) > -1 && txtValueDept.toUpperCase().indexOf(filter2) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
				tdYear.style.display = "none";
				tdDept.style.display = "none";
				} 	     
			}
			}

			function FilterDeptYear() {
			
			var input, filter, table, tr, td, i, txtValue, Status, Dept, inputDept;
			Dept = document.getElementById("cmbDept").value;
			inputDept = document.getElementById("cmbDept");
			Year = document.getElementById("cmbTahun").value;
			inputYear= document.getElementById("cmbTahun");
			filter2 = inputDept.value.toUpperCase();
			filter3 = inputYear.value.toUpperCase();
			table = document.getElementById("mytable");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				tdDept = tr[i].getElementsByTagName("td")[6];
				tdYear = tr[i].getElementsByTagName("td")[5];
				if (tdDept && tdYear) {
				txtValueDept = tdDept.textContent || tdDept.innerText;
				txtValueYear = tdYear.textContent || tdYear.innerText;
				if (txtValueDept.toUpperCase().indexOf(filter2) > -1 && txtValueYear.toUpperCase().indexOf(filter3) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
				tdYear.style.display = "none";
				tdDept.style.display = "none";
				} 
				     
			}
			}


			function FilterDeptStatYear() {
			
			var input, filter, table, tr, td, i, txtValue, Status, Dept, inputDept;
			Dept = document.getElementById("cmbDept").value;
			inputDept = document.getElementById("cmbDept");
			Year = document.getElementById("cmbTahun").value;
			inputYear= document.getElementById("cmbTahun");
			Status = document.getElementById("cmbStatus").value;
			input = document.getElementById("cmbStatus");
			filter1 = input.value.toUpperCase();
			filter2 = inputDept.value.toUpperCase();
			filter3 = inputYear.value.toUpperCase();
			table = document.getElementById("mytable");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[4];
				tdDept = tr[i].getElementsByTagName("td")[6];
				tdYear = tr[i].getElementsByTagName("td")[5];
				if (td && tdDept && tdYear) {
				txtValue = td.textContent || td.innerText 
				txtValueDept = tdDept.textContent || tdDept.innerText;
				txtValueYear = tdYear.textContent || tdYear.innerText;
				if (Year==Status){
					if (txtValueDept.toUpperCase().indexOf(filter2) > -1){
						tr[i].style.display = "";
					}else{
						tr[i].style.display = "none";
					}
				}else{
				if (Year=="Semua" && Status=="Semua"){
					tr[i].style.display = "";
				}else{
				if (txtValue.toUpperCase().indexOf(filter1) > -1 && txtValueDept.toUpperCase().indexOf(filter2) > -1 && txtValueYear.toUpperCase().indexOf(filter3) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
				}
				} 
				tdYear.style.display = "none";
				tdDept.style.display = "none";
				}
				     
			}


			
			}

			function Filter(){
				$('#divLoadingCari').show();
				var Status = document.getElementById("cmbStatus").value;
				var Dept = document.getElementById("cmbDept").value;
				var Year = document.getElementById("cmbTahun").value;
				if (Year==Status){
					FilterDeptStatYear();
				}else if(Year!='Semua' && Status =='Semua'){
					FilterDeptYear();
				}else if(Status !='Semua' && Year=='Semua'){
					FilterDeptStat();
				}else if(Status !='Semua' && Year!='Semua'){
					FilterDeptStatYear();
				}
			$('#divLoadingCari').hide();
			$('#divContent').show();	
			$('#hid').hide();
			$('#hide').hide();
			}
	
		
	</script>
	
</html>