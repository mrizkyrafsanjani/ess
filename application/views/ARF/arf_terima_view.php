<html>
    <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-ui.min.js"></script>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <script type="text/javascript">
        $(document).ready(function(){
            <?php if($this->session->flashdata('msg')){ ?>
            alert('<?php echo $this->session->flashdata('msg'); ?>');
            <?php } ?>
        });
    </script>
    <head>
        <title><?php echo $title ?></title>
        <!--site css -->
        <!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
        <style>
            td {padding:5px 5px 5px 5px;}
        </style>
    </head>
    <body>
    <?php echo validation_errors(); ?>
        <div class="panel panel-primary">

            <div class="panel-body">
                <form action="<?php echo $this->config->base_url(); ?>index.php/ARF/ArfController/do_terimaARF/<?php echo $kode; ?>" method="post" accept-charset="utf-8">
                    <fieldset>
                    <!-- <legend>Data Kepegawaian</legend> -->
                    <table border=0>
                        <tbody>
                        <tr>
                            <td class="lbl">No ARF </td>
                            <td><input id="txtNoARF" class="form-control" name="txtNoARF" type="text" size="25" maxlength="6"
                                       value="<?php  echo $NoARF;  ?>" disabled="disabled"><?php echo form_error('txtNoARF'); ?></td>
                        </tr>
                        <tr>
                            <td class="lbl">Subject </td>
                            <td><input id="txtSubject" class="form-control" name="txtSubject" type="text" size="25" maxlength="6"
                                       value="<?php  echo $subject;  ?>" disabled="disabled"><?php echo form_error('txtSubject'); ?></td>
                        </tr>
                        <tr>
                            <td class="lbl">Resolver *</td>
                            <td>
                                <?php
                                $sql = "SELECT NPK, Nama FROM mstruser where deleted='0' and Departemen='5' and (TanggalBerhenti is null or TanggalBerhenti=0)";
                                $query = $this->db->query($sql);

                                if($query->num_rows() > 0){
                                    $result= $query->result();
                                }else{
                                    echo "gada";
                                }

                                echo "<select name='cmbStatus' id='cmbStatus'>";

                                foreach ($result as $row) {
                                    //echo print_r($row);
                                    //echo $row->Nama;
                                    echo "<option value='" . $row->NPK ."'>" . $row->Nama ."</option>";
                                }
                                echo "</select>";
                                ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </fieldset>
                <div id="note">
                </div>
                <table class="tblPureNoBorder" style="float:right;">
                    <tr>
                        <td>
                            <br/><input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin mengupdate data ini ke dalam database?')" id="btnSubmit" name="submitForm" value="Submit">
                            <div id="divError"></div>
                        </td>
                    </tr>
                </table>
                <br/>
            </form>
            </div>
        </div>
    </body>
</html>