<html>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-ui.min.js"></script>
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
<script type="text/javascript">
    $(function() {

    });

    $(document).ready(function(){
        <?php if($this->session->flashdata('msg')){ ?>
        alert('<?php echo $this->session->flashdata('msg'); ?>');
        <?php } ?>
    });
</script>
<head>
    <title><?php echo $title ?></title>
    <!--site css -->
    <!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
    <style>
        td {padding:5px 5px 5px 5px;}
    </style>
</head>
<body>
<?php echo validation_errors(); ?>
<div class="panel panel-primary">

    <div class="panel-body">
        <form action="<?php echo $this->config->base_url(); ?>index.php/ARF/ArfController/do_tolakARF/<?php echo $kode; ?>" method="post" accept-charset="utf-8">
            <fieldset>
                <!-- <legend>Data Kepegawaian</legend> -->
                <table border=0>
                    <tbody>
                    <tr>
                        <td class="lbl">User </td>
                        <td><input id="txtARF" class="form-control" name="txtARF" type="text" size="25" maxlength="6"
                                   value="<?php  echo $user;  ?>" disabled="disabled"><?php echo form_error('txtARF'); ?></td>
                    </tr>
                    <tr>
                        <td class="lbl">Subject </td>
                        <td><input id="txtSubject" class="form-control" name="txtSubject" type="text" size="25" maxlength="6"
                                   value="<?php  echo $subject;  ?>" disabled="disabled"><?php echo form_error('txtSubject'); ?></td>
                    </tr>
                    <tr>
                        <td class="lbl">Decline Reason *</td>
                        <td><input id="txtDecline" class="form-control" name="txtDecline" type="text" size="75"
                                   value="<?php  echo $decline;  ?>"><?php echo form_error('txtDecline'); ?></td>
                    </tr>
                    </tbody>
                </table>

            </fieldset>
            <div id="note">
            </div>
            <table class="tblPureNoBorder" style="float:right;">
                <tr>
                    <td>
                        <br/><input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin mengupdate data ini ke dalam database?')" id="btnSubmit" name="submitForm" value="Submit">
                        <div id="divError"></div>
                    </td>
                </tr>
            </table>
            <br/>
        </form>
    </div>
</div>
</body>
</html>