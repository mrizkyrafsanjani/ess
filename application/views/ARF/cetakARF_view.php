<html>
   <script>
        function cetak(){
            window.print();
            window.close();
            location.href="../../ArfController/ViewUser/BelumDiterima"
        }
   </script>
   <style type="text/css">
       .tg  {border-collapse:collapse;border-spacing:0;}
       .tg td{font-family:Arial, sans-serif;font-size:12px;padding:1px 10px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
       .tg th{font-family:Arial, sans-serif;font-size:12px;font-weight:normal;padding:1px 10px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
       ul {

           list-style:none;
           padding-left:0;
       }

       ::selection {
           background-color: #E13300;
           color: white;
       }
       #wrapper {
           width: 100%;
           height: 100%;
           margin: auto;
       }
       #body {
           float: right;
           width: 100%;
           height: 100%;
       }
       html {
           margin: 20px
       }
       body {
           background-color: #fff;
           font: 10px/20px normal Helvetica, Arial, sans-serif;
           color: #4F5155;
       }

       table.gridtable th {
           border-width: 1px;
           border-style: solid;
           border-color: #666666;
           background-color: #ffffff;
       }

       table, tr, th, td{

           border-collapse: collapse;
           border: 1px solid black;
       }
       table.gridtable td {
           border-width: 1px;
           border-style: solid;
           border-color: #666666;
           background-color: #ffffff;
           text-align: center;
       }

       ul {
           margin: 0;
       }
       ul.dashed > li {
           text-indent: -5px;
       }
       ul.dashed > li:before {
           content: "-";
           text-indent: -5px;
       }
   </style>
    <head>
        <title>ARF</title>
    </head>
    <body onload="cetak()">
        <div id="wrapper">
            <div id="body" >
                <div id="table">
                    <table class="tg" border="1">
                        <tr >
                            <td  colspan="1"><image id="logoDPA"  src='../../../../assets/images/LogoDPA.png' align="left"/></td>
                            <td  colspan="4">DANA PENSIUN ASTRA</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center"><h3>APPLICATION REQUEST FORM</h3></td>
                            <td colspan="2">Request No :</td>
                        </tr>
                        <tr style="background-color: greenyellow;">
                            <td colspan="3">Filled by User</td>
                            <td colspan="2">Request Date : <?php $tanggal= $a_array[0]['reqdate'] ;
                                echo date('D,d F Y',  strtotime($tanggal));?></td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td align="center">Name</td>
                            <td align="center">Dept/Unit</td>
                            <td align="center">Email</td>
                            <td align="center">Ext</td>
                        </tr>
                        <tr>
                            <td>User/Initiator</td>
                            <td align="center"><?php echo $a_array[0]['namauser']; ?></td>
                            <td align="center"><?php echo $a_array[0]['deptuser']; ?></td>
                            <td align="center"><?php echo $a_array[0]['emailuser']; ?></td>
                            <td align="center"><?php echo $a_array[0]['extuser']; ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $a_array[0]['jabatanatasan']; ?></td>
                            <td align="center"><?php echo $a_array[0]['atasan']; ?></td>
                            <td align="center"><?php echo $a_array[0]['deptatasan']; ?></td>
                            <td align="center"><?php echo $a_array[0]['emailatasan']; ?></td>
                            <td align="center"><?php echo $a_array[0]['extatasan']; ?></td>
                        </tr>
                        <tr>
                            <td>Application Name    :</td>
                            <?php
                            $checked1= "";
                            $checked2= "";
                            $checked3= "";
                            $checked4= "";
                            $checked5= "";
                            $checked6= "";
                            $checked7= "";
                            $checked8= "";
                            $checked9= "";
                            $checked10= "";
                            $checked11= "";
                            $checked12= "";
                            if($a_array[0]['application']=='Core'){
                                $checked1= "checked";
                            }else if($a_array[0]['application']=='Siap'){
                                $checked2= "checked";
                            }else if($a_array[0]['application']=='DPA Mobile/BB'){
                                $checked3= "checked";
                            }else if($a_array[0]['application']=='Accpac') {
                                $checked4 = "checked";
                            }else if($a_array[0]['application']=='siDapen DPA1') {
                                $checked5 = "checked";
                            }else if($a_array[0]['application']=='Lainnya') {
                                $checked6 = "checked";
                            }else if($a_array[0]['application']=='SIDP') {
                                $checked7 = "checked";
                            }else if($a_array[0]['application']=='Apisoft') {
                                $checked8 = "checked";
                            }else if($a_array[0]['application']=='IVR') {
                                $checked9 = "checked";
                            }else if($a_array[0]['application']=='Web Dapenastra') {
                                $checked10 = "checked";
                            }else if($a_array[0]['application']=='Vinno SMS') {
                                $checked11 = "checked";
                            }else if($a_array[0]['application']=='ESS') {
                                $checked12 = "checked";
                            }
                            ?>
                            <td>
                                <ul>
                                    <li><input type="checkbox" <?php echo $checked1; ?>  disabled="disabled"> Core</li>
                                    <li><input type="checkbox" <?php echo $checked2; ?> disabled="disabled"> SIAP</li>
                                    <li><input type="checkbox" <?php echo $checked3; ?> disabled="disabled"> DPA Mobile/BB</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li><input type="checkbox" <?php echo $checked4; ?> disabled="disabled"> Accpac</li>
                                    <li><input type="checkbox" <?php echo $checked5; ?> disabled="disabled"> siDAPEN DPA1</li>
                                    <li><input type="checkbox" <?php echo $checked6; ?> disabled="disabled"> Lainnya</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li><input type="checkbox" <?php echo $checked7; ?> disabled="disabled"> SIDP</li>
                                    <li><input type="checkbox" <?php echo $checked8; ?> disabled="disabled"> Apisoft</li>
                                    <li><input type="checkbox" <?php echo $checked9; ?> disabled="disabled"> IVR</li>

                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li><input type="checkbox" <?php echo $checked10; ?> disabled="disabled"> Web Dapenastra</li>
                                    <li><input type="checkbox" <?php echo $checked11; ?> disabled="disabled"> Vinno</li>
                                    <li><input type="checkbox" <?php echo $checked12; ?> disabled="disabled"> ESS</li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">Request  :</td>
                        </tr>
                        <tr height="250px" >
                            <td colspan="5" style="text-align:left;vertical-align:top;padding:1"><?php echo $a_array[0]['request']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center">Request Approval</td>
                        </tr>
                        <tr>
                            <td align="center">User/Initiator</td>
                            <td align="center"><?php echo $a_array[0]['jabatanatasan']; ?></td>
                            <td align="center"><?php echo $a_array[0]['jabatanHead'];  ?></td>
                            <td align="center">IT DIC</td>
                            <td rowspan="3" style="text-align:left;vertical-align:top;padding:1">Note:</td>
                        </tr>
                        <tr height="60px">
                            <td ></td>
                            <td></td>
                            <td ></td>
                            <td ></td>

                        </tr>
                        <tr >
                            <td align="center"><?php echo $a_array[0]['namauser']; ?></td>
                            <td align="center" ><?php echo $a_array[0]['atasan']; ?></td>
                            <td align="center" > <?php echo $a_array[0]['namaHead']; ?></td>
                            <td align="center"> <?php echo $a_array[0]['namaDIC']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="5"> Filled by IT</td>
                        </tr>
                        <tr>
                            <td>Priority</td>
                            <td>
                                <input type="checkbox">Urgent
                            </td>
                            <td>
                                <input type="checkbox">Medium
                            </td>
                            <td>
                                <input type="checkbox">Normal
                            </td>
                            <td align="center">Receive Date</td>
                        </tr>
                        <tr>
                            <td>Category</td>
                            <td>
                                <input type="checkbox">Revision
                            </td>
                            <td>
                                <input type="checkbox">Development
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="center">Start Work Date</td>
                            <td align="center">Target Date</td>
                            <td align="center">UAT Date</td>
                            <td align="center" colspan="2">Implementation Date</td>
                        </tr>
                        <tr height="25px">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td  colspan="2"></td>
                        </tr>
                        <tr height="80px" >
                            <td colspan="5" style="text-align:left;vertical-align:top;padding:1">Note :</td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center">UAT Approval</td>
                        </tr>
                        <tr>
                            <td align="center">User/Initiator</td>
                            <td align="center"><?php echo $a_array[0]['jabatanatasan'];  ?></td>
                            <td align="center"><?php echo $a_array[0]['jabatanHead'];  ?></td>
                            <td align="center">IT DIC</td>
                            <td align="center">Resolver</td>
                        </tr>
                        <tr height="60px">
                            <td ></td>
                            <td></td>
                            <td ></td>
                            <td ></td>
                            <td rowspan="2"></td>
                        </tr>
                        <tr >
                            <td align="center"><?php echo $a_array[0]['namauser']; ?></td>
                            <td align="center"><?php echo $a_array[0]['atasan']; ?></td>
                            <td align="center" > <?php echo $a_array[0]['namaHead']; ?></td>
                            <td align="center"> <?php echo $a_array[0]['namaDIC']; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>