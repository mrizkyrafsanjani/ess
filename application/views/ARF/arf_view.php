<html>
<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
<script type="text/javascript">
    function alertsizeARF(pixels){
        pixels+=60;
        document.getElementById('iFrameARF').style.height=pixels+"px";
    }

    function search(){
        var npk = '<?php echo $npk; ?>';
        var status= document.getElementById('txtStatus').value;

        var iframe = document.getElementById('iFrameARF');
        if(npk === '')
        {
            if (status ===''){
                alert('Harap masukkan status yang ingin dicari dengan benar!');
            }else {

                        iframe.src = '<?php echo $this->config->base_url(); ?>index.php/ARF/Arf/viewAdmin/'+status;

            }

        }
        else
        {
            iframe.src = '<?php echo $this->config->base_url(); ?>index.php/ARF/Arf/viewUser/'+status+'/'+npk;
        }
    }
</script>
<head>
    <title><?php $title ?></title>
    <!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
    <style>
        td {padding:5px 5px 5px 5px;}
    </style>
</head>
<body>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><?php
        if($npk == '' && $admin == '1'){
            ?>ARF<?php }else{ ?>ARF Yang Dibuat Oleh Saya <?php } ?></h3>
    </div>
    <div class="panel-body">
        <?php echo validation_errors(); ?>
        <table border=0>
           <tr>
                <td>Status</td>
                <td>
                    <select class="form-control" id="txtStatus" name="txtStatus">
                        <option value="All">All</option>
                        <option value="BelumDiterima">Belum Diterima</option>
                        <option value="Diterima"> Diterima</option>
                        <option value="Pending">Pending</option>
                        <option value="BPA">Proses BPA</option>
                        <option value="Development">Development</option>
                        <option value="UAT">UAT</option>
                        <option value="Resolve">Resolve</option>
                        <option value="Implementation">Implementasi</option>
                        <option value="BugFixing">Bug Fixing</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari">

                </td>
            </tr>
        </table>
        <?php
        if($npk == '' && $admin == '1'){
            ?>
            <iframe id="iFrameARF" src="<?php echo $this->config->base_url(); ?>index.php/ARF/Arf/viewAdmin" width="100%" height="200px" seamless frameBorder="0">
                <p>Your browser does not support iframes.</p>
            </iframe>
        <?php }else{ ?>
            <iframe id="iFrameARF" src="<?php echo $this->config->base_url(); ?>index.php/ARF/Arf/viewUser/non/<?php echo $npk; ?>" width="100%" height="200px" seamless frameBorder="0">
                <p>Your browser does not support iframes.</p>
            </iframe>
        <?php } ?>
    </div>
</div>
</body>
</html>
