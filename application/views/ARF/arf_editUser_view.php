<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-ui.min.js">
	</script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title>Data ARF User</title>
		<style>
		td {padding:5px 5px 5px 5px;}	

		</style>
	</head>
	<body >
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-body">
				<font face="Helvetica">
					<form action="<?php echo $this->config->base_url(); ?>index.php/ARF/ARFController/SubmitEditARFUser/<?php echo $kode;  ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
						<fieldset>
							<legend>Data ARF</legend>
							<table border=0>
								<tbody>
									<tr>
										<td font-size=13>Subject</td>
										<td><input id="txtSubject" size="100" type="text" name="txtSubject" value="<?php  echo $subject;  ?>" ><?php echo form_error('txtSubject'); ?></td>
									</tr>
									<tr>
										<td>Application</td>
										<td>
	                    					<?php
	                    					$selectCore="";
	                    					$selectSiap="";
	                    					$selectMobileBB="";
	                    					$selectAccpac="";
	                    					$selectSidapen="";
	                    					$selectSidp="";
	                    					$selectApisoft="";
	                    					$selectIVR="";
	                    					$selectWeb="";
	                    					$selectVinno="";
	                    					$selectEss="";
	                    					$selectLainnya="";

	                    					switch($application){
	                    						case "Core" : $selectCore="selected";
	                    						break;
	                    						case "Siap" : $selectSiap="selected";
	                    						break;
	                    						case "DPA Mobile/BB" : $selectMobileBB="selected";
	                    						break;
	                    						case "Accpac" : $selectAccpac="selected";
	                    						break;
	                    						case "siDapen DPA1" : $selectSidapen="selected";
	                    						break;
	                    						case "SIDP" : $selectSidp="selected";
	                    						break;
	                    						case "Apisoft" : $selectApisoft="selected";
	                    						break;
	                    						case "IVR" : $selectIVR="selected";
	                    						break;
	                    						case "Web Dapenastra" : $selectWeb="selected";
	                    						break;
	                    						case "Vinno SMS" : $selectVinno="selected";
	                    						break;
	                    						case "ESS" : $selectEss="selected";
	                    						break;
	                    						case "Lainnya" : $selectLainnya="selected";
	                    						break;
	                    					}
	                    					?>

											<select name="cmbAplikasi">
												<option value=''></option>
												<option value='Core' <?php echo $selectCore; ?> >Core</option>
												<option value='Siap' <?php echo $selectSiap; ?> >Siap</option>
												<option value='DPA Mobile/BB' <?php echo $selectMobileBB; ?> >DPA Mobile/BB</option>
												<option value='Accpac' <?php echo $selectAccpac; ?> >Accpac</option>
												<option value='siDapen DPA1' <?php echo $selectSidapen; ?> >siDapen DPA1</option>
												<option value='SIDP' <?php echo $selectSidp; ?> >SIDP</option>
												<option value='Apisoft' <?php echo $selectApisoft; ?> >Apisoft</option>
												<option value='IVR' <?php echo $selectIVR; ?> >IVR</option>
												<option value='Web Dapenastra' <?php echo $selectWeb; ?> >Web Dapenastra</option>
												<option value='Vinno SMS' <?php echo $selectVinno; ?> >Vinno SMS</option>
												<option value='ESS' <?php echo $selectEss; ?> >ESS</option>
												<option value='Lainnya' <?php echo $selectLainnya; ?> >Lainnya</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Request</td>
										<td><textarea id="txtReq" class="form-control" name='txtReq' type="text" cols="60" rows="3" ><?php if(set_value('txtReq')!=''){ echo set_value('txtReq'); }else { echo $request; } ?></textarea><?php echo form_error('txtReq'); ?>
										</td>
									</tr>
								</tbody>
							</table>
						</fieldset>
						<table class="tblPureNoBorder" style="float:right;">
							<tr>
								<td>
								<br/><input class="btn btn-primary" type="submit" onClick="return confirm('Apakah Anda yakin ingin mengedit data ini ke dalam database?')" id="btnSubmit" name="submitForm" value="Edit dan Cetak">
								</td>
							</tr>
						</table>
					</form>
				</font>
			</div>
		</div>
	</body>
</html>