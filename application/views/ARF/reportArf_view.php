<html>
<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <script src="http://www.datejs.com/build/date.js" type="text/javascript"></script>
    <script type="text/javascript">
        function alertsizeReportARF(pixels){
            pixels+=60;
            document.getElementById('iFrameReportARF').style.height=pixels+"px";
        }

        function search(){
            var npk = '<?php echo $npk; ?>';
            //var ya = document.getElementById('cmbTahun').value;
            //var tahun= ya.toString("yyyy");

            var ada = document.getElementById('txtDari').value;
            var dari = ada.toString("yyyy-mm-dd");

            var aja = document.getElementById('txtSampai').value;
            var sampai = aja.toString("yyyy-mm-dd");
            
            //alert(tahun);
            if (dari==='' || dari >=sampai){
                alert('Harap masukkan tanggal yang ingin dicari dengan benar!');
            }else {
                
                //location.href="../../ArfController/laporan/"+tahun;

                window.location.href = "<?php echo site_url('ARF/ArfController/laporan');?>/"+dari+"/"+sampai;
            }
        }

        function newWindow(kode){
            window.open('..\\..\\..\\Arf\\viewAdmin\\read\\'+kode, "", "width=900,height=600");            
        }

        
    </script>
    

    
    <head>
        <title><?php $title ?></title>
        <!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
        <style>
            td {padding:5px 5px 5px 5px;}
        </style>
    </head>
    <body>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Report ARF
                </h3>
            </div>
            <div class="panel-body">
               
                <table border=0>
                   <tr>
                        <td>Dari Tanggal</td>
                        <td>
                            <input class="form-control" id="txtDari" name="txtDari" type="date" size="10" value="">
                        </td>
                    </tr>
                    <tr>
                        <td>Sampai Tanggal</td>
                        <td>
                            <input class="form-control" id="txtSampai" name="txtSampai" type="date" size="10" value="">
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari">
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>Report ARF per tanggal <?php echo $dari; ?> hingga tanggal <?php echo $sampai; ?> <br/></td>
                    </tr>
                    
                </table>
            </div>
        </div>

        
<div id="highcharts-6971edc5-8c60-47cc-9553-26bb201c6867"></div>
<script>
(function(){ 
    var files = ["https://code.highcharts.com/stock/highstock.js","https://code.highcharts.com/highcharts-more.js","https://code.highcharts.com/highcharts-3d.js","https://code.highcharts.com/modules/data.js","https://code.highcharts.com/modules/exporting.js","https://code.highcharts.com/modules/funnel.js","https://code.highcharts.com/modules/annotations.js","https://code.highcharts.com/modules/solid-gauge.js"],loaded = 0; 

    if (typeof window["HighchartsEditor"] === "undefined") {
        window.HighchartsEditor = {
            ondone: [cl],hasWrapped: false,hasLoaded: false
        };
        include(files[0]);
    } else {
        if (window.HighchartsEditor.hasLoaded) {
            cl();
        } else {
            window.HighchartsEditor.ondone.push(cl);
        }
    }
    function isScriptAlreadyIncluded(src){
        var scripts = document.getElementsByTagName("script");
        for (var i = 0; i < scripts.length; i++) {
            if (scripts[i].hasAttribute("src")) {
                if ((scripts[i].getAttribute("src") || "").indexOf(src) >= 0 || (scripts[i].getAttribute("src") === "http://code.highcharts.com/highcharts.js" && src === "https://code.highcharts.com/stock/highstock.js")) {
                return true;
            }
        }
    }
    return false;
}
function check() {
    if (loaded === files.length) {
        for (var i = 0; i < window.HighchartsEditor.ondone.length; i++) {
            try {window.HighchartsEditor.ondone[i]();} catch(e) {
                console.error(e);}}window.HighchartsEditor.hasLoaded = true;
            }
        }

        function include(script) {
            function next() {
                ++loaded;
                if (loaded < files.length) {
                    include(files[loaded]);
                }check();
            }if (isScriptAlreadyIncluded(script)) {
                return next();
            }
            var sc=document.createElement("script");sc.src = script;sc.type="text/javascript";sc.onload=function() { 
                next(); 
            };
            document.head.appendChild(sc);
        }
        function each(a, fn){
            if (typeof a.forEach !== "undefined"){
                a.forEach(fn);
            }else{
                for (var i = 0; i < a.length; i++){if (fn) {
                    fn(a[i]);
                }
            }
        }
    }
    var inc = {},incl=[]; each(document.querySelectorAll("script"), function(t) {
        inc[t.src.substr(0, t.src.indexOf("?"))] = 1; 
    }); 
    function cl() {
        if(typeof window["Highcharts"] !== "undefined"){var options={
            "series":[{
                "name":"Jane","turboThreshold":0,"_colorIndex":0
            },{
                "name":"John","turboThreshold":0,"_colorIndex":5
            }],"xAxis":[{"type":"category","uniqueNames":false
        }],
        "data":{
            "csv":"\"Category\";\"Solved\";\"YTD\"\n\"MRC\";<?php echo $mrc1 ?>;<?php echo $mrc2 ?>\n\"Fin & Invest\";<?php echo $fin1 ?>;<?php echo $fin2 ?>\n\"ACC\";<?php echo $acc1 ?>;<?php echo $acc2 ?>\n\"IT\";<?php echo $it1 ?>;<?php echo $it2 ?>\n\"HRGA\";<?php echo $hr1 ?>;<?php echo $hr2 ?>\n\"Claim & Actuary\";<?php echo $ca1 ?>;<?php echo $ca2 ?>\n\"Corporate Secretary\";<?php echo $cse1 ?>;<?php echo $cse2 ?>\n\"Total\";<?php echo $jumlah?>;<?php echo $ytd?>","googleSpreadsheetKey":false,"googleSpreadsheetWorksheet":false
        },
        "chart":{"type":"column"},
        "title":{"text":"ARF Departemen <?php echo $tahun?> "},
         "yAxis":[{
            "allowDecimals":false,"title":{
                "text":""
            }
        }],
        "tooltip":{},
            "plotOptions":{
                "column":{
                    "dataLabels":{
                            "enabled":true,"color":"grey"
                    }
                },
                "series":{
                    "animation":true
                }
            },"subtitle":{}
        };/*
// Sample of extending options:
Highcharts.merge(true, options, {
    chart: {
        backgroundColor: "#bada55"
    },
    plotOptions: {
        series: {
            cursor: "pointer",
            events: {
                click: function(event) {
                    alert(this.name + " clicked\n" +
                          "Alt: " + event.altKey + "\n" +
                          "Control: " + event.ctrlKey + "\n" +
                          "Shift: " + event.shiftKey + "\n");
                }
            }
        }
    }
});
*/new Highcharts.Chart("highcharts-6971edc5-8c60-47cc-9553-26bb201c6867", options);}}})();
</script>




<div id="highcharts-b2ff9b8c-e42f-472f-8f82-54548d0e621d"></div><div></div>
<script>
(function(){
    var files = ["https://code.highcharts.com/stock/highstock.js","https://code.highcharts.com/highcharts-more.js","https://code.highcharts.com/highcharts-3d.js","https://code.highcharts.com/modules/data.js","https://code.highcharts.com/modules/exporting.js","https://code.highcharts.com/modules/funnel.js","https://code.highcharts.com/modules/annotations.js","https://code.highcharts.com/modules/solid-gauge.js"],loaded = 0; 
    if (typeof window["HighchartsEditor"] === "undefined") {
        window.HighchartsEditor = {
            ondone: [cl],hasWrapped: false,hasLoaded: false
        };
        include(files[0])
    ;} 
    else {
        if (window.HighchartsEditor.hasLoaded) {
            cl();
        } else {
            window.HighchartsEditor.ondone.push(cl);
        }
    }function isScriptAlreadyIncluded(src){
        var scripts = document.getElementsByTagName("script");for (var i = 0; i < scripts.length; i++) {
            if (scripts[i].hasAttribute("src")) {
                if ((scripts[i].getAttribute("src") || "").indexOf(src) >= 0 || (scripts[i].getAttribute("src") === "http://code.highcharts.com/highcharts.js" && src === "https://code.highcharts.com/stock/highstock.js")) {return true;
                }
            }
        }return false;
    }function check() {
        if (loaded === files.length) {
            for (var i = 0; i < window.HighchartsEditor.ondone.length; i++) {
                try {window.HighchartsEditor.ondone[i]();} catch(e) {console.error(e);
                }
            }window.HighchartsEditor.hasLoaded = true;
        }
    }function include(script) {
        function next() {
            ++loaded;if (loaded < files.length) {
                include(files[loaded]);
            }check();
        }if (isScriptAlreadyIncluded(script)) {
            return next();
        }var sc=document.createElement("script");sc.src = script;sc.type="text/javascript";sc.onload=function() {
            next(); 
        };
        document.head.appendChild(sc);
    }function each(a, fn){
        if (typeof a.forEach !== "undefined"){
            a.forEach(fn);
        }else{
            for (var i = 0; i < a.length; i++){
                if (fn) {
                    fn(a[i]);
                }
            }
        }
    }var inc = {},incl=[]; each(document.querySelectorAll("script"), function(t) {inc[t.src.substr(0, t.src.indexOf("?"))] = 1; });
    function cl() {
        if(typeof window["Highcharts"] !== "undefined"){var options={
            "chart":{"type":"column"},"title":{"text":"Detail ARF"
        },"xAxis":[{
            "categories":["MRC","Fin & Invest","ACC","IT","HRGA", "Claim & Actuary","Corporate Secretary","Total"]
        }],"yAxis":[{
            "min":0,"title":{
                "text":""
            },"stackLabels":{
                    "enabled":true,"style":{
                        "fontWeight":"bold","color":"gray"}
                    }
                }],"legend":{
                    "align":"left","x":30,"verticalAlign":"top","y":25,"floating":true,"backgroundColor":"#F0F0EA","borderColor":"#CCC","borderWidth":0,"shadow":false
                },"tooltip":{
                    "headerFormat":"<b>{point.x}</b><br/>","pointFormat":"{series.name}: {point.y}<br/>Total: {point.stackTotal}"
                },"plotOptions":{
                    "column":{
                        "stacking":"normal","dataLabels":{
                            "enabled":true,"color":"black"
                        }
                    },"series":{"animation":true
                }
            },"series":[{
                "name":"John","turboThreshold":0,"_colorIndex":5
            },{
                "name":"Jane","turboThreshold":0,"_colorIndex":3
            },{
                "name":"Joe","turboThreshold":0,"_colorIndex":4
            },{
                "name":"Joe","turboThreshold":0,"_colorIndex":2
            }],"data":{
                    "csv":"\"Category\";\"BPA/Pending\";\"Development\";\"UAT\";\"Solved\"\n\"MRC\";<?php echo $mrcbpa?>;<?php echo $mrcdev?>;<?php echo $mrcuat?>;<?php echo $mrc1?>\n\"Fin & Invest\";<?php echo $finbpa?>;<?php echo $findev?>;<?php echo $finuat?>;<?php echo $fin1?>\n\"ACC\";<?php echo $accbpa?>;<?php echo $accdev?>;<?php echo $accuat?>;<?php echo $acc1?>\n\"IT\";<?php echo $itbpa?>;<?php echo $itdev?>;<?php echo $ituat?>;<?php echo $it1?>\n\"HRGA\";<?php echo $hrbpa?>;<?php echo $hrdev?>;<?php echo $hruat?>;<?php echo $hr1?>\n\"Claim & Actuary\";<?php echo $cabpa?>;<?php echo $cadev?>;<?php echo $cauat?>;<?php echo $ca1?>\n\"Corporate Secretary\";<?php echo $csebpa?>;<?php echo $csedev?>;<?php echo $cseuat?>;<?php echo $cse1?>\n\"Total\";<?php echo $jumlahbpa?>;<?php echo $jumlahdev?>;<?php echo $jumlahuat?>;<?php echo $jumlah?>","googleSpreadsheetKey":false,"googleSpreadsheetWorksheet":false
                }
            };
            new Highcharts.Chart("highcharts-b2ff9b8c-e42f-472f-8f82-54548d0e621d", options);
        }
    }
})();
</script>
        
        <div class="box box-danger box-solid" style="display: block;">
            <div class="box-header with-border">
            <h3 class="box-title">List ARF Pending/BPA</h3>            
            </div>
            <div class="box-body">
                <table class="table">
                    <tr style='background-color: #FF7F50'>
                        <th>No</th>
                        <th>No ARF</th>
                        <th>User</th>
                        <th>Request</th>
                        <th>Start Work Plan</th>
                        <th>Target Plan</th>
                        <th>Resolver</th>
                    </tr>
                    <?php
                     $no=0;
                    foreach ($data1 as $row) {
                   
                    $no++;
                    echo "
                    <tr >
                        <td>$no</td>
                        <td><a href='#' onClick='newWindow(".$row->KodeARF.")'/>".$row->NoARF."</td>
                        <td>".$row->namauser."</td>
                        <td>".$row->Subject."</td>
                        <td>".$row->StartWorkDate."</td>
                        <td>".$row->TargetFinishDate."</td>
                        <td>".$row->resolver."</td>
                    </tr>
                    ";
                    
                    }
                    ?>
                </table>
            </div>
        </div>
        

        
        <div class="box box-warning box-solid" style="display: block;">
            <div class="box-header with-border">
            <h3 class="box-title">List ARF Development</h3>            
            </div>
            <div class="box-body">
                <table class="table">
                    <tr style="background-color: #FFE4B5">
                        <th>No</th>
                        <th>No ARF</th>
                        <th>User</th>
                        <th>Request</th>
                        <th>Start Work Plan</th>
                        <th>Target Plan</th>
                        <th>Resolver</th>
                    </tr>
                    
                    <?php
                    $no=1;
                    foreach ($data2 as $row) {
                    echo "
                    <tr >
                        <td>$no</td>
                        <td><a href='#' onClick='newWindow(".$row->KodeARF.")'/>".$row->NoARF."</td>
                        <td>".$row->namauser."</td>
                        <td>".$row->Subject."</td>
                        <td>".$row->StartWorkDate."</td>
                        <td>".$row->TargetFinishDate."</td>
                        <td>".$row->resolver."</td>
                    </tr>
                    ";
                    $no++;
                    }
                    ?>
                    
                </table>
            </div>
        </div>
        
        <div class="box  box-success box-solid" >
            <div class="box-header with-border">
            <h3 class="box-title">List ARF UAT</h3>            
            </div>
            <div class="box-body">
                <table class="table">
                    <tr style='background-color: #B0E0E6'>
                        <th>No</th>
                        <th>No ARF</th>
                        <th>User</th>
                        <th>Request</th>
                        <th>Finish Date</th>
                        <th>UAT Date</th>
                        <th>Resolver</th>
                    </tr>
                    
                        <?php
                        $no=1;
                        foreach ($data3 as $row) {
                        echo "
                        <tr >
                        <td>$no</td>
                        <td><a href='#' onClick='newWindow(".$row->KodeARF.")'/>".$row->NoARF."</td>
                        <td>".$row->namauser."</td>
                        <td>".$row->Subject."</td>
                        <td>".$row->FinishDate."</td>
                        <td>".$row->UATDate."</td>
                        <td>".$row->resolver."</td>
                        </tr>
                        ";
                        $no++;
                    }
                        ?>
                </table>
            </div>
        </div>
        
        <div class="box box-success box-solid" style="display: block;">
            <div class="box-header with-border">
            <h3 class="box-title">List ARF Implementasi</h3>            
            </div>
            <div class="box-body">
                <table class="table">
                    <tr style='background-color: #3CB371'>
                        <th>No</th>
                        <th>No ARF</th>
                        <th>User</th>
                        <th>Request</th>
                        <th>UAT Date</th>
                        <th>Implementation Date</th>
                        <th>Resolver</th>
                    </tr>
                    <?php
                    $no=1;
                    foreach ($data4 as $row) {
                    echo "
                    <tr >
                        <td>$no</td>
                        <td><a href='#' onClick='newWindow(".$row->KodeARF.")'/>".$row->NoARF."</td>
                        <td>".$row->namauser."</td>
                        <td>".$row->Subject."</td>
                        <td>".$row->UATDate."</td>
                        <td>".$row->ImplementationDate."</td>
                        <td>".$row->resolver."</td>
                    </tr>
                    ";
                    $no++;
                }
                ?>
                </table>
            </div>
        </div>
        <section class="content">
            <div class="callout callout-info">
                Total ARF sudah Implementasi per tahun <?php echo $tahun?> = <?php echo $jumlah?> ARF
            </div>
            
        </section> 
    </body>
</html>