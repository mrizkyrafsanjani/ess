<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript">

		$(function() {
			
			$('#buttonKirim').click(function(){
				if($('#txtFeedback').val()==""){
					$('#divError').html('Harap isi feedback terlebih dahulu dahulu');
				}else{
					$('#btnKirim')
					$('#divError').html("<img src='<?php echo $this->config->base_url(); ?>assets/images/ProgressAnim.gif'> <font size='2px'>...sedang proses mengirim...</font>");
					$.ajax({
					  type: "POST",
					  url: "<?php echo $this->config->base_url(); ?>index.php/submitFeedback",
					  data: { message: $('#txtFeedback').val() }
					}).done(function( msg ) {
						$('#divError').html(msg);
						setTimeout("parent.$.fancybox.close()", 1000);
					});
				}
			});
			
		});

	</script>
	<head>
		<title>Feedback</title>
	</head>
	<body>
		<form action="submitFeedback">
Tulis komentar Anda di bawah ini : <br/>
		<textarea id="txtFeedback" name="txtFeedback" rows="18" cols="110"></textarea><br/>
		<input class="buttonSubmitBebas" id="buttonKirim" type="button" value="Kirim">
		</form><div id="divError"></div>
	</body>
</html>