<html>
	<style type="text/css">
	@import "../assets/css/jquery-ori.datepick.css";
	@import "../assets/css/cetak.css";
	</style>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>/plugins/datepicker/bootstrap-datepicker.css">
	<!--<script type="text/javascript" src="../assets/js/jquery.js"></script>-->
	<script type="text/javascript" src="../assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="../assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="../assets/js/myfunction.js?v=2.17"></script>
	
	<head>
		<title>Laporan SPD</title>
	<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		</head>
	<body>
	<?php echo validation_errors(); ?>
	<?php if($UangMuka<>0 || $TanggalTerima<>0){
	if ( $TglTerimaPermohonan_array['TglTerimaHRGAPermohonan']<>'' && $TglTerimaPermohonan_array['TglTerimaFinancePermohonan']<>'' && $TglTerimaPermohonan_array['TglBayarFinancePermohonan']<>'' ) {?> 
	<form action="submitLaporanSPD" method="post" accept-charset="utf-8">
<table id="tablelaporan" border=0>
   <tbody>
	<!-- Results table headers -->
	<tr  class="tblHeader">
	  <td colspan="2">Laporan Perjalanan Dinas</td>
          <td colspan="3"><image id="logoDPA" src='../assets/images/logoDPA.png'>*DANA PENSIUN ASTRA<br/>
		  <?php 
			$checked1 = "";
			$checked2 = "";
			if($trkSPD_array[0]['DPA'] == 1){
				$checked1 = "checked";
			}else{
				$checked2 = "checked";
			}
		  ?>
			    <input type="radio" id="radbDpaSatu" name="radbDPA" value="1" <?php echo $checked1; ?> disabled="disabled" hidden="hidden">
          <input type="radio" id="radbDpaDua" name="radbDPA" value="2" <?php echo $checked2; ?> disabled="disabled" hidden="hidden">
          <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked1; ?> disabled="disabled" >SATU
          <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked2; ?> disabled="disabled" >DUA
		  
		  <input class="clsUangMuka" type="radio" id="rbYa" name="rbUangMuka" value="1" checked hidden>
          </td>
	</tr>
	<tr>
	  <td class="lbl">No SPD</td>
	  <td><input id="txtNoSPD" class='bordered' name='txtNoSPD' type="text" size="30" value="<?php echo $trkSPD_array[0]['nospd']; ?>" readonly="true"></td>
		<td id="UM"><input id="txtUangMuka" class='bordered' name='txtUangMuka' type="text" size="30" value="<?php echo $UangMuka ?>" readonly="true"></td>
	  <td colspan="3" rowspan="10" align="center">
		<div id="catatan">
			<div class="lblTop">Catatan Penting</div>
				<div id="txtCatatan"><ol><li>Pertanggungjawaban atas uang muka untuk biaya perjalanan dinas wajib dilaporkan selambat-lambatnya 3 (tiga) hari setelah tiba di tempat kerja.</li>
<li>Pengembalian kelebihan ataupun kekurangan uang muka diterima/diserahkan 2 hari kerja setelah dokumen lengkap dan disetujui oleh HRD.</li>
<li>Semua bukti pengeluaran harus dilampirkan:</li>
</ol>
				<ul>
					<li>-Invoice Biaya penginapan (Hotel) Asli</li>
					<li>-Airport tax</li>
					<li>-Kwitansi deklarasi Taksi</li>
					<li>-dll (Berkas yang terkait)</li>
				</ul>
				</div>
		</div>
      </td>
	</tr>
	<tr>
	  <td class="lbl">Tanggal Laporan</td>
	  <td><?php echo date("j F Y"); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">NPK</td>
	  <td><?php echo $trkSPD_array[0]['npk']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Golongan</td>
	  <td><?php echo $trkSPD_array[0]['golongan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Nama</td>
	  <td><?php echo $trkSPD_array[0]['nama']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Jabatan</td>
	  <td><?php echo $trkSPD_array[0]['jabatan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Departemen</td>
	  <td><?php echo $trkSPD_array[0]['departemen']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Atasan</td>
	  <td><?php echo $trkSPD_array[0]['atasan']; ?></td>
	  
	</tr>
	<tr>
	<td class="lbl">*Tanggal Keberangkatan</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
	  <input id="txtTanggalBerangkat" class="form-control pull-right" name='txtTanggalBerangkat'  type="text" size="7" value="<?php echo $trkSPD_array[0]['tanggalberangkatspd']; ?>" >
	  </div>
	</tr>
	<tr id="alasanextend">
			<td class="lbl">*Alasan Extend</td>
			<td > <select  style="width:350px" id="txtAlasanExtend" name="txtAlasanExtend">
						<option value ='Cuti'>CUTI</option> 
            <option value ='ExtendTugas'>EXTEND TUGAS</option>      
            </select>
            </td>	
		</tr>
	<tr id="tanggalkembali">
	  <td class="lbl" id="lblkembali">*Tanggal Kembali</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembali" name='txtTanggalKembali' value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>">
      </div>
		
		</td>
	</tr>

	<tr id="hidden">
	  <td class="lbl" id="lblkembali">*Tanggal Kembali</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembaliPermohonan" name='txtTanggalKembaliPermohonan' value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>">
      </div>
		
		</td>
	</tr>
	<tr id="hidden2">
		<td></td>
		<td><input type="text" id="txtKegiatan" name='txtTanggalKembaliPermohonan' value="<?php echo $joinUangMuka_array['JenisKegiatanUangMuka']; ?>"></td>
		</tr>
		<tr id="hidden3">
	  <td class="lbl" id="lblkembali">*Tanggal Kembali Kantor 1</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembaliKantor1" name='txtTanggalKembaliKantor1' value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>">
      </div>
		
		</td>
	</tr>
	<tr id="hidden4">
	  <td class="lbl" id="lblkembali">*Tanggal Kembali Permohonan 1</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembaliPermohonan1" name='txtTanggalKembaliPermohonan1' value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>">
      </div>
		
		</td>
	</tr>
	<tr id="tanggalkembalikantor" id="lblkembalikantor">
	  <td class="lbl">*Tanggal Kembali Kekantor</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembaliKantor" name='txtTanggalKembaliKantor' value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>">
      </div>

		</td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tujuan</td>
	  <td colspan="4"><input class='bordered' id="txtTujuan" name='txtTujuan' type="text" value="<?php echo $trkSPD_array[0]['tujuan']; ?>" readonly></td>
	</tr>
	<tr>
	  <td class="lbl">Alasan Perjalanan</td>
	  <td colspan="4" valign="top">  <textarea id="txtAlasan" class='bordered' name='txtAlasan' type="text" cols="60" rows="3" readonly ><?php echo $trkSPD_array[0]['alasan']; ?></textarea></td>
	</tr>
	<tr>
                <td class="lbl">Outstanding Uang Muka</td>
                <td><?php 
                        if($dataOutstanding!='0')
                        {  echo "Ada"; 
                           echo "<input id='txtOutstanding'  name='txtOutstanding' type='hidden' value='Ada' >";
                        }
                        else
                        {  echo "Tidak Ada";
                           echo "<input id='txtOutstanding'  name='txtOutstanding' type='hidden' value='TidakAda' >";
                        }
                    ?></td>	  
        </tr>
    </tbody>
    </table>
	
<br/>
<!-- part untuk perkiraaan biaya -->
<table border=0>
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Perkiraan Biaya
	  <input class="clsUangMuka" type="radio" id="rbInternasional" name="rbRegion" value="1">Internasional
		<input class="clsUangMuka" type="radio" id="rbNasional" name="rbRegion" value="0">Nasional
	  </td>
    </tr>
    <tr>
      <td class="lbl" width="170px">Jenis Biaya</td>
      <td id="keterangan" class="lblTop">Keterangan</td>
      <td class="lblTop">Beban Harian</td>
      <td class="lblTop">#Jml Hari</td>
      <td class="lblTop">Total Perkiraan Biaya</td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Saku</td>
      <td><input id="txtKetUangSaku" class='bordered' name='txtKetUangSaku' type="text" size="40" value="<?php echo $trkSPD_array[0]['keteranganspd']; ?>" ></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtBebanHarianUangSaku" name='txtBebanHarianUangSaku' type="text" size="15" value="<?php 
		if($trkSPD_array[0]['uangmuka'] == 0){
			echo $dataUser['uangsaku'];
		}else{
			echo $trkSPD_array[0]['bebanharianspd']; 
		}
	?>"   readonly></td>
      <td class='clsHari'bgcolor="#D3D3D3" ><input class='defult' id="txtJmlhHariUangSaku" name='txtJmlhHariUangSaku' type="text" size="5" value="<?php echo $trkSPD_array[0]['jumlahharispd']; ?>" ></td>
      <td class='clsUang'bgcolor="#D3D3D3" ><input class='defult' id="txtTotalUangSaku" name='txtTotalUangSaku' type="text" size="15" value="<?php echo $trkSPD_array[0]['bebanharianspd']*$trkSPD_array[0]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Makan</td>
      <td><input id="txtKetUangMakan" class='bordered' name='txtKetUangMakan' type="text" size="40" value="<?php echo $trkSPD_array[1]['keteranganspd']; ?>" ></td>
	  <?php 
		$readonly = "";
		if($trkSPD_array[0]['golongan'] <= 5){
			$readonly = "readonly";
		}
	  ?>
      <td class='clsUang' bgcolor="#D3D3D3"><input class='defult'  id="txtBebanHarianUangMakan" name='txtBebanHarianUangMakan' type="text" size="15" value="<?php 
		if($trkSPD_array[0]['uangmuka'] == 0){
			echo $dataUser['uangmakan'];
		}else{
			echo $trkSPD_array[1]['bebanharianspd']; 
		}		
	?>"  <?php echo $readonly; ?> ></td>
      <td class='clsHari'><input class='bordered' id="txtJmlhHariUangMakan" name='txtJmlhHariUangMakan' type="text" size="5" value="<?php echo $trkSPD_array[1]['jumlahharispd']; ?>" >
			<input class='bordered' id="txtJmlhHariUangMakanDb" name='txtJmlhHariUangMakanDb' type="hidden" size="5" value="<?php echo $trkSPD_array[1]['jumlahharispd']; ?>"></td>
      <td class='clsUang'><input class='bordered'  id="txtTotalUangMakan" name='txtTotalUangMakan' type="text" size="15" value="<?php echo $trkSPD_array[1]['bebanharianspd']*$trkSPD_array[1]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;*Akomodasi</td>
      <td><input id="txtKetHotel" class='bordered' name='txtKetHotel' type="text" size="40" value="<?php echo $trkSPD_array[2]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='bordered' id="txtBebanHarianHotel" name='txtBebanHarianHotel' type="text" size="15" value="<?php echo $trkSPD_array[2]['bebanharianspd']; ?>" ></td>
      <td class='clsHari'><input class='bordered' id="txtJmlhHariHotelLap" name="txtJmlhHariHotelLap" type="text" size="5" value="<?php echo $trkSPD_array[2]['jumlahharispd']; ?>" ></td>
      <td class='clsUang' ><input class='bordered' id="txtTotalHotelLap" type="text" size="15" value="<?php echo $trkSPD_array[2]['bebanharianspd']*$trkSPD_array[2]['jumlahharispd']; ?>" ></td>
    </tr>
	<tr class='clsInputNumber'>
      <td class="lbl"></td>
      <td><input id="txtKetHotel2" class='bordered' name="txtKetHotel2" type="text" size="40" value="" ></td>
      <td class='clsUang'><input class='bordered'  id="txtBebanHarianHotel2" name="txtBebanHarianHotel2" type="text" size="15" value="" ></td>
      <td class='clsHari'><input class='bordered' id="txtJmlhHariHotel2" name="txtJmlhHariHotel2" type="text" size="5" value="" ></td>
      <td class='clsUang'><input class='bordered'  id="txtTotalHotel2" name="txtTotalHotel2" type="text" size="15" value="" ></td>
    </tr>
    <!--<tr>
      <td><br/></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>-->
    <tr>
      <td class="lbl"><i>Transportasi</i></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr class='clsInputNumber'>
      <td  class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Taksi</td>
      <td><input id="txtKetTaksi" class='bordered' name='txtKetTaksi' type="text" size="40" value="<?php echo $trkSPD_array[4]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='bordered'  id="txtBebanHarianTaksi" name='txtBebanHarianTaksi' type="text" size="15" value="<?php echo $trkSPD_array[4]['bebanharianspd']; ?>" ></td>
      <td></td>
      <td class='clsUang'><input class='bordered'  id="txtTotalTaksi" name='txtTotalTaksi' type="text" size="15" value="<?php echo $trkSPD_array[4]['bebanharianspd']*$trkSPD_array[4]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Airport Tax</td>
      <td><input id="txtKetAirport" class='bordered' name='txtKetAirport' type="text" size="40" value="<?php echo $trkSPD_array[5]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='bordered'  id="txtBebanHarianAirport" name='txtBebanHarianAirport' type="text" size="15" value="<?php echo $trkSPD_array[5]['bebanharianspd']; ?>" ></td>
      <td></td></td>
      <td class='clsUang'><input class='bordered'  id="txtTotalAirport" name='txtTotalAirport' type="text" size="15" value="<?php echo $trkSPD_array[5]['bebanharianspd']*$trkSPD_array[5]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl"><i>Lain-lain</i></td>
	  <?php 
		$counter = 1;
		for($i=6;$i<count($trkSPD_array);$i++){
			echo "<td><input class='bordered' id=\"txtKetLain".$counter."\" name='txtKetLain".$counter."' type=\"text\" size=\"40\" value='". $trkSPD_array[$i]['keteranganspd'] ."' ></td>
			  <td class='clsUang'><input class='bordered' id=\"txtBebanHarianLain".$counter."\" name='txtBebanHarianLain".$counter."' type=\"text\" size=\"15\" value='". $trkSPD_array[$i]['bebanharianspd'] ."' ></td>
			  <td></td>
			  <td class='clsUang'><input class='bordered' id=\"txtTotalLain".$counter."\" name='txtTotalLain".$counter."' type=\"text\" size=\"15\" value='" . $trkSPD_array[$i]['bebanharianspd']*$trkSPD_array[$i]['jumlahharispd'] . "' ></td>
			</tr>
			<tr class='clsInputNumber'>
				<td></td>";
			$counter++;
		}
		$counter = 1;
		if(count($trkSPD_array)==6){
			for($i=1;$i<=3;$i++){
				echo "<td><input id=\"txtKetLain".$counter."\" name='txtKetLain".$counter."' type=\"text\" size=\"40\" ></td>
				  <td class='clsUang'><input class='clsBebanHarian' id=\"txtBebanHarianLain".$counter."\" name='txtBebanHarianLain".$counter."' type=\"text\" size=\"15\" ></td>
				  <td></td>
				  <td class='clsUang'><input class='clsTotal' id=\"txtTotalLain".$counter."\" name='txtTotalLain".$counter."' type=\"text\" size=\"15\" value='' ></td>
				</tr>
				<tr class='clsInputNumber'>
					<td></td>";
				$counter++;
			}
		}
	  ?>
      <td></td>
      <td class="lbl" colspan="2"><b>Grand Total</b></td>
      <td class='clsUang' bgcolor="#D3D3D3"><input class='defult' id="txtGrandTotal" name='txtGrandTotal' type="text" size="15" ></td>
    </tr>
	

  </tbody>
</table>
<br/>

<!--realisasi uang muka-->
<table bordered="0">
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Realisasi Uang Muka</td>
		</tr>
		
		<tr>
			<td>No Permohonan Uang Muka</td><td><?php echo $joinUangMuka_array['Nouangmuka']; ?></td>
		</tr>
		<tr>
			<td>Keterangan</td><td><?php echo $joinUangMuka_array['KeteranganPermohonan']; ?></td>
		</tr>
		<tr>
			<td>No Persetujuan Pengeluaran</td><td><?php echo $joinUangMuka_array['noPP']; ?></td>
		</tr>
		<tr>
			<td>Tanggal Permohonan Uang Muka</td><td><?php echo $joinUangMuka_array['TanggalPermohonan']; ?></td>
		</tr>
		<tr>
			<td>Tanggal Realisasi Uang Muka</td><td><?php echo date("Y-m-d"); ?></td>
		</tr>
		<tr>
			<td>Paling Lambat Waktu Penyelesaian</td>
			<td><input type="text" class='bordered' id="lblWaktuPenyelesaian" name="lblWaktuPenyelesaian" value="<?php echo $joinUangMuka_array['WaktuPenyelesaianTerlambat']; ?>" readonly></td>
		</tr>

      <td class="lbl">Total Pengajuan Uang Muka</td>
      <td colspan="4">Rp. <input type="text" class='bordered' id="lblTotalPengajuanUangMuka" value="<?php	
		if ($trkSPD_array[0]['uangmuka'] == 0){
			echo "0";
		}else{
			echo $trkSPD_array[0]['totalSPD']; 
		}
	  ?>" readonly>
	  </td>
    </tr>
		<tr>
      <td class="lbl">Total Pengeluaran Perjalanan Dinas</td>
      <td colspan="4">Rp. <input type="text" class='bordered' id="lblTotalPengajuan" readonly></td>
    </tr>
    <tr>
	<tr>
      <td class="lbl"><label id="statusLebih"></label></td>
      <td colspan="4">Rp. <input type="text" class='bordered' id="lblSelisih" readonly></td>
    </tr>
			<tr>
      <td>Terbilang</td>
      <td colspan="4"><label id="lblTerbilang"></label></td>
    </tr>
	</tbody>
</table>

<br/>
<table id ="tablepembayaran" bordered="0">
  <tbody>
		<tr>
			<td colspan="2"><label id="psn"></label></td>
		</tr>
		<tr>
			<td >Bank</td><td>: <label id="nmbank"></label></td>
		</tr>
		<tr>
			<td>No Rekening</td><td>: <label id="norek"></label></td>
		</tr>
		<tr>
			<td>Nama Penerima</td><td>: <label id="nmpenerima"></label></td>
		</tr>
		
	</tbody>
</table>
<br/>

<table id ="tablepembayarankurang" bordered="0">
  <tbody>
		<tr>
			<td>Kekurangan Uang Muka harap di transfer ke</td>
		  <td><input type="text" id="txtPilihBayarKurang" name="txtPilihBayarKurang" value='TRANSFER' readonly></td>
		</tr>
		<tr>
			<td >Bank</td><td><input type="text" id="txtBankKurang" name="txtBankKurang"></td>
		</tr>
		<tr>
			<td>No Rekening</td><td><input type="number" id="txtNoRekKurang" name="txtNoRekKurang"></td>
		</tr>
		<tr>
			<td>Nama Penerima</td><td><input type="text" id="txtPenerimaKurang" name="txtPenerimaKurang"></td>
		</tr>
		
	</tbody>
</table>

<!-- tanda tangan -->
<table id="tblTandaTangan"  border=0>
  <tbody>
  
    <tr class="ttd">
		<td rowspan="2">Pemohon</td>
      <td colspan="3">Menyetujui</td>
    </tr>
    <tr class="ttd">
		
      <td><?php 
		if($trkSPD_array[0]['atasan']== $trkSPD_array[0]['nama'])
		{
			echo "DIC HRGA";
		}
		else if($trkSPD_array[0]['atasan']=='')
		{ 
			echo "Chief"; 
		}
		else
		{ 
			echo "Atasan"; 
		}
		?></td>
      <td>HRGA Dept Head</td>
			<?php if($flagOutStanding!='0' ){ echo "<td >DIC Accounting, Tax & Control</td>";} else  { echo "";}?>
    </tr>
    <tr class="ttd">
      <td><br/><br/><br/><br/><br/></td>
      <td></td>
      <td></td>
			<?php if($flagOutStanding!='0' ){ echo "<td ></td>";} else  { echo "";}?>
    </tr>
    <tr class="ttd">
      <td><?php echo $trkSPD_array[0]['nama']; ?></td>
      <td><?php echo $trkSPD_array[0]['atasan']==$trkSPD_array[0]['nama']?"":$trkSPD_array[0]['atasan']; ?></td>
      <td><?php echo $HeadHRGA ?></td>
			<?php if($flagOutStanding!='0' ){ echo "<td >".$DICACC."</td>";} else  { echo "";}?>
    </tr>
  </tbody>
</table>
<br>
<?php if($flagOutStanding=='1' ){  ?>
	Outstanding Uang Muka
	<table id="tblOutStanding" border=0>
		<tbody>
			<tr class="tblHeader">
				<td >No Permohonan Uang Muka</td>     
				<td >Keterangan</td> 
				<td >Nominal</td> 
				<td >Penyebab Outstanding</td>       
			</tr>
			<?php for($i=0;$i<count($dataOutstanding);$i++){		
				if($dataOutstanding[$i]['Total']<>'' or $dataOutstanding[$i]['Total']<>null or $dataOutstanding[$i]['Total']<>0){?>
				<tr>
					<td ><?php echo $dataOutstanding[$i]['NoUangMuka']; ?></td>
					<td><?php echo $dataOutstanding[$i]['KeteranganPermohonan']; ?></td>
					<td class='clsUang'><?php echo $dataOutstanding[$i]['Total']; ?></td>
					<td ><?php echo $dataOutstanding[$i]['ReasonOutstanding']; ?></td>
					</tr>
			<?php }else{?>
				<td ><?php echo $dataOutstanding[$i]['NoUangMuka']; ?></td>
					<td><?php echo $dataOutstanding[$i]['KeteranganPermohonan']; ?></td>
					<td class='clsUang'><?php echo $dataOutstanding[$i]['TotalSPD']; ?></td>
					<td ><?php echo $dataOutstanding[$i]['ReasonOutstanding']; ?></td>
					</tr>

			<?php }
			}?>   
            </tbody>
            </table>
            <?php }?>
<br>
<div id="note">
</div><br/>
<table class="tblPureNoBorder"><tr><td>
	<input class="btn btn-primary"  type="submit" onClick="return confirm('Apakah Anda yakin ingin menyimpan Laporan SPD ini ke dalam database?')" id="btnSubmit" name="submitLaporanSPD" value="Simpan">
	<a href="laporanSPD"><input class="btn btn-default"  type="button" value="Batal"></a>
	<div id="divError"></div>
</tr></td></table>
</form>



	<?php }else{
		echo "SPD BELUM DAPAT DI REALISASIKAN !";
	}
}else{?>
	<form action="submitLaporanSPD" method="post" accept-charset="utf-8">
<table id="tablelaporan" border=0>
   <tbody>
	<!-- Results table headers -->
	<tr  class="tblHeader">
	  <td colspan="2">Laporan Perjalanan Dinas</td>
          <td colspan="3"><image id="logoDPA" src='../assets/images/logoDPA.png'>*DANA PENSIUN ASTRA<br/>
		  <?php 
			$checked1 = "";
			$checked2 = "";
			if($trkSPD_array[0]['DPA'] == 1){
				$checked1 = "checked";
			}else{
				$checked2 = "checked";
			}
		  ?>
			    <input type="radio" id="radbDpaSatu" name="radbDPA" value="1" <?php echo $checked1; ?> disabled="disabled" hidden="hidden">
          <input type="radio" id="radbDpaDua" name="radbDPA" value="2" <?php echo $checked2; ?> disabled="disabled" hidden="hidden">
          <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked1; ?> disabled="disabled" >SATU
          <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked2; ?> disabled="disabled" >DUA
		  
		  <input class="clsUangMuka" type="radio" id="rbYa" name="rbUangMuka" value="1" checked hidden>
          </td>
	</tr>
	<tr>
	  <td class="lbl">No SPD</td>
	  <td><input id="txtNoSPD" class='bordered' name='txtNoSPD' type="text" size="30" value="<?php echo $trkSPD_array[0]['nospd']; ?>" readonly="true"></td>
	  <td id="UM"><input id="txtUangMuka" class='bordered' name='txtUangMuka' type="text" size="30" value="<?php echo $UangMuka ?>" readonly="true"></td>
	  <td colspan="3" rowspan="10" align="center">
		<div id="catatan">
			<div class="lblTop">Catatan Penting</div>
				<div id="txtCatatan"><ol><li>Pertanggungjawaban atas uang muka untuk biaya perjalanan dinas wajib dilaporkan selambat-lambatnya 3 (tiga) hari setelah tiba di tempat kerja.</li>
<li>Pengembalian kelebihan ataupun kekurangan uang muka diterima/diserahkan 2 hari kerja setelah dokumen lengkap dan disetujui oleh HRD.</li>
<li>Semua bukti pengeluaran harus dilampirkan:</li>
</ol>
				<ul>
					<li>-Invoice Biaya penginapan (Hotel) Asli</li>
					<li>-Airport tax</li>
					<li>-Kwitansi deklarasi Taksi</li>
					<li>-dll (Berkas yang terkait)</li>
				</ul>
				</div>
		</div>
      </td>
	</tr>
	<tr>
	  <td class="lbl">Tanggal Laporan</td>
	  <td><?php echo date("j F Y"); ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">NPK</td>
	  <td><?php echo $trkSPD_array[0]['npk']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Golongan</td>
	  <td><?php echo $trkSPD_array[0]['golongan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Nama</td>
	  <td><?php echo $trkSPD_array[0]['nama']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Jabatan</td>
	  <td><?php echo $trkSPD_array[0]['jabatan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Departemen</td>
	  <td><?php echo $trkSPD_array[0]['departemen']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Atasan</td>
	  <td><?php echo $trkSPD_array[0]['atasan']; ?></td>
	  
	</tr>
	<tr>
	<td class="lbl">*Tanggal Keberangkatan</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
	  <input id="txtTanggalBerangkat" class="form-control pull-right" name='txtTanggalBerangkat'  type="text" size="7" value="<?php echo $trkSPD_array[0]['tanggalberangkatspd']; ?>" >
	  </div>
	</tr>
	<tr id="alasanextend">
			<td class="lbl">*Alasan Extend</td>
			<td > <select  style="width:350px" id="txtAlasanExtend" name="txtAlasanExtend">
						<option value ='Cuti'>CUTI</option> 
            <option value ='ExtendTugas'>EXTEND TUGAS</option>      
            </select>
            </td>	
		</tr>
	<tr id="tanggalkembali">
	  <td class="lbl" id="lblkembali">*Tanggal Kembali</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembali" name='txtTanggalKembali' value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>">
      </div>
		
		</td>
	</tr>

	<tr id="hidden">
	  <td class="lbl" id="lblkembali">*Tanggal Kembali</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembaliPermohonan" name='txtTanggalKembaliPermohonan' value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>">
      </div>
		
		</td>
	</tr>
	<tr id="hidden2">
		<td></td>
		<td><input type="text" id="txtKegiatan" name='txtTanggalKembaliPermohonan' value="<?php echo $joinUangMuka_array['JenisKegiatanUangMuka']; ?>"></td>
		</tr>
		<tr id="hidden3">
	  <td class="lbl" id="lblkembali">*Tanggal Kembali Kantor 1</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembaliKantor1" name='txtTanggalKembaliKantor1' value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>">
      </div>
		
		</td>
	</tr>
	<tr id="hidden4">
	  <td class="lbl" id="lblkembali">*Tanggal Kembali Permohonan 1</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembaliPermohonan1" name='txtTanggalKembaliPermohonan1' value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>">
      </div>
		
		</td>
	</tr>
	<tr id="tanggalkembalikantor" id="lblkembalikantor">
	  <td class="lbl">*Tanggal Kembali Kekantor</td>
	  <td><div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="txtTanggalKembaliKantor" name='txtTanggalKembaliKantor' value="<?php echo $trkSPD_array[0]['tanggalkembalispd']; ?>">
      </div>

		</td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tujuan</td>
	  <td colspan="4"><input class='bordered' id="txtTujuan" name='txtTujuan' type="text" value="<?php echo $trkSPD_array[0]['tujuan']; ?>" readonly></td>
	</tr>
	<tr>
	  <td class="lbl">Alasan Perjalanan</td>
	  <td colspan="4" valign="top">  <textarea id="txtAlasan" class='bordered' name='txtAlasan' type="text" cols="60" rows="3" readonly ><?php echo $trkSPD_array[0]['alasan']; ?></textarea></td>
	</tr>
  </tbody>
</table>
<br/>
<!-- part untuk perkiraaan biaya -->
<table border=0>
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Perkiraan Biaya
	  <input class="clsUangMuka" type="radio" id="rbInternasional" name="rbRegion" value="1">Internasional
		<input class="clsUangMuka" type="radio" id="rbNasional" name="rbRegion" value="0">Nasional
	  </td>
    </tr>
    <tr>
      <td class="lbl" width="170px">Jenis Biaya</td>
      <td id="keterangan" class="lblTop">Keterangan</td>
      <td class="lblTop">Beban Harian</td>
      <td class="lblTop">#Jml Hari</td>
      <td class="lblTop">Total Perkiraan Biaya</td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Saku</td>
      <td><input id="txtKetUangSaku" class='bordered' name='txtKetUangSaku' type="text" size="40" value="<?php echo $trkSPD_array[0]['keteranganspd']; ?>" ></td>
      <td class='clsUang' bgcolor="#D3D3D3" ><input class='defult' id="txtBebanHarianUangSaku" name='txtBebanHarianUangSaku' type="text" size="15" value="<?php 
		if($trkSPD_array[0]['uangmuka'] == 0){
			echo $dataUser['uangsaku'];
		}else{
			echo $trkSPD_array[0]['bebanharianspd']; 
		}
	?>"   readonly></td>
      <td class='clsHari'bgcolor="#D3D3D3" ><input class='defult' id="txtJmlhHariUangSaku" name='txtJmlhHariUangSaku' type="text" size="5" value="<?php echo $trkSPD_array[0]['jumlahharispd']; ?>" ></td>
      <td class='clsUang'bgcolor="#D3D3D3" ><input class='defult' id="txtTotalUangSaku" name='txtTotalUangSaku' type="text" size="15" value="<?php echo $trkSPD_array[0]['bebanharianspd']*$trkSPD_array[0]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Makan</td>
      <td><input id="txtKetUangMakan" class='bordered' name='txtKetUangMakan' type="text" size="40" value="<?php echo $trkSPD_array[1]['keteranganspd']; ?>" ></td>
	  <?php 
		$readonly = "";
		if($trkSPD_array[0]['golongan'] <= 5){
			$readonly = "readonly";
		}
	  ?>
      <td class='clsUang' bgcolor="#D3D3D3"><input class='defult'  id="txtBebanHarianUangMakan" name='txtBebanHarianUangMakan' type="text" size="15" value="<?php 
		if($trkSPD_array[0]['uangmuka'] == 0){
			echo $dataUser['uangmakan'];
		}else{
			echo $trkSPD_array[1]['bebanharianspd']; 
		}		
	?>"  <?php echo $readonly; ?> ></td>
      <td class='clsHari'><input class='bordered' id="txtJmlhHariUangMakan" name='txtJmlhHariUangMakan' type="text" size="5" value="<?php echo $trkSPD_array[1]['jumlahharispd']; ?>" ></td>
      <td class='clsUang'><input class='bordered'  id="txtTotalUangMakan" name='txtTotalUangMakan' type="text" size="15" value="<?php echo $trkSPD_array[1]['bebanharianspd']*$trkSPD_array[1]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;*Akomodasi</td>
      <td><input id="txtKetHotel" class='bordered' name='txtKetHotel' type="text" size="40" value="<?php echo $trkSPD_array[2]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='bordered' id="txtBebanHarianHotel" name='txtBebanHarianHotel' type="text" size="15" value="<?php echo $trkSPD_array[2]['bebanharianspd']; ?>" ></td>
      <td class='clsHari'><input class='bordered' id="txtJmlhHariHotelLap" name="txtJmlhHariHotelLap" type="text" size="5" value="<?php echo $trkSPD_array[2]['jumlahharispd']; ?>" ></td>
      <td class='clsUang' ><input class='bordered' id="txtTotalHotelLap" type="text" size="15" value="<?php echo $trkSPD_array[2]['bebanharianspd']*$trkSPD_array[2]['jumlahharispd']; ?>" ></td>
    </tr>
	<tr class='clsInputNumber'>
      <td class="lbl"></td>
      <td><input id="txtKetHotel2" class='bordered' name="txtKetHotel2" type="text" size="40" value="" ></td>
      <td class='clsUang'><input class='bordered'  id="txtBebanHarianHotel2" name="txtBebanHarianHotel2" type="text" size="15" value="" ></td>
      <td class='clsHari'><input class='bordered' id="txtJmlhHariHotel2" name="txtJmlhHariHotel2" type="text" size="5" value="" ></td>
      <td class='clsUang'><input class='bordered'  id="txtTotalHotel2" name="txtTotalHotel2" type="text" size="15" value="" ></td>
    </tr>
    <!--<tr>
      <td><br/></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>-->
    <tr>
      <td class="lbl"><i>Transportasi</i></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr class='clsInputNumber'>
      <td  class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Taksi</td>
      <td><input id="txtKetTaksi" class='bordered' name='txtKetTaksi' type="text" size="40" value="<?php echo $trkSPD_array[4]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='bordered'  id="txtBebanHarianTaksi" name='txtBebanHarianTaksi' type="text" size="15" value="<?php echo $trkSPD_array[4]['bebanharianspd']; ?>" ></td>
      <td></td>
      <td class='clsUang'><input class='bordered'  id="txtTotalTaksi" name='txtTotalTaksi' type="text" size="15" value="<?php echo $trkSPD_array[4]['bebanharianspd']*$trkSPD_array[4]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Airport Tax</td>
      <td><input id="txtKetAirport" class='bordered' name='txtKetAirport' type="text" size="40" value="<?php echo $trkSPD_array[5]['keteranganspd']; ?>" ></td>
      <td class='clsUang'><input class='bordered'  id="txtBebanHarianAirport" name='txtBebanHarianAirport' type="text" size="15" value="<?php echo $trkSPD_array[5]['bebanharianspd']; ?>" ></td>
      <td></td></td>
      <td class='clsUang'><input class='bordered'  id="txtTotalAirport" name='txtTotalAirport' type="text" size="15" value="<?php echo $trkSPD_array[5]['bebanharianspd']*$trkSPD_array[5]['jumlahharispd']; ?>" ></td>
    </tr>
    <tr class='clsInputNumber'>
      <td class="lbl"><i>Lain-lain</i></td>
	  <?php 
		$counter = 1;
		for($i=6;$i<count($trkSPD_array);$i++){
			echo "<td><input class='bordered' id=\"txtKetLain".$counter."\" name='txtKetLain".$counter."' type=\"text\" size=\"40\" value='". $trkSPD_array[$i]['keteranganspd'] ."' ></td>
			  <td class='clsUang'><input class='bordered' id=\"txtBebanHarianLain".$counter."\" name='txtBebanHarianLain".$counter."' type=\"text\" size=\"15\" value='". $trkSPD_array[$i]['bebanharianspd'] ."' ></td>
			  <td></td>
			  <td class='clsUang'><input class='bordered' id=\"txtTotalLain".$counter."\" name='txtTotalLain".$counter."' type=\"text\" size=\"15\" value='" . $trkSPD_array[$i]['bebanharianspd']*$trkSPD_array[$i]['jumlahharispd'] . "' ></td>
			</tr>
			<tr class='clsInputNumber'>
				<td></td>";
			$counter++;
		}
		$counter = 1;
		if(count($trkSPD_array)==6){
			for($i=1;$i<=3;$i++){
				echo "<td><input id=\"txtKetLain".$counter."\" name='txtKetLain".$counter."' type=\"text\" size=\"40\" ></td>
				  <td class='clsUang'><input class='clsBebanHarian' id=\"txtBebanHarianLain".$counter."\" name='txtBebanHarianLain".$counter."' type=\"text\" size=\"15\" ></td>
				  <td></td>
				  <td class='clsUang'><input class='clsTotal' id=\"txtTotalLain".$counter."\" name='txtTotalLain".$counter."' type=\"text\" size=\"15\" value='' ></td>
				</tr>
				<tr class='clsInputNumber'>
					<td></td>";
				$counter++;
			}
		}
	  ?>
      <td></td>
      <td class="lbl" colspan="2"><b>Grand Total</b></td>
      <td class='clsUang' bgcolor="#D3D3D3"><input class='defult' id="txtGrandTotal" name='txtGrandTotal' type="text" size="15" ></td>
    </tr>
	

  </tbody>
</table>
<br/>

<!--realisasi uang muka-->
<table bordered="0">
  <tbody>
    <tr>
      <td class="lbl">Total Pengajuan Uang Muka</td>
      <td colspan="4">Rp. <input type="text" class='bordered' id="lblTotalPengajuanUangMuka" value="<?php	
		if ($trkSPD_array[0]['uangmuka'] == 0){
			echo "0";
		}else{
			echo $trkSPD_array[0]['totalSPD']; 
		}
	  ?>" readonly>
	  </td>
    </tr>
		<tr>
      <td class="lbl">Total Pengeluaran Perjalanan Dinas</td>
      <td colspan="4">Rp. <input type="text" class='bordered' id="lblTotalPengajuan" readonly></td>
    </tr>
    <tr>
	<tr>
      <td class="lbl"><label id="statusLebih"></label></td>
      <td colspan="4">Rp. <input type="text" class='bordered' id="lblSelisih" readonly></td>
    </tr>
			<tr>
      <td>Terbilang</td>
      <td colspan="4"><label id="lblTerbilang"></label></td>
    </tr>
	</tbody>
</table>

<br/>
<table id ="tablepembayaran" bordered="0">
  <tbody>
		<tr>
			<td colspan="2"><label id="psn"></label></td>
		</tr>
		<tr>
			<td >Bank</td><td>: <label id="nmbank"></label></td>
		</tr>
		<tr>
			<td>No Rekening</td><td>: <label id="norek"></label></td>
		</tr>
		<tr>
			<td>Nama Penerima</td><td>: <label id="nmpenerima"></label></td>
		</tr>
		
	</tbody>
</table>
<br/>

<table id ="tablepembayarankurang" bordered="0">
  <tbody>
		<tr>
			<td>Kekurangan Uang Muka harap di transfer ke</td>
		  <td><input type="text" id="txtPilihBayarKurang" name="txtPilihBayarKurang" value='TRANSFER' readonly></td>
		</tr>
		<tr>
			<td >Bank</td><td><input type="text" id="txtBankKurang" name="txtBankKurang"></td>
		</tr>
		<tr>
			<td>No Rekening</td><td><input type="number" id="txtNoRekKurang" name="txtNoRekKurang"></td>
		</tr>
		<tr>
			<td>Nama Penerima</td><td><input type="text" id="txtPenerimaKurang" name="txtPenerimaKurang"></td>
		</tr>
		
	</tbody>
</table>

<!-- tanda tangan -->
<table id="tblTandaTangan"  border=0>
  <tbody>
  
    <tr class="ttd">
		<td rowspan="2">Pemohon</td>
      <td colspan="3">Menyetujui</td>
    </tr>
    <tr class="ttd">
		
      <td><?php 
		if($trkSPD_array[0]['atasan']== $trkSPD_array[0]['nama'])
		{
			echo "DIC HRGA";
		}
		else if($trkSPD_array[0]['atasan']=='')
		{ 
			echo "Chief"; 
		}
		else
		{ 
			echo "Atasan"; 
		}
		?></td>
      <td>HRGA Dept Head</td>
		
    </tr>
    <tr class="ttd">
      <td><br/><br/><br/><br/><br/></td>
      <td></td>
      <td></td>
			
    </tr>
    <tr class="ttd">
      <td><?php echo $trkSPD_array[0]['nama']; ?></td>
      <td><?php echo $trkSPD_array[0]['atasan']==$trkSPD_array[0]['nama']?"":$trkSPD_array[0]['atasan']; ?></td>
      <td>Eviyati</td>
			
    </tr>
  </tbody>
</table>
<div id="note">
</div><br/>
<table class="tblPureNoBorder"><tr><td>
	<input class="btn btn-primary"  type="submit" onClick="return confirm('Apakah Anda yakin ingin menyimpan Laporan SPD ini ke dalam database?')" id="btnSubmit" name="submitLaporanSPD" value="Simpan">
	<a href="laporanSPD"><input class="btn btn-default"  type="button" value="Batal"></a>
	<div id="divError"></div>
</tr></td></table>
</form>
<?php } ?>
<script>
      $(function(){
      //Date picker
          
          $('#txtTanggalKembali').datepicker({
            autoclose: true,
						format: 'dd-mm-yyyy'
					}).on('changeDate', function (selected) {
              var minDate = new Date(selected.date.valueOf());
              $('#txtTanggalKembaliKantor').datepicker('setStartDate', minDate);
					});
				
					$('#txtTanggalKembaliKantor').datepicker({
            autoclose: true,
						format: 'dd-mm-yyyy'
					}).on('changeDate', function (selected) {
              var minDate = new Date(selected.date.valueOf());
              $('#txtTanggalKembali').datepicker('setEndDate', minDate);
					});
					$('#UM').hide();
				$('#txtTanggalKembaliKantor').change(function(){				
        loadtanggalPembayaranPenyelesaianUM();
			});
			$('#txtTanggalKembali').change(function(){				
        loadtanggalPembayaranPenyelesaianUM();
      });
			$('#txtAlasanExtend').change(function(){				
        loadtanggalPembayaranPenyelesaianUM();
      });
      function loadtanggalPembayaranPenyelesaianUM(){
        $('#divLoadingSubmit').show();
        var idKegiatan = document.getElementById('txtKegiatan').value;  
				var tglMulai = document.getElementById("txtTanggalBerangkat").value;
				if($('#txtAlasanExtend').val()=='Cuti'){
        var tglSelesai = document.getElementById("txtTanggalKembaliKantor").value;
				}else{
					var tglSelesai = document.getElementById("txtTanggalKembali").value;
				}
        tglMulai = tglMulai.substring(6, 10)+"-"+tglMulai.substring(3, 5)+"-"+tglMulai.substring(0, 2);
        tglSelesai = tglSelesai.substring(6, 10)+"-"+tglSelesai.substring(3, 5)+"-"+tglSelesai.substring(0, 2);

        if (tglSelesai=='' && tglMulai=='')
        {
            alert("Harap mengisi Tanggal Mulai dan Tanggal Selesai terlebih dulu"); 
        }
        else
        { 
            $.ajax({
                url: '<?php echo $this->config->base_url(); ?>index.php/UangMuka/formUMLain/ajax_loadWaktuJenisKegiatanDPA',
                type: "POST",             
                data: {idKegiatan:idKegiatan,tglMulai:tglMulai,tglSelesai:tglSelesai},
                dataType: 'json',
                cache: false,                
                success: function(data)
                {	
                console.log(data);   
                //alert('testttt');                 
                   // $('#txtWaktuBayarCepat').val(data[0].WaktuBayarUangMuka);
                    $('#lblWaktuPenyelesaian').val(data[0].WaktuSelesaiUangMuka);
                    $('#divLoadingSubmit').hide();
                },
                error: function (request, status, error) {
                    console.log(error);
                    $('#divLoadingSubmit').hide();
                }
							});
        }
			}
			$('#txtAlasanExtend').change(function(){				
        loadtanggalPembayaranPenyelesaianUM();
      });
			$('#txtTanggalKembali').change(function(){				
        loadtanggalPembayaranPenyelesaianUM();
      });
      $('#txtTanggalKembaliKantor').change(function () { loadtanggalPembayaranPenyelesaianUM(); });	
	}); 
		</script>
    <script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
    

    <script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
    <!--<script type="text/javascript">
    
                
      $('#txtTanggalKembali').change(function () { loadtanggalPembayaranPenyelesaianUM(); });
    </script>-->
       
      <script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>

	</body>
</html>