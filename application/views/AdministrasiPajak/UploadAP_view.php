<html>
<style type="text/css">
    @import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
    @import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
</style>
<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js">
</script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>

<script type="text/javascript">

        $(function() {
            $("#tanggalPelaporan").datepicker({
                format: 'yyyy-mm-dd'
            });
        });

        function SubmitTambahSPTMasa() {
            var _validFileExtensionsSPT = [".jpg", ".jpeg", ".bmp", ".gif", ".png", ".pdf", ".tif"];
            var _validFileExtensionsCSV = [".csv"];
            var flag = true;
            var arrInputs = TambahSPTMasa.getElementsByTagName("input");
            var countFileSPT = 0;
            var countFileCSV = 0;
            for (var i = 0; i < arrInputs.length; i++) {
                var HtmlInput = arrInputs[i];
                if (HtmlInput.type == "file") {
                    console.log(HtmlInput);

                    if (HtmlInput.name == 'uploadSPT[]') {
                        var sFileName = HtmlInput.value;
                        if (sFileName.length > 0) {
                            countFileSPT++;
                            var blnValid = false;
                            for (var j = 0; j < _validFileExtensionsSPT.length; j++) {
                                var sCurExtension = _validFileExtensionsSPT[j];
                                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() ==
                                    sCurExtension.toLowerCase()) {
                                    blnValid = true;
                                    break;
                                }
                            }
                            if (!blnValid) {
                                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensionsSPT.join(
                                    ", "));
                                flag = false;
                            }
                        }
                    }
                    if (HtmlInput.name == 'uploadCSV') {
                        var sFileName = HtmlInput.value;
                        if (sFileName.length > 0) {
                            countFileCSV++;
                            var blnValid = false;
                            for (var j = 0; j < _validFileExtensionsCSV.length; j++) {
                                var sCurExtension = _validFileExtensionsCSV[j];
                                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() ==
                                    sCurExtension.toLowerCase()) {
                                    blnValid = true;
                                    break;
                                }
                            }
                            if (!blnValid) {
                                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensionsCSV.join(
                                    ", "));
                                flag = false;
                            }
                        }

                    }

                }
            }
            if (countFileSPT == 0 || countFileCSV == 0) {
                alert("Pastikan sudah mengupload SPT dan CSV bersamaan");
                flag = false;
            }
            if (flag) {
                var x = window.confirm("Upload Dokumen Pajak?");
                if (x) {
                    $("#TambahSPTMasa").attr("action"); //will retrieve it
                    $("#TambahSPTMasa").attr("target", "_self"); //will retrieve it
                    $("#TambahSPTMasa").attr("action",
                        "<?php echo site_url('AdministrasiPajak/AdministrasiPajakController/tambahSPTMasa'); ?>/");
                } else {
                    event.preventDefault();
                }
            } else {
                event.preventDefault();
            }
        }

        function BatalTambahSPTMasa() {
            var x = window.confirm("Batalkan Upload Scan Dokumen SPT Masa ini?");
            if (x) {
                $("#TambahSPTMasa").attr("action"); //will retrieve it
                $("#TambahSPTMasa").attr("target", "_self"); //will retrieve it
                $("#TambahSPTMasa").attr("action", "<?php echo site_url('AdministrasiPajak/AdministrasiPajakController/'); ?>");
            } else {
                event.preventDefault();
            }
        }
</script>
<style>
    #error {
        color: red;
    }
</style>

<head>
    <title>Tambah Bupot</title>
</head>

<body>
    <?php echo $this->session->flashdata('message'); ?>

    <?php if (isset($error)) VAR_DUMP($error); ?>
    
    <div class="container" style="width:80%">
        <form class="form-group" method="POST" name="TambahSPTMasa" id="TambahSPTMasa" enctype="multipart/form-data">

            <div class="row">
                <div class="col"><br />
                    <label for="uploadSPT">Tambah Bupot</label>
                    <input type="file" multiple class="form-control form-control-file" name="uploadSPT[]">
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="uploadCSV">File Dokumen CSV</label>
                    <input type="file" class="form-control form-control-file" name="uploadCSV">
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="jenisPPH">Jenis PPH*</label>
                    <select class="form-control" id="cmbJenisPPH" name="cmbJenisPPH">
                        <?php
                        for ($i = 0; $i < sizeof($jenisPPh); $i++) {
                            echo "<option value='" . $jenisPPh[$i]->KodeJenisPPh . "'>" . $jenisPPh[$i]->JenisPPh . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="jenisPPH">Masa*</label>
                    <select class="form-control" id="cmbMasa" name="cmbMasa">
                        <?php
                        for ($i = 0; $i < sizeof($masa); $i++) {
                            echo "<option value='" . $masa[$i] . "'>" . $masa[$i] . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="jenisPPH">Tahun*</label>
                    <select class="form-control" id="cmbTahun" name="cmbTahun">
                        <?php
                        for ($i = $tahun; $i < ($tahun + 3); $i++) {
                            echo "<option value='" . $i . "'>" . $i . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col"><br />
                    <label for="jenisPPH">Tanggal Pelaporan*</label>
                    <input type="text" class="form-control" id="tanggalPelaporan" name="tanggalPelaporan">
                </div>
            </div> -->
            <div class="row">
                <div class="col"><br />
                    <input type="submit" name="Tambah" class="btn btn-success" onclick="SubmitTambahSPTMasa()" value="Tambah">
                    <input type="submit" name="BatalTambah" class="btn btn-danger" onclick="BatalTambahSPTMasa()" value="Batal Tambah">
                </div>
            </div>
        </form>
    </div>

</body>

</html>