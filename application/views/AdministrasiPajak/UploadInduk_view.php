<html>
<style type="text/css">
    @import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
    @import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
</style>
<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.ui.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js">
</script>
<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>

<script type="text/javascript">

        $(function() {
            $("#tanggalPelaporan").datepicker({ dateFormat: 'yy-mm-dd' });
        });

        function SubmitTambahInduk() {
            var _validFileExtensionsSPT = [".jpg", ".jpeg", ".bmp", ".gif", ".png", ".pdf"];
            var _validFileExtensionnPDF = [".pdf"];
            var flag = true;
            var arrInputs = TambahInduk.getElementsByTagName("input");
            var countFileSPT = 0;
            var countFilePDF = 0;
            for (var i = 0; i < arrInputs.length; i++) {
                var HtmlInput = arrInputs[i];
                if (HtmlInput.type == "file") {
                    console.log(HtmlInput);
                    if (HtmlInput.name == 'uploadPDF') {
                        var sFileName = HtmlInput.value;
                        if (sFileName.length > 0) {
                            countFilePDF++;
                            var blnValid = false;
                            for (var j = 0; j < _validFileExtensionnPDF.length; j++) {
                                var sCurExtension = _validFileExtensionnPDF[j];
                                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() ==
                                    sCurExtension.toLowerCase()) {
                                    blnValid = true;
                                    break;
                                }
                            }
                            if (!blnValid) {
                                alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensionnPDF.join(
                                    ", "));
                                flag = false;
                            }
                        }

                    }

                }
            }
            if (countFilePDF == 0) {
                alert("Tidak ada file PDF yang diupload");
                flag = false;
            }
            if (flag) {
                var x = window.confirm("Upload Induk?");
                if (x) {
                    $("#TambahInduk").attr("action"); //will retrieve it
                    $("#TambahInduk").attr("target", "_self"); //will retrieve it
                    $("#TambahInduk").attr("action",
                        "<?php echo site_url('AdministrasiPajak/AdministrasiPajakController/TambahInduk'); ?>/");
                } else {
                    event.preventDefault();
                }
            } else {
                event.preventDefault();
            }
        }

        // function BatalTambahInduk() {
        //     var x = window.confirm("Batalkan Upload Scan Dokumen SPT Masa ini?");
        //     if (x) {
        //         $("#TambahInduk").attr("action"); //will retrieve it
        //         $("#TambahInduk").attr("target", "_self"); //will retrieve it
        //         $("#TambahInduk").attr("action", "<?php echo site_url('AdministrasiPajak/AdministrasiPajakController/'); ?>");
        //     } else {
        //         event.preventDefault();
        //     }
        // }
</script>
<style>
    #error {
        color: red;
    }
</style>

<head>
    <title>Tambah Induk SPT Masa / Tahunan</title>
</head>

<body>
    <?php echo $this->session->flashdata('message'); ?>

    <?php if (isset($error)) VAR_DUMP($error); ?>
    
    <div class="container" style="width:80%">
        <form class="form-group" method="POST" name="TambahInduk" id="TambahInduk" enctype="multipart/form-data">

            <!-- <div class="row">
                <div class="col"><br />
                    <label for="uploadSPT">Tambah Induk SPT Masa / Tahunan</label>
                    <input type="file" multiple class="form-control form-control-file" name="uploadSPT[]">
                </div>
            </div> -->
            <div class="row">
                <div class="col"><br />
                    <label for="uploadPDF">File Dokumen PDF</label>
                    <input type="file" class="form-control form-control-file" name="uploadPDF">
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="DPA">DPA*</label>
                    <select class="form-control" id="cmbDPA" name="cmbDPA">
                        <option value='1'>1</option>
                        <option value='2'>2</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="jenisInduk">Jenis Induk*</label>
                    <select class="form-control" id="cmbJenisInduk" name="cmbJenisInduk">
                        <?php
                        for ($i = 0; $i < sizeof($jenisDokumen); $i++) {
                            echo "<option value='" . $jenisDokumen[$i]->KodeJenisDokumenLegal . "'>" . $jenisDokumen[$i]->JenisDokumenLegal . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="jenisSPT">Jenis SPT*</label>
                    <select class="form-control" id="cmbJenisSPT" name="cmbJenisSPT">
                        <option value='Tahunan'>Tahunan</option>
                        <option value='Masa'>Masa</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="jenisPPH">Jenis PPH*</label>
                    <select class="form-control" id="cmbJenisPPH" name="cmbJenisPPH">
                        <?php
                        for ($i = 0; $i < sizeof($jenisPPh); $i++) {
                            echo "<option value='" . $jenisPPh[$i]->KodeJenisPPh . "'>" . $jenisPPh[$i]->JenisPPh . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="jenisPPH">Masa*</label>
                    <select class="form-control" id="cmbMasa" name="cmbMasa">
                        
                        <?php
                        echo "<option value='Tahunan'>Tahunan</option>";
                        for ($i = 0; $i < sizeof($masa); $i++) {
                            echo "<option value='" . $masa[$i] . "'>" . $masa[$i] . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="jenisPPH">Tahun*</label>
                    <select class="form-control" id="cmbTahun" name="cmbTahun">
                        <?php
                        for ($i = $tahun; $i < ($tahun + 3); $i++) {
                            echo "<option value='" . $i . "'>" . $i . "</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <label for="jenisPPH">Tanggal Pelaporan*</label>
                    <input type="text" class="form-control" id="tanggalPelaporan" name="tanggalPelaporan">
                </div>
            </div>
            <div class="row">
                <div class="col"><br />
                    <input type="submit" name="Tambah" class="btn btn-success" onclick="SubmitTambahInduk()" value="Tambah">
                    <input type="submit" name="BatalTambah" class="btn btn-danger" onclick="BatalTambahInduk()" value="Batal Tambah">
                </div>
            </div>
        </form>
    </div>

</body>

</html>