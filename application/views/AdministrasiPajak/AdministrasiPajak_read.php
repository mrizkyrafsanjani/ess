<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
</head>
<body onload="parent.alertsizeLDArchive(document.body.clientHeight);">
		<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">Administrasi Pajak Detail</h3>
		</div><?php foreach ($detail as $key => $value) {
            ?>
		<div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h4><?php echo $value->FilePath; ?></h4>
                </div>
                <div class="col-sm-6">
                    <h5><a href="<?php echo $this->config->base_url(). $value->FilePath; ?>" download>download</a></h5>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <h5>Tanggal Mulai Berlaku</h5>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <h5><?php echo $value->TanggalLaporan; ?></h5>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <h5>Tanggal Kadaluarsa</h5>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <h5><?php echo $value->TanggalKadaluarsa; ?></h5>
                    </div>
                </div>
            </div> -->
            <div class="row">    
                <div class="col-md-12"> 
                    <iframe src="<?php echo $this->config->base_url(). $value->FilePath; ?>" width="90%" height="720px">
                    This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->config->base_url().'assets/uploads/files/dokumenpajak/'. $value->FilePath; ?>">Download PDF</a>
                    </iframe>   
                </div>
            </div>
            <?php
            /*if(($related!=null)){
                ?>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <h5>Dokumen yang diganti</h5>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php 
                                foreach($related as $value2){
                                    echo "<h5><a target='_parent' href='".site_url('LDArchive/LDArchiveController/read/'.$value2->KodeDokumen)."'>$value2->DocumentName</a></h5>";
                                }
                            ?>
                        </div>
                    </div>
                </div>

                <?php
            }*/
            ?>
        </div>
        <?php } ?>
</body>
</html>