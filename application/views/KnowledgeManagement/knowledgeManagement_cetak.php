<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.js"></script>
	
	<script type="text/javascript">
	
	function cetak()
	{
		window.print();		
	}

	</script>
	
	<head>
		<title>Cetak Knowledge Management</title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
		<style type="text/css" media="print">
		   .no-print { display: none; }
		   span.tab{
				padding: 0 80px; /* Or desired space*/
			}
		</style>
	</head>
	<body>
	<body onload="top.scrollTo(0,0)">
	<div class="no-print">
	<input class='buttonSubmit' type="button" value="Cetak" onClick="cetak()">
	</div>
	</br>
	</br>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Knowledge Management</h3>
			</div>
			</br></br>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						Dibuat Oleh	: <?php echo $nama; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						NPK : <?php echo $npk; ?>
					</div>
					<div class="col-md-6">
						
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Departemen: <?php  echo $departemen; ?>
					</div>
					<div class="col-md-6">
						
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Judul :
					</div>
					<div class="col-md-6">
						<font size="6"><b><?php echo $judul; ?></b></font>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Artikel :
					</div>
					<div class="col-md-6">
						<?php echo $artikel; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Sumber : <?php echo $sumber; ?>
					</div>
					<div class="col-md-6">
						
					</div>
				</div>
				</br></br></br></br>
				<div align="right">
				<td align="right" colspan="2" width="200px"><b>Mengetahui</b></td>
				</br></br></br></br>
				</div>
				<div align="right">
						<?php echo $atasan; ?>
					</div>
				<br/>
				</div>
	</body>
</html>
