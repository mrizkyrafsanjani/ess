<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetakPajak.css?v=1";
	</style>
	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript">
	
		function printDiv(divName) {
			 var printContents = document.getElementById(divName).innerHTML;
			 var originalContents = document.body.innerHTML;

			 document.body.innerHTML = printContents;

			 window.print();

			 document.body.innerHTML = originalContents;
		}

		$(function() {
			$(".clsUang").formatNumber({format:"#,##0", locale:"us"});
			$('.buttonSubmit').click(function (){				
				$('#printArea').printElement({
					//leaveOpen: true,
					pageTitle:'Form Pajak',
					printMode:'popup',
					overrideElementCSS:[
						'<?php echo $this->config->base_url(); ?>assets/css/cetak.css','<?php echo $this->config->base_url(); ?>assets/css/style-common.css']
				});
			});
			
			
			
		});
		
		

	</script>
	<head>
		<title>Pajak</title>
	</head>
	<body>
	<br/>
<div id='printArea'>
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">	  
	  
		<image  height="90" width="150" src='<?php echo $this->config->base_url(); ?>assets/images/LogoDPAvector.png'>
		<td colspan="3" style="text-align:center;" > <b>REKAPITULASI NON MP<br/>
							<?php if($masa==1) echo "Januari" ;	else
										if($masa==2) echo "Februari";	else
										if($masa==3) echo "Maret";	else
										if($masa==4) echo "April";	else
										if($masa==5) echo "Mei";else
										if($masa==6) echo "Juni";	else
										if($masa==7) echo "Juli";	else
										if($masa==8) echo "Agustus"	;else
										if($masa==9) echo "September";	else
										if($masa==10) echo "Oktober";	else
										if($masa==11) echo "November"	;else
										if($masa==12) echo "Desember" ; 

										echo ' ' + $tahun;
							?> </b>
		</td>
		
	</tr>
	 
  </tbody>
</table>



<br>
<?php 
	$JumP21=0; $JumpphP21=0;
	$JumP21GAJI=0; $JumpphP21GAJI=0;
	$JumP21PESANGON=0; $JumpphP21PESANGON=0;
	$JumP42=0;$JumpphP42=0;
	$JumP42Jasa=0;$JumpphP42Jasa=0;
	$JumP23=0;$JumpphP23=0;
	$JumP231=0;$JumpphP231=0;
	$JumP26=0;$JumpphP26=0;

	if(count($dataPajakPPH21data)>0){		?>
	<B>PPh Pasal 21 </B>
	<br>
	<table width="500" border="1" > 
		<tr class="tblHeader"> 
			<th   scope="col">Kode Pajak</th> 
			<th   scope="col">Kode Setor</th> 
			<th   scope="col">Jenis Penghasilan</th> 
			<th   scope="col">Nama</th> 
			<th   scope="col">Jumlah Penghasilan Bruto</th> 
			<th   scope="col">Jumlah Pajak Penghasilan </th> 
		</tr> 
		
		<?php
					for($i=0;$i<count($dataPajakPPH21data);$i++){		
					$JumP21=$JumP21+$dataPajakPPH21data[$i]['brutonf'];
					$JumpphP21=$JumpphP21+$dataPajakPPH21data[$i]['pphnf'];
					?>
					<tr>
						<td ><?php echo $dataPajakPPH21data[$i]['kodepajak']; ?></td>
						<td><?php echo $dataPajakPPH21data[$i]['KodeSetor']; ?></td>
						<td ><?php echo $dataPajakPPH21data[$i]['JenisPenghasilan']; ?></td>
						<td ><?php echo $dataPajakPPH21data[$i]['Nama']; ?></td>
						<td style="text-align:right;"><?php echo $dataPajakPPH21data[$i]['bruto']; ?></td>
						<td style="text-align:right;"><?php echo $dataPajakPPH21data[$i]['pph']; ?></td>
			
		</tr>
		<?php }?>
		<tr class=borderedtop>
					<td   colspan=4 >Total</td>
					<td    style="text-align:right;" class='clsUang'><?php echo number_format($JumP21, 0, '.', ',')  ; ?></td>
					<td    style="text-align:right;" class='clsUang'><?php echo number_format($JumpphP21, 0, '.', ',')  ; ?></td>
		
	</tr>
	</table>
<?php }		?>
<br>

<?php if(count($dataPajakPPH21GAJIdata)>0){		?>
	<B>PPh Pasal 21 - Gaji </B>
	<br>
	<table width="500" border="1" > 
		<tr class="tblHeader"> 
			<th   scope="col">Kode Pajak</th> 
			<th   scope="col">Kode Setor</th> 
			<th   scope="col">Jenis Penghasilan</th> 
			<th   scope="col">Nama</th> 
			<th   scope="col">Jumlah Penghasilan Bruto</th> 
			<th   scope="col">Jumlah Pajak Penghasilan </th> 
		</tr> 
		
		<?php 
					for($a=0;$a<count($dataPajakPPH21GAJIdata);$a++){		
					$JumP21GAJI=$JumP21GAJI+$dataPajakPPH21GAJIdata[$a]['brutonf'];
					$JumpphP21GAJI=$JumpphP21GAJI+$dataPajakPPH21GAJIdata[$a]['pphnf'];
					?>
					<tr>
						<td ><?php echo $dataPajakPPH21GAJIdata[$a]['kodepajak']; ?></td>
						<td><?php echo $dataPajakPPH21GAJIdata[$a]['KodeSetor']; ?></td>
						<td ><?php echo $dataPajakPPH21GAJIdata[$a]['JenisPenghasilan']; ?></td>
						<td ><?php echo $dataPajakPPH21GAJIdata[$a]['Nama']; ?></td>
						<td style="text-align:right;"><?php echo $dataPajakPPH21GAJIdata[$a]['bruto']; ?></td>
						<td style="text-align:right;"><?php echo $dataPajakPPH21GAJIdata[$a]['pph']; ?></td>
			
		</tr>
		<?php }?>
		<tr class=borderedtop>
					<td   colspan=4 >Total</td>
					<td    style="text-align:right;" class='clsUang'><?php echo number_format($JumP21GAJI, 0, '.', ',')  ; ?></td>
					<td    style="text-align:right;" class='clsUang'><?php echo number_format($JumpphP21GAJI, 0, '.', ',')  ; ?></td>
		
	</tr>
	</table>
<?php }		?>
<br>

<?php if(count($dataPajakPPH21PESANGONdata)>0){		?>
	<B>PPh Pasal 21 - Pesangon </B>
	<br>
	<table width="500" border="1" > 
		<tr class="tblHeader"> 
			<th   scope="col">Kode Pajak</th> 
			<th   scope="col">Kode Setor</th> 
			<th   scope="col">Jenis Penghasilan</th> 
			<th   scope="col">Nama</th> 
			<th   scope="col">Jumlah Penghasilan Bruto</th> 
			<th   scope="col">Jumlah Pajak Penghasilan </th> 
		</tr> 
		
		<?php 
					for($b=0;$b<count($dataPajakPPH21PESANGONdata);$b++){		
					$JumP21PESANGON=$JumP21PESANGON+$dataPajakPPH21PESANGONdata[$b]['brutonf'];
					$JumpphP21PESANGON=$JumpphP21PESANGON+$dataPajakPPH21PESANGONdata[$b]['pphnf'];
					?>
					<tr>
						<td ><?php echo $dataPajakPPH21PESANGONdata[$b]['kodepajak']; ?></td>
						<td><?php echo $dataPajakPPH21PESANGONdata[$b]['KodeSetor']; ?></td>
						<td ><?php echo $dataPajakPPH21PESANGONdata[$b]['JenisPenghasilan']; ?></td>
						<td ><?php echo $dataPajakPPH21PESANGONdata[$b]['Nama']; ?></td>
						<td style="text-align:right;"><?php echo $dataPajakPPH21PESANGONdata[$b]['bruto']; ?></td>
						<td style="text-align:right;"><?php echo $dataPajakPPH21PESANGONdata[$b]['pph']; ?></td>
			
		</tr>
		<?php }?>
		<tr class=borderedtop>
					<td   colspan=4 >Total</td>
					<td    style="text-align:right;" class='clsUang'><?php echo number_format($JumP21PESANGON, 0, '.', ',')  ; ?></td>
					<td    style="text-align:right;" class='clsUang'><?php echo number_format($JumpphP21PESANGON, 0, '.', ',')  ; ?></td>
		
	</tr>
	</table>
<?php }		?>

<br>
<?php if(count($dataPajakPPH4data)>0){		?>
<B>PPh Pasal 4 Ayat 2 Sewa</B>
<br>
<table width="500" border="1" > 
  <tr class="tblHeader"> 
    <th   scope="col">Kode Pajak</th> 
    <th   scope="col">Kode Setor</th> 
		<th   scope="col">Jenis Penghasilan</th> 
		<th   scope="col">Nama</th> 
		<th   scope="col">Jumlah Penghasilan Bruto</th> 
		<th   scope="col">Jumlah Pajak Penghasilan </th> 
  </tr> 
  
	<?php 
				for($i=0;$i<count($dataPajakPPH4data);$i++){		
				$JumP42=$JumP42+$dataPajakPPH4data[$i]['brutonf'];
				$JumpphP42=$JumpphP42+$dataPajakPPH4data[$i]['pphnf'];
		?>
				<tr>
					<td ><?php echo $dataPajakPPH4data[$i]['kodepajak']; ?></td>
					<td><?php echo $dataPajakPPH4data[$i]['KodeSetor']; ?></td>
					<td ><?php echo $dataPajakPPH4data[$i]['JenisPenghasilan']; ?></td>
					<td ><?php echo $dataPajakPPH4data[$i]['Nama']; ?></td>
					<td style="text-align:right;"><?php echo $dataPajakPPH4data[$i]['bruto']; ?></td>
					<td style="text-align:right;"><?php echo $dataPajakPPH4data[$i]['pph']; ?></td>
		
	</tr>
	<?php }?>
	<tr class="borderedtop">
					<td  colspan="4">Total</td>
					<td   style="text-align:right;" class='clsUang'><?php echo number_format($JumP42, 0, '.', ',')  ; ?></td>
					<td    style="text-align:right;" class='clsUang'><?php echo number_format($JumpphP42, 0, '.', ','); ?></td>
		
	</tr>
</table>
	<br>
<?php }		?>

<br>
<?php if(count($dataPajakPPH4Jasadata)>0){		?>
<B>PPh Pasal 4 Ayat 2 Jasa</B>
<br>
<table width="500" border="1" > 
  <tr class="tblHeader"> 
    <th   scope="col">Kode Pajak</th> 
    <th   scope="col">Kode Setor</th> 
		<th   scope="col">Jenis Penghasilan</th> 
		<th   scope="col">Nama</th> 
		<th   scope="col">Jumlah Penghasilan Bruto</th> 
		<th   scope="col">Jumlah Pajak Penghasilan </th> 
  </tr> 
  
	<?php 
				for($l=0;$l<count($dataPajakPPH4Jasadata);$l++){		
				$JumP42Jasa=$JumP42Jasa+$dataPajakPPH4Jasadata[$l]['brutonf'];
				$JumpphP42Jasa=$JumpphP42Jasa+$dataPajakPPH4Jasadata[$l]['pphnf'];
		?>
				<tr>
					<td ><?php echo $dataPajakPPH4Jasadata[$l]['kodepajak']; ?></td>
					<td><?php echo $dataPajakPPH4Jasadata[$l]['KodeSetor']; ?></td>
					<td ><?php echo $dataPajakPPH4Jasadata[$l]['JenisPenghasilan']; ?></td>
					<td ><?php echo $dataPajakPPH4Jasadata[$l]['Nama']; ?></td>
					<td style="text-align:right;"><?php echo $dataPajakPPH4Jasadata[$l]['bruto']; ?></td>
					<td style="text-align:right;"><?php echo $dataPajakPPH4Jasadata[$l]['pph']; ?></td>
		
	</tr>
	<?php }?>
	<tr class="borderedtop">
					<td  colspan="4">Total</td>
					<td   style="text-align:right;" class='clsUang'><?php echo number_format($JumP42Jasa, 0, '.', ',')  ; ?></td>
					<td    style="text-align:right;" class='clsUang'><?php echo number_format($JumpphP42Jasa, 0, '.', ','); ?></td>
		
	</tr>
</table>
	<br>
<?php }		?>

<?php if(count($dataPajakPPH23data)>0){		?>
<B>PPh Pasal 23 </B>
<br>
<table width="500" border="1" > 
  <tr class="tblHeader"> 
    <th   scope="col">Kode Pajak</th> 
    <th   scope="col">Kode Setor</th> 
		<th   scope="col">Jenis Penghasilan</th> 
		<th   scope="col">Nama</th> 
		<th   scope="col">Jumlah Penghasilan Bruto</th> 
		<th   scope="col">Jumlah Pajak Penghasilan </th> 
  </tr> 
  
	<?php 
				for($i=0;$i<count($dataPajakPPH23data);$i++){		
				$JumP23=$JumP23+$dataPajakPPH23data[$i]['brutonf'];
				$JumpphP23=$JumpphP23+$dataPajakPPH23data[$i]['pphnf'];
		?>
				<tr>
					<td ><?php echo $dataPajakPPH23data[$i]['kodepajak']; ?></td>
					<td><?php echo $dataPajakPPH23data[$i]['KodeSetor']; ?></td>
					<td ><?php echo $dataPajakPPH23data[$i]['JenisPenghasilan']; ?></td>
					<td ><?php echo $dataPajakPPH23data[$i]['Nama']; ?></td>
					<td style="text-align:right;"><?php echo $dataPajakPPH23data[$i]['bruto']; ?></td>
					<td style="text-align:right;"><?php echo $dataPajakPPH23data[$i]['pph']; ?></td>
		
				</tr>				
	<?php }?>
	<tr class="borderedtop">
					<td  colspan="4">Total</td>
					<td   style="text-align:right;" class='clsUang'><?php echo number_format($JumP23, 0, '.', ','); ?></td>
					<td   style="text-align:right;" class='clsUang'><?php echo number_format($JumpphP23, 0, '.', ','); ?></td>
		
	</tr>
  
</table> 
<?php }		?>

<?php if(count($dataPajakPPH23data1)>0){		
	if (count($dataPajakPPH23data)==0){?>
		<br>
		<B>PPh Pasal 23 </B>
	<?php } ?>
<br>
<table width="500" border="1" > 
  <tr class="tblHeader"> 
    <th   scope="col">Kode Pajak</th> 
    <th   scope="col">Kode Setor</th> 
		<th   scope="col">Jenis Penghasilan</th> 
		<th   scope="col">Nama</th> 
		<th   scope="col">Jumlah Penghasilan Bruto</th> 
		<th   scope="col">Jumlah Pajak Penghasilan </th> 
  </tr> 
  
	<?php 
				for($j=0;$j<count($dataPajakPPH23data1);$j++){		
				$JumP231=$JumP231+$dataPajakPPH23data1[$j]['brutonf'];
				$JumpphP231=$JumpphP231+$dataPajakPPH23data1[$j]['pphnf'];
		?>
				<tr>
					<td ><?php echo $dataPajakPPH23data1[$j]['kodepajak']; ?></td>
					<td><?php echo $dataPajakPPH23data1[$j]['KodeSetor']; ?></td>
					<td ><?php echo $dataPajakPPH23data1[$j]['JenisPenghasilan']; ?></td>
					<td ><?php echo $dataPajakPPH23data1[$j]['Nama']; ?></td>
					<td style="text-align:right;"><?php echo $dataPajakPPH23data1[$j]['bruto']; ?></td>
					<td style="text-align:right;"><?php echo $dataPajakPPH23data1[$j]['pph']; ?></td>
		
				</tr>				
	<?php }?>
	<tr class="borderedtop">
					<td  colspan="4">Total</td>
					<td   style="text-align:right;" class='clsUang'><?php echo number_format($JumP231, 0, '.', ','); ?></td>
					<td   style="text-align:right;" class='clsUang'><?php echo number_format($JumpphP231, 0, '.', ','); ?></td>
		
	</tr>
  
</table> 
<?php }		?>
<br>
<?php if(count($dataPajakPPH26data)>0){		?>
<B>PPh Pasal 26 </B>
<br>
<table width="500" border="1" > 
  <tr class="tblHeader"> 
    <th   scope="col">Kode Pajak</th> 
    <th   scope="col">Kode Setor</th> 
		<th   scope="col">Jenis Penghasilan</th> 
		<th   scope="col">Nama</th> 
		<th   scope="col">Jumlah Penghasilan Bruto</th> 
		<th   scope="col">Jumlah Pajak Penghasilan </th> 
  </tr> 
  
	<?php 
				for($k=0;$k<count($dataPajakPPH26data);$k++){		
				$JumP26=$JumP26+$dataPajakPPH26data[$k]['brutonf'];
				$JumpphP26=$JumpphP26+$dataPajakPPH26data[$k]['pphnf'];
		?>
				<tr>
					<td ><?php echo $dataPajakPPH26data[$k]['kodepajak']; ?></td>
					<td><?php echo $dataPajakPPH26data[$k]['KodeSetor']; ?></td>
					<td ><?php echo $dataPajakPPH26data[$k]['JenisPenghasilan']; ?></td>
					<td ><?php echo $dataPajakPPH26data[$k]['Nama']; ?></td>
					<td style="text-align:right;"><?php echo $dataPajakPPH26data[$k]['bruto']; ?></td>
					<td style="text-align:right;"><?php echo $dataPajakPPH26data[$k]['pph']; ?></td>
		
				</tr>				
	<?php }?>
	<tr class="borderedtop">
					<td  colspan="4">Total</td>
					<td   style="text-align:right;" class='clsUang'><?php echo number_format($JumP26, 0, '.', ','); ?></td>
					<td   style="text-align:right;" class='clsUang'><?php echo number_format($JumpphP26, 0, '.', ','); ?></td>
		
	</tr>
  
</table> 
<?php }	
$JumPajakAll=0;$JumBrutoAll=0;
$JumPajakAll= $JumpphP21+ $JumpphP21GAJI+ $JumpphP21PESANGON+$JumpphP42+$JumpphP23+$JumpphP231+$JumpphP26+$JumpphP42Jasa;
$JumBrutoAll =$JumP21+$JumP21GAJI+$JumP21PESANGON+$JumP42+$JumP23+$JumP231+$JumP26;
?>
<br>
<table width="500" border="1" > 
  <tr class="tblHeader"> 
    <th   scope="col" >Grand Total</th>  
	
	<th   style="text-align:right;" class='clsUang'><?php echo number_format($JumPajakAll, 0, '.', ','); ?></th>
  </tr>   

  
</table> 

</div>

<br>
<div id="note">
<table class="tblPureNoBorder"><tr><td>	
	<input type="button" onclick="printDiv('printArea')" value="Cetak" />
	</tr></td></table>
</div>
	</body>
</html>