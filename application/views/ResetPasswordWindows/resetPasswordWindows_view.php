<html>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url(); ?>assets/css/style-common.css">
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.js"></script>

	<script type="text/javascript">
		$(function() {
			var jmlhTekanTombolKirimToken = 0;
			var jmlhTekanTombolVerifikasi = 0;
			$('#btnKirimKode').click(function(){
                $('#btnKirimKode').attr('disabled',true);
				if($('#username').val()==""){
                    $('#username').focus();
					alert('Harap isi username terlebih dahulu');
					$('#btnKirimKode').attr('disabled',false);
				}else{
					$('#divError').html("<img src='<?php echo $this->config->base_url(); ?>assets/images/ProgressAnim.gif'> <font size='2px'>Tunggu sebentar...</font>");
					$.ajax({
					  type: "POST",
					  url: "<?php echo $this->config->base_url(); ?>index.php/ResetPasswordWindows/ResetPasswordWindows/sendSMStoken",
					  data: { username: $('#username').val() }
					}).done(function( msg ) {
						if(msg!="Terjadi kegagalan proses pengiriman SMS. Mohon dicoba kembali." && msg!="Username "+ $('#username').val() +" tidak ada!") 
						{
							//jika sukses
							jmlhTekanTombolKirimToken++;
							sec = 5*60;
							$('#username').attr("disabled", "disabled"); 
							$('#verifikasiBox').show();
							$("#verifikasiBox").animate({opacity: '1'});
						}
						else
						{							
							sec = 10;
						}
						if(jmlhTekanTombolKirimToken > 3)
						{
							msg = "Anda sudah mencoba lebih dari 3x melakukan pengiriman token. Informasi ini secara otomatis dilaporkan ke IT.";
							sendEmailKeIT('kirimToken');
						}
						$('#divError').html('');
					    alert(msg);                        
                        countDown();
					});
				}
			});

            var sec = 5*60;
            function countDown(){
                $('#divError').html('Anda dapat mengirim ulang kode konfirmasi setelah ' + sec + ' detik');
                if(sec<=0){
                    $('#btnKirimKode').attr('disabled',false);
                    $("#divError").html('');
                    return;
                }
                sec -= 1;
                window.setTimeout(countDown, 1000);
            }

			function sendEmailKeIT(jenis)
			{
				$.ajax({
					type:"POST",
					url:"<?php echo $this->config->base_url(); ?>index.php/ResetPasswordWindows/ResetPasswordWindows/sendEmailIT",
					data: { username: $('#username').val(), jenis: jenis }
				});
			}

			$('#btnVerifikasi').click(function(){
                $('#btnKirimKode').attr('disabled',true);
				$('#btnVerifikasi').attr('disabled',true);
				if($('#username').val()=="" || $('#kodeverifikasi').val() == ""){
                    $('#kodeverifikasi').focus();
					alert('Harap isi username dan kode verifikasi terlebih dahulu');                    
				}else{
					$('#divError').html("<img src='<?php echo $this->config->base_url(); ?>assets/images/ProgressAnim.gif'> <font size='2px'>Tunggu sebentar...</font>");
					$.ajax({
					  type: "POST",
					  url: "<?php echo $this->config->base_url(); ?>index.php/ResetPasswordWindows/ResetPasswordWindows/verifikasiToken",
					  data: { username: $('#username').val(), kodetoken: $('#kodeverifikasi').val() }
					}).done(function( msg ) {
						$('#divError').html('');						
						if(msg==="false"){
							$('#btnKirimKode').attr('disabled',false);
							$('#btnVerifikasi').attr('disabled',false);
					    	alert('Kode verifikasi yang Anda masukkan salah!');
							jmlhTekanTombolVerifikasi++;
							if(jmlhTekanTombolVerifikasi>3)
							{
								alert("Anda sudah mencoba lebih dari 3x melakukan verifikasi. Informasi ini secara otomatis dilaporkan ke IT.");
								sendEmailKeIT('verifikasi');
							}							
						}else{
							window.location.replace('ResetPasswordWindows/resetpasswin?token='+msg+'&uname='+ $('#username').val());
						}
					});
				}
			});

			//ripple effect
			// MAD-RIPPLE // (jQ+CSS)
			$(document).on("mousedown", "[data-ripple]", function(e) {			
				var $self = $(this);
				
				if($self.is(".btn-disabled")) {
				return;
				}
				if($self.closest("[data-ripple]")) {
				e.stopPropagation();
				}
				
				var initPos = $self.css("position"),
					offs = $self.offset(),
					x = e.pageX - offs.left,
					y = e.pageY - offs.top,
					dia = Math.min(this.offsetHeight, this.offsetWidth, 100), // start diameter
					$ripple = $('<div/>', {class:"ripple", appendTo:$self});
				
				if(!initPos || initPos==="static") {
				$self.css({position:"relative"});
				}
				
				$('<div/>', {
					class : "rippleWave",
					css : {
						background: $self.data("ripple"),
						width: dia,
						height: dia,
						left: x - (dia/2),
						top: y - (dia/2),
					},
					appendTo : $ripple,
					one : {
						animationend : function(){
						$ripple.remove();
						}
					}
				});
			});
		});
	</script>
	<head>
		<title>Enterprise Self Service Login</title>
		<link rel="shortcut icon" href=<?php echo str_replace('index.php/','',site_url("favicon.ico")); ?> />
		<style>
			#verifikasiBox{
				opacity:0;
				border: 1px;
				border-style: solid;
				padding: 10px 10px 10px 10px;
				border-color: lightblue;
				background-color: lightblue;
				transition: visibility 0s, opacity 0.5s linear;
			}
		</style>
	</head>
	<body>
		<div id="tv-bg-frame"></div>
		<div id="tv-topbar">
			<div style="width:500px; margin-left:auto; margin-right:auto;">
				<h1>RESET PASSWORD WINDOWS</h1></div>
		</div>
		<div id="container" style="border: none !important; width:100% !important;">
			<div id="leftcontent">
				<img src=<?php echo str_replace('index.php/','',site_url("assets/images/windows_image.jpg")); ?> alt="Enterprise Self Service Image">
			</div>
			<div id="rightcontentresetpwd">
			<!--<div style="width:500px; margin-left:auto; margin-right:auto;">-->
				
				<!--<div id='tv-top-logo'><br/><br/><div style="float:right; color:grey !important;">Enterprise Self Service</div></div>-->
				<!--<image id="logoDPA" src='../assets/images/LogoDPA.png' style="vertical-align: middle;">
				<span style="vertical-align: middle;"><font size="4px" color="grey">Dana Pensiun Astra</font></span>-->
				<br/><br/>
				Masukkan <b>username windows</b> Anda. Username windows adalah username <br/>yang biasa muncul pada saat layar login windows<br/><br/>
                <label for="username">Username :&nbsp;</label>
                <input class="textbox" type="text" size="20" id="username" name="username" autocomplete="off" />
                <button data-ripple id="btnKirimKode" class="mad-button-raised bg-primary">Kirim Kode Reset ke HP</button>
				
                <div id="divError"></div>
                <br/><br/>
				<div id="verifikasiBox" hidden>
					Mohon diisi dengan kode verifikasi yang di dapatkan dalam sms
					<br/><br/>
					<label for="kodeverifikasi">Kode Verifikasi :&nbsp;</label>
					<input class="textbox" type="text" size="20" id="kodeverifikasi" name="kodeverifikasi" />
					<button data-ripple id="btnVerifikasi" class="mad-button-raised bg-primary">Verifikasi </button>
				</div>
                
			<!--</div>-->
			</div>
		</div>
		<div id="footer" style="text-align: center"><div id='copyright'>BPIT DPA &copy;2013 - <?php echo strftime("%Y"); ?></div></div>
	</body> 	
</html>