<html>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url(); ?>assets/css/style-common.css?v=1.1">
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.js"></script>

	<script type="text/javascript">
		$(function() {		

			$('#btnGantiPass').click(function(){				
				passwordbaru = $('#txtnewpass').val();
				passwordconfirm = $('#txtconfpass').val();
				errorMsg = '';
				valid = true;
				if(passwordbaru != passwordconfirm){
					valid = false;
					errorMsg = "Password baru dan konfirmasi password tidak sama!";
				}else{
					if(passwordbaru.length < 7)
					{
						valid = false;
						errorMsg = "Panjang password minimal 7 karakter!";
					}
					else
					{
						if(passwordbaru.indexOf("<?php echo $uname;?>") > 0)
						{
							valid=false;
							errorMsg = "Password tidak boleh mengandung username!";
						}
						else
						{
							jumlahPassValid = 0;
							containsLowerCaseLetter = /[a-z]/.test(passwordbaru);
							containsUpperCaseLetter = /[A-Z]/.test(passwordbaru);
							containsDigit = /\d/.test(passwordbaru);
							containsSpecial = /[^a-zA-Z\d]/.test(passwordbaru);
							if(containsLowerCaseLetter) jumlahPassValid++;
							if(containsUpperCaseLetter) jumlahPassValid++;
							if(containsDigit) jumlahPassValid++;
							if(containsSpecial) jumlahPassValid++;
							if(jumlahPassValid >= 3){
								$('#btnGantiPass').attr('disabled',true);
								//eksekusi ganti password
								$('#divError').html("<img src='<?php echo $this->config->base_url(); ?>assets/images/ProgressAnim.gif'> <font size='2px'>Tunggu sebentar...</font>");
								$.ajax({
								type: "POST",
								url: "<?php echo $this->config->base_url(); ?>index.php/ResetPasswordWindows/ResetPasswordWindows/changeWinPass",
								data: { username: '<?php echo $uname; ?>', tokenenc: '<?php echo $tokenenc; ?>', passwordbaru: passwordbaru, passwordconfirm: passwordconfirm }
								}).done(function( msg ) {
									$('#btnGantiPass').attr('disabled',false);
									if(msg.indexOf("Gagal merubah password!") > 0)
									{
										alert('Mohon ganti password baru dengan pola lain. Pastikan gunakan huruf besar, huruf kecil, angka, dan simbol');
									}
									else if(msg=="redirect" || msg=="")
									{
										alert("Kode verifikasi Anda sudah tidak berlaku, mohon masukkan kode verifikasi baru");
										window.location.replace('<?php echo $this->config->base_url(); ?>index.php/ResetPasswordWindows/ResetPasswordWindows');
									}
									else if(msg.indexOf("Password Anda berhasil dirubah!") > 0)
									{
										alert(msg);
										window.location.replace('<?php echo $this->config->base_url(); ?>index.php');
									}
									else
									{
										alert(msg);
									}
								});
							}else{
								valid = false;
								errorMsg = "Password baru harus mengandung huruf besar, huruf kecil, angka dan simbol!";
							}
						}
					}
				}
				$('#divError').html(errorMsg);
			});

			//ripple effect
			// MAD-RIPPLE // (jQ+CSS)
			$(document).on("mousedown", "[data-ripple]", function(e) {			
				var $self = $(this);
				
				if($self.is(".btn-disabled")) {
				return;
				}
				if($self.closest("[data-ripple]")) {
				e.stopPropagation();
				}
				
				var initPos = $self.css("position"),
					offs = $self.offset(),
					x = e.pageX - offs.left,
					y = e.pageY - offs.top,
					dia = Math.min(this.offsetHeight, this.offsetWidth, 100), // start diameter
					$ripple = $('<div/>', {class:"ripple", appendTo:$self});
				
				if(!initPos || initPos==="static") {
				$self.css({position:"relative"});
				}
				
				$('<div/>', {
					class : "rippleWave",
					css : {
						background: $self.data("ripple"),
						width: dia,
						height: dia,
						left: x - (dia/2),
						top: y - (dia/2),
					},
					appendTo : $ripple,
					one : {
						animationend : function(){
						$ripple.remove();
						}
					}
				});
			});
		});
	</script>
	<head>
		<title>Enterprise Self Service Login</title>
		<link rel="shortcut icon" href=<?php echo str_replace('index.php/','',site_url("favicon.ico")); ?> />
	</head>
	<body>
		<div id="tv-bg-frame"></div>
		<div id="tv-topbar">
			<div style="width:500px; margin-left:auto; margin-right:auto;">
				<h1>RESET PASSWORD WINDOWS</h1></div>
		</div>
		<div id="container" style="border: none !important; width:100% !important;">
			<div id="leftcontent">
				<img src=<?php echo str_replace('index.php/','',site_url("assets/images/windows_image.jpg")); ?> alt="Enterprise Self Service Image">
			</div>
			<div id="rightcontent" style="width:500px; margin-left:auto; margin-right:auto;text-align: right;">
			<!--<div style="width:500px; margin-left:auto; margin-right:auto;">-->
				<!--<div id='tv-top-logo'><br/><br/><div style="float:right; color:grey !important;">Enterprise Self Service</div></div>-->
				<!--<image id="logoDPA" src='../assets/images/LogoDPA.png' style="vertical-align: middle;">
				<span style="vertical-align: middle;"><font size="4px" color="grey">Dana Pensiun Astra</font></span>-->

                <br/><br/>
                Mohon masukkan password baru Anda. Gunakan huruf besar, kecil, angka, dan simbol
                <br/><br/>
                <label for="username">Username :&nbsp;</label>
                <input class="textbox" type="text" size="20" id="txtusername" name="txtusername" value="<?php echo $uname;?>" autocomplete="off" readonly/><br/>

                <label for="txtnewpass">Password baru :&nbsp;</label>
                <input class="textbox" type="password" size="20" id="txtnewpass" name="txtnewpass" autocomplete="off" /><br/>

                <label for="txtnewpass">Konfirmasi password baru :&nbsp;</label>
                <input class="textbox" type="password" size="20" id="txtconfpass" name="txtconfpass" autocomplete="off" /><br/><br/>
                
				<button data-ripple id="btnGantiPass" class="mad-button-raised bg-primary">Ganti Password</button>
                <div id="divError"></div>

            <!--</div>--> 
			</div>
		</div>
		<div id="footer" style="text-align: center"><div id='copyright'>BPIT DPA &copy;2013 - <?php echo strftime("%Y"); ?></div></div>
	</body> 	
</html>