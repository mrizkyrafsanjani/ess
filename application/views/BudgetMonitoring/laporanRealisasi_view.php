<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}
		th {text-align:center;}
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>			
			</div>
			<div class="panel-body">
				<div class="">
					<div class="col-sm-3">
					<label for="cmbDPA" class="control-label">DPA</label>					
					<select id="cmbDPA" class="form-control">					
						<option value="1">Satu</option>
						<option value="2">Dua</option>
						<option value="0">Semua</option>
					</select>					
					</div>

					<div class="col-sm-3">
					<label for="dtTanggal" class="control-label">Tanggal</label>
					<div class="input-group date">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</div>
					<input type="text" class="form-control pull-right" id="dtTanggal">
					</div>
					</div>

					<div class="col-sm-3">
					<label for="cmbJenisPengeluaran" class="control-label">Jenis</label>					
					<select id="cmbJenisPengeluaran" class="form-control">					
						<option value="CAPEX">CAPEX</option>
						<option value="OPEX">OPEX</option>
					</select>					
					</div>

				</div>				
			</div>
			<div class="panel-footer clearfix">
				<div class="col-sm-1">
				<button id="btnSubmit" type="submit" class="btn btn-primary">Submit</button>
				</div>
				<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i>Processing</div>
			</div>
		</div>
		
		<div id="divRealisasiNonValid" class="alert alert-danger alert-dismissible" hidden>
			<h4><i class="icon fa fa-ban"></i> Alert!</h4>
			Terdapat Realisasi Yang Kode Activitynya Tidak Valid!
		</div>

		<div id="divLaporan" class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Laporan Realisasi Budget-DPA</h3>			  
            </div>
            <div class="box-body">
				<div id="divKontenLaporan"></div>
			</div>
		</div>

	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".select2").select2();
			$("#dtTanggal").datepicker({ autoclose: true, format: "dd/mm/yyyy"});
			$('#divLaporan').hide();
			$('#divLoading').hide();

			if(<?php echo $boolRealisasiNonValid; ?>)
			{
				$('#divRealisasiNonValid').show();
			}

			var downloadClick = function(){				
				document.location.href = "<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/RealisasiController/downloadCSV?dpa="+ $('#cmbDPA').val() +"&tanggal="+$('#dtTanggal').val()+"&jenispengeluaran="+$('#cmbJenisPengeluaran').val();
			}

			var validate = function(){
				var error = false;
				var dtTanggal = $('#dtTanggal');
				if(!dtTanggal.val()){
					alert("Mohon diisi tanggal realisasi!");
					error = true;
					dtTanggal.focus();
				}
				return error;
			}

			$('#btnSubmit').bind('click', function (e) {
				if(validate())
					return;
				$('#divLoadingSubmit').show();
				$('#divKontenLaporan').html('<i class="fa fa-refresh fa-spin"></i>Processing');
				$('#btnSubmit').prop('disabled', true);
				$.ajax({
					type: "POST",
					url: "<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/RealisasiController/ajax_loadLaporanRealisasi",
					data: {dpa: $('#cmbDPA').val(), tanggal: $('#dtTanggal').val(), jenispengeluaran: $('#cmbJenisPengeluaran').val()},
					success: function (response) {
						$('#divLaporan').show();
						var result = JSON.parse(response);
						$('#divKontenLaporan').html(result[0]);
						$('#divTglUpdate').html(result[1]);
						$('#btnSubmit').prop('disabled', false);
						$('#divLoadingSubmit').hide();
						
						$('#btnDownload').bind('click',downloadClick);
					}
				});
			});
			
			
			
		});
	</script>
</html>
