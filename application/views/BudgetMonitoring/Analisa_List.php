<!DOCTYPE html>
<html lang="en">
   <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

<head>
   <meta charset="utf-8" />
   <link rel="shortcut icon" href=<?php echo str_replace('index.php/','',site_url("favicon.ico")); ?> />

   <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <!-- Bootstrap 3.3.6 -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
   <!-- Font Awesome -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
   <!-- Ionicons -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/adminlte/ionicons.min.css">
   <!-- Select2 -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
   <!-- Theme style -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>dist/css/AdminLTE.min.css">
   <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
      page. However, you can choose any other skin. Make sure you
      apply the skin class to the body tag so the changes take effect.
   -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>dist/css/skins/skin-blue.min.css">
   <!-- Pace style -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/pace/pace.min.css">
   <script src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
   <!-- fullCalendar -->
   <script src="<?php echo $this->config->base_url(); ?>assets/bower_components/moment/moment.js"></script>
   <script src="<?php echo $this->config->base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

   <!-- Datatables -->
   <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css"> -->
   <!-- DataTables -->
   <!-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
   <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
   <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
   <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script> -->
   <link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url(); ?>assets/DataTables/datatables.min.css"/>
 
   <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/DataTables/datatables.min.js"></script>
   

</head>
<body onload="parent.alertsizeAnalisa(document.body.clientHeight);">
   <div class="dataTablesContainer" style="margin: 10px 0px;">
      <table id="table_list" class="display" style="border-style: black solid 2px ;font-size:12px;">
         <thead>
            <!-- <td><strong>No</strong></td> -->
            <td><strong>Kode COA</strong></td>
            <td><strong>Nama COA</strong></td>
            <td><strong>Kode Activity</strong></td>
            <td><strong>Nama Activity</strong></td>
            <td><strong>Jumlah Budget</strong></td>
            <td><strong>Realisasi</strong></td>
            <td><strong>Under / Over</strong></td>
            <td><strong>Persentase</strong></td>
            <td><strong>Analisa</strong></td>
            <td><strong>Budget Belum Realisasi</strong></td>
            <td><strong>Outlook</strong></td>
            <td><strong>Action</strong></td>
         </thead></strong>
         <tbody>
            <?php
            $No = 1;
            foreach($results as $row){
               ?>
               <tr>
                  <!-- <td><?php echo $No ?></td> -->
                  <td><?php echo $row->KodeCoa ?></td>
                  <td><?php echo $row->NamaCoa ?></td>
                  <td><?php echo $row->KodeActivity ?></td>
                  <td><?php echo $row->NamaActivity ?></td>
                  <td><?php echo $row->TotalBudget ?></td>
                  <td><?php echo $row->TotalRealisasi ?></td>
                  <td><?php echo $row->Under_Over ?></td>
                  <td><?php 
                  if($row->Persentase != null){
                     echo number_format((float)$row->Persentase, 2, '.', ''). '%';
                  }else {
                     echo '-';
                  }
                  // echo $row->Persentase . ' %';
                  ?></td>
                  <td><?php echo $row->Analisa ?></td>
                  <td><?php echo $row->belumrealisasi; ?></td>
                  <td><?php echo $row->pengajuan; ?></td>
                  <td><button class="button btn-primary" onclick="lihat('<?php echo $row->KodeActivity?>/<?php echo $row->MaxPeriode;?>')">Lihat</button>
                     <!-- <button class="button btn-success" onclick="lihat('<?php echo $row->KodeActivity?>/<?php echo $row->MaxPeriode;?>')">Analisa</button> -->
               </td>
               </tr>
            <?php $No++;
            }
            ?>
         </tbody>
      </table>
   </div>
</body>
   <!-- Bootstrap 3.3.6 -->
<script src="<?php echo $this->config->base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $this->config->base_url(); ?>dist/js/app.min.js"></script>

<!-- PACE -->
<script src="<?php echo $this->config->base_url(); ?>plugins/pace/pace.min.js"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
<script>
   function lihat(KodeActivity,MaxPeriode){
      window.location.href = "<?php echo site_url('BudgetMonitoring/AnalisaBudgetController/LihatDetil');?>/"+KodeActivity+"/"+MaxPeriode;
   }
</script>

<script type="text/javascript">
   $(document).ready( function () {
    $('#table_list').DataTable({
        "scrollX": true,
        dom: 'Bfrtip',
        buttons: [{
           extend :'excel',
           text : 'Export to Excel',
           title : 'Data Analisa'
        }]
      } );
} );
</script>
</html>