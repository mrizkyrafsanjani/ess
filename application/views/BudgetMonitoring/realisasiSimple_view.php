<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
 
<?php 
foreach($body->css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<?php foreach($body->js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<script>

	$(document).ready(function(){
		<?php if($this->session->flashdata('msg')){ ?>
		alert('<?php echo $this->session->flashdata('msg'); ?>');
		<?php } ?>
	});
</script>

</head>
<body onload="parent.alertsize(document.body.clientHeight);">
<!-- Beginning header -->

<!-- End of header-->
    <?php if($view == "ViewDetil"){ ?>
    <div class="col-sm-2">
        <label for="btnDownload" class="control-label">&nbsp;</label>	
        <button id="btnDownload" class="btn btn-default form-control" type="button" onClick="downloadClick()"><i class="fa fa-download"></i>Download</button>
    </div>    
    <?php } ?>
    <div>
        <?php echo $body->output; ?>
    </div>
<!-- Beginning footer -->
<div></div>
<!-- End of Footer -->
</body>
<script type="text/javascript">
    var downloadClick = function(){	
        var currentURL = window.location.href;
        var paramStr = currentURL.substr(currentURL.search("viewdetil")+10);
        var param = paramStr.split("/");
        document.location.href = "<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/Realisasi/downloadCSV?tahun="+ param[0] 
            +"&bulanMulai="+param[1]+"&bulanSelesai="+param[2]
            +"&status="+param[3]+"&kodeActivity="+param[4]+"&kodeCoa="+param[5];
    }
</script>
</html>