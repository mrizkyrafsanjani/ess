<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->		
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Budget Monitor <?php echo date('Y'); ?></h3>
		</div>
		<div class="panel-body">
		<?php echo validation_errors(); ?>
		<table border=0>			
			<tr>
				<td>DPA *</td>
				<td><select class="form-control" id="txtDPA">
					<option value='1'>1</option>
					<option value='2'>2</option>
					<option value='all'>Semua</option>
				</select></td>
			</tr>
			<tr>
				<td>Nama Activity</td>
				<td>
					<input class="form-control" type="text" id="txtKodeActivity"/>
					<select class="form-control select2" id="txtNamaActivity">
						<option value =''></option>
					<?php foreach($dataActivity as $row){
						echo "<option value='".$row->KodeActivity."'>".$row->NamaActivity." (". $row->KodeActivity.")</option>";
					} ?>
					</select>
				</td>
				<td>					
					<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
				</td>
			</tr>			
			<tr>
				<td></td>
				<td>
					<input class="btn btn-primary col-sm-2" type="button" onClick="search()" id="btnCari" name="" value="Cari">
					<div id="divLoadingCari" hidden class="col-sm-3" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i>Loading...</div>
				</td>
			</tr>
		</table>
		
		<iframe id="iFrameBudgetMonitoring" src="<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/BudgetMonitoring" width="100%" height="200px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		
		
		</div>
		</div>
	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
		function alertsizeBudgetMonitoring(pixels){
			pixels+=35;
			document.getElementById('iFrameBudgetMonitoring').style.height=pixels+"px";
		}
		
		function search(){
			var DPA = document.getElementById('txtDPA').value;
			var kodeActivity = document.getElementById('txtKodeActivity').value;
			var namaActivity = document.getElementById('txtNamaActivity').value;
			var npk = '<?php echo $npk; ?>';
			
			if(DPA === '')
			{
				alert('Harap masukkan DPA yang ingin dicari dengan benar!');
			}
			else
			{
				$('#divLoadingCari').show();
				var iframe = document.getElementById('iFrameBudgetMonitoring');				
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/BudgetMonitoringController/ajax_KalkulasiBudgetRealisasi',
					 { kodeActivity: kodeActivity, DPA: DPA },
					 function(response) {  					
					   iframe.src = '<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/BudgetMonitoring/search/'+ <?php echo $admin; ?> +'/'+DPA+'/'+kodeActivity;
					   $('#divLoadingCari').hide();
					 },
					 "html"
				);
			}
		}
		
		$('#txtDPA').change(function(){				
			$('#divLoadingSubmit').show();
			var DPA = document.getElementById('txtDPA').value;
			 $.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/BudgetMonitoringController/ajax_loadActivityBasedDPA',
				type: "POST",             
				data: {DPA : DPA, admin: <?php echo $admin; ?>},
				dataType: 'json',
				cache: false,
				success: function(data)
				{	
					$('#txtKodeActivity').val('');
					$('#txtNamaActivity').empty(); //remove all child nodes
					var newOption = $('<option value=""></option>');
					$('#txtNamaActivity').append(newOption);
					for(opsi in data)
					{
						$('#txtNamaActivity').append($('<option value="'+data[opsi].KodeActivity+'">'+data[opsi].NamaActivity+' ('+ data[opsi].KodeActivity +')</option>'));
					}					
					$('#txtNamaActivity').trigger("chosen:updated");
					$('#divLoadingSubmit').hide();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		});
		
		$('#txtKodeActivity').hide();
		$('#txtKodeActivity').keyup(function(){
			var kodeActivity = document.getElementById('txtKodeActivity').value;
			$('#txtNamaActivity').val(kodeActivity);			
		});
		
		$('#txtNamaActivity').change(function(){
			$('#txtKodeActivity').val($('#txtNamaActivity').val());
			var kodeActivity = $('#txtKodeActivity').val();
			//$('#txtDPA').val(kodeActivity.substr(kodeActivity.length - 1));
		});
		
		$(".select2").select2();
	</script>
</html>
