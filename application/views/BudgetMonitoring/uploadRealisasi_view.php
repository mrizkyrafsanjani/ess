<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Upload Realisasi</h3>			
			</div>
			<div class="panel-body">
			<?php echo validation_errors(); ?>
				<div class="col-md-6">
					<div class="box box-danger">
						<div class="box-header">
						<h3 class="box-title">Upload File Realisasi</h3>
						</div>
						<div class="box-body">
							<div class="input-group margin">
								<input type="file" class="form-control" id="inputFileUpload" accept=".csv">
								<span class="input-group-btn">
								<button id="btnUpload" type="button" class="btn btn-info btn-flat"><i class="fa fa-upload"></i>Upload!</button>						
								</span>
							</div>
							<h5>Tanggal pada file csv harus berformat "mm/dd/yyyy"</h5>
						</div>
						<!-- /.box-body -->
						<!-- Loading (remove the following to stop the loading)-->
						<div id="divLoading" class="overlay">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
						<!-- end loading -->
					</div>
					<!-- /.box -->
				</div>
				<!--<button class="btn btn-info" onclick="showUploadedData()">Show Uploaded Data</button>-->
				<div class="col-md-12" id="divKontenUploadData"></div>
			</div>
			<div class="panel-footer">
				<button id="btnSubmit" type="submit" class="btn btn-primary">Submit</button>
				<div id="divLoadingSubmit" hidden="true"><i class="fa fa-refresh fa-spin"></i>Processing</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		var dataSubmit;
		$('#divLoading').hide();
		
		$(':file').on('change', function() {
			var file = this.files[0];
			if (file.type != "application/vnd.ms-excel") {
				alert('Pastikan yang diupload bertipe csv');
			}
			// Also see .name, .type
		});

		$('#btnUpload').bind('click', function (e) {
			$('#btnSubmit').prop('disabled', true);
			var file = document.getElementById('inputFileUpload').files[0]; //Files[0] = 1st file
			var reader = new FileReader();
			reader.readAsBinaryString(file);
			reader.onload = doUpload;
			
			function doUpload(event) {
				$('#divLoading').show();
				var result = event.target.result;
				var fileName = document.getElementById('inputFileUpload').files[0].name;
				$.ajax({
					type: "POST",
					url: "ajax_uploadRealisasi",
					data: { data: result, name: fileName },					
					success: function (response) {						
						$('#divLoading').hide();
						if(response.indexOf('Error') !== -1){							
							alert(response);
						}
						else
						{
							alert('Upload berhasil, silahkan review sebelum submit!');
							document.getElementById("inputFileUpload").value = "";
							var result = JSON.parse(response);
							$('#divKontenUploadData').html(result[0]);
							dataSubmit = result[1];
						}
						$('#btnSubmit').prop('disabled', false);
					},
					error: function (){
						$('#divLoading').hide();
						alert('Upload gagal, silahkan coba kembali!');
						$('#btnSubmit').prop('disabled', false);
					}
				});				
			}
		});
		
		$('#btnSubmit').bind('click', function (e) {
			$('#divLoadingSubmit').show();
			$('#btnSubmit').prop('disabled', true);
			$.ajax({
				type: "POST",
				url: "ajax_submitUploadData",
				data: {data: dataSubmit},
				success: function (response) {
					if(response.indexOf('Realisasi telah dimasukkan') !== -1)
					{
						alert(response);
						window.location.replace("<?php echo $this->config->base_url(); ?>index.php/budgetmonitoring/RealisasiController/UploadRealisasi");
					}
					else{
						alert(response);
					}
					$('#btnSubmit').prop('disabled', false);
					$('#divLoadingSubmit').hide();
				}
			});
		});
	</script>
</html>
