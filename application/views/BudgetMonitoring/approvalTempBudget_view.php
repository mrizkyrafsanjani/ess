<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Approval Budget</h3>			
			</div>
			<div class="panel-body">
			<?php echo validation_errors(); ?>				
				<div id="divLoading" class="overlay" style="text-align:center">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
				<div class="col-md-12" id="divKontenUploadData"></div>
			</div>
			<div class="panel-footer">
				<button id="btnApprove" type="submit" class="btn btn-primary">Approve</button>
				<button id="btnDecline" type="submit" class="btn btn-default">Decline</button>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		showUploadedData();
		function showUploadedData(){
			$('#divLoading').show();
			$.ajax({
				type: "POST",
				url: "<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/TempBudgetController/ajax_getUploadData/<?php echo $kodeusertask;?>",
				success: function(response){					
					//console.log(response);
					$('#divKontenUploadData').html(response);
					$('#divLoading').hide();
				},
				error: function(response){
					$('#divKontenUploadData').html(response);
					$('#divLoading').hide();
				}
			});
		}

		$('#btnApprove').bind('click', function (e) {
			if(confirm("Apakah Anda yakin untuk melakukan APPROVE?")){
				$('.btn').prop('disabled', true);
				$.ajax({
					type: "POST",
					url: "<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/TempBudgetController/ajax_ApprovalUploadData",
					data: {kodeusertask: "<?php echo $kodeusertask;?>", statustransaksi: "AP"},
					success: function (response) {
						if(response.indexOf('Master budget telah diapprove') !== -1)
						{
							alert(response);
							window.location.replace("<?php echo $this->config->base_url(); ?>index.php/UserTask");
						}
						else{
							alert(response);
						}
						$('.btn').prop('disabled', false);
					}
				});
			}
		});

		$('#btnDecline').bind('click', function (e) {
			if(confirm("Apakah Anda yakin untuk melakukan DECLINE?")){
				$('.btn').prop('disabled', true);
				$.ajax({
					type: "POST",
					url: "<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/TempBudgetController/ajax_ApprovalUploadData",
					data: {kodeusertask: "<?php echo $kodeusertask;?>", statustransaksi: "DE"},
					success: function (response) {
						if(response.indexOf('Master budget telah didecline') !== -1)
						{
							alert(response);
							window.location.replace("<?php echo $this->config->base_url(); ?>index.php/UserTask");
						}
						else{
							alert(response);
						}
						$('.btn').prop('disabled', false);
					}
				});
			}
		});
	</script>
</html>
