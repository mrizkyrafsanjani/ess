<!DOCTYPE html>
<html lang="en">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">	
    <head>
    <meta charset="utf-8" />
   <link rel="shortcut icon" href=<?php echo str_replace('index.php/','',site_url("favicon.ico")); ?> />

   <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <!-- Bootstrap 3.3.6 -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
   <!-- Font Awesome -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
   <!-- Ionicons -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/adminlte/ionicons.min.css">
   <!-- Select2 -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
   <!-- Theme style -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>dist/css/AdminLTE.min.css">
   <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
      page. However, you can choose any other skin. Make sure you
      apply the skin class to the body tag so the changes take effect.
   -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>dist/css/skins/skin-blue.min.css">
   <!-- Pace style -->
   <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/pace/pace.min.css">
   <script src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
   <!-- fullCalendar -->
   <script src="<?php echo $this->config->base_url(); ?>assets/bower_components/moment/moment.js"></script>
   <script src="<?php echo $this->config->base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

   <!-- Datatables -->
   <link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url(); ?>assets/DataTables/datatables.min.css"/>
   <script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/DataTables/datatables.min.js"></script>
    </head>
    <body onload="parent.alertsizeAnalisa(document.body.clientHeight);">
        <div class="box" style="margin-top: 10px;">
            <div class="box-header" style="background-color:#e7f3ff;">
            <h3><strong>Budget - Realisasi Per Bulan Sebelum : <?php echo $periode ;?> </strong></h3>
            </div>
            <div class="row">
                <table id="headerDetail" class="display nowrap" style="margin: 10px;">
                    <tr>
                        <thead>
                            <td><strong>Bulan</strong></td>
                            <?php foreach($detil as $row){
                                ?><td><?php echo date("F",mktime(0,0,0,$row->Bulan,10))?></td>
                        
                            <?php }?>
                        </thead>
                    </tr>
                    <tr>
                        <td><strong>Budget</strong></td>
                        <?php foreach($detil as $row){
                        ?><td><?php echo $row->JumlahBudget?></td>
                        
                        <?php }?>
                        
                    </tr>
                    <tr>
                        <td><strong>Realisasi</strong></td>
                        <?php foreach($detil as $row){
                        ?><td><?php echo $row->Nominal?></td>
                        
                        <?php }?>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row" style="padding: 5%;">
            <form class = "form-group" action="<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/AnalisaBudgetController/TambahAnalisa" method="post">
                <div class="row" style="margin: 10px 0px;">
                    <label><strong>Kode COA</Strong></label>
                        <input type="text" class="form-control input-lg" id="kodeCoa"value = "<?php 
                        foreach($myHead as $rowHead)
                        {
                            echo $rowHead->KodeCoa;
                        }?>" 
                    readonly>
                    
                </div>

                <div class="row" style="margin: 10px 0px;">
                    <label><strong>Kode Activity</strong></label>
                    <input type="text" class="form-control input-lg" name="kodeActivity" id="kodeActivity" value = "<?php echo ($rowHead->KodeActivity);?>" readonly>
                </div>
                
                <div class="row" style="margin: 10px 0px;">
                    <label><strong>Nama Activity</strong></label>
                    <input type="text" class="form-control input-lg" name="namaActivity" id="namaActivity" value = "<?php echo ($rowHead->NamaActivity);?>" readonly>
                </div>
                <div class="row" style="margin: 10px 0px;">
                    <label><strong>Analisa</strong></label>
                    <textarea name ="analisa" class="form-control input-lg" id="analisa" required><?php echo($rowHead->analisa); ?></textarea>
                </div>
                <div class="row" style="margin: 10px 0px;">
                    <label><strong>Periode</strong></label>
                    <input type="text" class="form-control input-lg" name="periode" id="periode" value = "<?php echo $periode;?>" readonly>
                </div>
                <div class="row" style="margin: 10px 0px;">
                    <input type="submit" class="btn btn-primary">
                </div>
            </div>
            <div class="row" style="margin-left: 2px;margin-top: 20px; border-top: 1px solid gray;">
                <div style= "padding-top: 5px;">
                    <div class = "row">
                        <div class = "col-md-2">
                            <strong>History Analisa</strong>
                        </div>
                    </div>
                    <table id="historyTable" class="table table-bordered">
                        <thead>
                            <td><strong>No</strong></td>
                            <td><strong>Analisa</strong></td>
                            <td><strong>Tanggal Buat</strong></td>
                            <td><strong>Dibuat Oleh</strong></td>
                        </thead>
                        <tbody>
                            <?php $No = 1;
                            foreach($history as $hisRow){?>
                                <tr>
                                    <td><?php echo $No; ?></td>
                                    <td><?php echo $hisRow->Analisa;?></td>
                                    <td><?php echo $hisRow->CreatedOn;?></td>
                                    <td><?php echo $hisRow->CreatedBy;?></td>
                                </tr>
                                <?php $No ++;
                            }
                            
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>

        
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo $this->config->base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo $this->config->base_url(); ?>dist/js/app.min.js"></script>

        <!-- PACE -->
        <script src="<?php echo $this->config->base_url(); ?>plugins/pace/pace.min.js"></script>
        <!-- Optionally, you can add Slimscroll and FastClick plugins.
        Both of these plugins are recommended to enhance the
        user experience. Slimscroll is required when using the
        fixed layout. -->
    </body>
<script type="text/javascript">
   $(document).ready( function () {
        $('#headerDetail').DataTable({
            "scrollX": true
        } );
    } );
</script>
<script type="text/javascript">
   $(document).ready( function () {
        $('#historyTable').DataTable({
            "scrollX": true
        } );
    } );</script>
</html>