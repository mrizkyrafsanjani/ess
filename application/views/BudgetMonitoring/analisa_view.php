<!DOCTYPE html>
<html lang="en">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">	
	
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
<!-- <script>
function alertsizeAnalisa(pixels){
			pixels+=60;
			document.getElementById('iFrameAssetManagement').style.height=pixels+"px";
		}
	</script> -->
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>
			</div>
			<div class="panel-body">
				<div class="col-sm-2">
					<label for="DPA" class="control-label">DPA :</label>					
					<select id="DPA" class="form-control">
						<option value="all">Semua</option>					
						<option value="1">1</option>
						<option value="2">2</option>
					</select>					
				</div>
				<div class="col-sm-2">
					<label for="cmbTahun" class="control-label">Tahun</label>					
					<select id="cmbTahun" class="form-control">
						<option value="all">Semua</option>					
						<?php 
							for($i=date('Y');$i>date('Y')-2;$i--)
							{
								$selected = '';
								if(date('Y') == $i )
									$selected = 'selected';
								echo "<option value='".$i."' $selected>".$i."</option>";
							}
						?>
					</select>					
				</div>

				<div class="col-sm-2">
					<label for="cmbBulan" class="control-label">Bulan</label>					
					<select id="cmbBulan" class="form-control">
						<?php 
							for($i=1;$i<=12;$i++)
							{
								$selected = '';
								if(date('n') == $i )
									$selected = 'selected';
								echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
							}
						?>					
					</select>					
				</div>
				
				<div class="col-sm-2">
					<label for="cmbJenis" class="control-label">Jenis Pengeluaran</label>					
					<select id="cmbJenis" class="form-control">					
						<option value="all">Semua</option>
						<option value="OPEX">OPEX</option>
						<option value="CAPEX">CAPEX</option>
					</select>					
				</div>
				
				<div class="col-sm-2">
					<label for="Departemen" class="control-label">Departemen</label>
					<select id="Departemen" class="form-control">
						<option value ="all">Semua</option>
						<?php 
						foreach($Departemen as $row){
							echo "<option value='".$row->KodeDepartemen."'>".$row->NamaDepartemen."</option>";
						} 
						// foreach($dataActivityDPA2 as $row){
						// 	echo "<option value='".$row->KodeActivity."'>".$row->NamaActivity." (". $row->KodeActivity.")</option>";
						// }
						?>
					</select>									
				</div>

				<div class="col-sm-1">
					<label for="btnCari" class="control-label">&nbsp;</label>	
					<input class="btn btn-primary form-control" type="button" onClick="search()" id="btnCari" name="" value="Cari">
					
				</div>

				<iframe id="iFrame" src="" width="100%"  height= "500px" seamless frameBorder="0">
				<p>Your browser does not support iframes.</p>
				</iframe>
			</div>
		</div>
	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
		// var downloadClick = function(){	
		// 	document.location.href = "<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/Realisasi/downloadCSV?tahun="+ $('#cmbTahun').val() 
		// 		+"&bulanMulai="+$('#cmbBulanMulai').val()+"&bulanSelesai="+$('#cmbBulanSelesai').val()
		// 		+"&status="+$('#cmbStatus').val()+"&kodeActivity="+$('#txtKodeActivity').val()+"&kodeCoa="+$('#txtKodeCoa').val();
		// }

		function alertsizeAnalisa(pixels){
			pixels+=60;
			document.getElementById('iFrame').style.height=pixels+"px";
			$('#table_list').hide();
		}

		function search(){
			$('#table_list').show();
			var DPA = document.getElementById('DPA').value;
			var tahun = document.getElementById('cmbTahun').value;
			var bulan = document.getElementById('cmbBulan').value;
			var Departemen = document.getElementById('Departemen').value;
			var Jenis = document.getElementById('cmbJenis').value;

			var iframe = document.getElementById('iFrame');			
			iframe.src = '<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/AnalisaBudget/ViewUser/'+DPA+'/'+tahun+'/'+bulan+'/'+Departemen+'/'+Jenis;
		}

		$(".select2").select2();
	</script>
</html>
