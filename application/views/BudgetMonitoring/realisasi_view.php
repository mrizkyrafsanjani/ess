<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">	
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>
			</div>
			<div class="panel-body">
			
			<div class="col-sm-2">
				<label for="cmbTahun" class="control-label">Tahun</label>					
				<select id="cmbTahun" class="form-control">
					<option value="all">Semua</option>					
					<?php 
						for($i=date('Y');$i>date('Y')-2;$i--)
						{
							$selected = '';
							if(date('Y') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".$i."</option>";
						}
					?>
				</select>					
			</div>
			
			<div class="col-sm-2">
				<label for="cmbBulanMulai" class="control-label">Bulan Mulai</label>					
				<select id="cmbBulanMulai" class="form-control">
					<option value="all">Semua</option>
					<?php 
						for($i=1;$i<=12;$i++)
						{
							$selected = '';
							if(date('n') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
						}
					?>					
				</select>					
			</div>

			<div class="col-sm-2">
				<label for="cmbBulanSelesai" class="control-label">Bulan Selesai</label>					
				<select id="cmbBulanSelesai" class="form-control">
					<option value="all">Semua</option>
					<?php 
						for($i=1;$i<=12;$i++)
						{
							$selected = '';
							if(date('n') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
						}
					?>					
				</select>					
			</div>
			
			<div class="col-sm-2">
				<label for="cmbStatus" class="control-label">Status Kode Activity</label>					
				<select id="cmbStatus" class="form-control">					
					<option value="all">Semua</option>
					<option value="valid">Valid</option>
					<option value="tidaksesuai">Tidak Sesuai</option>
				</select>					
			</div>
			
			<div class="col-sm-2">
				<label for="txtKodeActivity" class="control-label">Kode Activity</label>
				<select class="form-control select2" id="txtKodeActivity">
					<option value =''></option>
					<?php 
					foreach($dataActivityDPA1 as $row){
						echo "<option value='".$row->KodeActivity."'>".$row->NamaActivity." (". $row->KodeActivity.")</option>";
					} 
					foreach($dataActivityDPA2 as $row){
						echo "<option value='".$row->KodeActivity."'>".$row->NamaActivity." (". $row->KodeActivity.")</option>";
					}
					?>
				</select>									
			</div>

			<div class="col-sm-2">
				<label for="txtKodeCoa" class="control-label">Kode COA</label>					
				<select class="form-control select2" id="txtKodeCoa">
					<option value =''></option>
					<?php 
					foreach($dataCoa as $row){
						echo "<option value='".$row->KodeCoa."'>".$row->NamaCoa." (". $row->KodeCoa.")</option>";
					}
					?>
				</select>
			</div>

			<div class="col-sm-1">
				<label for="btnCari" class="control-label">&nbsp;</label>	
				<input class="btn btn-primary form-control" type="button" onClick="search()" id="btnCari" name="" value="Cari">
				
			</div>
			<div class="col-sm-2">
				<label for="btnDownload" class="control-label">&nbsp;</label>	
				<button id="btnDownload" class="btn btn-default form-control" type="button" onClick="downloadClick()"><i class="fa fa-download"></i>Download</button>
			</div>
			<div id="divLoadingCari" hidden class="col-sm-2" style="padding-top:30px"><i id="spinner" class="fa fa-refresh fa-spin"></i>Loading...</div>
			<iframe id="iFrame" src="<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/Realisasi/ViewUser" width="100%" height="500px" seamless frameBorder="0">
			<p>Your browser does not support iframes.</p>
			</iframe>
			
			
			</div>
		</div>
	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
		var downloadClick = function(){	
			document.location.href = "<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/Realisasi/downloadCSV?tahun="+ $('#cmbTahun').val() 
				+"&bulanMulai="+$('#cmbBulanMulai').val()+"&bulanSelesai="+$('#cmbBulanSelesai').val()
				+"&status="+$('#cmbStatus').val()+"&kodeActivity="+$('#txtKodeActivity').val()+"&kodeCoa="+$('#txtKodeCoa').val();
		}

		function alertsize(pixels){
			pixels+=60;
			document.getElementById('iFrame').style.height=pixels+"px";
			$('#divLoadingCari').hide();
		}

		function search(){
			$('#divLoadingCari').show();
			var tahun = document.getElementById('cmbTahun').value;
			var bulanMulai = document.getElementById('cmbBulanMulai').value;
			var bulanSelesai = document.getElementById('cmbBulanSelesai').value;
			var status = document.getElementById('cmbStatus').value;
			var kodeActivity = document.getElementById('txtKodeActivity').value;
			var kodeCoa = document.getElementById('txtKodeCoa').value;
			if(kodeActivity == "")
				kodeActivity = "all";
			if(kodeCoa == "")
				kodeCoa = "all";

			var iframe = document.getElementById('iFrame');			
			iframe.src = '<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/Realisasi/ViewUser/'+tahun+'/'+bulanMulai+'/'+bulanSelesai+'/'+status+'/'+kodeActivity+'/'+kodeCoa;
		}

		$(".select2").select2();
	</script>
</html>
