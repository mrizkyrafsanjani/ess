<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">CashFlow DPA2</h3>
		</div>
		<div class="panel-body">
		<?php echo validation_errors(); ?>
		<table border=0>			
			
			<!--<tr>
				<td>Rekening *</td>
				<td colspan=3><select class="form-control" id="cmbRekening">
					<option value='BPT01DPA2'>Rekening Iuran DPA2</option>
					<option value='BPT02DPA2'>Rekening Investasi DPA2</option>
					<option value='BPT03DPA2'>Rekening Opex DPA2</option>
					<option value='BPT04DPA2'>Rekening MP DPA2</option>
					<option value='BPTDPADPA2'>Rekening CTD DPA2</option>
				</select></td>
			</tr>-->
			<tr>			
			<td>Tahun</td>
				<td colspan=3>
					<select class="form-control" id="cmbTahun" name="cmbTahun">
					<?php 
						for($i=date('Y')+1;$i>date('Y')-2;$i--)
						{
							$selected = '';
							if(date('Y') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".$i."</option>";	
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Bulan</td>
				<td colspan=3>
					<select class="form-control" id="cmbBulan" name="cmbBulan">
					<?php 
						for($i=1;$i<=12;$i++)
						{
							$selected = '';
							if(date('n') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
						}
					?>
					</select>
				</td>
			</tr>			
			
			
			<tr>
				<td></td>
				<td>
				<div class="col-sm-4">
					<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari"></div>
					<div id="divLoadingCari" hidden class="col-sm-7" style="padding-top:8px"><i id="spinner" class="fa fa-refresh fa-spin"></i>Loading...</div>
				</td>
			</tr>
		</table>

		<div id="divContent" hidden>
		<H4><b>Rekening Iuran DPA2</b></H4>
		<iframe id="iFrameCashFlowIuran" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2CashFlow" width="100%" height="300px" seamless frameBorder="0">
			<p>Your browser does not support iframes.</p>
		</iframe>
		Total IN  : <input id="txtTotalIuranInDPA2" type="text" readonly="true">
		Total OUT : <input id="txtTotalIuranOutDPA2" type="text" readonly="true">

		<H4><b>Rekening Investasi DPA2</b></H4>
		<iframe id="iFrameCashFlowInv" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2CashFlow" width="100%" height="300px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		Total IN  : <input id="txtTotalInvestInDPA2" type="text" readonly="true">
		Total OUT : <input id="txtTotalInvestOutDPA2" type="text" readonly="true">

		<H4><b>Rekening Opex DPA2</b></H4>
		<iframe id="iFrameCashFlowOpex" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2CashFlow" width="100%" height="300px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		Total IN  : <input id="txtTotalOpexInDPA2" type="text" readonly="true">
		Total OUT : <input id="txtTotalOpexOutDPA2" type="text" readonly="true">

		<H4><b>Rekening MP DPA2</b></H4>
		<iframe id="iFrameCashFlowMP" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2CashFlow" width="100%" height="300px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		Total IN  : <input id="txtTotalMpInDPA2" type="text" readonly="true">
		Total OUT : <input id="txtTotalMpOutDPA2" type="text" readonly="true">

		<H4><b>Rekening CTD DPA2</b></H4>
		<iframe id="iFrameCashFlowCTD" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2CashFlow" width="100%" height="300px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		Total IN  : <input id="txtTotalCTDInDPA2" type="text" readonly="true">
		Total OUT : <input id="txtTotalCTDOutDPA2" type="text" readonly="true">
		</div>
		
		</div>
		</div>
	</body>
	<script type="text/javascript">
		function alertsizeCashFlowDPA2(pixels){
			pixels+=35;
			document.getElementById('iFrameCashFlow').style.height=pixels+"px";			
		}
		
		function search(){
			$('#divLoadingCari').show();
			var Tahun = document.getElementById('cmbTahun').value;
			var Bulan = document.getElementById('cmbBulan').value;
			//var Rekening = document.getElementById('cmbRekening').value;
			var TanggalAwal = 1;
			var TanggalAkhir = 0;

			if (Bulan == 1 ||  Bulan == 3 || Bulan == 5 || Bulan == 7 || Bulan == 8 || Bulan == 10 || Bulan == 12)
			TanggalAkhir=31;
			else if (Bulan == 2)
			TanggalAkhir=28;
			else
			TanggalAkhir=30;
			
			if (TanggalAwal<10 ) TanggalAwal= '0'+ TanggalAwal;
			if (TanggalAkhir<10 ) TanggalAkhir= '0'+ TanggalAkhir;
			
			if (Bulan<10 ) Bulan= '0'+ Bulan;
			
			
			var PilihanTglAwal = Tahun + "-" + Bulan + "-" + TanggalAwal;
			var PilihanTglAkhir = Tahun + "-" + Bulan + "-" + TanggalAkhir;


			
			
				//alert ( 'index.php/Cashflow/DPA2CashFlow/search/'+ Rekening +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir);
				var iframe1 = document.getElementById('iFrameCashFlowIuran');				
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashDPA2',
					 { Rekening: 'BPT01DPA2', Bulan: Bulan, Tahun: Tahun,PilihanTglAwal: PilihanTglAwal,PilihanTglAkhir:PilihanTglAkhir },
					 function(response) {  					
					   iframe1.src = '<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2CashFlow/search/'+ 'BPT01DPA2' +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir;
					   //load total in 
					   var txtTotalIuranInDPA2 = document.getElementById('txtTotalIuranInDPA2');
					   var txtTotalIuranOutDPA2 = document.getElementById('txtTotalIuranOutDPA2');
						
						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT01DPA2', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "IN"},
					 		function(response) { 
								txtTotalIuranInDPA2.value = response;
							 }, 	
							 "html"				
						);	

						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT01DPA2', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "OUT"},
					 		function(response) { 
								txtTotalIuranOutDPA2.value = response;
							 }, 	
							 "html"				
						);	

					 },
					 "html"
				);

				var iframe2 = document.getElementById('iFrameCashFlowInv');				
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashDPA2',
					 { Rekening: 'BPT02DPA2', Bulan: Bulan, Tahun: Tahun,PilihanTglAwal: PilihanTglAwal,PilihanTglAkhir:PilihanTglAkhir },
					 function(response) {  					
					   iframe2.src = '<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2CashFlow/search/'+ 'BPT02DPA2' +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir;
					   var txtTotalInvestInDPA2 = document.getElementById('txtTotalInvestInDPA2');
					   var txtTotalInvestOutDPA2 = document.getElementById('txtTotalInvestOutDPA2');
						
						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT02DPA2', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "IN"},
					 		function(response) { 
								txtTotalInvestInDPA2.value = response;
							 }, 	
							 "html"				
						);	

						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT02DPA2', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "OUT"},
					 		function(response) { 
								txtTotalInvestOutDPA2.value = response;
							 }, 	
							 "html"				
						);	
					 },
					 "html"
				);

				var iframe3 = document.getElementById('iFrameCashFlowOpex');				
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashDPA2',
					 { Rekening: 'BPT03DPA2', Bulan: Bulan, Tahun: Tahun,PilihanTglAwal: PilihanTglAwal,PilihanTglAkhir:PilihanTglAkhir },
					 function(response) {  					
					   iframe3.src = '<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2CashFlow/search/'+ 'BPT03DPA2' +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir;
					   var txtTotalOpexInDPA2 = document.getElementById('txtTotalOpexInDPA2');
					   var txtTotalOpexOutDPA2 = document.getElementById('txtTotalOpexOutDPA2');
						
						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT03DPA2', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "IN"},
					 		function(response) { 
								txtTotalOpexInDPA2.value = response;
							 }, 	
							 "html"				
						);	

						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT03DPA2', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "OUT"},
					 		function(response) { 
								txtTotalOpexOutDPA2.value = response;
							 }, 	
							 "html"				
						);	
					 },
					 "html"
				);

				var iframe4 = document.getElementById('iFrameCashFlowMP');				
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashDPA2',
					 { Rekening: 'BPT04DPA2', Bulan: Bulan, Tahun: Tahun,PilihanTglAwal: PilihanTglAwal,PilihanTglAkhir:PilihanTglAkhir },
					 function(response) {  					
					   iframe4.src = '<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2CashFlow/search/'+ 'BPT04DPA2' +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir;
					   var txtTotalMpInDPA2 = document.getElementById('txtTotalMpInDPA2');
					   var txtTotalMpOutDPA2 = document.getElementById('txtTotalMpOutDPA2');
						
						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT04DPA2', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "IN"},
					 		function(response) { 
								txtTotalMpInDPA2.value = response;
							 }, 	
							 "html"				
						);	

						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT04DPA2', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "OUT"},
					 		function(response) { 
								txtTotalMpOutDPA2.value = response;
							 }, 	
							 "html"				
						);	
					 },
					 "html"
				);

				var iframe5 = document.getElementById('iFrameCashFlowCTD');				
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashDPA2',
					 { Rekening: 'BPTDPADPA2', Bulan: Bulan, Tahun: Tahun,PilihanTglAwal: PilihanTglAwal,PilihanTglAkhir:PilihanTglAkhir },
					 function(response) {  					
					   iframe5.src = '<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2CashFlow/search/'+ 'BPTDPADPA2' +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir;
					   var txtTotalCTDInDPA2 = document.getElementById('txtTotalCTDInDPA2');
					   var txtTotalCTDOutDPA2 = document.getElementById('txtTotalCTDOutDPA2');
						
						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPTDPADPA2', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "IN"},
					 		function(response) { 
								txtTotalCTDInDPA2.value = response;
							 }, 	
							 "html"				
						);	

						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA2Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPTDPADPA2', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "OUT"},
					 		function(response) { 
								txtTotalCTDOutDPA2.value = response;
								$('#divLoadingCari').hide();
								$('#divContent').show();
							 }, 	
							 "html"				
						);	
					 },
					 "html"
				);
			
		}
		
		
	
		
	</script>
</html>
