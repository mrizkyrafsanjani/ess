<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">CashFlow DPA1</h3>
		</div>
		<div class="panel-body">
		<?php echo validation_errors(); ?>
		<table border=0>			
			
			<!--<tr>
				<td>Rekening *</td>
				<td colspan=3><select class="form-control" id="cmbRekening">
					<option value='BPT01DPA1'>Rekening Iuran DPA1</option>
					<option value='BPT02DPA1'>Rekening Investasi DPA1</option>
					<option value='BPT03DPA1'>Rekening Opex DPA1</option>
					<option value='BPT04DPA1'>Rekening MP DPA1</option>
					<option value='BPTDPA1'>Rekening CTD DPA1</option>
				</select></td>
			</tr>-->
			<tr>			
			<td>Tahun</td>
				<td colspan=3>
					<select class="form-control" id="cmbTahun" name="cmbTahun">
					<?php 
						for($i=date('Y')+1;$i>date('Y')-2;$i--)
						{
							$selected = '';
							if(date('Y') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".$i."</option>";								
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Bulan</td>
				<td colspan=3>
					<select class="form-control" id="cmbBulan" name="cmbBulan">
					<?php 
						for($i=1;$i<=12;$i++)
						{
							$selected = '';
							if(date('n') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
						}
					?>
					</select>
				</td>
			</tr>
			
			
			
			<tr>
				<td></td>
				<td>
				<div class="col-sm-4">
					<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari"></div>
					<div id="divLoadingCari" hidden class="col-sm-7" style="padding-top:8px"><i id="spinner" class="fa fa-refresh fa-spin"></i>Loading...</div>
				</td>
				</td>
			</tr>
		</table>
		
		<div id="divContent" hidden>
		<H4><b>Rekening Iuran DPA1</b></H4>
		<iframe id="iFrameCashFlowIuran" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1CashFlow" width="100%" height="300px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		Total IN  : <input id="txtTotalIuranInDPA1" type="text" readonly="true">
		Total OUT : <input id="txtTotalIuranOutDPA1" type="text" readonly="true">

		<H4><b>Rekening Investasi DPA1</b></H4>
		<iframe id="iFrameCashFlowInv" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1CashFlow" width="100%" height="300px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		Total IN  : <input id="txtTotalInvestInDPA1" type="text" readonly="true">
		Total OUT : <input id="txtTotalInvestOutDPA1" type="text" readonly="true">

		<H4><b>Rekening Opex DPA1</b></H4>
		<iframe id="iFrameCashFlowOpex" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1CashFlow" width="100%" height="300px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		Total IN  : <input id="txtTotalOpexInDPA1" type="text" readonly="true">
		Total OUT : <input id="txtTotalOpexOutDPA1" type="text" readonly="true">

		<H4><b>Rekening MP DPA1</b></H4>
		<iframe id="iFrameCashFlowMP" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1CashFlow" width="100%" height="300px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		Total IN  : <input id="txtTotalMpInDPA1" type="text" readonly="true">
		Total OUT : <input id="txtTotalMpOutDPA1" type="text" readonly="true">

		<H4><b>Rekening CTD DPA1</b></H4>
		<iframe id="iFrameCashFlowCTD" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1CashFlow" width="100%" height="300px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		Total IN  : <input id="txtTotalCTDInDPA1" type="text" readonly="true">
		Total OUT : <input id="txtTotalCTDOutDPA1" type="text" readonly="true">
		
		</div>
		</div>
	</body>
	<script type="text/javascript">
		function alertsizeBudgetMonitoring(pixels){
			pixels+=35;
			document.getElementById('iFrameCashFlow').style.height=pixels+"px";
		}
		
		function search(){
			$('#divLoadingCari').show();
			var Tahun = document.getElementById('cmbTahun').value;
			var Bulan = document.getElementById('cmbBulan').value;
			//var Rekening = document.getElementById('cmbRekening').value;
			var TanggalAwal = 1;
			var TanggalAkhir = 0;

			//alert (Bulan);

			if (Bulan == 1 ||  Bulan == 3 || Bulan == 5 || Bulan == 7 || Bulan == 8 || Bulan == 10 || Bulan == 12)
				TanggalAkhir=31;
			else if (Bulan == 2)
			TanggalAkhir=28;
			else
				TanggalAkhir=30;
			
			if (TanggalAwal<10 ) TanggalAwal= '0'+ TanggalAwal;
			if (TanggalAkhir<10 ) TanggalAkhir= '0'+ TanggalAkhir;
			
			if (Bulan<10 ) Bulan= '0'+ Bulan;
			
			
			var PilihanTglAwal = Tahun + "-" + Bulan + "-" + TanggalAwal;
			var PilihanTglAkhir = Tahun + "-" + Bulan + "-" + TanggalAkhir;
			
			
			
			/*if(Rekening == '')
			{
				alert('Harap masukkan Rekening yang ingin dicari dengan benar!');
			}
			else	
			{*/
				//alert ( 'index.php/Cashflow/DPA1CashFlow/search/'+ Rekening +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir);
				var iframe1 = document.getElementById('iFrameCashFlowIuran');				
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashDPA1',
					 { Rekening: 'BPT01DPA1', Bulan: Bulan, Tahun: Tahun,PilihanTglAwal: PilihanTglAwal,PilihanTglAkhir:PilihanTglAkhir },
					 function(response) {  					
					   iframe1.src = '<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1CashFlow/search/'+ 'BPT01DPA1' +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir;
					   var txtTotalIuranInDPA1 = document.getElementById('txtTotalIuranInDPA1');
					   var txtTotalIuranOutDPA1 = document.getElementById('txtTotalIuranOutDPA1');
						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT01DPA1', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "IN"},
					 		function(response) {
								txtTotalIuranInDPA1.value = response;
							 },
							 "html"
						);

						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT01DPA1', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "OUT"},
					 		function(response) {
								txtTotalIuranOutDPA1.value = response;
							 },
							 "html"
						);
					 },
					 "html"
				);

				var iframe2 = document.getElementById('iFrameCashFlowInv');
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashDPA1',
					 { Rekening: 'BPT02DPA1', Bulan: Bulan, Tahun: Tahun,PilihanTglAwal: PilihanTglAwal,PilihanTglAkhir:PilihanTglAkhir },
					 function(response) {
					   iframe2.src = '<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1CashFlow/search/'+ 'BPT02DPA1' +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir;
					   var txtTotalInvestInDPA1 = document.getElementById('txtTotalInvestInDPA1');
					   var txtTotalInvestOutDPA1 = document.getElementById('txtTotalInvestOutDPA1');
						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT02DPA1', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "IN"},
					 		function(response) {
								txtTotalInvestInDPA1.value = response;
							 },
							 "html"
						);

						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT02DPA1', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "OUT"},
					 		function(response) {
								txtTotalInvestOutDPA1.value = response;
							 },
							 "html"
						);
					 },
					 "html"
				);

				var iframe3 = document.getElementById('iFrameCashFlowOpex');

				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashDPA1',
					 { Rekening: 'BPT03DPA1', Bulan: Bulan, Tahun: Tahun,PilihanTglAwal: PilihanTglAwal,PilihanTglAkhir:PilihanTglAkhir },
					 function(response) {
					   iframe3.src = '<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1CashFlow/search/'+ 'BPT03DPA1' +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir;
					   var txtTotalOpexInDPA1 = document.getElementById('txtTotalOpexInDPA1');
					   var txtTotalOpexOutDPA1 = document.getElementById('txtTotalOpexOutDPA1');
						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT03DPA1', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "IN"},
					 		function(response) {
								txtTotalOpexInDPA1.value = response;
							 },
							 "html"
						);

						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT03DPA1', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "OUT"},
					 		function(response) {
								txtTotalOpexOutDPA1.value = response;
							 },
							 "html"
						);
					 },
					 "html"
				);

				var iframe4 = document.getElementById('iFrameCashFlowMP');
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashDPA1',
					 { Rekening: 'BPT04DPA1', Bulan: Bulan, Tahun: Tahun,PilihanTglAwal: PilihanTglAwal,PilihanTglAkhir:PilihanTglAkhir },
					 function(response) {
					   iframe4.src = '<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1CashFlow/search/'+ 'BPT04DPA1' +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir;
					   var txtTotalMpInDPA1 = document.getElementById('txtTotalMpInDPA1');
					   var txtTotalMpOutDPA1 = document.getElementById('txtTotalMpOutDPA1');
						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT04DPA1', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "IN"},
					 		function(response) {
								txtTotalMpInDPA1.value = response;
							 },
							 "html"
						);

						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPT04DPA1', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "OUT"},
					 		function(response) {
								txtTotalMpOutDPA1.value = response;
							 },
							 "html"
						);
					 },
					 "html"
				);

				var iframe5 = document.getElementById('iFrameCashFlowCTD');
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashDPA1',
					 { Rekening: 'BPTDPA1', Bulan: Bulan, Tahun: Tahun,PilihanTglAwal: PilihanTglAwal,PilihanTglAkhir:PilihanTglAkhir },
					 function(response) {
					   iframe5.src = '<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1CashFlow/search/'+ 'BPTDPA1' +'/'+ Bulan +'/'+ Tahun +'/'+ PilihanTglAwal +'/'+ PilihanTglAkhir;
					   var txtTotalCTDInDPA1 = document.getElementById('txtTotalCTDInDPA1');
					   var txtTotalCTDOutDPA1 = document.getElementById('txtTotalCTDOutDPA1');
						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPTDPA1', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "IN"},
					 		function(response) {
								txtTotalCTDInDPA1.value = response;
							 },
							 "html"
						);

						$.post(
							// $Rekening, $Bulan, $Tahun, $TipeTransaksi
							'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1Controller/ajax_KalkulasiTotalCashFlow',
					 		{ Rekening: 'BPTDPA1', Bulan: Bulan, Tahun: Tahun, TipeTransaksi: "OUT"},
					 		function(response) {
								txtTotalCTDOutDPA1.value = response;
								$('#divLoadingCari').hide();
								$('#divContent').show();
							 },
							 "html"
						);
					 },
					 "html"
				);
			//}
		}
		
		
	
		
	</script>
</html>
