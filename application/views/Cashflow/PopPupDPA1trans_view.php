<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	
	
		$(function() {		
			
			$("#btnSubmit").click(function(){			
				$.post(
					'<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1DetailTransaksi/ajax_submitCashToday',
					{ tanggal: '<?php echo $tgldata; ?>', koderekening: '<?php echo $kodebank; ?>', tipetransaksi: '<?php echo $tipetrans;?>' },
					function(response){						
						alert(response);		
						window.opener.location.reload();			
					}
				);
			});
		});
	</script>
	<head>
		<title>Cashflow DPA1</title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Input Transaksi</h3>
			</div>
			<div class="panel-body">	

			<?php if ($kodebank == 'BPT02DPA1' or $kodebank=='BPTDPA1') { ?>
				
				Forecast Investasi
				<iframe id="iFrameShowForecash" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1DetailTransaksi/search/<?php echo $tgldata; ?>/<?php echo $kodebank; ?>/<?php echo $tipetrans; ?>" width="100%" height="300px" seamless frameBorder="0">
					<p>Your browser does not support iframes.</p>
				</iframe>			
			
			<?php }?>	
			
				Transaksi 	<?php echo $tgldata; ?>
				<iframe id="iFrameInputTransaksi" src="<?php echo $this->config->base_url(); ?>index.php/Cashflow/DPA1DetailTransaksi/search_trans_dpa1/<?php echo $tgldata; ?>/<?php echo $kodebank; ?>/<?php echo $tipetrans; ?>" width="100%" height="400px" seamless frameBorder="0">
					<p>Your browser does not support iframes.</p>
				</iframe>					
				
				<div class="row">
					<div align="right">
						<button id="btnSubmit" type="button" class="btn btn-primary">Simpan</button>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
</html>
