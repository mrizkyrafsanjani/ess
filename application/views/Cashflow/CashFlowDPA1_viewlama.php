<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function alertsizeAbsensi(pixels){
			pixels+=32;
			document.getElementById('iFrameAbsensi').style.height=pixels+"px";
		}
		
		function search(){
			var DPASelected = document.getElementById('cmbDPA').value;
			var tahun = document.getElementById('cmbTahun').value;
			var bulan = document.getElementById('cmbBulan').value;
			;
			
			$.post(
				'<?php echo $this->config->base_url(); ?>index.php/Absensi/AbsensiController/ajax_GetKehadiran',
				 { npkKaryawan: npkKaryawanSelected, tahun:tahun, bulan:bulan, npk:npk },
				 function(response) {  					
					   $('#kehadiran').html(response['kehadiran']);
					   $('#izin').html(response['izin']);
					   $('#cuti').html(response['cuti']);
					   $('#sakit').html(response['sakit']);
					   $('#perjalanandinas').html(response['perjalanandinas']);
				 },
				 "json"
			);
			
			if(npkKaryawanSelected === '' || npkKaryawanSelected === null)
				npkKaryawanSelected = 'non';
			
			var iframe = document.getElementById('iFrameAbsensi');
			if(npk === '')
			{
				iframe.src = '<?php echo $this->config->base_url(); ?>index.php/Absensi/Absensi/index/'+tahun+'/'+bulan+'/'+npkKaryawanSelected;
			}
			else
			{
				iframe.src = '<?php echo $this->config->base_url(); ?>index.php/Absensi/Absensi/user/'+tahun+'/'+bulan+'/'+npkKaryawanSelected+'/'+npk;
			}
		}
	</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>			
			</div>
			<div class="panel-body">
				<div class="">
					<div class="col-sm-3">
					<label for="cmbDPA" class="control-label">DPA</label>					
					<select id="cmbDPA" class="form-control">					
						<option value="1">Satu</option>
						<option value="2">Dua</option>
						<option value="0">Semua</option>
					</select>					
					</div>

					<tr>
				<div class="col-sm-3">
				<label for="cmbTahun" class="control-label">Tahun</label>
					<select class="form-control" id="cmbTahun" name="cmbTahun">
					<?php 
						for($i=date('Y');$i>date('Y')-2;$i--)
						{
							echo "<option value='".$i."'>".$i."</option>";
						}
					?>
					</select>
				</div>
				<div class="col-sm-3">
				<label for="cmbBulan" class="control-label">Bulan</label>
					<select class="form-control" id="cmbBulan" name="cmbBulan">
					<?php 
						for($i=1;$i<=12;$i++)
						{
							$selected = '';
							if(date('n') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
						}
					?>
					</select>
					</div>
	
					
				

				

				</div>				
			</div>
			<div class="panel-footer clearfix">
				<div class="col-sm-1">				
				<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari">
				</div>
				<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i>Processing</div>
			</div>
		</div>

		<div id="divLaporan" class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Cashflow DPA 1</h3>
            </div>
            <iframe id="iFrameLembur" src="<?php echo $this->config->base_url(); ?>index.php/Lembur/Lembur/viewAdmin" width="100%" height="200px" seamless frameBorder="0">
			
			</iframe>          
		</div>

	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script src="<?php echo $this->config->base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		var dataSubmit;		
		$(document).ready(function() {
			$(".select2").select2();
			$("#dtTanggal").datepicker({ autoclose: true, format: "dd/mm/yyyy"});
			$('#divLaporan').hide();
			$('#divLoading').hide();

			var validate = function(){
				var error = false;
				var dtTanggal = $('#dtTanggal');
				if(!dtTanggal.val()){
					alert("Mohon diisi tanggal realisasi!");
					error = true;
					dtTanggal.focus();
				}
				return error;
			}

			$('#btnSubmit').bind('click', function (e) {
				if(validate())
					return;
				$('#divLoadingSubmit').show();
				$('#divKontenLaporan').html('<i class="fa fa-refresh fa-spin"></i>Processing');
				$('#btnSubmit').prop('disabled', true);
				$.ajax({
					type: "POST",
					url: "<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/RealisasiController/ajax_loadLaporanRealisasi",
					data: {dpa: $('#cmbDPA').val(), tanggal: $('#dtTanggal').val(), jenispengeluaran: $('#cmbJenisPengeluaran').val()},
					success: function (response) {
						$('#divLaporan').show();
						var result = response;//JSON.parse(response);
						$('#divKontenLaporan').html(result);
						$('#btnSubmit').prop('disabled', false);
						$('#divLoadingSubmit').hide();
					}
				});
			});
		});
	</script>
</html>
