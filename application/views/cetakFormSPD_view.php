<html>
	<style type="text/css">
	@import "<?php echo $this->config->base_url(); ?>assets/css/jquery-ori.datepick.css";
	@import "<?php echo $this->config->base_url(); ?>assets/css/cetak.css";
	</style>

		<style>
		@page { size 21cm 29.7cm; margin: 2cm }
		div.page { page-break-after: always }
		</style>

	<!--<script type="text/javascript" src="/spd/assets/js/jquery.js"></script>-->
	<script src="<?php echo $this->config->base_url(); ?>assets/js/jquery-migrate-1.2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<script type="text/javascript">

		$(function() {
			$("#tblUangMuka").hide();
			$("#tblPerkiraanBiaya").hide();
			$('#txtTanggalBerangkat').datepick({dateFormat: 'yyyy-mm-dd'});
			$('#txtTanggalKembali').datepick({dateFormat: 'yyyy-mm-dd'});
			$(".clsUang").formatNumber({format:"#,##0", locale:"us"});
			if($("input:radio[name='rbUangMuka']:checked").val()  == 1 ){
				$("#tblUangMuka").show();
				$("#tblPerkiraanBiaya").show();
			}
			/*$('#printArea').printElement({
					pageTitle:'Form SPD <?php echo $trkSPD_array[0]['nospd']; ?>',
					printMode:'popup',
					overrideElementCSS:[
						'/spd/assets/css/cetak.css','/spd/assets/css/style-common.css']
				});
			*/
			$('.buttonSubmit').click(function (){
				$('#printArea').printElement({
					//leaveOpen: true,
					pageTitle:'Form SPD <?php echo $trkSPD_array[0]['nospd']; ?>',
					printMode:'popup',
					overrideElementCSS:[
						'<?php echo $this->config->base_url(); ?>assets/css/cetak.css','<?php echo $this->config->base_url(); ?>assets/css/style-common.css']
				});
			});
			
			
			
		});
		
		

	</script>
	<head>
		<title>Form SPD</title>
	</head>
	<body>
	<table class="tblPureNoBorder"><tr><td>
	<input class='buttonSubmit' type="button" value="Cetak" id="btnCetak">
	</tr></td></table><br/>
<div id='printArea'>
<div class="page" >
<table border=0>
   <tbody>
	<!-- Results table headers -->
	<tr class="tblHeader">
	  <td colspan="2">Surat Perjalanan Dinas</td>
	  <td colspan="3"><image id="logoDPA" src='../assets/images/logoDPA.png'>DANA PENSIUN ASTRA<br/>
	  <?php 
		$checked11 = "";
		$checked22 = "";
		if($trkSPD_array[0]['DPA'] == 1){
			$checked11 = "checked";
		}else{
			$checked22 = "checked";
		}
	  ?> 
	  <input type="radio" id="rbDpaSatu" name="rbDPA" value="1" <?php echo $checked11; ?> disabled="disabled" >SATU
	  <input type="radio" id="rbDpaDua" name="rbDPA" value="2" <?php echo $checked22; ?> disabled="disabled" >DUA
	  </td>
	</tr>
	<tr>
	  <td class="lbl">No SPD</td>
	  <td><?php echo $trkSPD_array[0]['nospd']; ?></td>
	  <td colspan="3" rowspan="8" align="center">
		<div id="catatan">
			<div class="lblTop">Catatan Penting</div>
			<div id="txtCatatan"><ol><li>Permintaan uang muka perjalanan dinas diajukan selambat-lambatnya 5 hari kerja sebelum melakukan perjalanan dinas.
<li>Pengeluaran uang muka paling lambat diterima 1 hari kerja sebelum keberangkatan (SPD).
<li>Pertanggungjawaban atas uang muka untuk biaya-biaya perjalanan dinas wajib dilaporkan selambat-lambatnya 3 (tiga) hari setelah tiba di tempat kerja.
</ol></div>
      </td>
	</tr>
	<tr>
	  <td class="lbl">Tanggal Pengajuan SPD</td>
	  <td><?php echo $trkSPD_array[0]['tanggalspd']; ?></td>	  
	</tr>
	<tr>
	  <td class="lbl">NPK</td>
	  <td><?php echo $trkSPD_array[0]['npk']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Golongan</td>
	  <td><?php echo $trkSPD_array[0]['golongan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Nama</td>
	  <td><?php echo $trkSPD_array[0]['nama']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Jabatan</td>
	  <td><?php echo $trkSPD_array[0]['jabatan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Departemen</td>
	  <td><?php echo $trkSPD_array[0]['departemen']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Atasan</td>
	  <td><?php echo $trkSPD_array[0]['atasan']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tanggal Keberangkatan</td>
	  <td colspan="4"><?php echo $trkSPD_array[0]['tanggalberangkatspd']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tanggal Kembali</td>
	  <td colspan="4"><?php echo $trkSPD_array[0]['tanggalkembalispd']; ?></td>
	  
	</tr>
	<tr>
	  <td class="lbl">Tujuan</td>
	  <td colspan="4"><?php echo $trkSPD_array[0]['tujuan']; ?></td>
	</tr>
	<tr>
	  <td class="lbl">Alasan Perjalanan</td>
	  <td colspan="4" valign="top"><?php echo $trkSPD_array[0]['alasan']; ?></td>
	</tr>
  </tbody>
</table>
	<!--<tr><td colspan="5">-->
Apakah ingin mengajukan uang muka 
<?php 
	$checked1 = "";
	$checked2 = "";
	if($trkSPD_array[0]['uangmuka'] == 1){
		$checked1 = "checked";
	}else{
		$checked2 = "checked";
	}
?>
<input type="radio" id="rbYa" name="rbUangMuka" value="1" <?php echo $checked1; ?> disabled="disabled" >Ya
<input type="radio" id="rbTidak" name="rbUangMuka" value="0" <?php echo $checked2; ?> disabled="disabled" >Tidak
<!--</td></tr>-->
<!-- part untuk perkiraaan biaya -->
<table id="tblPerkiraanBiaya" border=0>
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Perkiraan Biaya</td>      
    </tr>
    <tr>

      <td class="lbl">Jenis Biaya</td>
      <td id="keterangan" class="lblTop">Keterangan</td>
      <td class="lblTop">Beban Harian</td>
      <td class="lblTop">#Jml Hari</td>
      <td class="lblTop">Total Perkiraan Biaya</td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Saku</td>
      <td><?php echo $trkSPD_array[0]['keteranganspd']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[0]['bebanharianspd']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[0]['jumlahharispd']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[0]['bebanharianspd']*$trkSPD_array[0]['jumlahharispd']; ?></td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Uang Makan</td>
      <td><?php echo $trkSPD_array[1]['keteranganspd']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[1]['bebanharianspd']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[1]['jumlahharispd']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[1]['bebanharianspd']*$trkSPD_array[1]['jumlahharispd']; ?></td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Akomodasi</td>
      <td><?php echo $trkSPD_array[2]['keteranganspd']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[2]['bebanharianspd']; ?></td>
      <td class='clsHari'><?php echo $trkSPD_array[2]['jumlahharispd']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[2]['bebanharianspd']*$trkSPD_array[2]['jumlahharispd']; ?></td>
    </tr>
    <tr>
      <td><br/></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="lbl"><i>Transportasi</i></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Taksi</td>
      <td><?php echo $trkSPD_array[4]['keteranganspd']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[4]['bebanharianspd']; ?></td>
      <td></td>
      <td class='clsUang'><?php echo $trkSPD_array[4]['bebanharianspd']*$trkSPD_array[4]['jumlahharispd']; ?></td>
    </tr>
    <tr>
      <td class="lbl">&nbsp;&nbsp;&nbsp;&nbsp;Airport Tax</td>
      <td><?php echo $trkSPD_array[5]['keteranganspd']; ?></td>
      <td class='clsUang'><?php echo $trkSPD_array[5]['bebanharianspd']; ?></td>
      <td></td>
      <td class='clsUang'><?php echo $trkSPD_array[5]['bebanharianspd']*$trkSPD_array[5]['jumlahharispd']; ?></td>
    </tr>
    <tr>
      <td class="lbl"><i>Lain-lain</i></td>
	  <?php 
		
		for($i=6;$i<count($trkSPD_array);$i++){
			echo "<td>". $trkSPD_array[$i]['keteranganspd'] ."</td>
			  <td class='clsUang'>". $trkSPD_array[$i]['bebanharianspd'] ." </td>
			  <td></td>
			  <td class='clsUang'> " . $trkSPD_array[$i]['bebanharianspd']*$trkSPD_array[$i]['jumlahharispd'] . "</td>
			</tr>
			<tr>
				<td></td>";
		}
		if(count($trkSPD_array)==6){
			echo "<td><br/></td>
			  <td></td>
			  <td></td>
			  <td></td>
			</tr>
			<tr>
				<td></td>";
		}
	  ?>
      <td></td>
      <td class="lbl" colspan="2"><b>Grand Total</b></td>
      <td class='clsUang'><?php echo $trkSPD_array[0]['totalspd']; ?></td>
    </tr>
    <tr>
      <td class="lbl">Total Pengajuan</td>
      <td colspan="4"><label class='clsUang' id="lblTotalPengajuan"><?php echo $trkSPD_array[0]['totalspd']; ?></label></td>
    </tr>
	<tr>
      <td class="lbl">Terbilang</td>
      <td class="clsTerbilang" colspan="4"><label id="lblTerbilang"><?php if($terbilang==''){ echo 'Nol'; } else { echo ucwords($terbilang); } ?></label></td>
    </tr>
  </tbody>
</table>
<br/>
<!-- Permintaan Uang Muka -->
<table id="tblUangMuka" border=0>
  <tbody>
    <tr class="tblHeader">
      <td colspan="5">Permintaan Uang Muka</td>
    </tr>
    <tr>
      <td colspan="5"></td>
    </tr>
    <tr>
      <td width="200px" class="lbl">Nama Pemohon</td>
      <td  colspan="2" width="400px"><?php echo $trkSPD_array[0]['nama']; ?></td>
	  <td align="right" colspan="2" width="200px"><b>Tanggal : </b><?php echo $trkSPD_array[0]['tanggalspd']; ?></td>
    </tr>
    <tr>
      <td class="lbl">Departemen</td>
      <td colspan="2"><?php echo $trkSPD_array[0]['departemen']; ?></td>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td class="lbl">Jumlah Permintaan</td>
      <td colspan="2"><label class='clsUang' id="lblJmlhPermintaan"><?php echo $trkSPD_array[0]['totalspd']; ?></label></td>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td class="lbl">Terbilang</td>
      <td colspan="2" width="400px"><label class="clsTerbilang" id="lblTerbilang2"><?php echo ucwords($terbilang); ?></label></td>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td class="lbl">Keperluan</td>
      <td colspan="2"><?php echo $trkSPD_array[0]['alasan']; ?></td>
      <td colspan="2"></td>
    </tr>
    <tr>
      <td class="lbl">Tujuan</td>
      <td colspan="2"><?php echo $trkSPD_array[0]['tujuan']; ?></td>
      <td colspan="2"></td>
    </tr>
  </tbody>
</table>
<br/>
<!-- tanda tangan -->
<table id="tblTandaTangan"  border=0>
  <tbody>
    <tr class="ttd">
      <td rowspan="2">Pemohon</td>
      <td colspan="2">Menyetujui</td>
    </tr>
    <tr class="ttd">
      <td><?php 
		if($trkSPD_array[0]['atasan']== $trkSPD_array[0]['nama'])
		{
			echo "DIC HRGA";
		}
		else if($trkSPD_array[0]['atasan']=='')
		{ 
			echo "Chief"; 
		}
		else
		{ 
			echo "Atasan"; 
		}
		?></td>
      <td>HRGA Dept Head</td>
    </tr>
    <tr class="ttd">
      <td><br/><br/><br/><br/><br/></td>
      <td></td>
      <td></td>
    </tr>
    <tr class="ttd">
      <td><?php echo $trkSPD_array[0]['nama']; ?></td>
      <td><?php echo $trkSPD_array[0]['atasan']==$trkSPD_array[0]['nama']?"":$trkSPD_array[0]['atasan']; ?></td>
      <td>Eviyati</td>
    </tr>
  </tbody>
</table>
</div>

<BR><BR>
<?php if (($flagemail=='1' or $flagemail=='2') ) {
	echo '<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />';
 } ?>
	

	<?php 
		if($flagemail=='1' or $flagemail=='2'  ){
	?>
<div class="page" > <!-- buat pelimpahan wewenenang kalau ada-->
	<table border=0 class="nonbordered">
		<tbody>
		<!-- Results table headers -->
		<tr class="tblHeaderPW">	  
			<td colspan="1"><image id="header" height="100" width="920" src='<?php echo $this->config->base_url(); ?>assets/images/header_dpa.jpg'><br/>	
		</tr>
		
		</tbody>
	</table>
	<br>
	<table border=0 class="nonbordered"><tr><td>
	<div class=perihal>
		<p><B>Pelimpahan Wewenang</B><BR>
		<B><?php echo $KodePelimpahanWewenang ;?></B></p>
	</div>
	</td></tr></table>
	<BR><BR>
		Sehubungan dengan pengajuan  perjalanan dinas saya selama <u><?php echo $trkSPD_array[0]['jumlahharispd']; ?></u> hari,terhitung sejak tanggal <u><?php echo $trkSPD_array[0]['tanggalberangkatspd']; ?> </u> sampai dengan <u><?php echo $trkSPD_array[0]['tanggalkembalispd']; ?></u> <br> 
		sebagaimana dibuktikan dengan formulir  perjalanan dinas terlampir, maka wewenang atas : <br>
			<?php for($i=0;$i<count($dataPelimpahanWewenangDetail);$i++){					 									
					echo $dataPelimpahanWewenangDetail[$i]['Keterangan'] .'<br />' ; 	
					echo 'Akan dilimpahkan kepada : '. $dataPelimpahanWewenangDetail[$i]['NamaYangDilimpahkan'] . '<br /><br />' ;										  
			}?> 
		<br>
		<!--Mohon dijelaskan secara singkat dan jelas dasar dari adanya pelimpahan wewenang, penerima pelimpahan wewenang, 
		dan jangka waktu dari pelimpahan wewenang tersebut.-->

		<?php 
		if($flagemail=='2'){
		?>
				Selama saya perjalanan dinas, mohon disediakan fitur balas otomatis pada email saya, dengan format :<br>
				<i>
				Thank you for your email.<br>
				I am leave till <u><?php echo $trkSPD_array[0]['tanggalkembalispd']; ?></u>. I will read and reply the email when return to office. <br>
				For urgent matter, please contact 	
				<?php for($i=0;$i<count($dataEmailPelimpahanWewenangDetail);$i++){
					if ( $i==count($dataEmailPelimpahanWewenangDetail)-1) 										
						echo $dataEmailPelimpahanWewenangDetail[$i]['Nama'] . ' at ' . $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju'] . '.';
					else
						echo $dataEmailPelimpahanWewenangDetail[$i]['Nama'] . ' at ' . $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju'] . ' or '; 
					}
				?> <br>
				Thanks and Regards <br>
				<?php echo $trkSPD_array[0]['nama']; ?>
				</i> <br><br>

				Selama saya perjalanan dinas, untuk semua email yang masuk pada email saya, dialihkan kepada : <br>
				<?php for($i=0;$i<count($dataEmailPelimpahanWewenangDetail);$i++){
					if ( $i==count($dataEmailPelimpahanWewenangDetail)-1)
						echo  $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju']  ;
					else
					echo  $dataEmailPelimpahanWewenangDetail[$i]['EmailYangDiTuju'] . ',' ;
				}
				?>
		<?php 
		}
		?>
		<BR><BR>
		<!-- tanda tangan -->
		<table border="1">			
			<tr class="ttdPW">
			<td rowspan="2">Di Buat </td>
			<td >Diketahui</td>
			<td >Disetujui</td>
			</tr>
			<tr class="ttdPW">
			<td>Penerima Pelimpahan Wewenang</td>
			<td>Atasan</td>
			</tr>
			<tr class="ttdPW">
			<td><br/><br/><br/><br/><br/></td>
			<td></td>
			<td></td>
			</tr>
			<tr class="ttdPW">
			<td><?php echo $trkSPD_array[0]['nama']; ?></td>
			<td>
			<?php for($i=0;$i<count($dataPelimpahanWewenangDetail);$i++){	 									
						if ( $i==count($dataPelimpahanWewenangDetail)-1) 
							echo $dataPelimpahanWewenangDetail[$i]['NamaYangDilimpahkan']  ;	
						else
							echo $dataPelimpahanWewenangDetail[$i]['NamaYangDilimpahkan'] . ' , ' ;											  
					}?> 
			</td>
			<td><?php echo $trkSPD_array[0]['atasan']; ?></td>
			</tr>	
		</table>
		<BR><BR>
		*Mohon untuk dilampirkan formulir URF terkait pelimpahan wewenang ini (jika diperlukan).
</div>
	<?php 
		}
	?>

</div>
<div id="note">
<table class="tblPureNoBorder"><tr><td>
	<input class='buttonSubmit' type="button" value="Cetak" id="btnCetak2">
	</tr></td></table>
</div>
	</body>
</html>