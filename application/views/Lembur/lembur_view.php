<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function alertsizeLembur(pixels){
			pixels+=35;
			document.getElementById('iFrameLembur').style.height=pixels+"px";
		}
		function search(){
			var namaKaryawan = document.getElementById('txtNamaKaryawan').value;
			var tahun = document.getElementById('cmbTahun').value;
			var bulan = document.getElementById('cmbBulan').value;
			var npk = '<?php echo $npk; ?>';
			$.post(
				'<?php echo $this->config->base_url(); ?>index.php/Lembur/LemburController/ajax_GetLembur',
				 { namaKaryawan: namaKaryawan, tahun:tahun, bulan:bulan, npk:npk },
				 function(response) {  					
					   //$('#tul').html(response['tul']);
					   $('#totaljamlembur').html(response['totaljamlembur']);
					   $('#tunjanganMakan').html(response['tunjanganmakan']);
					   $('#tunjanganTransportasi').html(response['tunjangantransport']);					   
				 },
				 "json"
			);
			
			if(namaKaryawan === '' || namaKaryawan === null)
				namaKaryawan = 'non';
			
			var iframe = document.getElementById('iFrameLembur');
			if(npk === '')
			{
				iframe.src = '<?php echo $this->config->base_url(); ?>index.php/Lembur/Lembur/viewAdmin/'+tahun+'/'+bulan+'/'+namaKaryawan;
			}
			else
			{
				iframe.src = '<?php echo $this->config->base_url(); ?>index.php/Lembur/Lembur/viewUser/'+tahun+'/'+bulan+'/'+namaKaryawan+'/'+npk;
			}
			
			
				
		}
	</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div style="width:100%;text-align:right;display:block;"><a href="#" onclick="window.open('<?php echo $this->config->base_url(); ?>assets/images/wf/LemburWorkFlow.jpg','', 'width=600, height=400')" >Proses Flow Lembur</a></div>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Lembur Karyawan</h3>
		</div>
		<div class="panel-body">
		<?php echo validation_errors(); ?>
		<table border=0>
			<?php 
				if($npk == ''){
			?>
			<tr>
				<td>Nama Karyawan *</td>
				<td><select class="form-control" id="txtNamaKaryawan">
				<?php foreach($databawahan as $row){
					echo "<option value='".$row->NPK."'>".$row->Nama."</option>";
				} ?>
				</select></td>
			</tr>
			<?php }else{ ?>
			<tr>
				<td></td>
				<td><input type="hidden" class="form-control" id="txtNamaKaryawan"></td>
			</tr>
			<?php } ?>
			<tr>
				<td>Tahun</td>
				<td>
					<select class="form-control" id="cmbTahun" name="cmbTahun">
					<?php 
						for($i=date('Y');$i>date('Y')-2;$i--)
						{
							echo "<option value='".$i."'>".$i."</option>";
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Bulan</td>
				<td>
					<select class="form-control" id="cmbBulan" name="cmbBulan">
					<?php 
						for($i=1;$i<=12;$i++)
						{
							$selected = '';
							if(date('n') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari">
				</td>
			</tr>
		</table>
		<?php 
			if($npk == '' && $admin == '1'){
		?>
		<iframe id="iFrameLembur" src="<?php echo $this->config->base_url(); ?>index.php/Lembur/Lembur/viewAdmin" width="100%" height="200px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		<?php }else{ ?>
		<iframe id="iFrameLembur" src="<?php echo $this->config->base_url(); ?>index.php/Lembur/Lembur/viewUser/non/non/non/<?php echo $npk; ?>/<?php echo $kodeusertask; ?>" width="100%" height="200px" seamless frameBorder="0">
		  <p>Your browser does not support iframes.</p>
		</iframe>		
		<?php } ?>
		
		<?php if($admin != 1){ ?>
		<table border=0>
			<tr>
				<td>Total Jam Lembur</td>
				<td><div id = 'totaljamlembur'><?php echo $totaljamlembur; ?></div></td>
			</tr>
			<!-- <tr>
				<td>Total Tunj. Makan</td>
				<td><div id = 'tunjanganMakan'><?php echo $tunjanganmakan; ?></div></td>
			</tr>
			<tr>
				<td>Total Tunj. Transportasi</td>
				<td><div id = 'tunjanganTransportasi'><?php echo $tunjangantransport; ?></div></td>
			</tr> -->
		</table>
		<?php } ?>
		</div>
		</div>
	</body>
</html>
