<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	$(function() {
		$("#btnDecline").click(function(){
			var txtCatatan = $('#txtCatatan');
			
			if(txtCatatan.val() === '')
			{
				alert('Catatan harus diisi!');
			}
			else
			{
			
				$.post(
					<?php if($realisasi == '1'){ ?>
					'<?php echo $this->config->base_url(); ?>index.php/Lembur/LemburController/ajax_prosesApprovalRealisasi',
					<?php }else{ ?>
					'<?php echo $this->config->base_url(); ?>index.php/Lembur/LemburController/ajax_prosesApproval',
					<?php } ?>
					 { kodeusertask: '<?php echo $kodeusertask; ?>', status: 'DE', catatan: txtCatatan.val() },
					 function(response) {
						alert(response);
						/*if (response == 1) {
						   alert('Decline berhasil');						   
						}else{
						   alert('Sistem gagal.');
						}*/
						window.location.replace('<?php echo site_url('UserTask'); ?>');
					 }
				);
			
			}
		});
		
		$("#btnApprove").click(function(){
			var txtCatatan = $('#txtCatatan');
			$.post( 
				<?php if($realisasi == '1'){ ?>
				'<?php echo $this->config->base_url(); ?>index.php/Lembur/LemburController/ajax_prosesApprovalRealisasi',
				<?php }else{ ?>
				'<?php echo $this->config->base_url(); ?>index.php/Lembur/LemburController/ajax_prosesApproval',
				<?php } ?>
				 { kodeusertask: '<?php echo $kodeusertask; ?>', status: 'AP',catatan: txtCatatan.val() },
				 function(response) {
					alert(response);
					/*
					if (response == 1) {
					   alert('Approve berhasil');						   
					}else{
					   alert('Sistem gagal.');
					}
					*/
					window.location.replace('<?php echo site_url('UserTask'); ?>');
				 }
			);
		});
	});
	</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Approval Lembur</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						Nama Karyawan :
					</div>
					<div class="col-md-6">
						<?php echo $nama; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Hari :
					</div>
					<div class="col-md-6">
						<?php echo $hari; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Tanggal :
					</div>
					<div class="col-md-6">
						<?php echo $tanggal; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						(Rencana) Dari Pukul :
					</div>
					<div class="col-md-2">
						<?php echo $jammulairencana; ?>
					</div>
					<?php if($jammulairealisasi != ''){ ?>
					<div class="col-md-3">
						(Realisasi) Dari Pukul :
					</div>
					<div class="col-md-2">
						<?php echo $jammulairealisasi; ?>
					</div>
					<?php } ?>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						(Rencana) Sampai Pukul :
					</div>
					<div class="col-md-2">
						<?php echo $jamselesairencana; ?>
					</div>
					
					<?php if($jamselesairealisasi != ''){ ?>
					<div class="col-md-3">
						(Realisasi) Sampai Pukul :
					</div>
					<div class="col-md-2">
						<?php echo $jamselesairealisasi; ?>
					</div>
					<?php } ?>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Tugas Yang Dikerjakan :
					</div>
					<div class="col-md-6">
						<?php echo $tugaslembur; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Total Akumulasi Jam Lembur Sampai Dengan Saat Ini :
					</div>
					<div class="col-md-6">
						<?php echo $akumulasilemburperbulan; ?> jam
					</div>
				</div>
				<br/>
				<div class="row">
				<?php if($jamselesairealisasi == ''){ ?>
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				<?php } ?>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						History Catatan :
					</div>
					<div class="col-md-9">
						<?php echo $historycatatan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div align="right">
						<button id="btnApprove" type="button" class="btn btn-primary">Approve</button>
						<?php if($realisasi != '1'){ ?>
						<button id="btnDecline" type="button" class="btn ">Decline</button>
						<?php }; ?>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
</html>
