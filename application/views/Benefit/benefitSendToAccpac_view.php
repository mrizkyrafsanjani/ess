<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	function alertsizeBenefit(pixels){
		pixels+=32;
		document.getElementById('iFrameBenefitSendToAccpac').style.height=pixels+"px";
	}
	$(function() {
		$("#btnSendToAccpac").click(function(){
			if(confirm("Apakah Anda yakin?")){				
				$.post( 
					'<?php echo $this->config->base_url(); ?>index.php/Benefit/BenefitController/ajax_prosesSendToAccpac',
					 { status: 'AP', deskripsi: $('#txtDeskripsi').val() },
					 function(response) {
						alert(response);
						window.location.replace('<?php echo site_url('Benefit/BenefitController/BenefitSendToAccpac'); ?>');
					 }
				);
			}
		});
	});
	</script>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						Deskripsi Invoice :
					</div>
					<div class="col-md-6">
						<input id="txtDeskripsi" type="text" class="form-control" value="Klaim Tunjangan Karyawan <?php echo date("M'y"); ?>" />
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Metode Pembayaran :
					</div>
					<div class="col-md-6">
						Others
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Tanggal :
					</div>
					<div class="col-md-6">
						<?php echo date("d M Y"); ?>
					</div>
				</div>
				<br/>
				
				<iframe id="iFrameBenefitSendToAccpac" src="<?php echo $this->config->base_url(); ?>index.php/Benefit/benefitSendToAccpac" width="100%" height="200px" seamless="seamless">
				  <p>Your browser does not support iframes.</p>
				</iframe>
				
				<div class="row">
					<div align="right">
						<button id="btnSendToAccpac" type="button" class="btn btn-primary">Kirim Ke Accpac</button>						
					</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
</html>
