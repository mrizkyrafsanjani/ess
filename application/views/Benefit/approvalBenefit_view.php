<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	/* function alertsizeCutiJenisTidakHadir(pixels){
		pixels+=32;
		document.getElementById('iFrameCutiJenisTidakHadir').style.height=pixels+"px";
	} */
	$(function() {
		var txtCatatan = $('#txtCatatan');
		var txtTotalBayar = $('#txtTotalBayar');
		$("#btnDecline").click(function(){			
			if(txtCatatan.val() === '')
			{
				alert('Catatan harus diisi!');
			}
			else
			{
				if(confirm("Apakah Anda yakin?")){
					$.post(
						'<?php echo $this->config->base_url(); ?>index.php/Benefit/BenefitController/ajax_prosesApproval',
						 { kodeusertask: '<?php echo $kodeusertask; ?>', status: 'DE', catatan: txtCatatan.val() },
						 function(response) {
							alert(response);
							window.location.replace('<?php echo site_url('UserTask'); ?>');
						 }
					);
				}
			}
		});
		
		$("#btnApprove").click(function(){
			if(txtTotalBayar.val() > <?php echo $TotalKlaim; ?>){
				alert("Nilai total bayar harus lebih kecil dari total klaim");
			}
			else
			{ 
				if(confirm("Apakah Anda yakin?")){
					var txtCatatan = $('#txtCatatan');
					$.post( 
						'<?php echo $this->config->base_url(); ?>index.php/Benefit/BenefitController/ajax_prosesApproval',
						 { kodeusertask: '<?php echo $kodeusertask; ?>', status: 'AP',catatan: txtCatatan.val(), totalbayar: txtTotalBayar.val()  },
						 function(response) {
							alert(response);
							window.location.replace('<?php echo site_url('UserTask'); ?>');
						 }
					);
				}
			}
		});
	});
	</script>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo $title; ?></h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						Nama Karyawan :
					</div>
					<div class="col-md-6">
						<?php echo $Nama; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Jenis Benefit :
					</div>
					<div class="col-md-6">
						<?php echo $JenisBenefit; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Tanggal Klaim :
					</div>
					<div class="col-md-6">
						<?php echo $TanggalKlaim; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Klaim Untuk :
					</div>
					<div class="col-md-6">
						<?php echo $KlaimUntuk; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Hubungan Keluarga :
					</div>
					<div class="col-md-6">
						<?php echo $HubunganKeluarga; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Total Klaim :
					</div>
					<div class="col-md-6">
						<?php echo "Rp. ".number_format($TotalKlaim,0,",","."); ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
					<div class="col-md-3">
						Total Bayar :
					</div>
					<div class="col-md-6">
						<?php if($TotalBayar <> 0){
							echo "Rp. ".number_format($TotalBayar,0,",",".");
						?>
							<input type="hidden" id="txtTotalBayar" class="form-control" value="<?php echo $TotalBayar ?>" />
						<?php
						}
						else
						{ ?>
							<input type="text" id="txtTotalBayar" class="form-control" value="<?php echo $TotalBayar ?>" />
						<?php } ?>
					</div>
				</div>
				<br/>
				
				<?php if($JenisBenefit == "Medical"){ ?>
				<div class="row">
					<div class="col-md-3">
						Sisa Jatah Medical :
					</div>
					<div class="col-md-6">
						<?php echo "Rp. ".number_format($SisaJatahMedical,0,",","."); ?>
					</div>
				</div>
				<br/>
				<?php } ?>
				<div class="row">
					<div class="col-md-3">
						Keterangan :
					</div>
					<div class="col-md-6">
						<?php echo $Keterangan; ?>
					</div>
				</div>
				<br/>
				
				<div class="row">
				
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						History Catatan :
					</div>
					<div class="col-md-9">
						<?php echo $historycatatan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div align="right">
						<button id="btnApprove" type="button" class="btn btn-primary">Approve</button>
						<button id="btnDecline" type="button" class="btn ">Decline</button>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
</html>
