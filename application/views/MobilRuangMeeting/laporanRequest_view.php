<html>
	<style type="text/css">
	</style>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
		function alertsizeRM(pixels){
			pixels+=32;
			document.getElementById('iFrameRM').style.height=pixels+"px";
		}
		
		function search(){
			var JenisRequest = document.getElementById('cmbJenisRequest').value;
			var tahun = document.getElementById('cmbTahun').value;
			var bulan = document.getElementById('cmbBulan').value;
			var npk = '<?php echo $npk; ?>';
			
			if(JenisRequest === '' || JenisRequest === null)
				JenisRequest = 'non';
			
			var iframe = document.getElementById('iFrameRM');
			
			iframe.src = '<?php echo $this->config->base_url(); ?>index.php/MobilRuangMeeting/RequestMobilRuangMeeting/viewUser/'+tahun+'/'+bulan+'/'+JenisRequest+'/'+npk;
						
		}
		
	
	</script>
	<head>
		<title><?php $title ?></title>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Laporan Request Mobil dan Ruang Meeting</h3>
		</div>
		<div class="panel-body">
		<?php echo validation_errors(); ?>
		<table border=0>
			<!-- <tr>
				<td>Request</td>
				<td><select class="form-control" id="txtJenisRequest">
				<?php foreach($jenisrequest as $row){
					echo "<option value='".$row->JenisRequest."</option>";
				} ?>
				</select></td>
			</tr> -->
			
				
			<tr>
				<td>Tahun</td>
				<td>
					<select class="form-control" id="cmbTahun" name="cmbTahun" >
					<?php 
						for($i=0;$i<5;$i++)
					{
						$tahun = date('Y') + $i;
						echo "<option value='$tahun'>$tahun</option>";
					}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Bulan</td>
				<td>
					<select class="form-control" id="cmbBulan" name="cmbBulan">
					<?php 
						for($i=1;$i<=12;$i++)
						{
							$selected = '';
							if(date('n') == $i )
								$selected = 'selected';
							echo "<option value='".$i."' $selected>".date("F",mktime(0,0,0,$i,10))."</option>";
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
		  <td class="lbl">Request</td>
		  <td>
		    <select class="form-control" id="cmbJenisRequest" name="cmbJenisRequest">
				<option value='Mobil' >Mobil</option>
				<option value='RuangMeeting' >Ruang Meeting</option>
			</select>
		  </td>
		</tr>
			<tr>
				<td></td>
				<td>
					<input class="btn btn-primary" type="button" onClick="search()" id="btnCari" name="" value="Cari">
				</td>
			</tr>
			
		</table>
		<?php 
			if($npk == '' && $admin == '1'){
		?>
		<iframe id="iFrameRM" src="<?php echo $this->config->base_url(); ?>index.php/MobilRuangMeeting/RequestMobilRuangMeeting/viewAdmin" width="100%" height="200px" seamless="seamless">
		  <p>Your browser does not support iframes.</p>
		</iframe>
		<?php }else{ ?>
		<iframe id="iFrameRM" src="<?php echo $this->config->base_url(); ?>index.php/MobilRuangMeeting/RequestMobilRuangMeeting/viewUser/non/non/non/<?php echo $npk; ?>/<?php echo $kodeusertask; ?>" width="100%" height="300px" seamless="seamless">
		  <p>Your browser does not support iframes.</p>
		</iframe>		
		<?php } ?>
		
		</div>
		</div>
	</body>
</html>
