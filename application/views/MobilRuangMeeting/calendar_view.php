<html>
<header>
<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.css">
<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
<style>
  .btn {
      background-color: #5cb85c;
      border: none;
      color: white;
      padding: 12px 16px;
      font-size: 16px;
      cursor: pointer;
  }

  #tipeList {
    height: 46px;
  }

  /* Darker background on mouse-over */
  .btn:hover {
      background-color: Green;
  }
</style>
</header>
<body>

<div class="btn-group">
    <button class="btn" onclick="openAddPeminjaman()" id="btnAdd" name ="btnAdd" value=""><i class="fa fa-plus"></i>Add Request </button>
  </div>
<div class="btn-group">
    <select class="form-control" id="tipeList" name="tipeList">
      <option <?php echo $tipe == "All"? "selected":"" ?> value="All">All</option>
      <option <?php echo $tipe == "Mobil"? "selected":"" ?> value="Mobil">Mobil</option>
      <option <?php echo $tipe == "Ruang Meeting"? "selected":"" ?> value="Ruang Meeting">Ruang Meeting</option>
    </select>
  </div>
<div id='calendar'></div>
</body>
<script>
  $(document).ready(function(){
      $('#tipeList').on('change',function(){
        var selectedTipe = $("#tipeList option:selected").text();
        var addBtnTrxt = document.get
        var events= "<?php echo $this->config->base_url(); ?>index.php/MobilRuangMeeting/requestMobilRuangMeeting/getCalendar/"+selectedTipe;
        $('#calendar').fullCalendar( 'removeEvents')
        $('#calendar').fullCalendar('removeEventSources'); 

        if($('#calendar').fullCalendar( 'clientEvents') == "") {  $('#calendar').fullCalendar( 'addEventSource', events); } // load the new source if the Calendar is empty

        $('#calendar').fullCalendar('refetchEvents');
        //load the eventSource which is always one event source 
      });

  });
  $(function() {
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      nextDayThreshold: '00:00:00',
      eventClick: function(eventObj) {
        if (eventObj.url) {
          window.open(eventObj.url);
          return false; 
        }
      },
      eventRender: function(eventObj, $el) {
        $el.popover({
          title: eventObj.title,
          content: eventObj.status,
          trigger: 'hover',
          placement: 'top',
          container: 'body'
        });
        $el.find('div.fc-title').html($el.find('div.fc-title').text());
      },
      events: "<?php echo $this->config->base_url(); ?>index.php/MobilRuangMeeting/requestMobilRuangMeeting/getCalendar/<?php echo $tipe; ?>",
      timeFormat: 'H(:mm)t'
    });
  });

</script>
<script>
	
  function openAddPeminjaman(kdevent) 
  {
      window.location.href = "<?php echo site_url('MobilRuangMeeting/requestMobilRuangMeeting/index/add');?>";
  }
</script>
</html>