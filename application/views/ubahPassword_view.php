<html>
	<style type="text/css">
	@import "../assets/css/jquery-ori.datepick.css";
	@import "../assets/css/cetak.css";
	@import "../assets/css/tablesorter.css";
	</style>
	<!--<script type="text/javascript" src="../assets/js/jquery.js"></script>-->

	
	<head>
	<script type="text/javascript">		
		$(function() {
			if($('#divError').html() != ''){
				if($('#divError').html().indexOf('Password change success!') != -1){
					$('#divError').hide();
					$('#divSuccess').show();
				}else{
					$('#divError').show();
				}
			} 

			document.getElementById('btnSubmit').disabled = true;
			$('#formChangePassword').bind('keyup click change', function(){
				//var passwordRegex = "/^[a-z0-9_-]{6,18}$/";
				//var re="[';=<>%#*!+`///-]";
				var re2="[A-Za-z0-9]";
				if (!document.getElementById('newPassword').value.match(re2)){
					//$('#divError').html('Password must contains at least one upper case letter, one lower case letter, and one digit');
					//document.getElementById('btnSubmit').disabled = true;
				}else if ($('#newPassword').val() != $('#retypePassword').val()){
					$('#divError').html('Retype password tidak sama dengan new password');
					$('#divError').show();
					$('#divRetypePass').addClass('has-error');
					document.getElementById('btnSubmit').disabled = true;
				}else if($('#oldPassword').val() == "" || $('#newPassword').val() == "" || $('#retypePassword').val() == ""){
					document.getElementById('btnSubmit').disabled = true;
				}else{
					clearError();
					document.getElementById('btnSubmit').disabled = false;
				}
			});
			
			$('#btnReset').click(function() {
				$('#oldPassword').val("");
				$('#newPassword').val('');
				$('#retypePassword').val('');
				clearError();				
			});

			$('#btnSubmit').click(function(){
				Pace.restart();
			});

			var clearError = function(){
				$('#divError').html('');
				$('#divError').hide();
				$('.form-group').removeClass('has-error');
				$('#divSuccess').hide();
			}
		});

	</script>
	
	</head>
	<body>
		<br/>
		<br/>
		<br/>
		<div class="col-md-2">
		</div>
		<div class="col-md-8">
			<div class="box box-info">
			<div class="box-header with-border">
              <h3 class="box-title">Reset Password</h3>
            </div>

			<form id="formChangePassword" action="submitNewPassword" method="post" class="form-horizontal" >
			<div class="box-body">
				<div id="divOldPass" class="form-group">
                  <label for="oldPassword" class="col-sm-3 control-label">Old Password</label>
                  <div class="col-sm-8">
                    <input type="password" size="25" class="form-control" id="oldPassword" name="oldPassword" >
                  </div>
				</div>
				<div id="divNewPass" class="form-group">
				  <label for="newPassword" class="col-sm-3 control-label">New Password</label>
                  <div class="col-sm-8">
                    <input type="password" size="25" class="form-control" id="newPassword" name="newPassword" >
                  </div>
                </div>
				<div id="divRetypePass" class="form-group">
				  <label for="retypePassword" class="col-sm-3 control-label">Retype Password</label>
                  <div class="col-sm-8">
                    <input type="password" size="25" class="form-control" id="retypePassword" name="retypePassword" >
                  </div>
                </div>
				<div id='divError' class="callout callout-danger" hidden="true" ><?php echo validation_errors(); ?></div>
				<div id='divSuccess' class="callout callout-success" hidden="true">Password berhasil dirubah!</div>
			</div>
			<div class="box-footer">
				<button id='btnReset' type="button" class="btn btn-default">Reset</button>
				<button id='btnSubmit' type="submit" class="btn btn-primary pull-right">Simpan</button>
			</div>
			
			</form>
			</div>
		</div>
	</body>
</html>