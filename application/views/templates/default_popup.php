<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <style>
    iframe {
      border:0;
    }
  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ESS DPA </title>
  <link rel="shortcut icon" href=<?php echo str_replace('index.php/','',site_url("favicon.ico")); ?> />
  
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/adminlte/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>dist/css/skins/skin-blue.min.css">
  <script src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<?php echo $body; ?>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo $this->config->base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $this->config->base_url(); ?>dist/js/app.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->

	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.fancybox.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url(); ?>assets/css/ripple-effect.css">
<script type="text/javascript">
	$(function(){				
		// MAD-RIPPLE // (jQ+CSS)
		$(document).on("mousedown", "[data-ripple]", function(e) {			
			var $self = $(this);
			
			if($self.is(".btn-disabled")) {
			return;
			}
			if($self.closest("[data-ripple]")) {
			e.stopPropagation();
			}
			
			var initPos = $self.css("position"),
				offs = $self.offset(),
				x = e.pageX - offs.left,
				y = e.pageY - offs.top,
				dia = Math.min(this.offsetHeight, this.offsetWidth, 100), // start diameter
				$ripple = $('<div/>', {class:"ripple", appendTo:$self});
			
			if(!initPos || initPos==="static") {
			$self.css({position:"relative"});
			}
			
			$('<div/>', {
				class : "rippleWave",
				css : {
					background: $self.data("ripple"),
					width: dia,
					height: dia,
					left: x - (dia/2),
					top: y - (dia/2),
				},
				appendTo : $ripple,
				one : {
					animationend : function(){
					$ripple.remove();
					}
				}
			});
		});
	});		
	</script>
</body>
</html>
