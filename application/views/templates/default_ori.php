<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.js"></script>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.fancybox.js"></script>-->


	<script type="text/javascript">
	$(function(){				
		// MAD-RIPPLE // (jQ+CSS)
		$(document).on("mousedown", "[data-ripple]", function(e) {			
			var $self = $(this);
			
			if($self.is(".btn-disabled")) {
			return;
			}
			if($self.closest("[data-ripple]")) {
			e.stopPropagation();
			}
			
			var initPos = $self.css("position"),
				offs = $self.offset(),
				x = e.pageX - offs.left,
				y = e.pageY - offs.top,
				dia = Math.min(this.offsetHeight, this.offsetWidth, 100), // start diameter
				$ripple = $('<div/>', {class:"ripple", appendTo:$self});
			
			if(!initPos || initPos==="static") {
			$self.css({position:"relative"});
			}
			
			$('<div/>', {
				class : "rippleWave",
				css : {
					background: $self.data("ripple"),
					width: dia,
					height: dia,
					left: x - (dia/2),
					top: y - (dia/2),
				},
				appendTo : $ripple,
				one : {
					animationend : function(){
					$ripple.remove();
					}
				}
			});
		});
	});
		/*
		$(function() {
			$("#container").css({ opacity: 0, "padding-left":'30px' });
			$("#container").animate({
				opacity: 1,
				"padding-left": '20px'
			  }, 200 );
		
			$("#feedback").fancybox({
				'width'				: '75%',
				'height'			: '75%',
				'autoScale'     	: false,
				'transitionIn'		: 'none',
				'transitionOut'		: 'none',
				'type'				: 'iframe'
			});
		});
		*/
	</script>
   <head>
		<link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url(); ?>assets/css/style-common.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url(); ?>assets/css/jquery.fancybox.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url(); ?>assets/css/notifikasi.css">
		<title><?php echo $title; ?></title>
		<link rel="shortcut icon" href=<?php echo str_replace('index.php/','',site_url("favicon.ico")); ?> />
   </head>
   <body>
		<div id="tv-bg-frame"></div>
		<div id="tv-topbar">
			<div class="tv-topbar-container">
				<div id="tv-top-logo" title="Dana Pensiun Astra"></div>
				<div id="tv-topbar-content">
					<!--<ul id="tv-topbar-links"> -->
						<!--<div id="navigation">-->
						
						<!--</div>-->
					<!--</ul>-->
				</div>
			</div>
		</div>
		<?php $this->load->view('home_view'); ?>
		<div id="container">
			<br/>
			<?php echo $body; ?>
		</div>
		<div id="footer" style="text-align: center">
			<div id='copyright'>BPIT DPA &copy;2013 - <?php echo strftime("%Y"); ?><br/>
			<!--<a id="feedback" href="<?php echo $this->config->base_url(); ?>index.php/feedback">give feedback</a>--></div>
		</div>
   </body>
</html>