<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <style>
    iframe {
      border:0;
    }
  </style>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ESS DPA - <?php echo $title; ?></title>
  <link rel="shortcut icon" href=<?php echo str_replace('index.php/','',site_url("favicon.ico")); ?> />
  
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/adminlte/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>dist/css/skins/skin-blue.min.css">
  <!-- Pace style -->
  <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>plugins/pace/pace.min.css">
  <script src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- fullCalendar -->
  <script src="<?php echo $this->config->base_url(); ?>assets/bower_components/moment/moment.js"></script>
  <script src="<?php echo $this->config->base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="<?php echo $this->config->base_url(); ?>index.php/dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>ESS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>ESS</b> DPA</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- /.messages-menu -->

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?php 
              
                  $datausertask = $this->db->query("SELECT * FROM usertasks WHERE deleted = 0 AND StatusApproval = 'PE' AND Username = ?",$npk);
                  $jumlahnotif=$datausertask->num_rows();
                  
                  if($jumlahnotif > 0){
                    echo $jumlahnotif;
                  }else{
                    echo "0";
                  }
              ?></span>
            </a>
            <ul class="dropdown-menu">
              <!--<li class="header">Terdapat 10 task saya</li>-->
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                  <li><!-- start notification -->
                    <a href="<?php echo $this->config->base_url(); ?>index.php/UserTask">
                      <i class="fa fa-users text-aqua"></i> <?php echo $jumlahnotif; ?> usertask yang harus dikerjakan
                    </a>
                  </li>
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="<?php echo $this->config->base_url(); ?>index.php/UserTask">View all</a></li>
            </ul>
          </li>
          <!-- Tasks Menu -->
          
          <?php 
              $datauser = $this->db->query("SELECT urlFoto FROM mstruser WHERE deleted = 0 AND  NPK = ?",$npk);
              foreach($datauser->result() as $usr){
                $urlfoto = $usr->urlFoto;
              }
              $urlFotoFinal = $this->config->base_url() . $urlfoto;               
          ?>
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="<?php echo $urlFotoFinal; ?>" class="user-image" alt="User Image" onerror="this.src='<?php echo $this->config->base_url(); ?>/assets/images/nophoto.jpg'">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><?php echo $nama; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="<?php echo $urlFotoFinal; ?>" class="img-circle" alt="User Image" onerror="this.src='<?php echo $this->config->base_url(); ?>/assets/images/nophoto.jpg'">

                <p>
                  <?php echo $nama; ?>
                  <!--<small>Member since Nov. 2012</small>-->
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <!--<div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>-->
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo $this->config->base_url(); ?>index.php/DataPribadi/DataPribadiController/EditDataPribadiUser" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo $this->config->base_url(); ?>index.php/home/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!--<li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">		
      <!-- Sidebar user panel (optional) -->
      
      <!-- search form (Optional) -->
      <!--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      <?php $this->load->view('home_view'); ?>
      
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!--<section class="content-header">
      <h1>
        Page Header
        <small>Optional description</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>-->

    <!-- Main content -->
    <section class="content">
      <!-- Your Page Content Here -->
		  <?php echo $body; ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="#">Dana Pensiun Astra</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo $this->config->base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $this->config->base_url(); ?>dist/js/app.min.js"></script>

<!-- PACE -->
<script src="<?php echo $this->config->base_url(); ?>plugins/pace/pace.min.js"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->

	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.fancybox.js"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url(); ?>assets/css/ripple-effect.css">
<script type="text/javascript">
	$(function(){				
		// MAD-RIPPLE // (jQ+CSS)
		$(document).on("mousedown", "[data-ripple]", function(e) {			
			var $self = $(this);
			
			if($self.is(".btn-disabled")) {
			return;
			}
			if($self.closest("[data-ripple]")) {
			e.stopPropagation();
			}
			
			var initPos = $self.css("position"),
				offs = $self.offset(),
				x = e.pageX - offs.left,
				y = e.pageY - offs.top,
				dia = Math.min(this.offsetHeight, this.offsetWidth, 100), // start diameter
				$ripple = $('<div/>', {class:"ripple", appendTo:$self});
			
			if(!initPos || initPos==="static") {
			$self.css({position:"relative"});
			}
			
			$('<div/>', {
				class : "rippleWave",
				css : {
					background: $self.data("ripple"),
					width: dia,
					height: dia,
					left: x - (dia/2),
					top: y - (dia/2),
				},
				appendTo : $ripple,
				one : {
					animationend : function(){
					$ripple.remove();
					}
				}
			});
		});
	});		
	</script>
</body>
</html>
