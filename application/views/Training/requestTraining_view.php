<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	function btnApprove_click(){
			var txtCatatan = $('#txtCatatan');
			if(confirm('Apakah Anda yakin ingin approval data ini?')==true)
			{
				$.post( 
					<?php if($realisasi == '1'){ ?>
					'<?php echo $this->config->base_url(); ?>index.php/Training/trainingController/ajax_prosesApprovalRealisasi',
					<?php }else{ ?>
					'<?php echo $this->config->base_url(); ?>index.php/Training/trainingController/ajax_prosesApproval',
					<?php } ?>
				 { kodeusertask: '<?php echo $kodeusertask; ?>', status: 'AP',catatan: txtCatatan.val() },
				 function(response) {
					alert(response);
					window.location.replace('<?php echo site_url('UserTask'); ?>');
				 }
			);
			}
		}
	
	function btnDecline_click(){
			var txtCatatan = $('#txtCatatan');
			
			if(txtCatatan.val() === '')
			{
				alert('Catatan harus diisi!');
			}
			else
			{
				if(confirm('Apakah Anda yakin ingin membatalkan data ini?')==true)
				{
					$.post(
					<?php if($realisasi == '1'){ ?>
					'<?php echo $this->config->base_url(); ?>index.php/Training/trainingController/ajax_prosesApprovalRealisasi',
					<?php }else{ ?>
					'<?php echo $this->config->base_url(); ?>index.php/Training/trainingController/ajax_prosesApproval',
					<?php } ?>
					 { kodeusertask: '<?php echo $kodeusertask; ?>', status: 'DE', catatan: txtCatatan.val() },
					 function(response) {
						alert(response);
						window.location.replace('<?php echo site_url('UserTask'); ?>');
					 }
				);
				}
			}
		}
	
	function btnCetak_click(){
			windowCetak = window.open('<?php echo site_url("Training/trainingController/CetakTraining/$kodeusertask")?>',"Cetak Training Need Analysis","width=200, height=100");
			window.setTimeout(function(){ windowCetak.close(); }, 1500);
		}
		
		
	//<a href='<?php echo site_url('Training/trainingController/CetakTraining')?>'>
		
		
	</script>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Request Kebutuhan Training</h3>
			</div>
			<div class="panel-body">
			<button class='buttonSubmit' type="button" id="btnCetak" onclick="btnCetak_click()">Cetak</button>
			<div id='printArea'>
						<div class="row">
					<div class="col-md-3">
						Nama:
					</div>
					<div class="col-md-6">
						<?php echo $nama; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						NPK:
					</div>
					<div class="col-md-6">
						<?php echo $npk; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Departemen:
					</div>
					<div class="col-md-6">
						<?php  echo $departemen; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Jabatan:
					</div>
					<div class="col-md-6">
						<?php  echo $jabatan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Diusulkan untuk mengikuti program training :
					</div>
					<div class="col-md-6">
						<?php echo $programtraining; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Penyelenggara :
					</div>
					<div class="col-md-6">
						<?php echo $penyelenggara; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Timbulnya kebutuhan Training disebabkan karena adanya :
					</div>
					<div class="col-md-6">
						<?php echo $alasantraining; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Tuntutan pekerjaan yang belum dapat dipenuhi :
					</div>
					<div class="col-md-6">
						<?php echo $tuntutanpekerjaan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Kemampuan karyawan yang diharapkan diperoleh melalui training :
					</div>
					<div class="col-md-6">
						<?php echo $kemampuanyangdiharapkan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Job description (uraian jabatan) dan job qualification (kualifikasi jabatan / tuntutan jabatan)
						dari pekerjaan yang didukung oleh training yang diusulkan :
					</div>
					<div class="col-md-6">
						<?php echo $jobdescription; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Biaya Training:
					</div>
					<div class="col-md-6">
						Rp. <?php echo number_format($biayatraining,2,",","."); ?>
					</div>
				</div>
				<br/>
				<div class="row">
				<div class="col-md-3">
					Catatan jika Decline :
				</div>
				<div class="col-md-6">
					<textarea id="txtCatatan" class="form-control" name="txtCatatan" type="text" cols="60" rows="3" ></textarea>
				</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						History Catatan :
					</div>
					<div class="col-md-9">
						<?php echo $historycatatan; ?>
					</div>
				</div>
				<br/>
				</div>
				<div class="row">
					<div align="right">
						<button id="btnApprove" type="button"  onClick="btnApprove_click()" class="btn btn-primary">Approve</button>
						<button id="btnDecline" type="button" onClick="btnDecline_click()" class="btn ">Decline</button>
						
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
