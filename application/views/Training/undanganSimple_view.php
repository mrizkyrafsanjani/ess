<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
 
<?php 
foreach($body->css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<?php foreach($body->js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<script>

	$(document).ready(function(){
		<?php if($this->session->flashdata('msg')){ ?>
		alert('<?php echo $this->session->flashdata('msg'); ?>');
		<?php } ?>
	});
</script>

</head>
<body onload="parent.alertsizeUndangan(document.body.clientHeight);">
<!-- Beginning header -->

<!-- End of header-->
    <div style='height:20px;'></div>  
    <div>
        <?php echo $body->output; ?>
    </div>
<!-- Beginning footer -->
<div></div>
<!-- End of Footer -->
</body>
</html>