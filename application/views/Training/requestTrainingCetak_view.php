<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jshashtable-2.1.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.numberformatter.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/js/jquery.printElement.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	
	
	$(function() {
			$('.buttonSubmit').click(function (){
				$('#printArea').printElement({
					//leaveOpen: true,
					pageTitle:'Form Kebutuhan Training',
					printMode:'popup',
					overrideElementCSS:[
						'<?php echo $this->config->base_url(); ?>assets/css/cetak.css','<?php echo $this->config->base_url(); ?>assets/css/style-common.css']
				});
			});
	});
	
	function eksekusiPrint(){
	$('#printArea').printElement({
					//leaveOpen: true,
					pageTitle:'Form Kebutuhan Training',
					printMode:'popup'
					/* overrideElementCSS:[
						'<?php echo $this->config->base_url(); ?>assets/css/cetak.css','<?php echo $this->config->base_url(); ?>assets/css/style-common.css'] */
				});
	}
	
	</script>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body onload="eksekusiPrint()">
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Request Kebutuhan Training</h3>
			</div>
			</br>
			<div class="panel-body">
			<input class='buttonSubmit' type="button" value="Cetak" id="btnCetak">
			<div id='printArea'>
				<center class="lblTop"><u> FORMULIR IDENTIFIKASI KEBUTUHAN TRAINING</center></u>
				<center class="lblTop"> TRAINING NEEDS FORM</center>
				<ol>
				</br>
				</br>
				<div class="row">
					<div class="col-md-3">
						<li>Nama: <?php echo $nama; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						NPK: <?php echo $npk; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Departemen: <?php  echo $departemen; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Jabatan: <?php  echo $jabatan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						<li>Diusulkan untuk mengikuti program training : <?php echo $programtraining; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Penyelenggara : <?php echo $penyelenggara; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						<li>Timbulnya kebutuhan Training disebabkan karena adanya :
					</div>
					<div class="col-md-6">
						<?php echo $alasantraining; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						<li>Tuntutan pekerjaan yang belum dapat dipenuhi :
					</div>
					<div class="col-md-6">
						<?php echo $tuntutanpekerjaan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						<li>Kemampuan karyawan yang diharapkan diperoleh melalui training :
					</div>
					<div class="col-md-6">
						<?php echo $kemampuanyangdiharapkan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Job description (uraian jabatan) dan job qualification (kualifikasi jabatan / tuntutan jabatan) 
						dari pekerjaan yang didukung oleh training yang diusulkan:
					</div>
					<div class="col-md-6">
						<?php echo $jobdescription; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						<li>Biaya Training: Rp. <?php echo number_format($biayatraining,2,",","."); ?>
					</div>
				</div>
				</ol>
				<br/>
				<!-- tanda tangan -->
				<table id="tblTandaTangan"  align="center" border=1>
				  <tbody>
					<tr class="row">
					  <td rowspan="2"></td>
					  <td width="120px" colspan="1" align="center">Dibuat</td>
					  <td width="120px" colspan="1" align="center">Disetujui</td>
					  <td width="120px"colspan="1" align="center">Disetujui</td>
					  <td width="120px" rowspan="2" align="center">Keterangan Status</td>
					</tr>
					<tr class="row">
					  <td align="center">Dept. Head</td>
					  <td align="center">DIC</td>
					  <td align="center">HRGA Dept. Head</td>
					</tr>
					<tr class="row">
					  <td align="center"><br/><br/>Tanda Tangan<br/><br/><br/></td>
					  <td></td>
					  <td></td>
					  <td></td>
					  <td></td>
					</tr>
					<tr class="row">
					  <td align="center">Nama</td>
					  <td align="center"><?php echo $atasan; ?></td>
					  <td align="center"><?php echo $dic; ?></td>
					  <td align="center"><?php echo $hrgahead; ?></td>
					  <td></td>
					</tr>
					<tr class="row">
					<td align="center">Tanggal</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					</tr>
				  </tbody>
				</table>
				</div>
			</div>
		</div>
	</body>
</html>
