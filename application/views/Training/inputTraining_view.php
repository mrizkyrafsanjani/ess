<html>
	<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<script type="text/javascript">
	function alertsizeTraining(pixels){
		pixels+=32;
		document.getElementById('iFrameCutiJenisTidakHadir').style.height=pixels+"px";
	}
	
	
	</script>
	<head>
		<title><?php $title ?></title>
		<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">
		<style>
		td {padding:5px 5px 5px 5px;}		
		</style>
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Input Training</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						NPK 
					</div>
					<div class="col-md-6">
						<?php echo $npk; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Nama 
					</div>
					<div class="col-md-6">
						<?php echo $nama; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Departemen 
					</div>
					<div class="col-md-6">
						<?php echo $departemen; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div class="col-md-3">
						Jabatan 
					</div>
					<div class="col-md-6">
						<?php echo $jabatan; ?>
					</div>
				</div>
				<br/>
				
				
				
				
				<div class="row">
					<div class="col-md-3">
						History Catatan :
					</div>
					<div class="col-md-9">
						<?php echo $historycatatan; ?>
					</div>
				</div>
				<br/>
				<div class="row">
					<div align="right">
						<button id="btnSubmit" type="button" class="btn btn-primary">Simpan</button>
						<button id="btnCancel" type="button" class="btn ">Batal</button>
					</div>
				</div>
			</div>
		</div>
		
		
		
		
	</body>
</html>
