<?php 
Class Cuti_model extends Ci_Model
{
	function getKodeCuti($npk)
	{	
		$sql = "select KodeCuti from cuti
		where NPK = ? and Deleted = 0
		ORDER BY KodeCuti DESC
		LIMIT 1";
		
		$query = $this->db->query($sql,array($npk));
		if($query->num_rows() > 0){
			$KodeCuti = "";
			foreach($query->result() as $row)
			{
				$KodeCuti = $row->KodeCuti;
			}
			return $KodeCuti;
		}else{
			return false;
		}
	}
	
	function getCuti($KodeUserTask)
	{
		/* 
		$sql = "select l.NPK,l.KodeCuti,Nama,jth.Deskripsi as JenisTidakHadir, Lokasi, TanggalMulai,TanggalSelesai,
      StatusApproval      
			from Cuti l 
			  join dtltrkrwy rw on l.KodeCuti = rw.NoTransaksi
			  join mstruser u on l.NPK = u.NPK
        join jenistidakhadir jth on l.KodeJenisTidakHadir = jth.KodeJenisTidakHadir
			where rw.KodeUserTask = ?";
		 */
		
		$sql = "select l.NPK,l.KodeCuti,Nama,
      StatusApproval, l.TanggalSelesai      
			from Cuti l 
			  join dtltrkrwy rw on l.KodeCuti = rw.NoTransaksi
			  join mstruser u on l.NPK = u.NPK        
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getCutiUser($NPK,$Tahun='')
	{
		try
		{
			$sql = '';
			if($Tahun == '')
			{	fire_print('log','masuk ke tahun yang kosong');
				$sql = "select * from mstrcutiuser
					where Deleted = 0 and NPK = ? 
					order by Tahun Desc
					limit 6";
				$query = $this->db->query($sql,array($NPK));
			}
			else
			{	fire_print('log',"masuk ke tahun yang tidak kosong. Tahun: $Tahun");
				$sql = "select * from mstrcutiuser
					where Deleted = 0 and NPK = ? and Tahun = ?
				";
				$query = $this->db->query($sql,array($NPK,$Tahun));
			}
			
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function getJenisTidakHadir($KodeCuti)
	{
		$sql = "select deskripsi as JenisTidakHadir 
			from jenistidakhadir
			where KodeJenisTidakHadir in (
				select KodeJenisTidakHadir from cutijenistidakhadir where KodeCuti = ? and Deleted = '0'
			)";
		$query = $this->db->query($sql,array($KodeCuti));
		$strJenisTidakHadir = "";
		foreach($query->result() as $row)
		{
			$strJenisTidakHadir .= $row->JenisTidakHadir .'<br/>';
		}
		return $strJenisTidakHadir;
	}
	
	function getCutiByNoTransaksi($KodeCuti)
	{
		try
		{
			$sql = "select l.NPK,l.KodeCuti, StatusApproval, u.Nama,d.NamaDepartemen
				from Cuti l
				  join mstruser u on l.NPK = u.NPK
				  join Departemen d on u.Departemen=d.KodeDepartemen
				where l.KodeCuti = ?";
			fire_print('log','KodeCuti proses getCutiByNoTransaksi='.$KodeCuti);
			$query = $this->db->query($sql,array($KodeCuti));
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}	
	
	function updateStatusCuti($KodeUserTask, $status, $updatedby)
	{
		try{
			$sql = "update Cuti l
				join dtltrkrwy rw on l.KodeCuti = rw.NoTransaksi
				set
					l.StatusApproval = ?,
					l.UpdatedOn = now(),
					l.UpdatedBy = ?
				where rw.KodeUserTask = ?";
			$query = $this->db->query($sql,array($status,$updatedby,$KodeUserTask));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function updateAkumulasiCuti($KodeUserTask,$updatedby)
	{
		try
		{	
			//dapatkan akumulasi terakhir tahun skrg
			$sql = "select * from Cuti l
				join dtltrkrwy rw on l.KodeCuti = rw.NoTransaksi				
				where rw.KodeUserTask = ?";
			$query = $this->db->query($sql,$KodeUserTask);
			
			foreach($query->result() as $rw)
			{
				$NPK = $rw->NPK;
				$KodeCuti = $rw->KodeCuti;
				
				$cutijenistidakhadirList = $this->db->query("select * from cutijenistidakhadir
					where Deleted = 0 AND KodeCuti = ?",$KodeCuti);
				foreach($cutijenistidakhadirList->result() as $cutijenistidakhadir)
				{
					$cutiTahunanYangDipakai = 0;
					$cutiBesarYangDipakai = 0;
					
					if($cutijenistidakhadir->KodeJenisTidakHadir == 1) //cuti tahunan
					{
						$cutiTahunanYangDipakai = $this->getHariKerja($cutijenistidakhadir->TanggalMulai,$cutijenistidakhadir->TanggalSelesai);
					}
					
					if($cutijenistidakhadir->KodeJenisTidakHadir == 2) //cuti besar
					{
						$cutiBesarYangDipakai = $this->getHariKerja($cutijenistidakhadir->TanggalMulai,$cutijenistidakhadir->TanggalSelesai);
					}
					
					$sql2 = "select * from mstrcutiuser
					where Deleted = 0 AND NPK = ? AND Tahun = ?";
					$query2 = $this->db->query($sql2,array($NPK,$cutijenistidakhadir->TahunCutiYangDipakai));
					if($query2){
						foreach($query2->result() as $rw2)
						{
							$akumulasiCutiTahunan = $rw2->AkumulasiCutiTahunan - $cutiTahunanYangDipakai;
							$akumulasiCutiBesar = $rw2->AkumulasiCutiBesar - $cutiBesarYangDipakai;

							$dataUpdate = array(
								"AkumulasiCutiTahunan"=>$akumulasiCutiTahunan,
								"AkumulasiCutiBesar"=>$akumulasiCutiBesar,
								"UpdatedBy"=>$updatedby,
								"UpdatedOn"=>date('Y-m-d H:i:s')
							);
							
							//insert rwymstrcutiuser
							$sqlwill = "insert into rwymstrcutiuser(KodeMasterCutiUser, Deleted, NPK, Tahun, JumlahCutiTahunanAwal, AkumulasiCutiTahunan, JumlahCutiBesarAwal, AkumulasiCutiBesar, CreatedOn, CreatedBy)
							select u.KodeMasterCutiUser, u.Deleted, u.NPK, u.Tahun, u.JumlahCutiTahunanAwal, u.AkumulasiCutiTahunan, u.JumlahCutiBesarAwal, u.AkumulasiCutiBesar, NOW(), ? from mstrcutiuser u
							where Deleted = 0 AND u.KodeMasterCutiUser = ? ";
							$this->db->query($sqlwill, array($updatedby,$rw2->KodeMasterCutiUser));

							$this->db->update("mstrcutiuser",$dataUpdate,array("KodeMasterCutiUser"=>$rw2->KodeMasterCutiUser));
						}
					}
				}
				
				fire_print('log',"cutiTahunanYangDipakai : $cutiTahunanYangDipakai, cutiBesarYangDipakai : $cutiBesarYangDipakai");
			}
			
			/* if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			} */
			
			return true;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function getHariKerjaDanCB($tanggalmulaiView='',$tanggalselesaiView='')
	{
		try
		{
			if($tanggalmulaiView == '' && $tanggalselesaiView == ''){
				$tanggalmulai = $_POST['tanggalmulai'];
				$tanggalselesai = $_POST['tanggalselesai'];		
			}else
			{
				$tanggalmulai = $tanggalmulaiView;
				$tanggalselesai = $tanggalselesaiView;
			}
			$hariKerja = 0;
			$hariLibur = 0;
			$selisihHari = $this->s_datediff('d',$tanggalmulai,$tanggalselesai);
			
			
			for($i = 0; $i <= $selisihHari;$i++)
			{
				$time = strtotime('+'.$i.' days',strtotime($tanggalmulai));
				$kodeHari =  date('w',$time);
				//fire_print('log','Kode Hari :'.$kodeHari);
				if($kodeHari==6||$kodeHari==0) // 0 = Minggu, 6 = Sabtu
				{
					$hariLibur++;
				}
				else
				{
					//check apakah ada di master libur
					$liburlist = $this->db->get_where('harilibur',array('Tanggal'=>date('Y-m-d',$time),'Deleted'=>'0','hari !=' => 'cb'));
					if($liburlist->num_rows() > 0)
						$hariLibur++;
				}
			}
			fire_print('log','Hari Libur :'.$hariLibur);
			$hariKerja = $selisihHari-$hariLibur+1;
			fire_print('log','Hari Kerja :'.$hariKerja." tanggalMulai: $tanggalmulai - $tanggalselesai");
			if($tanggalmulaiView == '' && $tanggalselesaiView =='')
				echo $hariKerja . ' hari kerja'; //untuk proses ajax di input
			
			return $hariKerja; //untuk column callback di view
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function getHariKerja($tanggalmulaiView='',$tanggalselesaiView='')
	{
		try
		{
			if($tanggalmulaiView == '' && $tanggalselesaiView == ''){
				$tanggalmulai = $_POST['tanggalmulai'];
				$tanggalselesai = $_POST['tanggalselesai'];		
			}else
			{
				$tanggalmulai = $tanggalmulaiView;
				$tanggalselesai = $tanggalselesaiView;
			}
			$hariKerja = 0;
			$hariLibur = 0;
			$selisihHari = $this->s_datediff('d',$tanggalmulai,$tanggalselesai);
			
			
			for($i = 0; $i <= $selisihHari;$i++)
			{
				$time = strtotime('+'.$i.' days',strtotime($tanggalmulai));
				$kodeHari =  date('w',$time);
				//fire_print('log','Kode Hari :'.$kodeHari);
				if($kodeHari==6||$kodeHari==0) // 0 = Minggu, 6 = Sabtu
				{
					$hariLibur++;
				}
				else
				{
					//check apakah ada di master libur
					$liburlist = $this->db->get_where('harilibur',array('Tanggal'=>date('Y-m-d',$time),'Deleted'=>'0'));
					if($liburlist->num_rows() > 0)
						$hariLibur++;
				}
			}
			fire_print('log','Hari Libur :'.$hariLibur);
			$hariKerja = $selisihHari-$hariLibur+1;
			fire_print('log','Hari Kerja :'.$hariKerja." tanggalMulai: $tanggalmulai - $tanggalselesai");
			if($tanggalmulaiView == '' && $tanggalselesaiView =='')
				echo $hariKerja . ' hari kerja'; //untuk proses ajax di input
			
			return $hariKerja; //untuk column callback di view
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function getJumlahCutiUser($NPKSelectedUser){
		$dataUser = $this->user->findUserByNPK($NPKSelectedUser);
		foreach($dataUser as $rw)
		{
			$BulanBekerjaKaryawan = date("n",strtotime($rw->TanggalBekerja));
			$TanggalBekerjaKaryawan = date("d",strtotime($rw->TanggalBekerja));
		}
		$jumlahCutiTahunan = 0;
		$jumlahCutiBesar = 0;
		$jumlahMedical = 0;
		$tahunTerakhirDlmDB = 0;
		$listCutiUser = $this->db->query("SELECT * FROM mstrcutiuser WHERE Deleted = 0 AND NPK = ? ORDER BY Tahun DESC LIMIT 6",$NPKSelectedUser);
		
		if($listCutiUser){
			$i = 0;
			$flagTahunSama = 0;
			foreach($listCutiUser->result() as $cutiUser)
			{
				if($i == 0)
					$tahunTerakhirDlmDB = $cutiUser->Tahun;
				if($i < 2)
				{
					$jumlahCutiTahunan += $cutiUser->AkumulasiCutiTahunan;
				}
				if($cutiUser->AkumulasiCutiBesar > 0)
				{
					fire_print('log',"BulanBekerjaKaryawan : $BulanBekerjaKaryawan, date : ". date("n"));
					
					if(date("Y") == $cutiUser->Tahun)
					{
						if(date("n") >= $BulanBekerjaKaryawan ) //klo bulan skrg lbh besar dari bulan tanggal bekerja maka baru akan tambah akumulasi cuti besar.
						{
							if(date("n") == $BulanBekerjaKaryawan){ //jika bulan sama, maka akumulasi akan bertambah untuk yg tanggalnya lebih besar dari tanggal bekerja.
								if(date("d") >= $TanggalBekerjaKaryawan){
									$jumlahCutiBesar += $cutiUser->AkumulasiCutiBesar;
									$flagTahunSama = 1;
								}
							}
							else
							{
								fire_print('log','Masuk ke part 1');
								$jumlahCutiBesar += $cutiUser->AkumulasiCutiBesar;
								$flagTahunSama = 1;
							}
						}
					}
					else
					{
						if($flagTahunSama == 0 && date("Y") >= $cutiUser->Tahun)
						{
							$jumlahCutiBesar += $cutiUser->AkumulasiCutiBesar;
						}
					}
				}
				
				$i++;
			}
		}
		
		if($jumlahCutiTahunan <= 0)
		{
			//cek hutang cuti
			$jumlahHutangCuti = 0;
			$listHutangCuti = $this->cutiJenisTidakHadir_model->getHutangCutiTahunan($NPKSelectedUser,$tahunTerakhirDlmDB+1);
			if($listHutangCuti)
			{
				foreach($listHutangCuti as $hutangCuti)
				{
					//$jumlahHutangCuti += ($this->s_datediff('d',$hutangCuti->TanggalMulai,$hutangCuti->TanggalSelesai)+1);
					$jumlahHutangCuti += $this->cuti_model->getHariKerjaDanCB($hutangCuti->TanggalMulai,$hutangCuti->TanggalSelesai);
					if($NPKSelectedUser == "064-14")
					fire_print('log',"jumlahHutangCuti: $jumlahHutangCuti ". print_r($hutangCuti,true));
				}
			}
			$sisacutitahunan = '0';
			if($jumlahHutangCuti > 0)
			{
				if($jumlahCutiTahunan < 0){
					$jumlahCutiTahunan += ($jumlahHutangCuti*-1);
				}else{
					$jumlahCutiTahunan = '-'.$jumlahHutangCuti;
				}
			}
		}
		
		$hasil = array('JumlahCutiTahunan'=>$jumlahCutiTahunan,'JumlahCutiBesar'=>$jumlahCutiBesar);
		if($NPKSelectedUser == "064-14"){
			fire_print('log',"jumlahHutangCuti: " . print_r($hasil,true));
		}
		return $hasil;
	}
	
	function getCutiUserForTrx($npk)
	{
		$sisacutibesar = '';
		$sisacutitahunan = '';
		$arraycutitahunan = array();
		$query = $this->user->dataUser($npk);
		foreach($query as $rw)
		{
			$departemen = $rw->departemen;
			$nama = $rw->nama;
			$BulanBekerjaKaryawan = date("n",strtotime($rw->TanggalBekerja));
			$TanggalBekerjaKaryawan = date("d",strtotime($rw->TanggalBekerja));
		}
		$tahunTerakhirDlmDB = 0;
		$listCutiUser = $this->db->query("SELECT * FROM mstrcutiuser WHERE Deleted = 0 AND NPK = ? ORDER BY Tahun DESC LIMIT 6",$npk);
		
		if($listCutiUser){
			$i = 0;
			$flagTahunSama = 0;
			$jumlahCutiTahunan = 0;
			foreach($listCutiUser->result() as $cutiUser)
			{
				if($i == 0)
					$tahunTerakhirDlmDB = $cutiUser->Tahun;
				if($i < 2)
				{
					$jumlahCutiTahunan += $cutiUser->AkumulasiCutiTahunan;
					if($cutiUser->AkumulasiCutiTahunan != 0){
						$sisacutitahunan .= $cutiUser->AkumulasiCutiTahunan . " (". $cutiUser->Tahun ."), ";
						$arraycutitahunan[$cutiUser->Tahun] = $cutiUser->AkumulasiCutiTahunan;
					}
				}
				if($cutiUser->AkumulasiCutiBesar > 0)
				{
					fire_print('log',"BulanBekerjaKaryawan : $BulanBekerjaKaryawan, date : ". date("n"));
					
					if(date("Y") == $cutiUser->Tahun)
					{
						if(date("n") >= $BulanBekerjaKaryawan ) //klo bulan skrg lbh besar dari bulan tanggal bekerja maka baru akan tambah akumulasi cuti besar.
						{
							if(date("n") == $BulanBekerjaKaryawan){ //jika bulan sama, maka akumulasi akan bertambah untuk yag tanggalnya lebih besar dari tanggal bekerja.
								if(date("d") >= $TanggalBekerjaKaryawan){
									$sisacutibesar .= ( $cutiUser->AkumulasiCutiBesar . " (". $cutiUser->Tahun ."), " );
									$flagTahunSama = 1;
								}
							}
							else
							{
								fire_print('log','Masuk ke part 1');
								$sisacutibesar .= ( $cutiUser->AkumulasiCutiBesar . " (". $cutiUser->Tahun ."), " );
								$flagTahunSama = 1;
							}
						}
					}
					else
					{
						if($flagTahunSama == 0  && date("Y") >= $cutiUser->Tahun)
						{
							$sisacutibesar .= ( $cutiUser->AkumulasiCutiBesar . " (". $cutiUser->Tahun ."), " );
						}
					}
				}
				
				
				$i++;
			}
		}
		
		
		if($jumlahCutiTahunan <= 0)
		{
			//cek hutang cuti
			$jumlahHutangCuti = 0;
			$listHutangCuti = $this->cutiJenisTidakHadir_model->getHutangCutiTahunan($npk,$tahunTerakhirDlmDB+1);
			if($listHutangCuti)
			{
				foreach($listHutangCuti as $hutangCuti)
				{
					$jumlahHutangCuti += $this->cuti_model->getHariKerja($hutangCuti->TanggalMulai,$hutangCuti->TanggalSelesai);
				}
			}
			$sisacutitahunan = '0';
			if($jumlahHutangCuti > 0)
			{
				if($jumlahCutiTahunan < 0){
					$sisacutitahunan = strval($jumlahCutiTahunan + ($jumlahHutangCuti*-1)) . "  ";
				}
				else{
					$sisacutitahunan = '-'.$jumlahHutangCuti . "  ";
				}
			}
		}

		if($sisacutitahunan != ''){
			$sisacutitahunan = substr($sisacutitahunan,0,strlen($sisacutitahunan) - 2);
		}

		if($sisacutibesar != ''){
			$sisacutibesar = substr($sisacutibesar,0,strlen($sisacutibesar) - 2);
		}else{
			$sisacutibesar = '0';
		}
		
		$hasil = array('sisacutitahunan'=>$sisacutitahunan,
			'arraycutitahunan'=>$arraycutitahunan,
			'sisacutibesar'=>$sisacutibesar,
			'departemen'=>$departemen,
			'nama'=>$nama);
		
		return $hasil;
	}
	
	function s_datediff( $str_interval, $dt_menor, $dt_maior, $relative=false){
		/* 
		 * A mathematical decimal difference between two informed dates 
		 *
		 * Author: Sergio Abreu
		 * Website: http://sites.sitesbr.net
		 *
		 * Features: 
		 * Automatic conversion on dates informed as string.
		 * Possibility of absolute values (always +) or relative (-/+)
		*/
	   if( is_string( $dt_menor)) $dt_menor = date_create( $dt_menor);
	   if( is_string( $dt_maior)) $dt_maior = date_create( $dt_maior);

	   $diff = date_diff( $dt_menor, $dt_maior, ! $relative);
	   
	   switch( $str_interval){
		   case "y": 
			   $total = $diff->y + $diff->m / 12 + $diff->d / 365.25; break;
		   case "m":
			   $total= $diff->y * 12 + $diff->m + $diff->d/30 + $diff->h / 24;
			   break;
		   case "d":
			   $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24 + $diff->i / 60;
			   break;
		   case "h": 
			   $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + $diff->i/60;
			   break;
		   case "i": 
			   $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + $diff->s/60;
			   break;
		   case "s": 
			   $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i)*60 + $diff->s;
			   break;
		  }
	   if( $diff->invert)
			   return -1 * $total;
	   else    return $total;
	}
	
	function getTempMasterJatahCuti($KodeUserTask)
	{
				
		$sql = "select tmp.TempNPK,tmp.KodeTempMasterCutiUser,Nama,
      StatusTransaksi, tmp.JumlahCutiTahunanAwal,tmp.AkumulasiCutiTahunan,tmp.JumlahCutiBesarAwal,tmp.AkumulasiCutiBesar,tmp.Tahun      
			from tempmstrcutiuser tmp 
			  join dtltrkrwy rw on tmp.KodeTempMasterCutiUser = rw.NoTransaksi
			  join mstruser u on tmp.TempNPK = u.NPK        
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function updateMasterCutiFromTemp($KodeTempMasterCutiUser, $status)
	{
		try
		{
			$this->db->trans_start();
			
			$session_data = $this->session->userdata('logged_in');
			$npkLogin = $session_data['npk'];
			//insert ke rwymstrcutiuser
			$sql = "insert into rwymstrcutiuser(KodeMasterCutiUser, Deleted, NPK, Tahun, JumlahCutiTahunanAwal, AkumulasiCutiTahunan, JumlahCutiBesarAwal, AkumulasiCutiBesar, CreatedOn, CreatedBy)
			select u.KodeMasterCutiUser, u.Deleted, u.NPK, u.Tahun, u.JumlahCutiTahunanAwal, u.AkumulasiCutiTahunan, u.JumlahCutiBesarAwal, u.AkumulasiCutiBesar, NOW(), ? from mstrcutiuser u
			join (
				select * from tempmstrcutiuser where Deleted = 0 and KodeTempMasterCutiUser = ? and StatusTransaksi = 'AP'
			  ) tu on u.NPK = tu.TempNPK and u.Tahun = tu.Tahun
			where u.Deleted = 0";
			$this->db->query($sql,array($npkLogin, $KodeTempMasterCutiUser));

			$sql = "update mstrcutiuser u
					  join (
						select * from tempmstrcutiuser where Deleted = 0 and KodeTempMasterCutiUser = ? and StatusTransaksi = 'AP'
					  ) tu on u.NPK = tu.TempNPK and u.Tahun = tu.Tahun
					set
					  u.JumlahCutiTahunanAwal	 = tu.JumlahCutiTahunanAwal	,
					  u.AkumulasiCutiTahunan = tu.AkumulasiCutiTahunan,
					  u.JumlahCutiBesarAwal = tu.JumlahCutiBesarAwal,
					  u.AkumulasiCutiBesar = tu.AkumulasiCutiBesar,
					  u.Tahun = tu.Tahun,
					  u.UpdatedOn = tu.UpdatedOn,
					  u.UpdatedBy = tu.UpdatedBy
					  ";
			$this->db->query($sql,array($KodeTempMasterCutiUser));
			
			
			$this->db->trans_complete();
			
			return true;
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	
	function InsertMasterCutiToTemp($npk, $tahunkary)
	{
		try
		{
			$this->db->trans_start();
			
			
			$sql = "INSERT INTO tempmstrcutiuser (`TempNPK`,`Tahun`,`JumlahCutiTahunanAwal`,`AkumulasiCutiTahunan`,
                               `JumlahCutiBesarAwal`,`AkumulasiCutiBesar`,`StatusTransaksi`,
                               `CreatedOn`, `CreatedBy` )
					SELECT `NPK`,`Tahun`,`JumlahCutiTahunanAwal`,`AkumulasiCutiTahunan`,`JumlahCutiBesarAwal`,
						  `AkumulasiCutiBesar`,'AP' as `AP` ,`CreatedOn`,`CreatedBy` from mstrcutiuser 
						  where NPK=? and Tahun=? order by CreatedOn desc limit 1";
			$this->db->query($sql,array($npk,$tahunkary));
			
			
			$this->db->trans_complete();
			
			return true;
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	
	function getCutiUserLast($NPK,$Tahun='')
	{
		try
		{
			$sql = '';
			if($Tahun == '')
			{	fire_print('log','masuk ke tahun yang kosong');
				$sql = "select * from mstrcutiuser
					where Deleted = 0 and NPK = ? 
					order by Tahun Desc 
					limit 1";
				$query = $this->db->query($sql,array($NPK));
			}
			else
			{	fire_print('log',"masuk ke tahun yang tidak kosong. Tahun: $Tahun");
				$sql = "select * from mstrcutiuser
					where Deleted = 0 and NPK = ? and Tahun = ? order by Tahun Desc limit 1
				";
				$query = $this->db->query($sql,array($NPK,$Tahun));
			}
			
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
function getCutiBreakDown($KodeCuti)
	{
		try
		{
			$sql = "select * from cutijenistidakhadir where KodeCuti = ?";
			fire_print('log','KodeCuti proses getCutiBreakDown='.$KodeCuti);
			$query = $this->db->query($sql,array($KodeCuti));
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}
	
	function getJenisTidakHadirDetail($KodeCutiJenisTidakHadir)
	{
		$sql = "select deskripsi as JenisTidakHadir 
			from jenistidakhadir
			where KodeJenisTidakHadir in (
				select KodeJenisTidakHadir from cutijenistidakhadir where KodeCutiJenisTidakHadir = ? and Deleted = '0'
			)";
		$query = $this->db->query($sql,array($KodeCutiJenisTidakHadir));
		$strJenisTidakHadir = "";
		foreach($query->result() as $row)
		{
			$strJenisTidakHadir .= $row->JenisTidakHadir .'<br/>';
		}
		return $strJenisTidakHadir;
	}
	
	
}
?>