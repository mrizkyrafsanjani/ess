<?php 
Class Bm_analisa_model extends Ci_Model
{
	function getListAnalisa($where)
	{
		$sql = "SELECT *, format(Under_Over/JumlahBudget,4)*100 as Persentase from (SELECT bmc.Kodecoa, bmc.NamaCoa, bma.KodeActivity, substring(bma.NamaActivity,1,locate('DPA',bma.NamaActivity)-2) NamaActivity, 
		format(sum(bmb.JumlahBudget),2) as JumlahBudget, format(sum(bmr.Nominal),2) as realisasi, 
		format(sum(bmb.JumlahBudget) - sum(bmr.Nominal),2) as Under_Over,'' as Analisa, max(bmb.periode) periode, tablebaru.SisaBudgetYTD SisaBudgetYTD, tablebaru.BudgetYTD BudgetYTD
		from dpageneraldevelopment.bm_coa as bmc
		inner join dpageneraldevelopment.bm_activity as bma
		on bmc.KodeCoa = bma.KodeCoa
		inner join dpageneraldevelopment.bm_budget bmb
		on bma.KodeActivity = bmb.KodeActivity
		inner join dpageneraldevelopment.bm_realisasi as bmr
		on bma.KodeActivity = bmr.KodeActivity
		inner join dpageneraldevelopment.departemen
		on bma.KodeDepartemen = departemen.KodeDepartemen
		$where
		group by KodeCoa, bmb.KodeActivity
		order by KodeCoa, NamaCoa, NamaActivity) a";
		// echo $sql; die();
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			// echo $query->num_rows();
			return $query->result();
		}else{
			return $query->result();
		}
	}

	function getListAnalisa_2($DPA,$Departemen,$Jenis,$tahun,$bulan){
		$sql = "SELECT bmc.KodeCoa, bmc.NamaCoa, bma.KodeActivity,substring(bma.NamaActivity,1,locate('DPA',bma.NamaActivity)-2) NamaActivity,departemen.NamaDepartemen, 
			case when t2.periode is null then ? else t2.periode end as MaxPeriode,format(t2.totalBudget,0) as TotalBudget, format(t2.TotalRealisasi,0) TotalRealisasi, format(t2.totalbudget - t2.totalrealisasi,0) as Under_Over, format(((t2.totalbudget - t2.totalrealisasi)/t2.totalbudget)*100,2) Persentase, bm_analisa.Analisa, bm_analisa.CreatedOn AS analisaCreatedOn, SisaBudgetYTD, BudgetYTD
		from bm_coa as bmc 
			inner join bm_activity as bma on bmc.KodeCoa = bma.KodeCoa
			inner join departemen on bma.KodeDepartemen = departemen.KodeDepartemen
			left join (
				SELECT bmb.KodeActivity,max(bmb.Periode)periode ,SUM(bmb.JumlahBudget)TotalBudget,MONTH(max(bmb.Periode)) Bulan,SUM(CASE
					WHEN bmr.Nominal IS NULL THEN 0
					ELSE bmr.Nominal
					END) AS TotalRealisasi,
					SUM(bmb.JumlahBudget) - SUM(bmr.Nominal) AS under_over						
				FROM (
					SELECT *
					FROM bm_budget AS bmb
					WHERE Deleted = 0 AND YEAR(bmb.periode) = ? AND MONTH(bmb.periode) <= ?
					) AS bmb
					LEFT JOIN (
						SELECT * 
						FROM bm_realisasi AS bmr
						WHERE Deleted = 0 AND YEAR(bmr.Tanggal) = ? AND MONTH(bmr.Tanggal) <= ?
					) AS bmr ON bmb.KodeActivity = bmr.KodeActivity AND MONTH(bmb.Periode) = MONTH(bmr.Tanggal)
				GROUP BY KodeActivity
				) as t2 on bma.KodeActivity = t2.KodeActivity
			LEFT JOIN
				(SELECT *FROM bm_analisa WHERE
					deleted = 0 and periode <= ?
				) AS bm_analisa ON bma.KodeActivity = bm_analisa.KodeActivity
			LEFT JOIN (SELECT ba.KodeActivity, SUBSTRING(ba.NamaActivity,1,LOCATE('DPA',ba.NamaActivity)-2) as NamaActivity,ba.JenisPengeluaran,format(SUM(BudgetYTD),0) as BudgetYTD,format(SUM(RealisasiYTD),0) as RealisasiYTD,format((SUM(BudgetYTD) - SUM(RealisasiYTD)),0) as SisaBudgetYTD,format(SUM(BudgetFullYear),0) as BudgetFullYear, format((SUM(BudgetFullYear)-SUM(RealisasiYTD)),0) as SisaFullYear, MAX(TanggalLaporan) as TanggalLaporan
					FROM bm_temptotalbudget sb
						left join bm_coa c on sb.KodeCoa = c.KodeCoa
						left join bm_activity ba on sb.KodeActivity = ba.KodeActivity
					WHERE (BudgetFullYear != 0 OR RealisasiYTD != 0)
					GROUP BY sb.KodeActivity, TanggalLaporan)  
				as tablebaru on bma.KodeActivity like concat(tablebaru.KodeActivity,'%') 
			WHERE DPA in $DPA AND departemen.KodeDepartemen IN $Departemen AND bma.JenisPengeluaran in $Jenis AND bma.deleted = 0 and bmc.deleted = 0
			";

		$sql2 = "SELECT bmc.KodeCoa, bmc.NamaCoa, bma.KodeActivity,substring(bma.NamaActivity,1,locate('DPA',bma.NamaActivity)-2) NamaActivity,
				departemen.NamaDepartemen, case when t2.periode is null then last_day(?) else t2.periode end as MaxPeriode,
				format(t2.totalBudget,0) as TotalBudget, format(t2.TotalRealisasi,0) TotalRealisasi, 
				format(t2.totalbudget - t2.totalrealisasi,0) as Under_Over, 
				format(((t2.totalbudget - t2.totalrealisasi)/t2.totalbudget)*100,2) Persentase, 
				bm_analisa.Analisa, bm_analisa.CreatedOn AS analisaCreatedOn, 
				format(sum(tablebaru.pengajuan) - sum(tablebaru.realisasi),0) as belumrealisasi, 
				format(sum(tablebaru.pengajuan),0) pengajuan 
			from bm_coa as bmc 
			inner join bm_activity as bma on bmc.KodeCoa = bma.KodeCoa 
			inner join departemen on bma.KodeDepartemen = departemen.KodeDepartemen 
			left join ( 
				SELECT bmb.KodeActivity,max(bmb.Periode)periode ,SUM(bmb.JumlahBudget)TotalBudget,MONTH(max(bmb.Periode)) Bulan,
				SUM(CASE WHEN bmr.Nominal IS NULL THEN 0 ELSE bmr.Nominal END) AS TotalRealisasi, 
				SUM(bmb.JumlahBudget) - SUM(bmr.Nominal) AS under_over 
				FROM ( 
					SELECT * FROM bm_budget AS bmb 
					WHERE Deleted = 0 AND YEAR(bmb.periode) = ? AND MONTH(bmb.periode) <= ? ) AS bmb 
				LEFT JOIN ( 
					SELECT * FROM bm_realisasi AS bmr 
					WHERE Deleted = 0 AND YEAR(bmr.Tanggal) = ? AND MONTH(bmr.Tanggal) <= ? ) AS bmr 
				ON bmb.KodeActivity = bmr.KodeActivity AND MONTH(bmb.Periode) = MONTH(bmr.Tanggal) 
				GROUP BY KodeActivity
				-- UNION
				-- SELECT bmr.KodeActivity,max(bmr.Tanggal)periode ,SUM(bmb.JumlahBudget)TotalBudget,MONTH(max(bmb.Periode)) Bulan,
				-- SUM(CASE WHEN bmr.Nominal IS NULL THEN 0 ELSE bmr.Nominal END) AS TotalRealisasi, 
				-- SUM(bmb.JumlahBudget) - SUM(bmr.Nominal) AS under_over 
				-- FROM ( 
				-- 	SELECT * FROM bm_budget AS bmb 
				-- 	WHERE Deleted = 0 AND YEAR(bmb.periode) = ? AND MONTH(bmb.periode) <= ? ) AS bmb 
				-- RIGHT JOIN ( 
				-- 	SELECT * FROM bm_realisasi AS bmr 
				-- 	WHERE Deleted = 0 AND YEAR(bmr.Tanggal) = ? AND MONTH(bmr.Tanggal) <= ? ) AS bmr 
				-- ON bmb.KodeActivity = bmr.KodeActivity AND MONTH(bmb.Periode) = MONTH(bmr.Tanggal) 
				-- GROUP BY KodeActivity 
				) as t2 
			on bma.KodeActivity = t2.KodeActivity 
			LEFT JOIN (
				SELECT *FROM bm_analisa 
				WHERE deleted = 0 and periode <= last_day(?) ) AS bm_analisa 
				ON bma.KodeActivity = bm_analisa.KodeActivity 
			LEFT JOIN (
				select ppb.NoPP,ppb.Tipe, ppb.KodeActivity, ppb.JenisPengeluaran, ppb.Keterangan, ppb.Status, ppb.isReleased, 
				ppb.isFinished, dtlppb.DPA, dtlppb.Pengeluaran, dtlppb.TahunBudget, dtlppb.Budget,dtlppb.Realisasi, dtlppb.pengajuan 
				from persetujuanpengeluaranbudget as ppb 
				left join dtlpersetujuanpengeluaranbudget as dtlppb on ppb.NoPP = dtlppb.NoPP 
				where ppb.deleted = 0 and dtlppb.Deleted = 0) as tablebaru 
			on bma.KodeActivity = tablebaru.KodeActivity and year(t2.periode) = tablebaru.TahunBudget 
			WHERE bma.DPA in $DPA AND departemen.KodeDepartemen IN $Departemen 
			AND bma.JenisPengeluaran in $Jenis AND bma.deleted = 0 and bmc.deleted = 0
			group by KodeActivity
			";

		$sql2 = "SELECT bmc.KodeCoa, bmc.NamaCoa, bma.KodeActivity,substring(bma.NamaActivity,1,locate('DPA',bma.NamaActivity)-2) NamaActivity,
				departemen.NamaDepartemen, case when t2.periode is null then last_day(?) else t2.periode end as MaxPeriode,
				format(t2.totalBudget,0) as TotalBudget, format(t2.TotalRealisasi,0) TotalRealisasi, 
				format(t2.totalbudget - t2.totalrealisasi,0) as Under_Over, 
				format(((t2.totalbudget - t2.totalrealisasi)/t2.totalbudget)*100,2) Persentase, 
				bm_analisa.Analisa, bm_analisa.CreatedOn AS analisaCreatedOn, 
				format(sum(tablebaru.pengajuan) - sum(tablebaru.realisasi),0) as belumrealisasi, 
				format(sum(tablebaru.pengajuan),0) pengajuan 
			from bm_coa as bmc 
			inner join bm_activity as bma on bmc.KodeCoa = bma.KodeCoa 
			inner join departemen on bma.KodeDepartemen = departemen.KodeDepartemen 
			left join ( 
				SELECT bmb.KodeActivity,max(bmb.Periode)periode ,SUM(bmb.JumlahBudget)TotalBudget,MONTH(max(bmb.Periode)) Bulan,
				SUM(CASE WHEN bmr.Nominal IS NULL THEN 0 ELSE bmr.Nominal END) AS TotalRealisasi, 
				SUM(bmb.JumlahBudget) - SUM(bmr.Nominal) AS under_over 
				FROM ( 
					SELECT * FROM bm_budget AS bmb 
					WHERE Deleted = 0 AND YEAR(bmb.periode) = ? AND MONTH(bmb.periode) <= ? ) AS bmb 
				LEFT JOIN ( 
					SELECT * FROM bm_realisasi AS bmr 
					WHERE Deleted = 0 AND YEAR(bmr.Tanggal) = ? AND MONTH(bmr.Tanggal) <= ? ) AS bmr 
				ON bmb.KodeActivity = bmr.KodeActivity AND MONTH(bmb.Periode) = MONTH(bmr.Tanggal) 
				GROUP BY KodeActivity
				UNION
				SELECT bmr.KodeActivity,max(bmr.Tanggal)periode ,SUM(bmb.JumlahBudget)TotalBudget,MONTH(max(bmb.Periode)) Bulan,
				SUM(CASE WHEN bmr.Nominal IS NULL THEN 0 ELSE bmr.Nominal END) AS TotalRealisasi, 
				SUM(bmb.JumlahBudget) - SUM(bmr.Nominal) AS under_over 
				FROM ( 
					SELECT * FROM bm_budget AS bmb 
					WHERE Deleted = 0 AND YEAR(bmb.periode) = ? AND MONTH(bmb.periode) <= ? ) AS bmb 
				RIGHT JOIN ( 
					SELECT * FROM bm_realisasi AS bmr 
					WHERE Deleted = 0 AND YEAR(bmr.Tanggal) = ? AND MONTH(bmr.Tanggal) <= ? ) AS bmr 
				ON bmb.KodeActivity = bmr.KodeActivity AND MONTH(bmb.Periode) = MONTH(bmr.Tanggal) 
				GROUP BY KodeActivity 
				) as t2 
			on bma.KodeActivity = t2.KodeActivity 
			LEFT JOIN (
				SELECT *FROM bm_analisa 
				WHERE deleted = 0 and periode <= last_day(?) ) AS bm_analisa 
				ON bma.KodeActivity = bm_analisa.KodeActivity 
			LEFT JOIN (
				select ppb.NoPP,ppb.Tipe, ppb.KodeActivity, ppb.JenisPengeluaran, ppb.Keterangan, ppb.Status, ppb.isReleased, 
				ppb.isFinished, dtlppb.DPA, dtlppb.Pengeluaran, dtlppb.TahunBudget, dtlppb.Budget,dtlppb.Realisasi, dtlppb.pengajuan 
				from persetujuanpengeluaranbudget as ppb 
				left join dtlpersetujuanpengeluaranbudget as dtlppb on ppb.NoPP = dtlppb.NoPP 
				where ppb.deleted = 0 and dtlppb.Deleted = 0) as tablebaru 
			on bma.KodeActivity = tablebaru.KodeActivity and year(t2.periode) = tablebaru.TahunBudget 
			WHERE bma.DPA in $DPA AND departemen.KodeDepartemen IN $Departemen 
			AND bma.JenisPengeluaran in $Jenis AND bma.deleted = 0 and bmc.deleted = 0
			group by KodeActivity
			";

		$sql3 = "SELECT bmc.KodeCoa, bmc.NamaCoa, bma.KodeActivity,substring(bma.NamaActivity,1,locate('DPA',bma.NamaActivity)-2) NamaActivity,
				departemen.NamaDepartemen, case when bmb.periode is null then last_day(?) else bmb.periode end as MaxPeriode,
				format(bmb.JumlahBudget,0) as TotalBudget, format(bmr.Nominal,0) TotalRealisasi, 
				format(bmb.JUmlahBudget - bmr.Nominal,0) as Under_Over, 
				format(((sum(bmb.Jumlahbudget) - sum(bmr.Nominal))/sum(bmb.JumlahBudget))*100,2) Persentase, 
				bm_analisa.Analisa, bm_analisa.CreatedOn AS analisaCreatedOn, 
				format(sum(tablebaru.pengajuan) - sum(tablebaru.realisasi),0) as belumrealisasi, 
				format(sum(tablebaru.pengajuan),0) pengajuan 
			from bm_coa as bmc 
			inner join bm_activity as bma on bmc.KodeCoa = bma.KodeCoa 
			inner join departemen on bma.KodeDepartemen = departemen.KodeDepartemen 
			left join ( 
				SELECT KodeActivity, sum(JumlahBudget) JumlahBudget, max(Periode) periode FROM bm_budget AS bmb 
				WHERE Deleted = 0 AND YEAR(bmb.periode) = ? AND MONTH(bmb.periode) <= ?  group by KodeActivity) AS bmb
			on bma.KodeActivity = bmb.KodeActivity
			LEFT JOIN ( 
				SELECT KodeActivity, sum(Nominal) Nominal, max(Tanggal) tanggal FROM bm_realisasi AS bmr 
				WHERE Deleted = 0 AND YEAR(bmr.Tanggal) = ? AND MONTH(bmr.Tanggal) <= ?  group by KodeActivity) AS bmr 
			ON bma.KodeActivity = bmr.KodeActivity  
			LEFT JOIN (
				SELECT *FROM bm_analisa 
				WHERE deleted = 0 and periode <= last_day(?) ) AS bm_analisa 
				ON bma.KodeActivity = bm_analisa.KodeActivity 
			LEFT JOIN (
				select ppb.NoPP,ppb.Tipe, ppb.KodeActivity, ppb.JenisPengeluaran, ppb.Keterangan, ppb.Status, ppb.isReleased, 
				ppb.isFinished, dtlppb.DPA, dtlppb.Pengeluaran, dtlppb.TahunBudget, dtlppb.Budget,dtlppb.Realisasi, dtlppb.pengajuan 
				from persetujuanpengeluaranbudget as ppb 
				left join dtlpersetujuanpengeluaranbudget as dtlppb on ppb.NoPP = dtlppb.NoPP 
				where ppb.deleted = 0 and dtlppb.Deleted = 0) as tablebaru 
				on bma.KodeActivity = tablebaru.KodeActivity and year(bmb.periode) = tablebaru.TahunBudget 
				WHERE bma.DPA in $DPA AND departemen.KodeDepartemen IN $Departemen 
				AND bma.JenisPengeluaran in $Jenis AND bma.deleted = 0 and bmc.deleted = 0
			group by KodeActivity
			";

		$query = $this->db->query($sql3,array($tahun.'-'.$bulan.'-1',$tahun,$bulan,$tahun,$bulan,$tahun.'-'.$bulan.'-1'));
		// echo $this->db->last_query();
		// die();
		if($query->num_rows() > 0){
			// echo $query->num_rows();
			return $query->result();
		}else{
			return $query->result();
		}
	}

	function getDepartemen(){
		$sql = "SELECT distinct KodeDepartemen, NamaDepartemen from dpageneraldevelopment.departemen";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			return $query->result();
		}
	}

	function getDetil($datas){
		$KodeActivity = $datas['KodeActivity'];
		$periode = $datas['periode'];
		$sql = "SELECT bmc.Kodecoa, bmc.NamaCoa, departemen.NamaDepartemen, bma.KodeActivity, bma.NamaActivity, t2.* 
		from bm_coa as bmc
		inner join bm_activity as bma
		on bmc.KodeCoa = bma.KodeCoa
		inner join
		(
			select bmb.KodeActivity, bmb.Periode, format(sum(bmb.JumlahBudget),0) JumlahBudget, month(bmb.Periode) Bulan, format(sum(case when bmr.Nominal is null then 0 else bmr.Nominal end),0) Nominal, bmb.JumlahBudget-bmr.Nominal as under_over
				from (
				select * from bm_budget as bmb where Deleted = 0 and KodeActivity = '$KodeActivity' and year(bmb.periode) = year(str_to_date('$periode','%Y-%m-%d')) and month(bmb.periode) <= month(str_to_date('$periode','%Y-%m-%d'))
				) as bmb
				left join
				(select * from bm_realisasi as bmr where Deleted = 0 and KodeActivity = '$KodeActivity' and year(bmr.Tanggal) = year(str_to_date('$periode','%Y-%m-%d')) and month(bmr.Tanggal) <= month(str_to_date('$periode','%Y-%m-%d'))
				) as bmr
				on bmb.KodeActivity = bmr.KodeActivity and month(bmb.Periode) = month(bmr.Tanggal)
				group by month(Periode)
			) as t2
		on bma.KodeActivity = t2.KodeActivity
		inner join
		departemen
		on bma.KodeDepartemen = departemen.KodeDepartemen";

		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return $query->result();
		}
	}

	function getHeaderInfo($KodeActivity,$periode){
		$sql = "select bmc.KodeCoa, bma.KodeActivity, bma.NamaActivity, bm_an.analisa  
		from bm_coa as bmc  
			inner join bm_activity as bma on bmc.KodeCoa = bma.KodeCoa 
			left join (
				Select * from bm_analisa where deleted = 0 AND periode = ?
			) as bm_an on bma.KodeActivity = bm_an.KodeActivity 
		where bma.KodeActivity = ?";
		$query = $this->db->query($sql,array($periode,$KodeActivity));
		// echo ($this->db->last_query()); 
		if($query->num_rows() > 0){
			return $query->result();
		}
		else {
			return $query->result();
		}
	}

	function getHistory($KodeActivity,$periode){
		$sql = "SELECT * FROM dpageneraldevelopment.bm_analisa where KodeActivity = ? 
		order by CreatedOn desc";
		$query = $this->db->query($sql,array($KodeActivity,$periode));

		return $query->result();
	}

	function tambahAnalisa($KodeActivity,$periode,$analisa,$npk){
		$sql = "INSERT INTO bm_analisa (Deleted, Kodeactivity, Periode, Analisa, CreatedOn, CreatedBy) values
			(0, ?, ?, ?,now(), ?)";
		$query = $this->db->Query($sql,array($KodeActivity,$periode,$analisa,$npk));
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function UpdateAnalisa($KodeActivity,$periode,$npk){
		$sql = "UPDATE bm_analisa  a
		set Deleted = 1, UpdatedOn = now(), UpdatedBy = ?
		where Deleted = 0 and KodeActivity = ? and periode = ?";
		
		$this->db->Query($sql,array($npk,$KodeActivity,$periode));
	}
	function getActivity($kodeActivity)
	{
		$sql = "select * from bm_activity 
		where Deleted = 0 and KodeActivity = ?";
		$query = $this->db->query($sql,array($kodeActivity));
		//$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getYear()
	{
		$sql = "SELECT distinct left(periode,4) as Tahun FROM bm_budget where deleted = 0 and
		left(periode,4)>=year(now())";
		$query = $this->db->query($sql);
		//$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getActivityAll($kodeActivity)
	{
		$sql = "select * from bm_activity 
		where Deleted = 0 and left(KodeActivity,5) = ?";
		$query = $this->db->query($sql,array($kodeActivity));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getDataActivity($npkLogin, $admin='0',$DPA='1')
	{
		$arrWhere = array();
		$strSelect = " * ";
		$strGroupBy = "";
		if($DPA == 'all')
		{
			$strDPA = " DPA in (1,2) ";
			$strSelect = " LEFT(KodeActivity,5) as KodeActivity, SUBSTRING(NamaActivity,1,LOCATE('DPA',NamaActivity)-2) as NamaActivity ";
			$strGroupBy = " GROUP BY LEFT(KodeActivity,5), SUBSTRING(NamaActivity,1,LOCATE('DPA',NamaActivity)-2) ";
		}else{
			$strDPA = " DPA = ? ";
			$arrWhere[] = $DPA;
		}
		$arrWhere[] = $npkLogin;
		if($admin == '1')
		{
			$sql = "select $strSelect from bm_activity
			where Deleted = 0 and $strDPA
			$strGroupBy
			order by NamaActivity";
		}
		else
		{
			$sql = "select $strSelect from bm_activity
			where Deleted = 0 and $strDPA and kodeDepartemen in (select Departemen from mstruser where NPK = ?)
			$strGroupBy
			order by NamaActivity";
		}
		$query = $this->db->query($sql,$arrWhere);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>