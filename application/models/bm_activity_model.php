<?php 
Class Bm_activity_model extends Ci_Model
{
	function getActivity($kodeActivity)
	{
		$sql = "select * from bm_activity 
		where Deleted = 0 and KodeActivity = ?";
		$query = $this->db->query($sql,array($kodeActivity));
		//$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getYear()
	{
		$sql = "SELECT distinct left(periode,4) as Tahun FROM bm_budget where deleted = 0 and
		left(periode,4)>=year(now())";
		$query = $this->db->query($sql);
		//$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getActivityAll($kodeActivity)
	{
		$sql = "select * from bm_activity 
		where Deleted = 0 and left(KodeActivity,5) = ?";
		$query = $this->db->query($sql,array($kodeActivity));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getDataActivity($npkLogin, $admin='0',$DPA='1')
	{
		$arrWhere = array();
		$strSelect = " * ";
		$strGroupBy = "";
		if($DPA == 'all')
		{
			$strDPA = " DPA in (1,2) ";
			$strSelect = " LEFT(KodeActivity,5) as KodeActivity, SUBSTRING(NamaActivity,1,LOCATE('DPA',NamaActivity)-2) as NamaActivity ";
			$strGroupBy = " GROUP BY LEFT(KodeActivity,5), SUBSTRING(NamaActivity,1,LOCATE('DPA',NamaActivity)-2) ";
		}else{
			$strDPA = " DPA = ? ";
			$arrWhere[] = $DPA;
		}
		$arrWhere[] = $npkLogin;
		if($admin == '1')
		{
			$sql = "select $strSelect from bm_activity
			where Deleted = 0 and $strDPA
			$strGroupBy
			order by NamaActivity";
		}
		else
		{
			$sql = "select $strSelect from bm_activity
			where Deleted = 0 and $strDPA and kodeDepartemen in (select Departemen from mstruser where NPK = ?)
			$strGroupBy
			order by NamaActivity";
		}
		$query = $this->db->query($sql,$arrWhere);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>