<?php 
Class LokasiAsset extends Ci_Model
{
	function getLokasiAsset($kodeLokasiAsset)
	{	
		
		$sql = "select l.KodeLokasiAsset, l.Deskripsi, l.KodeDepartemen, d.NamaDepartemen from lokasiasset l 
				  left join (select * from departemen where deleted = 0) d on l.KodeDepartemen = d.KodeDepartemen
				where l.deleted = 0 and l.KodeLokasiAsset = ? ";
		$query = $this->db->query($sql, $kodeLokasiAsset);
		
		//$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>