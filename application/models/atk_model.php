<?php 
Class Atk_model extends Ci_Model
{
	
	function getDepartemen($npk)
	{
		$query = $this->user->dataUser($npk);
		foreach($query as $rw)
		{
			$departemen = $rw->departemen;
			
		}
	
	$hasil = array('departemen'=>$departemen,);
		
		return $hasil;
		
	}
	
	function getHeaderAtk($KodeUserTask)
	{
		$sql = "select * from headerrequestatk
				ORDER BY CreatedOn DESC LIMIT 1";
		
		$query = $this->db->query($sql,array($KodeUserTask));
	
		if($query->num_rows() > 0){
			$KodeRequestAtk = "";
			
			foreach($query->result() as $row)
			{
				$KodeRequestAtk = $row->KodeRequestAtk;
			}
			return $query->result();
			
		}else{
			return false;
		}
		
	}
	
	function getAtk($KodeUserTask)
	{
		$sql = " select KodeRequestAtk, Status, headerrequestatk.CreatedBy from headerrequestatk    
			  join dtltrkrwy rw on KodeRequestAtk = rw.NoTransaksi
			  join mstruser u on NPK = u.NPK			  
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		//fire_print('log','masuk disini'.$KodeUserTask);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function updateStatusATK($KodeUserTask, $status, $updatedby)
	{
		fire_print('log','masuk disini update status');
		try{
			$sql = "update headerrequestatk l
				join dtltrkrwy rw on l.KodeRequestAtk = rw.NoTransaksi
				set
					l.Status = ?,
					l.UpdatedOn = now(),
					l.UpdatedBy = ?
				where rw.KodeUserTask = ?";
				
			$query = $this->db->query($sql,array($status,$updatedby,$KodeUserTask));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	
	}
	
	function getDtAtk($KodeUserTask)
	{
		$sql = " select KodeRequestAtk, KodeBarang, JumlahBarang
			from detailrequestatk
			where KodeRequestAtk = (select noTransaksi from dtltrkrwy where KodeUserTask = ? limit 1)
			group by KodeRequestAtk";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		//fire_print('log','masuk disini'.$KodeUserTask);
		if($query->num_rows() > 0){
		$dataAtk = array();
			foreach($query->result() as $row)
			{
				
				$dataAtk = array(
					'KodeRequestAtk' => $row->KodeRequestAtk
				);
			}
			return $dataAtk;
		}else{
			return false;
		}
	}

	function getPoAtk($npk)
	{
		$sql = "select * from detailrequestatk where KodeBarang = ?";
		
		fire_print('log',"NPK get : $npk");
		
		$query = $this->db->query($sql,array($npk));
		if($query->num_rows() > 0){			
			$dataPoAtk = array();
			foreach($query->result() as $row)
			{
				fire_print('log',"KodeBarang detail ".$row->KodeBarang);
				$dataPoAtk = array(
					'KodeBarang' => $row->KodeBarang,
					'JumlahBarang' => $row->JumlahBarang,
					'NPK' => $row->NPK
				);
			}
			return $dataPoAtk;
		}else{
			return false;
		}
	}
	
	function getDetailRequest()
	{
		$sql = "select dra.KodeBarang, ma.Stock,SUM(JumlahBarang) as Permintaan, ma.Stock - SUM(JumlahBarang) as Selisih, hra.Periode
			from detailrequestatk dra join mstratk ma on dra.KodeBarang = ma.KodeBarang
			  join headerrequestatk hra on dra.KodeRequestAtk = hra.KodeRequestAtk
			where hra.Periode = '' or hra.Periode is null
			group by dra.KodeBarang, ma.Stock
			";
		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){						
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getPeriode($periode)
	{
		$sql = "select * from headerrequestatk where Periode =?
		and (Status = 'APP' or Status = 'APR')";
		
		$query = $this->db->query($sql,array($periode));
		if($query->num_rows() > 0){						
			return $query->result();
		}else{
			return false;
		}
	}
	
	function updateStock($NPK,$jumlah, $KodeBarang)
	{
		try{
			fire_print('log',"updateStock. NPK,jumlah,KodeBarang : $NPK, $jumlah,$KodeBarang");
			
			$sql = "update  mstratk ma set UpdatedBy=?,UpdatedOn=now(), Stock = ma.Stock + ?
			where ma.KodeBarang= ? and ma.deleted = '0'";
			
			$query = $this->db->query($sql,array($NPK,$jumlah,$KodeBarang));
			fire_print('log',"updateStock. Hasil Query : ". print_r($query,true) . ". affected row :". $this->db->affected_rows());
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

}
?>