<?php 
Class AkomodasiTiket_model extends Ci_Model
{
	
	
	function getGlobalParam($parameter)
	{
		$sql = "select * from globalparam where Name=?";
		$query = $this->db->query($sql,$parameter);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->Value;
			}
		}
		else
		{
			return "0";
		}
	}
	

	function getAtasan($NPK)
	{
		$sql = "select NPKAtasan from mstruser where NPK=?";
		$query = $this->db->query($sql, $NPK);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->NPKAtasan;
			}
		}
		else
		{
			return "0";
		}
	}

	function getDataPP()
	{
		
			$sql = "select concat(NoPP ,' - ' , Keterangan) as display,NoPP  
					from persetujuanpengeluaranbudget 
					where Deleted=0 order by NoPP";
		
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
	}

	function getJenisKegiatanUM()
	{
		
			$sql = "select *   
					from mstrjnskegiatanum 
					 order by IdKegiatan";
		
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
	}

	
	function getJenisKegiatanUMBaseID($IdKegiatan)
	{
		
			$sql = "select *   
					from mstrjnskegiatanum where IdKegiatan=?";
		
			$query = $this->db->query($sql,$IdKegiatan);
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
	}

	function getLastNoAT()
	{
		$sql = "SELECT NoAkomodasiTiket FROM akomodasitiket ORDER BY CreatedOn DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->NoAkomodasiTiket;
			}
		}
		else
		{
			return "0";
		}
	}

	function getInitDept($NPK)
	{
		$sql = "SELECT INIT 
			   FROM departemen d join mstruser m
			   on d.KodeDepartemen=m.Departemen
			   where m.NPK= ? ";
		$query = $this->db->query($sql,$NPK);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->INIT;
			}
		}
		else
		{
			return "0";
		}
	}
	
		
	function insertAkomodasiTiket($noAT,$KodeAkomodasiTiket)
	{	
		$session_data = $this->session->userdata('logged_in');
		$NPKLogin = $session_data['npk'];
		$txtTanggalBerangkat = $this->input->post('txtTanggalBerangkat');
		$txtTanggalKembali = $this->input->post('txtTanggalKembali');
		$txtTujuan = $this->input->post('txtTujuan');
		$DPA = $this->input->post('rbDPA');
		$txtAlasan = $this->input->post('txtAlasan');
		$txtTanggalCheckIn = $this->input->post('txtTanggalCheckIn');
		$txtTanggalCheckOut = $this->input->post('txtTanggalCheckOut');
		$NPKAtasan = $this->input->post('txtNPKatasan');
		$NamaHotel = $this->input->post('txtHotel');
		$TiketPesawat = $this->input->post('txtTiket');
		$txtBerangkatPesawat = $this->input->post('txtBerangkatPesawat');
		$txtPulangPesawat = $this->input->post('txtPulangPesawat');
		$txtJamBerangkatPesawat = $this->input->post('txtJamBerangkatPesawat');
		$txtJamPulangPesawat = $this->input->post('txtJamPulangPesawat');
		
		
		$sql = "INSERT INTO akomodasitiket (NoAkomodasiTiket, KodeAkomodasiTiket,Deleted,TanggalBerangkat,TanggalKembali,Tujuan,
										Alasan,NPKAtasan,NamaHotel,TanggalCheckIn,TanggalCheckOut,
										TiketPesawat,TanggalBerangkatPesawat,JamBerangkatPesawat,TanggalKepulanganPesawat,JamKepulanganPesawat,
										CreatedOn,CreatedBy,StatusApprove,DPA) 
			VALUES (?,?,0,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),?,'PE',?)";

		$this->db->query($sql,array($noAT,$KodeAkomodasiTiket,$txtTanggalBerangkat,$txtTanggalKembali,$txtTujuan,$txtAlasan,
									$NPKAtasan,$NamaHotel,$txtTanggalCheckIn,$txtTanggalCheckOut,$TiketPesawat,
									$txtBerangkatPesawat,$txtJamBerangkatPesawat,$txtPulangPesawat,$txtJamPulangPesawat,$NPKLogin,$DPA));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function getTrxATbyKode($KodeAkomodasiTiket){
		$sql = "select NoAkomodasiTiket, KodeAkomodasiTiket,  
					   DATE_FORMAT(TanggalBerangkat,'%Y-%m-%d') TanggalBerangkat,
					   DATE_FORMAT(TanggalKembali,'%Y-%m-%d') TanggalKembali,
					   DATE_FORMAT(TanggalCheckOut,'%d %M %Y') TanggalCheckOut,
					   DATE_FORMAT(TanggalCheckIn,'%d %M %Y') TanggalCheckIn,
					   DATE_FORMAT(TanggalBerangkatPesawat,'%d %M %Y') TanggalBerangkatPesawat,
					   DATE_FORMAT(TanggalKepulanganPesawat,'%d %M %Y') TanggalKepulanganPesawat,
					  Tujuan, Alasan, a.NPKAtasan, NamaHotel, TiketPesawat,  
					   JamBerangkatPesawat,  JamKepulanganPesawat, a.CreatedOn, a.CreatedBy, StatusApprove, a.DPA,
					   case when StatusApprove='PE' then 'Pending Approval' else case when 'AP' then 'Approve' else case when 'DE' then 'Decline' end end end as KeteranganStatus,
					   AlasanDecline,a.CreatedBy as npkpemohon, u.nama as namapemohon
				from akomodasitiket a		
				join mstruser u on a.CreatedBy = u.NPK			
				where a.deleted = 0 and a.KodeAkomodasiTiket = ?";
		$query = $this->db->query($sql, $KodeAkomodasiTiket);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getTrxATbyNoAK($NoAkomodasiTiket){
		$sql = "select NoAkomodasiTiket, KodeAkomodasiTiket,  
					   DATE_FORMAT(TanggalBerangkat,'%Y-%m-%d') TanggalBerangkat,
					   DATE_FORMAT(TanggalKembali,'%Y-%m-%d') TanggalKembali,
					   DATE_FORMAT(TanggalCheckOut,'%d %M %Y') TanggalCheckOut,
					   DATE_FORMAT(TanggalCheckIn,'%d %M %Y') TanggalCheckIn,
					   DATE_FORMAT(TanggalBerangkatPesawat,'%d %M %Y') TanggalBerangkatPesawat,
					   DATE_FORMAT(TanggalKepulanganPesawat,'%d %M %Y') TanggalKepulanganPesawat,
					  Tujuan, Alasan, a.NPKAtasan, NamaHotel, TiketPesawat,  
					   JamBerangkatPesawat,  JamKepulanganPesawat, a.CreatedOn, a.CreatedBy, StatusApprove, a.DPA,
					   case when StatusApprove='PE' then 'Pending Approval' else case when 'AP' then 'Approve' else case when 'DE' then 'Decline' end end end as KeteranganStatus,
					   AlasanDecline,a.CreatedBy as npkpemohon, u.nama as namapemohon
				from akomodasitiket a		
				join mstruser u on a.CreatedBy = u.NPK			
				where a.deleted = 0 and a.NoAkomodasiTiket = ?";
		$query = $this->db->query($sql, $NoAkomodasiTiket);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	

	function integerToRoman($integer)
	{
		// Convert the integer into an integer (just to make sure)
		$integer = intval($integer);
		$result = '';
		
		// Create a lookup array that contains all of the Roman numerals.
		$lookup = array('M' => 1000,
		'CM' => 900,
		'D' => 500,
		'CD' => 400,
		'C' => 100,
		'XC' => 90,
		'L' => 50,
		'XL' => 40,
		'X' => 10,
		'IX' => 9,
		'V' => 5,
		'IV' => 4,
		'I' => 1);
		
		foreach($lookup as $roman => $value){
		// Determine the number of matches
		$matches = intval($integer/$value);
		
		// Add the same number of characters to the string
		$result .= str_repeat($roman,$matches);
		
		// Set the integer to be the remainder of the integer and the value
		$integer = $integer % $value;
		}
	
		// The Roman numeral should be built, return it
		return $result;
	}

	function NumberFromRoman($roman)
	{
		$romans = array(
			'M' => 1000,
			'CM' => 900,
			'D' => 500,
			'CD' => 400,
			'C' => 100,
			'XC' => 90,
			'L' => 50,
			'XL' => 40,
			'X' => 10,
			'IX' => 9,
			'V' => 5,
			'IV' => 4,
			'I' => 1,
		);		
		
		$result = 0;
		
		foreach ($romans as $key => $value) {
			while (strpos($roman, $key) === 0) {
				$result += $value;
				$roman = substr($roman, strlen($key));
			}
		}
		return $result;
	}

	function getDataATByKodeUT($KodeUserTask)
	{
				
		$sql = "SELECT tmp.NoAkomodasiTiket, tmp.KodeAkomodasiTiket, 
						DATE_FORMAT(TanggalBerangkat,'%d %M %Y') TanggalBerangkat,
					   DATE_FORMAT(TanggalKembali,'%d %M %Y') TanggalKembali,
					   DATE_FORMAT(TanggalCheckOut,'%d %M %Y') TanggalCheckOut,
					   DATE_FORMAT(TanggalCheckIn,'%d %M %Y') TanggalCheckIn,
					   DATE_FORMAT(TanggalBerangkatPesawat,'%d %M %Y') TanggalBerangkatPesawat,
					   DATE_FORMAT(TanggalKepulanganPesawat,'%d %M %Y') TanggalKepulanganPesawat,
					   Tujuan, Alasan, tmp.NPKAtasan, NamaHotel,TiketPesawat,  JamBerangkatPesawat, 
					    JamKepulanganPesawat, tmp.CreatedOn, tmp.DPA, AlasanDecline , u.nama namapemohon, tmp.CreatedBy npkpemohon,
					   rw.KodeUserTask, tmp.StatusApprove
				FROM akomodasitiket tmp  
			  	join dtltrkrwy rw on tmp.KodeAkomodasiTiket = rw.NoTransaksi
			  	join mstruser u on tmp.CreatedBy = u.NPK        
				where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function updateStatusAT($KodeUserTask, $status, $updatedby)
	{
		try{
			$sql = "update akomodasitiket l
				join dtltrkrwy rw on l.KodeAkomodasiTiket = rw.NoTransaksi
				set
					l.StatusApprove = ?,
					l.UpdatedOn = now(),
					l.UpdatedBy = ?
				where rw.KodeUserTask = ?";
			$query = $this->db->query($sql,array($status,$updatedby,$KodeUserTask));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	
}
?>