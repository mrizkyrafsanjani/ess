<?php 
Class User extends Ci_Model
{
	//TODO:tanda "/" dan "\" tidak di escape, coba di ganti dengan prepared statement
	function login($npk,$password)
	{
		try{
			/* $this->db->select('NPK,password,Nama,Golongan,Jabatan,Departemen,NPKAtasan,KodeRole');
			$this->db->from('mstruser');
			$this->db->where("NPK =  '$npk'");
			$this->db->where("password = '$password'");
			$this->db->where('deleted = 0');
			$this->db->where("(TanggalBerhenti = '0000-00-00' or TanggalBerhenti is null)");
			$this->db->limit(1);
			
			$query = $this->db->get();  */
			
			$sql = "select NPK,password,Nama,Golongan,Jabatan,Departemen,NPKAtasan,KodeRole 
					from mstruser 
					WHERE NPK = ? AND password = ? AND deleted = 0 
					AND (TanggalBerhenti = '0000-00-00' or TanggalBerhenti is null)
					LIMIT 1";
			$query = $this->db->query($sql, array($npk, $password));
			
			if($query->num_rows() == 1){
				return $query->result();
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}
	
	//TODO:tanda "/" dan "\" tidak di escape, coba di ganti dengan prepared statement
	function loginPasswordTemp($npk,$password)
	{
		
		/* $this->db->select('NPK,passwordTemp,Nama,Golongan,Jabatan,Departemen,NPKAtasan,KodeRole');
		$this->db->from('mstruser');
		$this->db->where("NPK = '$npk'");
		$this->db->where("passwordTemp = '$password'");
		$this->db->where('deleted = 0');
		$this->db->where("(TanggalBerhenti = '0000-00-00' or TanggalBerhenti is null)");
		$this->db->limit(1);
		$query = $this->db->get(); */
		
		$sql = "select NPK,passwordTemp,Nama,Golongan,Jabatan,Departemen,NPKAtasan,KodeRole
				from MstrUser
				WHERE NPK = ? AND passwordTemp = ? And deleted = 0 AND (TanggalBerhenti = '0000-00-00' or TanggalBerhenti is null)
				LIMIT 1";
		$query = $this->db->query($sql, array($npk, $password));
		
		
		if($query->num_rows() == 1){
			return $query->result();
		}else{
			return false;
		}
	}
	function dataUser($npk){
		try
		{
			$sql = "select a.npk,a.golongan,a.nama,a.emailInternal as email, a.NoExt as noExt, j.NamaJabatan as jabatan,d.namadepartemen as departemen,uangsaku,uangmakan,a.NPKAtasan as NPKAtasan, b.nama as atasan, a.TanggalBekerja,j.KodeJabatan,d.KodeDepartemen,a.KodeRole, a.DPA
				from mstruser a left join mstrgolongan g on a.golongan = g.golongan 
				left join mstruser b on a.npkatasan = b.npk and b.deleted = 0
				left join departemen d on a.departemen = d.kodedepartemen
				left join jabatan j on a.jabatan = j.KodeJabatan
				where a.npk = ? and a.deleted = 0";
			$query = $this->db->query($sql, $npk);
			
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

	}
	
	function findUserByNPK($npk){
		try
		{
			$sql = "SELECT * FROM mstruser where Deleted = 0 AND NPK = ?";
			$query = $this->db->query($sql, $npk);
			
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

	}
	
	function changePassword($npk,$password){
		$sql = "update mstruser set password = md5(?) where npk = ? ";
		$query = $this->db->query($sql,array($password."mX12J",$npk));
		return true;
		/*
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		*/
	}
	
	function cekPassword($npk,$password){
		$sql = "select password from mstruser where npk = ? and password = md5(?) and deleted = 0";
		$query = $this->db->query($sql,array($npk,$password."mX12J"));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function cekPasswordTemp($npk,$password){
		$sql = "select passwordtemp from mstruser where npk = ? and passwordtemp = md5(?) and deleted = 0";
		$query = $this->db->query($sql,array($npk,$password."mX12J"));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
		
	function insertPasswordTemp($npk,$password){
		$sql = "update mstruser set passwordtemp = md5(?) where npk = ? and deleted = 0";
		$query = $this->db->query($sql,array($password."mX12J",$npk));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function setNullPasswordTemp($npk){
		$sql = "update mstruser set passwordtemp = NULL where npk = ? and deleted = 0";
		$query = $this->db->query($sql,$npk);
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function get_departemen(){
		$sql = "select distinct NamaDepartemen as departemen from departemen where deleted = 0";
		$query = $this->db->query($sql);
		return $query->result();
		
	}
	
	function get_departemen_by_npk($npk){
		$sql = "select distinct d.NamaDepartemen as departemen 
			from mstruser u join departemen d on u.departemen = d.KodeDepartemen
			where u.npk = ? and u.deleted = 0";
		$query = $this->db->query($sql,$npk);
		return $query->result();
	}

	function getMenuAccess()
	{
		try{
			$session_data = $this->session->userdata('logged_in');
			$NPKlogin = $session_data['npk'];
			$sql = "select * 
					  from mstruser u 
					  left join rolemenu rm on u.KodeRole = rm.KodeRole
					  left join menu m on rm.KodeMenu = m.KodeMenu
					where u.NPK = ? and u.deleted =0 and rm.deleted = 0 and m.deleted = 0";
			$query = $this->db->query($sql,$NPKlogin);
			
			$result = $query->result();
			$kodeAccess = array();
			foreach($result as $result)
			{
				array_push($kodeAccess,$result->KodeMenu);
			}
			return $kodeAccess;
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	
	function getMenuNameAccess()
	{
		try{
			$session_data = $this->session->userdata('logged_in');
			$NPKlogin = $session_data['npk'];
			$sql = "select * 
					  from mstruser u 
					  left join rolemenu rm on u.KodeRole = rm.KodeRole
					  left join menu m on rm.KodeMenu = m.KodeMenu
					where u.NPK = ? and u.deleted =0 and rm.deleted = 0 and m.deleted = 0";
			$query = $this->db->query($sql,$NPKlogin);
			
			$result = $query->result();
			$menuNameAccess = array();
			foreach($result as $result)
			{
				array_push($menuNameAccess,$result->MenuName);
			}
			return $menuNameAccess;
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	
	function tambahKaryawan($proses,$namaFileUpload)
	{	
		try{
			$session_data = $this->session->userdata('logged_in');
			$NPKlogin = $session_data['npk'];
			$CreatedBy = $NPKlogin;
			
			$AlamatKTP = $this->input->post('txtAlamatKTP');
			$AlamatTinggal = $this->input->post('txtAlamatTinggal');
			$AtasNama = $this->input->post('txtAtasNama');
			$Cabang = $this->input->post('txtCabang');
			$DPA = $this->input->post('rbDPA');
			$EmailInternal = $this->input->post('txtEmailInternal');
			$Golongan = '1';//$this->input->post('cmbGolongan');
			$HubunganKeluarga = $this->input->post('txtHubunganKeluarga');
			$IDDanaPensiun = $this->input->post('txtIDManulife');
			$IDGardaMedika = $this->input->post('txtIDGardaMedika');
			$IDJamsostek = $this->input->post('txtIDJamsostek');
			$Jabatan = '';//$this->input->post('txtJabatan');
			$JenisKaryawan = $this->input->post('txtJenisKaryawan');
			$JenisKelamin = $this->input->post('rbJenisKelamin');
			$Keterangan = $this->input->post('txtKeterangan');
			$KodeDepartemen = '1';//$this->input->post('cmbDepartemen');
			$Nama = $this->input->post('txtNamaLengkap');
			$NamaBank = $this->input->post('txtBank');
			$NamaKontakKeluarga = $this->input->post('txtNamaKontak');
			$NoExt = $this->input->post('txtNoExt');
			$NoHP = $this->input->post('txtNoHP');
			$NoHPKontakKeluarga = $this->input->post('txtNoHPKontakKeluarga');
			$NoKTP = $this->input->post('txtNoKTP');
			$NoRek = $this->input->post('txtNoRekening');
			$NoTelp = $this->input->post('txtNoTelp');
			$NoTelpKontakKeluarga = $this->input->post('txtNoTelpKontakKeluarga');
			$NPK = $this->input->post('txtNPK');
			$NPWP = $this->input->post('txtNoNPWP');
			$StatusKaryawan = $this->input->post('cmbStatusKaryawan');
			$StatusKawin = $this->input->post('rbStatusKawin');
			$TanggalBekerja = $this->input->post('txtTanggalBekerja');
			$TanggalBerhenti = $this->input->post('txtTanggalBerhenti');
			$AlasanBerhenti = $this->input->post('txtAlasanBerhenti');
			$TanggalLahir = $this->input->post('txtTanggalLahir');
			$TempatLahir = $this->input->post('txtTempatLahir');
			
			$dataUserDb = $this->findUserByNPK($NPK);
			if($dataUserDb)
			{
				foreach($dataUserDb as $dataUserDb)
				{
					$urlDanaPensiun = $dataUserDb->urlDanaPensiun;
					$urlFoto = $dataUserDb->urlFoto;
					$urlGardaMedika = $dataUserDb->urlGardaMedika;
					$urlJamsostek = $dataUserDb->urlJamsostek;
					$urlKTP = $dataUserDb->urlKTP;
					$urlNPWP = $dataUserDb->urlNPWP;
				}
			}
			else
			{
				$urlDanaPensiun = "";
				$urlFoto = "";
				$urlGardaMedika = "";
				$urlJamsostek = "";
				$urlKTP = "";
				$urlNPWP = "";
			}
			$lokasiFolderUpload = "assets/uploads/files/";
			
			if (!empty($_FILES["inputFileManulife"]['name'])) {
				$urlDanaPensiun = $lokasiFolderUpload.$namaFileUpload['DanaPensiun'];
				fire_print('log',"urlDanaPensiun: $urlDanaPensiun");
			}
			if (!empty($_FILES["inputFileFoto"]['name'])) {	
				$urlFoto = $lokasiFolderUpload.$namaFileUpload['Foto'];
				fire_print('log',"urlFoto: $urlFoto");
			}
			if (!empty($_FILES["inputFileGardaMedika"]['name'])) {
				$urlGardaMedika = $lokasiFolderUpload.$namaFileUpload['GardaMedika'];
				fire_print('log',"urlGardaMedika: $urlGardaMedika");
			}
			if (!empty($_FILES["inputFileJamsostek"]['name'])) {	
				$urlJamsostek = $lokasiFolderUpload.$namaFileUpload['Jamsostek'];
				fire_print('log',"urlJamsostek: $urlJamsostek");
			}
			if (!empty($_FILES["inputFileKTP"]['name'])) {	
				$urlKTP = $lokasiFolderUpload.$namaFileUpload['KTP'];
				fire_print('log',"urlKTP: $urlKTP");
			}
			if (!empty($_FILES["inputFileNPWP"]['name'])) {
				$urlNPWP = $lokasiFolderUpload.$namaFileUpload['NPWP'];
				fire_print('log',"urlNPWP: $urlNPWP");
			}


			/* $tanggalBerangkatSPDddmmyyyy = $this->input->post('txtTanggalBerangkat');
			$tanggalBerangkatSPD = substr($tanggalBerangkatSPDddmmyyyy,6,4)."-".substr($tanggalBerangkatSPDddmmyyyy,3,2)."-".substr($tanggalBerangkatSPDddmmyyyy,0,2);
			$tanggalKembaliSPDddmmyyyy = $this->input->post('txtTanggalKembali');
			$tanggalKembaliSPD = substr($tanggalKembaliSPDddmmyyyy,6,4)."-".substr($tanggalKembaliSPDddmmyyyy,3,2)."-".substr($tanggalKembaliSPDddmmyyyy,0,2);
			$tujuan = $this->input->post('txtTujuan');
			$alasan = $this->input->post('txtAlasan');
			$DPA = $this->input->post('rbDPA');
			$uangMuka = $this->input->post('rbUangMuka');
			$NPKlogin = $session_data['npk'];		
			if($uangMuka == "1"){
				$totalSPD = str_replace(",","",$this->input->post('txtGrandTotal'));
			}else{
				$totalSPD = "0";
			} */
			
			//echo "$NPK $tanggalBerangkatSPD $tanggalKembaliSPD $tujuan $alasan $DPA $uangMuka $NPKlogin";
			
			if($proses == "edit"){
				$sql = "insert into rwymstruser(NPK, Deleted, Password, PasswordTemp, Nama, EmailInternal, Golongan, Jabatan, Departemen, NPKAtasan, KodeRole, JenisKelamin, TempatLahir, TanggalLahir, Agama, AlamatKTP, AlamatTinggal, NoTelp, NoHP, NamaKontakKeluarga, NoTelpKontakKeluarga, NoHPKontakKeluarga, HubunganKeluarga, StatusKawin, NoKTP, urlKTP, TanggalBekerja, NPWP, urlNPWP, IDDanaPensiun, urlDanaPensiun, urlFoto, TanggalBerhenti, AlasanBerhenti, JenisKaryawan, DPA, Keterangan, NoExt, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, NoRek, IDJamsostek, urlJamsostek, IDGardaMedika, urlGardaMedika, AtasNama, NamaBank, Cabang, StatusKaryawan)
						select NPK, Deleted, Password, PasswordTemp, Nama, EmailInternal, Golongan, Jabatan, Departemen, NPKAtasan, KodeRole, JenisKelamin, TempatLahir, TanggalLahir, Agama, AlamatKTP, AlamatTinggal, NoTelp, NoHP, NamaKontakKeluarga, NoTelpKontakKeluarga, NoHPKontakKeluarga, HubunganKeluarga, StatusKawin, NoKTP, urlKTP, TanggalBekerja, NPWP, urlNPWP, IDDanaPensiun, urlDanaPensiun, urlFoto, TanggalBerhenti, AlasanBerhenti, JenisKaryawan, DPA, Keterangan, NoExt, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, NoRek, IDJamsostek, urlJamsostek, IDGardaMedika, urlGardaMedika, AtasNama, NamaBank, Cabang, StatusKaryawan
						from mstruser
						where NPK = ?
					";
				$this->db->query($sql,array($NPK));
			}
			$this->db->trans_start();
			
			if($proses == "input"){
				
				$sql = "INSERT INTO mstruser (NPK, Deleted, Password, PasswordTemp, Nama, EmailInternal, Golongan, Jabatan, Departemen, NPKAtasan, KodeRole, JenisKelamin, TempatLahir, TanggalLahir, Agama, AlamatKTP, AlamatTinggal, NoTelp, NoHP, NamaKontakKeluarga, NoTelpKontakKeluarga, NoHPKontakKeluarga, HubunganKeluarga, StatusKawin, NoKTP, urlKTP, TanggalBekerja, NPWP, urlNPWP, IDDanaPensiun, urlDanaPensiun, UrlFoto, TanggalBerhenti, JenisKaryawan, DPA, Keterangan, NoExt, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, NoRek, IDJamsostek, urlJamsostek, IDGardaMedika, urlGardaMedika, AtasNama, NamaBank, Cabang,StatusKaryawan)
					VALUES (?,	0,	md5('abcDPA123@mX12J'),	md5('abcDPA123@mX12J'),	?,	?,	?,	?,	?,	null,	2,	?,	?,	?,	null,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	?,	now(),?,null,null,?,?,?,?,?,?,?,?,?)";
			
				$this->db->query($sql,array($NPK,$Nama,	$EmailInternal,	$Golongan,	$Jabatan,	$KodeDepartemen,$JenisKelamin,	$TempatLahir,	$TanggalLahir,$AlamatKTP,	$AlamatTinggal,	$NoTelp,	$NoHP,	$NamaKontakKeluarga,	$NoTelpKontakKeluarga,	$NoHPKontakKeluarga,	$HubunganKeluarga,	$StatusKawin,	$NoKTP,	$urlKTP,	$TanggalBekerja,	$NPWP,	$urlNPWP,	$IDDanaPensiun,	$urlDanaPensiun,	$urlFoto,	$TanggalBerhenti,	$JenisKaryawan,	$DPA,	$Keterangan,	$NoExt,		$CreatedBy,			$NoRek,	$IDJamsostek,	$urlJamsostek,	$IDGardaMedika,	$urlGardaMedika,	$AtasNama,	$NamaBank,	$Cabang, $StatusKaryawan));
				
				$whereStr = "WHERE CreatedBy = ? AND StatusTransaksi = 'PE' AND Deleted = 0 AND TempNPK IS NULL";
				$listTempTable = array('tempkeluarga','tempriwayatpekerjaan','tempriwayatpendidikan','tempriwayatrotasipekerjaandpa','tempriwayattraining');
				$listRealTable = array('keluarga','riwayatpekerjaan','riwayatpendidikan','riwayatrotasipekerjaandpa','riwayattraining');
				$listSelectTable = array(
					'Deleted, JenisKeluarga, HubunganKeluarga, Nama, JenisKelamin, TempatLahir, TanggalLahir, Pendidikan, Pekerjaan, Tanggungan, CreatedOn, CreatedBy',
					'Deleted, NamaPerusahaan, Jabatan, BulanMasuk, TahunMasuk, BulanKeluar, TahunKeluar, CreatedOn, CreatedBy',
					'Deleted, Pendidikan, Institusi, Jurusan, TahunKelulusan, CreatedOn, CreatedBy',
					'Deleted, KodeDepartemen, KodeJabatan, BulanRotasi, TahunRotasi,Golongan,Subgolongan, CreatedOn, CreatedBy',
					'Deleted, NamaTraining, Lembaga, DariTanggal, SampaiTanggal, SifatTraining, Biaya, Sertifikasi, Nilai, CreatedOn, CreatedBy'
				);
				
				for($i=0;$i<count($listRealTable);$i++)
				{
					$sql = "INSERT INTO ".$listRealTable[$i]." (NPK,".$listSelectTable[$i].") SELECT '".$NPK."',".str_replace("CreatedOn","NOW()",$listSelectTable[$i])." FROM ".$listTempTable[$i]." ".$whereStr;
					$this->db->query($sql,array($NPKlogin));
					
					$sql = "UPDATE ".$listTempTable[$i]." SET Deleted = 1,UpdatedBy=?,UpdatedOn=now(),StatusTransaksi='AP' ". $whereStr;
					$this->db->query($sql,array($NPKlogin,$NPKlogin));
				}
			}
			else if ($proses == "edit")
			{
				$sql = "UPDATE mstruser SET 
					Nama = ?,
					EmailInternal = ?,
					Golongan = ?, 
					Jabatan = ?, 
					Departemen = ?, 
					JenisKelamin = ?, 
					TempatLahir = ?, 
					TanggalLahir = ?, 
					AlamatKTP = ?, 
					AlamatTinggal = ?, 
					NoTelp = ?, 
					NoHP = ?, 
					NamaKontakKeluarga = ?, 
					NoTelpKontakKeluarga = ?, 
					NoHPKontakKeluarga = ?, 
					HubunganKeluarga = ?, 
					StatusKawin = ?, 
					NoKTP = ?, 
					urlKTP = ?, 
					TanggalBekerja = ?, 
					NPWP = ?, 
					urlNPWP = ?, 
					IDDanaPensiun = ?, 
					urlDanaPensiun = ?, 
					UrlFoto = ?, 
					TanggalBerhenti = ?, 
					JenisKaryawan = ?, 
					DPA = ?, 
					Keterangan = ?, 
					NoExt = ?, 
					UpdatedOn = NOW(), 
					UpdatedBy = ?, 
					NoRek = ?, 
					IDJamsostek = ?, 
					urlJamsostek = ?, 
					IDGardaMedika = ?, 
					urlGardaMedika = ?, 
					AtasNama = ?, 
					NamaBank = ?, 
					Cabang = ?,
					StatusKaryawan = ?,
					AlasanBerhenti = ?
					WHERE NPK = ?";
					
					$this->db->query($sql,array($Nama,	$EmailInternal,	$Golongan,	$Jabatan,	
						$KodeDepartemen,$JenisKelamin,	$TempatLahir,	$TanggalLahir,$AlamatKTP,	
						$AlamatTinggal,	$NoTelp,	$NoHP,	$NamaKontakKeluarga,	
						$NoTelpKontakKeluarga,	$NoHPKontakKeluarga,	$HubunganKeluarga,	
						$StatusKawin,	$NoKTP,	$urlKTP,	$TanggalBekerja,	$NPWP,	$urlNPWP,	
						$IDDanaPensiun,	$urlDanaPensiun,	$urlFoto,	$TanggalBerhenti,	
						$JenisKaryawan,	$DPA,	$Keterangan,	$NoExt,		$CreatedBy,			
						$NoRek,	$IDJamsostek,	$urlJamsostek,	$IDGardaMedika,	$urlGardaMedika,	
						$AtasNama,	$NamaBank,	$Cabang, $StatusKaryawan,$AlasanBerhenti,$NPK));
				
			}
			
			
			$this->db->trans_complete();
			
			
			$query = $this->db->query("select * from riwayatrotasipekerjaandpa
				where NPK = ? and Deleted = 0 
				order by tahunrotasi desc, bulanrotasi desc
				limit 1
				",array($NPK));
			foreach($query->result() as $row)
			{
				$kodeDept = $row->KodeDepartemen;
				$kodeJbtn = $row->KodeJabatan;
				$Golongan = $row->Golongan;
			}
			
			$sql = "UPDATE mstruser SET 
				Golongan = ?, 
				Jabatan = ?, 
				Departemen = ?				
				WHERE NPK = ?";
			
			$this->db->query($sql,array($Golongan,$kodeJbtn,$kodeDept,$NPK));
			
			return $this->db->trans_status();
			
			/* if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			} */
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	
	function tambahTempMstrUser($NPK,$KodeTempMstrUser)
	{
		try
		{
			$session_data = $this->session->userdata('logged_in');
			$NPKlogin = $session_data['npk'];
			$CreatedBy = $NPKlogin;
			
			$AlamatTinggal = $this->input->post('txtAlamatTinggal');
			$NoHP = $this->input->post('txtNoHP');
			$NoHPKontakKeluarga = $this->input->post('txtNoHPKontakKeluarga');
			$NoTelp = $this->input->post('txtNoTelp');
			$NoTelpKontakKeluarga = $this->input->post('txtNoTelpKontakKeluarga');
			$StatusKawin = $this->input->post('rbStatusKawin');
			$NamaKontakKeluarga = $this->input->post('txtNamaKontak');
			$HubunganKeluarga = $this->input->post('txtHubunganKeluarga');
			//$KodeTempMstrUser = generateNo('DP');
			
			$this->db->trans_start();
			
			$sql = "INSERT INTO tempmstruser(StatusTransaksi,KodeTempMstrUser,TempNPK,AlamatTinggal, NoHP, NoTelp, NoHPKontakKeluarga,NoTelpKontakKeluarga,
				StatusKawin,NamaKontakKeluarga,HubunganKeluarga,CreatedBy,CreatedOn) 
				VALUES ('PE',?,?,?,?,?,?,?,?,?,?,?,now())";
			$this->db->query($sql,array($KodeTempMstrUser,$NPK,$AlamatTinggal,$NoHP,$NoTelp,$NoHPKontakKeluarga,$NoTelpKontakKeluarga,
				$StatusKawin,$NamaKontakKeluarga,$HubunganKeluarga,$CreatedBy));
			
			$sql = "INSERT INTO tempriwayatpendidikan( TempNPK, Deleted, Pendidikan, Institusi, Jurusan, TahunKelulusan, StatusTransaksi, CreatedOn, CreatedBy) 
				SELECT TempNPK, Deleted, Pendidikan, Institusi, Jurusan, TahunKelulusan, 'PE', now(), ?
				FROM tempedituserriwayatpendidikan
				WHERE TempNPK = ?";
			$this->db->query($sql,array($NPK,$NPK));
			
			$sql = "INSERT INTO tempkeluarga( TempNPK, Deleted, JenisKeluarga, HubunganKeluarga, Nama, JenisKelamin, TempatLahir, TanggalLahir, Pendidikan, Pekerjaan, Tanggungan, StatusTransaksi, CreatedOn, CreatedBy) 
				SELECT TempNPK, Deleted, JenisKeluarga, HubunganKeluarga, Nama, JenisKelamin, TempatLahir, TanggalLahir, Pendidikan, Pekerjaan, Tanggungan, 'PE', now(), ?
				FROM tempedituserkeluarga
				WHERE TempNPK = ?";
			$this->db->query($sql,array($NPK,$NPK));
			
			$sql = "UPDATE tempriwayatpendidikan SET KodeTempMstrUser = ? WHERE StatusTransaksi = 'PE' AND TempNPK = ? ";
			$this->db->query($sql,array($KodeTempMstrUser,$NPK));
			
			$sql = "UPDATE tempkeluarga SET KodeTempMstrUser = ? WHERE StatusTransaksi = 'PE' AND TempNPK = ? AND JenisKeluarga = 'I' ";
			$this->db->query($sql,array($KodeTempMstrUser,$NPK));
			
			$this->db->trans_complete();
			
			return $this->db->trans_status();
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	
	function getLastTempNo()
	{
		$sql = "SELECT KodeTempMstrUser FROM tempmstruser ORDER BY KodeTempMstrUser DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->KodeTempMstrUser;
			}
		}
		else
		{
			return "0";
		}
	}
	
	function batalTambahKaryawan()
	{
		try
		{
			$session_data = $this->session->userdata('logged_in');
			$NPKlogin = $session_data['npk'];
			$whereStr = "WHERE CreatedBy = ? AND StatusTransaksi = 'PE' AND Deleted = 0 AND TempNPK IS NULL";
			$listTable = array('tempkeluarga','tempriwayatpekerjaan','tempriwayatpendidikan','tempriwayatrotasipekerjaandpa','tempriwayattraining');
			
			$this->db->trans_start();
			for($i=0;$i<count($listTable);$i++)
			{
				$sql = "UPDATE ".$listTable[$i]." SET Deleted = 1,UpdatedBy=?,UpdatedOn=now() ". $whereStr;
				$this->db->query($sql,array($NPKlogin,$NPKlogin));
			}
			$this->db->trans_complete();
			return true;
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	
	function cekTransaksiPerubahanDataKaryawan($NPK)
	{
		try
		{
			$sql = "SELECT * FROM tempmstruser WHERE TempNPK = ? AND StatusTransaksi = 'PE' AND Deleted = 0";
			$this->db->query($sql,array($NPK));
			
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	
	function updateLangsungUser($NPK,$AlamatTinggal,$NoTelp,$NoHP)
	{
		try
		{
			
			$sql = "UPDATE mstruser SET AlamatTinggal = ?, NoTelp = ?, NoHP = ?
				WHERE NPK = ?";
			
			$query = $this->db->query($sql,array($AlamatTinggal,$NoTelp,$NoHP,$NPK));
			
			return $query;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function updateDataPribadiFromTemp($KodeTempMstrUser, $status)
	{
		try
		{
			$this->db->trans_start();
			$sql = "insert into rwymstruser(NPK, Deleted, Password, PasswordTemp, Nama, EmailInternal, Golongan, Jabatan, Departemen, NPKAtasan, KodeRole, JenisKelamin, TempatLahir, TanggalLahir, Agama, AlamatKTP, AlamatTinggal, NoTelp, NoHP, NamaKontakKeluarga, NoTelpKontakKeluarga, NoHPKontakKeluarga, HubunganKeluarga, StatusKawin, NoKTP, urlKTP, TanggalBekerja, NPWP, urlNPWP, IDDanaPensiun, urlDanaPensiun, urlFoto, TanggalBerhenti, AlasanBerhenti, JenisKaryawan, DPA, Keterangan, NoExt, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, NoRek, IDJamsostek, urlJamsostek, IDGardaMedika, urlGardaMedika, AtasNama, NamaBank, Cabang, StatusKaryawan)
				select NPK, Deleted, Password, PasswordTemp, Nama, EmailInternal, Golongan, Jabatan, Departemen, NPKAtasan, KodeRole, JenisKelamin, TempatLahir, TanggalLahir, Agama, AlamatKTP, AlamatTinggal, NoTelp, NoHP, NamaKontakKeluarga, NoTelpKontakKeluarga, NoHPKontakKeluarga, HubunganKeluarga, StatusKawin, NoKTP, urlKTP, TanggalBekerja, NPWP, urlNPWP, IDDanaPensiun, urlDanaPensiun, urlFoto, TanggalBerhenti, AlasanBerhenti, JenisKaryawan, DPA, Keterangan, NoExt, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, NoRek, IDJamsostek, urlJamsostek, IDGardaMedika, urlGardaMedika, AtasNama, NamaBank, Cabang, StatusKaryawan
				from mstruser
				where NPK = (select TempNPK from tempmstruser where KodeTempMstrUser = ?)
			";
			$this->db->query($sql,array($KodeTempMstrUser));
			
			$sql = "update mstruser u
					  join (
						select * from tempmstruser where Deleted = 0 and KodeTempMstrUser = ? and StatusTransaksi = 'PE'
					  ) tu on u.NPK = tu.TempNPK
					set
					  u.AlamatTinggal = tu.AlamatTinggal,
					  u.NoTelp = tu.NoTelp,
					  u.NoHP = tu.NoHP,
					  u.StatusKawin = tu.StatusKawin,
					  u.NamaKontakKeluarga = tu.NamaKontakKeluarga,
					  u.HubunganKeluarga = tu.HubunganKeluarga,
					  u.NoHPKontakKeluarga = tu.NoHPKontakKeluarga,
					  u.NoTelpKontakKeluarga = tu.NoTelpKontakKeluarga";
			$this->db->query($sql,array($KodeTempMstrUser));
			
			//dihilangkan karena dianggap tidak perlu
			/* $sql = "update riwayatpendidikan
					set Deleted = 1, UpdatedOn = now(), UpdatedBy = (
					  select CreatedBy from tempmstruser
					  where Deleted = 0 and KodeTempMstrUser = ? and StatusTransaksi = 'PE' 
					)
					where NPK = (
					  select TempNPK 
					  from tempmstruser 
					  where KodeTempMstrUser = ? and StatusTransaksi = 'PE' 
					)";
			$this->db->query($sql,array($KodeTempMstrUser,$KodeTempMstrUser));
			
			$sql = "insert into riwayatpendidikan(NPK, Deleted, Pendidikan, Institusi, Jurusan, TahunKelulusan, CreatedOn, CreatedBy) 
					select TempNPK,0,Pendidikan,Institusi,Jurusan,TahunKelulusan,now(),CreatedBy from tempriwayatpendidikan 
					where KodeTempMstrUser = ? and StatusTransaksi = 'PE'";
			$this->db->query($sql,array($KodeTempMstrUser));
			 */
			 
			$sql = "update keluarga
					set Deleted = 1,UpdatedOn = now(), UpdatedBy = (
					  select CreatedBy from tempmstruser
					  where Deleted = 0 and KodeTempMstrUser = ? and StatusTransaksi = 'PE' 
					)
					where JenisKeluarga = 'I' and NPK = (
					  select TempNPK 
					  from tempmstruser 
					  where KodeTempMstrUser = ? and StatusTransaksi = 'PE' 
					);";
			$this->db->query($sql,array($KodeTempMstrUser,$KodeTempMstrUser));
			
			$sql = "insert into keluarga(NPK, Deleted, JenisKeluarga, HubunganKeluarga, Nama, JenisKelamin, TempatLahir, TanggalLahir, Pendidikan, Pekerjaan, Tanggungan, CreatedOn, CreatedBy) 
					select TempNPK,0,JenisKeluarga,HubunganKeluarga,Nama,JenisKelamin,TempatLahir,TanggalLahir,Pendidikan, Pekerjaan, Tanggungan, CreatedOn, CreatedBy from tempkeluarga 
					where KodeTempMstrUser = ? and StatusTransaksi = 'PE' and JenisKeluarga = 'I'";
			$this->db->query($sql,array($KodeTempMstrUser));
			$this->db->trans_complete();
			
			return true;
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	
	function refreshTempUser($NPK)
	{
		try
		{
			$this->db->trans_start();
			$sql = "delete from tempedituserriwayatpendidikan
			where TempNPK = ?";
			$this->db->query($sql,array($NPK));
			
			$sql = "delete from tempedituserkeluarga
			where JenisKeluarga = 'I' and TempNPK = ?";
			$this->db->query($sql,array($NPK));
			
			/* $sql = "delete from tempmstruser
			where CreatedBy = 'System' and StatusTransaksi = 'AP' and TempNPK = ?;";
			$this->db->query($sql,array($NPK)); */
			
			/*$sql="insert into tempmstruser
			(TempNPK, Deleted, Password, PasswordTemp, Nama, EmailInternal, Golongan, Jabatan, Departemen, NPKAtasan, KodeRole, JenisKelamin, TempatLahir, TanggalLahir, Agama, AlamatKTP, AlamatTinggal, NoTelp, NoHP, NamaKontakKeluarga, NoTelpKontakKeluarga, NoHPKontakKeluarga, HubunganKeluarga, StatusKawin, NoKTP, urlKTP, TanggalBekerja, NPWP, urlNPWP, IDDanaPensiun, urlDanaPensiun, UrlFoto, TanggalBerhenti, AlasanBerhenti, JenisKaryawan, DPA, Keterangan, NoExt, CreatedOn, CreatedBy, NoRek, IDJamsostek, urlJamsostek, IDGardaMedika, urlGardaMedika, AtasNama, NamaBank, Cabang, StatusTransaksi, StatusKaryawan)
			select NPK, Deleted, Password, PasswordTemp, Nama, EmailInternal, Golongan, Jabatan, Departemen, NPKAtasan, KodeRole, JenisKelamin, TempatLahir, TanggalLahir, Agama, AlamatKTP, AlamatTinggal, NoTelp, NoHP, NamaKontakKeluarga, NoTelpKontakKeluarga, NoHPKontakKeluarga, HubunganKeluarga, StatusKawin, NoKTP, urlKTP, TanggalBekerja, NPWP, urlNPWP, IDDanaPensiun, urlDanaPensiun, UrlFoto, TanggalBerhenti, AlasanBerhenti, JenisKaryawan, DPA, Keterangan, NoExt, now(), 'System', NoRek, IDJamsostek, urlJamsostek, IDGardaMedika, urlGardaMedika, AtasNama, NamaBank, Cabang, 'AP', StatusKaryawan
			from mstruser where npk not in (select npk from mstruser);";
			$this->db->query($sql,array($NPK));*/
			
			$sql="insert into tempedituserriwayatpendidikan
			(TempNPK, Deleted, Pendidikan, Institusi, Jurusan, TahunKelulusan, StatusTransaksi, CreatedOn, CreatedBy)
			select NPK, Deleted, Pendidikan, Institusi, Jurusan, TahunKelulusan, 'AP', now(), 'System'
			from riwayatpendidikan where NPK = ? and Deleted = 0";
			$this->db->query($sql,array($NPK));
			
			$sql="insert into tempedituserkeluarga
			(TempNPK, Deleted, JenisKeluarga, HubunganKeluarga, Nama, JenisKelamin, TempatLahir, TanggalLahir, Pendidikan, Pekerjaan, Tanggungan, StatusTransaksi, CreatedOn, CreatedBy)
			select NPK, Deleted, JenisKeluarga, HubunganKeluarga, Nama, JenisKelamin, TempatLahir, TanggalLahir, Pendidikan, Pekerjaan, Tanggungan, 'AP', now(), 'System'
			from keluarga where JenisKeluarga = 'I' and NPK = ? and Deleted = 0";
			$this->db->query($sql,array($NPK));		
			
			$this->db->trans_complete();
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	function isAdmin($npk){
		$data = $this->findUserByNPK($npk);
		foreach($data as $row){
			$kodeRole = $row->KodeRole;
		}
		if($kodeRole == 1){
			return true;
		}else{
			return false;
		}
	}
	function isHead($npk){
		$data = $this->findUserByNPK($npk);
		foreach($data as $row){
			$kodeRole = $row->KodeRole;
		}
		if($kodeRole == 4 || $kodeRole == 14){
			return true;
		}else{
			return false;
		}
	}
	
	function getDataBawahan($npkHead){		
		$data = "";
		if($this->isHead($this->npkLogin)){
			$databawahan = $this->db->query("SELECT NPK,Nama FROM mstruser
			WHERE deleted = 0 and departemen = (select departemen from mstruser where NPK = ?) and NPK != ? and (TanggalBerhenti = '0000-00-00' or TanggalBerhenti is null)
			ORDER BY Nama",array($this->npkLogin,$this->npkLogin));
		}else{
			$databawahan = $this->db->query("SELECT NPK,Nama FROM mstruser
			WHERE Deleted = 0 and (TanggalBerhenti = '0000-00-00' or TanggalBerhenti is null)
			ORDER BY Nama");
		}
		$data = $databawahan->result();
		return $data;
	}
	
	function integerToRoman($integer)
	{
		// Convert the integer into an integer (just to make sure)
		$integer = intval($integer);
		$result = '';
		
		// Create a lookup array that contains all of the Roman numerals.
		$lookup = array('M' => 1000,
		'CM' => 900,
		'D' => 500,
		'CD' => 400,
		'C' => 100,
		'XC' => 90,
		'L' => 50,
		'XL' => 40,
		'X' => 10,
		'IX' => 9,
		'V' => 5,
		'IV' => 4,
		'I' => 1);
		
		foreach($lookup as $roman => $value){
		// Determine the number of matches
		$matches = intval($integer/$value);
		
		// Add the same number of characters to the string
		$result .= str_repeat($roman,$matches);
		
		// Set the integer to be the remainder of the integer and the value
		$integer = $integer % $value;
		}
	
		// The Roman numeral should be built, return it
		return $result;
	}
	
	function getDIC($KodeDepartemen){
		switch($KodeDepartemen)
		{
			case 1: //Kepesertaan
			case 2: //HRGA
			case 5: //IT
				$DIC = "DIC_ITKpstHRGA";
				break;
			case 3: //Finance
				$DIC = "DIC_Finance";
				break;
			case 4: //Akunting
				$DIC = "DIC_Akunting";
				break;
		}
		$DICTarget = $this->db->get_where("globalparam",array("Name"=>$DIC));
		$NPKDIC = $DICTarget->row(1)->Value;
		$identitiasDIC = $this->db->get_where("mstruser",array("NPK"=>$NPKDIC));
		return $identitiasDIC->row(1);
	}
	function findUserGrupCutiByNPK($npk){
		try
		{
			$sql = "select a.NamaGroup from approvalgroup a
					join approvalgroupuser b  on a.KodeApprovalGroup=b.KodeApprovalGroup 
					where  a.Deleted = 0 and b.Deleted = 0 AND a.NamaGroup like 'Cuti%' and b.NPK = ?";
			$query = $this->db->query($sql, $npk);
			
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

	}
	function updateNPK($npk)
	{
		try{
			$session_data = $this->session->userdata('logged_in');
			$NPKlogin = $session_data['npk'];
			$CreatedBy = $NPKlogin;
			$statusKaryawan = "Tetap";

			$npkLama = $this->input->post('txtNPKLama');
			$TanggalMulaiBekerjaBaru = $this->input->post('txtTanggalMulaiBekerjaBaru');
			$npkBaru = $this->input->post('txtNPKBaru');
			$TanggalMulaiBekerjaLama = $this->input->post('txtTanggalMulaiBekerjaLama');		
			$exists = $this->findUserByNPK($npkBaru);
			if($exists)
			{
				return false;
			}
			else
			{
				$this->db->trans_start();
				$sql = "INSERT INTO riwayatperubahannpk(`Deleted`,`NPKLama`,`NPKBaru`,`TanggalMulaiBekerjaLama`,`TanggalMulaiBekerjaBaru`,`CreatedOn`,`CreatedBy`,`UpdatedOn`,`UpdatedBy`)VALUES(0, ?, ?, ?, ?, now(), ?, null, null)";
				$query = $this->db->query($sql, array($npkLama, $npkBaru, $TanggalMulaiBekerjaLama, $TanggalMulaiBekerjaBaru, $CreatedBy));
				$sql2 = "UPDATE mstruser set NPK = ?, UpdatedOn = now(), UpdatedBy = ?, StatusKaryawan = ?, TanggalBekerja = ? WHERE NPK = ?";
				$query2 = $this->db->query($sql2, array($npkBaru, $CreatedBy,$statusKaryawan, $TanggalMulaiBekerjaBaru, $npkLama));
				$this->db->trans_complete();
				return $this->db->trans_status();
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}

	function NumberFromRoman($roman)
	{
		$romans = array(
			'M' => 1000,
			'CM' => 900,
			'D' => 500,
			'CD' => 400,
			'C' => 100,
			'XC' => 90,
			'L' => 50,
			'XL' => 40,
			'X' => 10,
			'IX' => 9,
			'V' => 5,
			'IV' => 4,
			'I' => 1,
		);		
		
		$result = 0;
		
		foreach ($romans as $key => $value) {
			while (strpos($roman, $key) === 0) {
				$result += $value;
				$roman = substr($roman, strlen($key));
			}
		}
		echo $result;
	}
	function getAllUserByKodeDepartemen($departemen){
		$data = "";
		$SelectAllKaryawanInDepartemen = "SELECT NPK, EmailInternal, Departemen, Nama From Mstruser Where
		Departemen = ? and TanggalBerhenti is not null and TanggalBerhenti = '0000-00-00'";
		$dataKaryawanDepartemen = $this->db->query($SelectAllKaryawanInDepartemen, array($departemen));
		$result = $dataKaryawanDepartemen->result();
		$dataKaryawanDepartemen = array();
		foreach ($result as $user){
			if(!$this->isHead($user->NPK)){
				array_push($dataKaryawanDepartemen, $user);
			}
		}
		return $dataKaryawanDepartemen;
	}

	function getSingleUserBasedJabatan($namaJabatan){
		try{
			$sql = "select u.* from mstruser u join jabatan j on u.Jabatan = j.KodeJabatan
				where u.Deleted = 0 and j.Deleted = 0 and j.NamaJabatan like ? and (TanggalBerhenti = '0000-00-00' or TanggalBerhenti is null)";
			$query = $this->db->query($sql, $namaJabatan);
			
			if($query->num_rows() > 0){
				return $query->row();
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}

}
?>