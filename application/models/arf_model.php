<?php
Class Arf_model extends CI_Model{
    function findARFbyKode($kode){
        try
        {
            $sql = "SELECT NoARF, KodeARF, ReqDate, NPKUser, u.Nama as namauser, d.NamaDepartemen as deptuser, j.NamaJabatan as jabatanuser, u.EmailInternal as emailuser, u.NoEXT as extuser,
                    a.NPKAtasan, m.Nama as namaatasan, de.NamaDepartemen as deptatasan, ja.NamaJabatan as jabatanatasan, m.EmailInternal as emailatasan, m.NoExt as extatasan , ApplicationName, Subject, Request
                    FROM arf a
                    JOIN mstruser u ON a.NPKUser = u.npk
                    JOIN mstruser m ON a.NPKAtasan = m.npk
                    JOIN departemen d ON u.Departemen = d.KodeDepartemen
                    JOIN departemen de ON m.Departemen = de.KodeDepartemen
                    JOIN jabatan j ON u.Jabatan = j.KodeJabatan
                    JOIN jabatan ja ON m.Jabatan =  ja.KodeJabatan
                    WHERE a.Deleted =0 and a.KodeARF = ?";
            $query = $this->db->query($sql,array($kode) );

            if($query->num_rows() > 0){
                return $query->result();
            }else{
                return false;
            }
        }
        catch (Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    function getLastNumber(){
        try{
            $sql="Select max(KodeARF) as kode From arf ";
            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                $data = array();
                foreach($query->result() as $row)
                {
                    $data = array(
                        'kode' => $row->kode
                    );
                }
                return $data;

            }else{
                return false;
            }
        }catch (Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    function updateTerima($kode){
        try{
            $session_data = $this->session->userdata('logged_in');
            $NPKlogin = $session_data['npk'];
            $UpdatedBy = $NPKlogin;
            $status = "Diterima";

            $tahun = date('Y');
            $noarf = 'ARF/'.substr($tahun, -2).'/'.$this->user->integerToRoman(date('n'));
            $sqlSelect = "SELECT NoARF from arf where NoARF LIKE '%$noarf%' order by NoARF desc limit 1";
            $result = $this->db->query($sqlSelect);
            if($result->num_rows() == 0){
                $lastNumber = 0;
            }else{
                $currentnoarf=$result->result()[0]->NoARF;
                $lastNumber = explode('/', $currentnoarf);
                $lastNumber = $lastNumber[3];
            }
            $lastNumber++;
            $noarf = $noarf.'/'.str_pad($lastNumber,3,"0",STR_PAD_LEFT);

            $resolver= $this->input->post('cmbStatus');
            $exists = $this->findARFbyKode($kode);
            if($exists)
            {
                $this->db->trans_start();
                $this->insertRwyARF2($kode,$NPKlogin);
                
                $sql2 = "UPDATE arf set NoARF = ?, StatusARF = ?, ReceiveDate = now(), NPKResolver = ?, UpdatedOn = now(), UpdatedBy=? WHERE KodeARF = ?";
                if($this->db->query($sql2, array($noarf, $status,$resolver, $UpdatedBy, $kode))) {
                    
                    $userDataCreate= $this->findARFbyKode($kode);
                    if($userDataCreate){
                        foreach ($userDataCreate as $row) {
                            $emailuser= $row->emailuser;
                            $Subject= $row->Subject;
                            $req = $row->Request;
                            $status = "Diterima";
                            $email='it@dpa.co.id';
                        }
                    }else{
                        $emailuser='';
                        $Subject='';
                        $req='';
                        $email='';
                        $status = '';
                    }
                    

                    $this->load->library('email');
                    $this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
                    $recipientArr = array($email, $emailuser);

                    $this->email->to($recipientArr);

                    $this->email->subject('ARF Status '.$status.'');
                    $message = 'ARF dengan <br><br>

                    Perihal : '.$Subject.'<br>
                    Deskripsi : '.$req.'

                    kini statusnya telah Diterima Oleh tim IT dengan No '.$noarf.'
                    <br><br>Sekian Informasinya, 
                    <br>ESS';
                    $this->email->message($message);

                    if( ! $this->email->send())
                    {
                        fire_print('log','gagal email');
                        echo 'gagal';
                    }
                    else
                    {
                        fire_print('log','sukses email');
                    }
                }
                   
                $this->db->trans_complete();
                return $this->db->trans_status();
            }
            else
            {
                return false;
            }
        }catch (Exception $e){

        }
    }


    function updateTolak($kode){
        try{
            $session_data = $this->session->userdata('logged_in');
            $NPKlogin = $session_data['npk'];
            $UpdatedBy = $NPKlogin;
            $status = "Ditolak";

            $decline= $this->input->post('txtDecline');
            $exists = $this->findARFbyKode($kode);
            if($exists)
            {
                $this->db->trans_start();
                $this->insertRwyARF2($kode, $NPKlogin);
                $sql2 = "UPDATE arf set  StatusARF = ?, DeclineReason = ?, UpdatedOn = now(), UpdatedBy=? WHERE KodeARF = ?";
                if($this->db->query($sql2, array( $status,$decline, $UpdatedBy, $kode))){
                     
                    $userDataCreate= $this->findARFbyKode($kode);
                    if($userDataCreate){
                        foreach ($userDataCreate as $row) {
                            $emailuser= $row->emailuser;
                            $Subject= $row->Subject;
                            $req = $row->Request;
                            $email='it@dpa.co.id';
                            $status = "Ditolak";
                        }
                    }else{
                        $emailuser='';
                        $Subject='';
                        $req='';
                        $email='';
                        $status = '';
                    }
                    

                    $this->load->library('email');
                    $this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
                    $recipientArr = array($email, $emailuser);

                    $this->email->to($recipientArr);

                    $this->email->subject('ARF Status '.$status.'');
                    $message = 'ARF dengan <br>

                    Perihal : '.$Subject.'<br>
                    Deskripsi : '.$req.'

                    kini statusnya telah Ditolak Oleh tim IT dengan alasan : '.$decline.'
                    <br><br>Sekian Informasinya, 
                    <br>ESS';
                    $this->email->message($message);

                    if( ! $this->email->send())
                    {
                        fire_print('log','gagal email');
                        echo 'gagal';
                    }
                    else
                    {
                        fire_print('log','sukses email');
                    }
                }
                $this->db->trans_complete();
                return $this->db->trans_status();
            }
            else
            {
                return false;
            }
        }catch (Exception $e){

        }
    }

    function updateARFUser($kode){
        try{
            $session_data = $this->session->userdata('logged_in');
            $NPKlogin = $session_data['npk'];
            $UpdatedBy = $NPKlogin;

            $subject= $this->input->post('txtSubject');
            $aplikasi= $this->input->post('cmbAplikasi');
            $req= $this->input->post('txtReq');
            $exists = $this->findARFbyKode($kode);
            if($exists)
            {
                $this->db->trans_start();
                $this->insertRwyARF2($kode, $NPKlogin);
                $sql2 = "UPDATE arf set  Subject = ?, ApplicationName = ?, Request = ?, UpdatedOn = now(), UpdatedBy=? WHERE KodeARF = ?";
                if($this->db->query($sql2, array( $subject,$aplikasi, $req, $UpdatedBy, $kode))){
                     
                    $userDataCreate= $this->findARFbyKode($kode);
                    if($userDataCreate){
                        foreach ($userDataCreate as $row) {
                            $emailuser= $row->emailuser;
                            $email='it@dpa.co.id';
                        }
                    }else{
                        $emailuser='';
                        $email='';
                       
                    }
                    

                    $this->load->library('email');
                    $this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
                    $recipientArr = array($email, $emailuser);

                    $this->email->to($recipientArr);

                    $this->email->subject('ARF Status Diupdate');
                    $message = 'ARF dengan<br><br>

                    Subject : "'.$subject.'"<br>
                    Deskripsi : '.$req.'

                    kini telah dirubah oleh user dengan NPK '.$NPKlogin.'
                    <br><br>Sekian Informasinya, 
                    <br>ESS';
                    $this->email->message($message);

                    if( ! $this->email->send())
                    {
                        fire_print('log','gagal email');
                        echo 'gagal';
                    }
                    else
                    {
                        fire_print('log','sukses email');
                    }
                }
                $this->db->trans_complete();
                return $this->db->trans_status();
            }
            else
            {
                return false;
            }
        }catch (Exception $e){

        }
    }

    function diagram(){
        try{
            $query = $this->db->query("select departemen from arf");
            return $query->result();
        }catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    function insertRwyARF2($kode, $npk){
        $sql="INSERT into rwyarf(NoARF,ReqDate, NPKUser,Departemen, NPKAtasan, ApplicationName, Subject, Request, ReceiveDate,DeclineReason, NPKResolver, Priority,Category, StartWorkDate,StartWorkDateReal,TargetFinishDate,FinishDate,UATDate,ImplementationDate,NoteWork,StatusARF, Deleted, CreatedBy , CreatedOn)
            SELECT NoARF, ReqDate, NPKUser, Departemen, NPKAtasan, ApplicationName, Subject, Request, ReceiveDate, DeclineReason, NPKResolver, Priority, Category, StartWorkDate, StartWorkDateReal, TargetFinishDate, FinishDate, UATDate, ImplementationDate, NoteWork, StatusARF, Deleted, ?, NOW()
            FROM arf
            WHERE KodeARF = ?";
        $query =$this->db->query($sql, array($npk,$kode));
    }
}
?>