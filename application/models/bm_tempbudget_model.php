<?php 
Class Bm_tempbudget_model extends Ci_Model
{
	function insertoverwrite($data,$createdBy,$kodeTempBudgetHeader)
    {
        $success = 0;
        $KodeActivityQuery = "";
		$KodeActivityDelQuery = "";
		if($data[0] != "")
		{
			$KodeActivityDelQuery = " and KodeActivity = ?";
			$KodeActivityQuery = " and b.KodeActivity = ?";
		}
		$sql = "delete from bm_tempbudget
			where CreatedBy = ? ". $KodeActivityDelQuery;
		$query = $this->db->query($sql,array($createdBy,$data[0]));
		if($query){            
            for($i=1;$i<=12;$i++){
                $periode = $data[2]."-". $i."-1";
                $jumlahbudget = $data[2+$i];
                $sql = "insert into bm_tempbudget(KodeActivity,Status,KodeTempBudgetHeader,Periode,JumlahBudget,CreatedOn,CreatedBy)
                VALUES(?,?,?,?,?,NOW(),?)";
                $query = $this->db->query($sql,array($data[0],$data[1],$kodeTempBudgetHeader,$periode,$jumlahbudget,$createdBy));
                if($query) $success++;
            }
        }
		//$query = $this->db->get();
		
		if($success == 12){
			return true;
		}else{
			return false;
		}
    }

    function getUploadData($createdBy,$kodeUserTask = '',$dpa = '')
    {
        $strWhereDPA = "";
        if($dpa != '')
        {
            $strWhereDPA = " AND a.DPA = $dpa";
        }
        if($kodeUserTask == ''){
            $sql = "select tb.KodeTempBudgetHeader, k.NamaKategori, a.JenisPengeluaran , SUM(tb.JumlahBudget) as TotalBudget 
    from bm_tempbudget tb 
    join bm_tempbudgetheader tbh on tbh.KodeTempBudgetHeader = tb.KodeTempBudgetHeader
    join bm_activity a on a.KodeActivity = tb.KodeActivity
    join bm_kategori k on a.KodeKategori = k.KodeKategori
    where tb.Deleted = 0 AND tbh.StatusTransaksi = 'UP' AND tb.CreatedBy = ? $strWhereDPA
    group by tb.KodeTempBudgetHeader,k.NamaKategori, a.JenisPengeluaran
    order by a.JenisPengeluaran, k.NamaKategori;";
            $query = $this->db->query($sql,array($createdBy));
        }else{
            $sql = "select tb.KodeTempBudgetHeader, k.NamaKategori, a.JenisPengeluaran , SUM(tb.JumlahBudget) as TotalBudget 
    from bm_tempbudget tb 
    join bm_tempbudgetheader tbh on tbh.KodeTempBudgetHeader = tb.KodeTempBudgetHeader
    join bm_activity a on a.KodeActivity = tb.KodeActivity
    join bm_kategori k on a.KodeKategori = k.KodeKategori
    join dtltrkrwy dtr on tbh.KodeTempBudgetHeader = dtr.NoTransaksi
    where tb.Deleted = 0 AND tbh.StatusTransaksi = 'PE' AND dtr.KodeUserTask = ? $strWhereDPA
    group by tb.KodeTempBudgetHeader,k.NamaKategori, a.JenisPengeluaran
    order by a.JenisPengeluaran, k.NamaKategori;";
            $query = $this->db->query($sql,array($kodeUserTask));
        }
        if($query->num_rows() > 0)
        {
            return $query->result();
        }else{
            return false;
        }
    }
}
?>