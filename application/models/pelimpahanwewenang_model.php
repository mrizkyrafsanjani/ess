<?php
Class pelimpahanwewenang_model extends Ci_Model
{
	function getMinMaxTanggalCuti($KodeCuti)
	{
		$sql = "select min(TanggalMulai) as TanggalMulai, max(TanggalSelesai) as TanggalSelesai
			from cutijenistidakhadir
			where KodeCuti = ?";
		fire_print('log',"Kode Cuti get MinMax: $KodeCuti");
		$query = $this->db->query($sql,array($KodeCuti));
		if($query->num_rows() > 0){
			$dataMinMaxTanggalCuti = array();
			foreach($query->result() as $row)
			{
				fire_print('log',"TanggalMulaiCuti ".$row->TanggalMulai);
				$dataMinMaxTanggalCuti = array(
					'TanggalMulai' => $row->TanggalMulai,
					'TanggalSelesai' => $row->TanggalSelesai
				);
			}
			return $dataMinMaxTanggalCuti;
		}else{
			return false;
		}
	}

	function getDetailPelimpahanByNPK($npk)
	{
		$sql = "select Keterangan, KodeDtlPelimpahanWewenang, NPKYangDilimpahkan
			from dtlpelimpahanwewenang
			where CreatedBy = ? AND KodePelimpahanWewenang = '-' and Deleted=0";

		$query = $this->db->query($sql,array($npk));
		if($query->num_rows() > 0){
			$dataPelimpahanWewenang = array();
			foreach($query->result() as $row)
			{
				//fire_print('log',"TanggalMulaiCuti ".$row->TanggalMulai);
				$dataPelimpahanWewenang = array(
					'Keterangan' => $row->Keterangan,
                    'NPKYangDilimpahkan' => $row->NPKYangDilimpahkan,
                    'KodeDtlPelimpahanWewenang' => $row->KodeDtlPelimpahanWewenang
				);
			}
			return $dataPelimpahanWewenang;
		}else{
			return false;
		}
	}

	function getDetailPelimpahanByNoTransaksi($KodePelimpahanWewenang)
	{
		try
		{
			$sql = "select dtl.*, mu.Nama NamaYangDilimpahkan from dtlpelimpahanwewenang dtl
					left join mstruser mu
					on mu.NPK=dtl.NPKYangDIlimpahkan
					where dtl.KodePelimpahanWewenang = ? and dtl.Deleted=0";
			fire_print('log','KodePelimpahanWewenang proses getDetailPelimpahanByNoTransaksi='.$KodePelimpahanWewenang);
			$query = $this->db->query($sql,array($KodePelimpahanWewenang));
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}

	function getHeaderPelimpahanWewenang($NoTransaksi)
	{
		$sisacutibesar = '';
		$sisacutitahunan = '';
		$sql = "select KodePelimpahanWewenang, NoTransaksi, JenisTransaksi
				from pelimpahanwewenang
				where NoTransaksi = ?  and Deleted=0";
		$query = $this->db->query($sql,array($NoTransaksi));

		if($query->num_rows() > 0){
			$dataPelimpahanWewenang = array();
			foreach($query->result() as $row)
			{
				//fire_print('log',"TanggalMulaiCuti ".$row->TanggalMulai);
				$dataPelimpahanWewenang = array(
					'KodePelimpahanWewenang' => $row->KodePelimpahanWewenang,
                    'JenisTransaksi' => $row->JenisTransaksi
				);
			}
			return $dataPelimpahanWewenang;
		}

	}

	function getEmailPelimpahanByNoTransaksi($KodePelimpahanWewenang)
	{
		try
		{
			$sql = "select * from dtlemailpelimpahanwewenang
					where KodePelimpahanWewenang = ? and Deleted=0";
			fire_print('log','KodePelimpahanWewenang proses getEmailPelimpahanByNoTransaksi='.$KodePelimpahanWewenang);
			$query = $this->db->query($sql,array($KodePelimpahanWewenang));
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}

	function getMinMaxTanggalCutiOnEdit($kodeusertask)
	{
		$sql = "select KodeCuti,min(TanggalMulai) as TanggalMulai, max(TanggalSelesai) as TanggalSelesai
			from cutijenistidakhadir
			where KodeCuti = (select noTransaksi from dtltrkrwy where KodeUserTask = ? limit 1)
			group by KodeCuti";
		fire_print('log',"kodeusertask get MinMax: $kodeusertask");
		$query = $this->db->query($sql,array($kodeusertask));
		if($query->num_rows() > 0){
			$dataMinMaxTanggalCuti = array();
			foreach($query->result() as $row)
			{
				fire_print('log',"TanggalMulaiCuti ".$row->TanggalMulai);
				$dataMinMaxTanggalCuti = array(
					'TanggalMulai' => $row->TanggalMulai,
					'TanggalSelesai' => $row->TanggalSelesai,
					'KodeCuti' => $row->KodeCuti
				);
			}
			return $dataMinMaxTanggalCuti;
		}else{
			return false;
		}
	}

	function getAllTanggalOnProses($NPK)
	{
		try
		{
			fire_print('log','masuk ke getAllTanggalOnProses');
			$sql = "select * from cutijenistidakhadir
				where KodeCuti = '' AND Deleted = 0 AND CreatedBy = ?";
			$query = $this->db->query($sql,array($NPK));
			fire_print('log',"getAllTanggalOnProses. NPK: $NPK. jmlh baris query : ".$query->num_rows());
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	function getCutiTahunanOnProses($NPK,$Tahun='')
	{
		if($Tahun == '')
		{
			$sql = "select * from cutijenistidakhadir
				where KodeCuti = '' AND Deleted = 0 and KodeJenisTidakHadir = 1 and CreatedBy = ?";
			$query = $this->db->query($sql,array($NPK));
		}
		else
		{
			$sql = "select * from cutijenistidakhadir
				where KodeCuti = '' AND Deleted = 0 and KodeJenisTidakHadir = 1 and CreatedBy = ? and TahunCutiYangDipakai = ?";
			$query = $this->db->query($sql,array($NPK,$Tahun));
		}
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getCutiBesarOnProses($NPK,$Tahun='')
	{
		if($Tahun == '')
		{
			$sql = "select * from cutijenistidakhadir
				where KodeCuti = '' AND Deleted = 0 and KodeJenisTidakHadir = 2 and CreatedBy = ?";
			$query = $this->db->query($sql,array($NPK));
		}
		else
		{
			$sql = "select * from cutijenistidakhadir
				where KodeCuti = '' AND Deleted = 0 and KodeJenisTidakHadir = 2 and CreatedBy = ? and TahunCutiYangDipakai = ?";
			$query = $this->db->query($sql,array($NPK,$Tahun));
		}
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getHutangCutiTahunan($NPK,$Tahun)
	{
		try
		{
			$sql = "select cjt.* from cutijenistidakhadir cjt
							join cuti c on cjt.KodeCuti = c.KodeCuti
						  where c.StatusApproval = 'APR' and
							c.Deleted = 0 and cjt.Deleted = 0 and (cjt.KodeJenisTidakHadir = 1 or cjt.KodeJenisTidakHadir = 12) and
							cjt.TahunCutiYangDipakai = ? and c.NPK = ?
						  order by cjt.CreatedOn DESC";
			$query = $this->db->query($sql,array($Tahun, $NPK));

			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	function getMinMaxTanggalCutiDetail($KodeCutiJenisTidakHadir)
	{
		$sql = "select min(TanggalMulai) as TanggalMulai, max(TanggalSelesai) as TanggalSelesai
			from cutijenistidakhadir
			where KodeCutiJenisTidakHadir = ?";
		fire_print('log',"Kode Cuti get MinMax: $KodeCutiJenisTidakHadir");
		$query = $this->db->query($sql,array($KodeCutiJenisTidakHadir));
		if($query->num_rows() > 0){
			$dataMinMaxTanggalCuti = array();
			foreach($query->result() as $row)
			{
				fire_print('log',"TanggalMulaiCuti ".$row->TanggalMulai);
				$dataMinMaxTanggalCuti = array(
					'TanggalMulai' => $row->TanggalMulai,
					'TanggalSelesai' => $row->TanggalSelesai
				);
			}
			return $dataMinMaxTanggalCuti;
		}else{
			return false;
		}
	}

	function UpdateKeteranganPW($KodeUserTask,$updatedby)
	{
		try
		{

			$sql = "select * from Cuti l
				join dtltrkrwy rw on l.KodeCuti = rw.NoTransaksi
				where rw.KodeUserTask = ?";
			$query = $this->db->query($sql,$KodeUserTask);

			foreach($query->result() as $rw)
			{
				$NPK = $rw->NPK;
				$KodeCuti = $rw->KodeCuti;

				$pelimpahanwewenangList = $this->db->query("select * from pelimpahanwewenang
					where Deleted = 0 AND NoTransaksi = ?",$KodeCuti);
				if ($pelimpahanwewenangList)
				{
					foreach($pelimpahanwewenangList->result() as $pelimpahanwewenang)
					{
						$TanggalMulai= $pelimpahanwewenang->TanggalMulai;
						$TanggalSelesai= $pelimpahanwewenang->TanggalSelesai;

						$dtlpelimpahanwewenangList = $this->db->query("select a.*,b.Nama as NamaYangdilimpahkan
						from dtlpelimpahanwewenang a
						join mstruser b on b.NPK=a.NPKYangDilimpahkan
						where a.Deleted = 0 AND a.KodePelimpahanWewenang = ?",$pelimpahanwewenang->KodePelimpahanWewenang);

						$pelimpahanwewenangnama = '';
						foreach($dtlpelimpahanwewenangList->result() as $dtlpelimpahanwewenang)
						{
							$pelimpahanwewenangnama =$pelimpahanwewenangnama . $dtlpelimpahanwewenang->NamaYangdilimpahkan .' ' ;
						}

						$this->db->query("update Absensi set PelimpahanWewenang = ?,UpdatedOn = now(),UpdatedBy = ?
						where Date_format(tanggal, '%Y-%m-%d')  between Date_format(?, '%Y-%m-%d') and Date_format(?, '%Y-%m-%d') AND NPK = ?",array($pelimpahanwewenangnama,$this->npkLogin,$TanggalMulai,$TanggalSelesai, $pelimpahanwewenang->CreatedBy));
					}
				}

			}

			 if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}

			return true;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	function getPWByKode($KodePelimpahanWewenang)
	{
		try
		{
			$sql = "select p.KodePelimpahanWewenang,p.NoTransaksi,p.TanggalMulai,p.TanggalSelesai,m.Nama,p.CreatedBy as NPK from pelimpahanwewenang p
			join mstruser m on m.NPK=p.CreatedBy
			where p.Deleted=0 and p.KodePelimpahanWewenang = ?";

			$query = $this->db->query($sql,array($KodePelimpahanWewenang));
			if($query->num_rows() > 0){
				$dataMasterPW = array();
				foreach($query->result() as $row)
				{
					$dataMasterPW = array(
						'NoTransaksi' => $row->NoTransaksi,
						'Nama' => $row->Nama,
						'NPK' => $row->NPK,
						'TanggalMulai' =>  $row->TanggalMulai,
						'TanggalSelesai' =>  $row->TanggalSelesai
					);
				}
				return $dataMasterPW;
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}

	function getPWBreakDown($KodePelimpahanWewenang)
	{
		try
		{
			$sql = "select p.Keterangan,p.NPKYangDilimpahkan,m.nama from dtlpelimpahanwewenang p
					join mstruser m on m.NPK=p.NPKYangDilimpahkan
					where p.Deleted=0 and KodePelimpahanWewenang = ?";

			$query = $this->db->query($sql,array($KodePelimpahanWewenang));
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}
}
?>