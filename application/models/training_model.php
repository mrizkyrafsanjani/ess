<?php 
Class Training_model extends Ci_Model
{
	
	function getTrainingUser($npk)
	{
		$query = $this->user->dataUser($npk);
		foreach($query as $rw)
		{
			$departemen = $rw->departemen;
			$nama = $rw->nama;
			$jabatan = $rw->jabatan;
			
		}
	
	$hasil = array('departemen'=>$departemen,
	'nama'=>$nama,
	'jabatan'=>$jabatan);
		
		return $hasil;
		
	}
	
	function getTraining($KodeUserTask)
	{
		$sql = " select KodeTraining, training.NPK, ProgramTraining, Status, Penyelenggara, at.AlasanTraining, DetailAlasanTraining, TuntutanPekerjaan, KemampuanYangDiharapkan,  JobDescription, BiayaTraining,training.CreatedBy
			from training left join alasantraining at on training.KodeAlasanTraining = at.KodeAlasanTraining 
			  join dtltrkrwy rw on KodeTraining = rw.NoTransaksi
			  join mstruser u on training.NPK = u.NPK			  
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		
		fire_print('log','KodeUserTask Training: '.$KodeUserTask);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getKodeTraining($npk)
	{	
		$sql = "select KodeTraining from training
		where CreatedBy = ?
		ORDER BY CreatedOn DESC LIMIT 1";
		
		$query = $this->db->query($sql,array($npk));
		fire_print('log','getKodeTraining npk = '.$npk);
		
		if($query->num_rows() > 0){
			
			$KodeTraining = "";
			foreach($query->result() as $row)
			{
				$KodeTraining = $row->KodeTraining;
			}
			return $KodeTraining;
		}else{
			return false;
		}
	}
	
	function getByTransaksiTraining($KodeTraining)
	{
		try
		{
			$sql = " select l.NPK,l.KodeTraining, ProgramTraining, Status, Penyelenggara, KodeAlasanTraining, DetailAlasanTraining, TuntutanPekerjaan, KemampuanYangDiharapkan,  JobDescription, BiayaTraining
				from training l
				  join mstruser u on l.NPK = u.NPK
				where l.KodeTraining = ?";
			
			
			fire_print('log','KodeTraining proses getByTransaksiTraining ='.$KodeTraining. ". query = $sql");
			$query = $this->db->query($sql,array($KodeTraining));
			
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}
	
	function getDataTraining($KodeUserTask)
	{
		$sql = "select * from training
				ORDER BY CreatedOn DESC LIMIT 1";
		
		$query = $this->db->query($sql,array($KodeUserTask));
	
		if($query->num_rows() > 0){
			$KodeTraining = "";
			
			foreach($query->result() as $row)
			{
				$KodeTraining = $row->KodeTraining;
			}
			return $query->result();
			
		}else{
			return false;
		}
		
	}
	
	function updateStatusTraining($KodeUserTask, $status, $updatedby)
	{
		fire_print('log','masuk updateStatusTraining ');
		try{
			$sql = "update training l
				join dtltrkrwy rw on l.KodeTraining = rw.NoTransaksi
				set
					l.Status = ?,
					l.UpdatedOn = now(),
					l.UpdatedBy = ?
				where rw.KodeUserTask = ?";
				
			$query = $this->db->query($sql,array($status,$updatedby,$KodeUserTask));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	
	}
	
	function getEdit($npk)
	{	
		$sql = "select KodeTraining from training 
			  join dtltrkrwy rw on KodeTraining = rw.NoTransaksi	  
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($npk));
		fire_print('log','ini npk'.$npk);
		
		if($query->num_rows() > 0){
			
			$KodeTraining = "";
			foreach($query->result() as $row)
			{
				
				$KodeTraining = $row->KodeTraining;
				fire_print('log','kode training : '.$KodeTraining);
			}
			return $KodeTraining;
		}else{
			return false;
		}
	}
	
	function cetakTraining($KodeTraining)
	{
		$sql = " select * from training 
			  join dtltrkrwy rw on KodeTraining = rw.NoTransaksi	  
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeTraining));
		
		fire_print('log','KodeTraining: '.$KodeTraining);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	
	
	
	
	

}
?>