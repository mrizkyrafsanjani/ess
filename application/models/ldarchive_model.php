<?php 
Class LDArchive_model extends Ci_Model
{
	//TODO:tanda "/" dan "\" tidak di escape, coba di ganti dengan prepared statement
	
	function dataDokumen($KodeDokumen){
		try
		{
			$sql = "SELECT ms.KodeDokumen,
			ms.DocumentNo, 
			ms.DocumentName, 
			jd.JenisDokumenLegal, 
			ms.DokumenYangDiganti,
			ms.FilePath, ms.TanggalMulaiBerlaku, ms.TanggalKadaluarsa, ms.TanggalPengesahan, ms.status as Status from mstrdokumenlegal ms
			join jenisdokumenlegal jd on ms.KodeJenis = jd.KodeJenisDokumenLegal
			where ms.kodedokumen = ?";
			$query = $this->db->query($sql, $KodeDokumen);

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	function dataDokumenPajak($KodeDokumen){
		try
		{
			$sql = "SELECT mdp.KodeDokumenPajak, mdp.KodeJenis,
						mdp.DPA,
						mdp.Masa,
						mdp.Tahun,
						j.JenisDokumenLegal,
						jpph.JenisPPh,
						mdp.TanggalLaporan,
						mdp.NoBupot,
						mdp.Vendor, mdp.Nominal,
						mdp.FilePath,
						mu.Nama
				from mstrdokumenpajak mdp
					join jenispph jpph
					on jpph.KodeJenisPPh = mdp.JenisPPh
					join jenisdokumenlegal j on mdp.KodeJenis = j.KodeJenisDokumenLegal
				join mstruser mu on mdp.CreatedBy = mu.NPK
				where j.Deleted = 0 and mu.Deleted = 0 and jpph.Deleted = 0 and mdp.Deleted = 0
				and mdp.KodeDokumenPajak = ?
			";
			$query = $this->db->query($sql, $KodeDokumen);

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	function dataDokumenInduk($KodeDokumen){
		try
		{
			$sql = "SELECT mdp.KodeDokumenPajakInduk, mdp.KodeJenisDokumen,
							mdp.DPA,
							mdp.Masa,
							mdp.Tahun,
							j.JenisDokumenLegal,
							jpph.JenisPPh,
							mdp.TanggalLaporan,
							mdp.FilePath,
							mu.Nama
					from mstrdokumenpajakinduk mdp
						join jenispph jpph
						on jpph.KodeJenisPPh = mdp.JenisPPh
						join jenisdokumenlegal j on mdp.KodeJenisDokumen = j.KodeJenisDokumenLegal
					join mstruser mu on mdp.CreatedBy = mu.NPK
					where j.Deleted = 0 and mu.Deleted = 0 and jpph.Deleted = 0 and mdp.Deleted = 0
					and mdp.KodeDokumenPajakInduk = ?
			";
			$query = $this->db->query($sql, $KodeDokumen);

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	
	function dataHistory($KodeDokumen){
		try
		{
			$sql = "SELECT ms.KodeDokumen,
			ms.DocumentNo, 
			ms.DocumentName, 
			jd.JenisDokumenLegal, 
			ms.DokumenYangDiganti,
			ms.FilePath, ms.TanggalMulaiBerlaku, ms.TanggalKadaluarsa, ms.TanggalPengesahan from mstrdokumenlegal ms
			join jenisdokumenlegal jd on ms.KodeJenis = jd.KodeJenisDokumenLegal
			where ms.DokumenYangDiganti = ?";
			$query = $this->db->query($sql, $KodeDokumen);

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	function dataLaporan($KodeLaporan){
		try
		{
			$sql = "SELECT ml.KodeLaporan,
			ml.NamaLaporan, 
			ml.PeriodeLaporan,
			ml.DPA,
			DATE_FORMAT(ml.TanggalBatasAkhirPenyampaian,'%d %M %Y') AS TanggalBatasAkhirPenyampaian,
			ml.TanggalPengirimanLaporan,
			ml.UpdatedOn,
			mu.Nama
			from mstrreminderlaporan ml
			join mstruser mu on ml.UpdatedBy = mu.NPK 
			where ml.KodeLaporan = ?";
			$query = $this->db->query($sql, $KodeLaporan);

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	function picLaporan($KodeLaporan){
		try
		{
			$sql = "select mu.NPK, mu.Nama, mu.EmailInternal
			from picreminderlaporan pl
			join mstruser mu on pl.NPK = mu.NPK
			where KodeLaporan = ?";
			$query = $this->db->query($sql, $KodeLaporan);

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	function picDokumen($KodeDokumen){
		try
		{
			$sql = "select mu.NPK, mu.Nama, mu.EmailInternal
			from picdokumenlegal pl
			join mstruser mu on pl.NPK = mu.NPK
			where KodeDokumen = ?";
			$query = $this->db->query($sql, $KodeDokumen);

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}

	function getAllJenisDokumen(){
		try
		{
			$sql = "SELECT *
			from jenisdokumenlegal where deleted = 0";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	function getAllJenisDokumenInduk(){
		try
		{
			$sql = "SELECT *
			from jenisdokumenlegal where deleted = 0 and JenisDokumenlegal like '%Induk%'";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
	function getAllJenisPPh(){
		try
		{
			$sql = "SELECT *
			from jenispph where deleted = 0";
			$query = $this->db->query($sql);

			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
}
?>