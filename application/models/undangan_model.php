<?php 
Class Undangan_model extends Ci_Model
{
	function getKodeUndangan($npk)
	{	
		$sql = "select KodeUndangan from undangan
		where NPK = ? and Deleted = 0
		ORDER BY KodeUndangan DESC
		LIMIT 1";
		
		$query = $this->db->query($sql,array($npk));
		if($query->num_rows() > 0){
			$KodeUndangan = "";
			foreach($query->result() as $row)
			{
				$KodeUndangan = $row->KodeUndangan;
			}
			return $KodeUndangan;
		}else{
			return false;
		}
	}
	
	function getUndangan($KodeUserTask)
	{
		$sql = " select KodeUndangan, undangan.NPK, undangan.Status, undangan.Penyelenggara, tn.ProgramTraining, TanggalMulai, TanggalSelesai, AlamatTraining, undangan.BiayaTraining, Sertifikasi, Nilai, undangan.CreatedBy
			from undangan  left join training tn on undangan.KodeTraining = tn.KodeTraining 
			  join dtltrkrwy rw on KodeUndangan = rw.NoTransaksi
			  join mstruser u on undangan.NPK = u.NPK			  
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		
		fire_print('log','KodeUserTask undangan: '.$KodeUserTask);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	
	function getByTransaksiUndangan($KodeUndangan)
	{
		try
		{
			$sql = "select l.NPK,l.KodeUndangan, Status, Penyelenggara, TanggalMulai, TanggalSelesai, AlamatTraining, BiayaTraining, Sertifikasi, Nilai
				from undangan l
				  join mstruser u on l.NPK = u.NPK
				where l.KodeUndangan = ?";
			fire_print('log','KodeUndangan proses getByTransaksiUndangan='.$KodeUndangan);
			$query = $this->db->query($sql,array($KodeUndangan));
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}	
	
	/* function updateStatusUndangan($KodeUserTask, $status, $biayatraining, $sertifikasi, $nilai, $keterangan, $updatedby)
	{
		fire_print('log','masuk update undangan ');
		try{
			$sql = "update undangan l
				join dtltrkrwy rw on l.KodeUndangan = rw.NoTransaksi
				set
					l.Status = ?,
					1.BiayaTraining = ?,
					1.Sertifikasi = ?,
					1.Nilai = ?,
					1.Keterangan = ?,
					l.UpdatedOn = now(),
					l.UpdatedBy = ?
				where rw.KodeUserTask = ?";
				
			$query = $this->db->query($sql,array($KodeUserTask,$status, $biayatraining, $sertifikasi, $nilai, $keterangan, $updatedby));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	
	} */
	
	function updateStatusUndangan($KodeUserTask, $status, $updatedby)
	{
		fire_print('log','masuk update undangan ');
		try{
			$sql = "update undangan l
				join dtltrkrwy rw on l.KodeUndangan = rw.NoTransaksi
				set
					l.Status = ?,
					l.UpdatedOn = now(),
					l.UpdatedBy = ?
				where rw.KodeUserTask = ?";
				
			$query = $this->db->query($sql,array($status,$updatedby,$KodeUserTask));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	
	}
	
	
	function updateStatusUndanganRealisasi($KodeUserTask, $Status,$NPK, $biayatraining='', $nilai='', $sertifikasi='', $keterangan='', $KodeUndangan)
	{
		try
		{	
			fire_print('log','masuk update');
			$this->db->where('KodeUndangan',$KodeUndangan);
			return $this->db->update('undangan',array(
				'Status' => $Status,
				'BiayaTraining' => $biayatraining,
				'Nilai' => $nilai,
				'Sertifikasi' => $sertifikasi,
				'Keterangan' => $keterangan,
				'UpdatedOn' => date('Y-m-d H:i:s'),
				'UpdatedBy' => $NPK
			));
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function getEdit($npk)
	{	
		$sql = "select KodeUndangan from undangan 
			  join dtltrkrwy rw on KodeUndangan = rw.NoTransaksi	  
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($npk));
		fire_print('log','ini npk'.$npk);
		
		if($query->num_rows() > 0){
			
			$KodeUndangan = "";
			foreach($query->result() as $row)
			{
				
				$KodeUndangan = $row->KodeUndangan;
				fire_print('log','kode undangan : '.$KodeUndangan);
			}
			return $KodeUndangan;
		}else{
			return false;
		}
	}
	
	function getProgramTraining($npk){
		try
		{
			//fire_print('log','get program training');
			$sql = "select KodeTraining, NPK, ProgramTraining, Status, Penyelenggara,
					DetailAlasanTraining, TuntutanPekerjaan, KemampuanYangDiharapkan,
					JobDescription, BiayaTraining from training where NPK = ? and Status = 'APR' and deleted = '0' AND KodeTraining NOT IN (select KodeTraining from undangan where deleted = 0 and status != 'DER')";
			$query = $this->db->query($sql, $npk);
			
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

	}
	
	
	
	function getJumlahHari($tanggalmulaiView='',$tanggalselesaiView='')
	{
		try
		{
			
			if($tanggalmulaiView == '' && $tanggalselesaiView == ''){
				$tanggalmulai = $_POST['tanggalmulai'];
				$tanggalselesai = $_POST['tanggalselesai'];		
			}else
			{
				$tanggalmulai = $tanggalmulaiView;
				$tanggalselesai = $tanggalselesaiView;
			}
			$hariKerja = 0;
			
			$selisihHari = $this->s_datediff('d',$tanggalmulai,$tanggalselesai);
			
			
			for($i = 0; $i <= $selisihHari;$i++)
			{
				$time = strtotime('+'.$i.' days',strtotime($tanggalmulai));
				$kodeHari =  date('w',$time);
				//fire_print('log','Kode Hari :'.$kodeHari);
				
			}
			
			$hariKerja = $selisihHari+1;
			fire_print('log','Hari Kerja :'.$hariKerja." tanggalMulai: $tanggalmulai - $tanggalselesai");
			if($tanggalmulaiView == '' && $tanggalselesaiView =='')
				echo $hariKerja . ' hari kerja'; //untuk proses ajax di input
			
			return $hariKerja; //untuk column callback di view
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	
	function s_datediff( $str_interval, $dt_menor, $dt_maior, $relative=false){
		/* 
		 * A mathematical decimal difference between two informed dates 
		 *
		 * Author: Sergio Abreu
		 * Website: http://sites.sitesbr.net
		 *
		 * Features: 
		 * Automatic conversion on dates informed as string.
		 * Possibility of absolute values (always +) or relative (-/+)
		*/
	   if( is_string( $dt_menor)) $dt_menor = date_create( $dt_menor);
	   if( is_string( $dt_maior)) $dt_maior = date_create( $dt_maior);

	   $diff = date_diff( $dt_menor, $dt_maior, ! $relative);
	   
	   switch( $str_interval){
		   case "y": 
			   $total = $diff->y + $diff->m / 12 + $diff->d / 365.25; break;
		   case "m":
			   $total= $diff->y * 12 + $diff->m + $diff->d/30 + $diff->h / 24;
			   break;
		   case "d":
			   $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24 + $diff->i / 60;
			   break;
		   case "h": 
			   $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + $diff->i/60;
			   break;
		   case "i": 
			   $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + $diff->s/60;
			   break;
		   case "s": 
			   $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i)*60 + $diff->s;
			   break;
		  }
	   if( $diff->invert)
			   return -1 * $total;
	   else    return $total;
	}
}
?>