<?php 
Class Bm_summarybudget_model extends Ci_Model
{
	function kalkulasiBudgetSummary($kodeActivity, $DPA, $createdBy = '')
	{
		$paramWhere = Array();
		$strWhereDPA = " DPA in (1,2) ";
		$strWhereDPAQuery = " a.DPA in (1,2) ";
		if($DPA != "all")
		{
			$strWhereDPA = " DPA = ? ";
			$strWhereDPAQuery = " a.DPA = ? ";
			$paramWhere[] = $DPA;
		}
		$KodeActivityQuery = "";
		$KodeActivityDelQuery = "";
		if($kodeActivity != "")
		{
			if($DPA == "all")
			{
				$KodeActivityDelQuery = " and LEFT(KodeActivity,5) = ?";
				$KodeActivityQuery = " and LEFT(b.KodeActivity,5) = ?";
			}
			else
			{
				$KodeActivityDelQuery = " and KodeActivity = ?";
				$KodeActivityQuery = " and b.KodeActivity = ?";
			}
			$paramWhere[] = $kodeActivity;
		}
		$sql = "delete from bm_summarybudget
			where " . $strWhereDPA . $KodeActivityDelQuery . " AND CreatedBy = '$createdBy'";
		$query = $this->db->query($sql,$paramWhere);

		$sql = "INSERT into bm_summarybudget(KodeCoa,KodeActivity,JenisPengeluaran,BudgetYTD,BudgetFullYear,RealisasiYTD,TanggalLaporan,DPA,CreatedOn,CreatedBy)
		select a.KodeCoa,a.KodeActivity, a.JenisPengeluaran, IFNULL(sum(b.JumlahBudget),0) as BudgetYTD,IFNULL(t.budgetFullYear,0),IFNULL(r.RealisasiYTD,0),IFNULL(r.TanggalLaporan,'0000-00-00'), a.DPA,now(),'$createdBy' 
		from bm_budget b
		join bm_activity a on b.KodeActivity = a.KodeActivity
		join (select KodeActivity, sum(JumlahBudget) as budgetFullYear from bm_budget where deleted = 0 and year(Periode) = year(now()) group by KodeActivity) t on b.KodeActivity = t.KodeActivity 
		left join (select KodeActivity, max(TanggalLaporan) as TanggalLaporan, sum(Nominal) as RealisasiYTD from bm_realisasi where deleted = 0 and year(Tanggal) = year(now()) and Tanggal < now() group by KodeActivity ) r on r.KodeActivity = b.KodeActivity 
		where $strWhereDPAQuery ". $KodeActivityQuery ." and year(b.Periode) = year(now()) and b.periode < now() 
		group by a.KodeCoa,a.KodeActivity, a.JenisPengeluaran, t.budgetFullYear, r.RealisasiYTD,r.TanggalLaporan,a.DPA;
		";
		$query = $this->db->query($sql,$paramWhere);
		
		if($query){
			return true;
		}else{
			return false;
		}
	}
	function kalkulasiBudgetSummaryPPB($kodeActivity, $DPA, $createdBy = '', $TahunBudget)
	{
		$paramWhere = Array();
		$strWhereDPA = " DPA in (1,2) ";
		$strWhereDPAQuery = " a.DPA in (1,2) ";
		if($DPA != "all")
		{
			$strWhereDPA = " DPA = ? ";
			$strWhereDPAQuery = " a.DPA = ? ";
			$paramWhere[] = $DPA;
		}
		$KodeActivityQuery = "";
		$KodeActivityDelQuery = "";
		if($kodeActivity != "")
		{
			if($DPA == "all")
			{
				$KodeActivityDelQuery = " and LEFT(KodeActivity,5) = ?";
				$KodeActivityQuery = " and LEFT(b.KodeActivity,5) = ?";
			}
			else
			{
				$KodeActivityDelQuery = " and KodeActivity = ?";
				$KodeActivityQuery = " and b.KodeActivity = ?";
			}
			$paramWhere[] = $kodeActivity;
		}
		$sql = "delete from bm_temptotalbudget
			where " . $strWhereDPA . $KodeActivityDelQuery . " AND CreatedBy = '$createdBy'";
		$query = $this->db->query($sql,$paramWhere);

		$sql = "INSERT into bm_temptotalbudget(KodeCoa,KodeActivity,JenisPengeluaran,BudgetYTD,BudgetFullYear,RealisasiYTD,TanggalLaporan,DPA,CreatedOn,CreatedBy)
		select a.KodeCoa,a.KodeActivity, a.JenisPengeluaran, IFNULL(sum(b.JumlahBudget),0) as BudgetYTD,IFNULL(t.budgetFullYear,0),IFNULL(r.RealisasiYTD,0),IFNULL(r.TanggalLaporan,'0000-00-00'), a.DPA,now(),'$createdBy' 
		from bm_budget b
		join bm_activity a on b.KodeActivity = a.KodeActivity
		join (select KodeActivity, sum(JumlahBudget) as budgetFullYear from bm_budget where deleted = 0 and year(Periode) = ".$TahunBudget." group by KodeActivity) t on b.KodeActivity = t.KodeActivity 
		left join (select KodeActivity, max(TanggalLaporan) as TanggalLaporan, sum(Nominal) as RealisasiYTD from bm_realisasi where deleted = 0 and year(Tanggal) = year(now()) and Tanggal < now() group by KodeActivity ) r on r.KodeActivity = b.KodeActivity 
		where $strWhereDPAQuery ". $KodeActivityQuery ." and year(b.Periode) = ".$TahunBudget."
		group by a.KodeCoa,a.KodeActivity, a.JenisPengeluaran, t.budgetFullYear, r.RealisasiYTD,r.TanggalLaporan,a.DPA;
		";
		$query = $this->db->query($sql,$paramWhere);
		
		if($query){
			return true;
		}else{
			return false;
		}
	}
}
?>