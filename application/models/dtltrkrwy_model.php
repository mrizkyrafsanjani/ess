<?php 
Class Dtltrkrwy_model extends Ci_Model
{
	function getKodeUserTaskLast($notransaksi)
	{
		$sql = "select * from dtltrkrwy
			where NoTransaksi = ?
			order by CreatedOn Desc
			limit 1";
		
		$query = $this->db->query($sql,array($notransaksi));
		if($query->num_rows() > 0){
			$KodeUserTask = "";
			foreach($query->result() as $row)
			{
				$KodeUserTask = $row->KodeUserTask;
			}
			return $KodeUserTask;
		}else{
			return false;
		}
	}

	function getNoTransaksi($KodeUserTask)
	{
		$sql = "select * from dtltrkrwy where KodeUserTask = ? and deleted = 0";
		$query = $this->db->query($sql, array($KodeUserTask));
		if($query->num_rows() > 0){
			$notransaksi = "";
			foreach($query->result() as $row)
			{
				$notransaksi = $row->NoTransaksi;
			}
			return $notransaksi;
		}else{
			return false;
		}
	}
}
?>