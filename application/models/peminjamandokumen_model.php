<?php 
Class Peminjamandokumen_model extends Ci_Model
{
	function GetDetailByKodeFPD($KodeFPD){
		$sql = "SELECT * from pd_fpd
		where Deleted = 0 and KodeFPD = ?";
		$query = $this->db->query($sql,array($KodeFPD));
		if($query->num_rows() > 0){
			$rs = $query->result();
			foreach($rs as $value){
				return $value;
			}
		}else{
			return false;
		}
	}
	function GetDetailBySecretKey($SecretKey){
		$sql = "SELECT * from pd_fpd
		where Deleted = 0 and SecretKey = ?";
		$query = $this->db->query($sql,array($SecretKey));
		if($query->num_rows() > 0){
			$rs = $query->result();
			foreach($rs as $value){
				return $value;
			}
		}else{
			return false;
		}
	}
	function checkBorrowingStatus($NomorVoucher, $NomorPeserta){
		$sql = "SELECT IsBorrowed FROM pd_fpd where (NomorVoucher = ? and NomorPeserta = ?) and IsBorrowed = 1 and IsReceived = 1 and Deleted = 0";
		$query = $this->db->query($sql,array($NomorVoucher, $NomorPeserta));
		if($query->num_rows() > 0){
			return 'Sedang Dipinjam';
		}else{
			return 'Available';
		}
	}
	function GetBPKB($IdBPKB){
		$sql = "SELECT * from mstrbpkb
		where Deleted = 0 and IdBPKB = ?";
		$query = $this->db->query($sql,array($IdBPKB));
		if($query->num_rows() > 0){
			$rs = $query->result();
			foreach($rs as $value){
				return $value;
			}
		}else{
			return false;
		}
	}
	function generateKodeFPD($NPK, $DPA){
		$user = $this->user->dataUser($NPK);
		$sqlSelect = "SELECT KodeFPD from pd_FPD order by CreatedOn desc limit 1";
		$result = $this->db->query($sqlSelect);
		$lastNumber = 0;
		if($result->num_rows() == 0){
			$lastNumber = 0;
		}else{
			$currentKodeFPD=$result->result()[0]->KodeFPD;
			$lastNumber = explode('/', $currentKodeFPD);
			$lastNumber = $lastNumber[3];
		}
		$lastNumber++;
		if($lastNumber < 10){
			$lastNumber = '00'.$lastNumber;
		}else if($lastNumber <100){
			$lastNumber = '0'.$lastNumber;
		}
		$Departemen = '';
		if($user[0]->departemen == 'HRGA'){
			$Departemen = 'HRGA';
		}else if($user[0]->departemen == 'Mitra Relation & Communication'){
			$Departemen = 'MRC';
		}else if($user[0]->departemen == 'Claim & Actuary'){
			$Departemen = 'CA';
		}else if($user[0]->departemen == 'Kepesertaan'){
			$Departemen = 'KPST';
		}else if($user[0]->departemen == 'Finance & Investment'){
			$Departemen = 'FI';
		}else if($user[0]->departemen == 'IT'){
			$Departemen = 'IT';
		}else if($user[0]->departemen == 'Accounting Tax & Control'){
			$Departemen = 'ACCT';
		}else if($user[0]->departemen == 'Corporate Secretary'){
			$Departemen = 'CORSE';
		}else if($user[0]->departemen == 'Customer Relation'){
			$Departemen = 'CR';
		}else if($user[0]->departemen == 'Kepesertaan'){
			$Departemen = 'KPST';
		}
		$KodeFPD = 'FPD/'.$DPA.'/'.$Departemen.'/'.$lastNumber.'/'.$this->user->integerToRoman(date('n')).'/'.date('y');
		return $KodeFPD;
	}
	function getTrkBPKB($KodeUserTask)
	{
		$sql = "SELECT trk.NPK, KodeTrk, Nama,
				StatusApproval, Kepentingan, trk.IdBPKB from TrkBPKB trk
				join dtltrkrwy rw on trk.KodeTrk = rw.NoTransaksi
				join mstruser u on trk.NPK = u.NPK
					where rw.KodeUserTask = ?";
		$query = $this->db->query($sql,array($KodeUserTask));
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getTrkBPKBbyNoTransaksi($NoTransaksi)
	{
		$sql = "SELECT trk.NPK, KodeTrk, Nama,
				StatusApproval, Kepentingan, trk.IdBPKB,
				mb.NoPolisi, mb.NoBPKB, trk.TanggalTransaksi, trk.TanggalPengembalian, mb.TanggalPengambilan from TrkBPKB trk
				join mstruser u on trk.NPK = u.NPK
				join mstrbpkb mb on mb.IdBPKB = trk.IdBPKB
					where trk.KodeTrk = ?";
		$query = $this->db->query($sql,array($NoTransaksi));
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function updateStatusTrkBPKB($KodeUserTask, $status, $updatedby)
	{
		try{
			$sql = "update trkbpkb l
				join dtltrkrwy rw on l.KodeTrk = rw.NoTransaksi
				set
					l.StatusApproval = ?,
					l.UpdatedOn = now(),
					l.UpdatedBy = ?
				where rw.KodeUserTask = ?";
			$query = $this->db->query($sql,array($status,$updatedby,$KodeUserTask));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
}
?>