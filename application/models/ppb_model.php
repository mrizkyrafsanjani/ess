<?php
Class Ppb_model extends Ci_Model
{
	function getListPP($searchQuery)
	{
		if(!empty($searchQuery)){
			$sql = "SELECT * from persetujuanpengeluaranbudget
			where Deleted = 0 and Keterangan LIKE '%$searchQuery%'";
		}else{
			$sql = "SELECT * from persetujuanpengeluaranbudget
			where Deleted = 0";
		}
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getHeaderPP($ID){
		$sql = "SELECT * from persetujuanpengeluaranbudget
		where Deleted = 0 and ID = ?";
		$query = $this->db->query($sql,array($ID));
		if($query->num_rows() > 0){
			$rs = $query->result();
			foreach($rs as $value){
				return $value;
			}
		}else{
			return false;
		}
	}
	function getHeaderPPbyNoPP($NoPP){
		$sql = "SELECT * from persetujuanpengeluaranbudget
		where Deleted = 0 and NoPP = ?";
		$query = $this->db->query($sql,array($NoPP));
		if($query->num_rows() > 0){
			$rs = $query->result();
			foreach($rs as $value){
				return $value;
			}
		}else{
			return false;
		}
	}
	function getHeaderPPbyCreatedBy($CreatedBy){
		$sql = "SELECT NoPP from dtlppbkartubayar
		where Deleted = 0 and CreatedBy = ?";
		$query = $this->db->query($sql,array($CreatedBy));
		if($query->num_rows() > 0){
			$rs = $query->result();
			foreach($rs as $value){
				$resp = array();
				array_push($resp, $this->getHeaderPPbyNoPP($value->NoPP));
			}
			return $resp;
		}else{
			return false;
		}
	}
	function getDetailPP($NoPP){
		$sql = "SELECT * from dtlpersetujuanpengeluaranbudget
		where Deleted = 0 and NoPP = ?";
		$query = $this->db->query($sql,array($NoPP));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getDetailPRST($NoPP){
		$sql = "SELECT * from dtlppbpurchaserequest
		where Deleted = 0 and NoPP = ?";
		$query = $this->db->query($sql,array($NoPP));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getNoPPDPA2($npkLogin, $admin='0',$DPA='2')
	{
		$arrWhere = array();
		$strSelect = " * ";
		$strGroupBy = "";

		$strDPA = " dtl.DPA = ? ";
		$arrWhere[] = $DPA;
		if($admin == '1')
		{
			$sql = "SELECT ppb.ID, ppb.NoPP, ppb.Keterangan, ppb.FilePath, ba.NamaActivity from persetujuanpengeluaranbudget ppb
			join dtlpersetujuanpengeluaranbudget dtl
			ON ppb.NoPP = dtl.NoPP
			join bm_activity ba
			on LEFT(ppb.KodeActivity,5) = LEFT(ba.KodeActivity,5)
			where dtl.DPA != 1 and ppb.deleted = 0 and ppb.isReleased = 1 and ba.DPA = 2 and dtl.deleted = 0 and ".$strDPA;
		}
		$query = $this->db->query($sql,$arrWhere);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getDetailPPforCORE($ID){
		$sql = "SELECT * from persetujuanpengeluaranbudget ppb
		join dtlpersetujuanpengeluaranbudget dtl
		on ppb.NoPP = dtl.NoPP
		join bm_activity ba
		on ppb.KodeActivity = ba.KodeActivity
		where ppb.Deleted = 0 and dtl.Deleted = 0 and dtl.DPA = 2 and ba.DPA =2 and ppb.ID = ?";
		$query = $this->db->query($sql,array($ID));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function generateNoPP($npk, $DPA){
		$user = $this->user->dataUser($npk);
		$sqlSelect = "SELECT NoPP from persetujuanpengeluaranbudget where deleted = 0 order by CreatedOn desc limit 1";
		$result = $this->db->query($sqlSelect);
		$lastNumber = 0;
		if($result->num_rows() == 0){
			$lastNumber = 0;
		}else{
			$currentNoPP=$result->result()[0]->NoPP;
			$lastNumber = explode('/', $currentNoPP);
			$lastNumber = $lastNumber[3];
		}
		$lastNumber++;
		if($lastNumber < 10){
			$lastNumber = '00'.$lastNumber;
		}else if($lastNumber <100){
			$lastNumber = '0'.$lastNumber;
		}
		$Departemen = '';
		if($user[0]->departemen == 'HRGA'){
			$Departemen = 'HRGA';
		}else if($user[0]->departemen == 'Mitra Relation & Communication'){
			$Departemen = 'MRC';
		}else if($user[0]->departemen == 'Claim & Actuary'){
			$Departemen = 'CA';
		}else if($user[0]->departemen == 'Kepesertaan'){
			$Departemen = 'KPST';
		}else if($user[0]->departemen == 'Finance & Investment'){
			$Departemen = 'FI';
		}else if($user[0]->departemen == 'IT'){
			$Departemen = 'IT';
		}else if($user[0]->departemen == 'Accounting Tax & Control'){
			$Departemen = 'ACCT';
		}else if($user[0]->departemen == 'Corporate Secretary'){
			$Departemen = 'CORSE';
		}else if($user[0]->departemen == 'Customer Relation'){
			$Departemen = 'CR';
		}else if($user[0]->departemen == 'Kepesertaan'){
			$Departemen = 'KPST';
		}
		$NoPP = 'PP/'.$DPA.'/'.$Departemen.'/'.$lastNumber.'/'.$this->user->integerToRoman(date('n')).'/'.date('y');
		return $NoPP;
	}
	function insertPersetujuanPengeluaranBudget($NoPP, $Tipe, $KodeActivity, $JenisPengeluaran, $Keterangan, $txtSelStatus, $npk){
		$sqlInsertMstr = "INSERT INTO persetujuanpengeluaranbudget
						(Deleted, NoPP, Tipe, KodeActivity, JenisPengeluaran, Keterangan, Status, CreatedOn, CreatedBy)
						VALUES (0, ?, ?, ?, ?, ?, ?, now(), ?);
						";
		$this->db->query($sqlInsertMstr, array($NoPP, $Tipe, $KodeActivity, $JenisPengeluaran, $Keterangan, $txtSelStatus, $npk));
		return true;
	}

	function insertDetailPersetujuanPengeluaranBudget($NoPP, $DPA, $txtPengeluaran, $txtBudget, $txtRealisasi, $txtRealisasiBerjalan, $txtPengajuan, $txtSisaBudget, $npk, $TahunBudget){
		$sqlInsertDetail = "INSERT INTO dtlpersetujuanpengeluaranbudget
							(Deleted, NoPP, DPA, Pengeluaran, TahunBudget, Budget, Realisasi, RealisasiBerjalan, Pengajuan, SisaBudget, CreatedOn, CreatedBy)
							VALUES (0, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), ?);
							";
		$this->db->query($sqlInsertDetail, array($NoPP, $DPA, $txtPengajuan, $TahunBudget, $txtBudget, $txtRealisasi, $txtRealisasiBerjalan, $txtPengajuan, $txtSisaBudget, $npk));;
		return true;
	}
	function insertQtyPurchaseRequest($NoPP, $txtHargaSatuan, $txtQuantity, $npk){
		$sqlInsertQty = "INSERT INTO dtlppbpurchaserequest
						(Deleted, NoPP, HargaSatuan, Quantity, CreatedOn, CreatedBy)
						VALUES (0, ?,?,?, now(), ?);
						";
		$this->db->query($sqlInsertQty, array($NoPP, $txtHargaSatuan, $txtQuantity, $npk));
		return true;
	}
}
?>