<?php 
Class Bm_tempbudgetheader_model extends Ci_Model
{
	function insertoverwrite($createdBy,$kodeTempBudgetHeader)
    {
        $success = 0;        
		$sql = "update bm_tempbudgetheader set Deleted = 1, UpdatedOn = NOW(), UpdatedBy = ?
			where CreatedBy = ? AND StatusTransaksi = 'UP' AND UpdatedBy is null";
		$query = $this->db->query($sql,array($createdBy,$createdBy));
		if($query){
            $sql = "insert into bm_tempbudgetheader(KodeTempBudgetHeader,StatusTransaksi,CreatedOn,CreatedBy)
                VALUES(?,?,NOW(),?)";
            $query = $this->db->query($sql,array($kodeTempBudgetHeader,'UP',$createdBy));
        }
		//$query = $this->db->get();
		
		if($query){
			return true;
		}else{
			return false;
		}
    }
    
    function isexistpending($createdBy)
    {
        $sql = "select * from bm_tempbudgetheader 
			where CreatedBy = ? AND StatusTransaksi = 'PE'";
		$query = $this->db->query($sql,array($createdBy));
		
		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }

    function getByTransaksiTempBudgetHeader($KodeTempBudgetHeader)
    {
        $sql = "select KodeTempBudgetHeader, StatusTransaksi, CreatedBy 
				from bm_tempbudgetheader tbh
				where KodeTempBudgetHeader = ?";
        fire_print('log','KodeTempBudgetHeader proses getByTransaksiTempBudgetHeader='.$KodeTempBudgetHeader);
        $query = $this->db->query($sql,array($KodeTempBudgetHeader));
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    function updatestatustransaksi($KodeTempBudgetHeader,$createdBy,$statusTransaksi)
    {
        $sql = "update bm_tempbudgetheader set StatusTransaksi = ?, UpdatedOn = NOW(), UpdatedBy = ?
			where KodeTempBudgetHeader = ?";
		$query = $this->db->query($sql,array($statusTransaksi,$createdBy,$KodeTempBudgetHeader));
        if($query){
			return true;
		}else{
			return false;
		}
    }
}
?>