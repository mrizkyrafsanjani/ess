<?php 
Class Benefit_model extends Ci_Model
{	
	function getBenefitKodeUserTask($KodeUserTask)
	{
		$sql = "select b.*,u.Nama,jb.Deskripsi,k.Nama as NamaKeluarga, k.HubunganKeluarga 
			from benefit b 
			  join dtltrkrwy rw on b.KodeBenefit = rw.NoTransaksi
			  join mstruser u on b.NPK = u.NPK        
			  join jenisbenefit jb on b.KodeJenisBenefit = jb.KodeJenisBenefit
			  left join keluarga k on k.KodeKeluarga = b.KodeKeluarga
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function updateStatusBenefit($KodeUserTask, $status, $updatedby)
	{
		try{
			$sql = "update benefit b
				join dtltrkrwy rw on b.KodeBenefit = rw.NoTransaksi
				set
					b.StatusApproval = ?,
					b.UpdatedOn = now(),
					b.UpdatedBy = ?
				where rw.KodeUserTask = ?";
			$query = $this->db->query($sql,array($status,$updatedby,$KodeUserTask));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function updateAkumulasiBenefit($KodeUserTask,$updatedBy){
		try
		{
			$dataBenefit = $this->getBenefitKodeUserTask($KodeUserTask);
			foreach($dataBenefit as $dt)
			{
				$query = "select * from mstrbenefituser
					where Deleted = 0 AND NPK = ? AND Tahun = ?";
				$result = $this->db->query($query,array($dt->NPK,substr($dt->TanggalKlaim,0,4)));
				fire_print('log',"updateAkumulasiBenefit. result: ".print_r($result,true));
				foreach($result->result() as $rs){
					//update nilai akumulasi
					$this->db->update('mstrbenefituser',array(
						"AkumulasiBenefitTahunan"=>$rs->AkumulasiBenefitTahunan - $dt->TotalBayar,
						"UpdatedBy"=>$updatedBy,
						"UpdatedOn"=>date('Y-m-d H:i:s')
						),array("KodeMasterBenefitUser"=>$rs->KodeMasterBenefitUser));
				}
				return true;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}	
	
	function getSisaJatahMedical($NPK){
		$hasil = "0";
		$dataSisaJatahMedical = $this->db->query("SELECT * FROM mstrbenefituser WHERE Deleted = 0 AND Tahun = YEAR(NOW()) AND NPK = ?",array($NPK));
		if($dataSisaJatahMedical){
			foreach($dataSisaJatahMedical->result() as $dt){
				$hasil = $dt->AkumulasiBenefitTahunan;
			}
		}
		
		return $hasil;
	}
	
	function createKeteranganBenefit($keterangan,$atasnama){
		$hasil = "";
		switch($keterangan){
			case "Medical":
				$hasil = "Uang Obat an. $atasnama";
				break;
			case "1":
				$hasil = "Uang Obat an. $atasnama";
				break;
			default:
				$hasil = "$keterangan an. $atasnama";
				break;
		}
		return $hasil;
	}
	
	function getBenefitBelumKirim(){
		$dataBenefitBelumKirim = $this->db->query("SELECT * FROM benefit WHERE Deleted = 0 AND KodeBenefit NOT IN (select KodeBenefit FROM benefitsendtoaccpac WHERE Deleted = 0) AND StatusApproval = 'AP'");
		if($dataBenefitBelumKirim){
			return $dataBenefitBelumKirim->result();
		}else{
			return false;
		}
	}
	
	function getDataBenefit($KodeBenefit){
		$dataBenefit = $this->db->query('select u.Nama, jb.Deskripsi as JenisBenefit, b.Keterangan, b.TotalKlaim as Jumlah from benefit b join mstruser u on b.NPK = u.NPK
		join jenisbenefit jb on b.KodeJenisBenefit = jb.KodeJenisBenefit where b.KodeBenefit = ?',array($KodeBenefit));
		if($dataBenefit){
			return $dataBenefit->result();
		}else{
			return false;
		}
	}
}
?>