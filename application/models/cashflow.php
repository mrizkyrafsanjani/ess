<?php 
Class cashflow extends Ci_Model
{
		
	function updateCashINOUTDPA2($koderekening,$tgltransaksi,$npklogin)
	{
		try{

			$cashOUTsum = "0";
			$sqlcashOUTsum = "
							SELECT	*
							FROM temptrxdpa2 q
							where Date_format(q.tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='OUT' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')";
			$querycashOUTsum = $this->db->query($sqlcashOUTsum, array($tgltransaksi,$koderekening));
			if($querycashOUTsum->num_rows() == 0){			
				$updsqlcashOUTsum="
				UPDATE summarycashall_ p SET p.total = 0 , updatedby = ? ,	updatedon = now()
				where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='OUT' and p.koderekening=? 				
				";
	
				$updqueryCashOUTsum = $this->db->query($updsqlcashOUTsum,array($npklogin,$tgltransaksi,$koderekening));
			} 
			else
			{
				$updsqlcashOUT="
				UPDATE summarycashall_ p
				JOIN ( SELECT q.tipetransaksi,koderekening,tgltransaksi
							, SUM(ifnull(q.kredit,0)) AS sum_kredit
						FROM temptrxdpa2 q
						where Date_format(q.tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='OUT' and koderekening=?
						GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')
					) r
				ON Date_format(r.tgltransaksi, '%Y-%m-%d')  = Date_format(p.tanggal, '%Y-%m-%d')  and r.koderekening=p.koderekening and r.tipetransaksi=p.tipetransaksi
				SET p.total = r.sum_kredit , updatedby = ? ,	updatedon = now()
				where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='OUT' and p.koderekening=? 
				";
				$updqueryCashOUT = $this->db->query($updsqlcashOUT,array($tgltransaksi,$koderekening,$npklogin,$tgltransaksi,$koderekening));
			}

			$cashINsumFC = "0";
			$sqlcashINsumFC = "
							SELECT	*
							FROM trxforecastdpa2 q
							where Date_format(tglforecast, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
							";
			$querycashINsumFC = $this->db->query($sqlcashINsumFC, array($tgltransaksi,$koderekening));
			
			$cashINsum = "0";
			$sqlcashINsum = "
							SELECT	* 
							FROM temptrxdpa2 q
							where Date_format(q.tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')";
			$querycashINsum = $this->db->query($sqlcashINsum, array($tgltransaksi,$koderekening));
			if($querycashINsum->num_rows() == 0)
			{	
				if ($querycashINsumFC->num_rows()==0)		
				{	$updsqlcashINsum="
					UPDATE summarycashall_ p SET p.total = 0 , updatedby = ? ,	updatedon = now()
					where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='IN' and p.koderekening=? 				
					";
					$updqueryCashINsum = $this->db->query($updsqlcashINsum,array($npklogin,$tgltransaksi,$koderekening));
				} else
				{
					$updsqlcashINsum="
					UPDATE summarycashall_ p
					JOIN ( SELECT q.tipetransaksi,koderekening,tglforecast
								, SUM(ifnull(q.totalterima,0)) AS sum_debet
							FROM trxforecastdpa2 q
							where Date_format(tglforecast, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,Date_format(tglforecast, '%Y-%m-%d')
						) r
					ON Date_format(r.tglforecast, '%Y-%m-%d')  = Date_format(p.tanggal, '%Y-%m-%d')  and r.koderekening=p.koderekening and r.tipetransaksi=p.tipetransaksi
					SET p.total = r.sum_debet , updatedby = ? ,	updatedon = now()
					where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='IN' and p.koderekening=? 			
					";
					$updqueryCashINsum = $this->db->query($updsqlcashINsum,array($tgltransaksi,$koderekening,$npklogin,$tgltransaksi,$koderekening));
				}	
				
			}
			else
			{
				$updsqlcashIN="
				UPDATE summarycashall_ p
				JOIN ( 
						select tipetransaksi,koderekening,tgltransaksi, sum(sum_debet) as sum_debet
						from
						(  SELECT q.tipetransaksi,koderekening,tgltransaksi
										, SUM(ifnull(q.debet,0)) AS sum_debet
									FROM temptrxdpa2 q
									where Date_format(tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
									GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')
						union all
						select tipetransaksi,koderekening,tglforecast
										, SUM(ifnull(totalterima,0)) AS sum_debet
						  from trxforecastdpa2
						  where Date_format(tglforecast, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
									GROUP BY tipetransaksi,koderekening,Date_format(tglforecast, '%Y-%m-%d')
						) a
						group by tipetransaksi,koderekening,Date_format(tgltransaksi, '%Y-%m-%d')
					) r
				ON Date_format(r.tgltransaksi, '%Y-%m-%d')  = Date_format(p.tanggal, '%Y-%m-%d')  and r.koderekening=p.koderekening and r.tipetransaksi=p.tipetransaksi
				SET p.total = r.sum_debet , updatedby = ? ,	updatedon = now()
				where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='IN' and p.koderekening=? 
				";

				//echo $sqlcashIN;

				$updqueryCashIN = $this->db->query($updsqlcashIN,array($tgltransaksi,$koderekening,$tgltransaksi,$koderekening,$npklogin,$tgltransaksi,$koderekening));
			}
			

			return true;
			
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	function updateCashINOUTDPA1($koderekening,$tgltransaksi,$npklogin)
	{
		try{
				
			

			$cashOUTsum = "0";
			$sqlcashOUTsum = "
							SELECT	*
							FROM temptrxdpa1 q
							where Date_format(q.tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='OUT' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')";
			$querycashOUTsum = $this->db->query($sqlcashOUTsum, array($tgltransaksi,$koderekening));
			if($querycashOUTsum->num_rows() == 0){			
				$updsqlcashOUTsum="
					UPDATE summarycashall_ p SET p.total = 0 , updatedby = ? ,	updatedon = now()
					where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='OUT' and p.koderekening=? 				
				";
	
				$updqueryCashOUTsum = $this->db->query($updsqlcashOUTsum,array($npklogin,$tgltransaksi,$koderekening));
			}
			else
			{
				$updsqlcashOUT="
				UPDATE summarycashall_ p
				JOIN ( SELECT q.tipetransaksi,koderekening,tgltransaksi
							, SUM(ifnull(q.kredit,0)) AS sum_kredit
						FROM temptrxdpa1 q
						where Date_format(q.tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='OUT' and koderekening=?
						GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')
					) r
				ON r.tgltransaksi = p.tanggal and r.koderekening=p.koderekening and r.tipetransaksi=p.tipetransaksi
				SET p.total = r.sum_kredit , updatedby = ? ,	updatedon = now()
				where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='OUT' and p.koderekening=? 
				";
	
				$updqueryCashOUT = $this->db->query($updsqlcashOUT,array($tgltransaksi,$koderekening,$npklogin,$tgltransaksi,$koderekening));

			}

			$cashINsumFC = "0";
			$sqlcashINsumFC = "
							SELECT	* 
							FROM trxforecastdpa1 q
							where Date_format(tglforecast, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
							";
			$querycashINsumFC = $this->db->query($sqlcashINsumFC, array($tgltransaksi,$koderekening));

			$cashINsum = "0";
			$sqlcashINsum = "
							SELECT	* 
							FROM temptrxdpa1 q
							where Date_format(q.tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')";
			$querycashINsum = $this->db->query($sqlcashINsum, array($tgltransaksi,$koderekening));
			if($querycashINsum->num_rows() == 0){	
				if ($querycashINsumFC->num_rows()==0)		
				{	$updsqlcashINsum="
					UPDATE summarycashall_ p SET p.total = 0 , updatedby = ? ,	updatedon = now()
					where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='IN' and p.koderekening=? 				
					";
					$updqueryCashINsum = $this->db->query($updsqlcashINsum,array($npklogin,$tgltransaksi,$koderekening));
				} else
				{
					$updsqlcashINsum="
					UPDATE summarycashall_ p
					JOIN ( SELECT q.tipetransaksi,koderekening,tglforecast
								, SUM(ifnull(q.totalterima,0)) AS sum_debet
							FROM trxforecastdpa1 q
							where Date_format(tglforecast, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,Date_format(tglforecast, '%Y-%m-%d')
						) r
					ON Date_format(r.tglforecast, '%Y-%m-%d')  = Date_format(p.tanggal, '%Y-%m-%d')  and r.koderekening=p.koderekening and r.tipetransaksi=p.tipetransaksi
					SET p.total = r.sum_debet , updatedby = ? ,	updatedon = now()
					where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='IN' and p.koderekening=? 			
					";
					$updqueryCashINsum = $this->db->query($updsqlcashINsum,array($tgltransaksi,$koderekening,$npklogin,$tgltransaksi,$koderekening));
				}	
				
			}
			else
			{
				$updsqlcashIN="
				UPDATE summarycashall_ p
				JOIN ( 
						select tipetransaksi,koderekening,tgltransaksi, sum(sum_debet) as sum_debet
						from
						(  SELECT q.tipetransaksi,koderekening,tgltransaksi
										, SUM(ifnull(q.debet,0)) AS sum_debet
									FROM temptrxdpa1 q
									where Date_format(q.tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
									GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')
						union all
						select tipetransaksi,koderekening,tglforecast
										, SUM(ifnull(totalterima,0)) AS sum_debet
						  from trxforecastdpa1
						  where Date_format(tglforecast, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
									GROUP BY tipetransaksi,koderekening,Date_format(tglforecast, '%Y-%m-%d')
						) a
						group by tipetransaksi,koderekening,Date_format(tgltransaksi, '%Y-%m-%d')
					) r
				ON Date_format(r.tgltransaksi, '%Y-%m-%d')  = Date_format(p.tanggal, '%Y-%m-%d')  and r.koderekening=p.koderekening and r.tipetransaksi=p.tipetransaksi
				SET p.total = r.sum_debet , updatedby = ? ,	updatedon = now()
				where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='IN' and p.koderekening=? 
				";

				$updqueryCashIN = $this->db->query($updsqlcashIN,array($tgltransaksi,$koderekening,$tgltransaksi,$koderekening,$npklogin,$tgltransaksi,$koderekening));
			}
			
			return true;
			
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	function updateCashSaldo($koderekening,$tgltransaksi,$npklogin,$tipetransaksi)
	{
		try{
			
			$saldoawal = "0";
			$sqlsaldoawal = "select * from summarycashall_ where Date_format(tanggal, '%Y-%m-%d') =? and tipetransaksi='SALDOAWAL' and koderekening=?";
			$querysawal = $this->db->query($sqlsaldoawal, array($tgltransaksi,$koderekening));
			if($querysawal->num_rows() > 0){			
				foreach($querysawal->result() as $row)
				{
					$saldoawal = $row->total;
				}
			}
			
			$cashIN = "0";
			$sqlcashIN = "select * from summarycashall_ where Date_format(tanggal, '%Y-%m-%d')=? and tipetransaksi='IN' and koderekening=?";
			$querycashIN = $this->db->query($sqlcashIN, array($tgltransaksi,$koderekening));
			if($querycashIN->num_rows() > 0){				
				foreach($querycashIN->result() as $row)
				{
					$cashIN = $row->total;
				}
			}
			
			$cashOUT = "0";
			$sqlcashOUT = "select * from summarycashall_ where Date_format(tanggal, '%Y-%m-%d')=? and tipetransaksi='OUT' and koderekening=?";
			$querycashOUT = $this->db->query($sqlcashOUT, array($tgltransaksi,$koderekening));
			if($querycashOUT->num_rows() > 0){			
				foreach($querycashOUT->result() as $row)
				{
					$cashOUT = $row->total;
				}
			}

			
				$sqlcashsummary="
					UPDATE summarycashall_ p					
					SET p.total = ? + ? - ? , updatedby = ? ,	updatedon = now()
					where  Date_format(p.tanggal, '%Y-%m-%d')=? and p.tipetransaksi='SALDOAKHIR' and p.koderekening=?";

			$queryCashSUMMARY = $this->db->query($sqlcashsummary,array($saldoawal,$cashIN,$cashOUT,$npklogin,$tgltransaksi,$koderekening));

			//$cashtotal=

			/*if ($tipetransaksi=='IN')
			{ 
				if($querycashIN->num_rows() > 0)
				{	
					$sqlupdateallIN="
					UPDATE summarycashall_ p					
					SET p.total = p.total + ? , updatedby = ? ,	updatedon = now()
					where  Date_format(p.tanggal, '%Y-%m-%d') > ? and ( p.tipetransaksi='SALDOAKHIR' or p.tipetransaksi='SALDOAWAL' ) and p.koderekening=? 
					";
					$queryCashSUMMARYIN = $this->db->query($sqlupdateallIN,array($cashIN,$npklogin,$tgltransaksi,$koderekening));
				}				
			} else
			{
				if($querycashOUT->num_rows() > 0)
				{
				
					$sqlupdateallOUT="
					UPDATE summarycashall_ p					
					SET p.total = p.total - ? , updatedby = ? ,	updatedon = now()
					where  p.tanggal > ? and ( p.tipetransaksi='SALDOAKHIR' or p.tipetransaksi='SALDOAWAL' ) and p.koderekening=? 
					";
					$queryCashSUMMARYOUT = $this->db->query($sqlupdateallOUT,array($cashOUT,$npklogin,$tgltransaksi,$koderekening));
				}				
			}*/
			
			
			

			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
			
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function findForecastDPA2in($koderekening,$tgltransaksi){
		try
		{
			$sqlcashINfc = "SELECT	sum(totalterima) as jum
							FROM trxforecastdpa2 q
							where tglforecast=? and tipetransaksi='IN' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,tgltransaksi";
			$querycashINsumfc = $this->db->query($sqlcashINfc, array($tgltransaksi,$koderekening));			
			
			if($querycashINsumfc->num_rows() > 0){
				return $querycashINsumfc->result();
			}else{
				return 0;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

	}

	function findForecastDPA1in($koderekening,$tgltransaksi){
		try
		{
			$sqlcashINfc = "SELECT	sum(totalterima) as jum
							FROM trxforecastdpa1 q
							where tglforecast=? and tipetransaksi='IN' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,tgltransaksi";
			$querycashINsumfc = $this->db->query($sqlcashINfc, array($tgltransaksi,$koderekening));			
			
			if($querycashINsumfc->num_rows() > 0){
				return $querycashINsumfc->result();
			}else{
				return 0;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

	}

	function updateCashMOVEDPA2($koderekening,$tgltransaksi){
		try{
			//Start
			$cashOUTsum = "0";
			$sqlcashOUTsum = "
							SELECT	*
							FROM temptrxdpa2 q
							where Date_format(q.tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='OUT' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')";
			$querycashOUTsum = $this->db->query($sqlcashOUTsum, array($tgltransaksi,$koderekening));
			if($querycashOUTsum->num_rows() == 0){			
				$updsqlcashOUTsum="
				UPDATE summarycashall_ p SET p.total = 0 , updatedby = ? ,	updatedon = now()
				where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='OUT' and p.koderekening=? 				
				";
	
				$updqueryCashOUTsum = $this->db->query($updsqlcashOUTsum,array($npklogin,$tgltransaksi,$koderekening));
			} 
			else
			{
				$updsqlcashOUT="
				UPDATE summarycashall_ p
				JOIN ( SELECT q.tipetransaksi,koderekening,tgltransaksi
							, SUM(ifnull(q.kredit,0)) AS sum_kredit
						FROM temptrxdpa2 q
						where Date_format(q.tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='OUT' and koderekening=?
						GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')
					) r
				ON Date_format(r.tgltransaksi, '%Y-%m-%d')  = Date_format(p.tanggal, '%Y-%m-%d')  and r.koderekening=p.koderekening and r.tipetransaksi=p.tipetransaksi
				SET p.total = r.sum_kredit , updatedby = ? ,	updatedon = now()
				where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='OUT' and p.koderekening=? 
				";
				$updqueryCashOUT = $this->db->query($updsqlcashOUT,array($tgltransaksi,$koderekening,$npklogin,$tgltransaksi,$koderekening));
			}
			//End

			//Start
			$cashINsumFC = "0";
			$sqlcashINsumFC = "
							SELECT	*
							FROM trxforecastdpa2 q
							where Date_format(tglforecast, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
							";
			$querycashINsumFC = $this->db->query($sqlcashINsumFC, array($tgltransaksi,$koderekening));
			
			$cashINsum = "0";
			$sqlcashINsum = "
							SELECT	* 
							FROM temptrxdpa2 q
							where Date_format(q.tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')";
			$querycashINsum = $this->db->query($sqlcashINsum, array($tgltransaksi,$koderekening));
			if($querycashINsum->num_rows() == 0)
			{	
				if ($querycashINsumFC->num_rows()==0)		
				{	$updsqlcashINsum="
					UPDATE summarycashall_ p SET p.total = 0 , updatedby = ? ,	updatedon = now()
					where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='IN' and p.koderekening=? 				
					";
					$updqueryCashINsum = $this->db->query($updsqlcashINsum,array($npklogin,$tgltransaksi,$koderekening));
				} else
				{
					$updsqlcashINsum="
					UPDATE summarycashall_ p
					JOIN ( SELECT q.tipetransaksi,koderekening,tglforecast
								, SUM(ifnull(q.totalterima,0)) AS sum_debet
							FROM trxforecastdpa2 q
							where Date_format(tglforecast, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
							GROUP BY q.tipetransaksi,koderekening,Date_format(tglforecast, '%Y-%m-%d')
						) r
					ON Date_format(r.tglforecast, '%Y-%m-%d')  = Date_format(p.tanggal, '%Y-%m-%d')  and r.koderekening=p.koderekening and r.tipetransaksi=p.tipetransaksi
					SET p.total = r.sum_debet , updatedby = ? ,	updatedon = now()
					where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='IN' and p.koderekening=? 			
					";
					$updqueryCashINsum = $this->db->query($updsqlcashINsum,array($tgltransaksi,$koderekening,$npklogin,$tgltransaksi,$koderekening));
				}	
				
			}
			else
			{
				$updsqlcashIN="
				UPDATE summarycashall_ p
				JOIN ( 
						select tipetransaksi,koderekening,tgltransaksi, sum(sum_debet) as sum_debet
						from
						(  SELECT q.tipetransaksi,koderekening,tgltransaksi
										, SUM(ifnull(q.debet,0)) AS sum_debet
									FROM temptrxdpa2 q
									where Date_format(tgltransaksi, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
									GROUP BY q.tipetransaksi,koderekening,Date_format(q.tgltransaksi, '%Y-%m-%d')
						union all
						select tipetransaksi,koderekening,tglforecast
										, SUM(ifnull(totalterima,0)) AS sum_debet
						  from trxforecastdpa2
						  where Date_format(tglforecast, '%Y-%m-%d') =? and tipetransaksi='IN' and koderekening=?
									GROUP BY tipetransaksi,koderekening,Date_format(tglforecast, '%Y-%m-%d')
						) a
						group by tipetransaksi,koderekening,Date_format(tgltransaksi, '%Y-%m-%d')
					) r
				ON Date_format(r.tgltransaksi, '%Y-%m-%d')  = Date_format(p.tanggal, '%Y-%m-%d')  and r.koderekening=p.koderekening and r.tipetransaksi=p.tipetransaksi
				SET p.total = r.sum_debet , updatedby = ? ,	updatedon = now()
				where  Date_format(p.tanggal, '%Y-%m-%d') =? and p.tipetransaksi='IN' and p.koderekening=? 
				";

				//echo $sqlcashIN;

				$updqueryCashIN = $this->db->query($updsqlcashIN,array($tgltransaksi,$koderekening,$tgltransaksi,$koderekening,$npklogin,$tgltransaksi,$koderekening));
			//End
			}
			

			return true;
			
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	

}
?>