<?php 
Class Absensi extends Ci_Model
{
	function __construct()
    {
        parent::__construct();
		$this->load->helper('parse');
    }

	function getCountAbsen($npkKaryawanSelected, $Tahun, $Bulan, $npk)
	{
		$sqlKaryawanWhere = '';
		if($npkKaryawanSelected != '')
		{
			$sqlKaryawanWhere = " and a.npk like '%$npkKaryawanSelected%' ";
		}else if($npk != ''){
			$sqlKaryawanWhere = " and a.npk like '$npk' ";
		}
		
		$sql = "select a.keterangan, count(0) as total
			from absensi a join mstruser u on a.NPK = u.NPK
			where a.deleted = 0 
			  and year(tanggal) = ?
			  and month(tanggal) = ?
			  and a.Hari not in ('Sabtu','Minggu')
			  $sqlKaryawanWhere
			group by a.keterangan";
		
		fire_print('log',"sql get Count Absen : $sql");
		$query = $this->db->query($sql,array($Tahun,$Bulan));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getCountKehadiran($npkKaryawanSelected, $Tahun, $Bulan, $npk)
	{
		$sqlKaryawanWhere = '';
		if($npk != ''){
			$sqlKaryawanWhere = " and a.npk like '$npk' ";
		}else if($npkKaryawanSelected != '')
		{
			$sqlKaryawanWhere = " and a.npk like '%$npkKaryawanSelected%' ";
		} 
		
		$sql = "select count(0) as total
			from absensi a 
			where a.deleted = 0 
			  and year(tanggal) = ?
			  and month(tanggal) = ?
			  $sqlKaryawanWhere
			  and a.WaktuMasuk is not null and a.WaktuKeluar is not null
			  and a.WaktuMasuk != '00:00:00' and a.WaktuKeluar != '00:00:00'";
		
		$query = $this->db->query($sql,array($Tahun,$Bulan));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	
	function getCutiBersama($Tahun, $Bulan, $NPK)
	{
		$sql = "select 'Cuti Bersama' as Keterangan, count(0) as total from absensi
			where year(tanggal) = ? 
			and month(tanggal) = ?
			and npk = ?
			and deleted = 0
			and Keterangan =  'Cuti Bersama'
			group by Keterangan
			";
		
		$query = $this->db->query($sql,array($Tahun,$Bulan,$NPK));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	// ---- BEGIN - FITUR ABSENSI OTOMATIS ---- //
	// 1. Ambil kode absensi untuk cek apakah ada record pada tanggal tersebut
	function getRecordOnSomeDate($timenowYmd)
	{
		try{
			$sql = "select * FROM absensiauto where TanggalFormat = ?
			and Tanggal is not null
			and TanggalFormat is not null
			order by TanggalFormat asc limit 1";

			$query = $this->db->query($sql, array($timenowYmd));
			
			if($query){
				return $query->result();
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//2. Tarik data absensi dari mesin absensi
	function getDataAbsen($ipaddr, $key){

		print($ipaddr." | ".$key);
		//die();

		$IP = $ipaddr;
		$Key = $key;
        if($IP!=""){
        $Connect = fsockopen($IP, "80", $errno, $errstr, 1);
		if($Connect){

			print(" | Connect ke mesin | ");

			$soap_request="<GetAttLog>
							<ArgComKey xsi:type=\"xsd:integer\">".$Key."</ArgComKey>
							<Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";

			//------------- BEGIN - INI UNTUK AMBIL BERDASARKAN ID ABSENSI ----------------------------//
			// $soap_request = "<GetAttLog>
			// 							<ArgComKey xsi:type=\"xsd:integer\">" . $Key . "</ArgComKey>
			// 							<Arg>
			// 							<DateTime xsi:type=\"xsd:string\">" . date("Y-m-d H:i:s") . "</DateTime>
			// 							<PIN xsi:type=\"xsd:integer\">".$IDAbsensi."</PIN>
			// 							</Arg>
			// 							</GetAttLog>";
			//------------- END - INI UNTUK AMBIL BERDASARKAN ID ABSENSI ----------------------------//

			$newLine="\r\n";
			fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
			fputs($Connect, "Content-Type: text/xml".$newLine);
			fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
			fputs($Connect, $soap_request.$newLine);
			$buffer="";
			while($Response=fgets($Connect, 1024)){
				$buffer=$buffer.$Response;
			}
			$buffer = Parse_Data($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
			$buffer = explode("\r\n",$buffer);
			for($a=0;$a<count($buffer);$a++){
				$data = Parse_Data($buffer[$a],"<Row>","</Row>");
				$PIN = Parse_Data($data,"<PIN>","</PIN>");
				$DateTime = Parse_Data($data,"<DateTime>","</DateTime>");
				$Verified = Parse_Data($data,"<Verified>","</Verified>");
				$Status = Parse_Data($data,"<Status>","</Status>");

				//Other data
				$kdabsensidata = $this->getKdAbsensiData();
				$dateformat = substr($DateTime,0,10);
				$dateformatreplace = str_replace("-","",$dateformat);
				$timesubsting = substr($DateTime,11,8);
				$timesubstingreplace = str_replace(":","",$timesubsting);
				$kodeunik = $PIN.$dateformatreplace.$timesubstingreplace;
				$datetimeformat = $dateformatreplace . $timesubstingreplace;

				//---------------- BEGIN - AMBIL NPK DAN NAMA BERDASARKAN ID ABSENSI -------------//
				$data_npk_nama = $this->getNPKNama($PIN);
				$npk = "";
				$nama = "";
				foreach ($data_npk_nama as $data)
				{
					$npk = $data->NPK;
					$nama = $data->Nama;
				}
				//---------------- END - AMBIL NPK DAN NAMA BERDASARKAN ID ABSENSI -------------//

				//---------------- BEGIN - ATUR NAMA HARI -------------//
				$namahari = date('D', strtotime($dateformat));

				if($namahari == "Mon"){
					$namahari = "Senin";
				}else if($namahari == "Tue"){
					$namahari = "Selasa";
				}else if($namahari == "Wed"){
					$namahari = "Rabu";
				}else if($namahari == "Thu"){
					$namahari = "Kamis";
				}else if($namahari == "Fri"){
					$namahari = "Jumat";
				}else if($namahari == "Sat"){
					$namahari = "Sabtu";
				}else if($namahari == "Sun"){
					$namahari = "Minggu";
				}
				//---------------- END - ATUR NAMA HARI -------------//

				print(" | KdAbsensiData = ".$kdabsensidata."; NPK = ".$npk."; Tanggal = ".$dateformat."; Hari = ".$namahari." | ");

				$insertToDataAbsensi = $this->setAbsensiData(
					$kdabsensidata,
					$PIN,
					$npk,
					$nama,
					$DateTime,
					$namahari,
					$Verified,
					$Status,
					$dateformat,
					$dateformatreplace,
					$timesubsting,
					$timesubstingreplace,
					$datetimeformat
				);

				print(" | Success insert setAbsensiData | ");
					

			}
			if($buffer){
				return '<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-check"></i> Success !</h4>
					Anda terhubung dengan mesin.
				</div>';
			} else {
				return '<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-ban"></i> Alert!</h4>
					Anda tidak terhubung dengan mesin !
				</div>';
			}
		
            }
        } 
	}

	//3. Generate kode absensi data
	function getKdAbsensiData(){
		$result = $this->db->query("SELECT kdabsensidata FROM absensidata ORDER BY kdabsensidata DESC LIMIT 1");
		// $result = $this->db->get();
		$getDateNow = date('ymd');
		$kd="";
		if($result->num_rows()>0)
		{
				foreach($result->result() as $kd)
				{
					// echo "kode".$kd->KdPromosi;
					$splitVar_start = substr($kd->kdabsensidata,0,6);
					// echo "splitVar_start = ".$splitVar_start;
					$splitVar_end = substr($kd->kdabsensidata,6,14);
					// echo "splitVar_end = ".$splitVar_end;
					if($getDateNow == $splitVar_start)
					{
						$tmp = ((int)$splitVar_end)+1;
						$count = str_pad($tmp,9,"0",STR_PAD_LEFT);
						$kd = "$splitVar_start"."$count";
						// echo "jika tanggal sekarang sama".$kd;
					}
					else
					{
						$kd = "$getDateNow"."000000001";
						// echo "jika tanggal sekarang beda".$kd;
						
					}
				}    
		}
		else
		{
			$kd = "$getDateNow"."000000001";
			// echo "jika tidak ada record".$kd;
			
		}
		return $kd;    
	}

	//4. Ambil data NPK dan Nama karyawan
	function getNPKNama($UserIDAbsensi)
	{
		try{
			$sql = "SELECT NPK, Nama FROM mstruser WHERE UserIDAbsensi = ?";

			$query = $this->db->query($sql, array($UserIDAbsensi));
			
			if($query){
				return $query->result();
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//5. Insert into table 'absensidata'
	function setAbsensiData(
		$kdabsensidata,
		$PIN,
		$npk,
		$nama,
		$DateTime,
		$namahari,
		$Verified,
		$Status,
		$dateformat,
		$dateformatreplace,
		$timesubsting,
		$timesubstingreplace,
		$datetimeformat)
	{

		try{
			$sql = "INSERT into absensidata 
			(kdabsensidata, 
			pin, 
			npk, 
			nama,
			date_time, 
			`dayname`, 
			ver, 
			status, 
			`date`, 
			dateformat, 
			`time`, 
			timeformat, 
			datetimeformat,
			createdby,
			createdon
			)
			VALUES
			(?,?,?,?,?,?,?,?,?,?,?,?,?,'SYSTEM',NOW())";

			$query = $this->db->query($sql, array(
				$kdabsensidata,
				$PIN,
				$npk,
				$nama,
				$DateTime,
				$namahari,
				$Verified,
				$Status,
				$dateformat,
				$dateformatreplace,
				$timesubsting,
				$timesubstingreplace,
				$datetimeformat,
			));
			
			if($query){
				return $query;
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//6. Hapus data dari table 'absensidata' yang sudah lewat 2 hari
	function deleteTempFromAbsensidata2harisebelum($tanggal2hari){
		try{
			$sql = "DELETE FROM absensidata 
			WHERE date = ?";

			$query = $this->db->query($sql, array($tanggal2hari));
			
			if($query){
				return $query;
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//7. Hapus data dari table 'absensidata'
	function deleteTempFromAbsensidata(){
		try{
			$sql = "DELETE FROM absensidata ";

			$query = $this->db->query($sql);
			
			if($query){
				return $query;
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//8. Ambil semua data NPK, Nama, dan ID Absensi untuk dilooping nantinya
	function getArrayNPKNamaIDAbsensi()
	{
		try{
			$sql = "SELECT NPK, UserIDAbsensi, Nama FROM mstruser
			WHERE UserIDAbsensi is not null
			AND Deleted = 0
			ORDER BY UserIDAbsensi ASC";

			$query = $this->db->query($sql);
			
			if($query){
				return $query->result();
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//9. Cek apakah sudah ada data dari NPK tertentu pada table absensiauto atau absensi
	function checkDataNpkOnAbsensiToday($NPK,$tanggal)
	{
		try{
			$sql = "SELECT * FROM absensi WHERE Tanggal = ? AND Deleted = 0 AND NPK = ?";

			$query = $this->db->query($sql, array($tanggal,$NPK));
			
			if($query){
				return $query->result();
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//10. Insert Select data ketika belum ada record untuk NPK tersebut di hari itu
	function insertSelectToAbsensi($npk, $tanggal){
		try{
			$sql = "INSERT INTO absensi (NPK, Tanggal, Hari, WaktuMasuk, WaktuKeluar, CreatedOn, CreatedBy)
			SELECT 
			npk, `date`, `dayname`,
			(SELECT `time` FROM absensidata WHERE npk = ? AND date = ? ORDER BY timeformat ASC limit 1) AS WaktuMasuk,
			(SELECT `time` FROM absensidata WHERE npk = ? AND date = ? ORDER BY timeformat DESC limit 1) AS WaktuKeluar,
			NOW(),
			'System'
			FROM absensidata
			WHERE npk = ? AND date = ?
			ORDER BY pin asc limit 1";

			$query = $this->db->query($sql, array($npk,$tanggal,$npk,$tanggal,$npk,$tanggal));
			
			if($query){
				return $query;
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//11. Update data waktu keluar dari suatu NPK pada hari tersebut
	function updateWaktuKeluar($KodeAbsensi, $npk, $tanggal){
		try{
			$sql = "UPDATE absensi
			set WaktuKeluar = (SELECT `time` FROM absensidata WHERE npk = ? AND date = ? ORDER BY timeformat DESC limit 1),
			UpdatedOn = NOW(),
			UpdatedBy = 'SYSTEM'
			WHERE KodeAbsensi = ? AND NPK = ? AND Tanggal = ?";

			$query = $this->db->query($sql, array($npk,$tanggal,$KodeAbsensi,$npk,$tanggal));
			
			if($query){
				return $query;
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//12. Cek apakah sudah ada Waktu Masuk?
	function checkDataWaktuMasuk($KodeAbsensi)
	{
		try{
			$sql = "SELECT * FROM absensi WHERE KodeAbsensi = ?";

			$query = $this->db->query($sql, array($KodeAbsensi));
			
			if($query){
				return $query->result();
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//13. Update data waktu masuk dan waktu keluar dari suatu NPK pada hari tersebut
	function updateWaktuMasukWaktuKeluar($KodeAbsensi, $npk, $tanggal){
		try{
			$sql = "UPDATE absensi
			SET 
			WaktuKeluar = (SELECT `time` FROM absensidata WHERE npk = ? AND date = ? ORDER BY timeformat DESC limit 1),
			WaktuMasuk = (SELECT `time` FROM absensidata WHERE npk = ? AND date = ? ORDER BY timeformat ASC limit 1),
			UpdatedOn = NOW(),
			UpdatedBy = 'SYSTEM'
			WHERE KodeAbsensi = ? AND NPK = ? AND Tanggal = ?";

			$query = $this->db->query($sql, array($npk,$tanggal,$npk,$tanggal,$KodeAbsensi,$npk,$tanggal));
			
			if($query){
				return $query;
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}

	//8. Insert Select ke absensi_masuk
	function insertSelectToAbsensiMasuk(){
		try{
			$sql = "DELETE FROM absensidata ";

			$query = $this->db->query($sql);
			
			if($query){
				return $query;
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}



	// ---- END - FITUR ABSENSI OTOMATIS ---- //
	
}
?>