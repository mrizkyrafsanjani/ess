<?php 
Class Rebalancing_model extends Ci_Model
{
	
	
	function getGlobalParam($parameter)
	{
		$sql = "select * from globalparam where Name=?";
		$query = $this->db->query($sql,$parameter);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->Value;
			}
		}
		else
		{
			return "0";
		}
	}
	

	function getIndexIDX($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
        $sql = "select IDX_INDX as index_ from (select top 1 IDX_INDX from IndexIDX where year(idx_date)=year(?) and IDX_CODE='COMPOSITE' and DATEDIFF(DAY,?,idx_date)<=0 order by idx_date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->index_;
			}
		}
		else
		{
			return "0";
		}
	}

	function getIndexIDXLastMonth($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
		$sql = "select IDX_INDX as index_ from (select top 1 IDX_INDX 
												from IndexIDX where year(idx_date)=year(?) 
													 and month(idx_date)=month(DATEADD(month,-1,?)) and IDX_CODE='COMPOSITE' 
													 order by idx_date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->index_;
			}
		}
		else
		{
			return "0";
		}
	}

	function getIndexIDXLastYear($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
        $sql = "select IDX_INDX as index_ from (select top 1 IDX_INDX from IndexIDX where year(idx_date)=year(DATEADD(year,-1,?)) and IDX_CODE='COMPOSITE' order by idx_date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->index_;
			}
		}
		else
		{
			return "0";
		}
	}

	function getIndexLQ45($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
        $sql = "select IDX_INDX as LQ45_ from (select top 1 IDX_INDX from IndexIDX where year(idx_date)=year(?) and IDX_CODE='LQ45' and DATEDIFF(DAY,?,idx_date)<=0 order by idx_date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->LQ45_;
			}
		}
		else
		{
			return "0";
		}
	}

	function getIndexLQ45LastMonth($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
		$sql = "select IDX_INDX as LQ45_ from (select top 1 IDX_INDX 
												from IndexIDX where year(idx_date)=year(?) 
													 and month(idx_date)=month(DATEADD(month,-1,?)) and IDX_CODE='LQ45' 
													 order by idx_date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->LQ45_;
			}
		}
		else
		{
			return "0";
		}
	}

	function getIndexLQ45LastYear($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
        $sql = "select IDX_INDX as LQ45_ from (select top 1 IDX_INDX from IndexIDX where year(idx_date)=year(DATEADD(year,-1,?)) and IDX_CODE='LQ45' order by idx_date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->LQ45_;
			}
		}
		else
		{
			return "0";
		}
	}

	function getYield10($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
        $sql = "select today as yield10 from (select top 1 TODAY from yieldbytenor where year(date)=year(?) and TENORYEAR=10 and DATEDIFF(DAY,?,date)<=0 order by date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->yield10;
			}
		}
		else
		{
			return "0";
		}
	}

	function getYield10LastMonth($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
		$sql = "select today as yield10 from (select top 1 TODAY 
												from yieldbytenor where year(date)=year(?) 
													 and month(date)=month(DATEADD(month,-1,?)) and TENORYEAR='10' 
													 order by date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->yield10;
			}
		}
		else
		{
			return "0";
		}
	}

	function getYield10LastYear($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
        $sql = "select today as yield10 from (select top 1 TODAY from yieldbytenor where year(date)=year(DATEADD(year,-1,?)) and TENORYEAR='10' order by date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->LQ45_;
			}
		}
		else
		{
			return "0";
		}
	}

	function getYield3($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
        $sql = "select today as yield3 from (select top 1 TODAY from yieldbytenor where year(date)=year(?) and TENORYEAR=3 and DATEDIFF(DAY,?,date)<=0 order by date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->yield3;
			}
		}
		else
		{
			return "0";
		}
	}

	function getYield3LastMonth($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
		$sql = "select today as yield3 from (select top 1 TODAY 
												from yieldbytenor where year(date)=year(?) 
													 and month(date)=month(DATEADD(month,-1,?)) and TENORYEAR='3' 
													 order by date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->yield3;
			}
		}
		else
		{
			return "0";
		}
	}

	function getYield3LastYear($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
        $sql = "select today as yield3 from (select top 1 TODAY from yieldbytenor where year(date)=year(DATEADD(year,-1,?)) and TENORYEAR='3' order by date desc ) a";
		$query = $this->otherdb->query($sql,array($TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->yield3;
			}
		}
		else
		{
			return "0";
		}
	}

	function getRateBI($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
		$sql = "select rate as rateBI from (select top 1 rate from BI7DayRR where year(tanggal)=year(?) and DATEDIFF(DAY,?,tanggal)<=0 order by tanggal desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->rateBI;
			}
		}
		else
		{
			return "0";
		}
	}

	function getRateBILastMonth($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
		$sql = "select rate as rateBI from (select top 1 rate 
												from BI7DayRR where year(tanggal)=year(?) 
													 and month(tanggal)=month(DATEADD(month,-1,?))  
													 order by tanggal desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->rateBI;
			}
		}
		else
		{
			return "0";
		}
	}

	function getRateBILastYear($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
        $sql = "select rate as rateBI from (select top 1 rate from BI7DayRR where year(tanggal)=year(DATEADD(year,-1,?))  order by tanggal desc ) a";
		$query = $this->otherdb->query($sql,array($TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->rateBI;
			}
		}
		else
		{
			return "0";
		}
	}

	function getIDR($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
		$sql = "select kurs  from (select top 1 kurs from KURSUSDIDR where year(tanggal)=year(?) and DATEDIFF(DAY,?,tanggal)<=0 order by tanggal desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->kurs;
			}
		}
		else
		{
			return "0";
		}
	}

	function getIDRLastMonth($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
		$sql = "select kurs  from (select top 1 kurs 
												from KURSUSDIDR where year(tanggal)=year(?) 
													 and month(tanggal)=month(DATEADD(month,-1,?))  
													 order by tanggal desc ) a";
		$query = $this->otherdb->query($sql,array($TglData,$TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->kurs;
			}
		}
		else
		{
			return "0";
		}
	}

	function getIDRLastYear($TglData)
	{
        $this->otherdb = $this->load->database('otherdb', TRUE);        
        
        $sql = "select kurs from (select top 1 kurs from KURSUSDIDR where year(tanggal)=year(DATEADD(year,-1,?))  order by tanggal desc ) a";
		$query = $this->otherdb->query($sql,array($TglData));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->kurs;
			}
		}
		else
		{
			return "0";
		}
	}

	function getDataAset($TglData,$DPA)
	{
		try
		{
			if ($DPA==1)
				$sql = "exec sp_get_all_AsetDPA1test ?";	
			else
				$sql = "exec sp_get_all_AsetDPA2 ?";			
			
			$query = $this->otherdb->query($sql,array($TglData));
			
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}

	public function tampil(){
		// menjalankan stored procedure tampil_penerbit()
		$sql_query=$this->otherdb->query("exec sp_get_all_AsetDPA1test '01/01/2019'");  	  				
	    
            if($sql_query->num_rows()>0){
                return $sql_query->result_array();
            }
	}

	public function insertInvestmentPlan($data,$npkLogin){
		try{
			$this->otherdb = $this->load->database('otherdb', TRUE);
			foreach($data as $row){
				$sql = "INSERT INTO tblTransRB (tanggal,grup,jenistipe,remarkuser,nominal,DPA,isAstra,createdBy,createdOn,dealtypecode) 
				VALUES(?,'PLAN',?,?,?,?,?,?,getdate(),?)";
				if($row["tanggal"] != "" && $row["nominal"] != ""){
					$query = $this->otherdb->query($sql,array($row["tanggal"],$row["tipe"],$row["remark"],$row["nominal"],$row["dpa"],$row["isastra"],$npkLogin,$row["jenis"]));
				}
			}
			return $query;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}


	

	
}
?>