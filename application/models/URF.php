<?php 
Class URF extends Ci_Model
{
	function getAll($npk){
		$param = array();
		array_push($param, $npk);
		$sql = "SELECT * FROM MstrURF where NpkUser = ?";
		$query = $this->db->query($sql,$param);
		return $query->result();
	}

	function getURF_detail($URF_Number){
		$sql = "SELECT m.URF_ID,
				m.URF_Number,
				mu.Nama,
				d.Deskripsi,
				m.ExpiredPeriod
				FROM mstrurf m
				join dtlurf d on m.URF_Number = d.URF_Number
				join mstruser mu on m.CreatedBy = mu.NPK
				where d.Deleted = 0
				and m.URF_Number = '$URF_Number'";
		$query = $this->db->query($sql);
		return $query->result();
	}
}
?>