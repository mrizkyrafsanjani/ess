<?php 
Class UserPelimpahanWewenang extends Ci_Model
{
	function getNPKUser()
	{	
		
		$sql = "select NPK, Nama from mstruser where Deleted=0 order by Nama";
		$query = $this->db->query($sql);
		
		//$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function findUserByNPK($npk){
		try
		{
			$sql = "SELECT * FROM mstruser where Deleted = 0 AND NPK = ?";
			$query = $this->db->query($sql, array($npk));
			
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
		}
		catch (Exception $e) 
		{
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

	}

	

	
}
?>