<?php 
Class UangMuka_model extends Ci_Model
{
	
	
	function getOutstandingUMLain($NPK){
		$sql = "select t.NoUangMuka, t.KeteranganPermohonan, format(sum(TotalPermohonan),0) as Total,
				t.ReasonOutStanding
				from trxuangmuka t 
				join dtluangmuka d on d.NoUangmuka=t.NoUangMuka
				where t.deleted = 0 and d.deleted = 0 and t.Status='O' and t.NPKPemohon = ?
				group by t.NoUangMuka, t.KeteranganPermohonan,t.ReasonOutStanding ";
		$query = $this->db->query($sql, $NPK);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getOutstandingUM($NPK){
		$sql = "select t.NoUangMuka, t.KeteranganPermohonan, sum(TotalPermohonan) as Total, f.TotalSPD as TotalSPD,
				t.WaktuPenyelesaianTerlambat,t.TglTerimaHRGARealisasi,
				t.ReasonOutStanding
				from trxuangmuka t 
				left join dtluangmuka d on d.NoUangmuka=t.NoUangMuka
				left join (select k.NoUangMuka, k.KeteranganPermohonan, p.TotalSPD as TotalSPD,
							WaktuPenyelesaianTerlambat,k.TglTerimaHRGARealisasi,
							k.ReasonOutStanding
							from trxuangmuka k 
							left join trkspd p on p.NoSPD = k.NoSPD
							where k.deleted = 0 and p.deleted = 0 and datediff( k.WaktuPenyelesaianTerlambat, now()) < 0 and TanggalRealisasi is null
							and k.CreatedBy = '$NPK'
							group by k.NoUangMuka, k.KeteranganPermohonan,k.ReasonOutStanding)f on t.NoUangMuka = f.NoUangMuka
				where t.deleted = 0 and datediff( t.WaktuPenyelesaianTerlambat, now()) < 0 and TanggalRealisasi is null
				and t.CreatedBy = '$NPK'
				group by t.NoUangMuka, t.KeteranganPermohonan,t.ReasonOutStanding";
		$query = $this->db->query($sql);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getOutstandingUMSPD($NPK){
		$sql = "select t.NoUangMuka, t.KeteranganPermohonan, TotalSPD,
		WaktuPenyelesaianTerlambat,TglTerimaHRGARealisasi,
				t.ReasonOutStanding
				from trxuangmuka t 
				join trkspd d on d.NoSPD=t.NoSPD
				where t.deleted = 0 and d.deleted = 0 and datediff( WaktuPenyelesaianTerlambat, now()) < 0 and TanggalRealisasi is null
		and t.CreatedBy = ?
				group by t.NoUangMuka, t.KeteranganPermohonan,t.ReasonOutStanding";
		$query = $this->db->query($sql, $NPK);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	

	function getAtasan($NPK)
	{
		$sql = "select NPKAtasan from mstruser where NPK=?";
		$query = $this->db->query($sql, $NPK);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->NPKAtasan;
			}
		}
		else
		{
			return "0";
		}
	}

	function getNPKDIC($NPKAtasan)
	{
		$sql = "select NPKAtasan from mstruser where NPK= ?";
		$query = $this->db->query($sql, $NPKAtasan);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->NPKAtasan;
			}
		}
		else
		{
			return "0";
		}

	}

	function getNamaDIC($NPKDIC)
	{
		$sql = "select Nama from mstruser where NPK = ?";
		$query = $this->db->query($sql, $NPKDIC);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $query->result();
			}
		}
		else
		{
			return "0";
		}
	}

	function getDataPP()
	{
		
			$sql = "select concat(NoPP ,' - ' , Keterangan) as display,NoPP  
					from persetujuanpengeluaranbudget 
					where Deleted=0 order by NoPP";
		
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
	}

	function getJenisKegiatanUM()
	{
		
			$sql = "select *   
					from mstrjnskegiatanum 
					 order by IdKegiatan";
		
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
	}

	
	function getJenisKegiatanUMBaseID($IdKegiatan)
	{
		
			$sql = "select *   
					from mstrjnskegiatanum where IdKegiatan=?";
		
			$query = $this->db->query($sql,$IdKegiatan);
			if($query->num_rows() > 0){
				return $query->result();
			}else{
				return false;
			}
	}

	function getLastNoUangMuka()
	{
		$sql = "SELECT NoUangMuka FROM trxuangmuka ORDER BY CreatedOn DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->NoUangMuka;
			}
		}
		else
		{
			return "0";
		}
	}

	function getInitDept($NPK)
	{
		$sql = "SELECT INIT 
			   FROM departemen d join mstruser m
			   on d.KodeDepartemen=m.Departemen
			   where m.NPK= ? ";
		$query = $this->db->query($sql,$NPK);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->INIT;
			}
		}
		else
		{
			return "0";
		}
	}
	
	function getTotalDetilRealisasi($ID)
	{
		$sql = "select sum(hargarealisasi) as total from trxuangmuka h join dtluangmuka d on h.NoUangMuka = d.NoUangMuka
		where id = ? and h.deleted = 0 and d.deleted = 0";
		$query = $this->db->query($sql,array($ID));
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->total;
			}
		}
		else
		{
			return "0";
		}
	}

	function getNoUangMukaByID($ID)
	{
		$sql = "SELECT NoUangMuka FROM trxuangmuka where id=".$ID;
		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->NoUangMuka;
			}
		}
		else
		{
			return "0";
		}
	}

	function getNoUangMukaByNoSPD($NoSPD)
	{
		$sql = "SELECT NoUangMuka FROM trxuangmuka where NoSPD='".$NoSPD."'";
		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->NoUangMuka;
			}
		}
		else
		{
			return "0";
		}
	}
	
		
	function insertUangMuka($noUangMuka,$no='')
	{	
		$session_data = $this->session->userdata('logged_in');
		$NPKLogin = $session_data['npk'];
		$tanggalmulai = $this->input->post('txtTanggalMulai');
		$tanggalselesai = $this->input->post('txtTanggalSelesai');
		$keterangan = $this->input->post('txtKeterangan');
		$DPA = $this->input->post('rbDPA');
		$nopp = $this->input->post('txtDisplayPP');
		$jeniskegiatan = $this->input->post('txtKegiatan');
		$outstanding = $this->input->post('txtOutstanding');
		$jenisbauarum = $this->input->post('txtPilihBayar');
		$waktubayartercepat = $this->input->post('txtWaktuBayarCepat');
		$waktuselesaitercepat = $this->input->post('txtWaktuSelesaiUM');
		$bank = $this->input->post('txtBank');
		$norek = $this->input->post('txtNoRek');
		$namapenerima = $this->input->post('txtPenerima');
		$NPKPemohon = $this->input->post('txtNPK');

		if ($no<>'' && substr($no,0,3)=='SPD')
		{		$sql = "INSERT INTO trxuangmuka (NoUangMuka, DPA,Deleted,TanggalPermohonan,NoPP,KeteranganPermohonan,
						Status,TipeBayarUangMuka,BankBayarUangMuka,NoRekBayarUangMuka,
						NmPenerimaBayarUangMuka,CreatedOn,CreatedBy,JenisKegiatanUangMuka,TanggalMulai,
						TanggalSelesai,NPKPemohon,WaktuBayarTercepat,WaktuPenyelesaianTerlambat,NoSPD) 
						VALUES (?,?,0,now(),?,?,'P',?,?,?,?,now(),?,?,?,?,?,?,?,?)";
				$this->db->query($sql,array($noUangMuka,$DPA,$nopp,$keterangan,$jenisbauarum,$bank,$norek,$namapenerima,$NPKLogin,$jeniskegiatan,$tanggalmulai,$tanggalselesai,$NPKPemohon,$waktubayartercepat,$waktuselesaitercepat,$no));
		} else if ($no<>'' && substr($no,0,2)=='AT')
		{
				$sql = "INSERT INTO trxuangmuka (NoUangMuka, DPA,Deleted,TanggalPermohonan,NoPP,KeteranganPermohonan,
										Status,TipeBayarUangMuka,BankBayarUangMuka,NoRekBayarUangMuka,
										NmPenerimaBayarUangMuka,CreatedOn,CreatedBy,JenisKegiatanUangMuka,TanggalMulai,
										TanggalSelesai,NPKPemohon,WaktuBayarTercepat,WaktuPenyelesaianTerlambat,NoAkomodasiTiket,LastUpdateBy,LastUpdateOn) 
						VALUES (?,?,0,now(),?,?,'P',?,?,?,?,now(),?,?,?,?,?,?,?,?,?,now())";

				$this->db->query($sql,array($noUangMuka,$DPA,$nopp,$keterangan,$jenisbauarum,$bank,$norek,$namapenerima,$NPKLogin,$jeniskegiatan,$tanggalmulai,$tanggalselesai,$NPKPemohon,$waktubayartercepat,$waktuselesaitercepat,$no,$session_data['npk']));
		}
		else
		{
			$sql = "INSERT INTO trxuangmuka (NoUangMuka, DPA,Deleted,TanggalPermohonan,NoPP,KeteranganPermohonan,
								Status,TipeBayarUangMuka,BankBayarUangMuka,NoRekBayarUangMuka,
								NmPenerimaBayarUangMuka,CreatedOn,CreatedBy,JenisKegiatanUangMuka,TanggalMulai,
								TanggalSelesai,NPKPemohon,WaktuBayarTercepat,WaktuPenyelesaianTerlambat) 
					VALUES (?,?,0,now(),?,?,'P',?,?,?,?,now(),?,?,?,?,?,?,?)";
			$this->db->query($sql,array($noUangMuka,$DPA,$nopp,$keterangan,$jenisbauarum,$bank,$norek,$namapenerima,$NPKLogin,$jeniskegiatan,$tanggalmulai,$tanggalselesai,$NPKPemohon,$waktubayartercepat,$waktuselesaitercepat));
		}

		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	
	
	function InsertDtlUangMuka($noUangMuka){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$sqlupdate = "UPDATE dtluangmuka SET NoUangMuka =?,UpdatedOn = now(),  UpdatedBy=?,
		KeteranganRealisasi=KeteranganPermohonan,JumlahRealisasi=JumlahPermohonan,HargaRealisasi=HargaPermohonan,JenisBiayaRealisasi=JenisBiayaPermohonan,
		TotalRealisasi=TotalPermohonan WHERE CreatedBy = ? and Deleted=0 and NoUangMuka='-'";

		$this->db->query($sqlupdate,array($noUangMuka,$NPKlogin,$NPKlogin));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}

	function EditDtlUangMuka($noUangMuka){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$sqlupdate = "UPDATE dtluangmuka SET NoUangMuka =?,UpdatedOn = now(),  UpdatedBy=?,
		KeteranganRealisasi=KeteranganPermohonan,JumlahRealisasi=JumlahPermohonan,HargaRealisasi=HargaPermohonan,JenisBiayaRealisasi=JenisBiayaPermohonan,
		TotalRealisasi=TotalPermohonan WHERE CreatedBy = ? and Deleted=0 and NoUangMuka='-'";

		$this->db->query($sqlupdate,array($noUangMuka,$NPKlogin,$NPKlogin));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}

	function getHeaderTrxUangMukabyNoUM($noUangMuka){
		$sql = "select t.DPA,t.NoUangMuka,DATE_FORMAT(t.TanggalPermohonan,'%d %M %Y') as TanggalUangMuka,DATE_FORMAT(t.TanggalMulai,'%d %M %Y') as TanggalMulai,
				DATE_FORMAT(t.TanggalSelesai,'%d %M %Y') as TanggalSelesai,TipeBayarUangMuka,BankBayarUangMuka,NoRekBayarUangMuka,NmPenerimaBayarUangMuka,
				DATE_FORMAT(t.WaktuBayarTercepat,'%d %M %Y') as WaktuBayarTercepat,DATE_FORMAT(t.WaktuPenyelesaianTerlambat,'%d %M %Y') as WaktuPenyelesaianTerlambat,
				t.NPKPemohon,u.nama, u2.nama as atasan, NoPP,t.KeteranganPermohonan, t.NoAkomodasiTiket, sum(ifnull(TotalPermohonan,0)) as TotalPermohonan
				from trxuangmuka t
					left join mstruser u on t.CreatedBy = u.npk
					left join mstruser u2 on t.NPKPemohon = u2.npk and u2.deleted = 0
					left join dtluangmuka dtl on dtl.NoUangMuka=t.NoUangMuka and dtl.deleted=0
				where t.deleted = 0  and t.NoUangMuka = ?";
		$query = $this->db->query($sql, $noUangMuka);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getDetailTrxUangMukabyNoUM($noUangMuka){
		$sql = "select iddtluangmuka,JenisBiayaPermohonan,KeteranganPermohonan,
				JumlahPermohonan,
				HargaPermohonan,TotalPermohonan,
				format(HargaPermohonan,0) as fHargaPermohonan,
				format(TotalPermohonan,0) as fTotalPermohonan
				from dtluangmuka d					
				where d.deleted = 0 and d.NoUangMuka = ?
				order by JenisBiayaPermohonan";
		$query = $this->db->query($sql, $noUangMuka);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getHeaderTrxUangMukabyNoUMEdit($ID){
		$sql = "SELECT NoUangMuka, t.DPA,  DATE_FORMAT(TanggalPermohonan, '%Y-%m-%d') TanggalPermohonan,
					   NoPP, KeteranganPermohonan, Status, TipeBayarUangMuka, BankBayarUangMuka, NoRekBayarUangMuka, 
					   NmPenerimaBayarUangMuka, AlasanOutstanding, NoSPD, NoAkomodasiTiket, 
					   DATE_FORMAT(TglTerimaHRGAPermohonan, '%Y-%m-%d') TglTerimaHRGAPermohonan,DATE_FORMAT(TglTerimaFinancePermohonan, '%Y-%m-%d')   TglTerimaFinancePermohonan, 
					   DATE_FORMAT(TglBayarFinancePermohonan, '%Y-%m-%d') TglBayarFinancePermohonan, DATE_FORMAT(TglTerimaHRGARealisasi, '%Y-%m-%d') TglTerimaHRGARealisasi,
					   DATE_FORMAT(TglTerimaFinanceRealisasi, '%Y-%m-%d') TglTerimaFinanceRealisasi,  DATE_FORMAT(TglBayarFinanceRealisasi, '%Y-%m-%d') TglBayarFinanceRealisasi, 
					   NoBEPermohonan, NoBERealisasi, TipeKembaliUangMuka, BankKembaliUangMuka, 
					   NoRekKembaliUangMuka, NmPenerimaUangMuka, LebihKurangRealisasi, 
					   t.CreatedBy, t.CreatedOn, 
					   JenisKegiatanUangMuka, NPKPemohon, ReasonOutstanding, DATE_FORMAT(TanggalMulai, '%Y-%m-%d') TanggalMulai, 
					   DATE_FORMAT(TanggalSelesai, '%Y-%m-%d') TanggalSelesai, DATE_FORMAT(WaktuBayarTercepat, '%Y-%m-%d') WaktuBayarTercepat, WaktuPenyelesaianTerlambat, 
					   DATE_FORMAT(TanggalRealisasi, '%Y-%m-%d') TanggalRealisasi, FilePathPermohonan, FilePathRealisasi, id ,
					   u.nama, u2.nama as atasan, mj.JenisKegiatan
				FROM trxuangmuka t
					 left join mstruser u on t.CreatedBy = u.npk
					 left join mstruser u2 on t.NPKPemohon = u2.npk and u2.deleted = 0
					 left join mstrjnskegiatanum mj on t.JenisKegiatanUangMuka=mj.IdKegiatan
				where t.deleted = 0  and t.id = ?
				";
		$query = $this->db->query($sql, $ID);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getHeaderTrxUangMukabyNoSPDEdit($NoSPD){
		$sql = "SELECT NoUangMuka, t.DPA,  DATE_FORMAT(TanggalPermohonan, '%Y-%m-%d') TanggalPermohonan,
					   NoPP, KeteranganPermohonan, Status, TipeBayarUangMuka, BankBayarUangMuka, NoRekBayarUangMuka, 
					   NmPenerimaBayarUangMuka, AlasanOutstanding, NoSPD, NoAkomodasiTiket, 
					   DATE_FORMAT(TglTerimaHRGAPermohonan, '%Y-%m-%d') TglTerimaHRGAPermohonan,DATE_FORMAT(TglTerimaFinancePermohonan, '%Y-%m-%d')   TglTerimaFinancePermohonan, 
					   DATE_FORMAT(TglBayarFinancePermohonan, '%Y-%m-%d') TglBayarFinancePermohonan, DATE_FORMAT(TglTerimaHRGARealisasi, '%Y-%m-%d') TglTerimaHRGARealisasi,
					   DATE_FORMAT(TglTerimaFinanceRealisasi, '%Y-%m-%d') TglTerimaFinanceRealisasi,  DATE_FORMAT(TglBayarFinanceRealisasi, '%Y-%m-%d') TglBayarFinanceRealisasi, 
					   NoBEPermohonan, NoBERealisasi, TipeKembaliUangMuka, BankKembaliUangMuka, 
					   NoRekKembaliUangMuka, NmPenerimaUangMuka, LebihKurangRealisasi, 
					   t.CreatedBy, t.CreatedOn, 
					   JenisKegiatanUangMuka, NPKPemohon, ReasonOutstanding, DATE_FORMAT(TanggalMulai, '%Y-%m-%d') TanggalMulai, 
					   DATE_FORMAT(TanggalSelesai, '%Y-%m-%d') TanggalSelesai, DATE_FORMAT(WaktuBayarTercepat, '%Y-%m-%d') WaktuBayarTercepat, WaktuPenyelesaianTerlambat, 
					   DATE_FORMAT(TanggalRealisasi, '%Y-%m-%d') TanggalRealisasi, FilePathPermohonan, FilePathRealisasi, id ,
					   u.nama, u2.nama as atasan, mj.JenisKegiatan
				FROM trxuangmuka t
					 left join mstruser u on t.CreatedBy = u.npk
					 left join mstruser u2 on t.NPKPemohon = u2.npk and u2.deleted = 0
					 left join mstrjnskegiatanum mj on t.JenisKegiatanUangMuka=mj.IdKegiatan
				where t.deleted = 0  and t.NoSPD = ?
				";
		$query = $this->db->query($sql, $NoSPD);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}


	function updateUMLain($noUM){
		$session_data = $this->session->userdata('logged_in');
		$NPKLogin = $this->input->post('txtNPKLogin');
		$tanggalmulai = $this->input->post('txtTanggalMulai');
		$tanggalselesai = $this->input->post('txtTanggalSelesai');
		$keterangan = $this->input->post('txtKeterangan');
		$DPA = $this->input->post('rbDPA');
		$nopp = $this->input->post('txtDisplayPP');
		$jeniskegiatan = $this->input->post('txtKegiatan');
		$outstanding = $this->input->post('txtOutstanding');
		$jenisbauarum = $this->input->post('txtPilihBayar');
		$waktubayartercepat = $this->input->post('txtWaktuBayarCepat');
		$waktuselesaitercepat = $this->input->post('txtWaktuSelesaiUM');
		$bank = $this->input->post('txtBank');
		$norek = $this->input->post('txtNoRek');
		$namapenerima = $this->input->post('txtPenerima');
		$NPKPemohon = $this->input->post('txtNPK');
		

		$sql = "UPDATE trxuangmuka SET 
					   TanggalMulai= ?,	TanggalSelesai= ?,WaktuBayarTercepat= ?,WaktuPenyelesaianTerlambat= ?,
					   KeteranganPermohonan = ?, NoPP = ?, JenisKegiatanUangMuka = ?, TipeBayarUangMuka = ?, BankBayarUangMuka = ?, 
					   NoRekBayarUangMuka = ?, NmPenerimaBayarUangMuka =?,LastUpdateOn=now(), LastUpdateBy=?
				WHERE NoUangmuka = ?";
		
		$this->db->query($sql,array($tanggalmulai,$tanggalselesai,$waktubayartercepat, $waktuselesaitercepat, $keterangan,
						$nopp,$jeniskegiatan,$jenisbauarum,$bank,$norek,$namapenerima,$NPKLogin,$noUM));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function RealisasiUM($noUM){
		$session_data = $this->session->userdata('logged_in');
		$NPKLogin = $this->input->post('txtNPKLogin');
		$jenisbauarum = $this->input->post('txtPilihBayar');
		$waktupenyelesaianterlambat = $this->input->post('txtWaktuPenyelesaianTerlambat');
		$bank = $this->input->post('txtBank');
		$norek = $this->input->post('txtNoRek');
		$namapenerima = $this->input->post('txtPenerima');
		$NPKPemohon = $this->input->post('txtNPK');
		
		//$TotalRealisasi = this->input->post('txtNPK');

		$sql = "UPDATE trxuangmuka SET 
					   TipeKembaliUangMuka = ?, BankKembaliUangMuka = ?, 
					   NoRekKembaliUangMuka = ?, NmPenerimaUangMuka =?,LastUpdateOn=now(), LastUpdateBy=?,TanggalRealisasi=now(),
					   Status='R'
				WHERE NoUangmuka = ?";
		
		$this->db->query($sql,array($jenisbauarum,$bank,$norek,$namapenerima,$NPKLogin,$noUM));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function InsertDtlUangMukaRealisasi($noUangMuka){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$sqlupdate = "UPDATE dtluangmuka SET NoUangMuka =?,UpdatedOn = now(),  UpdatedBy=?,
		WHERE CreatedBy = ? and Deleted=0 and NoUangMuka='-'";

		$this->db->query($sqlupdate,array($noUangMuka,$NPKlogin,$NPKlogin));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}

	function getHeaderTrxUangMukabyNoUMRealisasi($ID){
		$sql = "SELECT t.NoUangMuka, t.DPA,  DATE_FORMAT(TanggalPermohonan, '%Y-%m-%d') TanggalPermohonan,
					   NoPP, t.KeteranganPermohonan, Status, TipeBayarUangMuka, BankBayarUangMuka, NoRekBayarUangMuka, 
					   NmPenerimaBayarUangMuka, AlasanOutstanding, NoSPD, NoAkomodasiTiket, 
					   DATE_FORMAT(TglTerimaHRGAPermohonan, '%Y-%m-%d') TglTerimaHRGAPermohonan,DATE_FORMAT(TglTerimaFinancePermohonan, '%Y-%m-%d')   TglTerimaFinancePermohonan, 
					   DATE_FORMAT(TglBayarFinancePermohonan, '%Y-%m-%d') TglBayarFinancePermohonan, DATE_FORMAT(TglTerimaHRGARealisasi, '%Y-%m-%d') TglTerimaHRGARealisasi,
					   DATE_FORMAT(TglTerimaFinanceRealisasi, '%Y-%m-%d') TglTerimaFinanceRealisasi,  DATE_FORMAT(TglBayarFinanceRealisasi, '%Y-%m-%d') TglBayarFinanceRealisasi, 
					   NoBEPermohonan, NoBERealisasi, TipeKembaliUangMuka, BankKembaliUangMuka, 
					   NoRekKembaliUangMuka, NmPenerimaUangMuka, LebihKurangRealisasi, 
					   t.CreatedBy, t.CreatedOn, 
					   JenisKegiatanUangMuka, NPKPemohon, ReasonOutstanding, DATE_FORMAT(TanggalMulai, '%Y-%m-%d') TanggalMulai, 
					   DATE_FORMAT(TanggalSelesai, '%Y-%m-%d') TanggalSelesai, WaktuBayarTercepat, WaktuPenyelesaianTerlambat, 
					   DATE_FORMAT(TanggalRealisasi, '%Y-%m-%d') TanggalRealisasi, FilePathPermohonan, FilePathRealisasi, id ,
					   u.nama, u2.nama as atasan, mj.JenisKegiatan, sum(ifnull(TotalPermohonan,0)) as TotalPermohonan, sum(ifnull(TotalRealisasi,0)) as TotalRealisasi,
					   sum(ifnull(TotalPermohonan,0))-sum(ifnull(TotalRealisasi,0)) as Selisih
				FROM trxuangmuka t
					 left join mstruser u on t.CreatedBy = u.npk
					 left join mstruser u2 on t.NPKPemohon = u2.npk and u2.deleted = 0
					 left join mstrjnskegiatanum mj on t.JenisKegiatanUangMuka=mj.IdKegiatan
					 left join dtluangmuka dtl on dtl.NoUangMuka=t.NoUangMuka and dtl.deleted=0
				where t.deleted = 0  and t.id = ? 
				group by t.NoUangMuka, t.DPA,  DATE_FORMAT(TanggalPermohonan, '%Y-%m-%d') ,
					   NoPP, t.KeteranganPermohonan, Status, TipeBayarUangMuka, BankBayarUangMuka, NoRekBayarUangMuka, 
					   NmPenerimaBayarUangMuka, AlasanOutstanding, NoSPD, NoAkomodasiTiket, 
					   DATE_FORMAT(TglTerimaHRGAPermohonan, '%Y-%m-%d') ,DATE_FORMAT(TglTerimaFinancePermohonan, '%Y-%m-%d')   , 
					   DATE_FORMAT(TglBayarFinancePermohonan, '%Y-%m-%d') , DATE_FORMAT(TglTerimaHRGARealisasi, '%Y-%m-%d') ,
					   DATE_FORMAT(TglTerimaFinanceRealisasi, '%Y-%m-%d') ,  DATE_FORMAT(TglBayarFinanceRealisasi, '%Y-%m-%d') , 
					   NoBEPermohonan, NoBERealisasi, TipeKembaliUangMuka, BankKembaliUangMuka, 
					   NoRekKembaliUangMuka, NmPenerimaUangMuka, LebihKurangRealisasi, 
					   t.CreatedBy, t.CreatedOn, 
					   JenisKegiatanUangMuka, NPKPemohon, ReasonOutstanding, DATE_FORMAT(TanggalMulai, '%Y-%m-%d') , 
					   DATE_FORMAT(TanggalSelesai, '%Y-%m-%d') , WaktuBayarTercepat, WaktuPenyelesaianTerlambat, 
					   DATE_FORMAT(TanggalRealisasi, '%Y-%m-%d') , FilePathPermohonan, FilePathRealisasi, id ,
					   u.nama, u2.nama , mj.JenisKegiatan
				";
		$query = $this->db->query($sql, $ID);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getTotalRealisasi($ID)
	{
		$sql = "select SUM(IFNULL(TotalRealisasi,0)) as TotalRealisasi,SUM(IFNULL(TotalPermohonan,0)) as TotalPermohonan 
			from dtluangmuka d join trxuangmuka m on d.NoUangMuka = m.NoUangMuka
		where d.Deleted = 0 AND id = ?";
		$query = $this->db->query($sql, $ID);
		if($query->num_rows() > 0)
		{
			$resultArr = array(
				'totalRealisasi' => 0,
				'totalPermohonan' => 0
			);
			foreach($query->result() as $row)
			{
				$resultArr["totalRealisasi"] = $row->TotalRealisasi;
				$resultArr["totalPermohonan"] = $row->TotalPermohonan;
				return $resultArr;
			}
		}else{
			return false;
		}
	}

	function getDetailTrxUangMukaRealisasibyNoUM($noUangMuka){
		$sql = "select iddtluangmuka,JenisBiayaRealisasi,KeteranganRealisasi,
				JumlahRealisasi,
				HargaRealisasi,TotalRealisasi,
				format(HargaRealisasi,0) as fHargaRealisasi,
				format(TotalRealisasi,0) as fTotalRealisasi
				from dtluangmuka d					
				where d.deleted = 0 and d.NoUangMuka = ?
				order by JenisBiayaRealisasi";
			//$this->db->order_by("JenisBiayaRealisasi", "asc");
		$query = $this->db->query($sql, $noUangMuka);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function insertDtlBiayaLaporan($noSPD){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$uangSaku = array($this->input->post('txtKetUangSaku'),str_replace(",","",$this->input->post('txtBebanHarianUangSaku')),$this->input->post('txtJmlhHariUangSaku'),$NPKlogin,$noSPD,'Uang Saku');
		$uangMakan = array($this->input->post('txtKetUangMakan'),str_replace(",","",$this->input->post('txtBebanHarianUangMakan')),$this->input->post('txtJmlhHariUangMakan'),$NPKlogin,$noSPD,'Uang Makan');
		$hotel = array($this->input->post('txtKetHotel'),str_replace(",","",$this->input->post('txtBebanHarianHotel')),$this->input->post('txtJmlhHariHotelLap'),$NPKlogin,$noSPD,'Hotel');
		$hotel2 = array($this->input->post('txtKetHotel2'),str_replace(",","",$this->input->post('txtBebanHarianHotel2')),$this->input->post('txtJmlhHariHotel2'),$NPKlogin,$noSPD,'Penginapan');
		$taksi = array($this->input->post('txtKetTaksi'),str_replace(",","",$this->input->post('txtBebanHarianTaksi')),1,$NPKlogin,$noSPD,'Taksi');
		$airport = array($this->input->post('txtKetAirport'),str_replace(",","",$this->input->post('txtBebanHarianAirport')),1,$NPKlogin,$noSPD,'Airport Tax');
		$lain1 = array($this->input->post('txtKetLain1'),str_replace(",","",$this->input->post('txtBebanHarianLain1')),1,$NPKlogin,$noSPD,'Lain-lain 1');
		$lain2 = array($this->input->post('txtKetLain2'),str_replace(",","",$this->input->post('txtBebanHarianLain2')),1,$NPKlogin,$noSPD,'Lain-lain 2');
		$lain3 = array($this->input->post('txtKetLain3'),str_replace(",","",$this->input->post('txtBebanHarianLain3')),1,$NPKlogin,$noSPD,'Lain-lain 3');
		
		$sql = "UPDATE dtlBiayaSPD SET KeteranganLap=?,BebanHarianLap=?,JumlahHariLap=?,UpdatedOn=now(),UpdatedBy=?
			WHERE noSPD = ? AND Deskripsi = ?";
		
		$this->db->query($sql,$uangSaku);
		$this->db->query($sql,$uangMakan);
		$this->db->query($sql,$hotel);
		$this->db->query($sql,$hotel2);
		$this->db->query($sql,$taksi);
		$this->db->query($sql,$airport);
		//if($this->input->post('txtBebanHarianLain1')>0)
			$this->db->query($sql,$lain1);
		//if($this->input->post('txtBebanHarianLain2')>0)
			$this->db->query($sql,$lain2);
		//if($this->input->post('txtBebanHarianLain3')>0)
			$this->db->query($sql,$lain3);
		/*print_r($lain1);
		print_r($lain2);
		print_r($lain3);*/
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function getHeaderTrxUangMukabyNoUMRealisasibyNoUM($NoUangMuka){
		$sql = "SELECT t.NoUangMuka, t.DPA,  DATE_FORMAT(TanggalPermohonan, '%Y-%m-%d') TanggalPermohonan,DATE_FORMAT(TanggalRealisasi, '%Y-%m-%d') TanggalRealisasi,
					   NoPP, t.KeteranganPermohonan, Status, TipeBayarUangMuka, BankBayarUangMuka, NoRekBayarUangMuka, 
					   NmPenerimaBayarUangMuka, AlasanOutstanding, NoSPD, NoAkomodasiTiket, 
					   DATE_FORMAT(TglTerimaHRGAPermohonan, '%Y-%m-%d') TglTerimaHRGAPermohonan,DATE_FORMAT(TglTerimaFinancePermohonan, '%Y-%m-%d')   TglTerimaFinancePermohonan, 
					   DATE_FORMAT(TglBayarFinancePermohonan, '%Y-%m-%d') TglBayarFinancePermohonan, DATE_FORMAT(TglTerimaHRGARealisasi, '%Y-%m-%d') TglTerimaHRGARealisasi,
					   DATE_FORMAT(TglTerimaFinanceRealisasi, '%Y-%m-%d') TglTerimaFinanceRealisasi,  DATE_FORMAT(TglBayarFinanceRealisasi, '%Y-%m-%d') TglBayarFinanceRealisasi, 
					   NoBEPermohonan, NoBERealisasi, TipeKembaliUangMuka, BankKembaliUangMuka, 
					   NoRekKembaliUangMuka, NmPenerimaUangMuka, LebihKurangRealisasi, 
					   t.CreatedBy, t.CreatedOn, 
					   JenisKegiatanUangMuka, NPKPemohon, ReasonOutstanding, DATE_FORMAT(TanggalMulai, '%Y-%m-%d') TanggalMulai, 
					   DATE_FORMAT(TanggalSelesai, '%Y-%m-%d') TanggalSelesai, WaktuBayarTercepat, WaktuPenyelesaianTerlambat, 
					   DATE_FORMAT(TanggalRealisasi, '%Y-%m-%d') TanggalRealisasi, FilePathPermohonan, FilePathRealisasi, id ,
					   u.nama, u2.nama as atasan, mj.JenisKegiatan, sum(ifnull(TotalPermohonan,0)) as TotalPermohonan, sum(ifnull(TotalRealisasi,0)) as TotalRealisasi,
					   sum(ifnull(TotalPermohonan,0))-sum(ifnull(TotalRealisasi,0)) as Selisih,
					   format(sum(ifnull(TotalPermohonan,0)),0) as fTotalPermohonan, format(sum(ifnull(TotalRealisasi,0)),0) as fTotalRealisasi,
					   format(sum(ifnull(TotalPermohonan,0))-sum(ifnull(TotalRealisasi,0)),0) as fSelisih,
					   case when  sum(ifnull(TotalPermohonan,0))-sum(ifnull(TotalRealisasi,0)) < 0 then  (sum(ifnull(TotalPermohonan,0))-sum(ifnull(TotalRealisasi,0))) *-1 else  sum(ifnull(TotalPermohonan,0))-sum(ifnull(TotalRealisasi,0)) end as selisihpositip
				FROM trxuangmuka t
					 left join mstruser u on t.CreatedBy = u.npk
					 left join mstruser u2 on t.NPKPemohon = u2.npk and u2.deleted = 0
					 left join mstrjnskegiatanum mj on t.JenisKegiatanUangMuka=mj.IdKegiatan
					 left join dtluangmuka dtl on dtl.NoUangMuka=t.NoUangMuka and dtl.deleted=0
				where t.deleted = 0  and t.NoUangMuka = ? 
				group by t.NoUangMuka, t.DPA,  DATE_FORMAT(TanggalPermohonan, '%Y-%m-%d') ,
					   NoPP, t.KeteranganPermohonan, Status, TipeBayarUangMuka, BankBayarUangMuka, NoRekBayarUangMuka, 
					   NmPenerimaBayarUangMuka, AlasanOutstanding, NoSPD, NoAkomodasiTiket, 
					   DATE_FORMAT(TglTerimaHRGAPermohonan, '%Y-%m-%d') ,DATE_FORMAT(TglTerimaFinancePermohonan, '%Y-%m-%d')   , 
					   DATE_FORMAT(TglBayarFinancePermohonan, '%Y-%m-%d') , DATE_FORMAT(TglTerimaHRGARealisasi, '%Y-%m-%d') ,
					   DATE_FORMAT(TglTerimaFinanceRealisasi, '%Y-%m-%d') ,  DATE_FORMAT(TglBayarFinanceRealisasi, '%Y-%m-%d') , 
					   NoBEPermohonan, NoBERealisasi, TipeKembaliUangMuka, BankKembaliUangMuka, 
					   NoRekKembaliUangMuka, NmPenerimaUangMuka, LebihKurangRealisasi, 
					   t.CreatedBy, t.CreatedOn, 
					   JenisKegiatanUangMuka, NPKPemohon, ReasonOutstanding, DATE_FORMAT(TanggalMulai, '%Y-%m-%d') , 
					   DATE_FORMAT(TanggalSelesai, '%Y-%m-%d') , WaktuBayarTercepat, WaktuPenyelesaianTerlambat, 
					   DATE_FORMAT(TanggalRealisasi, '%Y-%m-%d') , FilePathPermohonan, FilePathRealisasi, id ,
					   u.nama, u2.nama , mj.JenisKegiatan
				";
		$query = $this->db->query($sql, $NoUangMuka);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getNoSPDBelumLapor($npk){
		$sql = "select NoSPD
			from trkspd
			where deleted = 0 and statusLaporan = 'B' and NPK = ?";
		$query = $this->db->query($sql,$npk);
		return $query->result();
		
	}
	
	function getOneNoSPD($noSPD){
		$sql = "select NoSPD
			from trkspd
			where deleted = 0 and statusLaporan = 'B' and noSPD = ?";
		$query = $this->db->query($sql,$noSPD);
		return $query->result();
	}
	
	function getNoAkomodasiTiket($NoUangMuka)
	{
		$sql = "select NoAkomodasiTiket from trxuangmuka where NoUangMuka=?";
		$query = $this->db->query($sql, $NoUangMuka);
		
		if($query->num_rows() == 1 and $query->num_rows() !='')
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->NoAkomodasiTiket;
			}
		}
		else
		{
			return "0";
		}
	}
	
	
	
	
	function get_tahun(){
		$sql = "select distinct year(tanggalspd) as year from trkspd where deleted = 0";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function deleteSPD($noSPD){
		$sql = "update trkSPD set deleted = 1 where noSPD = ?";
		$query = $this->db->query($sql,$noSPD);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function deleteLaporanSPD($noSPD){
		$sql = "update trkSPD set StatusLaporan = 'B' where noSPD = ?";
		$query = $this->db->query($sql,$noSPD);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	

	function integerToRoman($integer)
	{
		// Convert the integer into an integer (just to make sure)
		$integer = intval($integer);
		$result = '';
		
		// Create a lookup array that contains all of the Roman numerals.
		$lookup = array('M' => 1000,
		'CM' => 900,
		'D' => 500,
		'CD' => 400,
		'C' => 100,
		'XC' => 90,
		'L' => 50,
		'XL' => 40,
		'X' => 10,
		'IX' => 9,
		'V' => 5,
		'IV' => 4,
		'I' => 1);
		
		foreach($lookup as $roman => $value){
		// Determine the number of matches
		$matches = intval($integer/$value);
		
		// Add the same number of characters to the string
		$result .= str_repeat($roman,$matches);
		
		// Set the integer to be the remainder of the integer and the value
		$integer = $integer % $value;
		}
	
		// The Roman numeral should be built, return it
		return $result;
	}

	function NumberFromRoman($roman)
	{
		$romans = array(
			'M' => 1000,
			'CM' => 900,
			'D' => 500,
			'CD' => 400,
			'C' => 100,
			'XC' => 90,
			'L' => 50,
			'XL' => 40,
			'X' => 10,
			'IX' => 9,
			'V' => 5,
			'IV' => 4,
			'I' => 1,
		);		
		
		$result = 0;
		
		foreach ($romans as $key => $value) {
			while (strpos($roman, $key) === 0) {
				$result += $value;
				$roman = substr($roman, strlen($key));
			}
		}
		return $result;
	}

	function insertPelimpahanWewenang ($noSPD,$noPW){
		
		
		$session_data = $this->session->userdata('logged_in');
		$NPK = $session_data['npk'];
		$tanggalBerangkatSPDddmmyyyy = $this->input->post('txtTanggalBerangkat');
		$tanggalBerangkatSPD = substr($tanggalBerangkatSPDddmmyyyy,6,4)."-".substr($tanggalBerangkatSPDddmmyyyy,3,2)."-".substr($tanggalBerangkatSPDddmmyyyy,0,2);
		$tanggalKembaliSPDddmmyyyy = $this->input->post('txtTanggalKembali');
		$tanggalKembaliSPD = substr($tanggalKembaliSPDddmmyyyy,6,4)."-".substr($tanggalKembaliSPDddmmyyyy,3,2)."-".substr($tanggalKembaliSPDddmmyyyy,0,2);
		
		
		$i=0;
		
		
		$sql = "INSERT INTO pelimpahanwewenang (KodePelimpahanWewenang, NoTransaksi,JenisTransaksi,TanggalMulai,TanggalSelesai,CreatedOn,CreatedBy) 
		VALUES (?,?,?,?,?,now(),?)";

		$this->db->query($sql,array($noPW,$noSPD,'S',$tanggalBerangkatSPD,$tanggalKembaliSPD,$NPK));

		$sqlupdate = "UPDATE dtlpelimpahanwewenang SET KodePelimpahanWewenang =?,UpdatedOn = now(),  UpdatedBy=?
		WHERE CreatedBy = ? and Deleted=0 and KodePelimpahanWewenang='-'";

		$this->db->query($sqlupdate,array($noPW,$NPK,$NPK));

		for( $i = 1; $i<=5; $i++ ) {
			if ( $this->input->post('txtNamaEmail'.$i) != '')
			{	
				$sqlemail = "INSERT INTO dtlemailpelimpahanwewenang (KodePelimpahanWewenang, Nama,EmailYangDiTuju,CreatedOn,CreatedBy) 
				VALUES (?,?,?,now(),?)";

				$this->db->query($sqlemail,array($noPW,ucwords($this->input->post('txtNamaEmail'.$i)),$this->input->post('txtAlamatEmail'.$i),$NPK));
			}		
		}
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function cekAdaPelimpahanWewenang ($noSPD)
	{
		$sql = "select * 
		from pelimpahanwewenang
		where NoTransaksi= ?";
		$query = $this->db->query($sql,$noSPD);
		
		if($query->num_rows() > 0){
			return true;
		}else {
			return false;
		}
	}

	
	function updateReasonByNoUM($noUM){
		$session_data = $this->session->userdata('logged_in');
		$NPKLogin = $session_data['npk'];
		$Reason = $this->input->post('txtReason');
		

		$sql = "UPDATE trxuangmuka SET 
					   ReasonOutStanding= ?,LastUpdateOn=now(), LastUpdateBy=?
				WHERE NoUangMuka = ?";
		
		$this->db->query($sql,array($Reason,$NPKLogin,$noUM));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function updateReasonByNoSPD($noSPD){
		$session_data = $this->session->userdata('logged_in');
		$NPKLogin = $session_data['npk'];
		$Reason = $this->input->post('txtReason');
		

		$sql = "UPDATE trxuangmuka SET 
					   ReasonOutStanding= ?,LastUpdateOn=now(), LastUpdateBy=?
				WHERE NoSPD = ?";
		
		$this->db->query($sql,array($Reason,$NPKLogin,$noSPD));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
}
?>