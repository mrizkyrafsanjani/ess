<?php 
Class LokasiKaryawan extends Ci_Model
{
	function getLokasiKaryawan($NPK)
	{	
		
		$sql = "select l.NPK, l.Nama, l.Departemen as KodeDepartemen, d.NamaDepartemen from mstruser l 
				  left join (select * from departemen where deleted = 0) d on l.Departemen = d.KodeDepartemen
				where l.deleted = 0 and l.NPK = ? ";
		$query = $this->db->query($sql, $NPK);
		
		//$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>