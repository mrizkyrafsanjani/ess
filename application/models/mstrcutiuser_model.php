<?php 
Class mstrcutiuser_model extends Ci_Model
{
	function getMstrCutiUser($NPK,$tahun)
	{	
		$sql = "SELECT * FROM mstrcutiuser 
			WHERE Deleted = 0 AND NPK = ? AND Tahun = ?";
		$query = $this->db->query($sql, array($NPK,$tahun));
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>