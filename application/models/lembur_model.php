<?php 
Class Lembur_model extends Ci_Model
{
	function getKodeLembur($npk)
	{	
		$sql = "select KodeLembur from lembur
		where NPK = ? and Deleted = 0
		ORDER BY KodeLembur DESC
		LIMIT 1";
		
		$query = $this->db->query($sql,array($npk));
		if($query->num_rows() > 0){
			$KodeLembur = "";
			foreach($query->result() as $row)
			{
				$KodeLembur = $row->KodeLembur;
			}
			return $KodeLembur;
		}else{
			return false;
		}
	}
	
	function getLembur($KodeUserTask)
	{
		$sql = "select l.NPK,l.KodeLembur,Nama,Tanggal, Hari, JamMulaiRencana, JamSelesaiRencana, JamMulaiRealisasi, JamSelesaiRealisasi,TugasLembur,StatusLembur
			from lembur l 
			  join dtltrkrwy rw on l.KodeLembur = rw.NoTransaksi
			  join mstruser u on l.NPK = u.NPK
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getLemburByNoTransaksi($KodeLembur)
	{
		try
		{
			$sql = "select l.NPK,l.KodeLembur,Nama,Tanggal, Hari, JamMulaiRencana, JamSelesaiRencana, JamMulaiRealisasi, JamSelesaiRealisasi,TugasLembur,StatusLembur
				from lembur l
				  join mstruser u on l.NPK = u.NPK
				where l.KodeLembur = ?";
			fire_print('log','KodeLembur proses getLemburByNoTransaksi='.$KodeLembur);
			$query = $this->db->query($sql,array($KodeLembur));
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}
	
	function getTotalJamLemburPerBulan($NPK,$Tanggal)
	{
		$sql = "select SUM(TotalJamRealisasi) as totaljam from lembur
			where deleted = 0 and (statuslembur = 'APR' or statuslembur = 'PER' )
			and NPK = ? and date_format(Tanggal,'%Y%m') = date_format(?,'%Y%m')
			";
		
		$query = $this->db->query($sql,array($NPK,$Tanggal));
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getUpahLembur($NamaKaryawan,$Tahun,$Bulan,$npk)
	{
		$sqlKaryawanWhere = '';
		if($NamaKaryawan != '')
		{
			$sqlKaryawanWhere = " and nama like '%$NamaKaryawan%' ";
		}
		else if($npk != '')
		{
			$sqlKaryawanWhere = " and l.npk like '$npk' ";
		}
		
		$sql = "select l.NPK, SUM(IFNULL(TunjanganMakan,0)) as TunjanganMakan, SUM(IFNULL(TunjanganTransport,0)) as TunjanganTransport,
				SUM(IFNULL(TotalUpahLembur,0)) as TotalUpahLembur, SUM(IFNULL(TotalJamRealisasi,0)) as TotalJamLembur from lembur l
				join mstruser u on u.NPK = l.NPK
				where year(Tanggal) = ? and month(Tanggal) = ?
				and l.Deleted = 0 and l.StatusLembur = 'APR'
				$sqlKaryawanWhere
				group by l.NPK";
		
		$query = $this->db->query($sql,array($Tahun,$Bulan));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function updateStatusLembur($KodeUserTask, $status, $updatedby)
	{
		try{
			$sql = "update lembur l
				join dtltrkrwy rw on l.KodeLembur = rw.NoTransaksi
				set
					l.StatusLembur = ?,
					l.UpdatedOn = now(),
					l.UpdatedBy = ?
				where rw.KodeUserTask = ?";
			$query = $this->db->query($sql,array($status,$updatedby,$KodeUserTask));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
}
?>