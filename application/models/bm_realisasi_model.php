<?php 
Class Bm_realisasi_model extends Ci_Model
{
	function insertoverwrite($data,$createdBy)
    {
        $tanggalArr = explode('/',$data[0]);
        $tanggal = $tanggalArr[2]."-".$tanggalArr[0]."-".$tanggalArr[1];
        $keterangan = $data[1];
        $noBatch = $data[2];
        $nominal = $data[3];
        $kodeActivity = $data[4];
        $tglLapArr = explode('/',$data[5]);
        $tanggalLaporan = $tglLapArr[2]."-".$tglLapArr[0]."-".$tglLapArr[1];
        $success = 0;
		$sql = "update bm_realisasi SET Deleted = 1, UpdatedBy = ?, UpdatedOn = NOW()
			where KodeActivity = ? AND Tanggal = ? AND NoBatch = ? AND Deleted = 0";
		$query = $this->db->query($sql,array($createdBy,$kodeActivity, $tanggal,$noBatch));
		
        $sql = "insert into bm_realisasi(Tanggal,Keterangan,NoBatch, Nominal, KodeActivity, TanggalLaporan,CreatedOn,CreatedBy)
        VALUES(?,?,?,?,?,?,NOW(),?)";
        $query = $this->db->query($sql,array($tanggal,$keterangan,$noBatch,$nominal,$kodeActivity,$tanggalLaporan,$createdBy));
        if($query) $success++;
    		
		if($success == 1){
			return true;
		}else{
			return false;
		}
    }

    function getUploadData($createdBy,$tanggalLaporan)
    {
        
        $sql = "select tanggal, keterangan, nobatch, nominal, kodeactivity, tanggallaporan
            from bm_realisasi
            where Deleted = 0 AND CreatedBy = ? AND TanggalLaporan = ?";
        $query = $this->db->query($sql,array($createdBy, $tanggalLaporan));
        
        if($query->num_rows() > 0)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function getLaporanRealisasi($dpa,$tanggal,$jenispengeluaran)
    {
        $sql = "SET @dpa := ?, @tanggal := ?, @jenispengeluaran := ?;";
        $query = $this->db->query($sql,array($dpa, $tanggal,$jenispengeluaran));

        $whereDPA = "";
        if($dpa == 1 || $dpa == 2){
            $select = "select a.KodeActivity,a.NamaActivity ,a.KodeCoa, k.NamaKategori, b.JumlahBudget as BudgetYTD,sum(ifnull(r.Nominal,0)) as RealisasiYTD,b.JumlahBudget-sum(ifnull(r.Nominal,0)) as SisaBudget, ifnull(r2.RealisasiYTDLalu,0) as RealisasiYTDLalu,
  MAX(r.TanggalLaporan) as TanggalLaporan";
            $whereDPA = " and a.DPA = @dpa ";
            $groupby = "group by a.KodeCoa,a.KodeActivity, b.JumlahBudget";
        }else{
            $select = "select LEFT(a.KodeActivity,5) as KodeActivity, SUBSTRING(a.NamaActivity,1,LOCATE('DPA',a.NamaActivity)-2) as NamaActivity , k.NamaKategori, SUM(b.JumlahBudget) as BudgetYTD,sum(ifnull(r.Nominal,0)) as RealisasiYTD,sum(b.JumlahBudget)-sum(ifnull(r.Nominal,0)) as SisaBudget, SUM(ifnull(r2.RealisasiYTDLalu,0)) as RealisasiYTDLalu,
  MAX(r.TanggalLaporan) as TanggalLaporan";
            $groupby = "group by LEFT(a.KodeActivity,5),SUBSTRING(a.NamaActivity,1,LOCATE('DPA',a.NamaActivity)-2), k.NamaKategori";
        }
        $sql = "$select
from bm_realisasi r 
  join bm_activity a on r.KodeActivity = a.KodeActivity 
  join bm_kategori k on a.KodeKategori = k.KodeKategori    
  join (
    select KodeActivity, sum(JumlahBudget) as JumlahBudget
    from bm_budget where Deleted = 0 AND year(Periode) = year(@tanggal) and periode <= @tanggal 
    group by KodeActivity
  ) b on r.KodeActivity = b.KodeActivity
  left join (
    select KodeActivity, sum(Nominal) as RealisasiYTDLalu 
    from bm_realisasi where deleted = 0 and year(Tanggal) = year(@tanggal)-1 and Tanggal <= DATE_ADD(@tanggal ,INTERVAL -1 YEAR)
    group by KodeActivity 
  ) r2 on r2.KodeActivity = r.KodeActivity 
where r.deleted = 0 AND year(Tanggal) = year(@tanggal) and Tanggal <= @tanggal $whereDPA and a.JenisPengeluaran = @jenispengeluaran
$groupby

UNION

$select
from (
    select KodeActivity, sum(JumlahBudget) as JumlahBudget
    from bm_budget where Deleted = 0 AND year(Periode) = year(@tanggal) and periode <= @tanggal and bm_budget.JumlahBudget != 0
    group by KodeActivity
  ) b
  join bm_activity a on b.KodeActivity = a.KodeActivity 
  join bm_kategori k on a.KodeKategori = k.KodeKategori    
  left join (
    select KodeActivity, sum(Nominal) as Nominal, max(TanggalLaporan) as TanggalLaporan
    from bm_realisasi where deleted = 0 and year(Tanggal) = year(@tanggal) and Tanggal <= @tanggal
    group by KodeActivity
  ) r on b.KodeActivity = r.KodeActivity
  left join (
    select KodeActivity, sum(Nominal) as RealisasiYTDLalu 
    from bm_realisasi where deleted = 0 and year(Tanggal) = year(@tanggal)-1 and Tanggal <= DATE_ADD(@tanggal ,INTERVAL -1 YEAR)
    group by KodeActivity 
  ) r2 on r2.KodeActivity = b.KodeActivity 
where a.JenisPengeluaran = @jenispengeluaran $whereDPA
$groupby
having sum(ifnull(r.Nominal,0)) = 0;
";

    $query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query;
        }else{
            return false;
        }
    }

    function getRealisasiTotal($kodeActivity)
    {
        $sql = "select SUM(Nominal) as TotalRealisasi from bm_realisasi
where Deleted = 0 and KodeActivity = ? and YEAR(Tanggal) = YEAR(NOW());
";
        $query = $this->db->query($sql,array($kodeActivity));
        if($query->num_rows() > 0)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function getRealisasiNonValid()
    {
        $sql = "select * from bm_realisasi
where Deleted = 0 AND KodeActivity not in (select KodeActivity from bm_activity where Deleted = 0);";
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            return $query->result();
        }else{
            return false;
        }
    }
}
?>