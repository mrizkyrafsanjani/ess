<?php 
Class Menu extends Ci_Model
{
	function getMenu($npk,$koderole)
	{	
		if($koderole == 1){
			$sql = "select KodeMenu,menuname,url,Parent as ParentMenu,MenuOrder,Icon from menu 
			where Deleted = 0 order by MenuOrder";
			$query = $this->db->query($sql);
		}else{
			$sql = "select m.KodeMenu,m.menuname,m.url,m.Parent as ParentMenu,m.MenuOrder,m.Icon from mstruser mu 
			join rolemenu rm on mu.koderole = rm.koderole 
			join menu m on rm.kodemenu = m.kodemenu 
			where mu.deleted=0 and rm.deleted = 0 and m.Deleted = 0 and npk = ? order by MenuOrder";
			$query = $this->db->query($sql, $npk);
		}		
		//$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>