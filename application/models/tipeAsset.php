<?php 
Class TipeAsset extends Ci_Model
{
	function getTipeAsset($kodeTipeAsset)
	{	
		if($kodeTipeAsset == ""){
			$sql = "select KodeTipeAsset, AssetMilik, Deskripsi, NomorTipeAsset from tipeasset
				where deleted = 0";
		}else{
			$sql = "select KodeTipeAsset, AssetMilik, Deskripsi, NomorTipeAsset from tipeasset 
				where deleted = 0 and KodeTipeAsset = ? ";
		}
		$query = $this->db->query($sql, $kodeTipeAsset);
		
		//$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>