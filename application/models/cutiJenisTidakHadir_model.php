<?php 
Class CutiJenisTidakHadir_model extends Ci_Model
{
	function getMinMaxTanggalCuti($KodeCuti)
	{
		$sql = "select min(TanggalMulai) as TanggalMulai, max(TanggalSelesai) as TanggalSelesai
			from cutijenistidakhadir
			where KodeCuti = ?";
		fire_print('log',"Kode Cuti get MinMax: $KodeCuti");
		$query = $this->db->query($sql,array($KodeCuti));
		if($query->num_rows() > 0){			
			$dataMinMaxTanggalCuti = array();
			foreach($query->result() as $row)
			{
				fire_print('log',"TanggalMulaiCuti ".$row->TanggalMulai);
				$dataMinMaxTanggalCuti = array(
					'TanggalMulai' => $row->TanggalMulai,
					'TanggalSelesai' => $row->TanggalSelesai
				);
			}
			return $dataMinMaxTanggalCuti;
		}else{
			return false;
		}
	}
	
	function getMinMaxTanggalCutiOnSubmit($npk)
	{
		$sql = "select min(TanggalMulai) as TanggalMulai, max(TanggalSelesai) as TanggalSelesai
			from cutijenistidakhadir
			where CreatedBy = ? AND KodeCuti = ''";
		fire_print('log',"NPK get MinMax: $npk");
		$query = $this->db->query($sql,array($npk));
		if($query->num_rows() > 0){			
			$dataMinMaxTanggalCuti = array();
			foreach($query->result() as $row)
			{
				fire_print('log',"TanggalMulaiCuti ".$row->TanggalMulai);
				$dataMinMaxTanggalCuti = array(
					'TanggalMulai' => $row->TanggalMulai,
					'TanggalSelesai' => $row->TanggalSelesai
				);
			}
			return $dataMinMaxTanggalCuti;
		}else{
			return false;
		}
	}
	
	function getMinMaxTanggalCutiOnEdit($kodeusertask)
	{
		$sql = "select KodeCuti,min(TanggalMulai) as TanggalMulai, max(TanggalSelesai) as TanggalSelesai
			from cutijenistidakhadir
			where KodeCuti = (select noTransaksi from dtltrkrwy where KodeUserTask = ? limit 1)
			group by KodeCuti";
		fire_print('log',"kodeusertask get MinMax: $kodeusertask");
		$query = $this->db->query($sql,array($kodeusertask));
		if($query->num_rows() > 0){			
			$dataMinMaxTanggalCuti = array();
			foreach($query->result() as $row)
			{
				fire_print('log',"TanggalMulaiCuti ".$row->TanggalMulai);
				$dataMinMaxTanggalCuti = array(
					'TanggalMulai' => $row->TanggalMulai,
					'TanggalSelesai' => $row->TanggalSelesai,
					'KodeCuti' => $row->KodeCuti
				);
			}
			return $dataMinMaxTanggalCuti;
		}else{
			return false;
		}
	}
	
	function getAllTanggalOnProses($NPK)
	{
		try
		{
			fire_print('log','masuk ke getAllTanggalOnProses');
			$sql = "select * from cutijenistidakhadir cjth
			join cuti c on cjth.kodecuti = c.kodecuti
				where c.Deleted = 0 AND cjth.Deleted = 0 AND cjth.CreatedBy = ?
				and c.StatusApproval NOT LIKE 'DE%'";
			
			$query = $this->db->query($sql,array($NPK));
			fire_print('log',"getAllTanggalOnProses. NPK: $NPK. jmlh baris query : ".$query->num_rows());
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function getCutiTahunanOnProses($NPK,$Tahun='')
	{
		if($Tahun == '')
		{
			$sql = "select * from cutijenistidakhadir
				where KodeCuti = '' AND Deleted = 0 and KodeJenisTidakHadir = 1 and CreatedBy = ?";
			$query = $this->db->query($sql,array($NPK));
		}
		else
		{
			$sql = "select * from cutijenistidakhadir
				where KodeCuti = '' AND Deleted = 0 and KodeJenisTidakHadir = 1 and CreatedBy = ? and TahunCutiYangDipakai = ?";
			$query = $this->db->query($sql,array($NPK,$Tahun));
		}
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getCutiBesarOnProses($NPK,$Tahun='')
	{
		if($Tahun == '')
		{
			$sql = "select * from cutijenistidakhadir
				where KodeCuti = '' AND Deleted = 0 and KodeJenisTidakHadir = 2 and CreatedBy = ?";
			$query = $this->db->query($sql,array($NPK));
		}
		else
		{
			$sql = "select * from cutijenistidakhadir
				where KodeCuti = '' AND Deleted = 0 and KodeJenisTidakHadir = 2 and CreatedBy = ? and TahunCutiYangDipakai = ?";
			$query = $this->db->query($sql,array($NPK,$Tahun));
		}
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getHutangCutiTahunan($NPK,$Tahun)
	{
		try
		{
			$sql = "select cjt.* from cutijenistidakhadir cjt 
							join cuti c on cjt.KodeCuti = c.KodeCuti
						  where c.StatusApproval = 'APR' and 
							c.Deleted = 0 and cjt.Deleted = 0 and (cjt.KodeJenisTidakHadir = 1 or cjt.KodeJenisTidakHadir = 12) and 
							cjt.TahunCutiYangDipakai = ? and c.NPK = ?
						  order by cjt.CreatedOn DESC";
			$query = $this->db->query($sql,array($Tahun, $NPK));
			
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function getMinMaxTanggalCutiDetail($KodeCutiJenisTidakHadir)
	{
		$sql = "select min(TanggalMulai) as TanggalMulai, max(TanggalSelesai) as TanggalSelesai
			from cutijenistidakhadir
			where KodeCutiJenisTidakHadir = ?";
		fire_print('log',"Kode Cuti get MinMax: $KodeCutiJenisTidakHadir");
		$query = $this->db->query($sql,array($KodeCutiJenisTidakHadir));
		if($query->num_rows() > 0){			
			$dataMinMaxTanggalCuti = array();
			foreach($query->result() as $row)
			{
				fire_print('log',"TanggalMulaiCuti ".$row->TanggalMulai);
				$dataMinMaxTanggalCuti = array(
					'TanggalMulai' => $row->TanggalMulai,
					'TanggalSelesai' => $row->TanggalSelesai
				);
			}
			return $dataMinMaxTanggalCuti;
		}else{
			return false;
		}
	}
}
?>