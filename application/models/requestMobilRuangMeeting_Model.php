<?php 
Class RequestMobilRuangMeeting_Model extends Ci_Model
{
	function getMobil($kodeMobil)
	{	
		if($kodeMobil == ""){
			$sql = "select kodeMobil, NamaMobil from mobil
				where deleted = 0";
		}else{
			$sql = "select kodeMobil,NamaMobil from mobil 
				where deleted = 0 and kodeMobil = ? ";
		}
		$query = $this->db->query($sql, $kodeMobil);
		
		//$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getRuangMeeting($KodeRuangMeeting)
	{	
		if($KodeRuangMeeting == ""){
			$sql = "select KodeRuangMeeting, NamaRuang from ruangmeeting
				where deleted = 0";
		}else{
			$sql = "select KodeRuangMeeting,NamaRuang from ruangmeeting 
				where deleted = 0 and KodeRuangMeeting = ? ";
		}
		$query = $this->db->query($sql, $KodeRuangMeeting);
		
		//$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getKodeRequest($npk)
	{	
		$sql = "select KodeRequestMobilRuangMeeting from requestmobilruangmeeting
		where CreatedBy = ?
		ORDER BY CreatedOn DESC LIMIT 1";
		
		$query = $this->db->query($sql,array($npk));
		fire_print('log','getKodeRequest: npk = '.$npk);
		
		if($query->num_rows() > 0){
			
			$KodeRequest = "";
			foreach($query->result() as $row)
			{
				$KodeRequest = $row->KodeRequestMobilRuangMeeting;
			}
			return $KodeRequest;
		}else{
			return false;
		}
	}
	
	function getByTransaksiMobilRuangMeeting($KodeRequestMobilRuangMeeting)
	{
		try
		{
			$sql = " select l.NPK,l.KodeRequestMobilRuangMeeting, Tanggal, JamMulai, JamSelesai, Kegiatan, Tujuan
				from requestmobilruangmeeting l
				  join mstruser u on l.NPK = u.NPK
				where l.KodeRequestMobilRuangMeeting = ?";
			
			
			fire_print('log','KodeRequestMobilRuangMeeting proses getByTransaksiMobilRuangMeeting='.$KodeRequestMobilRuangMeeting);
			$query = $this->db->query($sql,array($KodeRequestMobilRuangMeeting));
			
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}
	
	function getMobilRuangMeeting($KodeUserTask)
	{
		$sql = " select KodeRequestMobilRuangMeeting, requestmobilruangmeeting.NPK , Status, JenisRequest, Tanggal, JamMulai, JamSelesai, Kegiatan, Tujuan, requestmobilruangmeeting.CreatedBy
			from requestmobilruangmeeting  
			  join dtltrkrwy rw on KodeRequestMobilRuangMeeting = rw.NoTransaksi
			  join mstruser u on requestmobilruangmeeting.NPK = u.NPK			  
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		
		fire_print('log','KodeUserTask Mobil Ruang Meeting: '.$KodeUserTask);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function updateStatusMobilRuangMeeting($KodeUserTask, $status, $updatedby)
	{
		try{
			$sql = "update requestmobilruangmeeting l
				join dtltrkrwy rw on l.KodeRequestMobilRuangMeeting = rw.NoTransaksi
				set
					l.Status = ?,
					l.UpdatedOn = now(),
					l.UpdatedBy = ?
				where rw.KodeUserTask = ?";
				
			$query = $this->db->query($sql,array($status,$updatedby,$KodeUserTask));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function getJenisRequest($JenisRequest){		
		$data = "";
		
			$JenisRequest = $this->db->query("SELECT JenisRequest FROM requestmobilruangmeeting
			WHERE Deleted = 0 ");
		
		$data = $JenisRequest->result();
		return $data;
	}
	
	function getRequest($Tanggal, $Kode,$JenisRequest)
	{
		fire_print('log',"masuk ke get $JenisRequest : $Kode. tanggal: $Tanggal");
		
		if($JenisRequest == "Mobil"){
			$strQuery =  "and kodeMobil = ?";
		}else if($JenisRequest=="Ruang Meeting"){
			$strQuery = "and KodeRuangMeeting = ?";
		}
		
		$sql = "select * from requestmobilruangmeeting
			where tanggal = ?
			$strQuery
			and deleted = 0
			and status != 'DE'
			";
		
		$query = $this->db->query($sql,array($Tanggal, $Kode));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getActiveRequest($start_time,$end_time, $tipe)
	{
		try{
			if($tipe == "Mobil"){
				$sql = "SELECT Status as IsApproved, KodeRequestMobilRuangMeeting as id, jenisrequest, 
				CONCAT(mu.nama,', ', m.namaMobil) as title, 'false' as allDay,
				CONCAT(Tanggal, ' ', JamMulai) as start, CONCAT(Tanggal, ' ', JamSelesai) as end, rrm.KodeMobil as KodeMobilRuangMeeting,
				CONCAT(Tujuan, ', ', Kegiatan) as Kegiatan
						FROM requestmobilruangmeeting rrm
						join mobil m
						on rrm.kodemobil = m.kodemobil
						join mstruser mu on 
						rrm.createdby = mu.npk
						where mu.deleted = 0 and rrm.deleted = 0 and m.deleted = 0 and 
						jenisrequest = ? and rrm.Status != 'DE' and
						Tanggal BETWEEN ? and ?";
				$query = $this->db->query($sql,array($tipe, $start_time,$end_time));
			}else if ($tipe == "Ruang Meeting"){
				$sql = "SELECT Status as IsApproved, KodeRequestMobilRuangMeeting as id, jenisrequest, 
				CONCAT(mu.nama,', ',rm.Namaruang) as title, 'false' as allDay,
				CONCAT(Tanggal, ' ', JamMulai) as start, CONCAT(Tanggal, ' ', JamSelesai) as end, rrm.KodeRuangMeeting as KodeMobilRuangMeeting,
				Kegiatan
				FROM requestmobilruangmeeting rrm
						join ruangmeeting rm
						on rrm.koderuangmeeting = rm.koderuangmeeting
						join mstruser mu on 
						rrm.createdby = mu.npk
						where mu.deleted = 0 and rrm.deleted = 0 and rm.deleted = 0 and
						jenisrequest = ? and rrm.Status != 'DE' and
						Tanggal BETWEEN ? and ?";
				$query = $this->db->query($sql,array($tipe, $start_time,$end_time));

			}else if($tipe == "All"){
				$sql = "SELECT Status as IsApproved, KodeRequestMobilRuangMeeting as id, jenisrequest, 
				CONCAT(mu.nama,', ', m.namaMobil) as title, 'false' as allDay,
				CONCAT(Tanggal, ' ', JamMulai) as start, CONCAT(Tanggal, ' ', JamSelesai) as end, rrm.KodeMobil as KodeMobilRuangMeeting,
				CONCAT(Tujuan, ', ', Kegiatan) as Kegiatan
						FROM requestmobilruangmeeting rrm
						join mobil m
						on rrm.kodemobil = m.kodemobil
						join mstruser mu on 
						rrm.createdby = mu.npk
						where mu.deleted = 0 and  rrm.deleted = 0 and m.deleted = 0 and 
						jenisrequest = 'Mobil' and rrm.Status != 'DE' and
						Tanggal BETWEEN ? and ?
				UNION
				SELECT Status as IsApproved, KodeRequestMobilRuangMeeting as id, jenisrequest, 
				CONCAT(mu.nama,', ',rm.Namaruang) as title, 'false' as allDay,
				CONCAT(Tanggal, ' ', JamMulai) as start, CONCAT(Tanggal, ' ', JamSelesai) as end, rrm.KodeRuangMeeting as KodeMobilRuangMeeting,
				Kegiatan
				FROM requestmobilruangmeeting rrm
						join ruangmeeting rm
						on rrm.koderuangmeeting = rm.koderuangmeeting
						join mstruser mu on 
						rrm.createdby = mu.npk
						where mu.deleted = 0 and rrm.deleted = 0 and rm.deleted = 0 and
						jenisrequest = 'Ruang Meeting' and rrm.Status != 'DE' and
						Tanggal BETWEEN ? and ?";
				$query = $this->db->query($sql,array($start_time,$end_time,$start_time,$end_time));

			}
			
			if($query){
				return $query->result();
			}else{
				return false;
			}
		}
		catch(Exception $e)
		{
			 throw new Exception( 'Something really gone wrong', 0, $e);
			 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			 return false;
		}
	}	

	function getUserTerkait($KodeRequestMobilRuangMeeting){
		fire_print('log',"masuk ke get User Terkait : ".$KodeRequestMobilRuangMeeting);
		
		$sql = "SELECT mu.Nama from userrequestmobilruangmeeting rrm
		join mstruser mu on rrm.npk = mu.npk
			where KodeRequestMobilRuangMeeting = ?
			and mu.deleted = 0
			and rrm.deleted = 0
			";
		
		$query = $this->db->query($sql,array($KodeRequestMobilRuangMeeting));
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>