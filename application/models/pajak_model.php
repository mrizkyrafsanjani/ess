<?php 

Class Pajak_model extends Ci_Model
{
	
	
	

	function getDataPajakPPH42($masa,$tahun,$dpa){
		$sql = "select p42.kd_form as kodepajak,mp.KodeSetor, mp.JenisPenghasilan, SPLIT_STRING(dat_form,';',10) Nama,
						format(p42.Total_Bruto,0) as bruto,format(p42.Total_Pph,0) as pph,
						p42.Total_Bruto as brutonf,p42.Total_Pph as pphnf
						from pph4ayat2 p42
						join mstrpajak mp on mp.KodePajak=p42.kd_form 
				where ms_pajak=? and th_pajak=? and DPA=? and mp.jenispajak='PPH42' and KodeSetor='411128-403' ";
		$query = $this->db->query($sql, array( $masa,$tahun,$dpa));
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getDataPajakPPH42jasa($masa,$tahun,$dpa){
		$sql = "select p42.kd_form as kodepajak,mp.KodeSetor, mp.JenisPenghasilan, SPLIT_STRING(dat_form,';',10) Nama,
						format(p42.Total_Bruto2,0) as bruto,format(p42.Total_Pph2,0) as pph,
						p42.Total_Bruto2 as brutonf,p42.Total_Pph2 as pphnf
						from pph4ayat2 p42
						join mstrpajak mp on mp.KodePajak=p42.kd_form 
				where ms_pajak=? and th_pajak=? and DPA=? and mp.jenispajak='PPH42' and KodeSetor='411128-409' ";
		$query = $this->db->query($sql, array( $masa,$tahun,$dpa));
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getDataPajakPPH23($masa,$tahun,$dpa){
		$sql = "select Kd_form as kodepajak,KodeSetor,JenisPenghasilan,format(bruto,0) as bruto,format(pajak,0) as pph,nama as Nama,
				bruto as brutonf,pajak as pphnf
				from vw_pph23 p23 
				join mstrpajak mp on mp.KodePajak=p23.kd_form and mp.No = p23.jenis
				where ms_pajak=? and th_pajak=? and DPA=? and mp.jenispajak='PPH23' and 
				( upper(nama) not like '%SEKURITAS%' and upper(nama) not like '%SECURINDO%' and upper(nama) not like  '%SECURITIES%')
				and KodeSetor='411124-100'
				order by kodesetor";
		$query = $this->db->query($sql, array( $masa,$tahun,$dpa));
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getDataPajakPPH231($masa,$tahun,$dpa){
		$sql = "select Kd_form as kodepajak,KodeSetor,JenisPenghasilan,format(bruto,0) as bruto,format(pajak,0) as pph,nama as Nama,
				bruto as brutonf,pajak as pphnf
				from vw_pph23 p23 
				join mstrpajak mp on mp.KodePajak=p23.kd_form and mp.No = p23.jenis
				where ms_pajak=? and th_pajak=? and DPA=? and mp.jenispajak='PPH23' and
				( upper(nama) not like '%SEKURITAS%' and upper(nama) not like '%SECURINDO%' and upper(nama) not like  '%SECURITIES%')
				 and KodeSetor='411124-104'
				order by kodesetor";
		$query = $this->db->query($sql, array( $masa,$tahun,$dpa));
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getDataPajakPPH26($masa,$tahun,$dpa){
		$sql = "select Kd_form as kodepajak,KodeSetor,JenisPenghasilan,format(bruto,0) as bruto,format(pajak,0) as pph,nama as Nama,
				bruto as brutonf,pajak as pphnf
				from vw_pph26 p26 
				join mstrpajak mp on mp.KodePajak=p26.kd_form and mp.No = p26.jenis
				where ms_pajak=? and th_pajak=? and DPA=? and mp.jenispajak='PPH26' and 
				( upper(nama) not like '%SEKURITAS%' and upper(nama) not like '%SECURINDO%' and upper(nama) not like  '%SECURITIES%')
				order by kodesetor";
		$query = $this->db->query($sql, array( $masa,$tahun,$dpa));
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getDataPajakPPH23Grup($masa,$tahun,$dpa){
		$sql = "select Kd_form as kodepajak,KodeSetor,mp.No
				from vw_pph23 p23 
				join mstrpajak mp on mp.KodePajak=p23.kd_form and mp.No = p23.jenis
				where ms_pajak=? and th_pajak=? and DPA=? and mp.jenispajak='PPH23' ";
		$query = $this->db->query($sql, array( $masa,$tahun,$dpa));
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getDataPajakPPH21($masa,$tahun,$dpa){
		$sql = "select case when kode_pajak='21GAJI' then '-' else kode_pajak end as kodepajak,KodeSetor,JenisPenghasilan,nama as Nama, format(p21.jml_bruto,0) as bruto,format(p21.jml_pph,0) as pph,
				p21.jml_bruto as brutonf,p21.jml_pph as pphnf
				from pph21 p21
				join mstrpajak mp on mp.KodePajak=p21.kode_pajak
				where mp.jenispajak='PPH21' and p21.masa=? and p21.tahun=? and DPA=? and p21.kode_pajak not in ('21GAJI','21PESANGON')
				and nomor_bp not in ( select nomor_bp from pph21 where pembetulan=1)
				union all
				select case when kode_pajak='21GAJI' then '-' else kode_pajak end as kodepajak,KodeSetor,JenisPenghasilan,nama as Nama, format(p21.jml_bruto,0) as bruto,format(p21.jml_pph,0) as pph,
				p21.jml_bruto as brutonf,p21.jml_pph as pphnf
				from pph21 p21
				join mstrpajak mp on mp.KodePajak=p21.kode_pajak
				where mp.jenispajak='PPH21' and p21.masa=? and p21.tahun=? and DPA=? and p21.kode_pajak not in ('21GAJI','21PESANGON')
				and  pembetulan=1
				";
		$query = $this->db->query($sql, array( $masa,$tahun,$dpa,$masa,$tahun,$dpa));
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getDataPajakPPH21GAJI($masa,$tahun,$dpa){
		$sql = "select case when kode_pajak='21GAJI' then '-' else kode_pajak end as kodepajak,KodeSetor,JenisPenghasilan,nama as Nama, format(p21.jml_bruto,0) as bruto,format(p21.jml_pph,0) as pph,
				p21.jml_bruto as brutonf,p21.jml_pph as pphnf
				from pph21 p21
				join mstrpajak mp on mp.KodePajak=p21.kode_pajak
				where mp.jenispajak='PPH21' and p21.masa=? and p21.tahun=? and DPA=? and p21.kode_pajak in ('21GAJI')";
		$query = $this->db->query($sql, array( $masa,$tahun,$dpa));
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getDataPajakPPH21PESANGON($masa,$tahun,$dpa){
		$sql = "select case when kode_pajak='21GAJI' then '-' else kode_pajak end as kodepajak,KodeSetor,JenisPenghasilan,nama as Nama, format(p21.jml_bruto,0) as bruto,format(p21.jml_pph,0) as pph,
				p21.jml_bruto as brutonf,p21.jml_pph as pphnf
				from pph21 p21
				join mstrpajak mp on mp.KodePajak=p21.kode_pajak
				where mp.jenispajak='PPH21' and p21.masa=? and p21.tahun=? and DPA=? and p21.kode_pajak in ('21PESANGON')";
		$query = $this->db->query($sql, array( $masa,$tahun,$dpa));
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	

	
	
}
?>