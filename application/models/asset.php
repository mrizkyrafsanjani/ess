<?php 
Class Asset extends Ci_Model
{
	function getLastNumberAsset($DPA,$AssetMilik,$KodeTipeAsset)
	{		
		$sql = "select KodeAsset from asset
				where dpa = ? and assetmilik = ? and kodetipeasset in 
        ( select kodetipeasset from tipeasset where nomortipeasset in (select nomortipeasset from tipeasset where kodetipeasset = ?)) 
				order by kodeasset desc limit 1";
		
		$query = $this->db->query($sql,array($DPA,$AssetMilik,$KodeTipeAsset));
		
		//$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>