<?php 
Class Bm_budget_model extends Ci_Model
{
	function insertoverwrite($kodeTempBudgetHeader,$createdBy)
    {
		$sql = "update bm_budget b join bm_tempbudget tb on b.KodeActivity = tb.KodeActivity and b.Periode = tb.Periode
set b.Deleted = 1, b.UpdatedOn=NOW(), b.UpdatedBy = ?
where tb.KodeTempBudgetHeader = ?";
		$query = $this->db->query($sql,array($createdBy,$kodeTempBudgetHeader));
		if($query){                       
            $sql = "insert into bm_budget(KodeTempBudgetHeader, KodeActivity, Periode, JumlahBudget, Status, CreatedOn, CreatedBy)
select KodeTempBudgetHeader, KodeActivity, Periode, JumlahBudget, Status,NOW(),? from bm_tempbudget
where KodeTempBudgetHeader = ?;";
            $query = $this->db->query($sql,array($createdBy, $kodeTempBudgetHeader));                    
        }
		//$query = $this->db->get();
		
		if($query){
			return true;
		}else{
			return false;
		}
    }

    function getUploadData($createdBy,$kodeUserTask = '')
    {
        if($kodeUserTask == ''){
            $sql = "select tb.KodeTempBudgetHeader, k.NamaKategori, a.JenisPengeluaran , SUM(tb.JumlahBudget) as TotalBudget 
    from bm_tempbudget tb 
    join bm_tempbudgetheader tbh on tbh.KodeTempBudgetHeader = tb.KodeTempBudgetHeader
    join bm_activity a on a.KodeActivity = tb.KodeActivity
    join bm_kategori k on a.KodeKategori = k.KodeKategori
    where tb.Deleted = 0 AND tbh.StatusTransaksi = 'UP' AND tb.CreatedBy = ?
    group by tb.KodeTempBudgetHeader,k.NamaKategori, a.JenisPengeluaran
    order by a.JenisPengeluaran, k.NamaKategori;";
            $query = $this->db->query($sql,array($createdBy));
        }else{
            $sql = "select tb.KodeTempBudgetHeader, k.NamaKategori, a.JenisPengeluaran , SUM(tb.JumlahBudget) as TotalBudget 
    from bm_tempbudget tb 
    join bm_tempbudgetheader tbh on tbh.KodeTempBudgetHeader = tb.KodeTempBudgetHeader
    join bm_activity a on a.KodeActivity = tb.KodeActivity
    join bm_kategori k on a.KodeKategori = k.KodeKategori
    join dtltrkrwy dtr on tbh.KodeTempBudgetHeader = dtr.NoTransaksi
    where tb.Deleted = 0 AND tbh.StatusTransaksi = 'PE' AND dtr.KodeUserTask = ?
    group by tb.KodeTempBudgetHeader,k.NamaKategori, a.JenisPengeluaran
    order by a.JenisPengeluaran, k.NamaKategori;";
            $query = $this->db->query($sql,array($kodeUserTask));
        }
        if($query->num_rows() > 0)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function getRekapMasterBudget($dpa,$tanggal,$jenispengeluaran)
    {
        $sql = "SET @dpa := ?, @tanggal := ?, @jenispengeluaran := ?;";
        $query = $this->db->query($sql,array($dpa, $tanggal,$jenispengeluaran));
        if ($dpa == 1 || $dpa == 2) {
            $sql = "SELECT NamaKategori, SUM(JumlahBudget) as JumlahBudget from 
                      (SELECT kodeactivity, 
                      Sum(jumlahbudget) AS JumlahBudget 
                      FROM bm_budget WHERE Deleted = 0  AND Year(periode) = year(@tanggal) AND periode <=  @tanggal
                      GROUP  BY kodeactivity) b
                      join bm_activity a
                      on a.kodeactivity = b.kodeactivity
                      join bm_kategori k
                      on k.kodekategori = a.kodekategori
                    where dpa = @dpa
                    and a.JenisPengeluaran = @jenispengeluaran
                    group by NamaKategori";
        }else{
            $sql = "SELECT NamaKategori, SUM(JumlahBudget) as JumlahBudget from 
                      (SELECT kodeactivity, 
                      Sum(jumlahbudget) AS JumlahBudget 
                      FROM bm_budget WHERE Deleted = 0  AND Year(periode) = year(@tanggal) AND periode <=  @tanggal
                      GROUP  BY kodeactivity) b
                      join bm_activity a
                      on a.kodeactivity = b.kodeactivity
                      join bm_kategori k
                      on k.kodekategori = a.kodekategori
                    where a.JenisPengeluaran = @jenispengeluaran
                    group by NamaKategori";
        }
        

        $query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->result();
        }else{
            return false;
        }
    }

    function getBudgetTotal($kodeActivity)
    {
        $sql = "select Status,SUM(JumlahBudget) as TotalBudget from bm_budget
where Deleted = 0 and KodeActivity = ? and YEAR(Periode) = YEAR(NOW())
GROUP BY Status;
";
        $query = $this->db->query($sql,array($kodeActivity));
        if($query->num_rows() > 0)
        {
            return $query->result();
        }else{
            return false;
        }
    }
}
?>