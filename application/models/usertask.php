<?php 
Class Usertask extends Ci_Model
{
	function tambahUserTask($requester,$noTransaksi,$kodeUserTask,$jenisTask,$reportTo = '')
	{
		$this->load->model('lembur_model','',TRUE);
		$this->load->model('knowledgemanagement_model','',TRUE);
		$this->load->model('requestMobilRuangMeeting_Model','',TRUE);
		$this->load->model('training_model','',TRUE);
		$this->load->model('undangan_model','',TRUE);
		$this->load->model('peminjamandokumen_model','',TRUE);
		
		try
		{
			fire_print('log','tambahUserTask. requester:'.$requester.',noTransaksi:'.$noTransaksi.',kodeUserTask:'.$kodeUserTask.',jenisTask:'.$jenisTask.',reportTo:'.$reportTo);
			//$this->db->trans_begin(); tidak pakai transaction di level ini, karena malah langsung commit pada transaction yg di luar
			
			//plan menggunakan approval matrix
			//cek sebelum maju approval selanjutnya
			switch($jenisTask)
			{				
				case "LBAP":
					//cek apakah sudah bisa masuk ke proses LBAP.
					//yg dapat masuk ke proses LBAP adalah yg N-1 approval terakhir merupakan AP.
					//jika tidak kembalikan ke LB.
					
					if(!$this->isAlreadyMaxSequence($requester,'LB'))
					{
						$jenisTask = 'LB';
					}
					break;
				case "BPRAP":
					if(!$this->isAlreadyMaxSequence($requester,'BP'))
					{
						$jenisTask = 'BPR';
					}
					break;
				case "BPTAP":
					if(!$this->isAlreadyMaxSequence($requester,'BP'))
					{
						$jenisTask = 'BPT';
					}
					break;
				case "CTAP":
					if(!$this->isAlreadyMaxSequence($requester,'CT'))
					{
						$jenisTask = 'CT';
					}
					break;
				case "PRAP":
					if(!$this->isAlreadyMaxSequence($requester,'PR'))
					{
						$jenisTask = 'PR';
					}
					break;
				case "EPAP":
					if(!$this->isAlreadyMaxSequence($requester,'EP'))
					{
						$jenisTask = 'EP';
					}
					break;
				case "PMAP":
					if(!$this->isAlreadyMaxSequence($requester,'PM'))
					{
						$jenisTask = 'PM';
					}
					break;
				case "PAAP":
					if(!$this->isAlreadyMaxSequence($requester,'PA'))
					{
						$jenisTask = 'PA';
					}
					break;
				case "FPAP":
					if(!$this->isAlreadyMaxSequence($requester,'FP'))
					{
						$jenisTask = 'FP';
					}
					break;
				case "BEAP":
					if(!$this->isAlreadyMaxSequence($requester,'BE'))
					{
						$jenisTask = 'BE';
					}
					break;
				case "KMAP":
					if(!$this->isAlreadyMaxSequence($requester,'KM'))
					{
						$jenisTask = 'KM';
					}
					break;
				case "ATAP":
					if(!$this->isAlreadyMaxSequence($requester,'AT'))
					{
						$jenisTask = 'AT';
					}
					break;
				case "RMAP":
					if(!$this->isAlreadyMaxSequence($requester,'RM'))
					{
						$jenisTask = 'RM';
					}
					break;
				case "TNAP":
					if(!$this->isAlreadyMaxSequence($requester,'TN'))
					{
						$jenisTask = 'TN';
					}
					break;
				case "TSAP":
					if(!$this->isAlreadyMaxSequence($requester,'TS'))
					{
						$jenisTask = 'TS';
					}
					break;
				case "TBAP":
					if(!$this->isAlreadyMaxSequence($requester,'TB'))
					{
						$jenisTask = 'TS';
					}
					break;
			}
			
			//cari nama orang yang menginisiasi transaksi
			$namaInisiatorTransaksi = '';
			$npkInisiatorTransaksi = '';
			$NPKtrx = '';
			fire_print('log','noTransaksi='.$noTransaksi);
			switch(substr($jenisTask,0,2))
			{
				case "LB":
					$data = $this->lembur_model->getLemburByNoTransaksi($noTransaksi);
					foreach($data as $dt){
						$NPKtrx = $dt->NPK;
					}
					break;				
				case "CT":
					$cutilist = $this->db->get_where('cuti',array('KodeCuti'=>$noTransaksi));
					fire_print('log','row number cutilist='.$cutilist->num_rows());
					foreach($cutilist->result() as $ctli){
						$NPKtrx = $ctli->NPK;
					}
					break;
				case "BP":
					$BPKBList = $this->db->get_where('trkbpkb',array('KodeTrk'=>$noTransaksi));
					fire_print('log','row number BPKBlist='.$BPKBList->num_rows());
					foreach($BPKBList->result() as $BPKBli){
						$NPKtrx = $BPKBli->NPK;
					}
					break;
				case "BE":
					$benefitlist = $this->db->get_where('benefit',array('KodeBenefit'=>$noTransaksi));
					fire_print('log','row number cutilist='.$benefitlist->num_rows());
					foreach($benefitlist->result() as $beli){
						$NPKtrx = $beli->NPK;
					}
					break;
				case "KM":
					$artikel = $this->knowledgemanagement_model->getByTransaksiArtikel($noTransaksi);
					foreach($artikel as $ar){
						$NPKtrx = $ar->CreatedBy;
					}
					break;
				case "AT":
					$atklist = $this->db->get_where('headerrequestatk',array('KodeRequestAtk'=>$noTransaksi));
					fire_print('log','row number atklist='.$atklist->num_rows());
					foreach($atklist->result() as $atli){
						$NPKtrx = $atli->CreatedBy;
					}
					break;
				case "RM":
					$request = $this->requestMobilRuangMeeting_Model->getByTransaksiMobilRuangMeeting($noTransaksi);
					foreach($request as $re){
						$NPKtrx = $re->NPK;
					}
					break;
				case "TN":
					$traininglist = $this->training_model->getByTransaksiTraining($noTransaksi);
					foreach($traininglist as $tl){
						$NPKtrx = $tl->NPK;
					}
					break;
				case "TS":
					$undanganlist = $this->undangan_model->getByTransaksiUndangan($noTransaksi);
					foreach($undanganlist as $ts){
						$NPKtrx = $ts->NPK;
					}
					break;
				case "TB":
					$tempbudgetheaderlist = $this->bm_tempbudgetheader_model->getByTransaksiTempBudgetHeader($noTransaksi);
					foreach($tempbudgetheaderlist as $tbh){
						$NPKtrx = $tbh->CreatedBy;
					}
					break;
				case "AK":
					$akomodasilist = $this->db->get_where('akomodasitiket',array('KodeAkomodasiTiket'=>$noTransaksi));
					fire_print('log','row number akomodasilist='.$akomodasilist->num_rows());
					foreach($akomodasilist->result() as $row){
						$NPKtrx = $row->CreatedBy;
					}
					break;
				case "PR":
					$promise = $this->db->get_where('pmtdetailrequest',array('KodeRequestDetail'=>$noTransaksi));
					fire_print('log','row number promise='.$promise->num_rows());
					foreach($promise->result() as $cb){
						$NPKtrx = $cb->CreatedBy;
					}
					break;
				case "EP":
					$promiseedit = $this->db->get_where('pmtdetailrequest',array('KodeRequestDetail'=>$noTransaksi));
					fire_print('log','row number promise='.$promiseedit->num_rows());
					foreach($promiseedit->result() as $cb){
						$NPKtrx = $cb->CreatedBy;
					}
					break;
				case "PM":
					$promisemom = $this->db->get_where('pmtmomrequest',array('KodeRequestMOM'=>$noTransaksi));
					fire_print('log','row number promise='.$promisemom->num_rows());
					foreach($promisemom->result() as $cb){
						$NPKtrx = $cb->CreatedBy;
					}
					break;
				case "PA":
					$promisepara = $this->db->get_where('pmtprmtrequest',array('KodeRequestDetail'=>$noTransaksi));
					fire_print('log','row number promise='.$promisepara->num_rows());
					foreach($promisepara->result() as $cb){
						$NPKtrx = $cb->CreatedBy;
					}
					break;
				case "FP":
					$promisefinal = $this->db->get_where('pmtdetailrequest',array('KodeRequestDetail'=>$noTransaksi));
					fire_print('log','row number promise='.$promisefinal->num_rows());
					foreach($promisefinal->result() as $cb){
						$NPKtrx = $cb->CreatedBy;
					}
					break;
				
			}
			fire_print('log','NPKtrx='.$NPKtrx);
			$data = $this->db->get_where('mstruser',array('NPK'=>$NPKtrx));
			foreach($data->result() as $usr)
			{
				$namaInisiatorTransaksi = $usr->Nama;
				$npkInisiatorTransaksi = $usr->NPK;
			}
			switch($jenisTask)
			{
				case "DP": //DataPribadi
					$query = $this->db->get_where('globalparam',array('Name'=>'AdminHRGA'));
					foreach($query->result() as $row)
					{
						$reportTo = $row->Value;
					}
					$params = "DataPribadi/DataPribadiController/ApprovalDataPribadi";
					$keterangan = "Approval Perubahan Data NPK $requester";
					
					break;
				case "LB": //Lembur
					/*
					$query = $this->db->get_where('mstruser',array('NPK'=>$requester));
					foreach($query->result() as $row)
					{
						$reportTo = $row->NPKAtasan;
					}
					*/
					//plan menggunakan approval matrix
					//dibagian ini akan dilakukan pengecekan LB di approval matrix
					//cek apakah  merupakan pertama diajukan, jika ya, report to diisi dgn npk sequence ke 1,
					//jika tidak maka cek sedang posisi di seqence berapa, dan report to ke sequence selanjutnya.
					
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
										
					$requester = $npkInisiatorTransaksi;
					$params = "Lembur/LemburController/ApprovalRencanaLembur";
					$keterangan = "Approval Rencana Lembur $namaInisiatorTransaksi";
					break;
				case "LBR":
					/* $query = $this->db->get_where('mstruser',array('NPK'=>$requester));
					foreach($query->result() as $row)
					{
						$reportTo = $row->NPKAtasan;
					} */
					$reportTo = $this->getUserSelanjutnya($requester,substr($jenisTask,0,2),$npkInisiatorTransaksi);
					
					$params = "Lembur/LemburController/ApprovalRealisasiLembur";
					$keterangan = "Approval Realisasi Lembur $namaInisiatorTransaksi";
					break;
				case "LBDE":
					$params = "Lembur/LemburController/ViewLemburUser";
					$keterangan = "Edit Rencana Lembur $namaInisiatorTransaksi";
					break;
				case "LBAP":
					$query = $this->db->get_where('mstruser',array('NPK'=>$requester));
					foreach($query->result() as $row)
					{
						$atasan = $row->NPKAtasan;
					}
					$params = "Lembur/LemburController/ViewLemburUser";
					$keterangan = "Realisasi Rencana Lembur $namaInisiatorTransaksi";
					if($this->config->item('enableEmail') == 'true')
						$this->sendEmailToHRD();
					break;
				/*case "LBRDE":
					$params = "Lembur/LemburController/ViewLemburUser";
					$keterangan = "Edit Rencana Lembur NPK $reportTo";
					break;*/
				case "BPR1":
				//bpkb rent
					$reportTo = $this->getUserSelanjutnya($requester,substr($jenisTask,0,2),$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "PeminjamanDokumen/BPKBController/ApprovalBPKB";
					$keterangan = "Approval 1 Peminjaman BPKB $namaInisiatorTransaksi";
					
					break;
				case "BPT1":
				//bpkb take
					$reportTo = $this->getUserSelanjutnya($requester,substr($jenisTask,0,2),$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "PeminjamanDokumen/BPKBController/ApprovalBPKB";
					$keterangan = "Approval 1 Pengambilan BPKB $namaInisiatorTransaksi";
					
					break;
				case "BPR2":
				//bpkb rent
					$reportTo = $this->getUserSelanjutnya($requester,substr($jenisTask,0,2),$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "PeminjamanDokumen/BPKBController/ApprovalBPKB";
					$keterangan = "Approval 2 Peminjaman BPKB $namaInisiatorTransaksi";
					
					break;
				case "BPT2":
				//bpkb take

					$reportTo = $this->getUserSelanjutnya($requester,substr($jenisTask,0,2),$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "PeminjamanDokumen/BPKBController/ApprovalBPKB";
					$keterangan = "Approval 2 Pengambilan BPKB $namaInisiatorTransaksi";
					
					break;
				
				case "CT":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "Cuti/CutiController/ApprovalRencanaCuti";
					$keterangan = "Approval Rencana Cuti $namaInisiatorTransaksi";
					
					/*if($this->config->item('enableEmail') == 'true')
						$this->sendEmailToHRD();
					break;*/
					
					break;
				case "CTDE":
					$params = "Cuti/CutiController/EditCuti";
					$keterangan = "Edit Rencana Cuti $namaInisiatorTransaksi";
					break;
				case "CTAP":
					$query = $this->db->get_where('mstruser',array('NPK'=>$requester));
					foreach($query->result() as $row)
					{
						$atasan = $row->NPKAtasan;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'AdminHRGA'));
					foreach($query->result() as $row)
					{
						$reportTo = $row->Value;
					}
					$params = "Cuti/CutiController/ApprovalRealisasiCuti";
					$keterangan = "Approval Realisasi Cuti $namaInisiatorTransaksi";
					
					if($this->config->item('enableEmail') == 'true')
						$this->sendEmailToHRD();
					break;
				case "BE":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					
					$requester = $npkInisiatorTransaksi;
					$params = "Benefit/BenefitController/ApprovalBenefit";
					$keterangan = "Approval Benefit $namaInisiatorTransaksi";
					break;
				case "BEDE":
					$params = "Benefit/BenefitController/EditBenefit";
					$keterangan = "Edit Benefit $namaInisiatorTransaksi";
					break;
				case "KM":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "KnowledgeManagement/knowledgemanagementController/ApprovalAwalKnowledgeManagement";
					$keterangan = "Approval Knowledge Management $namaInisiatorTransaksi";
					break;
				case "KMDE":
					$requester = $npkInisiatorTransaksi;
					$params = "KnowledgeManagement/knowledgemanagementController/EditKM";
					$keterangan = "Edit Knowledge Management $namaInisiatorTransaksi";
					break;
				case "KMAP":
					$query = $this->db->get_where('mstruser',array('NPK'=>$requester));
					foreach($query->result() as $row)
					{
						$atasan = $row->NPKAtasan;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'AdminHRGA'));
					foreach($query->result() as $row)
					{
						$reportTo = $row->Value;
					}
					//$requester = $npkInisiatorTransaksi;
					$params = "KnowledgeManagement/knowledgemanagementController/ApprovalKnowledgeManagement";
					$keterangan = "Realisasi Knowledge Management $namaInisiatorTransaksi";
					break;
				case "AT":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "Atk/atkController/ApprovalRencanaAtk";
					$keterangan = "Approval ATK $namaInisiatorTransaksi";
					break;
				case "ATDE":
					$requester = $npkInisiatorTransaksi;
					$params = "Atk/atkController/EditATK";
					$keterangan = "Edit ATK $namaInisiatorTransaksi";
					break;
				case "ATAP":
					$query = $this->db->get_where('mstruser',array('NPK'=>$requester));
					foreach($query->result() as $row)
					{
						$atasan = $row->NPKAtasan;
					}
					$query = $this->db->get_where('mstruser',array('Jabatan'=>'6')); //head HRGA
					foreach($query->result() as $row)
					{
						$reportTo = $row->NPK;
					}
					$params = "Atk/AtkController/ApprovalRealisasiAtk";
					$keterangan = "Approval Realisasi ATK $namaInisiatorTransaksi";
					break;
				case "RM":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "MobilRuangMeeting/mobilRuangMeetingController/ApprovalMobilRuangMeeting";
					$keterangan = "Approval Mobil & R.Meeting $namaInisiatorTransaksi";
					break;
				case "TN":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "Training/trainingController/ApprovalAwalTraining";
					$keterangan = "Approval Kebutuhan Training $namaInisiatorTransaksi";
					break;
				case "TNDE":
					$requester = $npkInisiatorTransaksi;
					$params = "Training/trainingController/EditTraining";
					$keterangan = "Edit Training $namaInisiatorTransaksi";
					break;
				case "TNAP":
					$query = $this->db->get_where('mstruser',array('NPK'=>$requester));
					foreach($query->result() as $row)
					{
						$atasan = $row->NPKAtasan;
					}
					$query = $this->db->get_where('mstruser',array('Jabatan'=>'6')); //head HRGA
					foreach($query->result() as $row)
					{
						$reportTo = $row->NPK;
					}
					$params = "Training/trainingController/ApprovalTraining";
					$keterangan = "Approval Kebutuhan Training $namaInisiatorTransaksi";
					break;
				case "TS":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					$params = "Training/trainingController/ApprovalAwalUndangan";
					$keterangan = "Approval Undangan Training $namaInisiatorTransaksi";
					break;
				case "TSDE":
					$requester = $npkInisiatorTransaksi;
					$params = "Training/trainingController/EditUndangan";
					$keterangan = "Edit Undangan Training $namaInisiatorTransaksi";
					break;
				case "TSAP":
					$query = $this->db->get_where('mstruser',array('NPK'=>$requester));
					foreach($query->result() as $row)
					{
						$atasan = $row->NPKAtasan;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'AdminHRGA'));
					foreach($query->result() as $row)
					{
						$reportTo = $row->Value;
					}
					$params = "Training/trainingController/ApprovalUndangan";
					$keterangan = "Approval Realisasi Undangan $namaInisiatorTransaksi";

					break;
				case "MC": //MasterCuti
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadHRGA'));
					foreach($query->result() as $row)
					{
						$reportTo = $row->Value;
					}
					$query = $this->db->get_where('mstruser',array('NPK'=>$requester));
					foreach($query->result() as $row)
					{
						$namayangditambahjatahcuti = $row->Nama;
					}
					$params = "Cuti/cutiController/approvalMasterJatahCuti";
					$keterangan = "Approval Jatah Cuti $namayangditambahjatahcuti";
					$requester=$this->npkLogin;
					break;
				case "TB": //UploadTempBudget
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
										
					$requester = $npkInisiatorTransaksi;
					$params = "BudgetMonitoring/TempBudgetController/ApprovalTempBudget";
					$keterangan = "Approval Master Budget Dari $namaInisiatorTransaksi";
					break;
				case "AK": //AkomodasiTiket
					$query = $this->db->get_where('mstruser',array('NPK'=>$requester));
					foreach($query->result() as $row)
					{
						$atasan = $row->NPKAtasan;
					}
					$reportTo = $atasan;										
					$requester = $npkInisiatorTransaksi;
					$params = "UangMuka/AkomodasiTiketController/ApproveAkomodasiTiket" ;
					$keterangan = "Approval Akomodasi Tiket Dari $namaInisiatorTransaksi";
					break;
				case "AKAP":
					$requester = $npkInisiatorTransaksi;
					$query = $this->db->get_where('globalparam',array('Name'=>'AdminGA'));
					foreach($query->result() as $row)
					{
						$reportTo = $row->Value;
					}
					$params = "UangMuka/AkomodasiTiketController/CreateAkomodasiTiket";
					$keterangan = "Create Akomodasi Tiket $namaInisiatorTransaksi";
					break;
				case "EP":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "PROMISE/PROMISEController/ApprovalEditPROMISE";
					$keterangan = "Approval Edit PROMISE $namaInisiatorTransaksi";
					break;
				case "PR":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "PROMISE/PROMISEController/ApprovalNewPROMISE";
					$keterangan = "Approval New PROMISE $namaInisiatorTransaksi";
					break;
				case "PM":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "PROMISE/PROMISEController/ApprovalMOMPROMISE";
					$keterangan = "Approval MOM PROMISE $namaInisiatorTransaksi";
					break;	
				case "PA":
					$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
					$requester = $npkInisiatorTransaksi;
					$params = "PROMISE/PROMISEController/ParameterApproval";
					$keterangan = "Approval Parameter $namaInisiatorTransaksi";
					break;	
				case "FP":
					$CorpSec =$this->user->getSingleUserBasedJabatan("Corporate Secretary Leader")->NPK;
					if($this->npkLogin <> $CorpSec){
						$reportTo = $this->getUserSelanjutnya($requester,$jenisTask,$npkInisiatorTransaksi);
						$params = "PROMISE/PROMISEController/ApprovalFinalPROMISE";
						$keterangan = "Final Approval PROMISE $namaInisiatorTransaksi";
					}else{
						$reportTo = $requester;
						$params = "PROMISE/PROMISEController/UploadFinalDokumen";
						$keterangan = "Upload Final Dokumen PROMISE";
					}
					$requester = $npkInisiatorTransaksi;
					
					break;	
			}
			
			fire_print('log','reportTo='.$reportTo.',requester='.$requester);
			if($reportTo == '' || $requester == '')
			{
				//$this->db->trans_rollback();
				fire_print('log','usertask return false');
				return false;
			}
			else
			{
				$sql = "INSERT INTO usertasks(KodeUserTask, Deleted, Jenis, Username, StatusApproval, Requester, Params, Keterangan, CreatedOn, CreatedBy) VALUES 
				('$kodeUserTask',0,'$jenisTask','$reportTo','PE','$requester','$params','$keterangan', now(), '$requester')";
				$this->db->query($sql);

				$sql = "INSERT INTO dtltrkrwy(Deleted, NoTransaksi, KodeUserTask, CreatedOn, CreatedBy) VALUES 
				(0,'$noTransaksi','$kodeUserTask',now(),'$requester')";
				$this->db->query($sql);
				//$this->db->trans_commit();
				if(true)
				{
					if($this->config->item('enableEmail') == 'true'){
						$url = base_url() ."index.php/$params/$kodeUserTask";
						if ($jenisTask == "AK")
						$this->sendEmailAKToHead($reportTo,$jenisTask,$keterangan." ($kodeUserTask)",$requester,$url,$noTransaksi);
						else
						$this->sendEmail($reportTo,$jenisTask,$keterangan." ($kodeUserTask)",$requester,$url);
						
						if($jenisTask == "BPR1"){
							//message peminjaman bpkb
							$params = "PeminjamanDokumen/BPKBController/ListBPKB_User";
							$url = base_url() ."index.php/$params/$kodeUserTask";
							$this->sendEmailBPKB($jenisTask,$NPKtrx,$url,$noTransaksi);
						}
						if($jenisTask == "BPR2"){
							//message peminjaman bpkb
							$params = "PeminjamanDokumen/BPKBController/ListBPKB_User";
							$url = base_url() ."index.php/$params/$kodeUserTask";
							$this->sendEmailBPKB($jenisTask,$NPKtrx,$url,$noTransaksi);
						}
						if($jenisTask == "BPT1"){
							//message peminjaman bpkb
							$params = "PeminjamanDokumen/BPKBController/ListBPKB_User";
							$url = base_url() ."index.php/$params/$kodeUserTask";
							$this->sendEmailBPKB($jenisTask,$NPKtrx,$url,$noTransaksi);
						}
						if($jenisTask == "BPT2"){
							//message peminjaman bpkb
							$params = "PeminjamanDokumen/BPKBController/ListBPKB_User";
							$url = base_url() ."index.php/$params/$kodeUserTask";
							$this->sendEmailBPKB($jenisTask,$NPKtrx,$url,$noTransaksi);
						}
						if($jenisTask == "CT" ) // untuk test developer
						{							
							$params = "Cuti/CutiController/ViewRencanaCuti";
							$url = base_url() ."index.php/$params/$kodeUserTask";
							$this->sendEmailCuti("Notifikasi Pengajuan Cuti ",$NPKtrx,$url);
						}
						
						if(($jenisTask == "LBAP" || $jenisTask== "CTAP") && ($atasan != $reportTo))
						//if(($jenisTask == "LBAP" || $jenisTask== "CTAP")) // untuk test developer
						{
							//diminta dinonaktifkan oleh bu Ipung, sehingga tidak kirim email lagi ke Bu Ipung (URF/2017/II/06)
							//$this->sendEmailNotifikasi($atasan,$jenisTask,"Notifikasi Persetujuan ". ($jenisTask=="CTAP"?"Cuti":"Lembur") . " ",$NPKtrx);
						}
						
						if($jenisTask == 'TSAP')
							$this->sendEmailShare_TS($npkInisiatorTransaksi, $kodeUserTask);

						if($jenisTask == 'AKAP')
							$this->sendEmailNotifikasi($reportTo,$jenisTask,"Notifikasi Pengajuan Akomodasi & Tiket", $NPKtrx,$noTransaksi);
					}
					
					fire_print('log','usertask return true');
					return true;
				}
			}
		}
		catch(Exception $e)
		{
			echo $requester;
			echo $noTransaksi;
			echo $kodeUserTask;
			echo $jenisTask;
			$this->db->trans_rollback();

			die();
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);			
			return false;
		}
		
	}
	
	function isAlreadyMaxSequence($NPKLogin,$jenisTask)
	{
		$sequenceUser = '';
		try
		{
			$sqlquery = "select max(sequence) as maxSequence
				from approvalgroupuser agu 
				  join (select * from approvalmatrix where deleted = 0) am 
					on agu.KodeApprovalGroup = am.KodeApprovalGroup
				where agu.NPK = ? and am.JenisTask = ? and agu.Deleted = 0
				";
			$approvalgroupuserlist = $this->db->query($sqlquery,array($NPKLogin,$jenisTask));
			foreach($approvalgroupuserlist->result() as $row)
			{
				$maxSequence = $row->maxSequence;
			}
			
			$approvalmatrixlist = $this->db->get_where('approvalmatrix',array('NPK'=>$NPKLogin,'jenisTask'=>$jenisTask,'Deleted'=>'0'));
			foreach($approvalmatrixlist->result() as $row)
			{
				$sequenceUser = $row->Sequence;
			}
			if($sequenceUser == '')
			{
				throw new Exception('Sequence user kosong.');
			}
			fire_print('log',"maxSequence : $maxSequence, sequenceUser: $sequenceUser");
			if($maxSequence == $sequenceUser)
				return true;
			else
				return false;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);			
			return false;
		}
	}
	
	function getLastUserTask($NoTransaksi)
	{
		try
		{
			$sql = "select KodeUserTask 
				from dtltrkrwy 
				where NoTransaksi = ?
				order by KodeUserTask DESC Limit 1";
			$query = $this->db->query($sql,array($NoTransaksi));
			
			if($query->num_rows() == 1)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function getLastUserTaskValue($NoTransaksi)
	{
		try
		{
			$sql = "select dtr.KodeUserTask, ut.Username, u.Nama
				from dtltrkrwy dtr 
				  join usertasks ut on dtr.KodeUserTask = ut.KodeUserTask
				  join mstruser u on ut.Username = u.NPK
						where NoTransaksi = ?
						order by dtr.KodeUserTask DESC Limit 1";
			$query = $this->db->query($sql,array($NoTransaksi));
			
			if($query->num_rows() == 1)
			{
				foreach($query->result() as $dt){
					$data = array("KodeUserTask"=>$dt->KodeUserTask,
						"NPK"=>$dt->Username,
						"Nama"=>$dt->Nama);
				}
				return $data;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function getUserSelanjutnya($NPKLogin,$jenisTask,$NPKCreatorTask)
	{
		//get kode approval group
		$sqlquery = "SELECT agu.KodeApprovalGroup, am.NPK
			from approvalgroupuser agu 
			  join (select * from approvalmatrix where deleted = 0) am 
				on agu.KodeApprovalGroup = am.KodeApprovalGroup
			  join (select * from approvalgroup where deleted = 0 ) ag
				on ag.KodeApprovalGroup = agu.KodeApprovalGroup
			  join (select * from approvalgroupuser where deleted = 0 and NPK = ?) agu2
        		on agu2.KodeApprovalGroup = agu.KodeApprovalGroup
			where agu.NPK = ? and am.JenisTask = ? and agu.Deleted = 0
			";
		$KodeApprovalGroup = '';
		$nextuser = '';
		$approvalgroupuserlist = $this->db->query($sqlquery,array($NPKCreatorTask,$NPKLogin,$jenisTask));
		
		if($approvalgroupuserlist->num_rows() > 1)
		{
			foreach($approvalgroupuserlist->result() as $row)
			{
				$NPKApprover = $row->NPK;
				if($NPKApprover != $NPKCreatorTask) //pastikan group yang dipilih bukanlah group yang user yg mengajukan menjadi approver pada matrix
				{
					$KodeApprovalGroup = $row->KodeApprovalGroup;
				}
			}

			if($jenisTask == 'CT' && $KodeApprovalGroup == '')
			{
				//jika jenis task cuti dan yang mengajukan adalah head HRGA maka masuk ke group approval cuti head. Hal ini diperlukan karena Head HRGA melakukan approval atas cutinya sendiri
				$mstruserlist = $this->db->get_where('mstruser',array('NPK'=>$NPKLogin));
				foreach($mstruserlist->result() as $dtuser)
				{
					$KodeJabatan = $dtuser->Jabatan;						
				}
				if($KodeJabatan == 6) //jika jabatannya HRGA Dept Head
				{
					$KodeApprovalGroup = '16';
				}
			}
		}
		else
		{
			foreach($approvalgroupuserlist->result() as $row)
			{
				$KodeApprovalGroup = $row->KodeApprovalGroup;
			}
		}
		
		if($KodeApprovalGroup != '')
		{
			//dapatkan approval matrix
			$approvalmatrixlist = $this->db->get_where('approvalmatrix',array('KodeApprovalGroup'=>$KodeApprovalGroup,'NPK'=>$NPKLogin,'Deleted'=>'0','JenisTask'=>$jenisTask));
			fire_print('log','dapatkan approval matrix. KodeApprovalGroup:'.$KodeApprovalGroup.',NPK:'.$NPKLogin.',JenisTask:'.$jenisTask);
			if($approvalmatrixlist->num_rows() == 0)
			{
				$appmatrixlist = $this->db->get_where('approvalmatrix',array('KodeApprovalGroup'=>$KodeApprovalGroup,'Sequence'=>'1','Deleted'=>'0','JenisTask'=>$jenisTask));
				foreach($appmatrixlist->result() as $row)
				{
					$nextuser = $row->NPK;
				}
			}
			else
			{
				foreach($approvalmatrixlist->result() as $rowapp)
				{
					$sequenceSkrg = $rowapp->Sequence;
				}
				$appmatrixlist = $this->db->get_where('approvalmatrix',array('KodeApprovalGroup'=>$KodeApprovalGroup,'Sequence'=>$sequenceSkrg+1,'Deleted'=>'0','JenisTask'=>$jenisTask));
				
				if ($appmatrixlist->num_rows() > 0)
				{
					foreach($appmatrixlist->result() as $row)
					{
						$nextuser = $row->NPK;
					}
				}
				else
				{
					//jika tidak ada lagi sequence nya maka naik ke atasan dari npk Login.
					$dataUserList = $this->db->get_where('mstruser',array("NPK"=>$NPKLogin));
					$NPKAtasanUserLogin = $dataUserList->row(1)->NPKAtasan;
					$nextuser = $NPKAtasanUserLogin;

					if($jenisTask == 'CT' && $KodeApprovalGroup == '16')
					{
						//jika jenis task cuti dan yang mengajukan adalah head HRGA maka masuk ke group approval cuti head. Hal ini diperlukan karena Head HRGA melakukan approval atas cutinya sendiri
						$mstruserlist = $this->db->get_where('mstruser',array('NPK'=>$NPKLogin));
						foreach($mstruserlist->result() as $dtuser)
						{
							$KodeJabatan = $dtuser->Jabatan;						
						}
						if($KodeJabatan == 6) //jika jabatannya HRGA Dept Head
						{
							$nextuser = $NPKLogin;
						}
					}
					if($jenisTask == 'BP' && $KodeApprovalGroup == '24')
					{
						//jika jenis task bpkb dan yang mengajukan adalah head HRGA maka masuk ke group approval bpkb head. Hal ini diperlukan karena Head HRGA melakukan approval atas bpkbnya sendiri
						$mstruserlist = $this->db->get_where('mstruser',array('NPK'=>$NPKLogin));
						foreach($mstruserlist->result() as $dtuser)
						{
							$KodeJabatan = $dtuser->Jabatan;						
						}
						if($KodeJabatan == 6) //jika jabatannya HRGA Dept Head
						{
							$nextuser = $NPKLogin;
						}
					}
				}
			}
			return $nextuser;
		}
		else
		{
			return '';
		}
	}
	
	function getCatatan($NoTransaksi)
	{
		
		try
		{
			$sql = "
				select CONCAT(date_format(ApprovalDate,'%Y-%m-%d %T'),', ',mu.Nama,', ',Catatan) as catatan
				from usertasks ut 
					join mstruser mu on ut.UpdatedBy = mu.NPK
					join dtltrkrwy dw on ut.KodeUserTask = dw.KodeUserTask
				where dw.NoTransaksi = ?
				and ut.ApprovalDate is not null AND ut.Catatan != ''
				order by ut.KodeUserTask DESC";
			$query = $this->db->query($sql,array($NoTransaksi));
			
			if($query->num_rows() > 0)
			{
				$catatan = '';
				foreach($query->result() as $row)
				{
					$catatan .= $row->catatan ."<br/>";
				}
				return $catatan;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function updateStatusUserTask($KodeUserTask,$StatusApp,$NPK,$catatan='')
	{
		try
		{
			$this->db->where('KodeUserTask',$KodeUserTask);
			return $this->db->update('usertasks',array(
				'StatusApproval' => $StatusApp,
				'ApprovalDate' => date('Y-m-d H:i:s'),
				'Catatan' => $catatan,
				'UpdatedOn' => date('Y-m-d H:i:s'),
				'UpdatedBy' => $NPK
			));
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function getTempPeserta($KodeUserTask)
	{
		try
		{
			$sql = "select * 
				from tempmstruser tu join dtltrkrwy rw on tu.KodeTempMstrUser = rw.NoTransaksi
				where rw.KodeUserTask = ?";
			$query = $this->db->query($sql,array($KodeUserTask));
			
			if($query->num_rows() == 1)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function sendEmailToHRD()
	{
		try
		{
			$query = $this->db->get_where('globalparam',array('Name'=>'EmailHRGA'));
			foreach($query->result() as $row)
			{
				$to = $row->Value;
			}
			
			$this->load->library('email');
			$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
			$this->email->to($to); 

			$this->email->subject('Rencana Lembur Karyawan');
			$message = "Terdapat karyawan yang akan melakukan lembur mohon review di ESS.";
			$this->email->message($message);	

			if( ! $this->email->send())
			{	
				return false;
			}
			else
			{
				return true;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function sendEmail($target,$jenisTask,$keterangan,$requester,$url)
	{
		try
		{
			$query = $this->db->get_where('mstruser',array('NPK'=>$target));
			foreach($query->result() as $row)
			{
				$to = $row->EmailInternal;
			}
			
			$query2= $this->db->get_where('mstruser',array('NPK'=>$requester));
			$namaRequester = '';
			foreach($query2->result() as $row)
			{
				$namaRequester = $row->Nama;
			}
			
			$this->load->library('email');
			$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
			$this->email->to($to); 
			$this->email->subject($keterangan);
			$message = "Terdapat tugas saya yang baru pada ESS berasal dari $namaRequester. Mohon di review.
			Tugas saya : $keterangan 
			$url
			";
			$this->email->message($message);

			if( ! $this->email->send())
			{	
				return false;
			}
			else
			{
				return true;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function sendEmailNotifikasi($target,$jenisTask,$subject,$requester,$noTransaksi)
	{
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				$query = $this->db->get_where('mstruser',array('NPK'=>$target));
				foreach($query->result() as $row)
				{
					$to = $row->EmailInternal;
				}
				
				$query2= $this->db->get_where('mstruser',array('NPK'=>$requester));
				$namaRequester = '';
				foreach($query2->result() as $row)
				{
					$namaRequester = $row->Nama;
				}
				
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				$this->email->to($to); 

				$this->email->subject($subject);
				switch($jenisTask)
				{
					case "CTAP":
					case "LBAP":
						$url = base_url(). ($jenisTask=="CTAP"?"Cuti/CutiController/ViewCutiUser":"Lembur/LemburController/ViewLemburUser");
						$message = "Pengajuan ". ($jenisTask=="CTAP"?"Cuti":"Lembur") ." oleh $namaRequester sudah disetujui. Untuk selengkapnya dapat dilihat pada aplikasi Enterprise Self Service.
$url";
						break;
					case "BEAP":
					$message = "Pengajuan Medical oleh $namaRequester sudah disetujui. Untuk selengkapnya dapat dilihat pada aplikasi Enterprise Self Service.
						".base_url()."Benefit/BenefitController/ViewUser";
						break;
					case "AKAP":
						$message = "Pengajuan Akomodasi & Tiket oleh $namaRequester sudah disetujui, silahkan diproses Pengajuan Uang Muka pada aplikasi Enterprise Self Service.
						".base_url()."UangMuka/LihatAkomodasiTiket/KodeAkomodasiTiket=".$noTransaksi;
						break;
				}
				
				$this->email->message($message);	

				if( ! $this->email->send())
				{	
					return false;
				}
				else
				{
					return true;
				}
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function sendEmailShare_TS($target, $KodeUserTask)
	{
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				$query = $this->db->get_where('mstruser',array('NPK'=>$target));
				foreach($query->result() as $row)
				{
					$to = $row->EmailInternal;
				}

				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				$this->email->to($to); 
				$data = $this->undangan_model->getUndangan($KodeUserTask);
				if ($data){
					foreach($data as $row){
						date_default_timezone_set('UTC');
						$penyelenggara = $row->Penyelenggara;
						$programtraining = $row->ProgramTraining;
						$dateStart = new DateTime($row->TanggalMulai);
						$dateEnd = new DateTime($row->TanggalSelesai);
						$tanggalmulai = $dateStart->format('j M Y');
						$tanggalselesai = $dateEnd->format('j M Y');
						if($tanggalmulai == $tanggalselesai){
							$tanggal = $tanggalmulai;
						}else{
							$tanggal = $tanggalmulai." s/d ".$tanggalselesai;
						}
						$pukul = $dateStart->format('H:i') ." - ". $dateEnd->format('H:i');
						$alamattraining = $row->AlamatTraining;
					}
					
					$this->email->subject('Undangan Training '.$row->ProgramTraining);
					$message =
						"Kepada Yth.
Bapak / Ibu / Sdr / i
Di Tempat

Perihal : Surat Undangan Training $programtraining

Dengan hormat,

Bersama ini kami menyampaikan surat undangan dan menginformasikan bahwa akan mengadakan training $programtraining.
Informasi program training, penyelenggara, waktu dan tempat pelaksanaan dapat dilihat sebagai berikut :
	Program Training  = $programtraining
	Tanggal = $tanggal
	Pukul = $pukul
	Tempat =  $alamattraining
	Penyelenggara =  $penyelenggara
	

Semoga surat undangan training ini dapat diterima dengan baik dan menjadi referensi dalam upaya peningkatan
kualitas SDM. Demikian yang dapat kami sampaikan. Atas perhatiannya, kami ucapkan terimakasih.

Salam Hormat";
					
					fire_print ('log',"isi message email : ".$message);
					$this->email->message($message);	
				}
				if( ! $this->email->send())
				{	
					return false;
				}
				else
				{
					return true;
				}
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}
	
	function sendEmailAKToHead($target,$jenisTask,$keterangan,$requester,$url,$NoTransaksi)
	{
		try
		{
			$query = $this->db->get_where('mstruser',array('NPK'=>$target));
			foreach($query->result() as $row)
			{
				$to = $row->EmailInternal;
			}
			
			$query2= $this->db->get_where('mstruser',array('NPK'=>$requester));
			$namaRequester = '';
			foreach($query2->result() as $row)
			{
				$namaRequester = $row->Nama;
			}

			$query3= $this->db->get_where('akomodasitiket',array('KodeAkomodasiTiket'=>$NoTransaksi));
			$Alasan = '';$Tujuan = '';
			foreach($query3->result() as $row)
			{
				$Alasan = $row->Alasan;
				$Tujuan = $row->Tujuan;
				$TanggalBerangkat = $row -> TanggalBerangkat;
				$TanggalKembali = $row -> TanggalKembali;
			}

			$KeteranganFull='';
			$KeteranganFull= 'Akomodasi & Tiket'.$Alasan.' '.$Tujuan.' a/n '.$namaRequester. '<br>Tanggal Berangkat : '.$TanggalBerangkat.
							 '<br>TanggalKembali :'.$TanggalKembali ;
			
			$this->load->library('email');
			$this->email->from('ess@dpa.co.id', 'Employee Self Service');
			$this->email->to($to); 
			$this->email->subject($keterangan);
			$message = "No Transaksi : ".$NoTransaksi."<br/>Keterangan : ".$KeteranganFull."<br/> Klik link di bawah ini untuk melakukan Approve/Decline <br/> ".$url;
			
			$this->email->message($message);

			if( ! $this->email->send())
			{	
				return false;
			}
			else
			{
				return true;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function sendEmailCuti($subject,$requester,$url)
	{
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				$query = $this->db->get_where('globalparam',array('Name'=>'EmailHeadHRGA'));
				foreach($query->result() as $row)
				{
					$to = $row->Value;
				}
				
				$query2= $this->db->get_where('mstruser',array('NPK'=>$requester));
				$namaRequester = '';
				foreach($query2->result() as $row)
				{
					$namaRequester = $row->Nama;
				}
				
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				$this->email->to($to); 

				$this->email->subject($subject);
				//$params = "Cuti/CutiController/ApprovalRencanaCuti";
				//$url = base_url() ."index.php/$params/$kodeUserTask";
				//$url = base_url(). ($jenisTask=="CTAP"?"Cuti/CutiController/ViewCutiUser":"Lembur/LemburController/ViewLemburUser");
				$message = "Ada Pengajuan Cuti oleh $namaRequester . Untuk selengkapnya dapat dilihat pada aplikasi Enterprise Self Service.
				$url";
				$this->email->message($message);	

				if( ! $this->email->send())
				{	
					return false;
				}
				else
				{
					return true;
				}
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	function sendEmailBPKB($subject,$requester,$url, $notransaksi = '')
	{
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				$query2= $this->db->get_where('mstruser',array('NPK'=>$requester));
				$namaRequester = '';
				$emailRequester = '';
				foreach($query2->result() as $row)
				{
					$namaRequester = $row->Nama;
					$emailRequester = $row->EmailInternal;
				}
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				$this->email->to($emailRequester);
				
				if($subject == 'BPR1'){ 
					$subject = 'Informasi Peminjaman BPKB Proses Approval Atasan';
					$message = 'Dear '.$namaRequester.',<br/><br/>Peminjaman BPKB dengan <b>Nomor: '.$notransaksi.' </b> sedang pada proses Approval Atasan, harap menghubungi Atasan anda untuk menginformasikan lebih lanjut. <br/><br/>Terima Kasih<br/> ESS';
				} 
				if($subject == 'BPR2'){ 
					$subject = 'Informasi Peminjaman BPKB Proses Approval HR';
					$message = 'Dear '.$namaRequester.',<br/><br/>Peminjaman BPKB dengan <b>Nomor: '.$notransaksi.' </b> sedang pada proses Approval HR, harap menghubungi HR anda untuk menginformasikan lebih lanjut. <br/><br/>Terima Kasih<br/> ESS';
				} 
				if($subject == 'BPT1'){ 
					$subject = 'Informasi Pengambilan BPKB Proses Approval Atasan';
					$message = 'Dear '.$namaRequester.',<br/><br/>Pengambilan BPKB dengan <b>Nomor: '.$notransaksi.' </b> sedang pada proses Approval Atasan, harap menghubungi Atasan anda untuk menginformasikan lebih lanjut. <br/><br/>Terima Kasih<br/> ESS';
				} 
				if($subject == 'BPT2'){ 
					$subject = 'Informasi Pengambilan BPKB Proses Approval HR';
					$message = 'Dear '.$namaRequester.',<br/><br/>Pengambilan BPKB dengan <b>Nomor: '.$notransaksi.' </b> sedang pada proses Approval HR, harap menghubungi HR anda untuk menginformasikan lebih lanjut. <br/><br/>Terima Kasih<br/> ESS';
				}

				if($subject == 'DEBPR1'){ 
					$subject = 'Informasi Peminjaman BPKB Declined by Atasan';
					$message = 'Dear '.$namaRequester.',<br/><br/>Peminjaman BPKB dengan <b>Nomor: '.$notransaksi.' </b> tidak dapat diterima oleh Atasan, harap menghubungi Atasan anda untuk informasi lebih lanjut. <br/><br/>Terima Kasih<br/> ESS';
				} 
				if($subject == 'DEBPR2'){ 
					$subject = 'Informasi Peminjaman BPKB Declined by HR';
					$message = 'Dear '.$namaRequester.',<br/><br/>Peminjaman BPKB dengan <b>Nomor: '.$notransaksi.' </b> tidak dapat diterima oleh HR, harap menghubungi HR anda untuk informasi lebih lanjut. <br/><br/>Terima Kasih<br/> ESS';
				} 
				if($subject == 'DEBPT1'){ 
					$subject = 'Informasi Pengambilan BPKB Declined by Atasan';
					$message = 'Dear '.$namaRequester.',<br/><br/>Pengambilan BPKB dengan <b>Nomor: '.$notransaksi.' </b> tidak dapat diterima oleh Atasan, harap menghubungi Atasan anda untuk informasi lebih lanjut. <br/><br/>Terima Kasih<br/> ESS';
				} 
				if($subject == 'DEBPT2'){ 
					$subject = 'Informasi Pengambilan BPKB Declined by HR';
					$message = 'Dear '.$namaRequester.',<br/><br/>Pengambilan BPKB dengan <b>Nomor: '.$notransaksi.' </b> tidak dapat diterima oleh HR, harap menghubungi HR anda untuk informasi lebih lanjut. <br/><br/>Terima Kasih<br/> ESS';
				}


				if($subject == 'APBPR1'){ 
					$subject = 'Informasi Peminjaman BPKB Approved by Atasan';
					$message = 'Dear '.$namaRequester.',<br/><br/>Peminjaman BPKB dengan <b>Nomor: '.$notransaksi.' </b> telah disetujui oleh Atasan. <br/><br/>Terima Kasih<br/> ESS';
				} 
				if($subject == 'APBPR2'){ 
					$subject = 'Informasi Peminjaman BPKB Approved by HR';
					$message = 'Dear '.$namaRequester.',<br/><br/>Peminjaman BPKB dengan <b>Nomor: '.$notransaksi.' </b> telah disetujui oleh HR. <br/><br/>Terima Kasih<br/> ESS';
				} 
				if($subject == 'APBPT1'){ 
					$subject = 'Informasi Pengambilan BPKB Approved by Atasan';
					$message = 'Dear '.$namaRequester.',<br/><br/>Pengambilan BPKB dengan <b>Nomor: '.$notransaksi.' </b> telah disetujui oleh Atasan. <br/><br/>Terima Kasih<br/> ESS';
				} 
				if($subject == 'APBPT2'){ 
					$subject = 'Informasi Pengambilan BPKB Approved by HR';
					$message = 'Dear '.$namaRequester.',<br/><br/>Pengambilan BPKB dengan <b>Nomor: '.$notransaksi.' </b> telah disetujui oleh HR. <br/><br/>Terima Kasih<br/> ESS';
				}
				if($subject == 'TBPR'){ 
					$subject = 'Informasi Konfirmasi Pengembalian BPKB';
					$message = 'Dear '.$namaRequester.',<br/><br/>Pengembalian BPKB dengan <b>Nomor: '.$notransaksi.' </b> telah dikonfirmasi kembali oleh HR. <br/><br/>Terima Kasih<br/> ESS';
				}
				$this->email->subject($subject);
				$this->email->message($message);	

				if( ! $this->email->send())
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	function sendEmailBPKBToAct($subject,$requester,$url, $notransaksi = '')
	{
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				$query2= $this->db->get_where('mstruser',array('NPK'=>$requester));
				$namaRequester = '';
				$emailRequester = '';
				foreach($query2->result() as $row)
				{
					$namaRequester = $row->Nama;
				}
				$dataTrk = $this->peminjamandokumen_model->getTrkBPKBbyNoTransaksi($notransaksi);
				foreach($dataTrk as $trk){
					$this->load->library('email');
					$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
					$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
					foreach ($accountingTeam as $accountingUser) {
						$recipient = $accountingUser->EmailInternal;
						$this->email->to($recipient);
						
						if($subject == 'APBPR2' || $subject == 'Informasi Peminjaman BPKB Approved by HR'){ 
							$subject = 'Informasi Peminjaman BPKB Approved by HR';
							$message = 'Dear '.$accountingUser->Nama.',<br/>Peminjaman BPKB dengan <b>Nomor: '.$notransaksi.' </b> sudah diterima oleh HR.<br/>
							Detail BPKB yang di Pinjam antara lain:<br/>
							<ul>
								<li>NPK: '.$trk->NPK.' </li>
								<li>Nama: '.$trk->Nama.' </li>
								<li>Nomor Polisi: '.$trk->NoPolisi.' </li>
								<li>Nomor BPKB: '.$trk->NoBPKB.' </li>
								<li>Tanggal Peminjaman: '.$trk->TanggalTransaksi.' </li>
								<li>Tanggal Pengembalian: '.$trk->TanggalPengembalian.' </li>
							</ul>
							Terima Kasih<br/> ESS';
						} 
						if($subject == 'APBPT2' || $subject == 'Informasi Pengambilan BPKB Approved by HR'){ 
							$subject = 'Informasi Pengambilan BPKB Approved by HR';
							$message = 'Dear '.$accountingUser->Nama.',<br/>Pengambilan BPKB dengan <b>Nomor: '.$notransaksi.' </b> sudah diterima oleh HR.<br/>
							Detail BPKB yang di Ambil antara lain:<br/>
							<ul>
								<li>NPK: '.$trk->NPK.' </li>
								<li>Nama: '.$trk->Nama.' </li>
								<li>Nomor Polisi: '.$trk->NoPolisi.' </li>
								<li>Nomor BPKB: '.$trk->NoBPKB.' </li>
								<li>Tanggal Peminjaman: '.$trk->TanggalTransaksi.' </li>
								<li>Tanggal Pengambilan: '.$trk->TanggalPengambilan.' </li>
							</ul>
							Terima Kasih<br/> ESS';
						}
						if($subject == 'TBPR'){
							$subject = 'Informasi Penerimaan BPKB';
							$message = 'Dear '.$accountingUser->Nama.',<br/>BPKB dengan <b>Nomor: '.$notransaksi.' </b> sudah dikonfirmasi terima oleh '.$trk->Nama.'.<br/>
							Detail BPKB yang di terima antara lain:<br/>
							<ul>
								<li>NPK: '.$trk->NPK.' </li>
								<li>Nama: '.$trk->Nama.' </li>
								<li>Nomor Polisi: '.$trk->NoPolisi.' </li>
								<li>Nomor BPKB: '.$trk->NoBPKB.' </li>
								<li>Tanggal Peminjaman: '.$trk->TanggalTransaksi.' </li>
								<li>Tanggal Pengambilan: '.$trk->TanggalPengambilan.' </li>
							</ul>
							Terima Kasih<br/> ESS';
						}
						if($subject == 'TBPT'){
							$subject = 'Informasi Pengambilan BPKB';
							$message = 'Dear '.$accountingUser->Nama.',<br/>BPKB dengan <b>Nomor: '.$notransaksi.' </b> sudah dikonfirmasi terima oleh '.$trk->Nama.'.<br/>
							Detail BPKB yang di terima antara lain:<br/>
							<ul>
								<li>NPK: '.$trk->NPK.' </li>
								<li>Nama: '.$trk->Nama.' </li>
								<li>Nomor Polisi: '.$trk->NoPolisi.' </li>
								<li>Nomor BPKB: '.$trk->NoBPKB.' </li>
								<li>Tanggal Peminjaman: '.$trk->TanggalTransaksi.' </li>
								<li>Tanggal Pengambilan: '.$trk->TanggalPengambilan.' </li>
							</ul>
							Terima Kasih<br/> ESS';
						}
						$this->email->subject($subject);
						$this->email->message($message);
						if( ! $this->email->send())
						{
							log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
							throw new Exception( 'Something really gone wrong', 0, $e);
							$this->db->trans_rollback();
							return false;
						}
					}
				}
				
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	function sendEmailBPKBToHR($subject,$requester,$url, $notransaksi = '')
	{
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				$query2= $this->db->get_where('mstruser',array('NPK'=>$requester));
				$namaRequester = '';
				$emailRequester = '';
				foreach($query2->result() as $row)
				{
					$namaRequester = $row->Nama;
				}
				$dataTrk = $this->peminjamandokumen_model->getTrkBPKBbyNoTransaksi($notransaksi);
				foreach($dataTrk as $trk){
					$this->load->library('email');
					$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
					$hrTeam = $this->user->getAllUserByKodeDepartemen(2);
					foreach ($hrTeam as $hrUser) {
						$recipient = $hrUser->EmailInternal;
						$this->email->to($recipient);
						if($subject == 'TBPR'){
							$subject = 'Informasi Penerimaan BPKB';
							$message = 'Dear '.$hrUser->Nama.',<br/>BPKB dengan <b>Nomor: '.$notransaksi.' </b> sudah dikonfirmasi terima oleh '.$trk->Nama.'.<br/>
							Detail BPKB yang di terima antara lain:<br/>
							<ul>
								<li>NPK: '.$trk->NPK.' </li>
								<li>Nama: '.$trk->Nama.' </li>
								<li>Nomor Polisi: '.$trk->NoPolisi.' </li>
								<li>Nomor BPKB: '.$trk->NoBPKB.' </li>
								<li>Tanggal Peminjaman: '.$trk->TanggalTransaksi.' </li>
								<li>Tanggal Pengambilan: '.$trk->TanggalPengambilan.' </li>
							</ul>
							Terima Kasih<br/> ESS';
						}
						if($subject == 'TBPT'){
							$subject = 'Informasi Pengambilan BPKB';
							$message = 'Dear '.$hrUser->Nama.',<br/>BPKB dengan <b>Nomor: '.$notransaksi.' </b> sudah dikonfirmasi terima oleh '.$trk->Nama.'.<br/>
							Detail BPKB yang di terima antara lain:<br/>
							<ul>
								<li>NPK: '.$trk->NPK.' </li>
								<li>Nama: '.$trk->Nama.' </li>
								<li>Nomor Polisi: '.$trk->NoPolisi.' </li>
								<li>Nomor BPKB: '.$trk->NoBPKB.' </li>
								<li>Tanggal Peminjaman: '.$trk->TanggalTransaksi.' </li>
								<li>Tanggal Pengambilan: '.$trk->TanggalPengambilan.' </li>
							</ul>
							Terima Kasih<br/> ESS';

						}
						$this->email->subject($subject);
						$this->email->message($message);
						if( ! $this->email->send())
						{
							log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
							throw new Exception( 'Something really gone wrong', 0, $e);
							return false;
						}
					}
				}
				
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	function sendEmailNotifAT($StatusAPP,$requester,$url, $notransaksi = '')
	{
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				$query2= $this->db->get_where('mstruser',array('NPK'=>$requester));
				$namaRequester = '';
				$emailRequester = '';
				foreach($query2->result() as $row)
				{
					$namaRequester = $row->Nama;
					$emailRequester = $row->EmailInternal;
				}
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Employee Self Service');
				$this->email->to($emailRequester);
				
				

				if($StatusAPP == 'DE'){ 
					$subject = 'User Task Permohonan Akomodasi&Tiket '.$namaRequester.' Yang Sudah di Decline Atasan';
					$message = 'Dear '.$namaRequester.',<br/><br/>Permohonan Akomodasi&Tiket dengan <b>Nomor: '.$notransaksi.' </b> tidak dapat diterima oleh Atasan, harap menghubungi Atasan anda untuk informasi lebih lanjut. <br/><br/>Terima Kasih<br/> ESS';
				} 
				

				if($StatusAPP == 'AP'){ 
					$subject = 'User Task Permohonan Akomodasi&Tiket '.$namaRequester.' Yang Sudah di Approve Atasan';
					$message = 'Dear '.$namaRequester.',<br/><br/>Permohonan Akomodasi&Tiket dengan <b>Nomor: '.$notransaksi.' </b> telah disetujui oleh Atasan. <br/><br/>Terima Kasih<br/> ESS';
				} 

				if($StatusAPP == 'APTOHRGA'){ 
					$subject = 'User Task Permohonan Akomodasi&Tiket '.$namaRequester.' Yang Sudah di Approve Atasan';
					$message = 'Dear '.$namaRequester.',<br/><br/>Permohonan Akomodasi&Tiket dengan <b>Nomor: '.$notransaksi.' </b> telah disetujui oleh Atasan. <br/><br/>Terima Kasih<br/> ESS';
				} 
				
				$this->email->subject($subject);
				$this->email->message($message);	

				if( ! $this->email->send())
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	function sendEmailAToGA($subject,$requester,$url, $notransaksi = '')
	{
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				$query2= $this->db->get_where('mstruser',array('NPK'=>$requester));
				$namaRequester = '';
				$emailRequester = '';
				foreach($query2->result() as $row)
				{
					$namaRequester = $row->Nama;
				}
				$trxAT = $this->akomodasitiket_model->getTrxATbyKode($notransaksi);
				
				$query = $this->db->get_where('globalparam',array('Name'=>'EmailHRGA'));
				foreach($query->result() as $row)
				{
					$to = $row->Value;
				}
				
				foreach($trxAT as $trk){
					$this->load->library('email');
					$this->email->from('ess@dpa.co.id', 'Employee Self Service');
					
				
						$recipient = $to;
						$this->email->to($recipient);
						if($subject == 'TBPR'){
							$subject = 'Informasi Penerimaan BPKB';
							$message = 'Dear HRGA,<br/>Permohonan Akomodasi Tiket dengan <b>Nomor: '.$notransaksi.' </b> sudah di Approve<br/>
							Detail Permohonan Akomodasi Tiket :<br/>
							<ul>
								<li>NPK: '.$trk->NPK.' </li>
								<li>Nama: '.$namaRequester.' </li>
								<li>Tanggal Berangkat: '.$trk->TanggalBerangkat.' </li>
								<li>Tanggal Kembali: '.$trk->TanggalBerangkat.' </li>
							</ul>
							<br>
							Mohon di proses untuk uang muka melalui link ini : '.$url.'
							<br>
							Terima Kasih<br/> ESS';
						}
						
						$this->email->subject($subject);
						$this->email->message($message);
						if( ! $this->email->send())
						{
							log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
							throw new Exception( 'Something really gone wrong', 0, $e);
							return false;
						}
					
				}
				
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

}
?>