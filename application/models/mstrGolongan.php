<?php 
Class MstrGolongan extends Ci_Model
{
	function getGolongan($gol)
	{	
		if($gol == ""){
			$sql = "select Golongan, Deleted, UangSaku, UangMakan, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy from mstrgolongan
				where deleted = 0";
		}else{
			$sql = "select Golongan, Deleted, UangSaku, UangMakan, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy from mstrgolongan 
				where deleted = 0 and Golongan = ? ";
		}
		$query = $this->db->query($sql, $gol);
		
		//$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>