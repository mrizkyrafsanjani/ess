<?php 
Class Knowledgemanagement_model extends Ci_Model
{
	function getKodeArtikel($npk)
	{	
		$sql = "select KodeArtikel from artikel
		where CreatedBy = ?
		ORDER BY CreatedOn DESC LIMIT 1";
		
		$query = $this->db->query($sql,array($npk));
		//fire_print('log','ini query '.$npk);
		
		if($query->num_rows() > 0){
			
			$KodeArtikel = "";
			foreach($query->result() as $row)
			{
				$KodeArtikel = $row->KodeArtikel;
			}
			return $KodeArtikel;
		}else{
			return false;
		}
	}
	
	function getByTransaksiArtikel($KodeArtikel)
	{
		try
		{
			$sql = "select KodeArtikel,Artikel,Sumber,Status, artikel.CreatedBy 
				from artikel join mstruser u on NPK = u.NPK
				where KodeArtikel = ?";
			
			
			fire_print('log','KodeArtikel proses getByTransaksiArtikel='.$KodeArtikel);
			$query = $this->db->query($sql,array($KodeArtikel));
			
			if($query->num_rows() > 0)
			{
				return $query->result();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
			return false;
		}
	}
	
	function getArtikel($KodeUserTask)
	{
		$sql = "select KodeArtikel, Judul, Sumber, Artikel, Status, artikel.CreatedBy
			from artikel  
			  join dtltrkrwy rw on KodeArtikel = rw.NoTransaksi
			  join mstruser u on NPK = u.NPK			  
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($KodeUserTask));
		//fire_print('log','masuk disini'.$KodeUserTask);
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getTotalArtikel($Judul,$Artikel)
	{
		$sql = "select KodeArtikel,Judul,Artikel,Sumber,Status, artikel.CreatedBy 
				from artikel
				join mstruser u on NPK = u.NPK
				where KodeArtikel = ?";
		
		$query = $this->db->query($sql,array($Judul,$Artikel));
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	
	function updateStatusKM($KodeUserTask, $status, $updatedby)
	{
		try{
			$sql = "update artikel l
				join dtltrkrwy rw on l.KodeArtikel = rw.NoTransaksi
				set
					l.Status = ?,
					l.UpdatedOn = now(),
					l.UpdatedBy = ?
				where rw.KodeUserTask = ?";
				
			$query = $this->db->query($sql,array($status,$updatedby,$KodeUserTask));
			if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function getEdit($npk)
	{	
		$sql = "select KodeArtikel from artikel  
			  join dtltrkrwy rw on KodeArtikel = rw.NoTransaksi	  
			where rw.KodeUserTask = ?";
		
		$query = $this->db->query($sql,array($npk));
		fire_print('log','ini query '.$npk);
		
		if($query->num_rows() > 0){
			
			$KodeArtikel = "";
			foreach($query->result() as $row)
			{
				
				$KodeArtikel = $row->KodeArtikel;
				fire_print('log','ini kode artikel '.$KodeArtikel);
			}
			return $KodeArtikel;
		}else{
			return false;
		}
	}
	
	function updaterwynpkartikelendtime($NPKLogin,$KodeArtikel)
	{	
		fire_print('log',"tes masuk updaterwynpkartikel $NPKLogin,$KodeArtikel");
		
		$sql = "update rwynpkartikel
				set   EndTime = now()
				where KodeRwyNpkArtikel = ?";
		
		$KodeRwyNpkArtikel = $this->getKodeRwyNpkArtikel($NPKLogin,$KodeArtikel);
		
		$this->db->query($sql,array($KodeRwyNpkArtikel));
		if($this->db->affected_rows() > 0){
			return true;
			
		}else{
			return false;
		}
	}
	
	function insert_rwynpkartikel($NPKLogin,$KodeArtikel)
	{	
		fire_print('log',"insert insert_rwynpkartikel $NPKLogin,$KodeArtikel");
		
		$sql = "INSERT INTO rwynpkartikel
				(CreatedOn,CreatedBy,KodeArtikel) VALUES (now(),?,?)";
			
		$this->db->query($sql,array($NPKLogin,$KodeArtikel));
		if($this->db->affected_rows() > 0){
			return true;
			
		}else{
			return false;
		}
	}
	
	function getKodeRwyNpkArtikel($NPKLogin,$KodeArtikel)
	{	
		$sql = "select KodeRwyNpkArtikel from rwynpkartikel
		where CreatedBy = ? AND KodeArtikel = ?
		ORDER BY KodeRwyNpkArtikel DESC LIMIT 1";
		
		$query = $this->db->query($sql,array($NPKLogin,$KodeArtikel));
		//fire_print('log','ini query '.$npk);
		
		if($query->num_rows() > 0){
			
			$KodeRwyNpkArtikel = "";
			foreach($query->result() as $row)
			{
				$KodeRwyNpkArtikel = $row->KodeRwyNpkArtikel;
			}
			return $KodeRwyNpkArtikel;
		}else{
			return false;
		}
	}
	
	function updatecounterartikel($NPKLogin,$KodeArtikel)
	{	
		fire_print('log',"tes masuk update counter artikel $NPKLogin,$KodeArtikel");
		
		$sql = "update  artikel set Counter =(counter + 1)
				where KodeArtikel = ?";
		
		$this->db->query($sql,array($KodeArtikel));
		if($this->db->affected_rows() > 0){
			return true;
			
		}else{
			return false;
		}
	}
	
	function cetakArtikel($KodeArtikel)
	{
		$sql = "select KodeArtikel,Judul,Artikel,Sumber,Status, CreatedBy 
				from artikel
				where KodeArtikel = ?";
		
		$query = $this->db->query($sql,array($KodeArtikel));
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	
}
?>