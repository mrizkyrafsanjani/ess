<?php 
Class Departemen extends Ci_Model
{
	function getDepartemen($kodeDepartemen)
	{	
		if($kodeDepartemen == ""){
			$sql = "select KodeDepartemen, NamaDepartemen from departemen
				where deleted = 0";
		}else{
			$sql = "select KodeDepartemen,NamaDepartemen from departemen 
				where deleted = 0 and KodeDepartemen = ? ";
		}
		$query = $this->db->query($sql, $kodeDepartemen);
		
		//$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>