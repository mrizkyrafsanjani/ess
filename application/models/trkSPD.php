<?php 
Class TrkSPD extends Ci_Model
{
	function getAll($npk,$kodeRole,$departemen="0",$tahun="0"){
		$whereDepartemen = " 1 = 1 ";
		$whereTahun = " 1 = 1 ";
		$param = array();
		if($departemen != "0" && $kodeRole != 4 && $kodeRole != 13){
			$whereDepartemen = " d.namadepartemen like ? ";
			array_push($param,$departemen);
		}

		if($departemen != "0" && $kodeRole == 4 && $kodeRole == 13){
			$whereDepartemen = " d.kodedepartemen like ? ";
			array_push($param,$departemen);
		}

		if($tahun != "0"){
			$whereTahun = " year(t.tanggalspd) = ? ";
			array_push($param,$tahun);
		}
		if($kodeRole == 1 OR $kodeRole == 3 OR $kodeRole == 4 OR $kodeRole == 13){
			$sql = "select NoSPD ,t.DPA,TanggalSPD,t.NPK,d.namadepartemen as departemen,u.nama,
				TanggalBerangkatSPD,TanggalKembaliSPD,TanggalLap,TanggalBerangkatLap,TanggalKembaliLap,Tujuan,AlasanPerjalanan,UangMuka,StatusLaporan,TotalSPD,TotalLap 
				from trkspd t join mstruser u on t.NPK = u.NPK
				left join departemen d on u.departemen = d.kodedepartemen
				where $whereDepartemen and $whereTahun and t.deleted = 0
				order by NoSPD desc";
			$query = $this->db->query($sql,$param);
		}else{
			$sql = "select NoSPD ,t.DPA,TanggalSPD,t.NPK,d.namadepartemen as departemen,u.nama,
				TanggalBerangkatSPD,TanggalKembaliSPD,TanggalLap,TanggalBerangkatLap,TanggalKembaliLap,Tujuan,AlasanPerjalanan,UangMuka,StatusLaporan,TotalSPD,TotalLap 
				from trkspd t join mstruser u on t.NPK = u.NPK
				left join departemen d on u.departemen = d.kodedepartemen
				where $whereDepartemen and $whereTahun and t.NPK = ? and t.deleted = 0
				order by NoSPD desc";
			array_push($param,$npk);
			$query = $this->db->query($sql,$param);
		}
		
		return $query->result();
	}
	
	function getSPDbyNPK($npk){
		
		$sql = "select t.DPA,t.UangMuka,t.NoSPD,DATE_FORMAT(t.TanggalSPD,'%d %M %Y') as TanggalSPD,t.NPK,u.golongan,u.nama,
				  j.NamaJabatan as jabatan,de.namadepartemen as departemen,u2.nama as atasan,DATE_FORMAT(t.TanggalBerangkatSPD,'%d %M %Y') as TanggalBerangkatSPD,DATE_FORMAT(t.TanggalKembaliSPD,'%d %M %Y') as TanggalKembaliSPD,t.Tujuan,t.AlasanPerjalanan,t.TotalSPD,
				  d.Deskripsi,d.KeteranganSPD,d.BebanHarianSPD,d.JumlahHariSPD 
				from (select * from trkspd where NPK = ? and deleted = 0 order by createdOn desc limit 1) t 
					join dtlbiayaspd d on t.noSPD = d.noSPD 
					join mstruser u on t.npk = u.npk
					left join mstruser u2 on u.npkatasan = u2.npk and u2.deleted = 0
					left join departemen de on u.departemen = de.kodedepartemen
					left join jabatan j on u.jabatan = j.KodeJabatan
				where t.deleted = 0 and d.deleted = 0 and u.deleted = 0";
		$query = $this->db->query($sql, $npk);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getSPDbyNoSPD($noSPD){
		$sql = "select t.DPA,t.UangMuka,t.NoSPD,DATE_FORMAT(t.TanggalSPD,'%d %M %Y') as TanggalSPD,DATE_FORMAT(t.TanggalLap,'%d %M %Y') as TanggalLap,t.NPK,u.golongan,u.nama,
				  j.NamaJabatan as jabatan,de.namadepartemen as departemen,u2.nama as atasan,DATE_FORMAT(t.TanggalBerangkatSPD,'%d %M %Y') as TanggalBerangkatSPD,DATE_FORMAT(t.TanggalBerangkatLap,'%d %M %Y') as TanggalBerangkatLap,DATE_FORMAT(t.TanggalKembaliSPD,'%d %M %Y') as TanggalKembaliSPD,DATE_FORMAT(t.TanggalKembaliLap,'%d %M %Y') as TanggalKembaliLap,DATE_FORMAT(t.TanggalKembaliKantorLap,'%d %M %Y') as TanggalKembaliKantorLap, t.Tujuan,t.AlasanPerjalanan,t.TotalSPD,t.TotalLap,
				  d.Deskripsi,d.KeteranganSPD,d.BebanHarianSPD,d.JumlahHariSPD,d.KeteranganLap,d.BebanHarianLap,d.JumlahHariLap 
				from trkspd t join dtlbiayaspd d on t.noSPD = d.noSPD 
					join mstruser u on t.npk = u.npk
					left join mstruser u2 on u.npkatasan = u2.npk and u2.deleted = 0
					left join departemen de on u.departemen = de.kodedepartemen
					left join jabatan j on u.jabatan = j.KodeJabatan
				where t.deleted = 0 and d.deleted = 0 and u.deleted = 0 and t.NoSPD = ?";
		$query = $this->db->query($sql, $noSPD);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getSPDbyNoSPDEdit($noSPD){
		$sql = "select t.DPA,t.UangMuka,t.NoSPD,t.TanggalSPD,t.TanggalLap,t.NPK,u.golongan,u.nama,
				  j.NamaJabatan as jabatan,de.namadepartemen as departemen,u2.nama as atasan,t.TanggalBerangkatSPD,t.TanggalBerangkatLap,t.TanggalKembaliSPD,t.TanggalKembaliLap,t.Tujuan,t.AlasanPerjalanan,t.TotalSPD,t.TotalLap,
				  d.Deskripsi,d.KeteranganSPD,d.BebanHarianSPD,d.JumlahHariSPD,d.KeteranganLap,d.BebanHarianLap,d.JumlahHariLap 
				from trkspd t join dtlbiayaspd d on t.noSPD = d.noSPD 
					join mstruser u on t.npk = u.npk
					left join mstruser u2 on u.npkatasan = u2.npk and u2.deleted = 0
					left join departemen de on u.departemen = de.kodedepartemen
					left join jabatan j on u.jabatan = j.KodeJabatan
				where t.deleted = 0 and d.deleted = 0 and u.deleted = 0 and t.NoSPD = ?";
		$query = $this->db->query($sql, $noSPD);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function insertSPD($noSPD)
	{	
		$session_data = $this->session->userdata('logged_in');
		$NPK = $session_data['npk'];
		$tanggalBerangkatSPDddmmyyyy = $this->input->post('txtTanggalBerangkat');
		$tanggalBerangkatSPD = substr($tanggalBerangkatSPDddmmyyyy,6,4)."-".substr($tanggalBerangkatSPDddmmyyyy,3,2)."-".substr($tanggalBerangkatSPDddmmyyyy,0,2);
		$tanggalKembaliSPDddmmyyyy = $this->input->post('txtTanggalKembali');
		$tanggalKembaliSPD = substr($tanggalKembaliSPDddmmyyyy,6,4)."-".substr($tanggalKembaliSPDddmmyyyy,3,2)."-".substr($tanggalKembaliSPDddmmyyyy,0,2);
		$tujuan = $this->input->post('txtTujuan');
		$alasan = $this->input->post('txtAlasan');
		$DPA = $this->input->post('rbDPA');
		$uangMuka = $this->input->post('rbUangMuka');
		$NPKlogin = $session_data['npk'];		
		if($uangMuka == "1"){
			$totalSPD = str_replace(",","",$this->input->post('txtGrandTotal'));
		}else{
			$totalSPD = "0";
		}
		
		//echo "$NPK $tanggalBerangkatSPD $tanggalKembaliSPD $tujuan $alasan $DPA $uangMuka $NPKlogin";
		
		$sql = "INSERT INTO TrkSPD (noSPD, tanggalSPD,NPK,tanggalBerangkatSPD,tanggalKembaliSPD,tujuan,alasanPerjalanan,DPA,UangMuka,createdOn,createdBy,totalSPD) 
			VALUES (?,now(),?,?,?,?,?,?,?,now(),?,?)";

		$this->db->query($sql,array($noSPD,$NPK,$tanggalBerangkatSPD,$tanggalKembaliSPD,$tujuan,$alasan,$DPA,$uangMuka,$NPKlogin,$totalSPD));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function InsertDtlUangMuka($noSPD){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$sqlupdate = "UPDATE dtlbiayaSPD SET noSPD =?,UpdatedOn = now(),  UpdatedBy=?,
		KeteranganRealisasi=KeteranganRealisasi,JumlahRealisasi=JumlahRealisasi,HargaRealisasi=HargaRealisasi,JenisBiayaRealisasi=JenisBiayaRealisasi,
		TotalRealisasi=TotalRealisasi WHERE CreatedBy = ? and Deleted=0 and NoUangMuka='-'";

		$this->db->query($sqlupdate,array($noSPD,$NPKlogin,$NPKlogin));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}
	function insertDtlBiayaSPD($noSPD){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$uangSaku = array($noSPD,'Uang Saku',$this->input->post('txtKetUangSaku'),str_replace(",","",$this->input->post('txtBebanHarianUangSaku')),$this->input->post('txtJmlhHariUangSaku'),$NPKlogin);
		$uangMakan = array($noSPD,'Uang Makan',$this->input->post('txtKetUangMakan'),str_replace(",","",$this->input->post('txtBebanHarianUangMakan')),$this->input->post('txtJmlhHariUangMakan'),$NPKlogin);
		$hotel = array($noSPD,'Hotel',$this->input->post('txtKetHotel'),str_replace(",","",$this->input->post('txtBebanHarianHotel')),$this->input->post('txtJmlhHariHotel'),$NPKlogin);
		$hotel2 = array($noSPD,'Penginapan','',null,0,$NPKlogin);
		$taksi = array($noSPD,'Taksi',$this->input->post('txtKetTaksi'),str_replace(",","",$this->input->post('txtBebanHarianTaksi')),1,$NPKlogin);
		$airport = array($noSPD,'Airport Tax',$this->input->post('txtKetAirport'),str_replace(",","",$this->input->post('txtBebanHarianAirport')),1,$NPKlogin);
		$lain1 = array($noSPD,'Lain-lain 1',$this->input->post('txtKetLain1'),str_replace(",","",$this->input->post('txtBebanHarianLain1')),1,$NPKlogin);
		$lain2 = array($noSPD,'Lain-lain 2',$this->input->post('txtKetLain2'),str_replace(",","",$this->input->post('txtBebanHarianLain2')),1,$NPKlogin);
		$lain3 = array($noSPD,'Lain-lain 3',$this->input->post('txtKetLain3'),str_replace(",","",$this->input->post('txtBebanHarianLain3')),1,$NPKlogin);
		
		$sql = "INSERT INTO dtlBiayaSPD(noSPD,Deskripsi,KeteranganSPD,BebanHarianSPD,JumlahHariSPD,CreatedOn,CreatedBy)
			VALUES (?,?,?,?,?,now(),?)";
		
		$this->db->query($sql,$uangSaku);
		$this->db->query($sql,$uangMakan);
		$this->db->query($sql,$hotel);
		$this->db->query($sql,$hotel2);
		$this->db->query($sql,$taksi);
		$this->db->query($sql,$airport);
		//if($this->input->post('txtBebanHarianLain1')>0)
			$this->db->query($sql,$lain1);
		//if($this->input->post('txtBebanHarianLain2')>0)
			$this->db->query($sql,$lain2);
		//if($this->input->post('txtBebanHarianLain3')>0)
			$this->db->query($sql,$lain3);
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}
	
	function updateSPD($noSPD){
		$session_data = $this->session->userdata('logged_in');
		$tanggalBerangkatddmmyyyy = $this->input->post('txtTanggalBerangkat');
		$tanggalBerangkatLap = substr($tanggalBerangkatddmmyyyy,6,4)."-".substr($tanggalBerangkatddmmyyyy,3,2)."-".substr($tanggalBerangkatddmmyyyy,0,2);
		$tanggalKembaliddmmyyyy = $this->input->post('txtTanggalKembali');
		$tanggalKembaliLap = substr($tanggalKembaliddmmyyyy,6,4)."-".substr($tanggalKembaliddmmyyyy,3,2)."-".substr($tanggalKembaliddmmyyyy,0,2);
		$tujuan = $this->input->post('txtTujuan');
		$alasan = $this->input->post('txtAlasan');

		$NPKlogin = $session_data['npk'];
		$totalLap = str_replace(",","",$this->input->post('txtGrandTotal'));
		//echo "$noSPD $tanggalBerangkatLap $tanggalKembaliLap $NPKlogin $totalLap";
		$sql = "UPDATE TrkSPD SET tanggalLap = now(),tanggalBerangkatLap = ?, tanggalKembaliLap = ?, Tujuan = ?, AlasanPerjalanan = ?, updatedOn=now(), updatedBy=?, totalLap=?
			WHERE noSPD = ?";
		
		$this->db->query($sql,array($tanggalBerangkatLap,$tanggalKembaliLap,$tujuan, $alasan, $NPKlogin,$totalLap,$noSPD));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function insertLaporan($noSPD){
		$session_data = $this->session->userdata('logged_in');
		$tujuan = $this->input->post('txtTujuan');
		$alasan = $this->input->post('txtAlasan');
		$tanggalBerangkatddmmyyyy = $this->input->post('txtTanggalBerangkat');
		$tanggalBerangkatLap = substr($tanggalBerangkatddmmyyyy,6,4)."-".substr($tanggalBerangkatddmmyyyy,3,2)."-".substr($tanggalBerangkatddmmyyyy,0,2);
		$tanggalKembaliddmmyyyy = $this->input->post('txtTanggalKembali');
		$tanggalKembaliLap = substr($tanggalKembaliddmmyyyy,6,4)."-".substr($tanggalKembaliddmmyyyy,3,2)."-".substr($tanggalKembaliddmmyyyy,0,2);
		$tanggalKembaliKantorddmmyyyy = $this->input->post('txtTanggalKembaliKantor');
		$tanggalKembaliKantorLap = substr($tanggalKembaliKantorddmmyyyy,6,4)."-".substr($tanggalKembaliKantorddmmyyyy,3,2)."-".substr($tanggalKembaliKantorddmmyyyy,0,2);
		if ($tanggalKembaliKantorLap==''){
			$tanggalKembaliKantorLap = $tanggalKembaliLap;
		}		
		$NPKlogin = $session_data['npk'];
		$totalLap = str_replace(",","",$this->input->post('txtGrandTotal'));
		//echo "$noSPD $tanggalBerangkatLap $tanggalKembaliLap $NPKlogin $totalLap";
		$sql = "UPDATE TrkSPD SET statusLaporan='S',tanggalLap = now(),tanggalBerangkatLap = ?, tanggalKembaliLap = ?, tanggalKembaliKantorLap = ?, Tujuan = ?, AlasanPerjalanan = ?, updatedOn=now(), updatedBy=?, totalLap=?
			WHERE noSPD = ?";
		
		$this->db->query($sql,array($tanggalBerangkatLap,$tanggalKembaliLap,$tanggalKembaliKantorLap, $tujuan, $alasan, $NPKlogin,$totalLap,$noSPD));
	
		
		//echo "$noSPD $tanggalBerangkatLap $tanggalKembaliLap $NPKlogin $totalLap";
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	function UpdateUangMuka($noSPD){
		$session_data = $this->session->userdata('logged_in');
		$jenisbayarumkurang = $this->input->post('txtPilihBayarKurang');
		$bankkurang = $this->input->post('txtBankKurang');
		$norekkurang = $this->input->post('txtNoRekKurang');
		$namapenerimakurang = $this->input->post('txtPenerimaKurang');
		$waktupenyelesaianterlambat = $this->input->post('lblWaktuPenyelesaian');
		$NPKlogin = $session_data['npk'];
		
		$sql2 = "UPDATE trxuangmuka SET 
					   WaktuPenyelesaianTerlambat = ?, TipeKembaliUangMuka = ?, BankKembaliUangMuka = ?, 
					   NoRekKembaliUangMuka = ?, NmPenerimaUangMuka =?,LastUpdateOn=now(), LastUpdateBy=?,TanggalRealisasi=now()
				WHERE noSPD = ?";
		
		$query= $this->db->query($sql2,array($waktupenyelesaianterlambat,$jenisbayarumkurang,$bankkurang,$norekkurang,$namapenerimakurang,$NPKlogin,$noSPD));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}


	/*function bayarLaporan($noSPD){
		$session_data = $this->session->userdata('logged_in');
		$jenisbayarumkurang = $this->input->post('txtPilihBayarKurang');
		$bankkurang = $this->input->post('txtBankKurang');
		$norekkurang = $this->input->post('txtNoRekKurang');
		$namapenerimakurang = $this->input->post('txtPenerimaKurang');

		$NPKlogin = $session_data['npk'];
		
		//echo "$noSPD $tanggalBerangkatLap $tanggalKembaliLap $NPKlogin $totalLap";
		
		$sql = "UPDATE trxuangmuka SET 
					   TipeKembaliUangMuka = ?, BankKembaliUangMuka = ?, 
					   NoRekKembaliUangMuka = ?, NmPenerimaUangMuka =?,LastUpdateOn=now(), LastUpdateBy=?,TanggalRealisasi=now()
				WHERE noSPD = ?";
		
		$query= $this->db->query($sql,array($jenisbayarumkurang,$bankkurang,$norekkurang,$namapenerimakurang,$NPKlogin,$noSPD));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}*/
	function updateDtlBiayaSPD($noSPD)
	{
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$uangSaku = array($this->input->post('txtKetUangSaku'),str_replace(",","",$this->input->post('txtBebanHarianUangSaku')),$this->input->post('txtJmlhHariUangSaku'),$NPKlogin,$noSPD,'Uang Saku');
		$uangMakan = array($this->input->post('txtKetUangMakan'),str_replace(",","",$this->input->post('txtBebanHarianUangMakan')),$this->input->post('txtJmlhHariUangMakan'),$NPKlogin,$noSPD,'Uang Makan');
		$hotel = array($this->input->post('txtKetHotel'),str_replace(",","",$this->input->post('txtBebanHarianHotel')),$this->input->post('txtJmlhHariHotelLap'),$NPKlogin,$noSPD,'Hotel');
		$hotel2 = array($this->input->post('txtKetHotel2'),str_replace(",","",$this->input->post('txtBebanHarianHotel2')),$this->input->post('txtJmlhHariHotel2'),$NPKlogin,$noSPD,'Penginapan');
		$taksi = array($this->input->post('txtKetTaksi'),str_replace(",","",$this->input->post('txtBebanHarianTaksi')),1,$NPKlogin,$noSPD,'Taksi');
		$airport = array($this->input->post('txtKetAirport'),str_replace(",","",$this->input->post('txtBebanHarianAirport')),1,$NPKlogin,$noSPD,'Airport Tax');
		$lain1 = array($this->input->post('txtKetLain1'),str_replace(",","",$this->input->post('txtBebanHarianLain1')),1,$NPKlogin,$noSPD,'Lain-lain 1');
		$lain2 = array($this->input->post('txtKetLain2'),str_replace(",","",$this->input->post('txtBebanHarianLain2')),1,$NPKlogin,$noSPD,'Lain-lain 2');
		$lain3 = array($this->input->post('txtKetLain3'),str_replace(",","",$this->input->post('txtBebanHarianLain3')),1,$NPKlogin,$noSPD,'Lain-lain 3');
		
		$sql = "UPDATE dtlBiayaSPD SET KeteranganSPD=?,BebanHarianSPD=?,JumlahHariSPD=?,UpdatedOn=now(),UpdatedBy=?
			WHERE noSPD = ? AND Deskripsi = ?";
		
		$this->db->query($sql,$uangSaku);
		$this->db->query($sql,$uangMakan);
		$this->db->query($sql,$hotel);
		$this->db->query($sql,$hotel2);
		$this->db->query($sql,$taksi);
		$this->db->query($sql,$airport);
		//if($this->input->post('txtBebanHarianLain1')>0)
			$this->db->query($sql,$lain1);
		//if($this->input->post('txtBebanHarianLain2')>0)
			$this->db->query($sql,$lain2);
		//if($this->input->post('txtBebanHarianLain3')>0)
			$this->db->query($sql,$lain3);
		/*print_r($lain1);
		print_r($lain2);
		print_r($lain3);*/
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function insertDtlBiayaLaporan($noSPD){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$uangSaku = array($this->input->post('txtKetUangSaku'),str_replace(",","",$this->input->post('txtBebanHarianUangSaku')),$this->input->post('txtJmlhHariUangSaku'),$NPKlogin,$noSPD,'Uang Saku');
		$uangMakan = array($this->input->post('txtKetUangMakan'),str_replace(",","",$this->input->post('txtBebanHarianUangMakan')),$this->input->post('txtJmlhHariUangMakan'),$NPKlogin,$noSPD,'Uang Makan');
		$hotel = array($this->input->post('txtKetHotel'),str_replace(",","",$this->input->post('txtBebanHarianHotel')),$this->input->post('txtJmlhHariHotelLap'),$NPKlogin,$noSPD,'Hotel');
		$hotel2 = array($this->input->post('txtKetHotel2'),str_replace(",","",$this->input->post('txtBebanHarianHotel2')),$this->input->post('txtJmlhHariHotel2'),$NPKlogin,$noSPD,'Penginapan');
		$taksi = array($this->input->post('txtKetTaksi'),str_replace(",","",$this->input->post('txtBebanHarianTaksi')),1,$NPKlogin,$noSPD,'Taksi');
		$airport = array($this->input->post('txtKetAirport'),str_replace(",","",$this->input->post('txtBebanHarianAirport')),1,$NPKlogin,$noSPD,'Airport Tax');
		$lain1 = array($this->input->post('txtKetLain1'),str_replace(",","",$this->input->post('txtBebanHarianLain1')),1,$NPKlogin,$noSPD,'Lain-lain 1');
		$lain2 = array($this->input->post('txtKetLain2'),str_replace(",","",$this->input->post('txtBebanHarianLain2')),1,$NPKlogin,$noSPD,'Lain-lain 2');
		$lain3 = array($this->input->post('txtKetLain3'),str_replace(",","",$this->input->post('txtBebanHarianLain3')),1,$NPKlogin,$noSPD,'Lain-lain 3');
		
		$sql = "UPDATE dtlBiayaSPD SET KeteranganLap=?,BebanHarianLap=?,JumlahHariLap=?,UpdatedOn=now(),UpdatedBy=?
			WHERE noSPD = ? AND Deskripsi = ?";
		
		$this->db->query($sql,$uangSaku);
		$this->db->query($sql,$uangMakan);
		$this->db->query($sql,$hotel);
		$this->db->query($sql,$hotel2);
		$this->db->query($sql,$taksi);
		$this->db->query($sql,$airport);
		//if($this->input->post('txtBebanHarianLain1')>0)
			$this->db->query($sql,$lain1);
		//if($this->input->post('txtBebanHarianLain2')>0)
			$this->db->query($sql,$lain2);
		//if($this->input->post('txtBebanHarianLain3')>0)
			$this->db->query($sql,$lain3);
		/*print_r($lain1);
		print_r($lain2);
		print_r($lain3);*/
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function getLastNoSPD($DPA){
		$sql = "select noSPD 
			from TrkSPD
			where DPA like ?
			order by CreatedOn Desc
			limit 1";
		$query = $this->db->query($sql,$DPA);
		
		if($query->num_rows() > 0){
			return $query->result();
		}else {
			return false;
		}
	}
	
	function getNoSPDBelumLapor($npk){
		$sql = "select NoSPD
			from trkspd
			where deleted = 0 and statusLaporan = 'B' and NPK = ?";
		$query = $this->db->query($sql,$npk);
		return $query->result();
		
	}
	
	function getOneNoSPD($noSPD){
		$sql = "select NoSPD
			from trkspd
			where deleted = 0 and statusLaporan = 'B' and noSPD = ?";
		$query = $this->db->query($sql,$noSPD);
		return $query->result();
	}
	
	function get_tahun(){
		$sql = "select distinct year(tanggalspd) as year from trkspd where deleted = 0";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function deleteSPD($noSPD){
		$sql = "update trkSPD set deleted = 1 where noSPD = ?";
		$query = $this->db->query($sql,$noSPD);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	function deleteLaporanSPD($noSPD){
		$sql = "update trkSPD set StatusLaporan = 'B' where noSPD = ?";
		$query = $this->db->query($sql,$noSPD);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function getLastKodePW()
	{
		$sql = "SELECT KodePelimpahanWewenang FROM pelimpahanwewenang ORDER BY CreatedOn DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->KodePelimpahanWewenang;
			}
		}
		else
		{
			return "0";
		}
	}

	function integerToRoman($integer)
	{
		// Convert the integer into an integer (just to make sure)
		$integer = intval($integer);
		$result = '';
		
		// Create a lookup array that contains all of the Roman numerals.
		$lookup = array('M' => 1000,
		'CM' => 900,
		'D' => 500,
		'CD' => 400,
		'C' => 100,
		'XC' => 90,
		'L' => 50,
		'XL' => 40,
		'X' => 10,
		'IX' => 9,
		'V' => 5,
		'IV' => 4,
		'I' => 1);
		
		foreach($lookup as $roman => $value){
		// Determine the number of matches
		$matches = intval($integer/$value);
		
		// Add the same number of characters to the string
		$result .= str_repeat($roman,$matches);
		
		// Set the integer to be the remainder of the integer and the value
		$integer = $integer % $value;
		}
	
		// The Roman numeral should be built, return it
		return $result;
	}

	function NumberFromRoman($roman)
	{
		$romans = array(
			'M' => 1000,
			'CM' => 900,
			'D' => 500,
			'CD' => 400,
			'C' => 100,
			'XC' => 90,
			'L' => 50,
			'XL' => 40,
			'X' => 10,
			'IX' => 9,
			'V' => 5,
			'IV' => 4,
			'I' => 1,
		);		
		
		$result = 0;
		
		foreach ($romans as $key => $value) {
			while (strpos($roman, $key) === 0) {
				$result += $value;
				$roman = substr($roman, strlen($key));
			}
		}
		return $result;
	}

	function insertPelimpahanWewenang ($noSPD,$noPW){
		
		
		$session_data = $this->session->userdata('logged_in');
		$NPK = $session_data['npk'];
		$tanggalBerangkatSPDddmmyyyy = $this->input->post('txtTanggalBerangkat');
		$tanggalBerangkatSPD = substr($tanggalBerangkatSPDddmmyyyy,6,4)."-".substr($tanggalBerangkatSPDddmmyyyy,3,2)."-".substr($tanggalBerangkatSPDddmmyyyy,0,2);
		$tanggalKembaliSPDddmmyyyy = $this->input->post('txtTanggalKembali');
		$tanggalKembaliSPD = substr($tanggalKembaliSPDddmmyyyy,6,4)."-".substr($tanggalKembaliSPDddmmyyyy,3,2)."-".substr($tanggalKembaliSPDddmmyyyy,0,2);
		
		
		$i=0;
		
		
		$sql = "INSERT INTO pelimpahanwewenang (KodePelimpahanWewenang, NoTransaksi,JenisTransaksi,TanggalMulai,TanggalSelesai,CreatedOn,CreatedBy) 
		VALUES (?,?,?,?,?,now(),?)";

		$this->db->query($sql,array($noPW,$noSPD,'S',$tanggalBerangkatSPD,$tanggalKembaliSPD,$NPK));

		$sqlupdate = "UPDATE dtlpelimpahanwewenang SET KodePelimpahanWewenang =?,UpdatedOn = now(),  UpdatedBy=?
		WHERE CreatedBy = ? and Deleted=0 and KodePelimpahanWewenang='-'";

		$this->db->query($sqlupdate,array($noPW,$NPK,$NPK));

		for( $i = 1; $i<=5; $i++ ) {
			if ( $this->input->post('txtNamaEmail'.$i) != '')
			{	
				$sqlemail = "INSERT INTO dtlemailpelimpahanwewenang (KodePelimpahanWewenang, Nama,EmailYangDiTuju,CreatedOn,CreatedBy) 
				VALUES (?,?,?,now(),?)";

				$this->db->query($sqlemail,array($noPW,ucwords($this->input->post('txtNamaEmail'.$i)),$this->input->post('txtAlamatEmail'.$i),$NPK));
			}		
		}
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function cekAdaPelimpahanWewenang ($noSPD)
	{
		$sql = "select * 
		from pelimpahanwewenang
		where NoTransaksi= ?";
		$query = $this->db->query($sql,$noSPD);
		
		if($query->num_rows() > 0){
			return true;
		}else {
			return false;
		}
	}

	function joinUangMuka($noSPD)
	{
		$sql= "SELECT Nouangmuka, pl.WaktuPenyelesaianTerlambat, pl.NoPP, pl.JenisKegiatanUangMuka,
		pl.TanggalPermohonan, pl.KeteranganPermohonan, pl.DPA, pl.TanggalRealisasi,
		pl.TipeKembaliUangMuka, pl.BankKembaliUangMuka, 
		pl.NoRekKembaliUangMuka, pl.NmPenerimaUangMuka
		FROM trxuangmuka pl 
		JOIN trkspd pn ON pl.nospd=pn.nospd where pl.NoSPD= ?";
		$query = $this->db->query($sql,array($noSPD));
		if($query->num_rows() > 0){
			return $query->result();
		}else {
			return false;
		}
	}

	function TglTerimaPermohonan($noSPD)
	{
		$sql= "SELECT TglTerimaHRGAPermohonan, TglTerimaFinancePermohonan, TglTerimaFinancePermohonan, TglBayarFinancePermohonan
		FROM trxuangmuka pl 
		where NoSPD= ?";
		$query = $this->db->query($sql,array($noSPD));
		if($query->num_rows() > 0){
			return $query->result();
		}else {
			return false;
		}
	}

	function UpdateKeteranganPW($NoTransaksi)
	{
		try
		{	
			
			$session_data = $this->session->userdata('logged_in');	
			$NPK = $session_data['npk'];
				
				
				$pelimpahanwewenangList = $this->db->query("select * from pelimpahanwewenang
					where Deleted = 0 AND NoTransaksi = ?",$NoTransaksi);
				if ($pelimpahanwewenangList)
				{
					foreach($pelimpahanwewenangList->result() as $pelimpahanwewenang)
					{
						$TanggalMulai= $pelimpahanwewenang->TanggalMulai;
						$TanggalSelesai= $pelimpahanwewenang->TanggalSelesai;

						$dtlpelimpahanwewenangList = $this->db->query("select a.*,b.Nama as NamaYangdilimpahkan 
						from dtlpelimpahanwewenang a
						join mstruser b on b.NPK=a.NPKYangDilimpahkan
						where a.Deleted = 0 AND a.KodePelimpahanWewenang = ?",$pelimpahanwewenang->KodePelimpahanWewenang);

						$pelimpahanwewenangnama = '';
						foreach($dtlpelimpahanwewenangList->result() as $dtlpelimpahanwewenang)
						{
							$pelimpahanwewenangnama =$pelimpahanwewenangnama . $dtlpelimpahanwewenang->NamaYangdilimpahkan .' ' ;
						}
						
						$this->db->query("update Absensi set PelimpahanWewenang = ?,UpdatedOn = now(),UpdatedBy = ?
						where Date_format(tanggal, '%Y-%m-%d')  between Date_format(?, '%Y-%m-%d') and Date_format(?, '%Y-%m-%d')",array($pelimpahanwewenangnama,$NPK,$TanggalMulai,$TanggalSelesai));
					}
				}			
				
			
			
			 if($this->db->affected_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			} 
			
			return true;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
}
?>