<?php 
Class PROMISE_model extends Ci_Model
{
	public function  __construct() {
        parent::__construct();
        
    }
    public function Add_User($data_user){
    $this->db->insert('pmtrekap', $data_user);
   }
   function getLastKodePR()
	{
		$sql = "SELECT KodeRequestDetail FROM pmtdetailrequest ORDER BY KodeRequestID DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->KodeRequestDetail;
			}
		}
		else
		{
			return "0";
		}
	}
	function getLastKodeEP()
	{
		$sql = "SELECT KodeRequestDetail FROM pmtdetaileditreq ORDER BY KodeRequestID DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->KodeRequestDetail;
			}
		}
		else
		{
			return "0";
		}
	}
	function getLastKodePA()
	{
		$sql = "SELECT KodeRequestDetail FROM pmtprmtrequest ORDER BY KodeRequestDetail DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->KodeRequestDetail;
			}
		}
		else
		{
			return "0";
		}
	}
	function getLastKodePM()
	{
		$sql = "SELECT KodeRequestMOM FROM pmtmomrequest ORDER BY ID DESC LIMIT 1";
		$query = $this->db->query($sql);
		
		if($query->num_rows() == 1)
		{			
			$result = $query->result();
			foreach($result as $row)
			{
				return $row->KodeRequestMOM;
			}
		}
		else
		{
			return "0";
		}
	}
   function getAtasan($nama)
   {
	   $sql = "select a.NPKAtasan, b.NPK,b.Nama as NamaAtasan
	   from mstruser a
	   left join mstruser b on a.NPKAtasan = b.NPK
	   where a.Nama=?";
	   $query = $this->db->query($sql, $nama);
	   
	  if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
   }
   function getDataUserByNama($nama){
		$sql = "select a.npk,a.golongan,a.nama,a.emailInternal as email, a.NoExt as noExt, j.NamaJabatan as jabatan,d.namadepartemen as departemen,uangsaku,uangmakan,a.NPKAtasan as NPKAtasan, b.nama as atasan, a.TanggalBekerja,j.KodeJabatan,d.KodeDepartemen,a.KodeRole, a.DPA
			from mstruser a left join mstrgolongan g on a.golongan = g.golongan 
			left join mstruser b on a.npkatasan = b.npk and b.deleted = 0
			left join departemen d on a.departemen = d.kodedepartemen
			left join jabatan j on a.jabatan = j.KodeJabatan
			where a.nama = ? and a.deleted = 0";
		$query = $this->db->query($sql, $nama);
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

   function AddProject(){
	$session_data = $this->session->userdata('logged_in');
	$NPKlogin = $session_data['npk'];
	$ProjectID = $this->input->post('txtProjectID');
	$NmProject = $this->input->post('txtNmProject');
	$KPI = $this->input->post('txtKPI');
	$Target = $this->input->post('txtTarget');
	$StartDate = $this->input->post('txtStartDate');
	$DeadlineUREQ = $this->input->post('txtDeadlineUREQ');
	$Deadline = $this->input->post('txtDeadline');
	$PIC = $this->input->post('txtPIC');
	$Budget = $this->input->post('txtBudget');
	$Departemen = $this->input->post('txtDepartemen');
	$Keterangan = $this->input->post('txtKeterangan');
	
	$sql = "INSERT INTO pmtrekap (ProjectID,NamaProject, Deleted,CreatedOn, CreatedBy, UpdatedOn, UpdatedBy,
								  KPI,Target,StartDate,DeadlineUREQ,Deadline,PIC,Budget,Departemen,Keterangan,ReminderDate) 
					VALUES (?,?,'0',now(),?,now(),?,?,?,?,?,?,?,?,?,?,?)";
	$this->db->query($sql,array($ProjectID,$NmProject,$NPKlogin,$NPKlogin,$KPI,$Target,$StartDate,$DeadlineUREQ,$Deadline,$PIC,$Budget,$Departemen,$Keterangan,$StartDate));
	
	if($this->db->affected_rows() > 0){
		return true;
	}else{
		return false;
	}
	
}
	function UbahDetail($ProjectID){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$NmProject = $this->input->post('txtNmProject');
		$KPI = $this->input->post('txtKPI');
		$Target = $this->input->post('txtTarget');
		$StartDate = $this->input->post('txtStartDate');
		$DeadlineUREQ = $this->input->post('txtDeadlineUREQ');
		$Deadline = $this->input->post('txtDeadline');
		$PIC = $this->input->post('txtPIC');
		$Budget = $this->input->post('txtBudget');
		$Departemen = $this->input->post('txtDepartemen');
		$Keterangan = $this->input->post('txtKeterangan');
		$DetilInfo = $this->input->post('txtTujuan');
		$KetAdmin = $this->input->post('txtKetAdmin');
		$Status = $this->input->post('txtStatus');
		$sqlupdate = "UPDATE pmtrekap SET NamaProject =?,UpdatedOn = now(),  UpdatedBy=?,
		KPI=?,Target=?,StartDate=?,DeadlineUREQ=?,Deadline=?,PIC=?,Budget=?,Departemen=?,
		Keterangan=?,DetilInfo=?,KetAdmin=?,Status=?
		WHERE ProjectID=? and Deleted=0";

		$this->db->query($sqlupdate,array($NmProject,$NPKlogin,$KPI,$Target,$StartDate,$DeadlineUREQ,$Deadline,$PIC,$Budget,$Departemen,$Keterangan,$DetilInfo,$KetAdmin,$Status,$ProjectID));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}
	function UbahParameter($ProjectID){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$ProjectID = $ProjectID;
		$sqlupdate = "UPDATE pmtprmtr SET ProjectID =?,UpdatedOn = now(), 
		UpdatedBy=?, Inserts='0'
		WHERE Inserts=? and Deleted=0";

		$this->db->query($sqlupdate,array($ProjectID,$NPKlogin,$ProjectID));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}
	function ApprovalUbahParameter($ProjectID){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$ProjectID = $ProjectID;
		$Status = '';
		
		$sql = "INSERT INTO pmtprmtrequest (ParameterID, ProjectID, Parameter, Presentase, DeadlinePara,Inserts, Updates, Deletes, Status, FilePathParameter, KoreksiParameter, KoreksiPresentase, KoreksiDeadline, CreatedOn, CreatedBy)
		SELECT DISTINCT ParameterID, ProjectID, Parameter, Presentase, DeadlinePara, Inserts, Updates, Deletes, Status, FilePathParameter, KoreksiParameter, KoreksiPresentase, KoreksiDeadline, CreatedOn, CreatedBy FROM pmtprmtr
				WHERE Inserts=? and Deleted=0 or Updates=? and Deleted=0 or Deletes=? and Deleted=0";

		$this->db->query($sql,array($ProjectID,$ProjectID,$ProjectID));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}

	function ApprovalProgressParameter($ParaID){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$ParaID = $ParaID;
		
		$sql = "INSERT INTO pmtprmtrequest (ParameterID, ProjectID, Parameter, Presentase, DeadlinePara,Inserts, Updates, Deletes, Progress, Status, FilePathParameter, KoreksiParameter, KoreksiPresentase, KoreksiDeadline, CreatedOn, CreatedBy)
		SELECT DISTINCT ParameterID, ProjectID, Parameter, Presentase, DeadlinePara, Inserts, Updates, Deletes, Progress, Status, FilePathParameter, KoreksiParameter, KoreksiPresentase, KoreksiDeadline, CreatedOn, CreatedBy FROM pmtprmtr
				WHERE Progress=? and Deleted=0";

		$this->db->query($sql,array($ParaID));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}

	function UpdateAllParameter($KodeRequestDetail){		
		$sql = "SELECT * FROM pmtprmtrequest
		WHERE Inserts<> '' and StatusRequest<>'0' and KodeRequestDetail=? or Updates <> '' and StatusRequest<>'0' and KodeRequestDetail=? or Deletes<> '' and StatusRequest<>'0' and KodeRequestDetail=?";

		$query = $this->db->query($sql,array($KodeRequestDetail,$KodeRequestDetail,$KodeRequestDetail));
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
		
	}
	function UpdateProgressApproved($KodeRequestDetail){		
		$sql = "SELECT * FROM pmtprmtrequest
		WHERE Progress <> '' and StatusRequest<>'0' and KodeRequestDetail=?
		ORDER BY ID DESC LIMIT 1";

		$query = $this->db->query($sql,array($KodeRequestDetail));
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
		
	}
	function UpdateAllMOM($KodeRequestMOM){		
		$sql = "SELECT * FROM pmtmomrequest
		WHERE Inserts<> '' and StatusRequest<>'0' and KodeRequestMOM=? or Updates <> '' and StatusRequest<>'0' and KodeRequestMOM=? or Deletes<> '' and StatusRequest<>'0' and KodeRequestMOM=?";

		$query = $this->db->query($sql,array($KodeRequestMOM,$KodeRequestMOM,$KodeRequestMOM));
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
		
	}
	function getApprovalParameter($KodeRequestDetail){
		$sql = "SELECT * FROM pmtprmtrequest
		WHERE Inserts<> '0' and StatusRequest=0 and Deleted=0 and KodeRequestDetail=? or Updates <> '0' and StatusRequest=0 and Deleted=0 and KodeRequestDetail=? or Deletes<> '0' and StatusRequest=0 and Deleted=0 and KodeRequestDetail=?";

		$query = $this->db->query($sql,array($KodeRequestDetail,$KodeRequestDetail,$KodeRequestDetail));
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	}


	function getApprovalMOM($KodeRequestMOM){
		$sql = "SELECT * FROM pmtmomrequest
		WHERE Inserts<> '0' and StatusRequest=0 and Deleted=0 and KodeRequestMOM=? or Updates <> '0' and StatusRequest=0 and Deleted=0 and KodeRequestMOM=? or Deletes<> '0' and StatusRequest=0 and Deleted=0 and KodeRequestMOM=?";

		$query = $this->db->query($sql,array($KodeRequestMOM,$KodeRequestMOM,$KodeRequestMOM));
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	}

	function getApprovalProgress($KodeRequestDetail){
		$sql = "SELECT * FROM pmtprmtrequest
		WHERE Progress<> '0' and StatusRequest=0 and Deleted=0 and KodeRequestDetail=?
		ORDER BY ID DESC LIMIT 1";

		$query = $this->db->query($sql,array($KodeRequestDetail));
		
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
		
	}

	function ApprovalUbahMOM($ProjectID){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$ProjectID = $ProjectID;
		$Status = '';
		
		$sql = "INSERT INTO pmtmomrequest (MomID, ProjectID, Judul, Deskripsi,Inserts, Updates, Deletes, KoreksiJudul, KoreksiDeskripsi, CreatedOn, CreatedBy)
		SELECT DISTINCT MomID, ProjectID, Judul, Deskripsi,Inserts, Updates, Deletes, KoreksiJudul, KoreksiDeskripsi, CreatedOn, CreatedBy FROM pmtmom
				WHERE Inserts=? and Deleted=0 or Updates=? and Deleted=0 or Deletes=? and Deleted=0";

		$this->db->query($sql,array($ProjectID,$ProjectID,$ProjectID));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}
	function TambahMOM($ProjectID){
		$session_data = $this->session->userdata('logged_in');
		$NPKlogin = $session_data['npk'];
		$ProjectID = $ProjectID;
		$sqlupdate = "UPDATE pmtmom SET ProjectID =?,
		CreatedOn=now(), CreatedBy=?, Inserts='0'
		WHERE Inserts=? and Deleted='0'";

		$this->db->query($sqlupdate,array($ProjectID,$NPKlogin,$ProjectID));
		
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
		
	}
	function getPara($ProjectID){
		$sql = "select Parameter, DeadlinePara
		FROM pmtprmtr
		where ProjectID=? and Status=''
		order by ParameterID LIMIT 1";
				
		$query = $this->db->query($sql,$ProjectID);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getAllPara($ProjectID){
		$sql = "select Parameter, Presentase, DeadlinePara, Status, FilePathParameter
		FROM pmtprmtr
		where ProjectID=? and Deleted=0";
				
		$query = $this->db->query($sql,$ProjectID);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getLastID(){
		$sql = "select ProjectID FROM pmtrekap
		ORDER BY ProjectID DESC LIMIT 1";
				
		$query = $this->db->query($sql);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getDept(){
		$sql = "select KodeDepartemen, NamaDepartemen from departemen
				where deleted = 0
				order by KodeDepartemen";
		$query = $this->db->query($sql);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getRekapAdmin(){
		$sql = "select ProjectID, NamaProject, KPI, Target, DetilInfo, StartDate, EndDate, DeadlineUREQ, Deadline, PIC, Budget, Keterangan, FilePath, ReminderDate, Departemen, ProjectType, Status, KetAdmin, year(CreatedOn) as CreatedOn
		from pmtrekap
				where deleted = 0";
		$query = $this->db->query($sql);
			
		if($query->num_rows() > 0){
		return $query->result();
		}else{
			return false;
		}
	}
	function getApprovalRequest($KodeRequestDetail){
		$sql = "select KodeRequestDetail, ProjectID, NamaProject, KPI, Target, DetilInfo, StartDate, DeadlineUREQ, Deadline, PIC, Budget, Keterangan, ReminderDate, Departemen, Status,StatusRequest, FilePath, KetAdmin, CreatedOn,CreatedBy
		from pmtdetailrequest
				where deleted = 0 AND KodeRequestDetail = ?";
		$query = $this->db->query($sql,$KodeRequestDetail);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getLastApprovalRequest($KodeRequestDetail){
		$sql = "select KodeRequestDetail, ProjectID, NamaProject, KPI, Target, DetilInfo, StartDate, DeadlineUREQ, Deadline, PIC, Budget, Keterangan, ReminderDate, Departemen, Status,StatusRequest, KetAdmin, CreatedOn,CreatedBy
		from pmtdetailrequest
				where KodeRequestDetail = ?";
		$query = $this->db->query($sql,$KodeRequestDetail);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	function getKodeRequestDetail($KodeUserTask){
		$sql = "select NoTransaksi, KodeUserTask
		from dtltrkrwy
				where deleted = 0 AND KodeUserTask = ? 
				LIMIT 1";
		$query = $this->db->query($sql,$KodeUserTask);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getProgress(){
		$sql = "select t.ProjectID, u.ProjectID, SUM(CASE WHEN t.Status ='AP' THEN t.Presentase END) as Presentase
		from pmtprmtr t
		left join pmtrekap u on t.ProjectID=u.ProjectID
				where u.Deleted = 0
				group by t.ProjectID
				order by t.ProjectID";
		$query = $this->db->query($sql);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getProgressDetail($ProjectID){
		$sql = "select t.ProjectID, u.ProjectID, sum(ifnull(t.Presentase,0)) as Presentase
		from pmtprmtr t
		left join pmtrekap u on t.ProjectID=u.ProjectID
				where t.Status='AP' and u.Deleted = 0 and t.ProjectID=?
				group by t.ProjectID";
		$query = $this->db->query($sql,$ProjectID);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getDetailProject($ProjectID){
		$sql = "select * from pmtrekap
				where deleted = 0 and ProjectID = ?";
		$query = $this->db->query($sql,$ProjectID);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function getNamaJabatan($npk){
		
		$sql = "select u.npk,u.nama,j.NamaJabatan as jabatan,de.namadepartemen as departemen
				from mstruser u
					left join departemen de on u.departemen = de.kodedepartemen
					left join jabatan j on u.jabatan = j.KodeJabatan
				where u.npk=? and u.deleted = 0 ";
		$query = $this->db->query($sql, $npk);
			
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	function updateStatusNewPromiseRequest($KodeRequestDetail, $status, $updatedby)
	{

			$sqlupdate = "UPDATE pmtdetailrequest SET StatusRequest = ?,UpdatedOn = now(),UpdatedBy = ?
				WHERE KodeRequestDetail = ?";
				$this->db->query($sqlupdate,array($status, $updatedby,$KodeRequestDetail));
				if($this->db->affected_rows() > 0){
					return true;
				}else{
					return false;
				}

		}

	function updateStatusFinalPromiseRequest($KodeRequestDetail, $status, $updatedby, $statusProject)
		{
	
				$sqlupdate = "UPDATE pmtdetailrequest SET StatusRequest = ?, Status = ?, UpdatedOn = now(),UpdatedBy = ?
					WHERE KodeRequestDetail = ?";
					$this->db->query($sqlupdate,array($status, $statusProject, $updatedby,$KodeRequestDetail));
					if($this->db->affected_rows() > 0){
						return true;
					}else{
						return false;
					}
	
			}	
	function updateStatusParameterRequest($KodeRequestDetail, $ProjectID, $status, $updatedby)
		{
	
				$sqlupdate = "UPDATE pmtprmtrequest SET StatusRequest = ? ,ProjectID = ?, UpdatedOn = now(),UpdatedBy = ?
					WHERE KodeRequestDetail = ?";
					$this->db->query($sqlupdate,array($status,$ProjectID,$updatedby,$KodeRequestDetail));
					if($this->db->affected_rows() > 0){
						return true;
					}else{
						return false;
					}
	
			}
	function updateStatusProgressRequest($KodeRequestDetail, $ProjectID, $status, $updatedby)
			{
		
					$sqlupdate = "UPDATE pmtprmtrequest SET StatusRequest = ? , Status = ?,ProjectID = ?, UpdatedOn = now(),UpdatedBy = ?
						WHERE KodeRequestDetail = ?";
						$this->db->query($sqlupdate,array($status,$status,$ProjectID,$updatedby,$KodeRequestDetail));
						if($this->db->affected_rows() > 0 ){
							return true;
						}else{
							return false;
						}
		
				}
	function updateStatusMOMRequest($KodeRequestMOM, $ProjectID, $status, $updatedby)
			{
		
					$sqlupdate = "UPDATE pmtmomrequest SET StatusRequest = ? ,ProjectID = ?, UpdatedOn = now(),UpdatedBy = ?
						WHERE KodeRequestMOM = ?";
						$this->db->query($sqlupdate,array($status,$ProjectID,$updatedby,$KodeRequestMOM));
						if($this->db->affected_rows() > 0){
							return true;
						}else{
							return false;
						}
		
				}
	function getUpdatedData($KodeRequestDetail, $ProjectID, $status, $updatedby)
			{
				$sql = "select KoreksiParameter,KoreksiPresentase, KoreksiDeadline
				from pmtprmtrequest where KodeRequestDetail = ? and Updates <> 0";
				$query = $this->db->query($sql, $KodeRequestDetail);
					
				if($query->num_rows() > 0){
					return $query->result();
				}else{
					return false;
				}
		
			}	
	function getUpdatedMOM($KodeRequestMOM, $ProjectID, $status, $updatedby)
			{
				$sql = "select KoreksiJudul,KoreksiDeskripsi
				from pmtmomrequest where KodeRequestMOM = ? and Updates <> 0";
				$query = $this->db->query($sql, $KodeRequestMOM);
					
				if($query->num_rows() > 0){
					return $query->result();
				}else{
					return false;
				}
		
			}	
	function DeletedData($KodeRequestDetail, $ProjectID, $status, $updatedby)
			{
				$sqlupdate = "UPDATE pmtprmtrequest SET Deleted = 1
				WHERE KodeRequestDetail = ? and Deletes <> 0";
				$query = $this->db->query($sqlupdate, $KodeRequestDetail);
					
				if($this->db->affected_rows() > 0){
					return true;
				}else{
					return false;
				}

			}	
	function DeletedMOM($KodeRequestMOM, $ProjectID, $status, $updatedby)
			{
				$sqlupdate = "UPDATE pmtmomrequest SET Deleted = 1
				WHERE KodeRequestMOM = ? and Deletes <> 0";
				$query = $this->db->query($sqlupdate, $KodeRequestMOM);
					
				if($this->db->affected_rows() > 0){
					return true;
				}else{
					return false;
				}

			}				

	public function reminderPROMISE()
		{
			$searchAllDetail = "SELECT *
			from pmtrekap where deleted = 0";
			$allDetail = $this->db->query($searchAllDetail)->result();
			if($allDetail){
				foreach($allDetail as $detail){
					$flagBolehEmail = false;
					$today = date("Y-m-d");
					$startNotifikasi = $detail->StartDate;
					$startNotifikasi = strtotime($startNotifikasi);
					$firstDayNotifikasi = date("Y-m-d", $startNotifikasi);
					if($today >= $firstDayNotifikasi){
						$flagBolehEmail = true;
					}
					if($flagBolehEmail && $detail->Status<>'DONE'){

                        $ProjectID = $detail->ProjectID;
                        $NmProject = $detail->NamaProject;
                        $KPI = $detail->KPI;
                        $Target = $detail->Target;
                        $StartDate = $detail->StartDate;
                        $DeadlineUREQ = $detail->DeadlineUREQ;
                        $Deadline = $detail->Deadline;
                        $PIC = $detail->PIC;
                        $Budget = $detail->Budget;
                        $Departemen = $detail->Departemen;
						$Keterangan = $detail->Keterangan;
						$Reminder = $detail->ReminderDate;
					
						error_reporting(0);
						$this->email->from('ess@dpa.co.id', 'PROMISE');
						$ProjectProgress = $this->promise_model->getProgressDetail($ProjectID);
                        $Prog = $ProjectProgress[0]->Presentase;
						$nama = $this->promise_model->getDataUserByNama($PIC);
                        $namaAt = $this->promise_model->getAtasan($PIC);
                        $namaAta = $namaAt[0]->NamaAtasan;
                        $namaAtasan = $this->promise_model->getDataUserByNama($namaAta);
                        $CcArr=$namaAtasan[0]->email;
                        $recipientArr = array($nama[0]->email);
                        $this->email->to($recipientArr);
                        $this->email->cc($CcArr);
                        $this->email->subject('PROMISE REMINDER');
                        
                        $DeadlineUREQDat = '';
                        $BudgetDat ='';
                        $KeteranganDat ='';

                if($DeadlineUREQ<>''){
                    $DeadlineUREQDat = '<li>Deadline UREQ: <b>'.$DeadlineUREQ.'</b></li>';
                }
                if($Budget<>''){
                    $BudgetDat = '<li>Budget: <b>Rp ' .number_format($Budget,2,',','.').'</b></li>';
                }
                if($Keterangan<>''){
                    $KeteranganDat = ' <li>Keterangan: <b>'.$Keterangan.'</b></li>';
                }
                $message = 'REMINDER:
                    <br/><br/>Dear ' .$ProjectID.',<br/>
                    Harap mengerjakan project atau aktivitas dengan detail sebagai berikut :
                    <ul>
                    <li>Nama Project: <b>'.$NmProject.'</b></li>
                    <li>Target: <b>'.$Target.'</b></li>
                    <li>Start Date: <b>'.$StartDate.'</b></li>'
                    .$DeadlineUREQDat.'
                    <li>Deadline: <b>'.$Deadline.'</b></li>
                    <li>PIC: <b>'.$PIC.'</b></li>'
                    .$BudgetDat.'
                    <li>Departemen: <b>'.$Departemen.'</b></li>
                    '.$KeteranganDat.'
                    <li>Progress: <b>'.$Prog.'%</b></li>
                    <li><a href="'.base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID.'">Lihat Detail Project</a></li>
                    </ul>
                    <br/>Terima kasih
                    <br/>PROMISE
                ';
                $this->email->message($message);
				$this->email->send();
				$ReminderD = strtotime($Reminder.'+2 week');
				$ReminderDate = date("Y-m-d", $ReminderD);
				$sqlupdate = "UPDATE pmtrekap SET ReminderDate =?
				WHERE ProjectID = ? and Deleted='0'";
				$this->db->query($sqlupdate,array($ReminderDate, $ProjectID));
				if($this->db->affected_rows() > 0){
					return true;
				}else{
					return false;
				}
					}
				}
			}else{
				echo 'Tidak Email';
			}
		}
}
?>