<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Fungsi untuk mengirim pesan ke Fleep.
 *
 *
 * @author William <william.djong@gmail.com>
 *
 * @param int $kode - identifikasi setiap transaksi yang terjadi.
 * @return nomortransaksi varchar
 */

 if (!function_exists('sendFleep'))
{
	function sendFleep($message,$user) {
		try
		{
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://fleep.io/hook/1D9tIX5dSGmgYQh3rAYhkg",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "message=$message&user=$user",
				CURLOPT_HTTPHEADER => array(
					"Content-Type: application/x-www-form-urlencoded",
					"Postman-Token: 2f426e58-5530-4d76-ae88-683f6ea28860",
					"cache-control: no-cache"
				)
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err)
			{
				echo "cURL Error #:" . $err;
			}
			else
			{
				echo $response;
			}
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error sendFleep helper', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}


}