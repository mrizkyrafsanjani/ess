<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Fungsi untuk menentukan nama hari dalam bahasa indonesia. 
 *
 * @author William <william.djong@gmail.com>
 *
 * @param string $tanggal - Tanggal yang akan ditentukan harinya
 * @return string
 */
function namaHari($tanggal)
{
	$timestampTanggal = strtotime($tanggal);
	$nameDay = date('N', $timestampTanggal);
	$namaHari = '';
	switch($nameDay)
	{
		case 1: $namaHari = 'Senin'; break;
		case 2: $namaHari = 'Selasa'; break;
		case 3: $namaHari = 'Rabu'; break;
		case 4: $namaHari = 'Kamis'; break;
		case 5: $namaHari = 'Jumat'; break;
		case 6: $namaHari = 'Sabtu'; break;
		case 7: $namaHari = 'Minggu'; break;
	}
	
	return $namaHari;
}

function s_datediff( $str_interval, $dt_menor, $dt_maior, $relative=false){
	/* 
	 * A mathematical decimal difference between two informed dates 
	 *
	 * Author: Sergio Abreu
	 * Website: http://sites.sitesbr.net
	 *
	 * Features: 
	 * Automatic conversion on dates informed as string.
	 * Possibility of absolute values (always +) or relative (-/+)
	*/
   if( is_string( $dt_menor)) $dt_menor = date_create( $dt_menor);
   if( is_string( $dt_maior)) $dt_maior = date_create( $dt_maior);

   $diff = date_diff( $dt_menor, $dt_maior, ! $relative);
   
   switch( $str_interval){
	   case "y": 
		   $total = $diff->y + $diff->m / 12 + $diff->d / 365.25; break;
	   case "m":
		   $total= $diff->y * 12 + $diff->m + $diff->d/30 + $diff->h / 24;
		   break;
	   case "d":
		   $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24 + $diff->i / 60;
		   break;
	   case "h": 
		   $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + $diff->i/60;
		   break;
	   case "i": 
		   $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + $diff->s/60;
		   break;
	   case "s": 
		   $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i)*60 + $diff->s;
		   break;
	  }
   if( $diff->invert)
		   return -1 * $total;
   else    return $total;
}
	
function validateDate($date)
{
	try{
		$date = trim($date);
		$tanggalArr = explode('/',$date);
		if($date != "" && $date != null && count($tanggalArr) == 3)
		{
			$d = DateTime::createFromFormat('n/j/Y', $date);
			if($d){		
				$thedate = $d->format('n/j/Y');
				return $d && $d->format('n/j/Y') === $date;	
			}else{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	catch(Exception $er)
	{
		log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		throw new Exception( 'Something really gone wrong', 0, $e);
	}
}
?>