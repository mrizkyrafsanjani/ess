<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Fungsi untuk mengecek apakah user berhak atas page tertentu.
 *
 *
 * @author William <william.djong@gmail.com>
 *
 * @param int $kodeMenu - KodeMenu yang dilakukan pengecekan
 * @return true false
 */
 
 if (!function_exists('check_authorized'))
{
	function check_authorized($kodeMenu) {
		try{
			$ci=& get_instance();
			$ci->load->model('user','',TRUE);
			$session_data = $ci->session->userdata('logged_in');
			if($session_data['koderole'] != "1"){
				$kodeMenuAccess = $ci->user->getMenuAccess();
				if(!in_array($kodeMenu,$kodeMenuAccess))
				{
					redirect('notauthorized','refresh');
					
				}
			}
			
			return true;
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
	
	function check_authorizedByName($MenuName)
	{
		try{
			$ci=& get_instance();
			$ci->load->model('user','',TRUE);
			$session_data = $ci->session->userdata('logged_in');
			if($session_data['koderole'] != "1"){
				$MenuNameAccess = $ci->user->getMenuNameAccess();
				if(!in_array($MenuName,$MenuNameAccess))
				{
					redirect('notauthorized','refresh');					
				}
			}
			
			return true;
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}
}