<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Fungsi untuk melakuan generate nomor transaksi.
 *
 *
 * @author William <william.djong@gmail.com>
 *
 * @param int $kode - identifikasi setiap transaksi yang terjadi.
 * @return nomortransaksi varchar
 */
 
 if (!function_exists('generateNo'))
{
	function generateNo($kode) {
		try
		{
			$noTrx = "";
			$bulanFPWInt="";
			$ci=& get_instance();
			$ci->load->model('globalQuery','',TRUE);
			switch($kode)
			{
				case "DP": //datapribadi					
					$lastNo = $ci->globalQuery->getLastKode("KodeTempMstrUser","tempmstruser");
					break;
				case "UT": //usertask					
					$lastNo = $ci->globalQuery->getLastKode("KodeUserTask","usertasks");
					break;
				case "BP": //BPKB					
					$lastNo = $ci->globalQuery->getLastKode("KodeTrk","trkbpkb");
					break;
				case "LB": //lembur
					$lastNo = $ci->globalQuery->getLastKode("KodeLembur","lembur");
					break;
				case "CT": //cuti izin sakit
					$lastNo = $ci->globalQuery->getLastKode("KodeCuti","cuti");
					break;
				case "BE": //benefit
					$lastNo = $ci->globalQuery->getLastKode("KodeBenefit","benefit");
					break;
				case "AT": //ATK
					$lastNo = $ci->globalQuery->getLastKode("KodeRequestAtk","headerrequestatk");
					break;
				case "BA": //benefit Accpac
					$lastNo = $ci->globalQuery->getLastKode("KodeHeaderBenefit","benefitsendtoaccpac");
					break;
				case "TN": //Traning
					$lastNo = $ci->globalQuery->getLastKode("KodeTraining","training");
					break;
				case "TS": //Undangan
					$lastNo = $ci->globalQuery->getLastKode("KodeUndangan","undangan");
					break;
				case "RM": //RuangMeeting dan Mobil
					$lastNo = $ci->globalQuery->getLastKode("KodeRequestMobilRuangMeeting","requestmobilruangmeeting");
					break;
				case "KM": //KnowledgeManagement
					$lastNo = $ci->globalQuery->getLastKode("KodeArtikel","artikel");
					break;
				case "MC": //MasterCuti					
					$lastNo = $ci->globalQuery->getLastKode("KodeTempMasterCutiUser","tempmstrcutiuser");
					break;
				case "IS": //Issue					
					$lastNo = $ci->globalQuery->getLastKode("KodeIssue","issue");
					break;
				case "TB": //TempBudgetHeader
					$lastNo = $ci->globalQuery->getLastKode("KodeTempBudgetHeader","bm_tempbudgetheader");
					break;
				case "PR": //PROMISE Detail
					$lastNo = $ci->globalQuery->getLastKode("KodeRequestDetail","pmtdetailrequest");
					break;
				case "EP": //PROMISE Detail
					$lastNo = $ci->globalQuery->getLastKode("KodeRequestDetail","pmtdetailrequest");
					break;
				case "PM": //PROMISE Detail
					$lastNo = $ci->globalQuery->getLastKode("KodeRequestMOM","pmtmomrequest");
					break;
				case "PA": //PROMISE Detail
					$lastNo = $ci->globalQuery->getLastKode("KodeRequestDetail","pmtprmtrequest");
					break;
				case "FPW": //Formulir pelimpahan wewenang					
					$lastNo = $ci->globalQuery->getLastKode("KodePelimpahanWewenang","pelimpahanwewenang");						
					if ($lastNo != "0")
					{				
						$array = explode("-", $lastNo);					
						$bulanFPWRoman = $array[2];
						$NoUrut = (int) $array[4];
						$bulanFPWInt = $ci->globalQuery->NumberFromRoman($bulanFPWRoman);
					}
					break;
				case "AK": //
					$lastNo = $ci->globalQuery->getLastKode("KodeAkomodasiTiket","akomodasitiket");
					break;
				
			}
			fire_print('log',"lastNo = $lastNo");
		
			

			if ($kode=="FPW")
			{
				if($bulanFPWInt != date('m') || $lastNo == "0")
				{
					$NextNo = "001";	
				}
				else
				{
					$NextNo = (int)$NoUrut + 1;
				}
			}
			else
			{
				if(substr($lastNo,6,2) != date('d') || $lastNo == "0")
				{
					$NextNo = "001";	
				}
				else
				{
					$NextNo = (int)substr($lastNo,8,3) + 1;
				}
			}
			
			if ($kode=="FPW")			
				$noTrx = $kode.'-DPA-'. $ci->globalQuery->integerToRoman(date('n')).'-'.date('Y').'-'. str_pad($NextNo,3,"0",STR_PAD_LEFT);	
			else
				$noTrx = $kode. date('ymd') . str_pad($NextNo,3,"0",STR_PAD_LEFT);				
			
			return $noTrx;
		}
		catch(Exception $e)
		{
		 throw new Exception( 'Something really gone wrong', 0, $e);
		 log_message( 'error generateNo helper', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		 return false;
		}
	}

	
}