<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	class SubmitFeedback extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->library('email');
			$this->load->model('user','',TRUE);
		}
		
		 function index()
		 {
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$npk =  $session_data['npk'];
				$this->_kirim_feedback($npk);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		 }

		 function _kirim_feedback($npk)
		 {
		   $result = $this->user->dataUser($npk);

		   if($result)
		   {
			 $sess_array = array();
			 foreach($result as $row)
			 {
			   $sess_array = array(
				 'npk' => $row->npk,
				 'nama' => $row->nama,
				 'email' => $row->email
			   );
			 }
			
			$this->email->from($sess_array['email'], $sess_array['nama']);
			$this->email->to('william@dpa.co.id'); 
			
			$this->email->subject('Feedback Enterprise Self Service');
			$message = $this->input->post('message');
			$this->email->message($message);	
			
			if($this->email->send()){
				echo "Feedback Anda sudah terkirim ke pihak IT";
				return true;
			} else {
				echo "Something wrong. Hubungi IT Anda<br/>";
				echo $this->email->print_debugger();
				return false;
			}
		   }
		   else
		   {
			 echo "NPK yang Anda masukkan tidak ada di dalam database";
			 return false;
		   }
		 }
		

	}
?>