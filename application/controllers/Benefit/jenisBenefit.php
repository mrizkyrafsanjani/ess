<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class JenisBenefit extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("55"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_jenisBenefit();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _jenisBenefit()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Jenis Benefit');
		//$crud->set_theme('datatables');
		
        $crud->set_table('jenisbenefit');
		$crud->where('jenisbenefit.deleted','0');
		
		
		$crud->columns('KodeJenisBenefit', 'Deskripsi', 'CreatedOn', 'CreatedBy', 'UpdatedOn', 'UpdatedBy');
		$crud->fields('Deskripsi');
				
		$crud->required_fields('Deskripsi');
		
		$crud->callback_delete(array($this,'_delete'));		
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_update(array($this,'_update'));
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Jenis Benefit',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert($post_array)
	{
		$this->db->trans_start();
		
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		$this->db->insert('jenisbenefit',$post_array);
		
		$this->db->trans_complete(); 
		return $this->db->trans_status();
	}
	
	function _update($post_array,$primary_key)
	{
		$this->db->trans_start();
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		$this->db->update('jenisbenefit',$post_array,array('KodeJenisBenefit'=>$primary_key));
		$this->db->trans_complete(); 
		return $this->db->trans_status();
	}
	
	function _delete($primary_key){
		return $this->db->update('jenisbenefit',array('deleted' => '1'),array('KodeJenisBenefit' => $primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */