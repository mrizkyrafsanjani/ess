<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class MstrBenefitUser extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("56"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_mstrBenefitUser();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _mstrBenefitUser()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Master Benefit User');
		//$crud->set_theme('datatables');
		
        $crud->set_table('mstrbenefituser');
		$crud->where('mstrbenefituser.deleted','0');
		$crud->set_relation('NPK','mstruser','Nama',array('deleted'=>'0','TanggalBerhenti'=>'0000-00-00'));
		
		$crud->columns('NPK','Tahun','JumlahBenefitTahunanAwal','AkumulasiBenefitTahunan','UpdatedOn');
		$crud->fields('NPK','Tahun','JumlahBenefitTahunanAwal');

		$crud->display_as('JumlahBenefitTahunanAwal','Total Benefit');
		$crud->display_as('NPK','Nama');
		
		$crud->required_fields('NPK','Tahun','JumlahBenefitTahunanAwal');
		
		$crud->callback_delete(array($this,'_delete'));
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_update(array($this,'_update'));
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Master Benefit User',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
		
	function _insert($post_array)
	{
		$this->db->trans_start();
		
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		$this->db->insert('mstrbenefituser',$post_array);
		
		$this->db->trans_complete(); 
		return $this->db->trans_status();
	}
	
	function _update($post_array,$primary_key)
	{
		$this->db->trans_start();
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		$this->db->update('mstrbenefituser',$post_array,array('KodeMasterBenefitUser'=>$primary_key));
		$this->db->trans_complete(); 
		return $this->db->trans_status();
	}
	
	function _delete($primary_key){
		return $this->db->update('mstrbenefituser',array('deleted' => '1'),array('KodeMasterBenefitUser' => $primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */