<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class benefitSendToAccpac extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('benefit_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("65"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_benefitsendtoaccpac();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _benefitsendtoaccpac()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Benefit Send To Accpac');
		//$crud->set_theme('datatables');
		
        $crud->set_table('benefitsendtoaccpac');
		$crud->where('benefitsendtoaccpac.deleted','0');
		$crud->where('benefitsendtoaccpac.StatusKirim','B');
		
		
		$crud->columns('KodeBenefit','Nama','JenisBenefit','Keterangan','Jumlah');
		$crud->unset_print();
		$crud->unset_read();
		$crud->unset_export();
		
		$crud->fields('KodeBenefit','Nama','JenisBenefit','Keterangan','Jumlah');
		
		$crud->callback_field('Nama',array($this,'field_callback_Nama'));
		$crud->callback_field('JenisBenefit',array($this,'field_callback_JenisBenefit'));
		$crud->callback_field('Keterangan',array($this,'field_callback_Keterangan'));
		$crud->callback_add_field('KodeBenefit',array($this,'field_callback_KodeBenefit'));
		$crud->callback_edit_field('KodeBenefit',array($this,'field_callback_edit_KodeBenefit'));
		
		$crud->callback_column('Nama',array($this,'field_callback_column_Nama'));
		$crud->callback_column('JenisBenefit',array($this,'field_callback_column_JenisBenefit'));
		//$crud->callback_column('Keterangan',array($this,'field_callback_column_Keterangan'));
		
		$crud->required_fields('KodeBenefit','Jumlah');
		
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete'));
				
		//$crud->unset_back_to_list();
		$js = " <script>
				function loadBenefit(status){		
					var Nama = $('#Nama');
					var JenisBenefit = $('#JenisBenefit');
					var Keterangan = $('#Keterangan');
					var Jumlah = $('#field-Jumlah');
					$.ajax({
						url: '".base_url()."index.php/Benefit/benefitSendToAccpac/getBenefit',
						type: 'POST',
						data: {KodeBenefit: $('#KodeBenefit').val()},
						dataType: 'html'
					}).done(function(response){
						var data = response.split('|');
						Nama.html(data[0]);
						JenisBenefit.html(data[1]);
						if(status != 'edit'){
							Keterangan.val(data[2]);
							Jumlah.val(data[3]);
						}
					});
				}
				
				$(document).ready(function(){		
					var KodeBenefit = $('#KodeBenefit');			
					KodeBenefit.change(loadBenefit);
					if(KodeBenefit.val() != ''){
						loadBenefit('edit');
					}
				});
			</script>";
		
		$output = $crud->render();
		$output->output.= $js;
		
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			'title' => 'Benefit Send To Accpac',
			'body' => $output
		  );
		$this->load->helper(array('form','url'));
		
		$this->load->view('Benefit/benefitSimple_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert($post_array){
		fire_print('log',"benefitSendToAccpac.insert. post array: ".print_r($post_array,true));
		$data['KodeBenefit'] = $post_array['KodeBenefit'];
		$data['Jumlah'] = $post_array['Jumlah'];
		$data['Keterangan'] = $post_array['Keterangan'];
		$data['StatusKirim'] = 'B'; 
		$data['CreatedOn'] = date('Y-m-d H:i:s');
		$data['CreatedBy'] = $this->npkLogin;
		fire_print('log',"benefitSendToAccpac.insert. KodeBenefit: ".$data['KodeBenefit']);
		
		$this->db->trans_begin();
		if(!$this->db->insert('benefitsendtoaccpac',$data))
		{
			fire_print('log','insert benefitsendtoaccpac rollback');
			$this->db->trans_rollback();
			return false;
		}else{
			fire_print('log','insert benefitsendtoaccpac commit');
			$this->db->trans_commit();
			return true;
		}
	}
	
	function _update($post_array,$primary_key){
		fire_print('log',"post array update: ". print_r($post_array,true));
		$data['Jumlah'] = $post_array['Jumlah'];
		$data['Keterangan'] = $post_array['Keterangan'];
		$data['UpdatedOn'] = date('Y-m-d H:i:s');
		$data['UpdatedBy'] = $this->npkLogin;
		
		$this->db->trans_start();
		
		$this->db->update('benefitsendtoaccpac',$data,array('KodeBenefitSendToAccpac' => $primary_key));
		
		$this->db->trans_complete(); 
		return $this->db->trans_status();
		
	}
	
	function _delete($primary_key){		
		
		return $this->db->update('benefitsendtoaccpac',array('deleted' => '1'),array('KodeBenefitSendToAccpac' => $primary_key));
		
	}
	
	
	
	function field_callback_KodeBenefit($value='',$primary_key=null)
	{
		$listKodeBenefitBlmKirim = $this->benefit_model->getBenefitBelumKirim();		
		$strHTML = "<select id='KodeBenefit' name='KodeBenefit' class='chosen-select'>
			<option value=''>Pilih Kode Benefit</option>";
		fire_print('log','$valueKodeKeluarga: '.$value);		
		if($listKodeBenefitBlmKirim)
		{
			foreach($listKodeBenefitBlmKirim as $rw){
				$selected = "";
				if($rw->KodeBenefit == $value){
					$selected = "selected";
				}
				$strHTML .= "<option value='$rw->KodeBenefit' $selected>$rw->KodeBenefit</option>";
			}			
		}
		$strHTML .= "</select>";
		fire_print('log','$strHTML: '.$strHTML);
		return $strHTML;
	}
	
	function field_callback_edit_KodeBenefit($value='',$primary_key=null){
		return "<select id='KodeBenefit' name='KodeBenefit' class='chosen-select'>
			<option value='$value'>$value</option></select>";
	}
	
	function field_callback_JenisBenefit($value='',$primary_key=null){
		return "<div id='JenisBenefit'>$value</div>";
	}
	
	function field_callback_Keterangan($value='',$primary_key=null){
		return "<input type='text' id='Keterangan' name='Keterangan' value='$value'>";
	}
	
	function field_callback_Nama($value='',$primary_key=null){
		return "<div id='Nama'>$value</div>";
	}
	
	function field_callback_column_Nama($value,$row)
	{
		fire_print('log','row:'.print_r($row,true));
		$nama = "";
		$dataBenefit = $this->benefit_model->getDataBenefit($row->KodeBenefit);
		foreach($dataBenefit as $row){
			$nama = $row->Nama;
		}
		return $nama;
	}
	
	function field_callback_column_JenisBenefit($value,$row)
	{
		$jenisbenefit = "";
		$dataBenefit = $this->benefit_model->getDataBenefit($row->KodeBenefit);
		foreach($dataBenefit as $row){
			$jenisbenefit = $row->JenisBenefit;
		}
		return $jenisbenefit;
	}
	
	
	
	function field_callback_column_Keterangan($value,$row)
	{
		fire_print('log',"fieldcallback kolom keterangan. row:". print_r($row,true));
		$keterangan = "";
		$dataBenefit = $this->benefit_model->getDataBenefit($row->KodeBenefit);
		foreach($dataBenefit as $rw){
			$keterangan = $this->benefit_model->createKeteranganBenefit($rw->JenisBenefit,$rw->Nama);
		}
		return $keterangan;
	}
	
	
	function getBenefit(){
		$KodeBenefit = $_POST['KodeBenefit'];
		$hasil = "";
		$dataBenefit = $this->benefit_model->getDataBenefit($KodeBenefit);
		if($dataBenefit){
			foreach($dataBenefit as $rw){
				$hasil.= $rw->Nama.'|'.$rw->JenisBenefit.'|'.$this->benefit_model->createKeteranganBenefit($rw->JenisBenefit,$rw->Nama).'|'.$rw->Jumlah;				
			}
		}
		echo $hasil;
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */