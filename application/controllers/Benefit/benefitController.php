<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class BenefitController extends CI_Controller{
		var $npkLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('user','',TRUE);
			$this->load->model('benefit_model','',TRUE);
			$this->load->model('usertask','',TRUE);
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
		}
		
		function index() //viewHR untuk all karyawan
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("58"))
					{
						$data = array(
							'title' => 'View Benefit All Karyawan',
							'admin' => '1',
							'databawahan' => $this->user->getDataBawahan($this->npkLogin),
							'npk' => ''
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Benefit/benefit_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function BenefitSendToAccpac(){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("65"))
					{
						$data = array(
							'title' => 'Send To Accpac',
							'admin' => '1'
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Benefit/benefitSendToAccpac_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function sendBankEntry($a,$b){
			//$a = "BPT03;1;7;Test Will Web ESS 2014-12-18;12/01/2014;1;8207010000;167,800.00;;Penggantian p;HRGA-KARYAWA";
			//$b = ';';
			$hasil = "0";
			if($this->config->item('enableWebService') == 'true'){
				$url = $this->config->item('urlWebService');
				$client = new SoapClient($url);
				$params = array(
					"Bank" => $a,
					"Delimiter" => $b
				);
				fire_print('log',"nilai params: ".print_r($params,true));
				$result = $client->AccpacBankEntryString($params);
				$hasil = $result->AccpacBankEntryStringResult;
				fire_print('log',print_r($result,true));
			}
			return $hasil;
		}
		
		function getKodeBEAccpac($a){
			$hasil = "";
			if($this->config->item('enableWebService') == 'true'){
				$url = $this->config->item('urlWebService');
				$client = new SoapClient($url);
				$params = array(
					"Deskripsi" => $a
				);
				fire_print('log',"nilai params: ".print_r($params,true));
				$result = $client->GetKodeBE($params);
				$hasil = $result->GetKodeBEResult;
				fire_print('log',print_r($result,true));
			}
			return $hasil;
		}
		
		function ajax_prosesSendToAccpac(){
			$error = 0;
			$deskripsiHeader = $this->input->post('deskripsi');
			$dataListKirimKeAccpac = $this->db->query("SELECT bsa.KodeBenefitSendToAccpac, bsa.Jumlah,bsa.Keterangan, b.KodeJenisBenefit,u.Nama FROM benefitsendtoaccpac bsa JOIN benefit b on bsa.KodeBenefit = b.KodeBenefit JOIN mstruser u on u.NPK = b.NPK
  WHERE bsa.Deleted = 0 AND bsa.StatusKirim = 'B'");
			if($dataListKirimKeAccpac->num_rows() > 0){
				$this->db->trans_begin();
				$noHeaderBenefitAccpac = generateNo("BA");
				$stringSendToAccpac = "BPT03;1;7;$deskripsiHeader ($noHeaderBenefitAccpac);".date('m/d/Y');
				$stringSendToAccpac .= ";".$dataListKirimKeAccpac->num_rows();
				foreach($dataListKirimKeAccpac->result() as $rw){
					$strKeterangan = $rw->Keterangan;
					$stringSendToAccpac .= ";8102010000;".$rw->Jumlah.";;".$strKeterangan.";HRGA-KARYAWA";
					if(!$this->db->update('benefitsendtoaccpac',array('StatusKirim'=>'S','KodeHeaderBenefit'=>$noHeaderBenefitAccpac),array('KodeBenefitSendToAccpac'=>$rw->KodeBenefitSendToAccpac)))
					{
						$error += 1;
					}
				}
			
				fire_print("log","stringSendToAccpac: $stringSendToAccpac");
				$resultWS = $this->sendBankEntry($stringSendToAccpac,";");
				if($resultWS != "1"){
					$error += 1;
				}
				else
				{
					$KodeBEAccpac = trim($this->getKodeBEAccpac($noHeaderBenefitAccpac)," \"");
					if(!$this->db->update('benefitsendtoaccpac',array('KodeBEAccpac'=>$KodeBEAccpac),array('KodeHeaderBenefit'=>$noHeaderBenefitAccpac))){
						$error += 1;
					}
				}
				
				if($error == 0){
					fire_print('log','ajax_prosesSendToAccpac commit');
					$this->db->trans_commit();
					echo "Sukses. Kode BE pada Accpac adalah $KodeBEAccpac";
				}else{
					fire_print('log','ajax_prosesSendToAccpac rollback');
					$this->db->trans_rollback();
					echo "Gagal. $stringSendToAccpac";
				}
			}
			else
			{
				echo "Tidak ada data yang dikirim. Harap pilih data yang akan dikirim ke Accpac";
			}
		}
		
		function ViewUser($KodeUserTask='') //ViewUser
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("54"))
					{
						if($KodeUserTask == '')
							$KodeUserTask = 'non';
						$data = array(
							'title' => 'View Benefit User',
							'admin' => '0',
							'npk' => $this->npkLogin,
							'kodeusertask' => $KodeUserTask
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Benefit/benefit_view',$data);
					}
				}
				else
				{
					redirect('login?u=Benefit/benefitController/ViewUser','refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function tidakbisa_ubah()
		{
			$this->session->set_flashdata('msg','Tidak bisa ubah, proses ubah hanya bisa dilakukan jika rencana cuti di decline');
			redirect('cuti/cuti/viewuser');
		}		
		
		function ApprovalBenefit($KodeUserTask)
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("74"))
					{
						$datacuti = $this->benefit_model->getBenefitKodeUserTask($KodeUserTask);
						foreach($datacuti as $row){
							$historycatatan = $this->usertask->getCatatan($row->KodeBenefit);
							fire_print('log',"ApprovalBenefit. KodeBenefit:$row->KodeBenefit. $historycatatan");
							$data = array(
								'Nama' => $row->Nama,
								'JenisBenefit' => $row->Deskripsi,
								'TanggalKlaim' => $row->TanggalKlaim,
								'KlaimUntuk' => $row->NamaKeluarga == null ? $row->Nama : $row->NamaKeluarga,
								'HubunganKeluarga' => $row->HubunganKeluarga ==  null ? 'Pribadi' : $row->HubunganKeluarga,
								'TotalKlaim' => $row->TotalKlaim,
								'TotalBayar' => $row->TotalBayar,
								'SisaJatahMedical' => $row->SisaJatahMedical,
								'Keterangan' => $row->Keterangan,
								'title' => 'Approval Realisasi Benefit',
								'kodeusertask' => $KodeUserTask,
								'npk' => $row->NPK,
								'nama' => $row->Nama,						
								'historycatatan' => $historycatatan
							);
						}
						$this->load->helper(array('form','url'));
						$this->template->load('default','Benefit/approvalBenefit_view',$data);
					}
				}
				else
				{
					redirect("login?u=Benefit/benefitController/ApprovalBenefit/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_prosesGenerateCuti()
		{
			$jumlahCutiTahunanAwal = 0;
			$JumlahCutiBesarAwal = 0;
			$akumulasiCutiTahunan = 0;
			$akumulasiCutiBesar = 0;
			$akumulasiCutiTahunLalu = 0;
			
			try
			{
				$success = "Berhasil";
				$tahunInput = $this->input->post('tahun');
				$globalParamlist = $this->db->get_where('globalparam',array('Name'=>'tanggalPenentuanBulanMasukCuti'));
				$tglPenentuCuti = $globalParamlist->row(1)->Value;
				fire_print('log','TanggalPenentuCuti :'.$tglPenentuCuti);
				$this->db->trans_begin();
				//cari semua karyawan, lalu masukkan. Validasi apakah tanggal sudah masuk 
				$dataUserList = $this->db->query("SELECT NPK,TanggalBekerja FROM mstruser 
					WHERE Deleted = 0 AND (TanggalBerhenti is null OR TanggalBerhenti = '0000-00-00')");
				fire_print('log','dataUserList : '. print_r($dataUserList->result(),true));
				foreach($dataUserList->result() as $dataUser)
				{
					$jumlahCutiTahunanAwal = 0;
					$JumlahCutiBesarAwal = 0;
					$akumulasiCutiTahunan = 0;
					$akumulasiCutiBesar = 0;
					$tglBekerja = strtotime($dataUser->TanggalBekerja);
					if($tglPenentuCuti != 'now')
					{
						if(date('d',$tglBekerja) >= $tglPenentuCuti)
						{
							$tglBekerja = strtotime($dataUser->TanggalBekerja . ' first day of next month');
							fire_print('log','Not Now tgl Bekerja :'.date('Y-m-d',$tglBekerja));
						}
					}
					$tahunTanggalBekerja = date('Y',$tglBekerja);
					//fire_print('log','Tahun Tanggal Bekerja :'. $tahunTanggalBekerja);
					
					if($tahunTanggalBekerja == $tahunInput)
					{
						$jumlahCutiTahunanAwal = 0;
					}
					else if($tahunInput - $tahunTanggalBekerja > 1)
					{
						$jumlahCutiTahunanAwal = 12;
					}
					else if($tahunInput - $tahunTanggalBekerja == 1)
					{
						$jumlahCutiTahunanAwal = 13 - date('m',$tglBekerja);
					}
					fire_print('log','jumlahCutiTahunanAwal:'.$jumlahCutiTahunanAwal);
					
					$listCutiBersama = $this->db->get_where('harilibur',array('Deleted'=>'0','Hari'=>'cb','YEAR(Tanggal)'=>$tahunInput));
					$jumlahCutiBersama = $listCutiBersama->num_rows();
					
					fire_print('log','jumlahCutiBersama:'.$jumlahCutiBersama);
					
					//Hitung jumlah cuti karyawan yang hutang cuti pada tahun input.
					$jumlahHutangCuti = 0;					
					$listHutangCuti = $this->cutiJenisTidakHadir_model->getHutangCutiTahunan($dataUser->NPK,$tahunInput);
					if($listHutangCuti)
					{
						foreach($listHutangCuti as $hutangCuti)
						{
							$jumlahHutangCuti += $this->cuti_model->getHariKerja($hutangCuti->TanggalMulai,$hutangCuti->TanggalSelesai);
						}
					}
					
					/* $listCutiUserTahunLalu = $this->db->get_where('mstrcutiuser',array('Deleted'=>'0','NPK'=>$dataUser->NPK,'Tahun'=>$tahunInput-1));
					if($listCutiUserTahunLalu->num_rows() > 0)
					{
						$akumulasiCutiTahunLalu = $listCutiUserTahunLalu->row(1)->AkumulasiCutiTahunan;
						$akumulasiCutiBesar = $listCutiUserTahunLalu->row(1)->AkumulasiCutiBesar;
					} */
					
					$akumulasiCutiTahunan = $jumlahCutiTahunanAwal - $jumlahCutiBersama - $jumlahHutangCuti;
					fire_print('log','akumulasiCutiTahunan:'.$akumulasiCutiTahunan);
					//hitung apakah dapat cuti besar? jika tahun input - tahun masuk lalu mod 5 = 0, maka berarti dapat cuti besar
					fire_print('log',"penentu cuti besar.Rms: ( $tahunInput - $tahunTanggalBekerja ) % 5 = ". ($tahunInput - $tahunTanggalBekerja)%5 );
					$success .= ("\n $dataUser->NPK : ( $tahunInput - $tahunTanggalBekerja ) % 5 = ". ($tahunInput - $tahunTanggalBekerja)%5);
					if(($tahunInput - $tahunTanggalBekerja)%5 == 0)
					{
						$JumlahCutiBesarAwal = 22;
						$akumulasiCutiBesar = 22;
					}
					
					if($akumulasiCutiTahunan < 0)
					{
						if($listHutangCuti)
						{
							foreach($listHutangCuti as $hutangCuti)
							{
								if($akumulasiCutiTahunan < 0)
								{
									$this->db->update('cutijenistidakhadir',array('TahunCutiYangDipakai'=>$tahunInput+1),array('KodeCutiJenisTidakHadir'=>$hutangCuti->KodeCutiJenisTidakHadir));
								}
								$akumulasiCutiTahunan++;
							}
						}
						$akumulasiCutiTahunan = 0;
					}
					
					$data = array(
						"NPK"=> $dataUser->NPK,
						"Tahun" =>$tahunInput,
						"JumlahCutiTahunanAwal"=>$jumlahCutiTahunanAwal, 
						"AkumulasiCutiTahunan"=>$akumulasiCutiTahunan, 
						"JumlahCutiBesarAwal"=>$JumlahCutiBesarAwal, 
						"AkumulasiCutiBesar"=>$akumulasiCutiBesar, 
						"CreatedOn"=> date('Y-m-d H:i:s'), 
						"CreatedBy" => $this->npkLogin
					);
					$this->db->insert('mstrcutiuser',$data);					
				}
				$resultQ1 = $this->db->query("insert into cuti(KodeCuti,Deleted,NPK,TanggalMulai,TanggalSelesai,StatusApproval,CreatedOn,CreatedBy) 
				SELECT CONCAT('CB', DATE_FORMAT(l.Tanggal, '%y%m%d'), LEFT(NPK, 3))
						  AS KodeCuti,
					   '0' AS Deleted,
					   NPK,
					   l.Tanggal AS TanggalMulai,
					   l.Tanggal AS TanggalSelesai,
					   'APR' AS StatusApproval,
					   now() AS createdOn,
					   'Sys-CB' AS CreatedBy
				  FROM mstruser u CROSS JOIN harilibur l
				 WHERE     u.Deleted = 0
					   AND (u.TanggalBerhenti = '0000-00-00' OR u.TanggalBerhenti IS NULL)
					   AND l.Deleted = 0
					   AND l.Hari = 'cb'
					   AND YEAR(l.Tanggal) = ?
					   AND u.NPK NOT LIKE 'OS%'",$tahunInput);
				
				$resultQ2 = $this->db->query("insert into cutijenistidakhadir(Deleted,KodeCuti,KodeJenisTidakHadir,TahunCutiYangDipakai,Lokasi,TanggalMulai,TanggalSelesai,CreatedOn,CreatedBy)
				SELECT '0' AS Deleted,
					   CONCAT('CB', DATE_FORMAT(l.Tanggal, '%y%m%d'), LEFT(NPK, 3))
						  AS KodeCuti,
					   12 AS KodeJenisTidakHadir,
					   YEAR(CURDATE()) AS TahunCutiYangDipakai,
					   '' AS Lokasi,
					   l.Tanggal AS TanggalMulai,
					   l.Tanggal AS TanggalSelesai,
					   now() AS createdOn,
					   'Sys-CB' AS CreatedBy
				  FROM mstruser u CROSS JOIN harilibur l
				 WHERE     u.Deleted = 0
					   AND (u.TanggalBerhenti = '0000-00-00' OR u.TanggalBerhenti IS NULL)
					   AND l.Deleted = 0
					   AND l.Hari = 'cb'
					   AND YEAR(l.Tanggal) = ?
					   AND u.NPK NOT LIKE 'OS%'",$tahunInput);
				//$this->db->trans_rollback();
				$this->db->trans_commit();
				
				echo $success;
			}
			catch(Exception $e)
			{
				$this->db->trans_rollback();
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function EditBenefit($KodeUserTask)
		{
			if($this->session->userdata('logged_in'))
			{
				$dataBenefit = $this->benefit_model->getBenefitKodeUserTask($KodeUserTask);
				foreach($dataBenefit as $dt)
				{
					$KodeBenefit = $dt->KodeBenefit;
				}
				redirect('/Benefit/benefit/index/edit/'.$KodeBenefit, 'refresh');
			}
			else
			{
				redirect("login?u=Benefit/benefitController/EditBenefit/$KodeUserTask","refresh");
			}
		}
		
		function ajax_cancelCuti()
		{
			try
			{
				$success = "Berhasil";
				$npk = $this->input->post('npk');
				$this->db->trans_start();
				
				$this->db->delete('cutijenistidakhadir',array('CreatedBy'=>$npk,'KodeCuti'=>''));
				
				$this->db->trans_complete();
				
				if ($this->db->trans_status() === FALSE)
				{
					$success = "Gagal";
				}
				
				echo $success;
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_prosesApproval()
		{
			try
			{
				$error = 0;
				$success = "Gagal melakukan proses approval";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				$totalBayar = $this->input->post('totalbayar');
				$query = $this->benefit_model->getBenefitKodeUserTask($KodeUserTask);
				if($query){
					foreach($query as $row){
						$KodeBenefit = $row->KodeBenefit;
						$pengajuBenefit = $row->CreatedBy;
						$statusBenefit = $row->StatusApproval;
						$KodeJenisBenefit = $row->KodeJenisBenefit;
						$totalKlaim = $row->TotalKlaim;
					}
					if($totalBayar > $totalKlaim){
						$error += 1;
						$success = "Nilai Total Bayar tidak boleh melebihi total klaim";
					} 
					else if($statusBenefit == 'PE')
					{
						$this->db->trans_begin();
						
						//update status benefit jika sudah max sequence
						if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'BE') && $status == 'AP')
						{
							$this->benefit_model->updateStatusBenefit($KodeUserTask,$status,$this->npkLogin);
							
							if($KodeJenisBenefit == 1) //1 -> medical
							{
								if($this->benefit_model->updateAkumulasiBenefit($KodeUserTask,$this->npkLogin)){
									$success = "Berhasil";
									
									$this->usertask->sendEmailNotifikasi($pengajuBenefit,'BEAP',"Klaim Benefit sudah diapprove",$pengajuBenefit);
								}
								else
								{
									$error += 1;
									$success = "Sistem Gagal";
								}
							}else{
								$success = "Berhasil";
								
								$this->usertask->sendEmailNotifikasi($pengajuBenefit,'BEAP',"Klaim Benefit sudah diapprove",$pengajuBenefit);
							}
							
						}
						else if($status == 'AP')
						{
							//create user task baru untuk next proses
							$KodeUserTaskNext = generateNo('UT');			
							if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeBenefit,$KodeUserTaskNext,"BE".$status,$pengajuBenefit))
							{
								$error += 1;
								$success = "Pembuatan user task baru pada proses approval benefit mengalami kegagalan";
							}else{
								$success = "Berhasil";
							}
						}
						
						if($status == 'AP')
						{
							if(!$this->db->update('benefit',array('TotalBayar'=>$totalBayar),array('KodeBenefit'=>$KodeBenefit)))
							{
								$error +=1;
							}
						}
						
						if($status == 'DE')
						{
							$this->benefit_model->updateStatusBenefit($KodeUserTask,$status,$this->npkLogin);
							$KodeUserTaskBaru = generateNo('UT');
							$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeBenefit,$KodeUserTaskBaru,"BEDE",$pengajuBenefit);
							fire_print('log',$hasilTambahUserTask);
							if(!$hasilTambahUserTask)
							{
								$error += 1;
								$success = "Gagal Decline";
								fire_print('log','insert decline benefit rollback');
							}else{
								$success = "Berhasil Decline";
								fire_print('log','insert decline benefit commit');
							}
							
							if(!$this->db->update('benefit',array('TotalBayar'=>0),array('KodeBenefit'=>$KodeBenefit)))
							{
								$error +=1;
							}
						}
						
						//update user task sekarang
						$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin, $catatan);
						
						if ($error > 0)
						{
							fire_print('log','trans rollback approve benefit ');
							$this->db->trans_rollback();
						}
						else
						{
							fire_print('log','trans commit approve benefit ');
							$this->db->trans_commit();
						}
						fire_print('log','Proses Trans Approve benefit selesai ');
					}
				}else{
					$success = "Tidak ada data";
				}
				echo $success;
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		/* function ajax_prosesApprovalRealisasi()
		{
			try
			{
				$success = "Gagal";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				
				$query = $this->benefit_model->getBenefitKodeUserTask($KodeUserTask);
				if($query){
					foreach($query as $row){
						//$statusRencana = 'R'; //substr($row->StatusApproval, -1);
						$KodeBenefit = $row->KodeBenefit;
						$pengajuBenefit = $row->NPK;						
					}
					fire_print('log',"pengajuBenefit : $pengajuBenefit");
					
					//update status benefit
					if($this->benefit_model->updateStatusBenefit($KodeUserTask,$status,$this->npkLogin))
					{
						$this->db->trans_begin();
						//update user task sekarang
						$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan);
						
						//update akumulasi cuti jika Approve
						if($status == "AP"){
							if($this->benefit_model->updateAkumulasiBenefit($KodeUserTask,$this->npkLogin)){
								$this->db->trans_commit();
								$success = "Berhasil";
							}else{
								$this->db->trans_rollback();
								$success = "Sistem Gagal";
							}
						}else if($status == "DE"){
							$KodeUserTaskBaru = generateNo('UT');
							$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeBenefit,$KodeUserTaskBaru,"BEDE",$pengajuBenefit);
							fire_print('log',$hasilTambahUserTask);
							if(!$hasilTambahUserTask)
							{
								$this->db->trans_rollback();
								$success = "Gagal Decline";
								fire_print('log','insert decline benefit rollback');
							}else{
								$this->db->trans_commit();
								$success = "Berhasil Decline";
								fire_print('log','insert decline benefit commit');
							}
						}
					}
					else
					{
						$success = "Sistem Gagal";
					}
				}else{
					$success = "Tidak ada data";
				}
				echo $success;				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		} */
	}
?>