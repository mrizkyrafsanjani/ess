<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Benefit extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('benefit_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("57"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_benefit();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function viewUser($tahun='',$bulan='',$nama='',$NPKuser='',$KodeUserTask='')
	{
		if($KodeUserTask == 'non')
			$KodeUserTask = '';
		if($nama == 'non')
			$nama = '';
		if($tahun == 'non')
			$tahun = date('Y');
		if($bulan == 'non')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("54"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_benefit('viewUser',$nama,$tahun,$bulan,$KodeUserTask);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function viewAdmin($tahun='',$bulan='',$nama='non')
	{
		if($nama == 'non')
			$nama = 'xxx';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("58"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_benefit('viewAdmin',$nama,$tahun,$bulan);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function getJabatan($NPK)
	{
		$dataUser = $this->user->dataUser($NPK);
		foreach($dataUser as $dtU){
			$jabatanUserLogin = $dtU->KodeJabatan;
		}
		return $jabatanUserLogin;
	}
	
	public function _benefit($page='',$nama='',$tahun='',$bulan='',$KodeUserTask='')
    {
		$jabatanUserLogin = $this->getJabatan($this->npkLogin);
		fire_print('log',"benefit.load jabatanUserLogin : $jabatanUserLogin. page = $page, nama=$nama,tahun=$tahun,bulan=$bulan,KodeUserTask = $KodeUserTask");
		$crud = new grocery_crud();
		$crud->set_subject('Benefit');
		//$crud->set_theme('datatables');
		
        $crud->set_table('benefit');
		$crud->where('benefit.deleted','0');
		if($jabatanUserLogin == 26){ //jika HR Analyst
			$crud->set_relation('NPK','mstruser','Nama',array('deleted'=>'0','TanggalBerhenti'=>'0000-00-00'));
			$crud->set_relation('KodeJenisBenefit','jenisbenefit','Deskripsi',array('deleted'=>'0'));
		}else{
			$crud->set_relation('NPK','mstruser','Nama',array('deleted'=>'0','TanggalBerhenti'=>'0000-00-00','NPK'=>$this->npkLogin));
			$crud->set_relation('KodeJenisBenefit','jenisbenefit','Deskripsi',array('deleted'=>'0','Deskripsi'=>'Medical'));
		}
		
		$crud->set_relation('KodeKeluarga','keluarga','Nama',array('deleted'=>'0','JenisKeluarga'=>'I'));
		
		if($nama != '')
		{
			$crud->like('benefit.NPK',$nama);
		}
		if($tahun != '')
		{
			$crud->where('YEAR(benefit.TanggalKlaim)',$tahun);
		}
		if($bulan != 'all')
		{
			$crud->where('MONTH(benefit.TanggalKlaim)', $bulan);
		}
		
		$crud->columns('NPK', 'KodeJenisBenefit','TanggalKlaim','KodeKeluargaKlaim','HubunganKeluarga','TotalKlaim','TotalBayar','Keterangan','StatusApproval','CreatedOn');
		
		switch($page)
		{
			case "viewAdmin":
				$crud->unset_add();				
				$crud->unset_print();
				$crud->unset_read();
				break;
			case "viewUser":
				$crud->where('benefit.NPK',$this->npkLogin);
				$crud->set_theme('datatables');				
				$crud->unset_add();
				$crud->unset_edit();
				$crud->unset_print();
				$crud->unset_read();
				$crud->unset_export();
				//$crud->add_action('Ubah','','','ui-icon-image',array($this,'callback_action_ubah'));
				break;
		}
		
		$crud->fields('NPK', 'KodeJenisBenefit','TanggalKlaim','KodeKeluarga','HubunganKeluarga','TotalKlaim','SisaJatahMedical','Keterangan');
		
		$crud->edit_fields('NPK', 'KodeJenisBenefit','TanggalKlaim','KodeKeluarga','HubunganKeluarga','TotalKlaim','SisaJatahMedical','Keterangan','Catatan');
		
		$crud->display_as('NPK','Nama Karyawan');
		$crud->display_as('KodeJenisBenefit','Jenis Benefit');
		$crud->display_as('TanggalKlaim','Tanggal Klaim');
		$crud->display_as('KodeKeluarga','Klaim Untuk');
		$crud->display_as('KodeKeluargaKlaim','Klaim Untuk');
		$crud->display_as('HubunganKeluarga','Hubungan Keluarga');
		$crud->display_as('TotalKlaim','Total Klaim');
		$crud->display_as('SisaJatahMedical','Sisa Jatah Medical');
		
		$crud->callback_field('KodeKeluarga',array($this,'field_callback_KodeKeluarga'));
		$crud->callback_field('HubunganKeluarga',array($this,'field_callback_HubunganKeluarga'));
		$crud->callback_field('SisaJatahMedical',array($this,'field_callback_SisaJatahMedical'));
		$crud->callback_field('Catatan',array($this,'field_callback_Catatan'));
		
		$crud->callback_column('KodeKeluargaKlaim',array($this,'field_callback_column_KodeKeluargaKlaim'));
		$crud->callback_column('HubunganKeluarga',array($this,'field_callback_column_HubunganKeluarga'));
		$crud->callback_column('StatusApproval',array($this,'field_callback_column_StatusApproval'));
		$crud->callback_column('TotalKlaim',array($this,'field_callback_column_TotalKlaim'));
		$crud->callback_column('TotalBayar',array($this,'field_callback_column_TotalBayar'));
		
		$crud->required_fields('NPK', 'KodeJenisBenefit','TanggalKlaim','KodeKeluarga','TotalKlaim');
		
		$crud->set_rules('KodeJenisBenefit','Jenis Benefit','callback_validasiJenisBenefit');
		
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete'));
		
		$crud->unset_texteditor('Keterangan');
		$crud->unset_back_to_list();
		$js = " <script>
				function loadSisaJatahMedical(){
					var NPK = $('#field-NPK');
					var SisaJatahMedical = $('#SisaJatahMedical');
					$.ajax({
						url: '".base_url()."index.php/Benefit/Benefit/getSisaJatahMedical',
						type: 'POST',
						data: {NPK: NPK.val()},
						dataType: 'html'
					}).done(function(response){
						SisaJatahMedical.val(response);
					});
				}
		
				function loadKeluarga(){
					var NPK = $('#field-NPK');
					var Keluarga = $('#divKodeKeluarga');
					var HubunganKeluarga = $('#HubunganKeluarga');
					$.ajax({
						url: '".base_url()."index.php/Benefit/Benefit/getKeluargaMedical',
						type: 'POST',
						data: {NPK: NPK.val()},
						dataType: 'html'
					}).done(function(response){
						Keluarga.html(response);
						HubunganKeluarga.html('Pribadi');
					});
					
					loadSisaJatahMedical();
				} 
				
				function loadHubunganKeluarga(){
					var HubunganKeluarga = $('#HubunganKeluarga');
					$.ajax({
						url: '".base_url()."index.php/Benefit/Benefit/getHubunganKeluarga',
						type: 'POST',
						data: {KodeKeluarga: $('#KodeKeluarga').val()},
						dataType: 'html'
					}).done(function(response){
						HubunganKeluarga.html(response);
					});
				}
				
				function changeJenisBenefit(){
					var JenisBenefit = $('#field-KodeJenisBenefit');
					if(JenisBenefit.val() == 1) //Medical
					{
						$('#SisaJatahMedical_field_box').show();
					}
					else
					{
						$('#SisaJatahMedical_field_box').hide();
					}
				}
				
				$(document).ready(function(){		
					var NPK = $('#field-NPK');
					var Keluarga = $('#divKodeKeluarga');
					var JenisBenefit = $('#field-KodeJenisBenefit');
					if(NPK.val() != ''){
						loadHubunganKeluarga();
						loadSisaJatahMedical();
						changeJenisBenefit();
					}
					NPK.change(loadKeluarga);
					Keluarga.change(loadHubunganKeluarga);
					JenisBenefit.change(changeJenisBenefit);
					
				});
			</script>";
		
		$output = $crud->render();
		$output->output.= $js;
		
        $this-> _outputview($output,$page);        
    }
 
    function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Benefit Karyawan',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		if($page!='')
		{
			$this->load->view('Benefit/benefitSimple_view',$data);
		}
		else
		{
			$this->template->load('default','templates/CRUD_view',$data);
		}
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert($post_array){
		$post_array['SisaJatahMedical'] = str_replace(".","",$post_array['SisaJatahMedical']);
		$post_array['KodeBenefit'] = generateNo('BE');
		$post_array['StatusApproval'] = 'PE'; 
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		fire_print('log',"benefit.insert. Sisa jatah medical ".$post_array['SisaJatahMedical'] . ". KodeBenefit : ". $post_array['KodeBenefit']);
		if(($post_array['SisaJatahMedical'] >= 0 && $post_array['KodeJenisBenefit'] == 1) || $post_array['KodeJenisBenefit'] <> 1){
			$this->db->trans_begin();
			
			if($post_array['KodeJenisBenefit'] <> 1)
				$post_array['TotalBayar'] = $post_array['TotalKlaim'];
			
			$this->db->insert('benefit',$post_array);
			
			$KodeBenefit = $post_array['KodeBenefit'];
			
			$KodeUserTask = generateNo('UT');
			$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeBenefit,$KodeUserTask,"BE");
			fire_print('log',$hasilTambahUserTask);
			if(!$hasilTambahUserTask)
			{
				//echo('gagal simpan usertask benefit');
				fire_print('log','insert benefit rollback');
				$this->db->trans_rollback();
				return false;
			}else{
				fire_print('log','insert benefit commit');
				$this->db->trans_commit();
				return true;
			}
		}else{
			return false;
		}
	}
	
	function _update($post_array,$primary_key){	
		$error = 0;
		$query = $this->db->get_where('benefit',array('KodeBenefit'=>$primary_key));
		$databenefit = $query->row(1);
		if($databenefit->StatusApproval == 'DE'){
			$post_array['SisaJatahMedical'] = str_replace(".","",$post_array['SisaJatahMedical']);
			$post_array['StatusApproval'] = 'PE'; //Pending Plan
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			if(($post_array['SisaJatahMedical'] >= 0 && $post_array['KodeJenisBenefit'] == 1) || $post_array['KodeJenisBenefit'] <> 1){
				$this->db->trans_begin();
				if($post_array['KodeJenisBenefit'] <> 1)
					$post_array['TotalBayar'] = $post_array['TotalKlaim'];
				if(!$this->db->update('benefit',$post_array,array('KodeBenefit' => $primary_key)))
					$error += 1;
				$KodeBenefit = $primary_key;
				//usertask lama di nyatakan statusnya AP
				$usertasklama = $this->usertask->getLastUserTask($KodeBenefit);
				$KodeUserTaskLama = '';
				if($usertasklama){
					foreach($usertasklama as $row){
						$KodeUserTaskLama = $row->KodeUserTask;
					}
					if(!$this->usertask->updateStatusUserTask($KodeUserTaskLama,'AP',$this->npkLogin))
						$error += 1;
				}
				fire_print('log',$KodeUserTaskLama);
				//buat user task baru untuk proses approval rencana benefit
				$KodeUserTask = generateNo('UT');
				if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeBenefit,$KodeUserTask,"BE"))
				{
					$error += 1;
					echo('gagal simpan usertask benefit');
				}
				
				if($error > 0){
					$this->db->trans_rollback();
					return false;
				}else{
					$this->db->trans_commit();
					return true;
				}
			}else{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	function _delete($primary_key){
		$benefitList = $this->db->get_where('benefit',array("KodeBenefit"=>$primary_key));
		$benefitStatus = $benefitList->row(1)->StatusApproval;
		fire_print('log',"_delete. benefitStatus : $benefitStatus");
		if($benefitStatus == 'DE'){
			$query = $this->usertask->getLastUserTask($primary_key);
			foreach($query as $row){
				$KodeUserTaskLama = $row->KodeUserTask;
			}
			$this->usertask->updateStatusUserTask($KodeUserTaskLama,'DE',$this->npkLogin);
			
			return $this->db->update('benefit',array('deleted' => '1'),array('KodeBenefit' => $primary_key));
		}else{
			return false;
		}
	}
	
	function field_callback_Catatan($value,$primary_key)
	{
		$catatan = $this->usertask->getCatatan($primary_key);
		return $catatan;
	}
	
	function field_callback_NPK($value='',$primary_key=null)
	{
		return "<input type='textbox' id='field-NPK' name='field-NPK' value='$this->npkLogin'\>";
	}
	
	function field_callback_KodeJenisBenefit($value='',$primary_key=null)
	{
		return "<input type='textbox' id='field-KodeJenisBenefit' name='field-KodeJenisBenefit' value ='1'\>Medical";
	}
	
	function field_callback_KodeKeluarga($value='',$primary_key=null)
	{
		$strHTML = "<div id='divKodeKeluarga'>";
		fire_print('log','$valueKodeKeluarga: '.$value);
		if($value != '')
		{
			$strHTML .= $this->getKeluargaMedicalForEdit($primary_key,$value);
		}
		$strHTML .= "</div>";
		fire_print('log','$strHTML: '.$strHTML);
		return $strHTML;
	}
	
	function field_callback_HubunganKeluarga($value='',$primary_key=null)
	{
		return "<div id='HubunganKeluarga' name='HubunganKeluarga'>$value</div>";
	}
	
	function field_callback_SisaJatahMedical($value='',$primary_key=null)
	{
		return "Rp. <input id='SisaJatahMedical' name='SisaJatahMedical' type='text' value='$value' readonly style='border:0;background:white;'>";
	}
	
	function field_callback_column_KodeKeluargaKlaim($value,$row)
	{
		//fire_print('log','row:'.print_r($row,true));
		if($row->KodeKeluarga == 0)
		{
			return $row->s16dcbd43; //nama pengaju klaim
		}
		else
		{
			return $row->s1df240ef;	//klaim untuk siapa
		}
	}
	
	function field_callback_column_HubunganKeluarga($value,$row)
	{
		if($row->KodeKeluarga == 0)
		{
			return "Pribadi";
		}
		else
		{
			$keluargaList = $this->db->get_where('keluarga',array('Deleted'=>'0','KodeKeluarga'=>$row->KodeKeluarga));
			$dataKel = $keluargaList->row(1);
			return $dataKel->HubunganKeluarga;	
		}
	}
	
	function field_callback_column_StatusApproval($value,$row)
	{
		$lastUserTask = $this->usertask->getLastUserTaskValue($row->KodeBenefit);
		
		switch($value){
			case "DE":
				return "Ditolak";
				break;
			case "PE": 
				return "Menunggu Approval ".$lastUserTask["Nama"];
				break;
			case "AP":
				return "Sudah Disetujui";
				break;
		}
	}
	
	function field_callback_column_TotalKlaim($value,$row)
	{
		return number_format($value,0,",",".");
	}
	
	function field_callback_column_TotalBayar($value,$row)
	{
		return number_format($value,0,",",".");
	}
	
	function getKeluargaMedical()
	{
		$NPK = $_POST['NPK'];
		$Nama = "";
		$dataUser = $this->user->dataUser($NPK);
		$hasil = "";
		if($dataUser){
			foreach($dataUser as $rw){
				$Nama = $rw->nama;
			}
			
			$hasil = "<select id='KodeKeluarga' class='chosen-select' data-placeholder='Pilih Nama Keluarga' name='KodeKeluarga'>
				<option value='0'>$Nama</option>";
			$dataKeluargaInti = $this->db->query("select * from keluarga where NPK = ? AND Deleted = 0 AND JenisKeluarga = 'I'",array($NPK));
			if($dataKeluargaInti){
				foreach($dataKeluargaInti->result() as $dt)
				{
					$hasil .= "<option value='$dt->KodeKeluarga'>$dt->Nama</option>";
				}
			}
			$hasil .= "</select>";
		}
		echo $hasil;		
	}
	
	function getKeluargaMedicalForEdit($kodeBE, $kodeKeluarga)
	{
		$dataBEList = $this->db->get_where('benefit',array('Deleted'=>'0','KodeBenefit'=>$kodeBE));
		$dataBE = $dataBEList->row(1);
		$NPK = $dataBE->NPK;
		$Nama = "";
		$dataUser = $this->user->dataUser($NPK);
		foreach($dataUser as $rw){
			$Nama = $rw->nama;
		}
		$hasil = "<select id='KodeKeluarga' class='chosen-select' data-placeholder='Pilih Nama Keluarga' name='KodeKeluarga'>
			<option value='0'>$Nama</option>";
		$dataKeluargaInti = $this->db->query("select * from keluarga where NPK = ? AND Deleted = 0 AND JenisKeluarga = 'I'",array($NPK));
		if($dataKeluargaInti){
			foreach($dataKeluargaInti->result() as $dt)
			{
				$strKodeKeluargaSelected = "'" . ($dt->KodeKeluarga == $kodeKeluarga?$dt->KodeKeluarga . "' selected " : $dt->KodeKeluarga . "'"); 
				fire_print('log',"dt->KodeKeluarga=$dt->KodeKeluarga,kodeKeluarga = $kodeKeluarga,strKodeKeluargaSelected:$strKodeKeluargaSelected");
				$hasil .= "<option value=$strKodeKeluargaSelected>$dt->Nama</option>";
			}
		}
		$hasil .= "</select>";
		return $hasil;		
	}
	
	function getHubunganKeluarga(){
		$KodeKeluarga = $_POST['KodeKeluarga'];
		$hasil = "Pribadi";
		$dataHubKlg = $this->db->query('select * from keluarga where KodeKeluarga = ? AND Deleted = 0',array($KodeKeluarga));
		if($dataHubKlg){
			foreach($dataHubKlg->result() as $dt){
				$hasil = $dt->HubunganKeluarga;
			}
		}
		echo $hasil;
	}
	
	function getSisaJatahMedical(){
		$NPK = $_POST['NPK'];
		$hasil = "0";
		if($NPK != ''){
			$hasil = $this->benefit_model->getSisaJatahMedical($NPK);
		}
		echo number_format($hasil,0,",","."); //string number_format ( float $number , int $decimals = 0 , string $dec_point = "." , string $thousands_sep = "," )
	}
	
	function validasiJenisBenefit($KodeJenisBenefit)
	{
		//2 frame
		//3,4,5,6 lensa
		//frame 2 tahun sekali, lensa 1 tahun sekali
		//untuk semua golongan:
		//lensa monofokus biasa = 175000
		//lensa monofokus silindris = 200000
		//lensa bifokus biasa = 350000
		//lensa bifokus silindris = 375000
		//bingkai gol 1-3 = 250000
		//bingkai gol 4-5 = 400000
		//bingkai gol 6-7 = 450000
		
		fire_print('log',"validasi Jenis benefit. KodeJenisBenefit = $KodeJenisBenefit, NPK = " . print_r($this->input->post('NPK'),true));
		$listBenefit = $this->db->order_by('TanggalKlaim', 'DESC')->get_where('benefit',array('KodeJenisBenefit'=>$KodeJenisBenefit,'NPK'=>$this->input->post('NPK'),'StatusApproval'=>'AP','Deleted'=>'0'),1);
		fire_print('log',"listBenefit : ".print_r($listBenefit,true));
		
		if($listBenefit)
		{
			$TanggalKlaim = $listBenefit->row(1)->TanggalKlaim . " 00:00:00";
			$tglKlaimDate = strtotime($TanggalKlaim);
			
			if($KodeJenisBenefit == 2) //frame
			{
				fire_print('log',"tglKlaimDate = ". print_r($tglKlaimDate,true) . ". today = ". date("j M Y") .". Tanggal Klaim input : ".$this->input->post('TanggalKlaim'));
				//syarat 2 tahun sekali
				if((strtotime('+2 years',$tglKlaimDate) >= strtotime(date('Y-m-d'))) && (strtotime('+2 years',$tglKlaimDate) >= strtotime($this->input->post('TanggalKlaim'))))
				{
					$this->form_validation->set_message('validasiJenisBenefit', 'Frame masih belum bisa diklaim. Baru dapat diklaim setelah '. date('j M Y',strtotime('+2 years',$tglKlaimDate)).". Frame hanya dapat diklaim 2 tahun sekali");
					return false;
				}
			}
			else if($KodeJenisBenefit < 7 && $KodeJenisBenefit > 1) //lensa
			{
				//syarat 1 tahun sekali
				if((strtotime('+1 years',$tglKlaimDate) >= strtotime(date('Y-m-d'))) && (strtotime('+1 years',$tglKlaimDate) >= strtotime($this->input->post('TanggalKlaim'))))
				{
					$this->form_validation->set_message('validasiJenisBenefit', 'Lensa masih belum bisa diklaim. Baru dapat diklaim setelah '. date('j M Y',strtotime('+1 years',$tglKlaimDate)).". Lensa hanya dapat diklaim 1 tahun sekali");
					return false;
				}
			}
		}
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */