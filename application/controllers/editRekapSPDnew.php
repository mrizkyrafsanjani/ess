<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class EditRekapSPDnew extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('menu','',TRUE);
			$this->load->model('trkSPD','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('uangmuka_model','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');			
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}
				
				$noSPD = $this->trkSPD->getOneNoSPD($this->input->get('NoSPD'));
				
				if($noSPD){
					$noSPD_array = array();
					foreach($noSPD as $row){
						$noSPD_array[] = array(
							'NoSPD' => $row->NoSPD
						);
					}			
				
					$this->load->helper(array('form','url'));
					if(count($noSPD_array)>1){			
						$data = array(
								'title' => 'Input Laporan SPD',
								'menu_array' => $menu_array,
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'noSPD'=> $noSPD_array
							);
						$this->template->load('default','edit_rekapSPDnew_view',$data);
					}else if(count($noSPD_array)==1){					
						$this->loadSPD($noSPD_array);
					}
				}else{
					$data = array(
						'title' => 'Tidak Ada SPD yang Belum Dilaporkan',
						'menu_array' => $menu_array,
						'npk' => $session_data['npk'],
						'nama' => $session_data['nama'],
						'body' => 'Tidak Ada SPD Yang Harus Dilaporkan'
					);
					$this->template->load('default',null,$data);
				}
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		
		function loadSPD($noSPD){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$dataUser = $this->user->dataUser($session_data['npk']);
				$trkSPD = $this->trkSPD->getSPDbyNoSPDEdit($noSPD[0]);
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				$msg = $this->input->get('msg');
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}

				if($trkSPD){
					$trkSPD_array = array();
					foreach($trkSPD as $row){
						$trkSPD_array[] = array(
							'DPA' => $row->DPA,
							'uangmuka' => $row->UangMuka,
							'nospd' => $row->NoSPD,
							'tanggalspd' => $row->TanggalSPD,
							'npk' => $row->NPK,
							 'golongan' => $row->golongan,
							 'nama'=> $row->nama,
							 'jabatan'=> $row->jabatan,
							 'departemen'=> $row->departemen,
							 'atasan'=> $row->atasan,
							 'tanggalberangkatspd'=> $row->TanggalBerangkatSPD,
							 'tanggalkembalispd'=> $row->TanggalKembaliSPD,
							 'tujuan'=> $row->Tujuan,
							 'alasan'=> $row->AlasanPerjalanan,
							 'deskripsi'=> $row->Deskripsi,
							 'keteranganspd'=> $row->KeteranganSPD,
							 'bebanharianspd'=> $row->BebanHarianSPD,
							 'jumlahharispd'=> $row->JumlahHariSPD,
							 'totalSPD' => $row->TotalSPD
						);
					}
				}else{
					echo 'gagal111';
				}
				
				$trkUangMuka = $this->uangmuka_model->getHeaderTrxUangMukabyNoSPDEdit($noSPD[0]);
				
				$NPKAtasan = $this->uangmuka_model->getAtasan($session_data['npk']);
				$dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
				$dataOutstandingDetail = array();
				
				if($dataOutstanding){
					$dataOutstanding_array = array();
						foreach($dataOutstanding as $dataOutstanding){
									$dataOutstandingDetail[] = array(
										'title'=> 'Input Form Uang Muka Lainnya',
										//'menu_array' => $menu_array,
										'NoUangMuka' => $dataOutstanding->NoUangMuka,
										'KeteranganPermohonan' => $dataOutstanding->KeteranganPermohonan,
										'Total' => $dataOutstanding->Total,
										'TotalSPD' => $dataOutstanding->TotalSPD,
										'ReasonOutstanding'=> $dataOutstanding->ReasonOutStanding									 
									);
						}
				}

				if($trkUangMuka){
					$trkUangMuka_array = array();
					foreach($trkUangMuka as $row){
						$trkUangMuka_array[] = array(
							'DPA' => $row->DPA,
							'NoUangMuka' => $row->NoUangMuka,
							'TanggalPermohonan' => $row->TanggalPermohonan,
							'NoPP' => $row->NoPP,
							'KeteranganPermohonan' => $row->KeteranganPermohonan,
							'Status' => $row->Status,
							'TipeBayarUangMuka'=> $row->TipeBayarUangMuka,
							'BankBayarUangMuka'=> $row->BankBayarUangMuka,
							'NoRekBayarUangMuka'=> $row->NoRekBayarUangMuka,
							'NmPenerimaBayarUangMuka'=> $row->NmPenerimaBayarUangMuka,
							'AlasanOutstanding'=> $row->AlasanOutstanding,
							'NoSPD'=> $row->NoSPD,
							'NoAkomodasiTiket'=> $row->NoAkomodasiTiket,
							'TglTerimaHRGAPermohonan'=> $row->TglTerimaHRGAPermohonan,
							'TglTerimaFinancePermohonan'=> $row->TglTerimaFinancePermohonan,
							'TglBayarFinancePermohonan'=> $row->TglBayarFinancePermohonan,
							'TglTerimaHRGARealisasi'=> $row->TglTerimaHRGARealisasi,
							'TglTerimaFinanceRealisasi'=> $row->TglTerimaFinanceRealisasi,
							'TglBayarFinanceRealisasi' => $row->TglBayarFinanceRealisasi,
							'NoBEPermohonan' => $row->NoBEPermohonan,
							'NoBERealisasi' => $row->NoBERealisasi,
							'TipeKembaliUangMuka' => $row->TipeKembaliUangMuka,
							'BankKembaliUangMuka' => $row->BankKembaliUangMuka,
							'NoRekKembaliUangMuka' => $row->NoRekKembaliUangMuka,
							'NmPenerimaUangMuka' => $row->NmPenerimaUangMuka,
							'LebihKurangRealisasi' => $row->LebihKurangRealisasi,
							'CreatedBy' => $row->CreatedBy,
							'CreatedOn' => $row->CreatedOn,
							'JenisKegiatanUangMuka' => $row->JenisKegiatanUangMuka,
							'NPKPemohon' => $row->NPKPemohon,
							'ReasonOutstanding' => $row->ReasonOutstanding,
							'TanggalMulai' => $row->TanggalMulai,
							'TanggalSelesai' => $row->TanggalSelesai,
							'WaktuBayarTercepat' => $row->WaktuBayarTercepat,
							'WaktuPenyelesaianTerlambat' => $row->WaktuPenyelesaianTerlambat,
							'TanggalRealisasi' => $row->TanggalRealisasi,
							'FilePathPermohonan' => $row->FilePathPermohonan,
							'FilePathRealisasi' => $row->FilePathRealisasi,
							'id' => $row->id,
							'NamaCreatedBy' => $row->nama,
							'NamaPemohon' => $row->atasan,
							'NamaJenisKegiatan' =>  $row->JenisKegiatan

						);
					}
				}else{
					echo 'gagal111';
				}

				if($dataUser){
					$dataUser_array = array();
					foreach($dataUser as $dataUser){
						$dataUserDetail = array(
							'title'=> 'Input Form SPD',
							'menu_array' => $menu_array,
							'npk' => $dataUser->npk,
							 'golongan' => $dataUser->golongan,
							 'nama'=> $dataUser->nama,
							 'jabatan'=> $dataUser->jabatan,
							 'departemen'=> $dataUser->departemen,
							 'atasan'=> $dataUser->atasan,
							 'uangsaku'=> $dataUser->uangsaku,
							 'uangmakan'=> $dataUser->uangmakan					 
						);
					}
				}else{
					echo 'gagal';
				}
				
				$data2 = array(
						'title' => 'Edit Laporan SPD',
						'menu_array' => $menu_array,
						'npk' => $session_data['npk'],
					    'nama' => $session_data['nama'],
						'noSPD' => $noSPD,
						'trkSPD_array' => $trkSPD_array,
						'dataUser' => $dataUserDetail, 
						'msg'=> $msg,
						'trkUangMuka_array' => $trkUangMuka_array,
						'dataOutstanding' => $dataOutstandingDetail,
						'dataNoPP' => $this->uangmuka_model->getDataPP(),
						'dataJenisKeterangan' => $this->uangmuka_model->getJenisKegiatanUM()
					);
				$this->load->helper(array('form','url'));
				//$this->load->view('home_view', $data);
				$this->template->load('default','edit_rekapSPDnew_view',$data2);
				
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
	}
?>