<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class DPA1CashFlow extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
	
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Cashflow"))
			{
				$this->npkLogin = $session_data['npk'];
				//$this->_cashflowdpa1();
			}
		}else{
			redirect('login','refresh');
		}
    }
	
	public function search($Rekening='',$Bulan='', $Tahun='', $PilihanTglAwal ='',$PilihanTglAkhir ='')
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Cashflow DPA 1"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_cashflowdpa1($Rekening, $Bulan, $Tahun,$PilihanTglAwal,$PilihanTglAkhir);
			}
		}else{
			redirect('login','refresh');
		}
	}
	
	public function _cashflowdpa1_byNita($Rekening='', $Bulan='',$Tahun='',$PilihanTglAwal ='',$PilihanTglAkhir ='')
    {
		$crud = new grocery_crud();
		$crud->set_subject('Cashflow DPA1');
		//$crud->set_theme('datatables');
		
		$crud->set_model('custom_query_model');
		$crud->set_table('summarycashall_'); //Change to your table name

		
		$begin = new DateTime($PilihanTglAwal);
		$end = new DateTime($PilihanTglAkhir);
		$end = $end->modify( '+1 day' ); 

		

		$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
		
		$i=0;
			
		foreach($daterange as $date){			
			 
			 $dates[] = $date->format("Y-m-d");
			 ${'dateok' . $i} =  $dates[$i] ;
			 $i=$i+1;
			
		}
		//echo $i;
				
			if ($i==31)
				$sqlsyarat30hari =" format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok30."' then h.total else 0 end),0) AS `".$dateok30."`, '".$dateok30."' as tanggal30, ";
			else
				$sqlsyarat30hari =" ";

			if ($i==30 || $i==31)
				$sqlsyarat29hari =" 
				format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok28."' then h.total else 0 end),0) AS `".$dateok28."`, '".$dateok28."' as tanggal28,
				format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok29."' then h.total else 0 end),0) AS `".$dateok29."`, '".$dateok29."' as tanggal29, ";
			else
				$sqlsyarat29hari =" ";

			$praquery1 = " 
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok0."' then h.total else 0 end),0) AS `".$dateok0."`,'".$dateok0."' as tanggal0, 
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok1."' then h.total else 0 end),0) AS `".$dateok1."`,'".$dateok1."' as tanggal1,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok2."' then h.total else 0 end),0) AS `".$dateok2."`,'".$dateok2."' as tanggal2,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok3."' then h.total else 0 end),0) AS `".$dateok3."`,'".$dateok3."' as tanggal3,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok4."' then h.total else 0 end),0) AS `".$dateok4."`,'".$dateok4."' as tanggal4,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok5."' then h.total else 0 end),0) AS `".$dateok5."`,'".$dateok5."' as tanggal5,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok6."' then h.total else 0 end),0) AS `".$dateok6."`, '".$dateok6."' as tanggal6, 
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok7."' then h.total else 0 end),0) AS `".$dateok7."`, '".$dateok7."' as tanggal7,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok8."' then h.total else 0 end),0) AS `".$dateok8."`, '".$dateok8."' as tanggal8,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok9."' then h.total else 0 end),0) AS `".$dateok9."`, '".$dateok9."' as tanggal9,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok10."' then h.total else 0 end),0) AS `".$dateok10."`, '".$dateok10."' as tanggal10,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok11."' then h.total else 0 end),0) AS `".$dateok11."`, '".$dateok11."' as tanggal11,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok12."' then h.total else 0 end),0) AS `".$dateok12."`, '".$dateok12."' as tanggal12,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok13."' then h.total else 0 end),0) AS `".$dateok13."`, '".$dateok13."' as tanggal13,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok14."' then h.total else 0 end),0) AS `".$dateok14."`, '".$dateok14."' as tanggal14,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok15."' then h.total else 0 end),0) AS `".$dateok15."`, '".$dateok15."' as tanggal15,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok16."' then h.total else 0 end),0) AS `".$dateok16."`, '".$dateok16."' as tanggal16,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok17."' then h.total else 0 end),0) AS `".$dateok17."`, '".$dateok17."' as tanggal17,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok18."' then h.total else 0 end),0) AS `".$dateok18."`, '".$dateok18."' as tanggal18,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok19."' then h.total else 0 end),0) AS `".$dateok19."`, '".$dateok19."' as tanggal19,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok20."' then h.total else 0 end),0) AS `".$dateok20."`, '".$dateok20."' as tanggal20,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok21."' then h.total else 0 end),0) AS `".$dateok21."`, '".$dateok21."' as tanggal21,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok22."' then h.total else 0 end),0) AS `".$dateok22."`, '".$dateok22."' as tanggal22,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok23."' then h.total else 0 end),0) AS `".$dateok23."`, '".$dateok23."' as tanggal23,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok24."' then h.total else 0 end),0) AS `".$dateok24."`, '".$dateok24."' as tanggal24,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok25."' then h.total else 0 end),0) AS `".$dateok25."`, '".$dateok25."' as tanggal25,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok26."' then h.total else 0 end),0) AS `".$dateok26."`, '".$dateok26."' as tanggal26,
					  format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '".$dateok27."' then h.total else 0 end),0) AS `".$dateok27."`, '".$dateok27."' as tanggal27,				  
					  ". $sqlsyarat29hari." " .$sqlsyarat30hari  ;			  
					

			if ($i==31)
					  $praquery2=  "h.tanggal between '".$dateok0."' and '".$dateok30."'";
			else if ($i==30)
					  $praquery2=  "h.tanggal between '".$dateok0."' and '".$dateok29."'";	
			else
					 $praquery2=  "h.tanggal between '".$dateok0."' and '".$dateok27."'";	
		

		
		$sqlquery = "SELECT h.id,h.tipetransaksi,h.koderekening,m.namarekening, ".$praquery1."
					  case when h.tipetransaksi='SALDOAWAL' then 1 else case when h.tipetransaksi='IN' then 2 else case when h.tipetransaksi='OUT' then 3 else case when h.tipetransaksi='SALDOAKHIR' then 4 end end end end as No
					  from summarycashall_ h 					  
					  join mstrrekening m on m.koderekening=h.koderekening 
					  where h.DPA=1 and h.koderekening='".$Rekening."' and h.bulan=".$Bulan." and h.tahun=".$Tahun." and  ".$praquery2."
					  group by h.dpa,namarekening,h.koderekening,tipetransaksi order by No asc
					  ";
		
		//echo 	$sqlquery;		
		
	$crud->basic_model->set_query_str($sqlquery); //Query text here
		//$crud->order_by('id','desc');
		
		if ($i==31)
			$crud->columns('No','tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3,$dateok4,$dateok5,$dateok6,$dateok7,$dateok8,$dateok9,$dateok10,$dateok11,$dateok12,$dateok13,$dateok14,$dateok15,$dateok16,$dateok17,$dateok18,$dateok19,$dateok20,$dateok21,$dateok22,$dateok23,$dateok24,$dateok25,$dateok26,$dateok27,$dateok28,$dateok29,$dateok30);
		else if ($i==30)
			$crud->columns('No','tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3,$dateok4,$dateok5,$dateok6,$dateok7,$dateok8,$dateok9,$dateok10,$dateok11,$dateok12,$dateok13,$dateok14,$dateok15,$dateok16,$dateok17,$dateok18,$dateok19,$dateok20,$dateok21,$dateok22,$dateok23,$dateok24,$dateok25,$dateok26,$dateok27,$dateok28,$dateok29);
		else
			$crud->columns('No','tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3,$dateok4,$dateok5,$dateok6,$dateok7,$dateok8,$dateok9,$dateok10,$dateok11,$dateok12,$dateok13,$dateok14,$dateok15,$dateok16,$dateok17,$dateok18,$dateok19,$dateok20,$dateok21,$dateok22,$dateok23,$dateok24,$dateok25,$dateok26,$dateok27);
			
			if ($dateok0==date("Y-m-d")) 
				$crud->callback_column($dateok0,array($this,'_callback_input_trans71'));
			else 
				$crud->callback_column($dateok0,array($this,'_callback_show_trans71'));
			
			if ($dateok1==date("Y-m-d")) 
				$crud->callback_column($dateok1,array($this,'_callback_input_trans72'));
			else 
				$crud->callback_column($dateok1,array($this,'_callback_show_trans72'));
			
			if ($dateok2==date("Y-m-d")) 
				$crud->callback_column($dateok2,array($this,'_callback_input_trans73'));
			else 
				$crud->callback_column($dateok2,array($this,'_callback_show_trans73'));
			
			if ($dateok3==date("Y-m-d")) 
				$crud->callback_column($dateok3,array($this,'_callback_input_trans74'));
			else 
				$crud->callback_column($dateok3,array($this,'_callback_show_trans74'));
			
			if ($dateok4==date("Y-m-d")) 
				$crud->callback_column($dateok4,array($this,'_callback_input_trans75'));
			else 
				$crud->callback_column($dateok4,array($this,'_callback_show_trans75'));
			
			if ($dateok5==date("Y-m-d")) 
				$crud->callback_column($dateok5,array($this,'_callback_input_trans76'));
			else 
				$crud->callback_column($dateok5,array($this,'_callback_show_trans76'));
			
			if ($dateok6==date("Y-m-d")) 
				$crud->callback_column($dateok6,array($this,'_callback_input_trans77'));
			else 
				$crud->callback_column($dateok6,array($this,'_callback_show_trans77'));
			
			if ($dateok7==date("Y-m-d")) 
				$crud->callback_column($dateok7,array($this,'_callback_input_trans78'));
			else 
				$crud->callback_column($dateok7,array($this,'_callback_show_trans78'));
			
			if ($dateok8==date("Y-m-d")) 
				$crud->callback_column($dateok8,array($this,'_callback_input_trans79'));
			else 
				$crud->callback_column($dateok8,array($this,'_callback_show_trans79'));

			if ($dateok9==date("Y-m-d")) 
				$crud->callback_column($dateok9,array($this,'_callback_input_trans80'));
			else 
				$crud->callback_column($dateok9,array($this,'_callback_show_trans80'));

			if ($dateok10==date("Y-m-d")) 
				$crud->callback_column($dateok10,array($this,'_callback_input_trans81'));
			else 
				$crud->callback_column($dateok10,array($this,'_callback_show_trans81'));

			if ($dateok11==date("Y-m-d")) 
				$crud->callback_column($dateok11,array($this,'_callback_input_trans82'));
			else 
				$crud->callback_column($dateok11,array($this,'_callback_show_trans82'));

			if ($dateok12==date("Y-m-d")) 
				$crud->callback_column($dateok12,array($this,'_callback_input_trans83'));
			else 
				$crud->callback_column($dateok12,array($this,'_callback_show_trans83'));

			if ($dateok13==date("Y-m-d")) 
				$crud->callback_column($dateok13,array($this,'_callback_input_trans84'));
			else 
				$crud->callback_column($dateok13,array($this,'_callback_show_trans84'));

			if ($dateok14==date("Y-m-d")) 
				$crud->callback_column($dateok14,array($this,'_callback_input_trans85'));
			else 
				$crud->callback_column($dateok14,array($this,'_callback_show_trans85'));

			if ($dateok15==date("Y-m-d")) 
				$crud->callback_column($dateo15,array($this,'_callback_input_trans86'));
			else 
				$crud->callback_column($dateok15,array($this,'_callback_show_trans86'));

			if ($dateok16==date("Y-m-d")) 
				$crud->callback_column($dateok16,array($this,'_callback_input_trans87'));
			else 
				$crud->callback_column($dateok16,array($this,'_callback_show_trans87'));

			if ($dateok17==date("Y-m-d")) 
				$crud->callback_column($dateok17,array($this,'_callback_input_trans88'));
			else 
				$crud->callback_column($dateok17,array($this,'_callback_show_trans88'));
			
			if ($dateok18==date("Y-m-d")) 
				$crud->callback_column($dateok18,array($this,'_callback_input_trans89'));
			else 
				$crud->callback_column($dateok18,array($this,'_callback_show_trans89'));

			if ($dateok19==date("Y-m-d")) 
				$crud->callback_column($dateok19,array($this,'_callback_input_trans90'));
			else 
				$crud->callback_column($dateok19,array($this,'_callback_show_trans90'));

			if ($dateok20==date("Y-m-d")) 
				$crud->callback_column($dateok20,array($this,'_callback_input_trans91'));
			else 
				$crud->callback_column($dateok20,array($this,'_callback_show_trans91'));

			if ($dateok21==date("Y-m-d")) 
				$crud->callback_column($dateok21,array($this,'_callback_input_trans92'));
			else 
				$crud->callback_column($dateok21,array($this,'_callback_show_trans92'));

			if ($dateok22==date("Y-m-d")) 
				$crud->callback_column($dateok22,array($this,'_callback_input_trans93'));
			else 
				$crud->callback_column($dateok22,array($this,'_callback_show_trans93'));

			if ($dateok23==date("Y-m-d")) 
				$crud->callback_column($dateok23,array($this,'_callback_input_trans94'));
			else 
				$crud->callback_column($dateok23,array($this,'_callback_show_trans94'));

			if ($dateok24==date("Y-m-d")) 
				$crud->callback_column($dateok24,array($this,'_callback_input_trans95'));
			else 
				$crud->callback_column($dateok24,array($this,'_callback_show_trans95'));

			if ($dateok25==date("Y-m-d")) 
				$crud->callback_column($dateok25,array($this,'_callback_input_trans96'));
			else 
				$crud->callback_column($dateok25,array($this,'_callback_show_trans96'));

			if ($dateok26==date("Y-m-d")) 
				$crud->callback_column($dateok26,array($this,'_callback_input_trans97'));
			else 
				$crud->callback_column($dateok26,array($this,'_callback_show_trans97'));

			if ($dateok27==date("Y-m-d")) 
				$crud->callback_column($dateok27,array($this,'_callback_input_trans98'));
			else 
				$crud->callback_column($dateok27,array($this,'_callback_show_trans98'));

			if ($i==30 || $i==31)
			{
	
				if ($dateok28==date("Y-m-d")) 
					$crud->callback_column($dateok28,array($this,'_callback_input_trans99'));
				else 
					$crud->callback_column($dateok28,array($this,'_callback_show_trans99'));
				
	
				if ($dateok29==date("Y-m-d")) 
					$crud->callback_column($dateok29,array($this,'_callback_input_trans100'));
				else 
					$crud->callback_column($dateok29,array($this,'_callback_show_trans100'));
				
			}
	
			if ($i==31)
			{	
				if ($dateok30==date("Y-m-d")) 
					$crud->callback_column($dateok30,array($this,'_callback_input_trans101'));
				else 
					$crud->callback_column($dateok30,array($this,'_callback_show_trans101'));
			}				
			
       
		$crud->unset_delete();
		$crud->unset_edit();
		$crud->unset_add();
		$crud->unset_read();
		$output = $crud->render();
   
        $this-> _outputview($output);    
	
    }

	public function _cashflowdpa1($Rekening='', $Bulan='',$Tahun='',$PilihanTglAwal ='',$PilihanTglAkhir ='')
    {
		$crud = new grocery_crud();
		$crud->set_subject('Cashflow DPA1');
		//$crud->set_theme('datatables');
		
		$crud->set_model('custom_query_model');
		$crud->set_table('summarycashall_'); //Change to your table name

		
		$begin = new DateTime($PilihanTglAwal);
		$end = new DateTime($PilihanTglAkhir);
		$end = $end->modify( '+1 day' ); 

		$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);		
		$i=0;
		$praquery1 = "";
		foreach($daterange as $date){
			$dates[] = $date->format("Y-m-d");
			${'dateok' . $i} =  $dates[$i] ;
			
			$praquery1 .= " format(sum(case when Date_format(h.tanggal, '%Y-%m-%d') = '". ${"dateok".$i} ."' then h.total else 0 end),2) AS `". ${"dateok".$i} ."`,'". ${"dateok".$i} ."' as tanggal".$i.",";

			if (${"dateok".$i}>=date("Y-m-d")) 
				$crud->callback_column(${"dateok".$i},array($this,'_callback_input_trans'.(71+$i)));
			else 
				$crud->callback_column(${"dateok".$i},array($this,'_callback_show_trans'.(71+$i)));

			$i=$i+1;
		}
		//echo $i;
		$praquery2=  "h.tanggal between '".$dateok0."' and '".${"dateok".($i-1)}."'";
		
		$sqlquery = "SELECT h.id,h.tipetransaksi,h.koderekening,m.namarekening, ".$praquery1."
					  case when h.tipetransaksi='SALDOAWAL' then 1 else case when h.tipetransaksi='IN' then 2 else case when h.tipetransaksi='OUT' then 3 else case when h.tipetransaksi='SALDOAKHIR' then 4 end end end end as No
					  from summarycashall_ h 					  
					  join mstrrekening m on m.koderekening=h.koderekening 
					  where h.DPA=1 and h.koderekening='".$Rekening."' and h.bulan=".$Bulan." and h.tahun=".$Tahun." and  ".$praquery2."
					  group by h.dpa,namarekening,h.koderekening,tipetransaksi order by No asc
					  ";
		
		//echo 	$sqlquery;		
		
		$crud->basic_model->set_query_str($sqlquery); //Query text here
		//$crud->order_by('id','desc');
		
		if ($i==31)
			$crud->columns('No','tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3,$dateok4,$dateok5,$dateok6,$dateok7,$dateok8,$dateok9,$dateok10,$dateok11,$dateok12,$dateok13,$dateok14,$dateok15,$dateok16,$dateok17,$dateok18,$dateok19,$dateok20,$dateok21,$dateok22,$dateok23,$dateok24,$dateok25,$dateok26,$dateok27,$dateok28,$dateok29,$dateok30);
		else if ($i==30)
			$crud->columns('No','tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3,$dateok4,$dateok5,$dateok6,$dateok7,$dateok8,$dateok9,$dateok10,$dateok11,$dateok12,$dateok13,$dateok14,$dateok15,$dateok16,$dateok17,$dateok18,$dateok19,$dateok20,$dateok21,$dateok22,$dateok23,$dateok24,$dateok25,$dateok26,$dateok27,$dateok28,$dateok29);
		else if ($i==29)
			$crud->columns('No','tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3,$dateok4,$dateok5,$dateok6,$dateok7,$dateok8,$dateok9,$dateok10,$dateok11,$dateok12,$dateok13,$dateok14,$dateok15,$dateok16,$dateok17,$dateok18,$dateok19,$dateok20,$dateok21,$dateok22,$dateok23,$dateok24,$dateok25,$dateok26,$dateok27,$dateok28);
		else
			$crud->columns('No','tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3,$dateok4,$dateok5,$dateok6,$dateok7,$dateok8,$dateok9,$dateok10,$dateok11,$dateok12,$dateok13,$dateok14,$dateok15,$dateok16,$dateok17,$dateok18,$dateok19,$dateok20,$dateok21,$dateok22,$dateok23,$dateok24,$dateok25,$dateok26,$dateok27);
				
		$crud->unset_delete();
		$crud->unset_edit();
		$crud->unset_add();
		$crud->unset_read();
		$output = $crud->render();
   
        $this-> _outputview($output);	
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('Cashflow/CashFlowDPA1table_view',$data);
		    
    }
	
	public function _callback_show_trans($value, $row)
	{
		
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$value .'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}

	public function _callback_show_trans71($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal0.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans72($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal1.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans73($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal2.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans74($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal3.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans75($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal4.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans76($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal5.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans77($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal6.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans78($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal7.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans79($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal8.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans80($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal9.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans81($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal10.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans82($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal11.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans83($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal12.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans84($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal13.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans85($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal14.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans86($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal15.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans87($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal16.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans88($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal17.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans89($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal18.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans90($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal19.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans91($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal20.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans92($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal21.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans93($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal22.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans94($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal23.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans95($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal24.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans96($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal25.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans97($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal26.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans98($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal27.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans99($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal28.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans100($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal29.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_show_trans101($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->tanggal30.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	
	public function _callback_input_trans($value, $row)
	{
		
		  $atts = array(
			'width'	  => '700',
			'height'	 => '700',
			'scrollbars' => 'yes',
			'status'	 => 'yes',
			'resizable'  => 'yes',
			'screenx'	=> '0',
			'screeny'	=> '0'
			);

			return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$value.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}

	public function _callback_input_trans71($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal0.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans72($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal1.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans73($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal2.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans74($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal3.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans75($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal4.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans76($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal5.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans77($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal6.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans78($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal7.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	
	public function _callback_input_trans79($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal8.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}

	public function _callback_input_trans80($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal9.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}

	public function _callback_input_trans81($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal10.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans82($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal11.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans83($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal12.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans84($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal13.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans85($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal14.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans86($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal15.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans87($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal16.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans88($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal17.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans89($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal18.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans90($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal19.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans91($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal20.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans92($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal21.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans93($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal22.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans94($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal23.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans95($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal24.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans96($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal25.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans97($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal26.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans98($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal27.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans99($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal28.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans100($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal29.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	public function _callback_input_trans101($value, $row)
	{
		$atts = array('width'	  => '900','height'	 => '700','scrollbars' => 'yes','status'	 => 'yes','resizable'  => 'yes','screenx'	=> '0',	'screeny'	=> '0'	);
		return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->tanggal30.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	


}



/* End of file main.php */
/* Location: ./application/controllers/main.php */