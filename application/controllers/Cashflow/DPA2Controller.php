<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
session_start();
class DPA2Controller extends CI_Controller{
    var $npkLogin;
    function __construct()
    {
        parent::__construct();    
		$this->load->model('DPA2cashflow_model','',TRUE);			
        $this->load->helper('date');
        $session_data = $this->session->userdata('logged_in');
        $this->npkLogin = $session_data['npk'];
    }

    public function index()
    {
        try
        {
            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }

    

    public function filter_DPA2()
    {
        $session_data = $this->session->userdata('logged_in');
        if($this->session->userdata('logged_in')){
            if(check_authorizedByName("Cashflow DPA 2"))
            {
                $this->npkLogin = $session_data['npk'];
                $data = array(
                    'title' => 'Cashflow DPA 2'
                );
                $this->load->helper(array('form','url'));
                $this->template->load('default','Cashflow/CashFlowDPA2_view',$data);            
            }
        }else{
            //redirect('login','refresh');
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }
	
	function ajax_KalkulasiTotalCashDPA2()
	{			
			$Rekening = $_POST['Rekening'];
			$Bulan = $_POST['Bulan'];
			$Tahun = $_POST['Tahun'];
			$return = $this->DPA2cashflow_model->KalkulasiTotalCashDPA2($Rekening, $Bulan, $Tahun);
			if($return)
			{
				echo "success";
			}else{
				echo "fail";
			}
    }
    //Edit by angit
    function ajax_KalkulasiTotalCashFlow()
	{
        $Rekening = $_POST['Rekening'];
        $Bulan = $_POST['Bulan'];
        $Tahun = $_POST['Tahun'];
        $TipeTransaksi = $_POST['TipeTransaksi'];
        $return = $this->DPA2cashflow_model->KalkulasiTotalCashFlow($Rekening, $Bulan, $Tahun, $TipeTransaksi);
        if($return)
        {
        echo $return;
        }
    }

    
}
?>