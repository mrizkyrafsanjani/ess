<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class DPA2DetailTransaksi extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('cashflow','',TRUE);
		$this->load->library('grocery_crud');
		global  $temptanggal;
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');		
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Cashflow DPA 2"))
			{			
				$this->npkLogin = $session_data['npk'];
			}
		}else{
			redirect('login','refresh');
		}
    }
	
	public function search($tanggal="",$koderekening="",$tipetransaksi="")
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Cashflow DPA 2"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_detailcashflowdpa2($tanggal,$koderekening,$tipetransaksi);
			}
		}else{
			redirect('login','refresh');
		}
	}
	
	
	
	public function _detailcashflowdpa2($tanggal,$koderekening,$tipetransaksi)
    {
		$crud = new grocery_crud();
		$crud->set_subject('Detail Transaksi DPA2');
		//$crud->set_theme('datatables');
		
		$crud->set_model('custom_query_model');
		if (strtotime($tanggal) < strtotime(date("Y-m-d")))	
			$crud->set_table('trxdpa2'); //Change to your table name
		else
			$crud->set_table('trxforecastdpa2'); //Change to your table name
		

		if (strtotime($tanggal) < strtotime(date("Y-m-d")))			
			$sqlquery = " 
			select * from 
			(
				select t.*,Date_format(t.tgltransaksi, '%Y-%m-%d') as tgltransaksi1, format(debet,2) as debet1, format (kredit,2) as kredit1, 1 as urutan   from trxdpa2 t
				where Date_format(t.tgltransaksi, '%Y-%m-%d')  = '".$tanggal."' 
				and koderekening='".$koderekening."' and tipetransaksi='".$tipetransaksi."'
				union all
				select '' as tgltransaksi,'' as kodeakun, 0 as debet, 0 as kredit,'Total' as keterangan,'' as createdon, '' as createdby,'' as tipetransaksi,'' as koderekening, '' as idtrxdpa2,Date_format(t.tgltransaksi, '%Y-%m-%d') as tgltransaksi1, format(sum(debet),2) as debet1, format (sum(kredit),2) as kredit1,2 as urutan from trxdpa2 t 
				where Date_format(t.tgltransaksi, '%Y-%m-%d') = '".$tanggal."' and koderekening='".$koderekening."' and tipetransaksi='".$tipetransaksi."'
			) a order by urutan
			";
		else
			$sqlquery = " select t.*,Date_format(t.tglforecast, '%Y-%m-%d') as tglterima1, format(nominal,2) as nominal,format(bunga,2) as bunga,format(totalterima,2) as totalterima   from trxforecastdpa2 t
			where Date_format(t.tglforecast, '%Y-%m-%d')  = '".$tanggal."' 
			and koderekening='".$koderekening."' and tipetransaksi='".$tipetransaksi."' order by pfoliocode,instrumentcode";

		
		$crud->basic_model->set_query_str($sqlquery); //Query text here
		if (strtotime($tanggal) < strtotime(date("Y-m-d")))
			$crud->columns('tgltransaksi1','keterangan','debet1','kredit1');
		else
			$crud->columns('tglterima1','instrumentcode','pfoliocode','keterangan','nominal','bunga','totalterima');
			
		       
			
			$crud->unset_delete();
			$crud->unset_edit();
			$crud->unset_add();
			$crud->unset_read();
		
		$output = $crud->render();
   
        $this-> _outputview($output);    
	
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('Cashflow/CashFlowDPA2trans_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	public function search_trans_dpa2($tanggal="",$koderekening="",$tipetransaksi="")
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Cashflow DPA 2"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_detailcashflowdpa2_input($tanggal,$koderekening,$tipetransaksi);
			}
		}else{
			redirect('login','refresh');
		}
	}

	function search_input($tanggal="",$koderekening="",$tipetransaksi="")
	{
		try
		{
			
			$NPKSelectedUser = $this->npkLogin;
			
			if(!$this->session->userdata('logged_in'))
			{
				redirect('login','refresh');				
			}
			else
			{           
				  		$tgldata = $tanggal;					
						$kodebank = $koderekening;
						$tipetrans = $tipetransaksi;						
						 
						
							$dataDetail = array(
								'tgldata'=> $tanggal,
								'kodebank' => $koderekening,
								'tipetrans' => $tipetransaksi,
								'npk' => $NPKSelectedUser							
							);						
					
					
					$this->load->helper(array('form','url'));
					$this->template->load('default_popup','Cashflow/PopPupDPA2trans_view',$dataDetail);
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	
	var $tempkoderekening = "abc";
	
	
	public function _detailcashflowdpa2_input($tanggal,$koderekening,$tipetransaksi)
    {
		
		global $tempkoderekening, $temptanggal;

		$crud = new grocery_crud();
		$crud->set_subject('Detail Transaksi DPA2');
		//$crud->set_theme('datatables');
		
		$crud->set_model('custom_query_model');
		$crud->set_table('temptrxdpa2'); //Change to your table name
		
		
		//GLOBAL $tempkoderekening ;$tempkoderekening=$koderekening;
		//GLOBAL $temptipetransaksi;$temptipetransaksi=$tipetransaksi;

		$GLOBALS[$tempkoderekening] =$koderekening;
		$_SESSION["temptanggal"] = $tanggal;
		//$GLOBALS[$temptipetransaksi]=$tipetransaksi;
		
		$sqlquery = " select t.*,Date_format(t.tgltransaksi, '%Y-%m-%d') as tgltransaksi1, 
					  format(debet,2) as debet1, format (kredit,2) as kredit1   
					  from temptrxdpa2 t
		where Date_format(t.tgltransaksi, '%Y-%m-%d')  = '".$tanggal."' 
		and koderekening='".$koderekening."' and tipetransaksi='".$tipetransaksi."'";
		
		//echo 	$tipetransaksi;	
		$crud->basic_model->set_query_str($sqlquery); //Query text here

		if ($tipetransaksi=='IN')	
			$crud->columns('tgltransaksi1','keterangan',  'koderekening', 'debet1','tipetransaksi');
		else 
			$crud->columns('tgltransaksi1','keterangan',  'koderekening', 'kredit1','tipetransaksi');
		
			
		if ($tipetransaksi=='IN')	
			$crud->fields('tgltransaksi','koderekening','tipetransaksi','rekeningtujuan','keterangan' , 'debet');
		else 
			$crud->fields('tgltransaksi','koderekening','tipetransaksi','rekeningtujuan','keterangan', 'kredit');

		if ($tipetransaksi=='IN')	
			$crud->required_fields('keterangan', 'debet');
		else 
			$crud->required_fields('keterangan', 'kredit');

		$crud->display_as('tgltransaksi1','Tanggal Transaksi');
		$crud->display_as('koderekening','Kode Rekening');
		$crud->display_as('tipetransaksi','Tipe Transaksi');
				
		$crud->callback_field('tgltransaksi',array($this,'add_field_callback_tgltransaksi'));
		$crud->callback_field('koderekening',array($this,'add_field_callback_koderekening'));

		//Edit by anggit
		if ($tipetransaksi=='IN'){
			$crud->callback_field('tipetransaksi',array($this,'add_field_callback_tipetransaksi_in'));
		}else{
			$crud->callback_add_field('tipetransaksi',array($this,'add_field_callback_tipetransaksi_out'));
		}

		// $crud->callback_field('tipetransaksi',array($this,'add_field_callback_tipetransaksi'));
		$crud->callback_field('rekeningtujuan',array($this,'add_field_callback_rekeningtujuan'));
		
		$crud->callback_insert(array($this,'_insert_transaksi_now'));
		$crud->callback_update(array($this,'_update_transaksi_now'));	

		$crud->callback_column('tgltransaksi1',array($this,'column_callback_mewarnaiFont'));
		$crud->callback_column('keterangan',array($this,'column_callback_mewarnaiFont'));
		$crud->callback_column('koderekening',array($this,'column_callback_mewarnaiFont'));
		$crud->callback_column('kredit1',array($this,'column_callback_mewarnaiFont'));
		$crud->callback_column('debet1',array($this,'column_callback_mewarnaiFont'));
		$crud->callback_column('tipetransaksi',array($this,'column_callback_mewarnaiFont'));
		//$crud->callback_delete(array($this,'_delete_transaksi_now'));	
		$js = "
			<script>		
			$(document).ready(function() {
				$('#rekeningtujuan_field_box').hide();
				var cmbTipeTransaksi = $('#cmbTipeTransaksi');
				cmbTipeTransaksi.change(function(){
					if(cmbTipeTransaksi.val() == 'MOVEMENT'){
						$('#rekeningtujuan_field_box').show();
					}else{
						$('#rekeningtujuan_field_box').hide();
					}
				});
			}); 
			</script>";

		
		$output = $crud->render();
		$output->output.=$js;
        $this-> _outputviewtrans($output);    
	
	}
	
	var $temptanggal;	
	function add_field_callback_tgltransaksi($value = '', $primary_key = null)
	{
		$temptanggal="";	
		return '<input type="text" name="tgltransaksi" style="border:0;" value="'.$_SESSION["temptanggal"].'" readonly>';
	}

	
	function add_field_callback_koderekening($value = '', $primary_key = null)	
	{		
		$tempkoderekening="";
		return '<input type="text" name="koderekening" style="border:0;" value="'.$GLOBALS[$tempkoderekening].'" readonly>';
	}

	function column_callback_mewarnaiFont($value, $row){
		if($row->createdby != "SSIS"){
			return "<font color='red'>" . $value . "</font>";
		}else{
			return $value;
		}
	}

	
	function add_field_callback_tipetransaksi($value = '', $primary_key = null)
	{	
		//$temptipetransaksi="IN";	
		//return '<input type="text" id="field-tipetransaki" style="border:0;" value="'.$temptipetransaksi.'" readonly>';
		$selected1 = '';
		$selected2 = '';
		if($value == "IN"){
			$selected1 = 'selected';
		}else if($value == "OUT"){
			$selected2 = 'selected';
		}
		
		//Edited by anggit
		return ' <select id="cmbTipeTransaksi" name="tipetransaksi">
			<option value = "0">Pilih Jenis Transaksi</option>
			<option '.$selected1.' value="IN">IN</option>
			<option '.$selected2.' value="OUT">OUT</option>
			<option value="MOVEMENT">MOVEMENT</option>
		</select>';
	}

	//Edit by anggit
	function add_field_callback_tipetransaksi_in($value = '', $primary_key = null)
	{	
		//$temptipetransaksi="IN";	
		//return '<input type="text" id="field-tipetransaki" style="border:0;" value="'.$temptipetransaksi.'" readonly>';
		$selected1 = '';
		$selected2 = '';
		$selected3 = '';
		if($value == "IN"){
			$selected1 = 'selected';
		}else if($value == "OUT"){
			$selected2 = 'selected';
		}else if($value == "MOVEMENT"){
			$selected3 = 'selected';
		}
		
		//Edited by anggit
		return ' <select id="cmbTipeTransaksi" name="tipetransaksi">
			<option value = "0">Pilih Jenis Transaksi</option>
			<option '.$selected1.' value="IN">IN</option>
		</select>';
	}


	function add_field_callback_tipetransaksi_out($value = '', $primary_key = null)
	{	
		//$temptipetransaksi="IN";	
		//return '<input type="text" id="field-tipetransaki" style="border:0;" value="'.$temptipetransaksi.'" readonly>';
		$selected1 = '';
		$selected2 = '';
		$selected3 = '';
		if($value == "IN"){
			$selected1 = 'selected';
		}else if($value == "OUT"){
			$selected2 = 'selected';
		}else if($value == "MOVEMENT"){
			$selected3 = 'selected';
		}
		
		//Edited by anggit
		return ' <select id="cmbTipeTransaksi" name="tipetransaksi">
			<option value = "0">Pilih Jenis Transaksi</option>
			<option '.$selected2.' value="OUT">OUT</option>
			<option '.$selected3.' value="MOVEMENT">MOVEMENT</option>
		</select>';
	}

	function add_field_callback_rekeningtujuan($value = '', $primary_key = null)
	{	
		//$temptipetransaksi="IN";	
		//return '<input type="text" id="field-tipetransaki" style="border:0;" value="'.$temptipetransaksi.'" readonly>';
		
		return ' <select id="cmbRekeningTujuan" name="rekeningtujuan">
			<option value = "0">Pilih Rekening Tujuan</option>
			<option value="BPT01DPA2">BPT01DPA2</option>
			<option value="BPT02DPA2">BPT02DPA2</option>
			<option value="BPT03DPA2">BPT03DPA2</option>
			<option value="BPT04DPA2">BPT04DPA2</option>
			<option value="BPTDPADPA2">BPTDPADPA2</option>
		</select>';
	}


	function _insert_transaksi_now($post_array)
	{
		fire_print('log','sudah masuk ke insert detail');
		
		$this->db->trans_begin();
		$isMovement = false;
		if($post_array['tipetransaksi']=='MOVEMENT'){
			$isMovement = true;
			$post_array['tipetransaksi'] = "OUT";
		}

		if ($post_array['tipetransaksi']=='IN')	
		{	
			$jumlahdebit = $post_array['debet'];
			$jumlahkredit =0;
		}
		else 
		{	
			$jumlahdebit= 0;
			$jumlahkredit = $post_array['kredit'];
		}

		$dataTransaksi = array(	
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s'),
			"koderekening" => $post_array['koderekening'],
			"tgltransaksi" => $post_array['tgltransaksi'],
			"tipetransaksi" => $post_array['tipetransaksi'],
			"keterangan" => $post_array['keterangan'],
			"debet" =>$jumlahdebit ,	
			"kredit" =>$jumlahkredit 	
		);
		

		if($this->db->insert('temptrxdpa2',$dataTransaksi))
		{
			if($isMovement)
			{

				$dataTransaksi['debet'] = $post_array['kredit'];
				$dataTransaksi['kredit'] = 0;
				$dataTransaksi['koderekening'] = $post_array['rekeningtujuan'];
				$dataTransaksi['tipetransaksi'] = 'IN';
				$tableTargetInsert = 'temptrxdpa2';
				if(substr($post_array['rekeningtujuan'], -1) == "1"){
					//$tableTargetInsert = 'temptrxdpa1';
					$tableTargetInsert = 'temptrxdpa2';
				}
				if($this->db->insert($tableTargetInsert,$dataTransaksi)){
					$this->db->trans_commit();	

					//lakukan proses refresh nilai
					$this->ajax_submitCashToday_withparam($post_array['tgltransaksi'],  $post_array['koderekening'], "OUT");
					$this->ajax_submitCashToday_withparam($post_array['tgltransaksi'],  $post_array['rekeningtujuan'], "IN");
					//end of proses refresh nilai

					return true;
				}else{
					$this->db->trans_rollback();	
					return false;
				}
			}
			$this->db->trans_commit();	
			return true;
		}else{
			$this->db->trans_rollback();	
			return false;
		}
		
			
			
	}

	function _update_transaksi_now($post_array, $primary_key){		
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		unset($post_array['rekeningtujuan']);
		return $this->db->update('temptrxdpa2',$post_array,array('idtemptrxdpa2' => $primary_key));
	}
	
	 function _outputviewtrans($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('Cashflow/CashFlowDPA2trans_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
	}
	
	function ajax_submitCashToday()
	{
		try
		{
			$success = "Berhasil";
			$tanggal = $this->input->post('tanggal');
			$koderekening = $this->input->post('koderekening');
			$tipetransaksi = $this->input->post('tipetransaksi');
			
			//$rekeningtujuan = $this->input->post('rekeningtujuan');
			
			fire_print('log',"koderekening: $koderekening");
		
			
			$this->db->trans_begin();
			
			// if($rekeningtujuan == ""){
			// 	$this->cashflow->updateCashINOUTDPA2($koderekening,$tanggal,$this->npkLogin);
			// 	$hasilKalkulasi = $this->cashflow->updateCashSaldo($koderekening,$tanggal,$this->npkLogin,$tipetransaksi);
			// }else{
			// 	$tipetransaksi = "OUT";
			// 	$this->cashflow->updateCashINOUTDPA2($koderekening,$tanggal,$this->npkLogin);
			// 	$hasilKalkulasi = $this->cashflow->updateCashSaldo($koderekening,$tanggal,$this->npkLogin,$tipetransaksi);
			// 	$tipetransaksi = "IN";
			// 	$koderekening = $rekeningtujuan;
			// 	$this->cashflow->updateCashINOUTDPA2($koderekening,$tanggal,$this->npkLogin);
			// 	$hasilKalkulasi = $this->cashflow->updateCashSaldo($koderekening,$tanggal,$this->npkLogin,$tipetransaksi);
			// }
			$this->cashflow->updateCashINOUTDPA2($koderekening,$tanggal,$this->npkLogin);
			//$this->cashflow->updateCashMOVEDPA2($koderekening,$tanggal,$this->npkLogin);
			$hasilKalkulasi = $this->cashflow->updateCashSaldo($koderekening,$tanggal,$this->npkLogin,$tipetransaksi);
			
			if(!$hasilKalkulasi)
			{
				//echo('gagal simpan usertask lembur');
				fire_print('log','insert cash rollback');
				$this->db->trans_rollback();
				$success = 'Gagal menambahkan cash ke dalam sistem';
			}else{
				fire_print('log','insert cash commit');
				$this->db->trans_commit();					
			}
			echo $success;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	function ajax_submitCashToday_withparam($tanggal, $koderekening, $tipetransaksi)
	{
		try
		{
			$success = "Berhasil";
			fire_print('log',"koderekening: $koderekening");		
			
			$this->db->trans_begin();			
			
			$this->cashflow->updateCashINOUTDPA2($koderekening,$tanggal,$this->npkLogin);			
			$hasilKalkulasi = $this->cashflow->updateCashSaldo($koderekening,$tanggal,$this->npkLogin,$tipetransaksi);
			
			if(!$hasilKalkulasi)
			{
				//echo('gagal simpan usertask lembur');
				fire_print('log','insert cash rollback');
				$this->db->trans_rollback();
				$success = 'Gagal menambahkan cash ke dalam sistem';
			}else{
				fire_print('log','insert cash commit');
				$this->db->trans_commit();					
			}
			echo $success;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */