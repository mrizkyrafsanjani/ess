<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class DPA1CashFlow extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
	
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Cashflow"))
			{
				$this->npkLogin = $session_data['npk'];
				//$this->_cashflowdpa1();
			}
		}else{
			redirect('login','refresh');
		}
    }
	
	public function search($Rekening='',$Bulan='', $Tahun='', $PilihanTglAwal ='',$PilihanTglAkhir ='')
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Cashflow DPA 1"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_cashflowdpa1($Rekening, $Bulan, $Tahun,$PilihanTglAwal,$PilihanTglAkhir);
			}
		}else{
			redirect('login','refresh');
		}
	}
	
	public function _cashflowdpa1($Rekening='', $Bulan='',$Tahun='',$PilihanTglAwal ='',$PilihanTglAkhir ='')
    {
		$crud = new grocery_crud();
		$crud->set_subject('Cashflow DPA1');
		$crud->set_theme('datatables');
		
		$crud->set_model('custom_query_model');
		$crud->set_table('detailsummarycash_'); //Change to your table name
		
		$begin = new DateTime($PilihanTglAwal);
		$end = new DateTime($PilihanTglAkhir);
		$end = $end->modify( '+1 day' ); 

		$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
		
		$i=0;
			
		foreach($daterange as $date){			
			 
			 $dates[] = $date->format("Y-m-d");
			 ${'dateok' . $i} =  $dates[$i] ;
			 $i=$i+1;
			
		}
		
		if ($i==1)
		{
			$praquery1 = "sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok0."' then format(format(d.total,0),0) else 0 end) AS `".$dateok0."`" ;
			$praquery2=  "d.tanggal between '".$dateok0."' and '".$dateok0."'";
		}
		else if ($i==2) 
		{
			$praquery1 = "sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok0."' then format(d.total,0) else 0 end) AS `".$dateok0."`,
					      sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok1."' then format(d.total,0) else 0 end) AS `".$dateok1."`" ;
			$praquery2=  "d.tanggal between '".$dateok0."' and '".$dateok1."'";
			
		}
		else if ($i==3) 
		{
			$praquery1 = "sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok0."' then format(d.total,0) else 0 end) AS `".$dateok0."`,
						  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok1."' then format(d.total,0) else 0 end) AS `".$dateok1."`,
						  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok2."' then format(d.total,0) else 0 end) AS `".$dateok2."`" ;
			$praquery2=  "d.tanggal between '".$dateok0."' and '".$dateok2."'";			
			
		}
		else if ($i==4) 
		{
			$praquery1 = "sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok0."' then format(d.total,0) else 0 end) AS `".$dateok0."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok1."' then format(d.total,0) else 0 end) AS `".$dateok1."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok2."' then format(d.total,0) else 0 end) AS `".$dateok2."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok3."' then format(d.total,0) else 0 end) AS `".$dateok3."`" ;
			$praquery2=  "d.tanggal between '".$dateok0."' and '".$dateok3."'";				
			
		}
		else if ($i==5) 
		{
			$praquery1 = "sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok0."' then format(d.total,0) else 0 end) AS `".$dateok0."`,
						  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok1."' then format(d.total,0) else 0 end) AS `".$dateok1."`,
						  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok2."' then format(d.total,0) else 0 end) AS `".$dateok2."`,
						  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok3."' then format(d.total,0) else 0 end) AS `".$dateok3."`,
						  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok4."' then format(d.total,0) else 0 end) AS `".$dateok4."`" ;
			$praquery2=  "d.tanggal between '".$dateok0."' and '".$dateok4."'";			
			
		}
		else if ($i==6) 
		{
			$praquery1 = "sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok0."' then format(d.total,0) else 0 end) AS `".$dateok0."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok1."' then format(d.total,0) else 0 end) AS `".$dateok1."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok2."' then format(d.total,0) else 0 end) AS `".$dateok2."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok3."' then format(d.total,0) else 0 end) AS `".$dateok3."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok4."' then format(d.total,0) else 0 end) AS `".$dateok4."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok5."' then format(d.total,0) else 0 end) AS `".$dateok5."`" ;
			$praquery2=  "d.tanggal between '".$dateok0."' and '".$dateok5."'";		
			
			
		}
		else if ($i==7) 
		{
			$praquery1 = " sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok0."' then format(d.total,0) else 0 end) AS `".$dateok0."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok1."' then format(d.total,0) else 0 end) AS `".$dateok1."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok2."' then format(d.total,0) else 0 end) AS `".$dateok2."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok3."' then format(d.total,0) else 0 end) AS `".$dateok3."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok4."' then format(d.total,0) else 0 end) AS `".$dateok4."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok5."' then format(d.total,0) else 0 end) AS `".$dateok5."`,
					  sum(case when Date_format(d.tanggal, '%Y-%m-%d') = '".$dateok6."' then format(d.total,0) else 0 end) AS `".$dateok6."`" ;
			$praquery2=  "d.tanggal between '".$dateok0."' and '".$dateok6."'";	
		}
		
		$sqlquery = "SELECT h.idheadersummarycash,d.iddetailsummarycash,d.tipetransaksi,h.koderekening,m.namarekening, ".$praquery1." from headersummarycash_ h 
					  join detailsummarycash_ d on h.idheadersummarycash=d.idheadersummarycash
					  join mstrrekening m on m.koderekening=h.koderekening 
					  where h.DPA=1 and h.koderekening='".$Rekening."' and h.bulan=".$Bulan." and h.tahun=".$Tahun." and  ".$praquery2."
					  group by h.dpa,namarekening,h.koderekening,tipetransaksi
					  order by d.iddetailsummarycash asc";
		
		//echo 	$sqlquery;		
		
	
		
		
		$crud->basic_model->set_query_str($sqlquery); //Query text here
		
		
		if ($i==1)
		{
			$crud->columns('tipetransaksi',$dateok0);
			if ($dateok0==date("Y-m-d")) 
				$crud->callback_column($dateok0,array($this,'_callback_input_trans'));
			else 
				$crud->callback_column($dateok0,array($this,'_callback_show_trans'));
		}
		else if ($i==2) 
		{
			$crud->columns('tipetransaksi',$dateok0,$dateok1);
		}
		else if ($i==3) 
		{
			$crud->columns('tipetransaksi',$dateok0,$dateok1,$dateok2);
		}
		else if ($i==4) 
		{
			$crud->columns('tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3);
		}
		else if ($i==5) 
		{
			$crud->columns('tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3,$dateok4);
		}
		else if ($i==6) 
		{
			$crud->columns('tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3,$dateok4,$dateok5);
		}
		else if ($i==7) 
		{
			$crud->columns('tipetransaksi',$dateok0,$dateok1,$dateok2,$dateok3,$dateok4,$dateok5,$dateok6);
			
		}
		
		$crud->order_by('iddetailsummarycash','asc');
			
		
       
		$crud->unset_delete();
		$crud->unset_edit();
		$crud->unset_add();
		$crud->unset_read();
		$output = $crud->render();
   
        $this-> _outputview($output);    
	
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('Cashflow/CashFlowDPA1table_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	public function _callback_show_trans($value, $row)
	{
		
		  $atts = array(
			'width'	  => '700',
			'height'	 => '700',
			'scrollbars' => 'yes',
			'status'	 => 'yes',
			'resizable'  => 'yes',
			'screenx'	=> '0',
			'screeny'	=> '0'
			);

			return anchor_popup('Cashflow/DPA1DetailTransaksi/search/'.$row->iddetailsummarycash.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}
	
	public function _callback_input_trans($value, $row)
	{
		
		  $atts = array(
			'width'	  => '700',
			'height'	 => '700',
			'scrollbars' => 'yes',
			'status'	 => 'yes',
			'resizable'  => 'yes',
			'screenx'	=> '0',
			'screeny'	=> '0'
			);

			return anchor_popup('Cashflow/DPA1DetailTransaksi/search_input/'.$row->iddetailsummarycash.'/'.$row->koderekening.'/'.$row->tipetransaksi, $value, $atts);
	}

}



/* End of file main.php */
/* Location: ./application/controllers/main.php */