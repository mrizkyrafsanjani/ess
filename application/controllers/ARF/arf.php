<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arf extends CI_Controller {
	var $npkLogin;

    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->model('arf_model','',TRUE);
		$this->load->library('grocery_crud');
		$this->load->helper('date');

    }

    public function index()
    {
        try{
            $session_data = $this->session->userdata('logged_in');
            if($session_data){
                if(check_authorizedByName("Input ARF"))
                {

                    $this->npkLogin = $session_data['npk'];
                    $this->_arf();
                }
            }else{
                redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
            }
        }catch(Exception $e){
            throw new Exception( 'Something really gone wrong', 0, $e);
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }

    }

    public function viewUser($status='',$NPKuser=''){
        if($status == 'non')
            $status = '';
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorizedByName("ARF")){
                $this->npkLogin=$session_data['npk'];
                $this->_arf('viewUser', $status);
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }

    public function viewAdmin($status='')
    {
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorizedByName("All ARF"))
            {
                $this->npkLogin = $session_data['npk'];
                $this->_arf('viewAdmin',$status);
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }

    public  function terima($kode){
        return site_url("ARF/ArfController/terimaARF/$kode");

    }

    public  function tolak($kode){
        return site_url("ARF/ArfController/tolakARF/$kode");

    }

    public  function edit_ARF_user($kode){
        return site_url("ARF/ArfController/EditARFUser/$kode");

    }

    public function cetak($kode){

        return site_url("ARF/ArfController/cetakARF2/$kode");
    }

	public function _arf($page='',$status='')
    {
		try{
			$crud = new grocery_crud();
			$crud->set_subject(  'ARF');
			$crud->set_theme('datatables');

			$crud->set_table('arf');
			$crud->set_relation('NPKUser','mstruser','Nama',array('deleted' => '0'));
            $crud->set_relation('NPKAtasan','mstruser','Nama',array('deleted' => '0'));
            $crud->set_relation('NPKResolver','mstruser','Nama',array('Jabatan' => '27'));
            $crud->set_relation('Departemen','Departemen','NamaDepartemen',array('deleted' => '0'));
            $crud->where('arf.deleted','0');
            

            switch ($page){
                case "viewUser":
                    if($status=='' || $status=='All'){
                        $crud->where('arf.NPKUser', $this->npkLogin);
                        $crud->set_theme('datatables');
                        $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');
                        $crud->unset_edit();
                    }
                    if($status=='BelumDiterima'){
                        $crud->where('arf.StatusARF','Belum Diterima');
                        $crud->where('arf.NPKUser', $this->npkLogin);
                        $crud->set_theme('datatables');
                        $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');

                        $crud->add_action('Ubah','','','ui-icon-check',array($this,'edit_ARF_user'));
                        $crud->add_action('Cetak','','','ui-icon-print',array($this,'cetak'));

                        $crud->unset_edit();
                    }
                    if($status=='Diterima'){
                        $crud->where('arf.StatusARF','Diterima');
                        $crud->where('arf.NPKUser', $this->npkLogin);
                        $crud->set_theme('datatables');
                        $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');
                        $crud->unset_edit();
                    }

                    if($status=='Pending'){
                        $crud->where('arf.StatusARF','Pending');
                        $crud->where('arf.NPKUser', $this->npkLogin);
                        $crud->set_theme('datatables');
                        $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');
                        $crud->unset_edit();
                    }
                    
                    if($status=='BPA'){
                        $crud->where('arf.StatusARF','BPA');
                        $crud->where('arf.NPKUser', $this->npkLogin);
                        $crud->set_theme('datatables');
                        $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');
                        $crud->unset_edit();
                    }
                
                    if($status=='Development'){
                        $crud->where('arf.StatusARF','Development');
                        $crud->where('arf.NPKUser', $this->npkLogin);
                        $crud->set_theme('datatables');
                        $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');
                        $crud->unset_edit();
                    
                    }
                    if($status=='UAT'){
                        $crud->where('arf.StatusARF','UAT');
                        $crud->where('arf.NPKUser', $this->npkLogin);
                        $crud->set_theme('datatables');
                        $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');
                        $crud->unset_edit();
                    }
                    if($status=='Resolve'){
                        $crud->where('arf.StatusARF','Resolve');
                        $crud->where('arf.NPKUser', $this->npkLogin);
                        $crud->set_theme('datatables');
                        $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');
                        $crud->unset_edit();
                    }
                    if($status=='Implementasi'){
                        $crud->where('arf.StatusARF','Implementasi');
                        $crud->where('arf.NPKUser', $this->npkLogin);
                        $crud->set_theme('datatables');
                        $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');
                         $crud->unset_edit();
                    }
                    if($status=='BugFixing'){
                        $crud->like('arf.Subject','Bug Fix');
                        $crud->where('arf.NPKUser', $this->npkLogin);
                        $crud->set_theme('datatables');
                        $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');
                         $crud->unset_edit();
                    }
                    $crud->columns('NoARF', 'Subject', 'ApplicationName', 'ReqDate', 'StatusARF');


                    //$crud->add_action('Cetak', '', '','ui-icon-print',array($this,'cetak'));
                    //
                    //$crud->unset_edit();
                    $crud->unset_add();
                    $crud->unset_delete();
                    $crud->unset_print();
                    $crud->unset_read();
                    $crud->unset_export();
                    break;
                case "viewAdmin":
                    if($status=='' || $status=='All'){
                        $crud->columns('NoARF','NPKUser','Departemen', 'ApplicationName', 'Subject','ReqDate', 'StatusARF', 'NPKResolver','ReceiveDate' );

                        $crud->unset_edit();
                    }
                    if($status =='BelumDiterima'){

                        $crud->where('StatusARF','Belum Diterima');
                        $crud->columns('NPKUser', 'Departemen','ReqDate','Subject', 'ApplicationName');
                        
                        $crud->add_action('Terima','','','ui-icon-check',array($this,'terima'));
                        $crud->add_action('Tolak','','','ui-icon-close',array($this,'tolak'));
                        $crud->unset_read();
                        $crud->unset_edit();
                    }
                    if($status =='Diterima'){
                        $crud->where('StatusARF','Diterima');
                        $crud->columns('NoARF','NPKUser','Departemen','Subject','ReqDate','StatusARF','NPKResolver');

                        $crud->edit_fields('NoARF','NPKUser', 'ReqDate', 'Priority', 'Category', 'StartWorkDate', 'TargetFinishDate', 'StatusARF', 'StartWorkDateReal','NoteWork');
                        $crud->field_type('Priority','dropdown',
                            array(
                                'Urgent' =>'Urgent',
                                'Medium' => 'Medium',
                                'Normal' => 'Normal'
                            )
                        );
                        $crud->field_type('Category','dropdown',
                            array(
                                'Revision' =>'Revision',
                                'Development' => 'Development'
                            )
                        );
                        

                        $crud->field_type('StatusARF','dropdown',
                            array(
                                'BPA' =>'BPA',
                                'Pending' => 'Pending',
                                'Development' => 'Development'
                            )
                        );
                        $crud->field_type('Subject', 'readonly');
                        $crud->callback_update(array($this,'_update_status'));
                    }
                    if($status == 'Pending'){
                        $crud->where('arf.StatusARF','Pending');
                        $crud->columns('NoARF','NPKUser','Departemen','Subject','ReqDate','StatusARF','NPKResolver');
                        $crud->unset_columns('ApplicationName','ReceiveDate');
                        
                        $crud->edit_fields('NoARF','NPKUser', 'StatusARF', 'StartWorkDateReal','NoteWork');
                        $crud->field_type('StatusARF','dropdown',
                            array(
                                'BPA' =>'BPA',
                                
                                'Development' => 'Development'
                            )
                        );
                        $crud->field_type('Subject', 'readonly');
                        $crud->callback_update(array($this,'_update_status'));
                    }
                    if($status=='BPA'){
                        $crud->where('arf.StatusARF','BPA');
                        $crud->columns('NoARF','NPKUser','Departemen','Subject','ReqDate','StatusARF','NPKResolver');

                        $crud->unset_columns('ApplicationName','ReceiveDate');
                        $crud->edit_fields('NoARF','NPKUser', 'StatusARF','StartWorkDateReal','NoteWork');
                        $crud->field_type('StatusARF','dropdown',
                            array(
                                
                                'Pending' => 'Pending',
                                'Development' => 'Development'
                            )
                        );
                        $crud->field_type('Subject', 'readonly');
                        
                        $crud->callback_update(array($this,'_update_status'));
                    }
                    if($status=='Development'){
                        $crud->where('arf.StatusARF','Development');
                        $crud->columns('NoARF','NPKUser','Departemen','Subject','StatusARF','NPKResolver','StartWorkDate',  'TargetFinishDate','StartWorkDateReal');
                        
                        $crud->edit_fields('NoARF','NPKUser', 'StatusARF','StartWorkDateReal', 'FinishDate', 'UATDate', 'NoteWork');
                        $crud->field_type('Subject', 'readonly');
                        $crud->field_type('StatusARF','dropdown',
                            array(
                                'UAT' => 'UAT'
                               
                            )
                        );
                        $crud->field_type('StartWorkDateReal', 'readonly');
                        $crud->set_rules('FinishDate','Tanggal Real Selesai Pengerjaan','callback_validasiTanggal');
                        //$crud->set_rules()

                        $crud->unset_columns('ApplicationName','ReceiveDate');
                         $crud->callback_update(array($this,'_update_status'));
                        }
                    if($status=='UAT'){
                        $crud->where('arf.StatusARF','UAT');
                        $crud->columns('NoARF','NPKUser','Departemen','Subject','StatusARF','NPKResolver','StartWorkDate', 'TargetFinishDate','StartWorkDateReal', 'FinishDate', 'UATDate');
                        
                        $crud->edit_fields('NoARF','NPKUser', 'StatusARF','UATDate', 'ImplementationDate','NoteWork');

                        $crud->unset_columns('ApplicationName','ReceiveDate');
                        $crud->field_type('Subject', 'readonly');
                        $crud->field_type('StatusARF','dropdown',
                            array(
                                'UAT' => 'UAT',
                                'Resolve' => 'Resolve',
                                'Implementation' => 'Implementation'
                            )
                        );
                        $crud->callback_update(array($this,'_update_status'));
                    }
                    if($status=='Resolve'){
                        $crud->where('arf.StatusARF','Resolve');
                        $crud->columns('NoARF','NPKUser','Departemen','Subject','StatusARF','NPKResolver','StartWorkDate', 'TargetFinishDate','StartWorkDateReal', 'FinishDate', 'UATDate');

                        $crud->unset_columns('ApplicationName','ReceiveDate');
                        $crud->edit_fields('NoARF','NPKUser', 'StatusARF', 'ImplementationDate', 'NoteWork');
                        $crud->field_type('Subject', 'readonly');
                        $crud->field_type('StatusARF','dropdown',
                            array(
                                'Implementation' => 'Implementation'
                                
                                
                            )
                        );
                        $crud->callback_update(array($this,'_update_status'));
                    }
                    if($status=='Implementation'){
                        $crud->where('arf.StatusARF','Implementation');
                        $crud->columns('NoARF','NPKUser','Departemen','Subject','StatusARF','NPKResolver', 'StartWorkDateReal', 'FinishDate', 'UATDate', 'ImplementationDate');

                        $crud->unset_columns('ApplicationName','ReceiveDate');
                        $crud->edit_fields('NoARF','NPKUser', 'StatusARF', 'ImplementationDate', 'NoteWork');
                        $crud->field_type('StatusARF', 'readonly');
                        $crud->field_type('Subject', 'readonly');
                        $crud->field_type('ImplementationDate', 'readonly');
                    }

                    if($status=='BugFixing'){
                        $crud->like('arf.Subject','Bug Fix');
                        $crud->columns('NoARF','NPKUser','Departemen','Subject','StatusARF','NPKResolver', 'StartWorkDateReal', 'FinishDate', 'UATDate', 'ImplementationDate');

                        $crud->unset_columns('ApplicationName','ReceiveDate');
                        $crud->edit_fields('NoARF','NPKUser', 'StatusARF', 'ImplementationDate', 'NoteWork');
                        $crud->field_type('StatusARF', 'readonly');
                        $crud->field_type('Subject', 'readonly');
                        $crud->field_type('ImplementationDate', 'readonly');
                    }

                    $crud->field_type('NoARF', 'readonly');
                    $crud->field_type('ReqDate', 'readonly');
                    $crud->field_type('NPKUser', 'readonly');
                    $crud->field_type('Departemen', 'readonly');
                    $crud->field_type('NPKAtasan', 'readonly');
                    $crud->field_type('Request', 'readonly');

                    //$crud->unset_export();
                    $crud->unset_print();
                    $crud->unset_delete();
                    $crud->unset_add();
                 break;
            }

            $crud->field_type('ApplicationName','dropdown',
                array(
                    'Core' =>'Core',
                    'Siap' => 'Siap' ,
                    'DPA Mobile/BB' => 'DPA Mobile/BB',
                    'Accpac' => 'Accpac',
                    'siDapen DPA1' => 'siDapen DPA1',
                    'SIDP' => 'SIDP' ,
                    'Apisoft' => 'Apisoft',
                    'IVR' => 'IVR',
                    'Web Dapenastra' => 'Web Dapenastra',
                    'Vinno SMS' => 'Vinno SMS' ,
                    'ESS' => 'ESS',
                    'Lainnya' => 'Lainnya'
                )
            );
            $crud->add_fields(array('ApplicationName','Subject','Request'));

        
            $crud->required_fields('ApplicationName','Subject','Request', 'StatusARF');

            $crud->callback_insert(array($this,'_insert'));
            $lastnumber= $this->arf_model->getLastNumber();
            $kode = (int)$lastnumber['kode']+1;

            $crud->set_lang_string('insert_success_message',
                'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
                     <script type="text/javascript">
                      window.location = "'.site_url('ARF/ArfController/cetakARF/'.$kode).'";
                     </script>
                     <div style="display:none">
                     '
            );

            $crud->unset_back_to_list();
            
            $crud->display_as('NoARF','No ARF');
            $crud->display_as('ReqDate','Tanggal Request');
            $crud->display_as('NPKUser','Nama User');
            $crud->display_as('NPKAtasan','Nama Atasan');
            $crud->display_as('ApplicationName','Nama Aplikasi');
            $crud->display_as('Subject','Subject/Judul');
            $crud->display_as('ReceiveDate','Tanggal Terima ARF');
            $crud->display_as('DeclineReason','Alasan Ditolak');
            $crud->display_as('NPKResolver','Resolver');
            $crud->display_as('StartWorkDate','Target Pengerjaan');
            $crud->display_as('StartWorkDateReal','Tanggal Real Pengerjaan');
            $crud->display_as('TargetFinishDate','Target Selesai Pengerjaan');
            $crud->display_as('FinishDate','Tanggal Real Selesai Pengerjaan');
            $crud->display_as('UATDate','Tanggal UAT');
            $crud->display_as('ImplementationDate','Tanggal Implementasi');
            $crud->display_as('NoteWork','Note Pengerjaan');
            $crud->display_as('StatusARF','Status');

            $saveButton ='';
            $state = $crud->getState();
            if($state == 'add')
            {
                //echo "<style>#form-button-save{text}</style>";
               $saveButton="$('#form-button-save').prop('value', 'Simpan dan Cetak');";
            }
            $js = "
                        <script>
                            $(document).ready(function() {
                                $saveButton;
                                var cmbStatusARF = $('#field-StatusARF');
                                cmbStatusARF.change(function(){
                                    if(cmbStatusARF.val() == 'Development'){
                                        $('#StartWorkDateReal_field_box').show();
                                        $('#FinishDate_field_box').hide();
                                        $('#UATDate_field_box').hide();
                                    }else if(cmbStatusARF.val() == 'UAT'){
                                        $('#FinishDate_field_box').show();
                                        $('#UATDate_field_box').show();
                                        $('#ImplementationDate_field_box').hide();
                                    }else if(cmbStatusARF.val() == 'Implementation'){
                                        $('#ImplementationDate_field_box').show();
                                        $('#UATDate_field_box').hide();
                                    }else if(cmbStatusARF.val() == 'Diterima'){
                                        $('#NPKResolver_field_box').show();
                                        $('#DeclineReason_field_box').hide();
                                    }else if(cmbStatusARF.val() == 'Ditolak'){
                                        $('#NPKResolver_field_box').hide();
                                        $('#DeclineReason_field_box').show();
                                    }
                                    else{
                                        $('#ImplementationDate_field_box').hide();
                                        $('#StartWorkDateReal_field_box').hide();
                                        $('#UATDate_field_box').hide();
                                        $('#FinishDate_field_box').hide();
                                    }
                                });
                            }); 
                        </script>";
             //echo "<style>#form-button-save{display:none;}</style>";
			$output = $crud->render();
            
			$output->output.=$js;
			

            $this-> _outputview($output, $page);
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }

    function _insert($post_array){
        $userDataLogin = $this->user->findUserByNPK($this->npkLogin);
        if ($userDataLogin){
            foreach($userDataLogin as $userDataLogin){
                $npkAtasan = $userDataLogin->NPKAtasan;
                $emailuser = $userDataLogin->EmailInternal;
                $departemen = $userDataLogin->Departemen;
                $email= 'it@dpa.co.id';
            }
        } else{
            $npkAtasan="";
            $emailuser="";
            $departemen="";
            $email="";
        }


        try{
            $post_array['NPKUser'] = $this->npkLogin;
            $post_array['NPKAtasan']= $npkAtasan;
            $post_array['Departemen']=$departemen;
            $post_array['ReqDate'] = date('Y-m-d H:i:s');
            $post_array['StatusARF']= 'Belum Diterima';
            $post_array['Deleted']= '0';
            $post_array['CreatedOn'] = date('Y-m-d H:i:s');
            $post_array['CreatedBy'] = $this->npkLogin;

            if($this->db->insert('arf',$post_array))
            {
                try{

                    $this->load->library('email');
                    $this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
                    $recipientArr = array($email, $emailuser);

                    $this->email->to($recipientArr);

                    $this->email->subject('ARF Status '.$post_array['StatusARF'].'');
                    $message = 'ARF dengan <br><br>
                    Perihal: ' .$post_array['Subject']. '
                    <br>Deskripsi : '.$post_array['Request'].'

                    telah dibuat dan status saat ini '.$post_array['StatusARF'].' oleh tim IT 
                    <br><br>Sekian Informasinya, 
                    <br>ESS';
                    $this->email->message($message);

                    if( ! $this->email->send())
                    {
                        fire_print('log','gagal email');
                        echo 'gagal';
                    }
                    else
                    {
                        fire_print('log','sukses email');

                        return true;
                    }
                }catch (Exception $e){
                    log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
                    throw new Exception( 'Something really gone wrong', 0, $e);
                }
            }
        }
        catch(Exception $e)
        {
            throw new Exception( 'Something really gone wrong', 0, $e);
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            return false;
        }
    }

    function _update_status($post_array, $primary_key){
        
        try{
            $post_array['UpdatedBy']= $this->npkLogin;
            $post_array['UpdatedOn']= date('Y-m-d H:i:s');
            $no=$primary_key;

            $this->db->trans_start();
            $this->arf_model->insertRwyARF2($no, $this->npkLogin);

           if($this->db->update('arf',$post_array,array('KodeARF'=>$primary_key)))
           {
                
                $dataARF= $this->arf_model->findARFbyKode($no);
                   if($dataARF){
                       foreach ($dataARF as $data){
                            $emailuser= $data->emailuser;
                            $subject= $data->Subject;
                            $req=$data->Request;
                            $NoARF= $data->NoARF;
                            $email="it@dpa.co.id";
                       }
                   }
                   else{
                       $NoARF='';
                       $emailuser='';
                        $subject='';
                        $req='';
                        $email='';
                   }
                $this->sendEmailArf( $NoARF, $post_array['StatusARF'], $emailuser,$email,$subject,$req);
                
               $this->db->trans_complete();
                return $this->db->trans_status();
        
           }
        }catch (Exception $e){
            throw new Exception( 'Something really gone wrong', 0, $e);
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            return false;
        }
    }

    public function sendEmailArf($noArf,$status,$emailuser,$email,$subject,$req){
        fire_print('log', 'sudah masuk ke sebelum kirim email');
        //return $this->sendEmailArf('ARF Status', $post_array['Subject'], $post_array['StatusARF'], $emailuser, $post_array['KodeARF']);
        try{
            fire_print('log','sudah masuk ke awal kirim email');

            $this->load->library('email');
            $this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
            $recipientArr = array($email, $emailuser);

            $this->email->to($recipientArr);

            $this->email->subject('ARF Status '.$status.'');
            $message = 'ARF dengan No ' .$noArf. '<br><br>

            Perihal : '.$subject.'<br>
            Deskripsi : '.$req.'

            kini telah berubah statusnya menjadi '.$status.'
            <br><br>Sekian Informasinya, 
            <br>ESS';
            $this->email->message($message);

            if( ! $this->email->send())
            {
                fire_print('log','gagal email');
                echo 'gagal';
                return false;
            }
            else
            {
                fire_print('log','sukses email');

                return true;
            }
        }catch (Exception $e){
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            throw new Exception( 'Something really gone wrong', 0, $e);
        }
    }

    function _outputview($output= null,$page='')
    {
        $session_data = $this->session->userdata('logged_in');

        $data = array(
            'title' => 'ARF',
            'body' => $output
        );
        $this->load->helper(array('form','url'));
        fire_print('log','page arf: '.$page);
        //$this->load->view('ARF/arfSimple_view',$data);
        if($page!='')
        {
            $this->load->view('ARF/arfSimple_view',$data);
        }
        else
        {
            $this->template->load('default','templates/CRUD_view',$data);
        }
    }

    /*function validasiTanggal($str, $primary_key){
        fire_print('log',"validasiTanggal. str: $str");
        try{
             $dataARF= $this->arf_model->findARFbyKode($no);
                   if($dataARF){
                       foreach ($dataARF as $data){
                           $start= $data->StartWorkDateReal;
                       }
                   }
                   else{
                       $start='';
                   }
            
            $finish = $this->input->post('FinishDate');
            if(!$finish)
            {
                $this->form_validation->set_message('validasiTanggal', 'Tanggal Real Selesai Pengerjaan is required.');
                return false;
            }
            
            if($finish < $start){
                $this->form_validation->set_message('validasiTanggal', 'Tanggal Real Selesai Pengerjaan  harus lebih besar dari Tanggal Real Pengerjaan.');
                return false;
            }

        }catch( Exception $e){

        }
    }*/


}

/* End of file main.php */
/* Location: ./application/controllers/main.php */