<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
    session_start();
    class ArfController extends CI_Controller{
        var $npkLogin;

        function __construct()
        {
            parent::__construct();
            $this->load->model('menu','',TRUE);
            $this->load->model('user','',TRUE);
            $this->load->model('arf_model','',TRUE);
            $this->load->library('grocery_crud');
            $this->load->helper('date');
            $session_data = $this->session->userdata('logged_in');
            $this->npkLogin = $session_data['npk'];
        }

        function index() //view/Admin
        {
            try{
                $session_data = $this->session->userdata('logged_in');
                if($session_data){
                    if(check_authorizedByName("All ARF"))
                    {
                        $data= array(
                            'title' => 'View All ARF',
                            'admin' => '1',
                            'databawahan' => $this->user->getDataBawahan($this->npkLogin),
                            'npk'=>''
                            );
                        $this->load->helper(array('form','url'));
                        $this->template->load('default','ARF/arf_view', $data);
                    }
                }else{
                    redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
                }
            }catch(Exception $e){
                throw new Exception( 'Something really gone wrong', 0, $e);
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            }
        }

        function ViewUser() //ViewUser
        {
            try
            {
                if($this->session->userdata('logged_in'))
                {
                    if(check_authorizedByName("ARF"))
                    {
                        $data = array(
                            'title' => 'View ARF User',
                            'admin' => '0',
                            'npk' => $this->npkLogin,

                        );
                        $this->load->helper(array('form','url'));
                        $this->template->load('default','ARF/arf_view',$data);
                    }
                }
                else
                {
                    redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
                }
            }
            catch(Exception $e)
            {
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
                throw new Exception( 'Something really gone wrong', 0, $e);
            }
        }

        function laporan($dari='', $sampai=''){
            try
            {
                if($this->session->userdata('logged_in'))
                {
                    if(check_authorizedByName("Report ARF"))
                    {
                        //data pending
                        $tanggal= $dari;
                        $tahun= substr($tanggal,0,4);

                        $sql= "SELECT KodeARF, NoARF, u.Nama as namauser,Subject, Request, m.Nama as resolver, date(StartWorkDate) as StartWorkDate, date(TargetFinishDate) as TargetFinishDate
                         from arf a 
                         JOIN mstruser u ON a.NPKUser = u.npk
                         JOIN mstruser m ON a.NPKResolver = m.npk
                         where Year(ReceiveDate)= ? and StatusARF='Pending' OR StatusARF='BPA'   ";
                        $query = $this->db->query($sql, array($tahun))->result() ;

    
                        //data dev
                        $sql2= "SELECT KodeARF, NoARF, u.Nama as namauser,Subject, Request, m.Nama as resolver, date(StartWorkDate) as StartWorkDate, date(TargetFinishDate) as TargetFinishDate
                            from arf a 
                            JOIN mstruser u ON a.NPKUser = u.npk
                            JOIN mstruser m ON a.NPKResolver = m.npk
                            where StatusARF='Development' and Year(ReceiveDate)= ? " ;
                        $query2 = $this->db->query($sql2, array($tahun))->result();

                        //data uat
                        $sql3= "SELECT KodeARF,  NoARF, u.Nama as namauser,Subject, Request, m.Nama as resolver, date(FinishDate) as FinishDate, date(UATDate) as UATDate
                            from arf a 
                            JOIN mstruser u ON a.NPKUser = u.npk
                            JOIN mstruser m ON a.NPKResolver = m.npk
                            where Year(ReceiveDate)=? and StatusARF='UAT' OR StatusARF='Resolve' " ;
                        $query3 = $this->db->query($sql3, array($tahun))->result();

                        //data Implementasi
                        $sql4= "SELECT KodeARF,NoARF, u.Nama as namauser,Subject, Request, m.Nama as resolver, date(UATDate) as UATDate, date(ImplementationDate) as ImplementationDate
                            from arf a 
                            JOIN mstruser u ON a.NPKUser = u.npk
                            JOIN mstruser m ON a.NPKResolver = m.npk
                            where StatusARF='Implementation' and ImplementationDate between ? and ?" ;
                        $query4 = $this->db->query($sql4, array($dari, $sampai))->result();

                        //jumlah arf implemen
                        $sql5="SELECT count(*) as jumlah from arf where StatusARF='Implementation' and Year(ReceiveDate)=?";
                        $query5 = $this->db->query($sql5, array($tahun))->result();
                        foreach ($query5 as $row ) {
                           $jumlah=$row->jumlah;
                        }

                         //jumlah arf tahunan
                        $sql20="SELECT count(*) as ytd from arf where Year(ReceiveDate)=? and Deleted=0 and not StatusARF='Ditolak'";
                        $query20 = $this->db->query($sql20, array($tahun))->result();
                        foreach ($query20 as $row ) {
                           $ytd=$row->ytd;
                        }

                        //jumlah bpa/pending
                        $sql42="SELECT count(*) as jumlahbpa from arf where Year(ReceiveDate)=? and StatusARF='Pending' OR StatusARF='BPA'";
                        $query42 = $this->db->query($sql42, array($tahun))->result();
                        foreach ($query42 as $row ) {
                           $jumlahbpa=$row->jumlahbpa;
                        }

                        //jumlah dev
                        $sql43="SELECT count(*) as jumlahdev from arf where Year(ReceiveDate)=? and StatusARF='Development' ";
                        $query43 = $this->db->query($sql43, array($tahun))->result();
                        foreach ($query43 as $row ) {
                           $jumlahdev=$row->jumlahdev;
                        }

                        //jumlah uat
                        $sql44="SELECT count(*) as jumlahuat from arf where Year(ReceiveDate)=? and StatusARF='UAT' ";
                        $query44 = $this->db->query($sql44, array($tahun))->result();
                        foreach ($query44 as $row ) {
                           $jumlahuat=$row->jumlahuat;
                        }

                        
                        //mrc implementasi
                        $sql6="SELECT count(*) as mrc1 from arf where Departemen=6 and StatusARF='Implementation' and Year(ReceiveDate)=?";
                        $query6 = $this->db->query($sql6, array($tahun))->result();
                        foreach ($query6 as $row ) {
                           $mrc1=$row->mrc1;
                        }

                        //mrc ytd
                        $sql7="SELECT count(*) as mrc2 from arf where Departemen=6 and Year(ReceiveDate)=? and Deleted=0 and not StatusARF='Ditolak'";
                        $query7 = $this->db->query($sql7, array($tahun))->result();
                        foreach ($query7 as $row ) {
                           $mrc2=$row->mrc2;
                        }

                        //mrc bpa/pending
                        $sql21="SELECT count(*) as mrcbpa from arf where Departemen=6 and Year(ReceiveDate)=? and StatusARF='Pending' OR StatusARF='BPA'";
                        $query21 = $this->db->query($sql21, array($tahun))->result();
                        foreach ($query21 as $row ) {
                           $mrcbpa=$row->mrcbpa;
                        }

                        //mrc dev
                        $sql22="SELECT count(*) as mrcdev from arf where Departemen=6 and Year(ReceiveDate)=? and StatusARF='Development' ";
                        $query22 = $this->db->query($sql22, array($tahun))->result();
                        foreach ($query22 as $row ) {
                           $mrcdev=$row->mrcdev;
                        }

                        //mrc uat
                        $sql23="SELECT count(*) as mrcuat from arf where Departemen=6 and Year(ReceiveDate)=? and StatusARF='UAT' ";
                        $query23 = $this->db->query($sql23, array($tahun))->result();
                        foreach ($query23 as $row ) {
                           $mrcuat=$row->mrcuat;
                        }

                        //Fin implementasi
                        $sql8="SELECT count(*) as fin1 from arf where Departemen=3 and StatusARF='Implementation' and Year(ReceiveDate)=?";
                        $query8 = $this->db->query($sql8, array($tahun))->result();
                        foreach ($query8 as $row ) {
                           $fin1=$row->fin1;
                        }

                        //Fin ytd
                        $sql9="SELECT count(*) as fin2 from arf where Departemen=3 and Year(ReceiveDate)=? and Deleted=0 and not StatusARF='Ditolak'";
                        $query9 = $this->db->query($sql9, array($tahun))->result();
                        foreach ($query9 as $row ) {
                           $fin2=$row->fin2;
                        }

                        //fin bpa
                        $sql24="SELECT count(*) as finbpa from arf where Departemen=3 and Year(ReceiveDate)=? and StatusARF='Pending' OR StatusARF='BPA'";
                        $query24 = $this->db->query($sql24, array($tahun))->result();
                        foreach ($query24 as $row ) {
                           $finbpa=$row->finbpa;
                        }

                        //fin dev
                        $sql25="SELECT count(*) as findev from arf where Departemen=3 and Year(ReceiveDate)=? and StatusARF='Development' ";
                        $query25 = $this->db->query($sql25, array($tahun))->result();
                        foreach ($query25 as $row ) {
                           $findev=$row->findev;
                        }

                       //fin uat
                        $sql26="SELECT count(*) as finuat from arf where Departemen=3 and Year(ReceiveDate)=? and StatusARF='UAT' ";
                        $query26 = $this->db->query($sql26, array($tahun))->result();
                        foreach ($query26 as $row ) {
                           $finuat=$row->finuat;
                        }

                        //Acc implementasi
                        $sql10="SELECT count(*) as acc1 from arf where Departemen=4 and StatusARF='Implementation' and Year(ReceiveDate)=?";
                        $query10 = $this->db->query($sql10, array($tahun))->result();
                        foreach ($query10 as $row ) {
                           $acc1=$row->acc1;
                        }

                        //Acc ytd
                        $sql11="SELECT count(*) as acc2 from arf where Departemen=4 and Year(ReceiveDate)=? and Deleted=0 and not StatusARF='Ditolak'";
                        $query11 = $this->db->query($sql11, array($tahun))->result();
                        foreach ($query11 as $row ) {
                           $acc2=$row->acc2;
                        }

                        //acc bpa
                        $sql27="SELECT count(*) as accbpa from arf where Departemen=4 and Year(ReceiveDate)=? and StatusARF='Pending' OR StatusARF='BPA'";
                        $query27 = $this->db->query($sql27, array($tahun))->result();
                        foreach ($query27 as $row ) {
                           $accbpa=$row->accbpa;
                        }

                        //acc dev
                        $sql28="SELECT count(*) as accdev from arf where Departemen=4 and Year(ReceiveDate)=? and StatusARF='Development' ";
                        $query28 = $this->db->query($sql28, array($tahun))->result();
                        foreach ($query28 as $row ) {
                           $accdev=$row->accdev;
                        }

                        //acc uat
                        $sql29="SELECT count(*) as accuat from arf where Departemen=4 and Year(ReceiveDate)=? and StatusARF='UAT' ";
                        $query29 = $this->db->query($sql29, array($tahun))->result();
                        foreach ($query29 as $row ) {
                           $accuat=$row->accuat;
                        }

                        //IT implementasi
                        $sql12="SELECT count(*) as it1 from arf where Departemen=5 and StatusARF='Implementation' and Year(ReceiveDate)=?";
                        $query12 = $this->db->query($sql12, array($tahun))->result();
                        foreach ($query12 as $row ) {
                           $it1=$row->it1;
                        }

                        //IT ytd
                        $sql13="SELECT count(*) as it2 from arf where Departemen=5 and Year(ReceiveDate)=? and Deleted=0 and not StatusARF='Ditolak'";
                        $query13 = $this->db->query($sql13, array($tahun))->result();
                        foreach ($query13 as $row ) {
                           $it2=$row->it2;
                        }

                        //IT bpa
                        $sql30="SELECT count(*) as itbpa from arf where Departemen=5 and Year(ReceiveDate)=? and StatusARF='Pending' OR StatusARF='BPA'";
                        $query30 = $this->db->query($sql30, array($tahun))->result();
                        foreach ($query30 as $row ) {
                           $itbpa=$row->itbpa;
                        }

                        //IT dev
                        $sql31="SELECT count(*) as itdev from arf where Departemen=5 and Year(ReceiveDate)=? and StatusARF='Development' ";
                        $query31 = $this->db->query($sql31, array($tahun))->result();
                        foreach ($query31 as $row ) {
                           $itdev=$row->itdev;
                        }

                        //IT uat
                        $sql32="SELECT count(*) as ituat from arf where Departemen=5 and Year(ReceiveDate)=? and StatusARF='UAT' ";
                        $query32 = $this->db->query($sql32, array($tahun))->result();
                        foreach ($query32 as $row ) {
                           $ituat=$row->ituat;
                        }

                        //hrga implementasi
                        $sql14="SELECT count(*) as hr1 from arf where Departemen=2 and StatusARF='Implementation' and Year(ReceiveDate)=?";
                        $query14 = $this->db->query($sql14, array($tahun))->result();
                        foreach ($query14 as $row ) {
                           $hr1=$row->hr1;
                        }

                        //hrga bpa
                        $sql33="SELECT count(*) as hrbpa from arf where Departemen=2 and Year(ReceiveDate)=? and StatusARF='Pending' OR StatusARF='BPA'";
                        $query33 = $this->db->query($sql33, array($tahun))->result();
                        foreach ($query33 as $row ) {
                           $hrbpa=$row->hrbpa;
                        }

                        //hrga dev
                        $sql34="SELECT count(*) as hrdev from arf where Departemen=2 and Year(ReceiveDate)=? and StatusARF='Development' ";
                        $query34 = $this->db->query($sql34, array($tahun))->result();
                        foreach ($query34 as $row ) {
                           $hrdev=$row->hrdev;
                        }

                        //hrgahrga uat
                        $sql35="SELECT count(*) as hruat from arf where Departemen=2 and Year(ReceiveDate)=? and StatusARF='UAT' ";
                        $query35 = $this->db->query($sql35, array($tahun))->result();
                        foreach ($query35 as $row ) {
                           $hruat=$row->hruat;
                        }

                        //hrga ytd
                        $sql15="SELECT count(*) as hr2 from arf where Departemen=2 and Year(ReceiveDate)=? and Deleted=0 and not StatusARF='Ditolak'";
                        $query15 = $this->db->query($sql15, array($tahun))->result();
                        foreach ($query15 as $row ) {
                           $hr2=$row->hr2;
                        }

                        //Claim implementasi
                        $sql16="SELECT count(*) as ca1 from arf where Departemen=11 and StatusARF='Implementation' and Year(ReceiveDate)=?";
                        $query16 = $this->db->query($sql16, array($tahun))->result();
                        foreach ($query16 as $row ) {
                           $ca1=$row->ca1;
                        }

                        //claim ytd
                        $sql17="SELECT count(*) as ca2 from arf where Departemen=11 and Year(ReceiveDate)=? and Deleted=0 and not StatusARF='Ditolak'";
                        $query17 = $this->db->query($sql17, array($tahun))->result();
                        foreach ($query17 as $row ) {
                           $ca2=$row->ca2;
                        }

                        //claim bpa
                        $sql36="SELECT count(*) as cabpa from arf where Departemen=11 and Year(ReceiveDate)=? and StatusARF='Pending' OR StatusARF='BPA'";
                        $query36 = $this->db->query($sql36, array($tahun))->result();
                        foreach ($query36 as $row ) {
                           $cabpa=$row->cabpa;
                        }

                        //claim dev
                        $sql37="SELECT count(*) as cadev from arf where Departemen=11 and Year(ReceiveDate)=? and StatusARF='Development' ";
                        $query37 = $this->db->query($sql37, array($tahun))->result();
                        foreach ($query37 as $row ) {
                           $cadev=$row->cadev;
                        }

                        //claim uat
                        $sql38="SELECT count(*) as cauat from arf where Departemen=11 and Year(ReceiveDate)=? and StatusARF='UAT' ";
                        $query38 = $this->db->query($sql38, array($tahun))->result();
                        foreach ($query38 as $row ) {
                           $cauat=$row->cauat;
                        }

                        //CSE implementasi
                        $sql18="SELECT count(*) as cse1 from arf where Departemen=8 and StatusARF='Implementation' and Year(ReceiveDate)=?";
                        $query18 = $this->db->query($sql18, array($tahun))->result();
                        foreach ($query18 as $row ) {
                           $cse1=$row->cse1;
                        }

                        //CSE ytd
                        $sql19="SELECT count(*) as cse2 from arf where Departemen=8 and Year(ReceiveDate)=? and Deleted=0 and not StatusARF='Ditolak'";
                        $query19 = $this->db->query($sql19, array($tahun))->result();
                        foreach ($query19 as $row ) {
                           $cse2=$row->cse2;
                        }

                        //CSE bpa
                        $sql39="SELECT count(*) as csebpa from arf where Departemen=8 and Year(ReceiveDate)=? and StatusARF='Pending' OR StatusARF='BPA'";
                        $query39 = $this->db->query($sql39, array($tahun))->result();
                        foreach ($query39 as $row ) {
                           $csebpa=$row->csebpa;
                        }

                        //CSE dev
                        $sql40="SELECT count(*) as csedev from arf where Departemen=8 and Year(ReceiveDate)=? and StatusARF='Development' ";
                        $query40 = $this->db->query($sql40, array($tahun))->result();
                        foreach ($query40 as $row ) {
                           $csedev=$row->csedev;
                        }

                        //CSE uat
                        $sql41="SELECT count(*) as cseuat from arf where Departemen=8 and Year(ReceiveDate)=? and StatusARF='UAT' ";
                        $query41 = $this->db->query($sql41, array($tahun))->result();
                        foreach ($query41 as $row ) {
                           $cseuat=$row->cseuat;
                        }
                       


                        $data = array(
                            'title' => 'Report ARF',
                            'admin' => '0',
                            'npk' => $this->npkLogin,
                            'data1' => $query,
                            'data2' => $query2,
                            'data3'=>$query3,
                            'data4'=>$query4,
                            'jumlah'=>$jumlah,
                            'ytd' => $ytd,
                            'jumlahbpa'=>$jumlahbpa,
                            'jumlahdev'=>$jumlahdev,
                            'jumlahuat'=>$jumlahuat,
                            'tahun'=>$tahun,
                            'dari' => $dari,
                            'sampai'=> $sampai,
                            'mrc1'=>$mrc1,
                            'mrc2'=>$mrc2,
                            'mrcbpa'=>$mrcbpa,
                            'mrcdev'=>$mrcdev,
                            'mrcuat'=>$mrcuat,
                            'fin1'=>$fin1,
                            'fin2'=>$fin2,
                            'finbpa'=>$finbpa,
                            'findev'=>$findev,
                            'finuat'=>$finuat,
                            'acc1'=>$acc1,
                            'acc2'=>$acc2,
                            'accbpa'=>$accbpa,
                            'accdev'=>$accdev,
                            'accuat'=>$accuat,
                            'it1'=>$it1,
                            'it2'=>$it2,
                            'itbpa'=>$itbpa,
                            'itdev'=>$itdev,
                            'ituat'=>$ituat,
                            'hr1'=>$hr1,
                            'hr2'=>$hr2,
                            'hrbpa'=>$hrbpa,
                            'hrdev'=>$hrdev,
                            'hruat'=>$hruat,
                            'ca1'=>$ca1,
                            'ca2'=>$ca2,
                            'cabpa'=>$hrbpa,
                            'cadev'=>$hrdev,
                            'cauat'=>$hruat,
                            'cse1'=>$cse1,
                            'cse2'=>$cse2,
                            'csebpa'=>$csebpa,
                            'csedev'=>$csedev,
                            'cseuat'=>$cseuat
                            
                        );
                        $this->load->helper(array('form','url'));
                        $this->template->load('default','ARF/reportArf_view',$data);
                    }
                }
                else
                {
                    redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
                }
            }
            catch(Exception $e)
            {
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
                throw new Exception( 'Something really gone wrong', 0, $e);
            }
        }
        

        function cetakARF($kode){
            try{
                $a = $this->arf_model->findARFbyKode($kode);
                foreach ($a as $row){
                    $subject= $row->Subject;
                    $reqdate = $row->ReqDate;
                    $namauser = $row->namauser;
                    $deptuser = $row->deptuser;
                    $jabatanuser = $row->jabatanuser;
                    $emailuser = $row->emailuser;
                    $extuser = $row->extuser;
                    $atasan = $row->namaatasan;
                    $deptatasan = $row->deptatasan;
                    $jabatanatasan = $row->jabatanatasan;
                    $emailatasan = $row->emailatasan;
                    $extatasan = $row->extatasan;
                    $application = $row->ApplicationName;
                    $request = $row->Request;
                    $user = $row->NPKUser;

                    $query = $this->db->get_where('globalparam',array('Name'=>'DIC_IT'));
                    foreach($query->result() as $row)
                    {
                        $npkDIC = $row->Value;
                    }

                    $query2 = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
                    foreach($query2->result() as $row)
                    {
                        $npkHead = $row->Value;
                    }

                    $query3 = $this->user->dataUser($npkDIC);
                    foreach($query3 as $row)
                    {
                        $namaDIC = $row->nama;
                        $jabatanDIC = $row->jabatan;
                    }

                    $query3 = $this->user->dataUser($npkHead);
                    foreach($query3 as $row)
                    {
                        $namaHead = $row->nama;
                        $jabatanHead = $row->jabatan;
                    }

                    $a_array[]= array(
                            'KodeARF' => $kode,
                            'subject'=> $subject,
                            'reqdate' => $reqdate,
                            'namauser' => $namauser,
                            'deptuser' => $deptuser,
                            'jabatanuser' => $jabatanuser,
                            'emailuser' => $emailuser,
                            'extuser' => $extuser,
                            'atasan' => $atasan,
                            'deptatasan' => $deptatasan,
                            'jabatanatasan' => $jabatanatasan,
                            'emailatasan' => $emailatasan,
                            'extatasan' => $extatasan,
                            'application' => $application,
                            'request' => $request,
                            'user' => $user,
                            'namaDIC'=> $namaDIC,
                            'jabatanDIC'=>$jabatanDIC,
                            'namaHead'=>$namaHead,
                            'jabatanHead'=> $jabatanHead
                        );

                }

                $data = array(
                    'title' => 'Cetak ARF',
                    'a_array' =>$a_array,
                    'npk' => $this->npkLogin,
                );
                $this->load->helper(array('form','url'));
                return $this->load->view('ARF/cetakARF_view',$data);
            } catch (Exception $e){
                redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
            }
        }

        function cetakARF2($kode){
            try{
                $a = $this->arf_model->findARFbyKode($kode);
                foreach ($a as $row){
                    $subject= $row->Subject;
                    $reqdate = $row->ReqDate;
                    $namauser = $row->namauser;
                    $deptuser = $row->deptuser;
                    $jabatanuser = $row->jabatanuser;
                    $emailuser = $row->emailuser;
                    $extuser = $row->extuser;
                    $atasan = $row->namaatasan;
                    $deptatasan = $row->deptatasan;
                    $jabatanatasan = $row->jabatanatasan;
                    $emailatasan = $row->emailatasan;
                    $extatasan = $row->extatasan;
                    $application = $row->ApplicationName;
                    $request = $row->Request;
                    $user = $row->NPKUser;

                    $query = $this->db->get_where('globalparam',array('Name'=>'DIC_IT'));
                    foreach($query->result() as $row)
                    {
                        $npkDIC = $row->Value;
                    }

                    $query2 = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
                    foreach($query2->result() as $row)
                    {
                        $npkHead = $row->Value;
                    }

                    $query3 = $this->user->dataUser($npkDIC);
                    foreach($query3 as $row)
                    {
                        $namaDIC = $row->nama;
                        $jabatanDIC = $row->jabatan;
                    }

                    $query3 = $this->user->dataUser($npkHead);
                    foreach($query3 as $row)
                    {
                        $namaHead = $row->nama;
                        $jabatanHead = $row->jabatan;
                    }

                    $a_array[]= array(
                            'KodeARF' => $kode,
                            'subject'=> $subject,
                            'reqdate' => $reqdate,
                            'namauser' => $namauser,
                            'deptuser' => $deptuser,
                            'jabatanuser' => $jabatanuser,
                            'emailuser' => $emailuser,
                            'extuser' => $extuser,
                            'atasan' => $atasan,
                            'deptatasan' => $deptatasan,
                            'jabatanatasan' => $jabatanatasan,
                            'emailatasan' => $emailatasan,
                            'extatasan' => $extatasan,
                            'application' => $application,
                            'request' => $request,
                            'user' => $user,
                            'namaDIC'=> $namaDIC,
                            'jabatanDIC'=>$jabatanDIC,
                            'namaHead'=>$namaHead,
                            'jabatanHead'=> $jabatanHead
                        );

                }

                $data = array(
                    'title' => 'Cetak ARF',
                    'a_array' =>$a_array,
                    'npk' => $this->npkLogin,
                );
                $this->load->helper(array('form','url'));
                return $this->load->view('ARF/cetakARF2_view',$data);
            } catch (Exception $e){
                redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
            }
        }

        public  function terimaARF($kode){
            if(check_authorizedByName("All ARF")){
                $dataARF= $this->arf_model->findARFbyKode($kode);

                $tahun = date('Y');
                $noarf = 'ARF/'.substr($tahun, -2).'/'.$this->user->integerToRoman(date('n'));
                $sqlSelect = "SELECT NoARF from arf where NoARF LIKE '%$noarf%' order by NoARF desc limit 1";
                $result = $this->db->query($sqlSelect);
                if($result->num_rows() == 0){
                    $lastNumber = 0;
                }else{
                    $currentnoarf=$result->result()[0]->NoARF;
                    $lastNumber = explode('/', $currentnoarf);
                    $lastNumber = $lastNumber[3];
                }
                $lastNumber++;
                $noarf = $noarf.'/'.str_pad($lastNumber,3,"0",STR_PAD_LEFT);
                if($dataARF){
                    foreach ($dataARF as $row) {
                        $dataARFdetail=array(
                            'kode' => $row->KodeARF,
                            'NoARF'=>$noarf,
                            'title' => 'Data ARF yang akan diterima',
                            'subject' => $row->Subject,
                            'user' => $row->namauser,
                            'resolver'=>'',
                            'status'=>'Diterima',
                            'updatedby'=> $this->npkLogin,
                            'updatedon'=>date('Y-m-d H:i:s')
                        );
                    }
                    $this->load->helper(array('form','url'));
                    $this->load->view('ARF/arf_terima_view', $dataARFdetail);
                }else{
                    echo 'Gagal';
                }
            }
        }

        public  function tolakARF($kode){
            if(check_authorizedByName("All ARF")){
                $dataARF= $this->arf_model->findARFbyKode($kode);
                if($dataARF){
                    foreach ($dataARF as $row) {
                        $dataARFdetail=array(
                            'kode' => $row->KodeARF,
                            'subject' => $row->Subject,
                            'user' => $row->namauser,
                            'decline'=>'',
                        );
                    }
                    $this->load->helper(array('form','url'));
                    $this->load->view('ARF/arf_tolak_view', $dataARFdetail);
                }else{
                    echo 'Gagal';
                }
            }
        }

        public function EditARFUser($kode){
            try{
                if(!$this->session->userdata('logged_in'))
                {
                    redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');                
                }
                else{
                    if(check_authorizedByName("ARF")){
                        $dataARF= $this->arf_model->findARFbyKode($kode);
                        if($dataARF){
                            foreach ($dataARF as $row) {
                                $dataARFdetail= array(
                                    'kode' => $row->KodeARF,
                                    'subject' => $row->Subject,
                                    'application'=>$row->ApplicationName,
                                    'request'=> $row->Request 
                                );
                            }
                        }else{
                            echo $this->npkLogin;
                        }
                        $this->load->helper(array('form','url'));
                        $this->load->view('ARF/arf_editUser_view',$dataARFdetail);
                    }
                }
            }catch(Exception $e){

            }
        }

        function do_terimaARF($kode){
            if(!$this->session->userdata('logged_in'))
            {
                redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
            }

            try{
                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<div id="divError">', '</div>');
                $this->form_validation->set_rules('cmbStatus', 'Resolver', 'trim|required|xss_clean');

                if($this->form_validation->run()==false){
                    $this->terimaARF($kode);
                }else{
                    if($this->input->post()){
                        if(!$this->arf_model->updateTerima($kode)){
                            $this->session->set_flashdata('msg','Kode yang di Input sudah terpakai');
                            redirect('ARF/ArfController/terimaARF/'.$kode);
                        }
                        else{
                            redirect('ARF/Arf/viewAdmin/BelumDiterima','refresh');
                        }
                    }

                }

            }catch (Exception $e){
                throw new Exception( 'Something really gone wrong', 0, $e);
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            }
        }

        function do_tolakARF($kode){
            if(!$this->session->userdata('logged_in'))
            {
                redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
            }

            try{
                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<div id="divError">', '</div>');
                $this->form_validation->set_rules('txtDecline', 'Decline Reason', 'trim|required|xss_clean');

                if($this->form_validation->run()==false){
                    $this->terimaARF($kode);
                }else{
                    if($this->input->post()){
                        if(!$this->arf_model->updateTolak($kode)){
                            $this->session->set_flashdata('msg','Kode yang di Input sudah terpakai');
                            redirect('ARF/ArfController/tolakARF/'.$kode);
                        }
                        else{
                            redirect('ARF/Arf/viewAdmin/BelumDiterima','refresh');
                        }
                    }

                }

            }catch (Exception $e){
                throw new Exception( 'Something really gone wrong', 0, $e);
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            }
        }

        function SubmitEditARFUser($kode){
            if(!$this->session->userdata('logged_in'))
            {
                redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
            }

            try{
                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<div id="divError">', '</div>');
                $this->form_validation->set_rules('txtSubject', 'Resolver', 'trim|required|xss_clean');
                $this->form_validation->set_rules('cmbAplikasi', 'Application', 'trim|required|xss_clean');
                $this->form_validation->set_rules('txtReq', 'Request', 'trim|required|xss_clean');

                if($this->form_validation->run()==false){
                    $this->terimaARF($kode);
                }else{
                    if($this->input->post()){
                        if(!$this->arf_model->updateARFUser($kode)){
                            $this->session->set_flashdata('msg','Kode yang di Input sudah terpakai');
                            redirect('ARF/ArfController/EditARFUser/'.$kode);
                        }
                        else{
                            redirect('ARF/ArfController/cetakARF2/'.$kode);
                        }
                    }

                }

            }catch (Exception $e){
                throw new Exception( 'Something really gone wrong', 0, $e);
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            }
        }

        public function diagram(){
            try{
                $sql= "select count(*) as YTD, Departemen from arf where not StatusARF='Ditolak'";
                $query = $this->db->query($sql);
                $result = $result= $query->result();
                if($result){
                    foreach ($result as $row) {
                        $dataawal= array(
                            'jumlah'=>$row->YTD,
                            'dept'=> $row->Departemen
                        );  
                    }
                } 
                $this->load->helper(array('form','url'));
                $this->template->load('default','ARF/chart_view', $dataawal);
            }catch(Exception $e){
                throw new Exception( 'Something really gone wrong', 0, $e);
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            }
            
        }
    }

?>