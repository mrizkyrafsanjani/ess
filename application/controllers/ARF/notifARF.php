<?php
class NotifARF extends CI_Controller {
    var $npkLogin;
    function __construct()
    {
        parent::__construct();
        $this->load->model('user','',TRUE);
        $this->load->model('URF','',TRUE);
        $this->load->helper('string');
        $this->load->library('email');
        $this->load->model('usertask','',TRUE);
    }

    public function index()
    {
        $this->CheckerARF();
        $this->cekTerlambat();
        $this->cekUltah();
    }

    function CheckerARF(){
        $sqlSelect =
        "select NoARF, Subject, TargetFinishDate, StatusARF, u.Nama as NamaResolver, datediff(TargetFinishDate,curdate()) as selisih from arf a join mstruser u on a.NPKResolver = u.NPK
        where a.Deleted = 0 AND StatusARF not in ('Implementation','Ditolak','Belum Diterima') and TargetFinishDate between curdate() and date_add(curdate(),interval 7 day)";
        $data = $this->db->query($sqlSelect);
        $message = "";
        if($data->num_rows() > 0){
            $message = "*List ARF yang akan terkena deadline 1 minggu lagi atau kurang:* <br/>";
            foreach($data->result() as $row)
            {
                $message .= "$row->NoARF - $row->Subject ($row->StatusARF) Target: ".substr($row->TargetFinishDate,0,10).". Resolver: `$row->NamaResolver`. Tinggal $row->selisih hari. <br/>";
            }
        }

        $sqlSelect =
        "select NoARF, Subject, TargetFinishDate, StatusARF, u.Nama as NamaResolver, datediff(curdate(),TargetFinishDate) as selisih from arf a join mstruser u on a.NPKResolver = u.NPK
        where a.Deleted = 0 AND StatusARF not in ('Implementation','Ditolak','Belum Diterima') and TargetFinishDate < curdate();";
        $data = $this->db->query($sqlSelect);
        if($data->num_rows() > 0){
            $message .= "*List ARF yang sudah melewati deadline:* <br/>";
            foreach($data->result() as $row)
            {
                $message .= "$row->NoARF - $row->Subject ($row->StatusARF) Target: ".substr($row->TargetFinishDate,0,10).". Resolver: `$row->NamaResolver`. Sudah lewat $row->selisih hari. <br/>";
            }
        }

        if($message != ""){
            $message=str_replace('&','dan',str_replace('<br/>','%0A',$message));
            $this->load->helper('sendFleep_helper');
            sendFleep($message,"ARF Notifier");
            //echo $message;
        }

    }

    function cekRankingTerlambat($NPK)
    {
        $data = array();
        $rank = 0;
        $bulan = 0;
        $tahun = 0;
        $sqlSelect = "select * from (
            select *,@curRank := IF(@prevVal=jumlah, @curRank, @curRank := @curRank+1) AS rank,@prevVal:=jumlah from (
            select NPK,EmailInternal,Nama, bulan, tahun, count(0) as jumlah, tanggalDataMax
            from (
                        select a.NPK, u.Nama, u.EmailInternal, Tanggal, Hari, a.WaktuMasuk, month(a.Tanggal) as bulan, year(a.Tanggal) as tahun, amax.tanggalDataMax from absensi a join mstruser u on a.NPK = u.NPK
                        join (select a.NPK, max(a.Tanggal) as tanggalDataMax from absensi a where a.Deleted = 0 and a.Keterangan is null group by a.NPK) amax on amax.NPK = a.NPK
                        where a.Deleted = 0 and a.Hari not in ('Sabtu','Minggu') and a.Tanggal >= concat(cast(year(date_add(curdate(), interval 0 month)) as char(4)),'-',cast(month(date_add(curdate(), interval 0 month)) as char(2)),'-01')
                        and a.NPK not in (select NPK from mstruser m join jabatan j on m.Jabatan = j.KodeJabatan where (j.NamaJabatan like '%Director%' or j.NamaJabatan like '%head%' or j.NamaJabatan like '%leader%') and j.NamaJabatan not like '%Section%' and (TanggalBerhenti = '0000-00-00' or TanggalBerhenti is null))
                        and WaktuMasuk >= (select ADDTIME(JamMasuk , '00:01:00') from jamkerja) and a.Keterangan is null
                        ) temp
                        group by EmailInternal,Nama, bulan, tahun,tanggalDataMax
            ) temp2, (SELECT @curRank := 0, @prevVal := NULL) AS x
            order by jumlah DESC
            ) temp3
            where NPK = ?";
        $data = $this->db->query($sqlSelect,array($NPK));
        if($data->num_rows() > 0){
            foreach($data->result() as $row)
            {
                $data = array("rank"=>$row->rank,"bulan"=>$row->bulan,"tahun"=>$row->tahun);
            }
        }else{
            $data = array("rank"=>0);
        }
        return $data;
    }

    function cekTerlambat()
    {
        $sqlSelect = "select NPK,EmailInternal,Nama, bulan, tahun, count(0) as jumlah, tanggalDataMax from (
            select a.NPK, u.Nama, u.EmailInternal, Tanggal, Hari, a.WaktuMasuk, month(a.Tanggal) as bulan, year(a.Tanggal) as tahun, amax.tanggalDataMax from absensi a join mstruser u on a.NPK = u.NPK
            join (select a.NPK, max(a.Tanggal) as tanggalDataMax from absensi a where a.Deleted = 0 and a.Keterangan is null group by a.NPK) amax on amax.NPK = a.NPK
            where a.Deleted = 0 and a.Hari not in ('Sabtu','Minggu') and a.Tanggal >= concat(cast(year(date_add(curdate(), interval -1 month)) as char(4)),'-',cast(month(date_add(curdate(), interval -1 month)) as char(2)),'-01')
            and a.NPK in (select NPK from mstruser where Departemen = '5' and Deleted = 0 and (TanggalBerhenti = '0000-00-00' or TanggalBerhenti is null) and Jabatan != 33 )
            and WaktuMasuk >= (select ADDTIME(JamMasuk , '00:01:00') from jamkerja) and a.Keterangan is null
            ) temp
            group by EmailInternal,Nama, bulan, tahun,tanggalDataMax
            order by EmailInternal, tahun, bulan";
        $data = $this->db->query($sqlSelect);
        $message = "";
        if($data->num_rows() > 0){
            $message = "";
            $targetemailsblm = "";
            $namasblm = "";
            $i = 1;
            foreach($data->result() as $row)
            {
                if($row->EmailInternal != $targetemailsblm)
                {
                    echo $message;
                    $this->sendEmail($targetemailsblm,$namasblm,$message);
                    $dataRank = $this->cekRankingTerlambat($row->NPK);
                    $messageRank = "";
                    if($dataRank["rank"] > 0)
                    {
                        $messageRank = "Selamat Anda menjadi <b><font color='red'> peringkat nomor ". $dataRank["rank"] ."</font></b> atas keterlambatan Anda pada bulan ". $this->getMonthName($dataRank["bulan"]) ." ". $dataRank["tahun"] ." dibandingkan seluruh karyawan DPA :)<br/>";
                    }
                    $message = "*Jumlah keterlambatan $row->Nama per bulan (data per tanggal $row->tanggalDataMax):* <br/>$messageRank<font size='1'>(Jika ada merasa izin yang harusnya tidak dihitung terlambat tapi belum dimasukkan, mohon segera minta ke HRD untuk diinput. Terima kasih)</font><br/>";
                }
                $message .= "Bulan " . $this->getMonthName($row->bulan) ." Tahun $row->tahun : $row->jumlah kali <br/>";
                $targetemailsblm = $row->EmailInternal;
                $namasblm = $row->Nama;
                $i++;
            }
            echo $message;
            $this->sendEmail($targetemailsblm,$namasblm,$message);
        }
    }

    function cekUltah()
    {
        $sqlSelect = "select Nama, TanggalLahir, TanggalBerhenti, day(TanggalLahir) as tgl, month(TanggalLahir) as bulan from mstruser
        where Deleted = 0 and (TanggalBerhenti is null or TanggalBerhenti = '0000-00-00') and TanggalLahir is not null
        and day(date_add(TanggalLahir,INTERVAL -1 DAY)) = day(now()) and month(date_add(TanggalLahir,INTERVAL -1 DAY)) = month(now());";
        $data = $this->db->query($sqlSelect);
        $message = "Besok akan ada yang ulang tahun. Berikut orang-orang yang akan berulang tahun besok : <br/>";
        $nama = "";
        if($data->num_rows() > 0){
            foreach($data->result() as $row)
            {
                $nama .= $row->Nama .", ";
                $message .= $row->Nama . "<br/>";
            }
            $this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
            $this->email->to("william@dpa.co.id");
            $this->email->subject("Besok Ulang Tahun ". rtrim($nama,", "));
            $this->email->message($message);
            $this->email->send();
        }
    }

    function sendEmail($to,$nama,$message)
    {
        $this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
        $this->email->to($to);
        $this->email->cc("william@dpa.co.id");
        $this->email->subject('Jumlah Terlambat '.$nama);
        if($message != ''){
            $this->email->message($message);
            $this->email->send();
        }
    }

    function getMonthName($monthNum)
    {
        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
        $monthName = $dateObj->format('F');
        return $monthName;
    }
}

?>