<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanTipeAsset extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
		$this->load->model('user','',TRUE);
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		if($this->session->userdata('logged_in')){
			if(check_authorized("12"))
			{
				$this->_tipeAsset();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _tipeAsset()
    {
		$currUserDepartemen = $this->getCurrentUserDepartemen();
		if($currUserDepartemen != "IT")
		{
			$currUserDepartemen = "HRGA";
		}
		$crud = new grocery_crud();
		$crud->set_subject('Tipe Asset');
		//$crud->set_theme('datatables');
		
        $crud->set_table('tipeasset');
		$crud->where('tipeasset.deleted','0');
		$crud->where('tipeasset.AssetMilik',$currUserDepartemen);
		
		$crud->columns('KodeTipeAsset', 'AssetMilik', 'Deskripsi', 'NomorTipeAsset', 'CreatedOn', 'CreatedBy', 'UpdatedOn', 'UpdatedBy');
		$crud->fields('AssetMilik', 'Deskripsi', 'NomorTipeAsset');
				
		$crud->required_fields('AssetMilik', 'Deskripsi', 'NomorTipeAsset');
		
		$crud->callback_delete(array($this,'_delete'));
		$crud->unset_texteditor('Description','Url');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _delete($primary_key){
		return $this->db->update('tipeasset',array('deleted' => '1'),array('KodeTipeAsset' => $primary_key));
	}
	
	function getCurrentUserDepartemen()
	{
		$session_data = $this->session->userdata('logged_in');
		$dataUser = $this->user->dataUser($session_data['npk']);
		if($dataUser){
			foreach($dataUser as $dataUser){
				$dataUserDetail = array(
					'departemen'=> $dataUser->departemen
				);
			}
		}else{
			echo 'gagal get current user departemen';
		}
		return $dataUserDetail['departemen'];
	}
		
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */