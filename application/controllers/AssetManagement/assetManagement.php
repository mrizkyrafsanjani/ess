<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AssetManagement extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->model('lokasiAsset','',TRUE);
		$this->load->model('departemen','',TRUE);
		$this->load->model('tipeAsset','',TRUE);
		$this->load->model('asset','',TRUE);
		$this->load->library('grocery_crud');
		$this->load->library('gc_dependent_select');		
		//$this->load->helper('authorized_helper');
    }
 
    public function index($action='')
    {
		try{			
			$session_data = $this->session->userdata('logged_in');
			if($session_data){			
				if(check_authorized("10"))//menu aset management
				{
					$this->_assetmanagement($action);
				}
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );		 
		}
    }
	
	public function view()
	{
		try{
			$session_data = $this->session->userdata('logged_in');
			if($session_data){			
				if(check_authorized("10"))//menu aset management
				{
					$this->_assetmanagement('view');
				}
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );		 
		}
	}

	public function _assetmanagement($view = '')
    {
		$currUserDepartemen = $this->getCurrentUserDepartemen();
		$crud = new grocery_crud();
		$crud->set_subject('Asset');
		$crud->set_theme('datatables');		
		
        $crud->set_table('asset');
		$crud->where('asset.deleted','0');
		if($currUserDepartemen != "IT")
		{
			$currUserDepartemen = "HRGA";
		}
		$crud->where('asset.AssetMilik',$currUserDepartemen);
		$crud->set_relation('KodeTipeAsset','tipeasset','Deskripsi',array('deleted' => '0','AssetMilik' => $currUserDepartemen));
		$crud->set_relation('KodeDepartemen','departemen','NamaDepartemen',array('deleted' => '0'));
		$crud->set_relation('KodeLokasiAsset','lokasiasset','Deskripsi',array('deleted' => '0'));
		$crud->set_relation('KodeStatusAsset','statusasset','Deskripsi',array('deleted' => '0'));
		
		
		if($currUserDepartemen == "IT")
		{				
			$crud->columns('KodeAsset','DPA','AssetMilik','JenisAsset','KodeTipeAsset','MerekAsset','SpesifikasiAsset','TanggalPembelian','HargaAsset','KodeStatusAsset','KodeLokasiAsset','KodeDepartemen','TahunLelang','HargaLelang','Keterangan','CreatedOn');
			$crud->fields('KodeAsset','DPA','AssetMilik','JenisAsset','KodeTipeAsset','MerekAsset','SpesifikasiAsset','TanggalPembelian','HargaAsset','KodeStatusAsset','KodeLokasiAsset','KodeDepartemen','TahunLelang','HargaLelang','Keterangan');
		}
		else
		{
			$crud->columns('KodeAsset','DPA','AssetMilik','KodeTipeAsset','MerekAsset','SpesifikasiAsset','TanggalPembelian','HargaAsset','KodeStatusAsset','KodeLokasiAsset','KodeDepartemen','TahunLelang','HargaLelang','Keterangan','CreatedOn');
			$crud->fields('KodeAsset','DPA','AssetMilik','KodeTipeAsset','MerekAsset','SpesifikasiAsset','TanggalPembelian','HargaAsset','KodeStatusAsset','KodeLokasiAsset','KodeDepartemen','TahunLelang','HargaLelang','Keterangan');
		}
		$crud->display_as('AssetMilik','Asset');
        $crud->display_as('JenisAsset','Jenis Asset');
		$crud->display_as('KodeTipeAsset','Tipe Asset');
		$crud->display_as('KodeStatusAsset','Status Asset');
		$crud->display_as('KodeLokasiAsset','Lokasi Asset');
		$crud->display_as('KodeDepartemen','Departemen');
		
		//$crud->required_fields('KodeAsset');
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		$crud->callback_field('KodeAsset',array($this,'field_callback_KodeAsset'));
		$crud->callback_field('DPA',array($this,'add_field_callback_DPA'));
		$crud->callback_field('JenisAsset',array($this,'add_field_callback_JenisAsset'));
		$crud->callback_field('KodeDepartemen',array($this,'add_field_callback_KodeDepartemen'));
		$crud->callback_field('AssetMilik',array($this,'add_field_callback_AssetMilik'));
		$crud->callback_delete(array($this,'_delete_asset'));
		$crud->callback_update(array($this,'_update_asset'));
		$crud->callback_insert(array($this,'_insert_asset'));		
		$crud->unset_texteditor('SpesifikasiAsset','Keterangan');

		//---dependent combobox
		/*
		$fields = array(
			// first field:
			'KodeLokasiAsset' => array( // first dropdown name
			'table_name' => 'lokasiasset', // table of country
			'title' => 'Deskripsi', // country title
			'relate' => null // the first dropdown hasn't a relation
			),
			// second field
			'KodeDepartemen' => array( // second dropdown name
			'table_name' => 'departemen', // table of state
			'title' => 'NamaDepartemen', // state title
			'id_field' => 'KodeDepartemen', // table of state: primary key
			'relate' => 'KodeDepartemen', // table of state:
			'data-placeholder' => 'select departemen' //dropdown's data-placeholder:
			)
		);
		$config = array(
			'main_table' => 'asset',
			'main_table_primary' => 'KodeAsset',
			"url" => base_url() . __CLASS__ . '/' . __FUNCTION__ . '/' //path to method
			//'ajax_loader' => base_url() . 'ajax-loader.gif' // path to ajax-loader image. It's an optional parameter
			//'segment_name' =>'Your_segment_name' // It's an optional parameter. by default "get_items"
		);
		$categories = new gc_dependent_select($crud, $fields, $config);		
		$js = $categories->get_js();		
		*/
		//--end of dependent combobox		
		
		$js = " <script>
		$(document).ready(function() {
		var KodeLokasiAsset = $('#field-KodeLokasiAsset');
		var KodeDepartemen = $('#field-KodeDepartemen');
		KodeLokasiAsset.change(function(){
			$.ajax({
			  url: '" .base_url()."index.php/AssetManagement/AssetManagement/getDepartemenLokasi',
			  type: 'POST',
			  data: {id : KodeLokasiAsset.val()},
			  dataType: 'html'
			}).done(function(response){
				 KodeDepartemen.val(response);				 
			});
			//KodeDepartemen.val(KodeLokasiAsset.val());
		});
		
	}); </script>";
		
        $output = $crud->render();   
		$output->output.= $js;
        $this-> _outputview($output,$view);        
    }
	
	function field_callback_KodeAsset($value= '', $primary_key = null)
	{
		return '<input type="text" name="KodeAsset" value="'.$value.'" readonly>';
	}
	
	function getCurrentUserDepartemen()
	{
		$session_data = $this->session->userdata('logged_in');
		$dataUser = $this->user->dataUser($session_data['npk']);
		if($dataUser){
			foreach($dataUser as $dataUser){
				$dataUserDetail = array(
					'departemen'=> $dataUser->departemen
				);
			}
		}else{
			echo 'gagal get current user departemen';
		}
		return $dataUserDetail['departemen'];
	}
	
	function add_field_callback_AssetMilik($value= '', $primary_key = null)
	{
		$value = $this->getCurrentUserDepartemen();
		return '<input type="text" name="AssetMilik" value="'.$value.'" readonly>';
	}
	
	function add_field_callback_DPA($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selected1 = '';
		$selected2 = '';
		if($value == "Satu"){
			$selected1 = 'selected';
		}else if($value == "Dua"){
			$selected2 = 'selected';
		}
		
		return ' <select name="DPA">
			<option value = ""></option>
			<option '.$selected1.' value="Satu">Satu</option>
			<option '.$selected2.' value="Dua">Dua</option>
		</select>';
	}
	
	function add_field_callback_JenisAsset($value = '', $primary_key = null)
	{
		$selected1 = '';
		$selected2 = '';
		if($value == "Hardware"){
			$selected1 = 'selected';
		}else if($value == "Software"){
			$selected2 = 'selected';
		}
		
		return ' <select name="JenisAsset">
			<option value = ""></option>
			<option '.$selected1.' value="Hardware">Hardware</option>
			<option '.$selected2.' value="Software">Software</option>
		</select>';
	}
	
	function add_field_callback_KodeDepartemen($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$htmlText = '<select id="field-KodeDepartemen" name="KodeDepartemen">';
		$departemen = $this->departemen->getDepartemen('');
		$count = 0;
		if($departemen){
			foreach($departemen as $row){
				$htmlText .= '<option ';
				if($value == $count + 1)
					$htmlText .= ' selected ';
				$htmlText .= ' value="'.$row->KodeDepartemen.'">'.$row->NamaDepartemen.'</option>';
				$count++;
			}
		}
		$htmlText .= '</select>';
		return $htmlText;
	}
	
    function _outputview($output = null,$view='')
    {
		$data = array(
			'title' => 'Pengaturan Asset',
			'body' => $output
		  );
		$this->load->helper(array('form','url'));
		if($view == 'view'){
			$this->load->view('AssetManagement/assetManagement_view',$data);
		} else if($view== ''){
			$this->template->load('default','AssetManagement/assetManagementUser_view',$data);
		} else {
			$this->template->load('default','templates/CRUD_view',$data);
		}
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _update_asset($post_array, $primary_key)
	{
		//$post_array['TanggalPembelian'] = '05/03/2014';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		
		$session_data = $this->session->userdata('logged_in');
		$post_array['UpdatedBy'] = $session_data['npk'];
		
		if ($this->db->update('asset',$post_array,array('KodeAsset' => $primary_key)))
		{
		
		fire_print('log','sudah masuk ke sebelum kirim email');
			return $this->sendEmailAssetUpdate('Perubahan Asset IT', $post_array['KodeAsset'], $post_array['KodeTipeAsset'], $post_array['MerekAsset'], $post_array['SpesifikasiAsset'], $post_array["lokasiAsset"], $post_array["TanggalPembelian"], $post_array["HargaAsset"],$post_array["Keterangan"] );
		}
		else
		{
			return false;
		}		
				
		
	}
	
	function _insert_asset($post_array)
	{
		fire_print('log','sudah masuk ke insert asset');
		//vvv.xxx.yyy.zz
		/* vvv = Kepemilikan Asset
          300 = IT
          400 = GA
		xxx = Tipe Asset
		yyy = Nomor Urut Asset
		zz   = DPA
          01   = Satu
          02   = Dua */
		$kodeAsset = "";
		if($post_array['AssetMilik'] == "IT")
		{
			$kodeAsset = "300.";
		}
		else if($post_array['AssetMilik'] == "HRGA")
		{
			$kodeAsset = "400.";
		}
		
		$tipeAsset = $this->tipeAsset->getTipeAsset($post_array['KodeTipeAsset']);
		if($tipeAsset)
		{
			foreach($tipeAsset as $row)
			{
				$kodeAsset .= $row->NomorTipeAsset;
			}
		}
		
		/* $TipeAsset = $this->tipeAsset->getTipeAsset($post_array['TipeAsset']);
		if($tipeAsset)
		{
			foreach($tipeAsset as $row)
			{
				$kodeAsset .= $row->NomorTipeAsset;
			}
		} */
		
		$kodeAsset .= '.'.$this->getLastNumberAsset($post_array['DPA'],$post_array['AssetMilik'],$post_array['KodeTipeAsset']).'.';
		
		if($post_array['DPA'] == 'Satu')
		{
			$kodeAsset .= '01';
		}
		else if($post_array['DPA'] == 'Dua')
		{
			$kodeAsset .= '02';
		}
		
		$post_array['KodeAsset'] = $kodeAsset;
		//$post_array['KodeDepartemen'] = "1";
		
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		
		$session_data = $this->session->userdata('logged_in');
		$post_array['CreatedBy'] = $session_data['npk'];
		
		if($this->db->insert('asset',$post_array))
		{
			fire_print('log','sudah masuk ke sebelum kirim email');
			return $this->sendEmailAsset('Info Data Aset IT Baru', $kodeAsset, $post_array['KodeTipeAsset'], $post_array['MerekAsset'], $post_array['SpesifikasiAsset'],$post_array["lokasiAsset"], $post_array["TanggalPembelian"], $post_array["HargaAsset"]);
		}
		else
		{
			return false;
		}		
	}
	
	function sendEmailAsset($subject, $kodeAsset, $tipeAsset, $merek, $spesifikasi, $lokasiAsset, $tglPembelian, $harga )
	{
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				fire_print('log','sudah masuk ke awal kirim email');
				/* $queryACC = $this->db->get_where('globalparam',array('Name'=>'EmailAsetToACC'));
				foreach($queryACC->result() as $row)
				{					
					$toACC = $row->Value;
				}
				$queryGA = $this->db->get_where('globalparam',array('Name'=>'EmailAsetToGA'));
				foreach($queryGA->result() as $row)
				{
					$toGA = $row->Value;
				} */
							
				
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				//$this->email->to($toACC); 
				//$this->email->to($toGA);
				//$recipientArr = array('melani.driastuti@dpa.co.id', 'theresia.marlina@dpa.co.id','annisa.husnul.khosyiah@dpa.co.id','siti.ulfah@dpa.co.id');
				$recipientArr = array('laily.putri@dpa.co.id');
				$recipientArr1 = array('siti.ulfah@dpa.co.id');
				
				$this->email->to($recipientArr,$recipientArr1);

				$this->email->subject('Data Asset IT Baru');
				
		fire_print('log','sudah masuk lokasi aset');		
		
						
				$message = "Ada Asset IT baru, berikut selengkapnya :
Kode Asset			: $kodeAsset 
Merek Asset			: $merek
Spesifikasi			: $spesifikasi
Lokasi Asset		: $lokasiAsset
Tanggal Pembelian 	: $tglPembelian
Harga Asset			: $harga
				";
							
				$this->email->message($message);	
				
				if( ! $this->email->send())
				{	
					fire_print('log','gagal email');
					return false;
				}
				else
				{
					fire_print('log','sukses email');
					return true;
				}
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function sendEmailAssetUpdate($subject, $KodeAsset, $tipeAsset, $merek, $spesifikasi, $tglPembelian, $harga, $Keterangan )
	{
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				fire_print('log','sudah masuk ke awal kirim email');
				$queryACC = $this->db->get_where('globalparam',array('Name'=>'EmailAsetToACC'));
				foreach($queryACC->result() as $row)
				{					
					$toACC = $row->Value;
				}
				$queryGA = $this->db->get_where('globalparam',array('Name'=>'EmailAsetToGA'));
				foreach($queryGA->result() as $row)
				{
					$toGA = $row->Value;
				}
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				//$this->email->to($toACC); 
				//$this->email->to($toGA);
				$recipientArr = array('melani.driastuti@dpa.co.id', 'theresia.marlina@dpa.co.id','annisa.husnul.khosyiah@dpa.co.id','siti.ulfah@dpa.co.id');
				$this->email->to($recipientArr);

				$this->email->subject('Perubahan Asset IT');

				$lokasiAsset = $this->lokasiAsset->getLokasiAsset($_POST['id']);
		if($lokasiAsset){
			$lokasiAsset_array = array();
			foreach($lokasiAsset as $row){
			   $lokasiAsset_array[] = array(
				 'Deskripsi' => $row->Deskripsi,
				 );
			}
		}
		echo $lokasiAsset_array[0]['Deskripsi'];
				
				$message = "Ada Update Data Asset IT, berikut selengkapnya :
Kode Asset			: $KodeAsset 
Merek Asset			: $merek
Spesifikasi			: $spesifikasi
Lokasi Asset		: $lokasiAsset
Tanggal Pembelian 	: $tglPembelian
Harga Asset			: $harga
Keterangan			: $Keterangan
				";
							
				$this->email->message($message);	
				
				if( ! $this->email->send())
				{	
					fire_print('log','gagal email');
					return false;
				}
				else
				{
					fire_print('log','sukses email');
					return true;
				}
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function getLastNumberAsset($DPA,$AssetMilik,$KodeTipeAsset)
	{
		$LastKodeNumber = $this->asset->getLastNumberAsset($DPA,$AssetMilik,$KodeTipeAsset);
		if($LastKodeNumber){
			foreach($LastKodeNumber as $row){
				$lastKode = $row->KodeAsset;
			}
		}else{
			$lastKode = '00000000000000';
		}
		$lastNumber = substr($lastKode,-6,3);
		$lastNumber++;
		return str_pad($lastNumber,3,"0",STR_PAD_LEFT);
	}
	
	function _delete_asset($primary_key)
	{
		return $this->db->update('asset',array('deleted' => '1'),array('KodeAsset' => $primary_key));
	}
	
	function getDepartemenLokasi()
	{
		$lokasiAsset = $this->lokasiAsset->getLokasiAsset($_POST['id']);
		if($lokasiAsset){
			$lokasiAsset_array = array();
			foreach($lokasiAsset as $row){
			   $lokasiAsset_array[] = array(
				 'KodeDepartemen' => $row->KodeDepartemen,
				 'NamaDepartemen' => $row->NamaDepartemen
			   );
			}
		}
		echo $lokasiAsset_array[0]['KodeDepartemen'];
	}
	
	}

/* End of file main.php */
/* Location: ./application/controllers/main.php */