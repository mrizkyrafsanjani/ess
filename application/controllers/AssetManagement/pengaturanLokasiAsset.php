<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanLokasiAsset extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		if($this->session->userdata('logged_in')){
			if(check_authorized("13"))
			{
				$this->_lokasiAsset();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _lokasiAsset()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Lokasi Asset');
		//$crud->set_theme('datatables');
		
        $crud->set_table('lokasiasset');
		$crud->where('lokasiasset.deleted','0');
		$crud->set_relation('KodeDepartemen','departemen','NamaDepartemen',array('deleted' => '0'));
		
		$crud->columns('KodeLokasiAsset', 'Deskripsi', 'KodeDepartemen', 'CreatedOn', 'CreatedBy', 'UpdatedOn', 'UpdatedBy');
		$crud->fields('Deskripsi', 'KodeDepartemen');
				
		$crud->required_fields('Deskripsi', 'KodeDepartemen');
		
		$crud->callback_delete(array($this,'_delete'));
		$crud->unset_texteditor('Description','Url');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _delete($primary_key){
		return $this->db->update('lokasiasset',array('deleted' => '1'),array('KodeLokasiAsset' => $primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */