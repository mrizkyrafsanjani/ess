<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class BPKB extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('menu','',TRUE);
        $this->load->model('user','',TRUE);
        $this->load->library('grocery_crud');
        $this->load->model('usertask','',TRUE);
        $this->load->model('dtltrkrwy_model','',TRUE);
        $session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
	}
    public function index()
    {
	}
	public function ListPeminjamanBPKB(){
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("BPKB Admin"))
			{
				$this->npkLogin = $session_data['npk'];
				$user = $this->user->dataUser($this->npkLogin);
				$this->_ListPeminjamanBPKB($user);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	public function _ListPeminjamanBPKB($user){

	}
	public function ListAllBPKB()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("BPKB Admin"))
			{
				$this->npkLogin = $session_data['npk'];
				$user = $this->user->dataUser($this->npkLogin);
				$this->_ListAllBPKB($user);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	public function _ListAllBPKB($user)
    {
		$crud = new grocery_crud();
		$crud->set_subject('BPKB');
		$crud->set_table('mstrbpkb');
		$crud->where('mstrbpkb.deleted','0');
		$crud->columns('NPK', 'NoPolisi', 'DPA', 'TanggalBPKB', 'JenisKendaraan', 'NoBPKB', 'NoRangka','NoMesin','NoFaktur', 'Status', 'TanggalSTNK', 'TanggalPeminjaman','TanggalPengambilan','TanggalPengembalian');
        $crud->fields('NPK', 'NoPolisi', 'DPA', 'TanggalBPKB', 'JenisKendaraan', 'NoBPKB', 'NoRangka','NoMesin','NoFaktur', 'Status', 'TanggalSTNK');
		$crud->set_relation('NPK','mstruser','Nama');
		$crud->display_as('NPK','Nama Karyawan');
		$crud->display_as('NoPolisi','Nomor Polisi');
		$crud->display_as('NoBPKB','Nomor BPKB');
		$crud->display_as('Status','Status Peminjaman');
		$crud->display_as('NoMesin','Nomor Mesin');
		$crud->display_as('NoFaktur','Nomor Faktur');
		$crud->display_as('JenisKendaraan','Jenis Kendaraan');
		$crud->display_as('TanggalSTNK','Tanggal STNK');
		$crud->display_as('TanggalBPKB','Tanggal BPKB');
        $crud->field_type('DPA','dropdown',
			array('1' => '1', '2' => '2'));
        $crud->field_type('Status','dropdown',
			array('0' => 'Tidak Dapat Dipinjam', '1' => 'Dapat Dipinjam', '2' => 'Sedang Dalam Proses Peminjaman', '3' =>'Sedang Dipinjam', '4' => 'Dapat Diambil', '5'=>'Sedang Dalam Proses Pengambilan', '6' => 'Sudah Diambil', '7' => 'Tidak dapat Diambil', '8' => 'Sudah Diterima User'));
		//0: Tidak dapat Dipinjam, 1: Dapat Dipinjam, 2: Sedang Dalam Proses Peminjaman, 3: Sedang Dipinjam, 4: Dapat diambil, 5: Sedang Dalam Proses Pengambilan, 6: Sudah diambil
			
		//$crud->callback_column('Status',array($this,'_column_callback_Status'));
		$crud->field_type('CreatedOn','invisible');
		$crud->field_type('CreatedBy','invisible');
		$crud->field_type('UpdatedOn','invisible');
		$crud->field_type('UpdatedBy','invisible');
		$crud->required_fields('NPK', 'NoPolisi', 'NoBPKB', 'Status');
		$crud->callback_update(array($this,'_updateBPKB'));
		$crud->callback_insert(array($this,'_insertBPKB'));
		$crud->callback_delete(array($this,'_deleteBPKB'));
		$output = $crud->render();
		$sqlSelect2 ="SELECT trk.KodeTrk, m.NoBPKB, m.IdBPKB, trk.StatusApproval,
			trk.StatusTerima, m.Status,
			DATE_FORMAT(trk.TanggalTransaksi,'%d/%m/%Y') as TanggalTransaksi,
			DATE_FORMAT(trk.TanggalPengembalian,'%d/%m/%Y') as TanggalPengembalian,
			DATE_FORMAT(trk.TanggalPengambilan,'%d/%m/%Y') as TanggalPengambilan,
			trk.Kepentingan
			from trkbpkb trk
			join mstrbpkb m
			on trk.IdBPKB = m.IdBPKB
			where m.Deleted = 0
			";
		$dataTrk = $this->db->query($sqlSelect2)->result();
		$this-> _outputview($output, 'report', $dataTrk);
		
    }
	function _updateBPKB($post_array, $primary_key)
	{
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('mstrBPKB',$post_array,array('IDBPKB' => $primary_key));
	}
	function _insertBPKB($post_array)
	{
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		$this->db->insert('mstrBPKB',$post_array);	
	}
	function _deleteBPKB($primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['Deleted'] = '1';
		return $this->db->update('mstrBPKB',$post_array,array('IDBPKB' => $primary_key));
	}
	function _column_callback_Status($value = '', $primary_key = null)
	{
		//0: Tidak dapat Dipinjam, 1: Dapat Dipinjam, 2: Sedang Dipinjam, 3: Dapat diambil, 4: Sudah diambil
		//0: Tidak dapat Dipinjam, 1: Dapat Dipinjam, 2: Sedang Dalam Proses Peminjaman, 3: Sedang Dipinjam, 4: Dapat diambil, 5: Sedang Dalam Proses Pengambilan, 6: Sudah diambil

		if($value == '0'){
			return 'Tidak dapat Dipinjam';
		}else if($value == '1'){
			return 'Dapat Dipinjam';
		}else if($value == '2'){
			return 'Sedang Dalam Proses Approval Peminjaman';
		}else if($value == '3'){
			return 'Sedang Dipinjam';
		}else if($value == '4'){
			return 'Dapat diambil';
		}else if($value == '5'){
			return 'Sedang Dalam Proses Approval Pengambilan';
		}else if($value == '6'){
			return 'Sudah diambil';
		}else if($value == '7'){
			return 'Tidak dapat diambil';
		}
	}

	public function ListTrkBPKB($KodeUserTask, $status)
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("BPKB User"))
			{
				$this->_ListTrkBPKB($KodeUserTask, $status);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	public function _ListTrkBPKB($KodeUserTask ='', $status='')
    {
		$crud = new grocery_crud();
		$crud->set_subject('Transaksi BPKB');
		$crud->set_table('trkbpkb');
		$crud->where('trkbpkb.deleted','0');
		if($status== 'approve')
		{
			fire_print('log','enter to status approve');
			$dtltrkrwylist = $this->db->get_where('dtltrkrwy',array('KodeUserTask'=>$KodeUserTask));
			$dtltrkrwy = $dtltrkrwylist->row(1);
			$crud->where('trkbpkb.KodeTrk',$dtltrkrwy->NoTransaksi);
			//$this->kodeCutiGlobal = $dtltrkrwy->NoTransaksi;
			//fire_print('log',"kodeCutiGlobal = $this->kodeCutiGlobal");
			$crud->columns('KodeTrk', 'Kepentingan', 'NPK', 'TanggalTransaksi', 'TanggalPengembalian');
			$crud->fields('KodeTrk', 'Kepentingan', 'NPK', 'TanggalTransaksi');
			$crud->display_as('NPK','Nama Karyawan');
			$crud->display_as('KodeTrk','Nomor Transaksi');
			$crud->display_as('TanggalTransaksi','Tanggal Transaksi');
			$crud->display_as('TanggalPengembalian','Tanggal Pengembalian');
		
			$crud->unset_operations();

		}else if ($status == 'report'){
			$crud->columns('KodeTrk', 'IdBPKB', 'StatusApproval', 'Kepentingan', 'NPK', 'TanggalTransaksi', 'TanggalPengembalian','TanggalPengambilan','StatusTerima');
			$crud->set_relation('IdBPKB','mstrbpkb','NoBPKB');
			$crud->display_as('IdBPKB','No BPKB');
			$crud->display_as('NPK','Nama Karyawan');
			$crud->display_as('KodeTrk','Nomor Transaksi');
			$crud->display_as('StatusApproval','Status Approval');
			$crud->display_as('TanggalTransaksi','Tanggal Transaksi');
			$crud->display_as('TanggalPengembalian','Tanggal Pengembalian');
			$crud->display_as('TanggalPengambilan','Tanggal Pengambilan');
			$crud->field_type('StatusTerima','dropdown',
			array('0' => 'Belum Diterima User ', '1' => 'Sudah Diterima User'));
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->unset_read();

		}
		$crud->set_relation('NPK','mstruser','Nama');
		$output = $crud->render();
        $this-> _outputview($output, $status, null);
	}
	
    function _outputview($output = null,$page='', $dataTrk)
    {
		$session_data = $this->session->userdata('logged_in');
		$data = array(
			'title' => 'BPKB Admin',
			'body' => $output,
			'dataTrk' => $dataTrk
		);
		$this->load->helper(array('form','url'));
		if($page == 'approve'){
			$this->load->view('PeminjamanDokumen/BPKB_view',$data);
		}else if($page == 'report'){
			$this->template->load('default','PeminjamanDokumen/BPKB_view',$data);
		}
	}
	
	public function ReportTrkBPKB()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("BPKB Admin"))
			{
				$this->_ListTrkBPKB('', 'report');
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */