<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class PDControllerNonLogin extends CI_Controller
	{
		var $NPKLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('menu','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('ldarchive_model','',TRUE);
			$this->load->library('grocery_crud');
			$this->load->model('ppb_model','',TRUE);
			$this->load->model('peminjamandokumen_model','',TRUE);
			$this->load->model('bm_activity_model','',TRUE);
			$this->load->model('bm_summarybudget_model','',TRUE);
			$this->load->model('usertask','',TRUE);
			$this->load->model('lembur_model','',TRUE);
			$this->load->library('email');
			$this->load->model('dtltrkrwy_model','',TRUE);
			$this->linkEmail = base_url()."/index.php/PeminjamanDokumen/PDControllerNonLogin/";
		}
		function index()
		{
			try
			{
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
        }
		function cariDetail($NomorVoucher, $NomorPeserta, $DPA)
		{
			$today = date('Y/m/d');
			$tanggalDeadline = date('d/m/Y', strtotime($today. ' + 2 weekdays'));
			$today = date('d/m/Y');
			$url = $this->config->item('urlWebService');
			$client = new SoapClient($url);
			if($DPA == 1){
				$params = array(
					"NoVoucher"=>$NomorVoucher,
					"NomorPeserta"=>$NomorPeserta
				);
				$result = $client->GetDetailVoucher_siDAPEN($params);
				$result = ($result->GetDetailVoucher_siDAPENResult);
				if($result != "0"){
					$TanggalTerbit = explode("|", $result)[6];
					$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
					if(strlen(trim(explode("|", $result)[1])) != 6){
						$NomorPeserta = "-";
					}else{
						$NomorPeserta = trim(explode("|", $result)[1]);
					}
					if($NomorVoucher == ""){
						$NomorVoucher = explode("|",$result)[0];
					}
					$result = array(
						"NomorVoucher" => $NomorVoucher,
						"NomorPeserta" => $NomorPeserta,
						"TanggalTerbit" => $TanggalTerbit,
						"Keterangan" => explode("|",$result)[2],
						"Nominal"=> number_format( explode("|",$result)[4]),
						"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
						"Bank"=>  explode("|",$result)[3],
						"TanggalPinjam"=> $today,
						"TanggalPengembalian" => $tanggalDeadline);
				}else{
					$result = array("msg"=>"Tidak ditemukan");
				}
			}else if($DPA == 2){
				$params = array(
					"NoVoucher"=>$NomorVoucher,
					"NomorPeserta"=>$NomorPeserta
				);
				$result = $client->GetDetailVoucher_CORE($params);
				$result = ($result->GetDetailVoucher_COREResult);
				if($result == "0"){
					$params = array(
						"NoVoucher"=>$NomorVoucher
					);
					$result = $client->GetDetailVoucher_ACCPAC($params);
					$result = ($result->GetDetailVoucher_ACCPACResult);
					if($result == "0"){
						$result = $client->GetDetailVoucher_SIAP($params);
						$result = ($result->GetDetailVoucher_SIAPResult);
						if($result == "0"){
							$result = array("msg"=>"Tidak ditemukan");
						}else{
							$TanggalTerbit = explode("|", $result)[5];
							$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
							if(strlen(trim(explode("|", $result)[1])) != 6){
								$NomorPeserta = "-";
							}else{
								$NomorPeserta = trim(explode("|", $result)[1]);
							}
							if($NomorVoucher == ""){
								$NomorVoucher = explode("|",$result)[0];
							}
							$result = array(
								"NomorVoucher" => $NomorVoucher,
								"NomorPeserta" => $NomorPeserta,
								"TanggalTerbit" => $TanggalTerbit,
								"Keterangan" => explode("|",$result)[2],
								"Nominal"=> number_format( explode("|",$result)[3]),
								"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
								"Bank"=>  explode("|",$result)[0],
								"TanggalPinjam"=> $today,
								"TanggalPengembalian" => $tanggalDeadline);
						}
					}else{
						$TanggalTerbit = explode("|", $result)[5];
						$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
						if(strlen(trim(explode("|", $result)[1])) != 6){
							$NomorPeserta = "-";
						}else{
							$NomorPeserta = trim(explode("|", $result)[1]);
						}
						if($NomorVoucher == ""){
							$NomorVoucher = explode("|",$result)[0];
						}
						$result = array(
							"NomorVoucher" => $NomorVoucher,
							"NomorPeserta" => $NomorPeserta,
							"TanggalTerbit" => $TanggalTerbit,
							"Keterangan" => explode("|",$result)[2],
							"Nominal"=> number_format( explode("|",$result)[3]),
							"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
							"Bank"=>  explode("|",$result)[0],
							"TanggalPinjam"=> $today,
							"TanggalPengembalian" => $tanggalDeadline);
					}
				}else{
					$TanggalTerbit = explode("|", $result)[5];
					$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
					if(strlen(trim(explode("|", $result)[1])) != 6){
						$NomorPeserta = "-";
					}else{
						$NomorPeserta = trim(explode("|", $result)[1]);
					}
					if($NomorVoucher == ""){
						$NomorVoucher = explode("|",$result)[0];
					}
					$result = array(
						"NomorVoucher" => $NomorVoucher,
						"NomorPeserta" => $NomorPeserta,
						"TanggalTerbit" => $TanggalTerbit,
						"Keterangan" => explode("|",$result)[2],
						"Nominal"=> number_format( explode("|",$result)[3]),
						"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
						"Bank"=>  explode("|",$result)[0],
						"TanggalPinjam"=> $today,
						"TanggalPengembalian" => $tanggalDeadline);
				}
			}
			return json_encode($result);
		}
		//user
		public function CancelByEmail($NPK, $SecretKey){
			$DatabaseFPD = $this->peminjamandokumen_model->GetDetailBySecretKey($SecretKey);
			if($DatabaseFPD){
				$QueryUpdate = "UPDATE PD_FPD set deleted = 1,
				UpdatedOn = now(),
				UpdatedBy = $NPK
				where KodeFPD = '$DatabaseFPD->KodeFPD'
				and CreatedBy = '$NPK'
				and Deleted = 0
				and SecretKey = '$SecretKey'
				and IsBorrowed = 0 and IsReceived = 0";
				if($this->db->query($QueryUpdate)){
					if($this->db->affected_rows()==1){
						$DetailFPD = json_decode($this->cariDetail($DatabaseFPD->NomorVoucher, $DatabaseFPD->NomorPeserta, $DatabaseFPD->DPA), true);
						if($this->sendEmailPD('Pembatalan Peminjaman Dokumen',
							$DatabaseFPD->KodeFPD,
							$DatabaseFPD->CreatedBy,
							$DetailFPD['NomorVoucher'],
							$DetailFPD['NomorPeserta'],
							$DetailFPD['TanggalTerbit'],
							$DetailFPD['Nominal'],
							$DetailFPD['Bank'],
							$DatabaseFPD->DPA,
							$DatabaseFPD->SecretKey,
							$Purpose="AfterCanceled"))
						{
							echo ('Sukses Pembatalan Peminjaman Dokumen');
						}else{
							echo ('Gagal Mengirim Email Pembatalan Penerimaan Dokumen');
						}
					}else{
						echo ('Pembatalan sudah pernah dilakukan sebelumnya atau dokumen tidak dapat dibatalkan!');
					}
				}else{
					echo ('Gagal Pembatalan Peminjaman Dokumen');
				}
			}else{
				echo ('Gagal Pembatalan Peminjaman Dokumen');
			}
		}
		public function ReceiveByEmail($NPK, $SecretKey){
			$DatabaseFPD = $this->peminjamandokumen_model->GetDetailBySecretKey($SecretKey);
			if($DatabaseFPD){
				$QueryUpdate = "UPDATE PD_FPD set IsReceived = 1,
				IsBorrowed = 1,
				UpdatedOn = now(),
				UpdatedBy = '$NPK'
				where CreatedBy = '$NPK'
				and KodeFPD = '$DatabaseFPD->KodeFPD'
				and SecretKey = '$SecretKey' 
				and Deleted = 0 and IsReceived = 0";
				if($this->db->query($QueryUpdate)){
					if($this->db->affected_rows()==1){
						$DetailFPD = json_decode($this->cariDetail($DatabaseFPD->NomorVoucher, $DatabaseFPD->NomorPeserta, $DatabaseFPD->DPA), true);
						if($this->sendEmailPD('Penerimaan Peminjaman Dokumen',
							$DatabaseFPD->KodeFPD,
							$DatabaseFPD->CreatedBy,
							$DetailFPD['NomorVoucher'],
							$DetailFPD['NomorPeserta'],
							$DetailFPD['TanggalTerbit'],
							$DetailFPD['Nominal'],
							$DetailFPD['Bank'],
							$DatabaseFPD->DPA,
							$DatabaseFPD->SecretKey,
							$Purpose="AfterReceived"))
						{
							echo ('Sukses Konfirmasi Penerimaan Dokumen');
						}else{
							echo ('Gagal Mengirim Email Konfirmasi Penerimaan Dokumen');
						}
					}else{
						echo ('Penerimaan sudah pernah dilakukan sebelumnya atau konfirmasi penerimaan dokumen sudah tidak berlaku!');
					}
				}else{
					echo ('Gagal Konfirmasi Penerimaan Dokumen');
				}
			}else{
				echo ('Gagal Konfirmasi Penerimaan Dokumen');
			}
		}
		//accounting
		public function RejectByEmail($NPK, $SecretKey){
			$DatabaseFPD = $this->peminjamandokumen_model->GetDetailBySecretKey($SecretKey);
			if($DatabaseFPD){
				$QueryUpdate = "UPDATE PD_FPD set Deleted = 1,
					UpdatedOn = now(),
					UpdatedBy = '$NPK'
					where SecretKey = '$SecretKey' and Deleted = 0 and IsReceived = 0";
				if($this->db->query($QueryUpdate)){
					if($this->db->affected_rows()==1){
						$DetailFPD = json_decode($this->cariDetail($DatabaseFPD->NomorVoucher, $DatabaseFPD->NomorPeserta, $DatabaseFPD->DPA), true);
						if($this->sendEmailPD('Penolakan Peminjaman Dokumen',
							$DatabaseFPD->KodeFPD,
							$DatabaseFPD->CreatedBy,
							$DetailFPD['NomorVoucher'],
							$DetailFPD['NomorPeserta'],
							$DetailFPD['TanggalTerbit'],
							$DetailFPD['Nominal'],
							$DetailFPD['Bank'],
							$DatabaseFPD->DPA,
							$DatabaseFPD->SecretKey,
							$Purpose="AfterRejected"))
						{
							echo ('Sukses Menolak Peminjaman Dokumen');
						}else{
							echo ('Gagal Mengirim Email Peminjaman Dokumen');
						}
					}else{
						echo ('Penolakan sudah pernah dilakukan sebelumnya atau Penolakan dokumen sudah tidak berlaku!');
					}
				}else{
					echo ('Gagal Menolak Peminjaman Dokumen');
				}
			}else{
				echo ('Gagal Menolak Peminjaman Dokumen');
			}
		}
		public function ReturnByEmail($NPK, $SecretKey){
			$DatabaseFPD = $this->peminjamandokumen_model->GetDetailBySecretKey($SecretKey);
			if($DatabaseFPD){
				$QueryUpdate = "UPDATE PD_FPD set IsBorrowed = 0,
				IsBack = 1,
				UpdatedOn = now(),
				UpdatedBy = '$NPK'
					where SecretKey = '$SecretKey' and Deleted = 0 and IsReceived = 1 and IsBorrowed = 1";
				if($this->db->query($QueryUpdate)){
					if($this->db->affected_rows() == 1){
						$DetailFPD = json_decode($this->cariDetail($DatabaseFPD->NomorVoucher, $DatabaseFPD->NomorPeserta, $DatabaseFPD->DPA), true);
						if($this->sendEmailPD('Pengembalian Peminjaman Dokumen',
							$DatabaseFPD->KodeFPD,
							$DatabaseFPD->CreatedBy,
							$DetailFPD['NomorVoucher'],
							$DetailFPD['NomorPeserta'],
							$DetailFPD['TanggalTerbit'],
							$DetailFPD['Nominal'],
							$DetailFPD['Bank'],
							$DatabaseFPD->DPA,
							$DatabaseFPD->SecretKey,
							$Purpose="AfterReturned"))
						{
							echo ('Sukses Konfirmasi Pengembalian Dokumen');
						}else{
							echo ('Gagal Mengirim Email Konfirmasi Pengembalian Dokumen');
						}
					}else{
						echo ('Pengembalian sudah pernah dilakukan sebelumnya atau konfirmasi Pengembalian dokumen sudah tidak berlaku!');
					}
				}else{
					echo ('Gagal Konfirmasi Pengembalian Dokumen');
				}
			}else{
				echo ('Gagal Konfirmasi Pengembalian Dokumen');
			}
		}
		function sendEmailPD_bak($subject, $KodeFPD, $NPK, $NomorVoucher, $NomorPeserta, $TanggalTerbit, $Nominal, $Bank, $DPA, $SecretKey, $Purpose="Peminjaman", $Deadline="")
		{
			try
			{
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				if($this->config->item('enableEmail') == 'true'){
					if($Purpose == "Peminjaman"){
						$user = $this->user->dataUser($NPK);
						fire_print('log','sudah masuk ke awal kirim email');
						$recipientArr = array('annisa.husnul.khosyiah@dpa.co.id');
						$messageAccounting = 'Dear Accounting,
							<br/><br/>'.$user[0]->nama.' meminjam dokumen sebagai berikut:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>Jika peminjaman ini tidak dapat diterima, silahkan ditolak melalui <a href="'.$this->linkEmail.'RejectByEmail/Accounting/'.$SecretKey.'">link</a> berikut ini 
							<br/>Jika peminjaman dapat dilakukan, harap segera memberikan dokumen terkait ke user.
							<br/>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->to($recipientArr);
						$this->email->subject($subject);
						$this->email->message($messageAccounting);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							$user = $this->user->dataUser($NPK);
							$recipientArr = array($user[0]->email);
							$this->email->to($recipientArr);
							$this->email->subject('Konfirmasi Penerimaan Dokumen');
							fire_print('log','sudah masuk lokasi aset');
							$message = 'Dear User,
								<br/><br/>Peminjaman dokumen sedang diproses:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>Jika dokumen yang dipinjam sudah diterima, silahkan konfirmasi melalui link <a href="'.$this->linkEmail.'ReceiveByEmail/'.$NPK.'/'.$SecretKey.'">link</a> berikut ini 
								<br/>Jika peminjaman ini ingin dibatalkan silahkan konfirmasi batal melalui link <a href="'.$this->linkEmail.'CancelByEmail/'.$NPK.'/'.$SecretKey.'">link</a> berikut ini 
								<br/>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->message($message);
							if( ! $this->email->send())
							{
								fire_print('log','gagal email');
								return false;
							}
							else
							{
								fire_print('log','sukses email');
								echo ('Sukses Pinjam Dokumen');
								
								return true;
							}
						}
					}
					else if($Purpose == "AfterReceived"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$recipientArr = array('annisa.husnul.khosyiah@dpa.co.id');
						$this->email->to($recipientArr);
						$this->email->subject('Notifikasi Penerimaan Peminjaman Dokumen');
						$messageAccounting = 'Dear Accounting,
							<br/><br/>'.$user[0]->nama.' sudah mengkonfirmasi penerimaan peminjaman dokumen berikut:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							</ul>
							<br/>Terima kasih atas peminjamannya, Selamat bekerja kembali
							<br/>ESS
						';
						$this->email->message($messageAccounting);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							$user = $this->user->dataUser($NPK);
							$recipientArr = array($user[0]->email);
							$this->email->to($recipientArr);
							$this->email->subject('Notifikasi Status Peminjaman Dokumen');
							fire_print('log','sudah masuk lokasi aset');
							$message = 'Dear User,
								<br/><br/>Dokumen berikut sudah tercatat dengan status <b>sedang dipinjam</b>:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								</ul>
								<br/>Terima kasih atas konfirmasinya, Selamat bekerja kembali
								<br/>ESS
							';
							$this->email->message($message);
							if( ! $this->email->send())
							{
								fire_print('log','gagal email');
								return false;
							}
							else
							{
								fire_print('log','sukses email');
								return true;
							}
						}
					}
					else if($Purpose == "AfterCanceled"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$recipientArr = array('annisa.husnul.khosyiah@dpa.co.id');
						$this->email->to($recipientArr);
						$this->email->subject('Notifikasi Pembatalan Peminjaman Dokumen');
						$messageAccounting = 'Dear Accounting,
							<br/><br/>'.$user[0]->nama.' membatalkan peminjaman dokumen berikut:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($messageAccounting);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							$user = $this->user->dataUser($NPK);
							$recipientArr = array($user[0]->email);
							$this->email->to($recipientArr);
							$this->email->subject('Notifikasi Pembatalan Peminjaman Dokumen');
							fire_print('log','sudah masuk lokasi aset');
							$message = 'Dear User,
								<br/><br/>Peminjaman dokumen berikut sudah berhasil dibatalkan:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->message($message);
							if( ! $this->email->send())
							{
								fire_print('log','gagal email');
								return false;
							}
							else
							{
								fire_print('log','sukses email');
								return true;
							}
						}
					}
					else if($Purpose == "AfterRejected"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$recipientArr = array('annisa.husnul.khosyiah@dpa.co.id');
						$this->email->to($recipientArr);
						$this->email->subject('Notifikasi Penolakan Peminjaman Dokumen');
						$messageAccounting = 'Dear Accounting,
							<br/><br/>Dokumen berikut sudah tercatat dengan status <b>ditolak</b>:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($messageAccounting);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							$user = $this->user->dataUser($NPK);
							$recipientArr = array($user[0]->email);
							$this->email->to($recipientArr);
							$this->email->subject('Notifikasi Penolakan Peminjaman Dokumen');
							fire_print('log','sudah masuk lokasi aset');
							$message = 'Dear User,
								<br/><br/>Peminjaman dokumen berikut telah ditolak oleh Accounting:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->message($message);
							if( ! $this->email->send())
							{
								fire_print('log','gagal email');
								return false;
							}
							else
							{
								fire_print('log','sukses email');
								return true;
							}
						}
					}
					else if($Purpose == "AfterReturned"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$recipientArr = array('annisa.husnul.khosyiah@dpa.co.id');
						$this->email->to($recipientArr);
						$this->email->subject('Notifikasi Pengembalian Peminjaman Dokumen');
						$messageAccounting = 'Dear Accounting,
							<br/><br/>Dokumen berikut sudah tercatat dengan status <b>sudah dikembalikan</b>:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							</ul>
							<br/>Terima kasih atas konfirmasinya, Selamat Bekerja Kembali
							<br/>ESS
						';
						$this->email->message($messageAccounting);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							$user = $this->user->dataUser($NPK);
							$recipientArr = array($user[0]->email);
							$this->email->to($recipientArr);
							$this->email->subject('Notifikasi Pengembalian Peminjaman Dokumen');
							fire_print('log','sudah masuk lokasi aset');
							$message = 'Dear User,
								<br/><br/>Accounting sudah mengkonfirmasi pengembalian dokumen berikut:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								</ul>
								<br/>Terima kasih atas pengembaliannya, Selamat Bekerja Kembali
								<br/>ESS
							';
							$this->email->message($message);
							if( ! $this->email->send())
							{
								fire_print('log','gagal email');
								return false;
							}
							else
							{
								fire_print('log','sukses email');
								return true;
							}
						}
					}
					else if($Purpose == "Notifikasi"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$recipientArr = array('annisa.husnul.khosyiah@dpa.co.id');
						$this->email->to($recipientArr);
						$this->email->subject('Notifikasi Deadline Peminjaman Dokumen '.$NomorVoucher);
						$messageAccounting = 'Dear Accounting,
							<br/><br/>Dokumen berikut Sudah Mendekati Deadline Peminjaman '.$Deadline.' </b>:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>Jika dokumen sudah dikembalikan, silahkan konfirmasi melalui <a href="'.$this->linkEmail.'ReturnByEmail/Accounting/'.$SecretKey.'">link</a> berikut ini 
							<br/>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($messageAccounting);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							$user = $this->user->dataUser($NPK);
							$recipientArr = array($user[0]->email);
							$this->email->to($recipientArr);
							$this->email->subject('Notifikasi Deadline Peminjaman Dokumen '.$NomorVoucher);
							fire_print('log','sudah masuk lokasi aset');
							$message = 'Dear User,
								<br/><br/>Dokumen berikut Sudah Mendekati Deadline Peminjaman '.$Deadline.'
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->message($message);
							if( ! $this->email->send())
							{
								fire_print('log','gagal email');
								return false;
							}
							else
							{
								fire_print('log','sukses email');
								return true;
							}
						}
					}
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function sendEmailPD($subject, $KodeFPD, $NPK, $NomorVoucher, $NomorPeserta, $TanggalTerbit, $Nominal, $Bank, $DPA, $SecretKey, $Purpose="Peminjaman", $Deadline="")
		{
			try
			{
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				if($this->config->item('enableEmail') == 'true'){
					if($Purpose == "Peminjaman"){
						$user = $this->user->dataUser($NPK);
						fire_print('log','sudah masuk ke awal kirim email');
						//4 = Accounting tax & control
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>'.$user[0]->nama.' meminjam dokumen sebagai berikut:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>Jika peminjaman ini tidak dapat diterima, silahkan ditolak melalui <a href="'.$this->linkEmail.'RejectByEmail/'.$accountingUser->NPK.'/'.$SecretKey.'">link</a> berikut ini 
								<br/>Jika peminjaman dapat dilakukan, harap segera memberikan dokumen terkait ke user.
								<br/>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->to($recipient);
							$this->email->subject($subject);
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Konfirmasi Penerimaan Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Peminjaman dokumen sedang diproses:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>Jika dokumen yang dipinjam sudah diterima, silahkan konfirmasi melalui link <a href="'.$this->linkEmail.'ReceiveByEmail/'.$NPK.'/'.$SecretKey.'">link</a> berikut ini 
							<br/>Jika peminjaman ini ingin dibatalkan silahkan konfirmasi batal melalui link <a href="'.$this->linkEmail.'CancelByEmail/'.$NPK.'/'.$SecretKey.'">link</a> berikut ini 
							<br/>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							$this->session->set_flashdata('msg','Sukses Pinjam Dokumen');
							redirect("PeminjamanDokumen/PDController/ListFPD_User",'refresh');
							return true;
						}
					}
					else if($Purpose == "AfterReceived"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>'.$user[0]->nama.' sudah mengkonfirmasi penerimaan peminjaman dokumen berikut:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								</ul>
								<br/>Terima kasih atas peminjamannya, Selamat bekerja kembali
								<br/>ESS
							';
							$this->email->to($recipient);
							$this->email->subject('Notifikasi Penerimaan Peminjaman Dokumen');
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Notifikasi Status Peminjaman Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Dokumen berikut sudah tercatat dengan status <b>sedang dipinjam</b>:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							</ul>
							<br/>Terima kasih atas konfirmasinya, Selamat bekerja kembali
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							return true;
						}
					}
					else if($Purpose == "AfterCanceled"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$this->email->to($recipient);
							$this->email->subject('Notifikasi Pembatalan Peminjaman Dokumen');
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>'.$user[0]->nama.' membatalkan peminjaman dokumen berikut:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Notifikasi Pembatalan Peminjaman Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Peminjaman dokumen berikut sudah berhasil dibatalkan:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							return true;
						}
					}
					else if($Purpose == "AfterRejected"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$this->email->to($recipient);
							$this->email->subject('Notifikasi Penolakan Peminjaman Dokumen');
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>Dokumen berikut sudah tercatat dengan status <b>ditolak</b>:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Notifikasi Penolakan Peminjaman Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Peminjaman dokumen berikut telah ditolak oleh Accounting:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							return true;
						}
					}
					else if($Purpose == "AfterReturned"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$this->email->to($recipient);
							$this->email->subject('Notifikasi Pengembalian Peminjaman Dokumen');
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>Dokumen berikut sudah tercatat dengan status <b>sudah dikembalikan</b>:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								</ul>
								<br/>Terima kasih atas konfirmasinya, Selamat Bekerja Kembali
								<br/>ESS
							';
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Notifikasi Pengembalian Peminjaman Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Accounting sudah mengkonfirmasi pengembalian dokumen berikut:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							</ul>
							<br/>Terima kasih atas pengembaliannya, Selamat Bekerja Kembali
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							return true;
						}
					}
					else if($Purpose == "Notifikasi"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$this->email->to($recipient);
							$this->email->subject('Notifikasi Pengingat Deadline Peminjaman Dokumen');
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>Dokumen berikut Sudah Mendekati Deadline Peminjaman '.$Deadline.' </b>:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>Jika dokumen sudah dikembalikan, silahkan konfirmasi melalui <a href="'.$this->linkEmail.'ReturnByEmail/'.$accountingUser->NPK.'/'.$SecretKey.'">link</a> berikut ini 
								<br/>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Notifikasi Deadline Peminjaman Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Dokumen berikut Sudah Mendekati Deadline Peminjaman
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							return true;
						}
					}
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		public function reminderDeadlinePeminjamanDokumen()
		{
			$searchAllDokumen = "SELECT *, DATE_FORMAT(TanggalDeadline, '%d %M %Y') as Deadline
			from pd_fpd where deleted = 0 and IsBorrowed = 1 and IsReceived = 1 and IsBack = 0";
			$allDokumen = $this->db->query($searchAllDokumen)->result();
			if($allDokumen){
				foreach($allDokumen as $dokumen){
					$flag1Hari = false;
					$flag2Hari = false;
					$flag3Hari = false;
					$flagBolehEmail = false;
					$today = date("Y-m-d");
					$startNotifikasi = $dokumen->TanggalPeminjaman;
					$startNotifikasi = strtotime($startNotifikasi.'+1 weekdays');
					$firstDayNotifikasi = date("Y-m-d", $startNotifikasi);
					if($today >= $firstDayNotifikasi){
						$flagBolehEmail = true;
					}
					if($flagBolehEmail){
						$DatabaseFPD = $this->peminjamandokumen_model->GetDetailByKodeFPD($dokumen->KodeFPD);
						$DetailFPD = json_decode($this->cariDetail($DatabaseFPD->NomorVoucher, $DatabaseFPD->NomorPeserta, $DatabaseFPD->DPA), true);
						if($this->sendEmailPD('Notifikasi Deadline Dokumen',
							$dokumen->KodeFPD,
							$DatabaseFPD->CreatedBy,
							$DetailFPD['NomorVoucher'],
							$DetailFPD['NomorPeserta'],
							$DetailFPD['TanggalTerbit'],
							$DetailFPD['Nominal'],
							$DetailFPD['Bank'],
							$DatabaseFPD->DPA,
							$DatabaseFPD->SecretKey,
							$Purpose="Notifikasi",
						$dokumen->Deadline)){
							echo 'Sukses Email';
						}else{
							echo 'Gagal Email';
						}
					}else{
						echo 'Tidak Perlu di email';
					}
				}
			}else{
				echo 'Tidak ada Peminjaman sama sekali';
			}
		}
		public function reminderDeadlinePeminjamanBPKB()
		{
			$searchAllBPKB = "SELECT m.NoBPKB, m.NPK, mu.Nama, DATE_FORMAT(TanggalPengembalian, '%d %M %Y') as TanggalPengembalian
			from mstrbpkb m join mstruser mu on m.NPK = mu.NPK where m.deleted = 0 and m.Status = 3";
			$allBPKB = $this->db->query($searchAllBPKB)->result();
			if($allBPKB){
				foreach($allBPKB as $bpkb){
					$flag1Hari = false;
					$flag2Hari = false;
					$flag3Hari = false;
					$flagBolehEmail = false;
					$today = date("Y-m-d");
					$startNotifikasi = $bpkb->TanggalPengembalian;
					$startNotifikasi = strtotime($startNotifikasi.'-3 weekdays');
					$firstDayNotifikasi = date("Y-m-d", $startNotifikasi);
					if($today >= $firstDayNotifikasi){
						$flagBolehEmail = true;
					}
					if($flagBolehEmail){
						$subject = "Deadline Pengembalian BPKB ".$bpkb->Nama;
						$npk = $bpkb->NPK;
						$nobpkb = $bpkb->NoBPKB;
						$tanggaldeadline = $bpkb->TanggalPengembalian;
						if($this->emailReminderBPKB($subject, $npk, $nobpkb, $tanggaldeadline)){
							echo 'Sukses Email';
						}else{
							echo 'Gagal Email';
						}
					}else{
						echo 'Tidak Perlu di email';
					}
				}
			}else{
				echo 'Tidak ada Peminjaman sama sekali';
			}
		}

		public function emailReminderBPKB($subject, $npk, $nobpkb, $tanggaldeadline){
			$this->load->library('email');
			$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
			if($this->config->item('enableEmail') == 'true'){
				$npkAdminHRGA = $this->db->get_where('globalparam',array('Name'=>'AdminHRGA'));
				$query = $this->db->get_where('globalparam',array('Name'=>'AdminHRGA'));
				foreach($query->result() as $row)
				{
					$npkAdminHRGA = $row->Value;
				}
				$hr = $this->user->dataUser($npkAdminHRGA);
				$user = $this->user->dataUser($npk);
				$recipient = $hr[0]->email;
				$this->email->to($recipient);
				$this->email->subject($subject);
				$message = 'Dear '.$hr[0]->nama.',
					<br/><br/>Batas pengembalian atas BPKB No: '.$nobpkb.' adalah '.$tanggaldeadline.' </b>
					Segera ingatkan '.$user[0]->nama.' untuk melakukan pengembalian BPKB jika BPKB telah selesai dipinjam
					<br/>
					<br/>Terima kasih
					<br/>ESS
				';
				$this->email->message($message);
				$this->email->send();

				$recipient = array($user[0]->email);
				$this->email->to($recipient);
				$this->email->subject($subject);
				$message = 'Dear '.$user[0]->nama.',
					<br/><br/>Batas pengembalian atas BPKB No: '.$nobpkb.' adalah '.$tanggaldeadline.' </b>:
					Segera lakukan pengembalian BPKB jika BPKB telah selesai dipinjam
					<br/>
					<br/>Terima kasih
					<br/>ESS
				';
				$this->email->message($message);
				$this->email->send();
				return true;
			}
			
		}
	

		public function reminderDeadlineSTNK()
		{
			$searchAllBPKB = "SELECT m.NoBPKB, m.NPK, mu.Nama, DATE_FORMAT(TanggalSTNK, '%d %M %Y') as TanggalSTNKemail, TanggalSTNK
			from mstrbpkb m join mstruser mu on m.NPK = mu.NPK where m.deleted = 0 and m.Status = 3";
			$allBPKB = $this->db->query($searchAllBPKB)->result();
			if($allBPKB){
				foreach($allBPKB as $bpkb){
					//> 3 bulan, do nothing
					//> 1 <= 3 bulan email 1x/30 hari
					//< 1 bulan email 1x/7 hari
					//flag email
					$flag1Bulan = false;
					$flag3Bulan = false;
					$flagBolehEmail = false;
					$today = date("Y-m-d");
					$TanggalKadaluarsa = $bpkb->TanggalSTNK;
					$oneMonthReminder = strtotime($TanggalKadaluarsa.'-1 months');
					$oneMonthReminder = date("Y-m-d", $oneMonthReminder);
					$threeMonthReminder = strtotime($TanggalKadaluarsa.'-3 months');
					$threeMonthReminder = date("Y-m-d", $threeMonthReminder);
					if($TanggalKadaluarsa <= $today){
						//expired nih
						echo 'expired nih';
					}else if($threeMonthReminder <= $today){
						if($oneMonthReminder <= $today){
							echo 'masa 1 bulan sebelum';
							$flag1Bulan = true;
						}else{
							echo 'masa 3 bulan sebelum';
							$flag3Bulan = true;
						}
					}else{
						echo 'belum masuk masa reminder';
					}
					if($today == $threeMonthReminder || $today == $oneMonthReminder){
						$flagBolehEmail = true;
					}else if($flag3Bulan){
						$Hmin2Bulan = strtotime($threeMonthReminder.'+1 months');
						$Hmin2Bulan = date("Y-m-d", $Hmin2Bulan);
						echo $Hmin2Bulan;
						if($today == $Hmin2Bulan){
							$flagBolehEmail = true;
						}
					}else if($flag1Bulan){
						$hmin1minggu = strtotime($TanggalKadaluarsa.'-1 weeks');
						$hmin1minggu = date("Y-m-d", $hmin1minggu);
						if($today == $hmin1minggu){
							$flagBolehEmail = true;
						}
						$hmin2minggu = strtotime($TanggalKadaluarsa.'-2 weeks');
						$hmin2minggu = date("Y-m-d", $hmin2minggu);
						if($today == $hmin2minggu){
							$flagBolehEmail = true;
						}
						$hmin3minggu = strtotime($TanggalKadaluarsa.'-3 weeks');
						$hmin3minggu = date("Y-m-d", $hmin3minggu);
						if($today == $hmin3minggu){
							$flagBolehEmail = true;
						}
					}

					if($flagBolehEmail){
						$subject = "Deadline Perpanjangan STNK ".$bpkb->Nama;
						$npk = $bpkb->NPK;
						$nobpkb = $bpkb->NoBPKB;
						$tanggalstnk = $bpkb->TanggalSTNKemail;
						if($this->emailReminderSTNK($subject, $npk, $nobpkb, $tanggalstnk)){
							echo 'Sukses Email';
						}else{
							echo 'Gagal Email';
						}
					}else{
						echo 'Tidak Perlu di email';
					}
				}
				
				$message= 'email stnk';
			}else{
				$message= 'tidak email stnk';
			}
			
		}

		public function emailReminderSTNK($subject, $npk, $nobpkb, $tanggalstnk){
			$this->load->library('email');
			$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
			if($this->config->item('enableEmail') == 'true'){
				
				$user = $this->user->dataUser($npk);
				$recipient = array($user[0]->email);
				$this->email->to($recipient);
				$this->email->subject($subject);
				$message = 'Dear '.$user[0]->nama.',
					<br/><br/>Batas perpanjangan STNK dari BPKB No: '.$nobpkb.' adalah '.$tanggalstnk.' </b>
					Segera lakukan peminjaman BPKB jika diperlukan
					<br/>
					<br/>Terima kasih
					<br/>ESS
				';
				$this->email->message($message);
				$this->email->send();
				return true;
			}
			
		}
	}
?>