<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class BPKBController extends CI_Controller
	{
        var $NPKLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('menu','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->library('grocery_crud');
			$this->load->model('usertask','',TRUE);
            $this->load->model('dtltrkrwy_model','',TRUE);
            $this->load->model('peminjamandokumen_model','',TRUE);
            $this->load->library('email');
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
			$this->linkEmail = base_url()."/index.php/PeminjamanDokumen/PDControllerNonLogin/";
		}
		function index()
		{
			try
			{
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
        }
        public function ListBPKB_User(){
            $session_data = $this->session->userdata('logged_in');
			if($session_data){
				if(check_authorizedByName("BPKB User"))
				{
					$this->npkLogin = $session_data['npk'];
					$user = $this->user->dataUser($this->npkLogin);
					$this->_ListBPKBUser($user);
				}
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		public function _ListBPKBUser($user)
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("BPKB User"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						//0: Tidak dapat Dipinjam, 1: Dapat Dipinjam, 2: Sedang Dipinjam, 3: Dapat diambil, 4: Sudah diambil
						$sqlSelect =
						"SELECT m.IdBPKB, m.NoPolisi, m.NoBPKB, m.JenisKendaraan,
						DATE_FORMAT(m.TanggalPeminjaman,'%d/%m/%Y') as TanggalPeminjaman,
						DATE_FORMAT(m.TanggalPengembalian,'%d/%m/%Y') as TanggalPengembalian,
						DATE_FORMAT(m.TanggalPengambilan,'%d/%m/%Y') as TanggalPengambilan,
						m.Status
						from mstrbpkb m
						where
						m.NPK = '$this->npkLogin'
						and m.Deleted = 0
						";
						$sqlSelect2 =
						"SELECT trk.KodeTrk, m.NoBPKB, m.IdBPKB, trk.StatusApproval,
						trk.StatusTerima,
						DATE_FORMAT(trk.TanggalTransaksi,'%d/%m/%Y') as TanggalTransaksi,
						DATE_FORMAT(trk.TanggalPengembalian,'%d/%m/%Y') as TanggalPengembalian,
						DATE_FORMAT(trk.TanggalPengambilan,'%d/%m/%Y') as TanggalPengambilan,
						trk.Kepentingan
						from trkbpkb trk
						join mstrbpkb m
						on trk.IdBPKB = m.IdBPKB
						where
						trk.NPK = '$this->npkLogin'
						and m.Deleted = 0
						";
						$data = $this->db->query($sqlSelect)->result();
						$dataTrk = $this->db->query($sqlSelect2)->result();
						$isHead = false;
						if(strpos($user[0]->jabatan, "Head") !== false){
							$isHead = true;
						}
						$dataBPKB = array(
							'title'=> 'Data BPKB',
							'data' => $data,
							'dataTrk' => $dataTrk,
							'isHead' => $isHead
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','PeminjamanDokumen/BPKBUser_view',$dataBPKB);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ajax_pinjamAmbilBPKB(){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("BPKB User"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						//CreateUserTask to Atasan

						//insert into trkbpkb
						$IdBPKB = $this->input->post('IdBPKB');
						$Kepentingan = $this->input->post('Kepentingan');
						$isHead = false;
						if($Kepentingan == 'PeminjamanHead' || $Kepentingan == 'PengambilanHead'){
							$isHead = true;
							$StatusApproval =  'AP2';
						}else{
							$StatusApproval =  'PE1';

						}
						$bpkb = $this->peminjamandokumen_model->GetBPKB($IdBPKB);
						$Deleted = 0;
						$IdBPKB =  $IdBPKB;
						$TanggalTransaksi = date("Y-m-d H:i:s");
						$JenisTransaksi = "";
						$Kepentingan =  $Kepentingan;
						$Status = 0;
						$TanggalPengambilan = null;
						$CreatedOn = date("Y-m-d H:i:s");
						$CreatedBy = $this->npkLogin;
						$this->db->trans_begin();

						if($Kepentingan == 'Peminjaman'){
							$TanggalPengembalian = date('Y-m-d H:i:s', strtotime($TanggalTransaksi . ' +5 weekdays'));
							$JenisTransaksi = 'BPR1';
							$KodeUserTask = generateNo('UT');

							$Status = 2;
						}else if ($Kepentingan == 'PeminjamanHead'){
							$TanggalPengembalian = date('Y-m-d H:i:s', strtotime($TanggalTransaksi . ' +5 weekdays'));
							$JenisTransaksi = 'BPR3';
							$Status = 2;
						}
						else if ($Kepentingan == 'PengambilanHead'){
							$TanggalPengembalian = date('Y-m-d H:i:s', strtotime($TanggalTransaksi . ' +5 weekdays'));
							$JenisTransaksi = 'BPT3';
							$Status = 6;
						}
						else{
							$TanggalPengembalian = null;
							$JenisTransaksi = 'BPT1';
							$KodeUserTask = generateNo('UT');
							$Status = 5;
						}
						
						$dataTrkBPKB = array(
							"KodeTrk" => generateNo('BP'),
							"Deleted" => 0,
							"IdBPKB" => $IdBPKB,
							"TanggalTransaksi" => $TanggalTransaksi,
							"NPK" => $this->npkLogin,
							"StatusApproval" => $StatusApproval,
							"Kepentingan" => $Kepentingan,
							"TanggalPengembalian" => $TanggalPengembalian,
							"CreatedBy" => $this->npkLogin,
							"CreatedOn" => date('Y-m-d H:i:s'),
						);
						//0: Tidak dapat Dipinjam, 1: Dapat Dipinjam, 2: Sedang Dalam Proses Peminjaman, 3: Sedang Dipinjam, 4: Dapat diambil, 5: Sedang Dalam Proses Pengambilan, 6: Sudah diambil
						//0: Tidak dapat Dipinjam, 1: Dapat Dipinjam, 2: Sedang Dipinjam, 3: Dapat diambil, 4: Sudah diambil
						$KodeTrk = $dataTrkBPKB['KodeTrk'];
						$this->db->insert('trkbpkb',$dataTrkBPKB);
						$this->db->update('mstrbpkb',array('Status'=>$Status),array('IdBPKB' => $IdBPKB));

						if ($isHead){
								//$this->db->trans_rollback();
								$this->db->trans_commit();
								$success = 'Sukses menambahkan ke dalam sistem harap cetak dokumen ini';
						}else{
							$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeTrk,$KodeUserTask,$JenisTransaksi);
							if(!$hasilTambahUserTask)
							{
								fire_print('log','insert cuti rollback');
								$this->db->trans_rollback();
								$success = 'Gagal menambahkan ke dalam sistem';
							}else{
								fire_print('log','insert cuti commit');
								$this->db->trans_commit();
								$success = 'Sukses menambahkan ke dalam sistem';
							}
						}
						echo json_encode(array("message"=>$success));
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ajax_terimaBPKB(){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("BPKB User"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						$IdBPKB = $this->input->post('IdBPKB');
						$Kepentingan = $this->input->post('Kepentingan');
						$KodeTrk = $this->input->post('KodeTrk');
						$bpkb = $this->peminjamandokumen_model->GetBPKB($IdBPKB);
						$Deleted = 0;
						$IdBPKB =  $IdBPKB;
						$status = 0;
						$this->db->trans_begin();
						if($Kepentingan == 'Peminjaman' || $Kepentingan == 'PeminjamanHead'){
							$status = 3;
							$Kepentingan = 'R';
						}else{
							$status = 6;
							$Kepentingan = 'T';
						}
						$TanggalTransaksi = date("Y-m-d H:i:s");
						if($Kepentingan == 'R'){
							$TanggalPengembalian = date('Y-m-d H:i:s', strtotime($TanggalTransaksi . ' +5 weekdays'));
							$this->db->update('mstrbpkb',array('TanggalPeminjaman' => $TanggalTransaksi, 'TanggalPengembalian' => $TanggalPengembalian),array('IdBPKB' => $IdBPKB));
							$this->db->update('trkbpkb',array('TanggalPengembalian' => $TanggalPengembalian),array('KodeTrk' => $KodeTrk));
						}else{
							$this->db->update('mstrbpkb',array('TanggalPengambilan' => $TanggalTransaksi),array('IdBPKB' => $IdBPKB));
							$this->db->update('trkbpkb',array('TanggalPengambilan' => $TanggalTransaksi),array('KodeTrk' => $KodeTrk));
						}
						$this->db->update('mstrbpkb',array('Status'=>$status),array('IdBPKB' => $IdBPKB));
						$this->db->update('trkbpkb',array('StatusTerima'=>1),array('KodeTrk' => $KodeTrk));
						$this->usertask->sendEmailBPKBToAct('TBP'.$Kepentingan,$this->npkLogin,'',$KodeTrk);
						$this->usertask->sendEmailBPKBToHR('TBP'.$Kepentingan,$this->npkLogin,'',$KodeTrk);

						$this->db->trans_commit();
						$success = 'Sukses menambahkan ke dalam sistem';
						
						echo json_encode(array("message"=>$success));
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ajax_kembaliBPKB(){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("BPKB User"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						$IdBPKB = $this->input->post('IdBPKB');
						$Kepentingan = $this->input->post('Kepentingan');
						$KodeTrk = $this->input->post('KodeTrk');
						$bpkb = $this->peminjamandokumen_model->GetBPKB($IdBPKB);
						$Deleted = 0;
						$IdBPKB =  $IdBPKB;
						$status = 0;
						$this->db->trans_begin();
						if($Kepentingan == 'Peminjaman' || $Kepentingan == 'PeminjamanHead'){
							$status = 1;
							$Kepentingan = "R";
							$this->usertask->sendEmailBPKB('TBP'.$Kepentingan,$this->npkLogin,'',$KodeTrk);
						}
						$this->db->update('mstrbpkb',array('Status'=>$status),array('IdBPKB' => $IdBPKB));
						
						$UpdatedOn = date("Y-m-d H:i:s");
						$UpdatedBy = $this->npkLogin;
						$StatusTerima = 2;
						$this->db->update('trkbpkb',array('StatusTerima'=>$StatusTerima, 'UpdatedOn' => $UpdatedOn, 'UpdatedBy' => $UpdatedBy),array('KodeTrk' => $KodeTrk));
						$this->db->trans_commit();

						$success = 'Sukses menambahkan ke dalam sistem';
						
						echo json_encode(array("message"=>$success));
					}
				}
				else{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ApprovalBPKB($KodeUserTask)
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("BPKB User"))
					{
						$dataTrkBPKB = $this->peminjamandokumen_model->getTrkBPKB($KodeUserTask);
						$this->load->helper(array('form','url'));
						$historycatatan = $this->usertask->getCatatan($KodeUserTask);
						$data = array(
							'title'=> 'Data BPKB',
							'data' => $dataTrkBPKB,
							'historycatatan' => $historycatatan,
							'kodeusertask'=> $KodeUserTask
						);
						$this->template->load('default','PeminjamanDokumen/ApprovalBPKB_view',$data);
					}
				}
				else
				{
					redirect("login?u=PeminjamanDokumen/BPKBController/ApprovalBPKB_view/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ajax_prosesApprovalBPKB()
		{
			try
			{
				$success = "Berhasil";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				$IdBPKB = '';
				$dataTrkBPKB = $this->peminjamandokumen_model->getTrkBPKB($KodeUserTask);

				if($dataTrkBPKB){
					foreach($dataTrkBPKB as $row){
						$KodeTrk = $row->KodeTrk;
						$pengajuTrk = $row->NPK;
						$statusApproval = substr($row->StatusApproval, -1);//dapatkan approval 1/2
						$statusTrk = substr($row->StatusApproval,0,2); //dapatkan PE
						$Kepentingan = $row->Kepentingan;
						$IdBPKB = $row->IdBPKB;
						if($Kepentingan == "Peminjaman"){
							$Kepentingan = 'R';
						}else{
							$Kepentingan = 'T';
						}
					}
				}else{
					$success = "Tidak ada data";
				}
				
				if($statusTrk == 'PE')
				{
					$this->db->trans_begin();
					
					$params = "PeminjamanDokumen/BPKBController/ListBPKB_User";
					$url = base_url() ."index.php/$params/$KodeUserTask";
					//update status cuti
					if($status == 'AP')
					{
						if($statusApproval == 2){
							$TanggalTransaksi = date("Y-m-d H:i:s");
							$this->peminjamandokumen_model->updateStatusTrkBPKB($KodeUserTask,$status.$statusApproval,$this->npkLogin);
							if($Kepentingan == 'R'){
								$TanggalPengembalian = date('Y-m-d H:i:s', strtotime($TanggalTransaksi . ' +5 weekdays'));
								$this->db->update('mstrbpkb',array('TanggalPeminjaman' => $TanggalTransaksi, 'TanggalPengembalian' => $TanggalPengembalian),array('IdBPKB' => $IdBPKB));
								$this->db->update('trkbpkb',array('TanggalPengembalian' => $TanggalPengembalian),array('KodeTrk' => $KodeTrk));
							}else{
								$this->db->update('mstrbpkb',array('TanggalPengambilan' => $TanggalTransaksi),array('IdBPKB' => $IdBPKB));
								$this->db->update('trkbpkb',array('TanggalPengambilan' => $TanggalTransaksi),array('KodeTrk' => $KodeTrk));
							}
							$this->usertask->sendEmailBPKBToAct($status.'BP'.$Kepentingan.$statusApproval,$pengajuTrk,$url,$KodeTrk);
							$this->usertask->sendEmailBPKB($status.'BP'.$Kepentingan.$statusApproval,$pengajuTrk,$url,$KodeTrk);
						}else{
							$this->usertask->sendEmailBPKB($status.'BP'.$Kepentingan.$statusApproval,$pengajuTrk,$url,$KodeTrk);
							$statusApproval++;
							$this->peminjamandokumen_model->updateStatusTrkBPKB($KodeUserTask,$statusTrk.$statusApproval,$this->npkLogin);
							$KodeUserTaskNext = generateNo('UT');
							if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeTrk,$KodeUserTaskNext,"BP".$Kepentingan.$statusApproval,$pengajuTrk))
							{
								$success = "0";
								if($statusApproval == 2 && $status == 'AP'){
									$success = "Berhasil";
	
								}
							}
						}
						
					}
					if($status == 'DE')
					{
						$this->peminjamandokumen_model->updateStatusTrkBPKB($KodeUserTask,$status.$statusApproval,$this->npkLogin);
						if($Kepentingan == 'R'){
							$this->db->update('mstrbpkb',array('Status'=>0, 'TanggalPeminjaman' => null, 'TanggalPengembalian' => null),array('IdBPKB' => $IdBPKB));
						}else{
							$this->db->update('mstrbpkb',array('Status'=>7, 'TanggalPengambilan' => null),array('IdBPKB' => $IdBPKB));
						}
						$this->usertask->sendEmailBPKB($status.'BP'.$Kepentingan.$statusApproval,$pengajuTrk,$url,$KodeTrk);

					}
					$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin, $catatan);


					//create user task baru untuk next proses
					
					if ($this->db->trans_status() === FALSE)
					{
						fire_print('log','trans rollback approve cuti ');
						$this->db->trans_rollback();
					}
					else
					{
						fire_print('log','trans commit approve cuti ');
						$this->db->trans_commit();
					}
					fire_print('log','Proses Trans Approve Cuti selesai ');
				}
				echo $success;
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}

		function printbpkb(){
			$IdBPKB = $_GET['IdBPKB'];
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
			//CreateUserTask to Atasan
			$sqlSelect =
						"SELECT m.IdBPKB, m.NoPolisi, m.NoBPKB, m.JenisKendaraan,
						DATE_FORMAT(m.TanggalPeminjaman,'%d/%m/%Y') as TanggalPeminjaman,
						DATE_FORMAT(m.TanggalPengembalian,'%d/%m/%Y') as TanggalPengembalian,
						DATE_FORMAT(m.TanggalPengambilan,'%d/%m/%Y') as TanggalPengambilan,
						m.Status
						from mstrbpkb m
						where
						m.NPK = '$this->npkLogin' and m.IdBPKB = '$IdBPKB'
						and m.Deleted = 0
						";
			$user = $this->user->dataUser($this->npkLogin);
			$NamaUser = $user[0]->nama;
			$NPKAtasan = ($user[0]->NPKAtasan);
			$atasan = $this->user->dataUser($NPKAtasan);
			$NamaAtasan = $atasan[0]->nama;
			$data = $this->db->query($sqlSelect)->result();
			$dataBPKB = array(
				'title'=> 'Form BPKB',
				'data' => $data,
				'nama' => $NamaUser,
				'atasan' => $NamaAtasan

			);
			$this->load->view('PeminjamanDokumen/CetakBPKB.php', $dataBPKB);

		}
	}
?>