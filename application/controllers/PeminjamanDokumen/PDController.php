<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class PDController extends CI_Controller
	{
		var $NPKLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('menu','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('ldarchive_model','',TRUE);
			$this->load->library('grocery_crud');
			$this->load->model('ppb_model','',TRUE);
			$this->load->model('peminjamandokumen_model','',TRUE);
			$this->load->model('bm_activity_model','',TRUE);
			$this->load->model('bm_summarybudget_model','',TRUE);
			$this->load->model('usertask','',TRUE);
			$this->load->model('lembur_model','',TRUE);
			$this->load->library('email');
			$this->load->model('dtltrkrwy_model','',TRUE);
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
			$this->linkEmail = base_url()."/index.php/PeminjamanDokumen/PDControllerNonLogin/";
		}
		function index()
		{
			try
			{
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
        }
        public function FPD(){
            //Form Peminjaman Dokumen
            try
            {
                if($this->session->userdata('logged_in')){
                    if(check_authorizedByName("Form Peminjaman Dokumen"))
                    {
						$data = array(
							'title' => 'Form Peminjaman Dokumen',
							'admin' => '1',
							'npk' => $this->npkLogin
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','PeminjamanDokumen/FPD_view',$data);
                    }
                }
                else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
            }
            catch(Exception $e)
            {
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
            }
		}
		function ajax_cariDetail()
		{
			$today = date('Y/m/d');
			$tanggalDeadline = date('d/m/Y', strtotime($today. ' +2 weekdays'));
			$today = date('d/m/Y');
			$NomorVoucher = $_POST["NomorVoucher"];
			$NomorPeserta = $_POST["NomorPeserta"];
			$DPA = $_POST["DPA"];
			$url = $this->config->item('urlWebService');
			$client = new SoapClient($url);
			if($DPA == 1){
				$params = array(
					"NoVoucher"=>$NomorVoucher,
					"NomorPeserta"=>$NomorPeserta
				);
				$result = $client->GetDetailVoucher_siDAPEN($params);
				$result = ($result->GetDetailVoucher_siDAPENResult);
				if($result != "0"){
					$TanggalTerbit = explode("|", $result)[6];
					$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
					$NomorPeserta = "";
					if(strlen(trim(explode("|", $result)[1])) != 6){
						$NomorPeserta = "-";
					}else{
						$NomorPeserta = trim(explode("|", $result)[1]);
					}
					if($NomorVoucher == ""){
						$NomorVoucher = explode("|",$result)[0];
					}
					$result = array(
						"NomorVoucher" => $NomorVoucher,
						"NomorPeserta" => $NomorPeserta,
						"TanggalTerbit" => $TanggalTerbit,
						"Keterangan" => explode("|",$result)[2],
						"Nominal"=> number_format( explode("|",$result)[4]),
						"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
						"Bank"=>  explode("|",$result)[3],
						"TanggalPinjam"=> $today,
						"TanggalPengembalian" => $tanggalDeadline);
				}else{
					$result = array("msg"=>"Tidak ditemukan");
				}
			}else if($DPA == 2){
				$params = array(
					"NoVoucher"=>$NomorVoucher,
					"NomorPeserta"=>$NomorPeserta
				);
				$result = $client->GetDetailVoucher_CORE($params);
				$result = ($result->GetDetailVoucher_COREResult);
				if($result == "0"){
					$params = array(
						"NoVoucher"=>$NomorVoucher
					);
					$result = $client->GetDetailVoucher_ACCPAC($params);
					$result = ($result->GetDetailVoucher_ACCPACResult);
					if($result == "0"){
						$result = $client->GetDetailVoucher_SIAP($params);
						$result = ($result->GetDetailVoucher_SIAPResult);
						if($result == "0"){
							$result = array("msg"=>"Tidak ditemukan");
						}else{
							$TanggalTerbit = explode("|", $result)[5];
							$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
							if(strlen(trim(explode("|", $result)[1])) != 6){
								$NomorPeserta = "-";
							}else{
								$NomorPeserta = trim(explode("|", $result)[1]);
							}
							if($NomorVoucher == ""){
								$NomorVoucher = explode("|",$result)[0];
							}
							$result = array(
								"NomorVoucher" => $NomorVoucher,
								"NomorPeserta" => $NomorPeserta,
								"TanggalTerbit" => $TanggalTerbit,
								"Keterangan" => explode("|",$result)[2],
								"Nominal"=> number_format( explode("|",$result)[3]),
								"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
								"Bank"=>  explode("|",$result)[0],
								"TanggalPinjam"=> $today,
								"TanggalPengembalian" => $tanggalDeadline);
						}
					}else{
						$TanggalTerbit = explode("|", $result)[5];
						$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
						if(strlen(trim(explode("|", $result)[1])) != 6){
							$NomorPeserta = "-";
						}else{
							$NomorPeserta = trim(explode("|", $result)[1]);
						}
						if($NomorVoucher == ""){
							$NomorVoucher = explode("|",$result)[0];
						}
						$result = array(
							"NomorVoucher" => $NomorVoucher,
							"NomorPeserta" => $NomorPeserta,
							"TanggalTerbit" => $TanggalTerbit,
							"Keterangan" => explode("|",$result)[2],
							"Nominal"=> number_format( explode("|",$result)[3]),
							"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
							"Bank"=>  explode("|",$result)[0],
							"TanggalPinjam"=> $today,
							"TanggalPengembalian" => $tanggalDeadline);
					}
				}else{
					$TanggalTerbit = explode("|", $result)[5];
					$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
					if(strlen(trim(explode("|", $result)[1])) != 6){
						$NomorPeserta = "-";
					}else{
						$NomorPeserta = trim(explode("|", $result)[1]);
					}
					if($NomorVoucher == ""){
						$NomorVoucher = explode("|",$result)[0];
					}
					$result = array(
						"NomorVoucher" => $NomorVoucher,
						"NomorPeserta" => $NomorPeserta,
						"TanggalTerbit" => $TanggalTerbit,
						"Keterangan" => explode("|",$result)[2],
						"Nominal"=> number_format( explode("|",$result)[3]),
						"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
						"Bank"=>  explode("|",$result)[0],
						"TanggalPinjam"=> $today,
						"TanggalPengembalian" => $tanggalDeadline);
				}
			}

			echo json_encode($result);
		}
		function cariDetail($NomorVoucher, $NomorPeserta, $DPA)
		{
			$today = date('Y/m/d');
			$tanggalDeadline = date('d/m/Y', strtotime($today. ' +2 weekdays'));
			$today = date('d/m/Y');
			$url = $this->config->item('urlWebService');
			$client = new SoapClient($url);
			if($DPA == 1){
				$params = array(
					"NoVoucher"=>$NomorVoucher,
					"NomorPeserta"=>$NomorPeserta
				);
				$result = $client->GetDetailVoucher_siDAPEN($params);
				$result = ($result->GetDetailVoucher_siDAPENResult);
				if($result != "0"){
					$TanggalTerbit = explode("|", $result)[6];
					$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
					if(strlen(trim(explode("|", $result)[1])) != 6){
						$NomorPeserta = "-";
					}else{
						$NomorPeserta = trim(explode("|", $result)[1]);
					}
					if($NomorVoucher == ""){
						$NomorVoucher = explode("|",$result)[0];
					}
					$result = array(
						"NomorVoucher" => $NomorVoucher,
						"NomorPeserta" => $NomorPeserta,
						"TanggalTerbit" => $TanggalTerbit,
						"Keterangan" => explode("|",$result)[2],
						"Nominal"=> number_format( explode("|",$result)[4]),
						"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
						"Bank"=>  explode("|",$result)[3],
						"TanggalPinjam"=> $today,
						"TanggalPengembalian" => $tanggalDeadline);
				}else{
					$result = array("msg"=>"Tidak ditemukan");
				}
			}else if($DPA == 2){
				$params = array(
					"NoVoucher"=>$NomorVoucher,
					"NomorPeserta"=>$NomorPeserta
				);
				$result = $client->GetDetailVoucher_CORE($params);
				$result = ($result->GetDetailVoucher_COREResult);
				if($result == "0"){
					$params = array(
						"NoVoucher"=>$NomorVoucher
					);
					$result = $client->GetDetailVoucher_ACCPAC($params);
					$result = ($result->GetDetailVoucher_ACCPACResult);
					if($result == "0"){
						$result = $client->GetDetailVoucher_SIAP($params);
						$result = ($result->GetDetailVoucher_SIAPResult);
						if($result == "0"){
							$result = array("msg"=>"Tidak ditemukan");
						}else{
							$TanggalTerbit = explode("|", $result)[5];
							$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
							if(strlen(trim(explode("|", $result)[1])) != 6){
								$NomorPeserta = "-";
							}else{
								$NomorPeserta = trim(explode("|", $result)[1]);
							}
							if($NomorVoucher == ""){
								$NomorVoucher = explode("|",$result)[0];
							}
							$result = array(
								"NomorVoucher" => $NomorVoucher,
								"NomorPeserta" => $NomorPeserta,
								"TanggalTerbit" => $TanggalTerbit,
								"Keterangan" => explode("|",$result)[2],
								"Nominal"=> number_format( explode("|",$result)[3]),
								"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
								"Bank"=>  explode("|",$result)[0],
								"TanggalPinjam"=> $today,
								"TanggalPengembalian" => $tanggalDeadline);
						}
					}else{
						$TanggalTerbit = explode("|", $result)[5];
						$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
						if(strlen(trim(explode("|", $result)[1])) != 6){
							$NomorPeserta = "-";
						}else{
							$NomorPeserta = trim(explode("|", $result)[1]);
						}
						if($NomorVoucher == ""){
							$NomorVoucher = explode("|",$result)[0];
						}
						$result = array(
							"NomorVoucher" => $NomorVoucher,
							"NomorPeserta" => $NomorPeserta,
							"TanggalTerbit" => $TanggalTerbit,
							"Keterangan" => explode("|",$result)[2],
							"Nominal"=> number_format( explode("|",$result)[3]),
							"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
							"Bank"=>  explode("|",$result)[0],
							"TanggalPinjam"=> $today,
							"TanggalPengembalian" => $tanggalDeadline);
					}
				}else{
					$TanggalTerbit = explode("|", $result)[5];
					$TanggalTerbit = date('d/m/Y', strtotime($TanggalTerbit));
					if(strlen(trim(explode("|", $result)[1])) != 6){
						$NomorPeserta = "-";
					}else{
						$NomorPeserta = trim(explode("|", $result)[1]);
					}
					if($NomorVoucher == ""){
						$NomorVoucher = explode("|",$result)[0];
					}
					$result = array(
						"NomorVoucher" => $NomorVoucher,
						"NomorPeserta" => $NomorPeserta,
						"TanggalTerbit" => $TanggalTerbit,
						"Keterangan" => explode("|",$result)[2],
						"Nominal"=> number_format( explode("|",$result)[3]),
						"Status" =>  $this->peminjamandokumen_model->checkBorrowingStatus($NomorVoucher, $NomorPeserta),
						"Bank"=>  explode("|",$result)[0],
						"TanggalPinjam"=> $today,
						"TanggalPengembalian" => $tanggalDeadline);
				}
			}
			return json_encode($result);
		}
		public function CreateFPD()
		{
			$this->session->set_flashdata('msg','');
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Form Peminjaman Dokumen"))
					{
						$DPA = strip_tags($this->input->post('txtDPA'));
						$NPK = $this->npkLogin;
						$CreatedBy = $NPK;
						$NomorVoucher = strip_tags($this->input->post('txtNomorVoucher'));
						$NomorPeserta = strip_tags($this->input->post('txtNomorPeserta'));

						$TanggalTerbit = $this->input->post('txtTanggalTerbit');
						$TanggalTerbitDate = date_create_from_format('d/m/Y', $TanggalTerbit);
						$TanggalTerbitVoucher = $TanggalTerbitDate->format('Y/m/d');
						$Keterangan = strip_tags($this->input->post('txtKeterangan'));
						$Nominal = strip_tags($this->input->post('txtNominal'));
						$Bank = strip_tags($this->input->post('txtBank'));
						$Keperluan = strip_tags($this->input->post('selKeperluan'));
						$KodeFPD = $this->peminjamandokumen_model->generateKodeFPD($NPK, $DPA);
						$SecretKey = $this->generateRandomString();
						$sqlInsert = "INSERT INTO pd_fpd
						(Deleted, KodeFPD, DPA, NomorVoucher, NomorPeserta, IsBorrowed, Keperluan, TanggalPeminjaman, TanggalTerbitVoucher, TanggalDeadline, IsReceived, IsBack, CreatedOn, CreatedBy, SecretKey) 
						VALUES (0, ?, ?, ?, ?, 0, ?, curdate(), ?,
						DATE_ADD(curdate(), INTERVAL 2 +
							IF(
								(WEEK(curdate()) <> WEEK(DATE_ADD(curdate(), INTERVAL 2 DAY)))
								OR (WEEKDAY(DATE_ADD(curdate(), INTERVAL 2 DAY)) IN (5, 6)),
								2,
								0)
							DAY
							)
						, 0, 0, now(), ?, ?);
						";
						if($this->db->query($sqlInsert, array($KodeFPD, $DPA, $NomorVoucher, $NomorPeserta, $Keperluan, $TanggalTerbitVoucher, $CreatedBy, $SecretKey)))
						{
							fire_print('log','sudah masuk ke sebelum kirim email');
							return $this->sendEmailPD('Peminjaman Dokumen', $KodeFPD, $NPK, $NomorVoucher, $NomorPeserta, $TanggalTerbitVoucher, $Nominal, $Bank, $DPA, $SecretKey, "Peminjaman");
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		//user
        public function ListFPD_User(){
            //Form Peminjaman Dokumen
            try
            {
                if($this->session->userdata('logged_in')){
                    if(check_authorizedByName("List FPD User"))
                    {
						$sqlSelect =
						"SELECT fpd.KodeFPD, mu.Nama as Peminjam, CONCAT(fpd.NomorVoucher,', ',fpd.NomorPeserta) as KodeVoucherNomorPeserta
						, fpd.IsBorrowed, fpd.TanggalDeadline, fpd.IsReceived, fpd.IsBack FROM PD_FPD fpd join mstruser mu on fpd.CreatedBy = mu.NPK WHERE
						fpd.CREATEDBY = ?
						and fpd.deleted = 0
						and mu.deleted = 0 order by fpd.createdOn desc;";
						$dataFPD = $this->db->query($sqlSelect, array($this->npkLogin))->result();
                        $data = array(
							'title' => 'List Form Peminjaman Dokumen',
							'admin' => '1',
							'data' => $dataFPD,
							'npk' => $this->npkLogin
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','PeminjamanDokumen/listFPDuser_view',$data);
                    }
                }
                else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
            }
            catch(Exception $e)
            {
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
            }
		}
		function ajax_cancel(){
			$KodeFPD = $_POST['KodeFPD'];
			if($this->Cancel_($KodeFPD)){
				$this->session->set_flashdata('msg','Sukses Batal Pinjam Dokumen');
				echo json_encode(array("message"=>"Sukses Batal Pinjam Dokumen"));
			}else{
				$this->session->set_flashdata('msg','Gagal Batal Pinjam Dokumen');
				echo json_encode(array("message"=>"Gagal Batal Pinjam Dokumen"));
			}
		}
		public function Cancel_($KodeFPD){
			$DatabaseFPD = $this->peminjamandokumen_model->GetDetailByKodeFPD($KodeFPD);
			if($DatabaseFPD){
				$QueryUpdate = "UPDATE PD_FPD set deleted = 1 where KodeFPD = '$KodeFPD' and Deleted = 0 and IsBorrowed = 0 and IsReceived = 0";
				if($this->db->query($QueryUpdate)){
					$DetailFPD = json_decode($this->cariDetail($DatabaseFPD->NomorVoucher, $DatabaseFPD->NomorPeserta, $DatabaseFPD->DPA), true);
					if($this->sendEmailPD('Pembatalan Peminjaman Dokumen',
						$KodeFPD,
						$DatabaseFPD->CreatedBy,
						$DetailFPD['NomorVoucher'],
						$DetailFPD['NomorPeserta'],
						$DetailFPD['TanggalTerbit'],
						$DetailFPD['Nominal'],
						$DetailFPD['Bank'],
						$DatabaseFPD->DPA,
						$DatabaseFPD->SecretKey,
						$Purpose="AfterCanceled"))
					{
						return true;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		function ajax_receive(){
			$KodeFPD = $_POST['KodeFPD'];
			if($this->Receive_($KodeFPD)){
				$this->session->set_flashdata('msg','Sukses Konfirmasi Terima Dokumen');
				echo json_encode(array("message"=>"Sukses Konfirmasi Terima Dokumen"));
			}else{
				$this->session->set_flashdata('msg','Gagal Konfirmasi Terima Dokumen');
				echo json_encode(array("message"=>"Gagal Konfirmasi Terima Dokumen"));
			}
		}
		public function Receive_($KodeFPD){
			$DatabaseFPD = $this->peminjamandokumen_model->GetDetailByKodeFPD($KodeFPD);
			if($DatabaseFPD){
				$QueryUpdate = "UPDATE PD_FPD set IsReceived = 1, IsBorrowed = 1 where KodeFPD = '$KodeFPD' and Deleted = 0 and IsReceived = 0";
				if($this->db->query($QueryUpdate)){
					$DetailFPD = json_decode($this->cariDetail($DatabaseFPD->NomorVoucher, $DatabaseFPD->NomorPeserta, $DatabaseFPD->DPA), true);
					if($this->sendEmailPD('Penerimaan Peminjaman Dokumen',
						$KodeFPD,
						$DatabaseFPD->CreatedBy,
						$DetailFPD['NomorVoucher'],
						$DetailFPD['NomorPeserta'],
						$DetailFPD['TanggalTerbit'],
						$DetailFPD['Nominal'],
						$DetailFPD['Bank'],
						$DatabaseFPD->DPA,
						$DatabaseFPD->SecretKey,
						$Purpose="AfterReceived"))
					{
						return true;
					}
					else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		//accounting
		function ajax_reject(){
			$KodeFPD = $_POST['KodeFPD'];
			if($this->Reject_($KodeFPD)){
				$this->session->set_flashdata('msg','Sukses Menolak Peminjaman Dokumen');
				echo json_encode(array("message"=>"Sukses Menolak Peminjaman Dokumen"));
			}else{
				$this->session->set_flashdata('msg','Gagal Menolak Peminjaman Dokumen');
				echo json_encode(array("message"=>"Gagal Menolak Peminjaman Dokumen"));
			}
		}
		public function Reject_($KodeFPD){
			$DatabaseFPD = $this->peminjamandokumen_model->GetDetailByKodeFPD($KodeFPD);
			if($DatabaseFPD){
				$QueryUpdate = "UPDATE PD_FPD set Deleted = 1,
					UpdatedOn = now(), UpdatedBy = '$this->npkLogin'
					where KodeFPD = '$KodeFPD' and Deleted = 0 and IsReceived = 0";
				if($this->db->query($QueryUpdate)){
					$DetailFPD = json_decode($this->cariDetail($DatabaseFPD->NomorVoucher, $DatabaseFPD->NomorPeserta, $DatabaseFPD->DPA), true);
					if($this->sendEmailPD('Penolakan Peminjaman Dokumen',
						$KodeFPD,
						$DatabaseFPD->CreatedBy,
						$DetailFPD['NomorVoucher'],
						$DetailFPD['NomorPeserta'],
						$DetailFPD['TanggalTerbit'],
						$DetailFPD['Nominal'],
						$DetailFPD['Bank'],
						$DatabaseFPD->DPA,
						$DatabaseFPD->SecretKey,
						$Purpose="AfterRejected"))
					{
						return true;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		function ajax_return(){
			$KodeFPD = $_POST['KodeFPD'];
			if($this->Return_($KodeFPD)){
				$this->session->set_flashdata('msg','Sukses Menerima Pengembalian Dokumen');
				echo json_encode(array("message"=>"Sukses Menerima Pengembalian Dokumen"));
			}else{
				$this->session->set_flashdata('msg','Gagal Menerima Pengembalian Dokumen');
				echo json_encode(array("message"=>"Gagal Menerima Pengembalian Dokumen"));
			}
		}
		public function Return_($KodeFPD){
			$DatabaseFPD = $this->peminjamandokumen_model->GetDetailByKodeFPD($KodeFPD);
			if($DatabaseFPD){
				$QueryUpdate = "UPDATE PD_FPD set IsBorrowed = 0, IsBack = 1,
					UpdatedOn = now(), UpdatedBy = '$this->npkLogin'
					where KodeFPD = '$KodeFPD' and Deleted = 0 and IsReceived = 1 and IsBorrowed = 1";
				if($this->db->query($QueryUpdate)){
					$DetailFPD = json_decode($this->cariDetail($DatabaseFPD->NomorVoucher, $DatabaseFPD->NomorPeserta, $DatabaseFPD->DPA), true);
					if($this->sendEmailPD('Pengembalian Peminjaman Dokumen',
						$KodeFPD,
						$DatabaseFPD->CreatedBy,
						$DetailFPD['NomorVoucher'],
						$DetailFPD['NomorPeserta'],
						$DetailFPD['TanggalTerbit'],
						$DetailFPD['Nominal'],
						$DetailFPD['Bank'],
						$DatabaseFPD->DPA,
						$DatabaseFPD->SecretKey,
						$Purpose="AfterReturned"))
					{
						return true;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
        public function ListFPD_Accounting(){
            //Form Peminjaman Dokumen
            try
            {
                if($this->session->userdata('logged_in')){
                    if(check_authorizedByName("List FPD Accounting"))
                    {
						$sqlSelect =
						"SELECT fpd.KodeFPD, mu.Nama as Peminjam, CONCAT(fpd.NomorVoucher,', ',fpd.NomorPeserta) as KodeVoucherNomorPeserta
						, fpd.IsBorrowed, fpd.TanggalDeadline, fpd.TanggalTerbitVoucher, fpd.IsReceived, fpd.IsBack FROM PD_FPD fpd join mstruser mu on fpd.CreatedBy = mu.NPK
						and fpd.deleted = 0
						and mu.deleted = 0
						and fpd.isBack = 0
						order by fpd.isBack, fpd.createdOn desc limit 100;
							";
						$dataFPD = $this->db->query($sqlSelect)->result();
                        $data = array(
							'title' => 'List Peminjaman Dokumen Accounting',
							'admin' => '1',
							'data' => $dataFPD,
							'npk' => $this->npkLogin
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','PeminjamanDokumen/listFPDact_view',$data);
                    }
                }
                else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
            }
            catch(Exception $e)
            {
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
            }
		}
		function generateRandomString($length = 20) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			$selectSecretKeyFromDB = "Select SecretKey from pd_fpd where SecretKey ='$randomString'";
			if($result = $this->db->query($selectSecretKeyFromDB)->result()){
				if($result[0]->SecretKey == $randomString){
					generateRandomString();
				}else{
					return $randomString;
				}
			}else{
				return $randomString;
			}
		}
		function sendEmailPD($subject, $KodeFPD, $NPK, $NomorVoucher, $NomorPeserta, $TanggalTerbit, $Nominal, $Bank, $DPA, $SecretKey, $Purpose="Peminjaman", $Deadline="")
		{
			try
			{
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				if($this->config->item('enableEmail') == 'true'){
					if($Purpose == "Peminjaman"){
						$user = $this->user->dataUser($NPK);
						fire_print('log','sudah masuk ke awal kirim email');
						//4 = Accounting tax & control
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
						# code...
							$recipient = $accountingUser->EmailInternal;
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>'.$user[0]->nama.' meminjam dokumen sebagai berikut:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>Jika peminjaman ini tidak dapat diterima, silahkan ditolak melalui <a href="'.$this->linkEmail.'RejectByEmail/'.$accountingUser->NPK.'/'.$SecretKey.'">link</a> berikut ini 
								<br/>Jika peminjaman dapat dilakukan, harap segera memberikan dokumen terkait ke user.
								<br/>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->to($recipient);
							$this->email->subject($subject);
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Konfirmasi Penerimaan Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Peminjaman dokumen sedang diproses:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>Jika dokumen yang dipinjam sudah diterima, silahkan konfirmasi melalui link <a href="'.$this->linkEmail.'ReceiveByEmail/'.$NPK.'/'.$SecretKey.'">link</a> berikut ini 
							<br/>Jika peminjaman ini ingin dibatalkan silahkan konfirmasi batal melalui link <a href="'.$this->linkEmail.'CancelByEmail/'.$NPK.'/'.$SecretKey.'">link</a> berikut ini 
							<br/>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							$this->session->set_flashdata('msg','Sukses Pinjam Dokumen');
							redirect("PeminjamanDokumen/PDController/ListFPD_User",'refresh');
							return true;
						}
						
					}
					else if($Purpose == "AfterReceived"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>'.$user[0]->nama.' sudah mengkonfirmasi penerimaan peminjaman dokumen berikut:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								</ul>
								<br/>Terima kasih atas peminjamannya, Selamat bekerja kembali
								<br/>ESS
							';
							$this->email->to($recipient);
							$this->email->subject('Notifikasi Penerimaan Peminjaman Dokumen');
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Notifikasi Status Peminjaman Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Dokumen berikut sudah tercatat dengan status <b>sedang dipinjam</b>:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							</ul>
							<br/>Terima kasih atas konfirmasinya, Selamat bekerja kembali
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							return true;
						}
					}
					else if($Purpose == "AfterCanceled"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$this->email->to($recipient);
							$this->email->subject('Notifikasi Pembatalan Peminjaman Dokumen');
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>'.$user[0]->nama.' membatalkan peminjaman dokumen berikut:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Notifikasi Pembatalan Peminjaman Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Peminjaman dokumen berikut sudah berhasil dibatalkan:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							return true;
						}
					}
					else if($Purpose == "AfterRejected"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$this->email->to($recipient);
							$this->email->subject('Notifikasi Penolakan Peminjaman Dokumen');
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>Dokumen berikut sudah tercatat dengan status <b>ditolak</b>:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Notifikasi Penolakan Peminjaman Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Peminjaman dokumen berikut telah ditolak oleh Accounting:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							return true;
						}
					}
					else if($Purpose == "AfterReturned"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$this->email->to($recipient);
							$this->email->subject('Notifikasi Pengembalian Peminjaman Dokumen');
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>Dokumen berikut sudah tercatat dengan status <b>sudah dikembalikan</b>:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								</ul>
								<br/>Terima kasih atas konfirmasinya, Selamat Bekerja Kembali
								<br/>ESS
							';
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Notifikasi Pengembalian Peminjaman Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Accounting sudah mengkonfirmasi pengembalian dokumen berikut:
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							</ul>
							<br/>Terima kasih atas pengembaliannya, Selamat Bekerja Kembali
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							return true;
						}
					}
					else if($Purpose == "Notifikasi"){
						fire_print('log','sudah masuk ke awal kirim email');
						$user = $this->user->dataUser($NPK);
						$accountingTeam = $this->user->getAllUserByKodeDepartemen(4);
						foreach ($accountingTeam as $accountingUser) {
							# code...
							$recipient = $accountingUser->EmailInternal;
							$this->email->to($recipient);
							$this->email->subject('Notifikasi Pengingat Deadline Peminjaman Dokumen');
							$messageAccounting = 'Dear '.$accountingUser->Nama.',
								<br/><br/>Dokumen berikut Sudah Mendekati Deadline Peminjaman '.$Deadline.' </b>:
								<ul>
								<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
								<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
								<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
								<li>Nominal: <b>'.($Nominal).'</b></li>
								<li>Kode Bank: <b>'.$Bank.'</b></li>
								<li>DPA: <b>'.$DPA.'</b></li>
								</ul>Jika dokumen sudah dikembalikan, silahkan konfirmasi melalui <a href="'.$this->linkEmail.'ReturnByEmail/'.$accountingUser->NPK.'/'.$SecretKey.'">link</a> berikut ini 
								<br/>
								<br/>Terima kasih
								<br/>ESS
							';
							$this->email->message($messageAccounting);
							$this->email->send();
						}
						$user = $this->user->dataUser($NPK);
						$recipient = array($user[0]->email);
						$this->email->to($recipient);
						$this->email->subject('Notifikasi Deadline Peminjaman Dokumen');
						fire_print('log','sudah masuk lokasi aset');
						$message = 'Dear User,
							<br/><br/>Dokumen berikut Sudah Mendekati Deadline Peminjaman
							<ul>
							<li>Kode FPD: <b>'.$KodeFPD.'</b></li>
							<li>No: <b>'.$NomorVoucher.', '.$NomorPeserta.'</b></li>
							<li>Tanggal Terbit: <b>'.$TanggalTerbit.'</b></li>
							<li>Nominal: <b>'.($Nominal).'</b></li>
							<li>Kode Bank: <b>'.$Bank.'</b></li>
							<li>DPA: <b>'.$DPA.'</b></li>
							</ul>
							<br/>Terima kasih
							<br/>ESS
						';
						$this->email->message($message);
						if( ! $this->email->send())
						{
							fire_print('log','gagal email');
							return false;
						}
						else
						{
							fire_print('log','sukses email');
							return true;
						}
					}
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}

		public function ListFPD_Done($dpa='',$tahun='',$jenis='',$katakunci='', $page='')
		{
			$session_data = $this->session->userdata('logged_in');
			if($session_data){
				if(check_authorized("319"))
				{
					$this->npkLogin = $session_data['npk'];
					if($dpa == 'non'){
						$dpa = '';
					}if($tahun == 'non'){
						$tahun = '';
					}if($jenis == 'non'){
						$jenis = '';
					}if($katakunci == 'non'){
						$katakunci = '';
					}
					$this->_ListFPD_ActDone($dpa, $tahun, $jenis, $katakunci, 'view');
				}
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		public function _ListFPD_ActDone($dpa='',$tahun='',$jenis='',$katakunci='',$page='')
		{
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
			$crud = new grocery_crud();
			$crud->set_subject('List FPD Accounting');
			//$crud->set_theme('datatables');
			$crud->set_theme('datatables');
			$crud->set_table('pd_fpd');
			$crud->where('pd_fpd.deleted','0');
			$crud->where('pd_fpd.isReceived','1');
			$crud->where('pd_fpd.isBack','1');
			
			if($dpa != '')
			{
				$crud->where("pd_fpd.dpa", $dpa);
			}
			if($tahun != '')
			{
				$crud->where('pd_fpd.tahun',$tahun);				
			}
			if($jenis != '')
			{
				$crud->where('pd_fpd.kodejenis',$jenis);
			}
			if($katakunci != '')
			{
				$crud->like('pd_fpd.vendor', $katakunci);
			}

			switch($page)
			{
					case "add":
						break;
					case "view":
						$crud->unset_add();
						break;
					default:
						break;
			}
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->set_relation('CreatedBy','mstruser','Nama',array('deleted' => '0'));
			$crud->order_by('CreatedOn','desc');
			$state = $crud->getState();
			
			$crud->unset_back_to_list();
			// $crud->set_relation_n_n('PeraturanTerkait','peraturanterkaitdokumenlegal','mstrdokumenpajak','KodeDokumenPajakParent','KodeDokumenPajak','{NoBupot} DPA {DPA}', null, array('mstrdokumenpajak.deleted'=>0));

			$crud->columns('ID','KodeFPD', 'CreatedBy', 'NomorVoucher', 'NomorPeserta', 'TanggalDeadline', 'TanggalTerbitVoucher', 'IsBack');
			$crud->fields('ID','KodeFPD', 'CreatedBy', 'NomorVoucher', 'NomorPeserta', 'TanggalDeadline', 'TanggalTerbitVoucher', 'IsBack');
			$crud->display_as('CreatedBy','Nama Peminjam');
			// $crud->callback_column('PeraturanTerkait', array($this,'_callback_peraturan_terkait'));
			$crud->callback_column('TanggalDeadline', array($this,'_callback_formatTanggalLaporan'));
			$crud->callback_column('TanggalTerbitVoucher', array($this,'_callback_formatTanggalLaporan'));
			$crud->callback_column('IsBack', array($this,'_callback_IsBack'));
			//$crud->callback_column('TanggalMulaiBerlaku', array($this,'_callback_formatTanggalMulaiBerlaku'));
			//$crud->callback_column('TanggalKadaluarsa', array($this,'_callback_formatTanggalKadaluarsa'));
			//$crud->callback_column('DokumenYangDiganti', array($this,'_callback_dokumen_yang_diganti'));
			// $crud->callback_column('NamaFile', array($this,'_callback_show_Induk'));
			$output = $crud->render();
			$this-> _outputview($output, $page);      
		}	
		function _outputview($output = null, $page = '')
		{
			$session_data = $this->session->userdata('logged_in');
			
			$data = array(
				'title' => 'Peminjaman Dokumen',
				'body' => $output
			);
			$this->load->helper(array('form','url'));
			if($page == 'view')
			{
				$this->load->view('PeminjamanDokumen/PD_iframe',$data);
			}
			else
			{		
				$this->template->load('default','templates/CRUD_view',$data);
			}
		}
		function _callback_formatTanggalLaporan($value, $row){
			return substr($value, 8,2).'-'.substr($value, 5,2).'-'.substr($value, 0,4);
		}
		function _callback_isBack($value, $row){
			return 'Sudah Dikembalikan';
		}
	}
?>