<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class ReportPD extends CI_Controller {
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->model('ldarchive_model','',TRUE);
		$this->load->library('grocery_crud');
		$this->load->model('ppb_model','',TRUE);
		$this->load->model('peminjamandokumen_model','',TRUE);
		$this->load->model('bm_activity_model','',TRUE);
		$this->load->model('bm_summarybudget_model','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('lembur_model','',TRUE);
		$this->load->library('email');
		$this->load->model('dtltrkrwy_model','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$this->linkEmail = "http://localhost:8080/essdev/index.php/PeminjamanDokumen/PDControllerNonLogin/";
	}
 
    public function index()
    {
    }
    function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		$data = array(
			'title' => 'Kartu Bayar',
			'body' => $output
		);
		$this->load->helper(array('form','url'));
		if($page == ''){
			$this->load->view('PersetujuanPengeluaranBudget/KartuBayar_view',$data);
		}else if($page == 'list'){
			$this->template->load('default','PersetujuanPengeluaranBudget/KartuBayar_view',$data);
		}
    }
	public function ReportPeminjamanDokumen()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Report Peminjaman Dokumen"))
			{
				$this->npkLogin = $session_data['npk'];
				$user = $this->user->dataUser($this->npkLogin);
				$this->_ReportPeminjamanDokumen($user);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	public function _ReportPeminjamanDokumen($user)
    {
		$crud = new grocery_crud();
		$crud->set_subject('Report Peminjaman Dokumen');
		$crud->set_table('pd_fpd');
		$crud->where('pd_fpd.deleted','0');
		$crud->set_relation('CreatedBy','mstruser','Nama',array('deleted' => '0'));

		$crud->columns('KodeFPD','DPA', 'NomorVoucher', 'NomorPeserta', 'Keperluan', 'IsBorrowed', 'TanggalPeminjaman', 'TanggalDeadline', 'IsReceived','IsBack','CreatedBy');
		$crud->display_as('KodeFPD','Kode FPD');
		$crud->display_as('IsBorrowed','Status Pinjam');
		$crud->display_as('IsReceived','Status Diterima');
		$crud->display_as('IsBack','Status Kembali');
		$crud->display_as('CreatedBy','Nama Peminjam');
		$crud->display_as('NomorVoucher','Nomor Voucher');
		$crud->display_as('NomorPeserta','Nomor Peserta');
		$crud->display_as('TanggalPeminjaman','Tanggal Peminjaman');
		$crud->display_as('TanggalDeadline','Tanggal Deadline');

		$crud->callback_column('IsBorrowed', array($this,'_callback_IsBorrowed'));
		$crud->callback_column('IsReceived', array($this,'_callback_IsReceived'));
		$crud->callback_column('IsBack', array($this,'_callback_IsBack'));
		
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		$crud->unset_add();
        $output = $crud->render();
        $this-> _outputview($output, 'list');
	}
	
	function _callback_IsBorrowed($value, $row){
		if($value == 0){
			return 'Available';
		}else{
			return 'Not Available';
		}
	}
	function _callback_IsReceived($value, $row){
		if($value == 0){
			return 'Belum Diterima';
		}else{
			return 'Sudah Diterima';
		}
	}
	function _callback_IsBack($value, $row){
		if($value == 0){
			return 'Belum Kembali';
		}else{
			return 'Sudah Kembali';
		}
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */