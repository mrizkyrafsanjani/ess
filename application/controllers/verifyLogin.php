<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');

class VerifyLogin extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
 }

 function index()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');

   $this->form_validation->set_rules('npk', 'NPK', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'Password', 'required|xss_clean|callback_check_database');

   $urlTujuan = $this->input->get('u');
	 fire_print('log',"urlTujuan:$urlTujuan");
	 if($urlTujuan == ''){
		$urlTujuan = 'dashboard';
	 }
	 
   if($this->form_validation->run() == false)
   {
     //Field validation failed.  User redirected to login page
	 $data = array(
		'urlTujuan' => $urlTujuan
	 );
     $this->load->view('login_view',$data);
   }
   else
   {
     //Go to private area
     redirect($urlTujuan, 'refresh');
   }

 }

 function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $npk = $this->input->post('npk');
   //query the database
   $result = $this->user->login($npk, md5($password . "mX12J"));
   $resultTemp = $this->user->loginPasswordTemp($npk,md5($password."mX12J"));
   if ($resultTemp)
   {
		$sess_array = array();
		 foreach($resultTemp as $row)
		 {
		   $sess_array = array(
			 'npk' => $row->NPK,
			 'nama' => $row->Nama,
			 'koderole' => $row->KodeRole,
			 'departemen' => $row->Departemen
		   );
		   $this->session->set_userdata('logged_in', $sess_array);
		 }
		 
		redirect('ubahPassword','refresh');
		return true;
   } 
   else if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'npk' => $row->NPK,
         'nama' => $row->Nama,
		 'koderole' => $row->KodeRole,
		 'departemen' => $row->Departemen
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
	$this->user->setNullPasswordTemp($npk);
	 return true;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid NPK or password!');
     return false;
   }
 }
}
?>