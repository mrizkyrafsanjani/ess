<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class Home extends CI_Controller{
		function __construct(){
			parent::__construct();
		}

		function index(){
			if($this->session->userdata('logged_in')){
				$data = array(
					'title' => 'Dashboard',
					'body' => ''
					);

				$this->load->helper(array('form','url'));
				$this->template->load('default','notauthorized_view',$data);
			}
			else
			{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}

		function logout(){
			$this->session->unset_userdata('logged_in');
			session_destroy();
			redirect('','refresh');
		}
	}
?>