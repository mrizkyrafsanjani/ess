<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');

class SubmitNewPassword extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
   $this->load->model('menu','',TRUE);
   
	$this->load->helper(array('form','url'));
 }

 function index()
 {
	 $session_data = $this->session->userdata('logged_in');
	$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
	if($menu){
		$menu_array = array();
		foreach($menu as $row){
		   $menu_array[] = array(
			 'menuname' => $row->menuname,
			 'url' => $row->url
		   );					   
		}
	}
	 $data = array(
		   'title'=>'Ubah Password',
		   'npk' => $session_data['npk'],
		   'nama' => $session_data['nama'],
		   'menu_array' => $menu_array
	  );
   //This method will have the credentials validation
   $this->load->library('form_validation');

   $this->form_validation->set_rules('oldPassword', 'Old Password', 'trim|required|xss_clean');
   $this->form_validation->set_rules('newPassword', 'New Password', 'trim|required|xss_clean');
   $this->form_validation->set_rules('retypePassword', 'Retype Password', 'trim|required|xss_clean|callback_submit_password');

   if($this->form_validation->run() == false)
   {
     //Field validation failed.  User redirected to login page
     //$this->load->view('ubahPassword_view');
	 $this->template->load('default','ubahPassword_view',$data);
   }
   else
   {
     //Go to private area
     $this->template->load('default','ubahPassword_view',$data);
   }

 }

 function submit_password($password)
 {
   $session_data = $this->session->userdata('logged_in');
   $npk = $session_data['npk'];
	$oldPassword = $this->input->post('oldPassword');
   if($this->user->cekPassword($npk,$oldPassword) OR $this->user->cekPasswordTemp($npk,$oldPassword) ){
 
	   $result = $this->user->changePassword($npk, $password);
	
	   if($result)
	   {
		 $this->form_validation->set_message('submit_password', 'Password change success!');
		 $this->user->setNullPasswordTemp($npk);
		 return false;
	   }
	   else
	   {
		 $this->form_validation->set_message('submit_password', 'Invalid NPK!');
		 return false;
	   }
   }else{
		$this->form_validation->set_message('submit_password', 'Invalid old password!');
		 return false;
   }
 }
}
?>