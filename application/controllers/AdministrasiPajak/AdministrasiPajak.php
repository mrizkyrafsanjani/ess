<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class AdministrasiPajak extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->model('LDarchive_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
	public function index()
	{
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("319"))
            {
                $this->npkLogin = $session_data['npk'];
				$this->_AdministrasiPajak();
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }	
	
	
	public function _AdministrasiPajak($dpa='',$tahun='',$jenis='',$katakunci='',$page='')
    {
        $session_data = $this->session->userdata('logged_in');

		$this->npkLogin = $session_data['npk'];
		$crud = new grocery_crud();
		$crud->set_subject('Administrasi Pajak');
		//$crud->set_theme('datatables');
		
        $crud->set_table('mstrdokumenpajak');
        $crud->where('mstrdokumenpajak.deleted','0');
        
		if($dpa != '')
		{
			$crud->where("mstrdokumenpajak.dpa", $dpa);
		}
		if($tahun != '')
		{
			$crud->where('mstrdokumenpajak.tahun',$tahun);				
		}
		if($jenis != '')
		{
			$crud->where('mstrdokumenpajak.kodejenis',$jenis);
		}
		if($katakunci != '')
		{
			$crud->like('mstrdokumenpajak.vendor', $katakunci);
		}

		switch($page)
			{
				case "add":
					break;
				case "view":
					$crud->unset_add();
					break;
				default:
					break;
			}	
		// $query = $this->db->get_where('globalparam',array('Name'=>'Admin_DokumenLegal'));
		// foreach($query->result() as $row)
		// {
		// 	$npkAdminDokumenLegal = $row->Value;
		// }
		// if($this->user->isAdmin($this->npkLogin)){
		// 	//Admin DPA
			
		// }else if(strpos($npkAdminDokumenLegal, $this->npkLogin) != false){
		// 	//berarti dia admin DokumenLegal
		// }else{
		// 	//brarti user biasa / PIC
		// 	$crud->unset_edit();
		// 	$crud->unset_delete();
		// }
		$crud->unset_edit();
		$crud->unset_delete();

		$crud->set_relation('JenisPPh','jenispph','JenisPPh',array('deleted' => '0'));
		$crud->set_relation('CreatedBy','mstruser','Nama',array('deleted' => '0'));
		//$crud->set_relation('DokumenYangDiganti','mstrdokumenpajak','{NoBupot} DPA {DPA}',array('deleted' => '0'));
        
		// $crud->set_relation_n_n('PIC','picdokumenlegal','mstruser','KodeDokumenPajak','NPK','Nama');
		
		//$crud->set_relation('NPKYangDilimpahkan','mstruser','Nama',array('Deleted' => 0,'TanggalBerhenti' => '0000-00-00'));
		$state = $crud->getState();
		
		$crud->unset_back_to_list();
		// $crud->set_relation_n_n('PeraturanTerkait','peraturanterkaitdokumenlegal','mstrdokumenpajak','KodeDokumenPajakParent','KodeDokumenPajak','{NoBupot} DPA {DPA}', null, array('mstrdokumenpajak.deleted'=>0));

		$crud->columns('NoBupot','DPA','Vendor','TanggalLaporan', 'Masa', 'Tahun', 'JenisPPh','Nominal','CreatedBy','CreatedOn');
		
		// $crud->callback_column('PeraturanTerkait', array($this,'_callback_peraturan_terkait'));
		$crud->callback_column('TanggalLaporan', array($this,'_callback_formatTanggalLaporan'));
		//$crud->callback_column('TanggalMulaiBerlaku', array($this,'_callback_formatTanggalMulaiBerlaku'));
		//$crud->callback_column('TanggalKadaluarsa', array($this,'_callback_formatTanggalKadaluarsa'));
		//$crud->callback_column('DokumenYangDiganti', array($this,'_callback_dokumen_yang_diganti'));
		$crud->callback_column('NoBupot', array($this,'_callback_show_Bupot'));
		$crud->callback_column('Nominal', array($this,'_callback_show_Nominal'));
        

		$crud->callback_after_insert(array($this,'_after_insert_administrasiPajak'));
		$crud->callback_after_update(array($this,'_after_update_administrasiPajak'));
		$crud->callback_delete(array($this,'_delete_administrasiPajak'));
		$crud->set_lang_string('insert_success_message',
		'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
		<script type="text/javascript">
		window.location = "'.$this->config->base_url().'index.php/AdministrasiPajak/AdministrasiPajakController/";
		</script>
		<div style="display:none">');
		$crud->set_lang_string('update_success_message',
		'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
		<script type="text/javascript">
		window.location = "'.$this->config->base_url().'index.php/AdministrasiPajak/AdministrasiPajak/searchAndViewAdministrasiPajak/";
		</script>
		<div style="display:none">');
        $output = $crud->render();
        $this-> _outputview($output, $page);      
	}
	public function searchAndViewAdministrasiPajak($dpa='',$tahun='',$jenis='',$katakunci='', $page='')
    {
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("319"))
            {
                $this->npkLogin = $session_data['npk'];
                if($dpa == 'non'){
				    $dpa = '';
                }if($tahun == 'non'){
					$tahun = '';
                }if($jenis == 'non'){
					$jenis = '';
                }if($katakunci == 'non'){
                    $katakunci = '';
                }
				$this->_AdministrasiPajak($dpa, $tahun, $jenis, $katakunci, 'view');
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}
	public function searchAndViewAdministrasiPajakInduk($dpa='',$tahun='',$jenis='',$katakunci='', $page='')
    {
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("319"))
            {
                $this->npkLogin = $session_data['npk'];
                if($dpa == 'non'){
				    $dpa = '';
                }if($tahun == 'non'){
					$tahun = '';
                }if($jenis == 'non'){
					$jenis = '';
                }if($katakunci == 'non'){
                    $katakunci = '';
                }
				$this->_AdministrasiPajakInduk($dpa, $tahun, $jenis, $katakunci, 'view');
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}
	public function _AdministrasiPajakInduk($dpa='',$tahun='',$jenis='',$katakunci='',$page='')
    {
        $session_data = $this->session->userdata('logged_in');

		$this->npkLogin = $session_data['npk'];
		$crud = new grocery_crud();
		$crud->set_subject('Administrasi Pajak');
		//$crud->set_theme('datatables');
		
        $crud->set_table('mstrdokumenpajakinduk');
        $crud->where('mstrdokumenpajakinduk.deleted','0');
        
		if($dpa != '')
		{
			$crud->where("mstrdokumenpajakinduk.dpa", $dpa);
		}
		if($tahun != '')
		{
			$crud->where('mstrdokumenpajakinduk.tahun',$tahun);				
		}
		if($jenis != '')
		{
			$crud->where('mstrdokumenpajakinduk.kodejenis',$jenis);
		}
		if($katakunci != '')
		{
			$crud->like('mstrdokumenpajakinduk.vendor', $katakunci);
		}

		switch($page)
			{
				case "add":
					break;
				case "view":
					$crud->unset_add();
					break;
				default:
					break;
			}	
		// $query = $this->db->get_where('globalparam',array('Name'=>'Admin_DokumenLegal'));
		// foreach($query->result() as $row)
		// {
		// 	$npkAdminDokumenLegal = $row->Value;
		// }
		// if($this->user->isAdmin($this->npkLogin)){
		// 	//Admin DPA
			
		// }else if(strpos($npkAdminDokumenLegal, $this->npkLogin) != false){
		// 	//berarti dia admin DokumenLegal
		// }else{
		// 	//brarti user biasa / PIC
		// 	$crud->unset_edit();
		// 	$crud->unset_delete();
		// }
		$crud->unset_edit();
		$crud->unset_delete();

		$crud->set_relation('JenisPPh','jenispph','JenisPPh',array('deleted' => '0'));
		$crud->set_relation('KodeJenisDokumen','jenisdokumenlegal','jenisdokumenlegal',array('deleted' => '0'));
		$crud->set_relation('CreatedBy','mstruser','Nama',array('deleted' => '0'));
		//$crud->set_relation('DokumenYangDiganti','mstrdokumenpajak','{NoBupot} DPA {DPA}',array('deleted' => '0'));
        
		// $crud->set_relation_n_n('PIC','picdokumenlegal','mstruser','KodeDokumenPajak','NPK','Nama');
		
		//$crud->set_relation('NPKYangDilimpahkan','mstruser','Nama',array('Deleted' => 0,'TanggalBerhenti' => '0000-00-00'));
		$state = $crud->getState();
		
		$crud->unset_back_to_list();
		// $crud->set_relation_n_n('PeraturanTerkait','peraturanterkaitdokumenlegal','mstrdokumenpajak','KodeDokumenPajakParent','KodeDokumenPajak','{NoBupot} DPA {DPA}', null, array('mstrdokumenpajak.deleted'=>0));

		$crud->columns('DPA', 'NamaFile', 'KodeJenisDokumen', 'Masa', 'Tahun', 'JenisPPh', 'JenisSPT', 'TanggalLaporan', 'CreatedBy', 'CreatedOn');
		$crud->display_as('KodeJenisDokumen','Jenis Induk');
		// $crud->callback_column('PeraturanTerkait', array($this,'_callback_peraturan_terkait'));
		$crud->callback_column('TanggalLaporan', array($this,'_callback_formatTanggalLaporan'));
		//$crud->callback_column('TanggalMulaiBerlaku', array($this,'_callback_formatTanggalMulaiBerlaku'));
		//$crud->callback_column('TanggalKadaluarsa', array($this,'_callback_formatTanggalKadaluarsa'));
		//$crud->callback_column('DokumenYangDiganti', array($this,'_callback_dokumen_yang_diganti'));
		$crud->callback_column('NamaFile', array($this,'_callback_show_Induk'));
        $output = $crud->render();
        $this-> _outputview($output, $page);      
	}	
	
    function _outputview($output = null, $page = '')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Administrasi Pajak',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		if($page == 'view')
		{
			$this->load->view('AdministrasiPajak/AdministrasiPajak_iframe',$data);
		}
		else
		{		
			$this->template->load('default','templates/CRUD_view',$data);
		}
    }
	
	function _delete_administrasiPajak($primary_key){
		return $this->db->update('mstrdokumenpajak',array('deleted' => '1'),array('KodeDokumenPajak' => $primary_key));
	}

	// function _callback_peraturan_terkait($value, $row){
	// 	$rs = $this->db->query("Select KodeDokumenPajak from peraturanterkaitdokumenlegal where KodeDokumenPajakParent = '$row->KodeDokumenPajak'");
	// 	if($result = $rs->result()){
	// 		$string = '';
	// 		$counter = 1;
	// 		foreach($result as $row2){
	// 			$rs2 = $this->db->query("Select NoBupot from mstrdokumenpajak where KodeDokumenPajak = '$row2->KodeDokumenPajak'");
	// 			$result2 = $rs2->result();
	// 			foreach($result2 as $row3){
	// 				if($counter == count($result)){
	// 					$string .= "<a target='_parent' href='".site_url('AdministrasiPajak/AdministrasiPajakController/read/'.$row2->KodeDokumenPajak)."'>$row3->NoBupot</a>";
	// 				}else{
	// 					$string .= "<a target='_parent' href='".site_url('AdministrasiPajak/AdministrasiPajakController/read/'.$row2->KodeDokumenPajak)."'>$row3->NoBupot</a>".", ";
	// 				}
	// 				$counter++;
	// 			}
	// 		}
	// 		return $string;
	// 	}
	
	// }

	function _callback_show_Bupot($value, $row)
	{
		return "<a target='_parent' href='".site_url('AdministrasiPajak/AdministrasiPajakController/read/'.$row->KodeDokumenPajak)."'>$value</a>";
	}

	function _callback_show_Nominal($value, $row)
	{
		return number_format($value);
	}
	function _callback_show_Induk($value, $row)
	{
		return "<a target='_parent' href='".site_url('AdministrasiPajak/AdministrasiPajakController/readInduk/'.$row->KodeDokumenPajakInduk)."'>$value</a>";
	}
	
	function _callback_formatTanggalLaporan($value, $row){
	
		return substr($value, 8,2).'-'.substr($value, 5,2).'-'.substr($value, 0,4);
	}
	function _after_insert_administrasiPajak($post_array,$primary_key)
	{
		$createDate = array(
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s')
		);
		fire_print('log',print_r($post_array,true));
		$this->db->update('mstrdokumenpajak',$createDate,array('KodeDokumenPajak'=>$primary_key));
		$dokumenSebelum = $this->AdministrasiPajak_model->dataDokumen($primary_key);
		$dokumenYangDiganti = $this->AdministrasiPajak_model->dataDokumen($dokumenSebelum[0]->DokumenYangDiganti);
		
		if($dokumenYangDiganti){
			$this->db->query('UPDATE mstrdokumenpajak SET Status = 0 where KodeDokumenPajak = '.$dokumenSebelum[0]->DokumenYangDiganti);
		}
		//$this->db->query('UPDATE mstrdokumenpajak SET OneMonthReminder = DATE_SUB(TanggalKadaluarsa, INTERVAL 30 DAY), ThreeMonthReminder = DATE_SUB(TanggalKadaluarsa, INTERVAL 90 DAY) where KodeDokumenPajak = '.$primary_key);
		return true;
	}
    function _after_update_administrasiPajak($post_array,$primary_key)
	{
		$updateDate = array(
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s')
		);
		fire_print('log',print_r($post_array,true));
		fire_print('log','pk dokumen_legal : '.$primary_key);
		$dokumenSebelum = $this->AdministrasiPajak_model->dataDokumen($primary_key);
		$dokumenYangDiganti = $this->AdministrasiPajak_model->dataDokumen($dokumenSebelum[0]->DokumenYangDiganti);
		
		if($dokumenYangDiganti){
			$this->db->query('UPDATE mstrdokumenpajak SET Status = 0 where KodeDokumenPajak = '.$dokumenSebelum[0]->DokumenYangDiganti);
		}
		$this->db->update('mstrdokumenpajak',$updateDate,array('KodeDokumenPajak'=>$primary_key));
		//$this->db->query('UPDATE mstrdokumenpajak SET OneMonthReminder = DATE_SUB(TanggalKadaluarsa, INTERVAL 30 DAY), ThreeMonthReminder = DATE_SUB(TanggalKadaluarsa, INTERVAL 90 DAY) where KodeDokumenPajak = '.$primary_key);
		return true;
    }
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */