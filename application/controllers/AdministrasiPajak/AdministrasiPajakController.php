<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class AdministrasiPajakController extends CI_Controller
{
	var $npkLogin;
	function __construct()
	{
		parent::__construct();
		$this->load->model('menu', '', TRUE);
		$this->load->model('user', '', TRUE);
		$this->load->model('ldarchive_model', '', TRUE);
		$this->load->library('grocery_crud');
	}

	public function index()
	{
		$session_data = $this->session->userdata('logged_in');
		if ($session_data) {
			if (check_authorized("319")) {
				$this->npkLogin = $session_data['npk'];
				//$this->_AdministrasiPajak();
				$session_data = $this->session->userdata('logged_in');
				$this->npkLogin = $session_data['npk'];
				$user = $this->user->dataUser($this->npkLogin);
				$this->load->helper(array('form', 'url'));
				$jenisDokumen = $this->ldarchive_model->getAllJenisDokumen();
				$jenisPPh = $this->ldarchive_model->getAllJenisPPh();

				$data = array(
					'title' => 'Search and View Admimnistrasi Pajak',
					'jenisDokumen' => $jenisDokumen,
					'jenisPPh' => $jenisPPh,
				);
				$this->load->helper(array('form','url'));
				$this->template->load('default','AdministrasiPajak/AdministrasiPajak_view',$data);
			}
		} else {
			redirect('login?u=' . substr($_SERVER["REQUEST_URI"], stripos($_SERVER["REQUEST_URI"], "index.php/") + 10), 'refresh');
		}
	}

	public function addDokumenPajak()
	{
		try {
			if ($this->session->userdata('logged_in')) {
				$session_data = $this->session->userdata('logged_in');
				$this->npkLogin = $session_data['npk'];
				$user = $this->user->dataUser($this->npkLogin);
				$this->load->helper(array('form', 'url'));
				$jenisDokumen = $this->ldarchive_model->getAllJenisDokumen();
				$jenisPPh = $this->ldarchive_model->getAllJenisPPh();
				$masa = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
				$tahun = date('Y')-2;
				
				$data = array(
					'title' => 'Tambah Bupot',
					'data' => $user,
					'jenisdokumen' => $jenisDokumen,
					'jenisPPh' => $jenisPPh,
					'masa' => $masa,
					'tahun' => $tahun
				);
				$this->template->load('default', 'AdministrasiPajak/UploadAP_view', $data);
			} else {
				redirect('login?u=' . substr($_SERVER["REQUEST_URI"], stripos($_SERVER["REQUEST_URI"], "index.php/") + 10), 'refresh');
			}
		} catch (Exception $e) {
			log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
			throw new Exception('Something really gone wrong', 0, $e);
		}
	}

	public function tambahSPTMasa(){
		// File upload configuration
		require 'application/third_party/PHPExcel/PHPExcel/IOFactory.php';
		$config['max_size']             = 1000000000;
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$uploadPath = './assets/uploads/files/DokumenPajak/';
		$config['upload_path'] = $uploadPath;
		$config['allowed_types'] = 'jpg|png|pdf|tif|tiff|text/plain|text/anytext|csv|text/x-comma-separated-values|text/comma-separated-values|application/octet-stream|application/vnd.ms-excel|application/x-csv|text/x-csv|text/csv|application/csv|application/excel|application/vnd.msexcel';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

        // If file upload form submitted
        if($this->input->post('Tambah') && !empty($_FILES['uploadSPT']['name']) && !empty($_FILES['uploadCSV']['name'])){
			$filesSPTCount = count($_FILES['uploadSPT']['name']);
			$excelFileName = $_FILES['uploadCSV']['name'];
			$excelInputFileName = $uploadPath . $excelFileName;
			$duplicate = false;
			$duplicateName = '';
			if($this->upload->do_upload('uploadCSV')){
				try
				{
					$inputfiletype = PHPExcel_IOFactory::identify($excelInputFileName);
					
					$objReader = PHPExcel_IOFactory::createReader($inputfiletype)->setDelimiter(";");;
					$objPHPExcel = $objReader->load($excelInputFileName);
					//  Get worksheet dimensions
					$sheet = $objPHPExcel->getActiveSheet(); 
					// $sheet = $objPHPExcel->getActiveSheet()->getRowIterator(); 
					// $sql = "INSERT INTO riwayatuploadcsvpajak VALUES ('',0,'".$excelFileName."','".$this->npkLogin."',now(),null,null)";
					// $this->db->query($sql);
					$highestRow = $sheet->getHighestRow(); 
					$highestColumn = $sheet->getHighestColumn();
					if($highestRow-1 == $filesSPTCount){
						//Jumlah baris di CSV sama dengan jumlah SPT yg diupload
						for($i = 0; $i < $filesSPTCount; $i++){
							//mengisi semua uploadSPT ke sptdata
							$sptData[$i]['file_name']=$_FILES['uploadSPT']['name'][$i];
							$temp = explode('-',$_FILES['uploadSPT']['name'][$i]);
							$temp[1] = explode('.',$temp[1])[0];
							$temp[1] = str_pad($temp[1],6,"0",0);
							$temp[0] = str_replace('_','/',$temp[0]);
							$sptData[$i]['aligned_name'] = $temp[1].'/'.$temp[0];
						}

						$jumlahFileBupotSesuai = 0;
						for ($row = 2; $row <= $highestRow; $row++)
						{
							$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
							$NoBupotCSV = $rowData[0][7];
							$indexFileBupot = -1;
							for($x = 0;$x<count($sptData);$x++){
								if($NoBupotCSV == $sptData[$x]['aligned_name']){
									$indexFileBupot = $x;
									$jumlahFileBupotSesuai++;
								}
							}
						}
						if($jumlahFileBupotSesuai == $filesSPTCount){
							//boleh upload semua SPT detail + siapin data csv ke array utk insert ke db
							$berhasilUploadSemua = true;
							for($i = 0; $i < $filesSPTCount; $i++){
								$_FILES['file']['name']     = $_FILES['uploadSPT']['name'][$i];
								$_FILES['file']['type']     = $_FILES['uploadSPT']['type'][$i];
								$_FILES['file']['tmp_name'] = $_FILES['uploadSPT']['tmp_name'][$i];
								$_FILES['file']['error']    = $_FILES['uploadSPT']['error'][$i];
								$_FILES['file']['size']     = $_FILES['uploadSPT']['size'][$i];
								if(file_exists($uploadPath.$_FILES['uploadSPT']['name'][$i])==0){
									if($this->upload->do_upload('file')){
										// Uploaded file data
										$fileData = $this->upload->data();
										$uploadData[$i]['file_name'] = $fileData['file_name'];
										$uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
									}else{
										foreach ($uploadData as $row) {
											unlink($uploadPath.$fileData['file_name']);
											$berhasilUploadSemua = false;
											echo $berhasilUploadSemua;
											//hapus semua yg udah ke upload jika gagal upload
										}
									}
								}else{
									$berhasilUploadSemua = false;
									$duplicateName .= $uploadPath.$_FILES['uploadSPT']['name'][$i].'<br/>';
								}
							}
							if($berhasilUploadSemua){
								$dataDB = array();
								for ($row = 2; $row <= $highestRow; $row++)
								{ 
									$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
									$NoBupotCSV = $rowData[0][7];
									$TanggalPelaporan = $rowData[0][8];
									$temp = explode('/', $TanggalPelaporan);
									$TanggalPelaporan = $temp[2]."-".$temp[1]."-".$temp[0];
									$Vendor = $rowData[0][5];
									$Nominal = $rowData[0][54];
									$indexFileBupot = -1;
									for($x = 0;$x<count($sptData);$x++){
										if($NoBupotCSV == $sptData[$x]['aligned_name']){
											$indexFileBupot = $x;
										}
									}
									if($indexFileBupot > -1){
										$DPA = 1;
										if(strrpos($NoBupotCSV,'DPA2') > 0){
											$DPA = 2;
										}
										$dataDB[$indexFileBupot]['Deleted'] = 0;
										$dataDB[$indexFileBupot]['DPA'] = $DPA;
										$dataDB[$indexFileBupot]['KodeJenis'] = 4; //Kode Jenis BUPOT
										$dataDB[$indexFileBupot]['Masa'] = $_POST['cmbMasa'];
										$dataDB[$indexFileBupot]['Tahun'] = $_POST['cmbTahun'];
										$dataDB[$indexFileBupot]['JenisSPT'] = 'Masa';
										$dataDB[$indexFileBupot]['JenisPPh'] = $_POST['cmbJenisPPH'];
										$dataDB[$indexFileBupot]['TanggalLaporan'] = $TanggalPelaporan;
										$dataDB[$indexFileBupot]['NoBupot'] = $NoBupotCSV;
										$dataDB[$indexFileBupot]['Vendor'] = $Vendor;
										$dataDB[$indexFileBupot]['Nominal'] = $Nominal;
										$dataDB[$indexFileBupot]['FilePath'] = $uploadPath.$sptData[$indexFileBupot]['file_name'];
										$dataDB[$indexFileBupot]['CreatedBy'] = $this->npkLogin;
										$dataDB[$indexFileBupot]['CreatedOn'] = date('Y-m-d h:i:s');
										$dataDB[$indexFileBupot]['UpdatedBy'] = null;
										$dataDB[$indexFileBupot]['UpdatedOn'] = null;
									}
								}
								$berhasilInsertSemua = true;
								foreach($dataDB as $row){
									$sqlInsertToMstrDokumenPajak = "INSERT INTO MSTRDOKUMENPAJAK (Deleted,DPA,KodeJenis,Masa,Tahun,JenisSPT,JenisPPh,TanggalLaporan,NoBupot,Vendor,Nominal,FilePath,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn)
									values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
									$query = $this->db->query($sqlInsertToMstrDokumenPajak, array($row['Deleted'], $row['DPA'], $row['KodeJenis'], $row['Masa'], $row['Tahun'], $row['JenisSPT'], $row['JenisPPh'], $row['TanggalLaporan'], $row['NoBupot'], $row['Vendor'], $row['Nominal'], $row['FilePath'], $row['CreatedBy'], $row['CreatedOn'], $row['UpdatedBy'], $row['UpdatedOn']));
									if(!$query){
										$berhasilInsertSemua = false;
									}
								}
								if($berhasilInsertSemua){
									 redirect('AdministrasiPajak/AdministrasiPajakController/');
								}else{
									echo 'Gagal insert semua';
								}
							}else{
								unlink($excelInputFileName);
								echo 'Tidak berhasil upload semua karena ada duplikat, file yg duplikat adalah <br/>';
								echo $duplicateName;
								die();
							}
						}else{
							unlink($excelInputFileName);
							echo "Jumlah file Sesuai $jumlahFileBupotSesuai <br/>";
							echo "Jumlah file SPT $filesSPTCount <br/>";
							echo 'Terdapat file bupot yang tidak ada detailnya';
							die();
						}
					}else{
						unlink($excelInputFileName);
						echo "Jumlah file $filesSPTCount <br/>";
						echo "Jumlah row $highestRow <br/>";
						echo 'Pastikan jumlah detail dan SPT yg diupload sama';
						die();
					}
					
				}
				catch(Exception $e)
				{
					die('Error loading file "'.pathinfo($excelInputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
				}
			}else{
				$error = array('error' => $this->upload->display_errors());
				var_dump($error);
				die();
			}
		}
	}

	public function read($KodeDokumen = "")
	{
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorized("319"))
			{
				$detail = $this->ldarchive_model->dataDokumenPajak($KodeDokumen);
				// $related = array();
				// if($detail = $this->ldarchive_model->dataDokumen($KodeDokumen)){
				// 	foreach ($detail as $value) {
				// 		$related = ($this->ldarchive_model->dataDokumen($value->DokumenYangDiganti));
				// 	}
				// }
				$data = array(
					'title' => 'Dokumen Legal',
					'detail' => $detail
			  	);
				$this->npkLogin = $session_data['npk'];
				$this->template->load('default','AdministrasiPajak/AdministrasiPajak_read',$data);
			}
		}else{
			redirect('login','refresh');
		}
	}
	public function readInduk($KodeDokumen = "")
	{
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorized("319"))
			{
				$detail = $this->ldarchive_model->dataDokumenInduk($KodeDokumen);
				
				// $related = array();
				// if($detail = $this->ldarchive_model->dataDokumen($KodeDokumen)){
				// 	foreach ($detail as $value) {
				// 		$related = ($this->ldarchive_model->dataDokumen($value->DokumenYangDiganti));
				// 	}
				// }
				$data = array(
					'title' => 'Dokumen Induk',
					'detail' => $detail
			  	);
				$this->npkLogin = $session_data['npk'];
				$this->template->load('default','AdministrasiPajak/AdministrasiPajak_read',$data);
			}
		}else{
			redirect('login','refresh');
		}
	}
	public function spt_ap($dpa = '', $tahun = '', $jenisDok = '', $jenisSPT = '', $jenisPPH = '', $masa = '', $katakunci = '', $page = '')
	{
		$session_data = $this->session->userdata('logged_in');

		$this->npkLogin = $session_data['npk'];
		$crud = new grocery_crud();
		$crud->set_subject('Administrasi Pajak');
		//$crud->set_theme('datatables');

		$crud->set_table('mstrdokumenpajak');
		$crud->where('mstrdokumenpajak.deleted', '0');

		if ($dpa != '') {
			$crud->where("mstrdokumenpajak.dpa", $dpa);
		}
		if ($tahun != '') {
			$crud->where('mstrdokumenpajak.tahun', $tahun);
		}
		if ($jenisDok != '') {
			$crud->where('mstrdokumenpajak.kodejenis', $jenisDok);
		}
		if ($jenisSPT != '') {
			$crud->where('mstrdokumenpajak.jenisSPT', $jenisSPT);
		}
		if ($jenisPPH != '') {
			$crud->where('mstrdokumenpajak.jenisPPH', $jenisPPH);
		}
		if ($masa != '') {
			$crud->where('mstrdokumenpajak.masa', $masa);
		}
		if ($katakunci != '') {
			$crud->like('mstrdokumenpajak.keyword', $katakunci);
		}

		switch ($page) {
			case "add":
				addDokumenPajak();
				break;
			case "view":
				$crud->unset_add();
				break;
			default:
				break;
		}

		$state = $crud->getState();
		if ($state == 'add') {
			$this->addDokumenPajak();
		}
		$crud->unset_back_to_list();

		$output = $crud->render();
		$this->_outputview($output, $page);
	}
	public function induk_ap($dpa = '', $tahun = '', $jenisDok = '', $jenisSPT = '', $jenisPPH = '', $masa = '', $katakunci = '', $page = '')
	{
		$session_data = $this->session->userdata('logged_in');

		$this->npkLogin = $session_data['npk'];
		$crud = new grocery_crud();
		$crud->set_subject('Administrasi Pajak');
		//$crud->set_theme('datatables');

		$crud->set_table('mstrdokumenpajakinduk');
		$crud->where('mstrdokumenpajakinduk.deleted', '0');

		if ($dpa != '') {
			$crud->where("mstrdokumenpajakinduk.dpa", $dpa);
		}
		if ($tahun != '') {
			$crud->where('mstrdokumenpajakinduk.tahun', $tahun);
		}
		if ($jenisDok != '') {
			$crud->where('mstrdokumenpajakinduk.kodejenis', $jenisDok);
		}
		if ($jenisSPT != '') {
			$crud->where('mstrdokumenpajakinduk.jenisSPT', $jenisSPT);
		}
		if ($jenisPPH != '') {
			$crud->where('mstrdokumenpajakinduk.jenisPPH', $jenisPPH);
		}
		if ($masa != '') {
			$crud->where('mstrdokumenpajakinduk.masa', $masa);
		}
		if ($katakunci != '') {
			$crud->like('mstrdokumenpajakinduk.keyword', $katakunci);
		}

		switch ($page) {
			case "add":
				addDokumenPajakInduk();
				break;
			case "view":
				$crud->unset_add();
				break;
			default:
				break;
		}

		$state = $crud->getState();
		if ($state == 'add') {
			$this->addDokumenPajakInduk();
		}
		$crud->unset_back_to_list();

		$output = $crud->render();
		$this->_outputview($output, $page);
	}
	public function addDokumenPajakInduk()
	{
		try {
			if ($this->session->userdata('logged_in')) {
				$session_data = $this->session->userdata('logged_in');
				$this->npkLogin = $session_data['npk'];
				$user = $this->user->dataUser($this->npkLogin);
				$this->load->helper(array('form', 'url'));
				$jenisDokumen = $this->ldarchive_model->getAllJenisDokumenInduk();
				$jenisPPh = $this->ldarchive_model->getAllJenisPPh();
				$masa = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
				$tahun = date('Y')-2;
				
				$data = array(
					'title' => 'Tambah Bupot',
					'data' => $user,
					'jenisDokumen' => $jenisDokumen,
					'jenisPPh' => $jenisPPh,
					'masa' => $masa,
					'tahun' => $tahun
				);
				$this->template->load('default', 'AdministrasiPajak/UploadInduk_view', $data);
			} else {
				redirect('login?u=' . substr($_SERVER["REQUEST_URI"], stripos($_SERVER["REQUEST_URI"], "index.php/") + 10), 'refresh');
			}
		} catch (Exception $e) {
			log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
			throw new Exception('Something really gone wrong', 0, $e);
		}
	}
	public function tambahInduk(){
		// File upload configuration
		require 'application/third_party/PHPExcel/PHPExcel/IOFactory.php';
		$config['max_size']             = 1000000000;
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$uploadPath = './assets/uploads/files/DokumenPajak/Induk/';
		$config['upload_path'] = $uploadPath;
		$config['allowed_types'] = 'pdf';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		$duplicate = false;
		$duplicateName = '';
		$dataDB = array();

        // If file upload form submitted
        if($this->input->post('Tambah') && !empty($_FILES['uploadPDF']['name'])){
			$filesSPTCount = count($_FILES['uploadPDF']['name']);
			for($i = 0; $i < $filesSPTCount; $i++){
				$_FILES['file']['name']     = $_FILES['uploadPDF']['name'];
				$_FILES['file']['type']     = $_FILES['uploadPDF']['type'];
				$_FILES['file']['tmp_name'] = $_FILES['uploadPDF']['tmp_name'];
				$_FILES['file']['error']    = $_FILES['uploadPDF']['error'];
				$_FILES['file']['size']     = $_FILES['uploadPDF']['size'];
				
				if(!file_exists($uploadPath.$_FILES['file']['name'])){
					if($this->upload->do_upload('file')){
						// Uploaded file data
						$dataDB[$i]['Deleted'] = 0;
						$dataDB[$i]['NamaFile'] = $_FILES['file']['name'];
						$dataDB[$i]['DPA'] = $_POST['cmbDPA']; //Kode Jenis BUPOT
						$dataDB[$i]['KodeJenisDokumen'] = $_POST['cmbJenisInduk'];
						$dataDB[$i]['Masa'] = $_POST['cmbMasa'];
						$dataDB[$i]['Tahun'] = $_POST['cmbTahun'];
						$dataDB[$i]['JenisPPh'] = $_POST['cmbJenisPPH'];
						$dataDB[$i]['JenisSPT'] = $_POST['cmbJenisSPT'];
						$dataDB[$i]['TanggalLaporan'] = $_POST['tanggalPelaporan'];
						$dataDB[$i]['FilePath'] = $uploadPath.$_FILES['uploadPDF']['name'];
						$dataDB[$i]['CreatedBy'] = $this->npkLogin;
						$dataDB[$i]['CreatedOn'] = date('Y-m-d h:i:s');
					}else{
						$error = array('error' => $this->upload->display_errors());
						var_dump($error);
						die();
					}
				}else{
					$berhasilUploadSemua = false;
					$duplicateName .= $uploadPath.$_FILES['uploadPDF']['name'].'<br/>';
				}
			}
			
						
			$berhasilInsertSemua = true;
			foreach($dataDB as $row){
				$sqlInsertToMstrDokumenPajak = "INSERT INTO MstrDokumenPajakInduk (
					Deleted, DPA, NamaFile, KodeJenisDokumen, Masa, Tahun, JenisPPh, JenisSPT, TanggalLaporan, FilePath, CreatedBy, CreatedOn
				) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				$query = $this->db->query($sqlInsertToMstrDokumenPajak, array($row['Deleted'], $row['DPA'], $row['NamaFile'], $row['KodeJenisDokumen'], $row['Masa'], $row['Tahun'], $row['JenisPPh'], $row['JenisSPT'], $row['TanggalLaporan'], $row['FilePath'], $row['CreatedBy'], $row['CreatedOn']));
				
				if(!$query){
					$berhasilInsertSemua = false;
				}
			}
			if($berhasilInsertSemua){
				redirect('AdministrasiPajak/AdministrasiPajakController/');
			}else{
				echo 'Gagal insert semua';
			}
		}else{
			$error = array('error' => $this->upload->display_errors());
			var_dump($error);
			die();
		}
	}
	function _outputview($output = null, $page = '')
	{
		$session_data = $this->session->userdata('logged_in');
		$data = array(
			'title' => 'Dokumen Legal',
			'body' => $output
		);
		$this->load->helper(array('form', 'url'));
		if ($page == 'view') {
			$this->load->view('LDArchive/LDArchive_iframe', $data);
		} else {
			$this->template->load('default', 'templates/CRUD_view', $data);
		}
	}

}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
