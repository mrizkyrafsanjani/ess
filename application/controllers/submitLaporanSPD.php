<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');

class SubmitLaporanSPD extends CI_Controller {

	 function __construct()
	 {
	   parent::__construct();
	   $this->load->model('trkSPD','',TRUE);
	 }

	 function index()
	 {
	   //This method will have the credentials validation
	   $this->load->library('form_validation');

	   $this->form_validation->set_rules('txtTanggalBerangkat', 'Tanggal Berangkat', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtTanggalKembali', 'Tanggal Kembali', 'trim|required|xss_clean');
	   
	   $this->form_validation->set_rules('txtKetUangSaku', 'Keterangan Uang Saku', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariUangSaku', 'Jumlah Hari Uang Saku', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetUangMakan', 'Keterangan Uang Makan', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianUangMakan', 'Beban Harian Uang Makan', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariUangMakan', 'Jumlah Hari Uang Makan', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetHotel', 'Keterangan Hotel', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianHotel', 'Beban Harian Hotel', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariHotel', 'Jumlah Hari Hotel', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetTaksi', 'Keterangan Taksi', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianTaksi', 'Beban Harian Taksi', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetAirport', 'Keterangan Airport', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianAirport', 'Beban Harian Airport', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetLain1', 'Keterangan Lain-lain 1', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain1', 'Beban Harian Lain-lain 1', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetLain2', 'Keterangan Lain-lain 2', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain2', 'Beban Harian Lain-lain 2', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetLain3', 'Keterangan Lain-lain 3', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain3', 'Beban Harian Lain-lain 3', 'trim|xss_clean');
	   
	   /*
	   $this->form_validation->set_rules('txtKetUangSaku', 'Keterangan Uang Saku', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariUangSaku', 'Jumlah Hari Uang Saku', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetUangMakan', 'Keterangan Uang Makan', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianUangMakan', 'Beban Harian Uang Makan', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariUangMakan', 'Jumlah Hari Uang Makan', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetHotel', 'Keterangan Hotel', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianHotel', 'Beban Harian Hotel', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariHotel', 'Jumlah Hari Hotel', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetTaksi', 'Keterangan Taksi', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianTaksi', 'Beban Harian Taksi', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetAirport', 'Keterangan Airport', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianAirport', 'Beban Harian Airport', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetLain1', 'Keterangan Lain-lain 1', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain1', 'Beban Harian Lain-lain 1', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetLain2', 'Keterangan Lain-lain 2', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain2', 'Beban Harian Lain-lain 2', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetLain3', 'Keterangan Lain-lain 3', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain3', 'Beban Harian Lain-lain 3', 'trim|numeric|xss_clean');
	   */
	 
	   if($this->form_validation->run() == false)
	   {
		 //Field validation failed.  User redirected to login page
		 $this->load->view('laporanSPD_view');
		 
	   }
	   else
	   {	 
		 //Go to private area
		 if($this->input->post('submitLaporanSPD')){
			$noSPD = $this->input->post('txtNoSPD');
			$UM = $this->input->post('txtUangMuka');
			if($UM<>0){
			if($this->trkSPD->insertLaporan($noSPD) && $this->trkSPD->insertDtlBiayaLaporan($noSPD) && $this->trkSPD->UpdateUangMuka($noSPD)){
				if ($this->trkSPD->cekAdaPelimpahanWewenang($noSPD))
				{$this->trkSPD->UpdateKeteranganPW($noSPD);}
				redirect('cetakLaporanSPD?NoSPD='.$noSPD.'&msg= Sukses Update','refresh');
			}else{
				echo 'gagal insert';
			}
		}else{
			if($this->trkSPD->insertLaporan($noSPD) && $this->trkSPD->insertDtlBiayaLaporan($noSPD)){
				if ($this->trkSPD->cekAdaPelimpahanWewenang($noSPD))
				{$this->trkSPD->UpdateKeteranganPW($noSPD);}
				redirect('cetakLaporanSPD?NoSPD='.$noSPD.'&msg= Sukses Update','refresh');
			}else{
				echo 'gagal';
			}
		}
	}
	   }
	}
}
?>