<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Cuti extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('user','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('cuti_model','',TRUE);
		$this->load->model('cutiJenisTidakHadir_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("38"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_cuti();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function viewUser($tahun='',$bulan='',$nama='',$NPKuser='',$KodeUserTask='')
	{
		if($KodeUserTask == 'non')
			$KodeUserTask = '';
		if($nama == 'non')
			$nama = '';
		if($tahun == 'non')
			$tahun = date('Y');
		if($bulan == 'non')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("37"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_cuti('viewUser',$nama,$tahun,$bulan,$KodeUserTask);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function viewAdmin($tahun='',$bulan='',$nama='non')
	{
		if($nama == 'non')
			$nama = 'xxx';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("29"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_cuti('viewAdmin',$nama,$tahun,$bulan);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function _cuti($page='',$nama='',$tahun='',$bulan='',$KodeUserTask='')
    {
		$currUserGrupCuti = $this->findUserGrupCuti();
		$crud = new grocery_crud();
		$crud->set_subject('Cuti Izin Sakit');
		//$crud->set_theme('datatables');
		
        $crud->set_table('cuti');
		//$crud->set_relation('KodeJenisTidakHadir','jenistidakhadir','Deskripsi',array('Deleted' => '0'));
		$crud->set_relation_n_n('JenisTidakHadir','cutijenistidakhadir','jenistidakhadir','KodeCuti','KodeJenisTidakHadir','Deskripsi');
		$crud->set_relation('NPK','mstruser','Nama',array('Deleted'=>'0'));
		$crud->where('cuti.deleted','0');
		
		if($nama != '')
		{
			$crud->like('cuti.NPK',$nama);
		}
		if($tahun != '')
		{
			$crud->where('YEAR(cuti.TanggalMulai)',$tahun);
		}
		if($bulan != '' && $bulan != 'all')
		{
			$crud->where('MONTH(cuti.TanggalMulai)', $bulan);
		}
		
		if($KodeUserTask != '')
		{				
			$lembur = $this->cuti_model->getCuti($KodeUserTask);
			foreach($lembur as $rowCuti){
				$kodeCuti = $rowCuti->KodeCuti;
			}
			fire_print('log','KodeCuti: '.$kodeCuti);
			$crud->where('cuti.KodeCuti',$kodeCuti);
		}
		
		switch($page)
		{
			case "viewAdmin":
				$crud->columns('NPK','TanggalMulai','TanggalSelesai','JumlahHari','JenisTidakHadir','StatusApproval');
				$crud->unset_add();
				$crud->unset_edit();
				$crud->unset_print();
				$crud->unset_read();
				$crud->unset_delete();
				break;
			case "viewUser":
				$crud->where('cuti.NPK',$this->npkLogin);
				$crud->set_theme('datatables');
				$crud->columns('TanggalMulai','TanggalSelesai','JumlahHari','JenisTidakHadir','StatusApproval');
				$crud->unset_add();
				$crud->unset_edit();
				//$crud->unset_print();
				$crud->unset_read();
				$crud->unset_export();
				//$crud->add_action('Ubah','','','ui-icon-image',array($this,'callback_action_ubah'));
				if($currUserGrupCuti == "CutiHead" )
				{
					$crud->add_action('Print','','','ui-icon-print',array($this,'callback_action_cetak'));
				}
				else
				if($currUserGrupCuti == "CutiSpesial")
				{ 	
					$crud->add_action('Print','','','ui-icon-print',array($this,'callback_action_cetak_spesial'));
				}
				else
				{ //buat karyawan biasa 
					$crud->add_action('Print','','','ui-icon-print',array($this,'callback_action_cetak_nonhead'));
				}
				break;
		}
		
		//$crud->columns('Nama','NPK','Departemen','JenisTidakHadir','Lokasi','TanggalMulai','TanggalSelesai','JumlahHari','SisaCutiTahunan','SisaCutiBesar');
		$crud->fields('KodeCuti','NPK','StatusApproval','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy','NamaKaryawan','Departemen','JenisTidakHadir','Lokasi','TanggalMulai','TanggalSelesai','JumlahHari','SisaCutiTahunan','SisaCutiBesar');
		$crud->edit_fields('KodeCuti','NPK','StatusApproval','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy','NamaKaryawan','Departemen','JenisTidakHadir','Lokasi','TanggalMulai','TanggalSelesai','JumlahHari','SisaCutiTahunan','SisaCutiBesar','Catatan');
		
		$crud->display_as('NamaKaryawan','Nama');
		$crud->display_as('Deskripsi','Jenis Ketidakhadiran');
		$crud->display_as('TanggalMulai','Mulai Tanggal');
		$crud->display_as('TanggalSelesai','Sampai Tanggal');
		$crud->display_as('SisaCutiTahunan','Sisa Cuti Tahunan');
		$crud->display_as('SisaCutiBesar','Sisa Cuti Besar');
		$crud->display_as('JumlahHari','Jumlah Hari');
		$crud->display_as('JenisTidakHadir','Jenis Ketidakhadiran');
		
		$crud->field_type('NPK','invisible');
		$crud->field_type('KodeCuti','invisible');		
		$crud->field_type('StatusApproval','invisible');
		$crud->field_type('CreatedOn','invisible');
		$crud->field_type('CreatedBy','invisible');
		$crud->field_type('UpdatedOn','invisible');
		$crud->field_type('UpdatedBy','invisible');
		
		$crud->required_fields('JenisTidakHadir','TanggalMulai','TanggalSelesai');
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		//$crud->callback_column('TanggalMulai',array($this,'_callback_tanggalmulai'));
		//$crud->callback_column('TanggalSelesai',array($this,'_callback_tanggalselesai'));
		$crud->callback_column('JumlahHari',array($this,'_callback_column_jumlahhari'));
		$crud->callback_column('StatusApproval',array($this,'_callback_statuscuti'));
		$crud->callback_edit_field('Catatan',array($this,'_callback_catatan'));
		
		$crud->callback_field('KodeCuti',array($this,'_callback_cuti'));
		$crud->callback_field('NamaKaryawan',array($this,'_callback_nama'));
		$crud->callback_field('NPK',array($this,'_callback_npk'));
		$crud->callback_field('Departemen',array($this,'_callback_departemen'));
		$crud->callback_field('Lokasi',array($this,'_callback_lokasi'));
		$crud->callback_field('JumlahHari',array($this,'_callback_jumlahhari'));
		$crud->callback_field('SisaCutiTahunan',array($this,'_callback_sisacutitahunan'));
		$crud->callback_field('SisaCutiBesar',array($this,'_callback_sisacutibesar'));
		
		$crud->callback_insert(array($this,'_insert'));		
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete_cuti'));
		$crud->unset_back_to_list();
		$js = "
		<script>
			var spans = document.getElementsByTagName('span');					
			for(var i=0;i<spans.length;i++)
			{
				if(spans[i].innerHTML == '&nbsp;Edit'){
					//alert(spans[i].visible);
					spans[i].style.display = 'none';
				}
				if(spans[i].innerHTML == '&nbsp;Delete'){				
					spans[i].innerHTML = 'Hapus';		
				}
			}
			
			$(document).ready(function() {
				var TanggalMulai = $('#field-TanggalMulai');
				var TanggalSelesai = $('#field-TanggalSelesai');
				var JumlahHari = $('#field-JumlahHari');
				TanggalSelesai.change(function(){
					//var selisihHari = DateDiff(new Date(TanggalSelesai.val()),new Date(TanggalMulai.val()))+1;
					$.ajax({
					  url: '" .base_url()."index.php/Cuti/Cuti/getHariKerja',
					  type: 'POST',
					  data: {tanggalmulai : TanggalMulai.val(), tanggalselesai: TanggalSelesai.val()},
					  dataType: 'html'
					}).done(function(response){
						JumlahHari.val(response);
					});
				});
			}); 
			
			//source: http://stackoverflow.com/questions/12003660/javascript-datediff
			function DateDiff(date1, date2) {
				var datediff = date1.getTime() - date2.getTime(); //store the getTime diff - or +
				return (datediff / (24*60*60*1000)); //Convert values to -/+ days and return value      
			}
		</script>";
		
        $output = $crud->render();
		$output->output.=$js;   
        $this-> _outputview($output,$page);
    }
 
    function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Cuti Izin Sakit',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		fire_print('log','page cuti: '.$page);
		if($page!='')
		{
			$this->load->view('Cuti/cutiSimple_view',$data);
		}
		else
		{
			$this->template->load('default','templates/CRUD_view',$data);
		}
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _callback_nama($value= '', $primary_key = null)
	{
		$datauser = $this->user->findUserByNPK($this->npkLogin);
		$nama = '';
		foreach($datauser as $dt)
		{
			$nama = $dt->Nama;
		}
		//return '<input type="text" style="border:0;" value="'.$nama.' ( '.$this->npkLogin .' )" readonly>';
		return '<input type="text" style="border:0;" value="'.$nama.'" readonly>';
	}
	
	function _callback_npk($value= '', $primary_key = null)
	{
		return '<input type="text" style="border:0;" value="'.$this->npkLogin.'" readonly>';
	}
	
	function _callback_departemen($value= '', $primary_key = null)
	{
		$datauser = $this->user->get_departemen_by_npk($this->npkLogin);
		$departemen = '';
		foreach($datauser as $dt)
		{
			$departemen = $dt->departemen;
		}
		return '<input type="text" style="border:0;" value="'.$departemen.'" readonly>';
	}
	
	function _callback_lokasi($value='', $primary_key = null)
	{
		$selectedDN = '';
		$selectedLN = '';
		
		switch($value)
		{
			case "DK": $selectedDN = "checked"; break;
			case "LK" : $selectedLN = "checked"; break;
		}
		
		return ' <input type="radio" name="Lokasi" value="DK" '.$selectedDN.' /> Dalam Kota
			<input type="radio" name="Lokasi" value="LK" '.$selectedLN.' /> Luar Kota';
	}
	
	function _callback_jumlahhari($value= '', $primary_key = null)
	{
		return '<input type="text" id="field-JumlahHari" style="border:0;" value="" readonly>';
	}
	
	function _callback_sisacutitahunan($value='',$primary_key=null)
	{
		return '<input type="text" id="field-SisaCutiTahunan" style="border:0;" value="" readonly>';
	}
	
	function _callback_sisacutibesar($value='',$primary_key=null)
	{
		return '<input type="text" id="field-SisaCutiBesar" style="border:0;" value="" readonly>';
	}
	
	function _callback_tanggalmulai($value,$row)
	{
		$data = $this->cutiJenisTidakHadir_model->getMinMaxTanggalCuti($row->KodeCuti);
		fire_print('log','data tanggal min max:'.print_r($data,true));
		return $data['TanggalMulai'];
	}
	function _callback_tanggalselesai($value,$row)
	{
		$data = $this->cutiJenisTidakHadir_model->getMinMaxTanggalCuti($row->KodeCuti);
		fire_print('log','data tanggal min max:'.print_r($data,true));
		return $data['TanggalSelesai'];
	} 
	
	function _callback_column_jumlahhari($value,$row)
	{
		//$jumlahHari = $this->getHariKerja($row->TanggalMulai,$row->TanggalSelesai);
		$dataCutiUser = $this->cuti_model->getCutiBreakDown($row->KodeCuti);
		$dataCutiUserDetail = array();
		if($dataCutiUser)
		{
			foreach($dataCutiUser as $row)
			{	
				$dataMinMaxTanggalCuti = $this->cutiJenisTidakHadir_model->getMinMaxTanggalCutiDetail($row->KodeCutiJenisTidakHadir);						
				$TanggalMulaiDetail = $dataMinMaxTanggalCuti['TanggalMulai'];
				$TanggalSelesaiDetail = $dataMinMaxTanggalCuti['TanggalSelesai'];						
				$jumlahHariDetail = $this->cuti_model->getHariKerja($TanggalMulaiDetail,$TanggalSelesaiDetail);
				$dataJenisTidakHadirDetail = $this->cuti_model->getJenisTidakHadirDetail($row->KodeCutiJenisTidakHadir);
				
				
				$dataCutiUserDetail[] = array(
					'KodeCuti' => $row->KodeCuti,
					'KodeCutiJenisTidakHadir' => $row->KodeCutiJenisTidakHadir,
					'KodeJenisTidakHadir' => $row->KodeJenisTidakHadir,
					'TahunCutiYangDipakai' => $row->TahunCutiYangDipakai,
					'TanggalMulaiDetail' => $TanggalMulaiDetail,
					'TanggalSelesaiDetail' => $TanggalSelesaiDetail,
					'jumlahHariDetail' => $jumlahHariDetail,
					'dataJenisTidakHadirDetail' => $dataJenisTidakHadirDetail
				);		
			}
		}
		$jumlahHari = 0;
		for($i=0;$i<count($dataCutiUserDetail);$i++){
			$jumlahHari += $dataCutiUserDetail[$i]['jumlahHariDetail'];
		}
		fire_print('log','Hari mulai cuti:'.$row->TanggalMulai.',hari selesai:'.$row->TanggalSelesai);
		fire_print('log','jumlah hari kerja column:'.$jumlahHari);
		return $jumlahHari;
	}
	
	function callback_action_ubah($primary_key,$row)
	{		
		if($row->StatusApproval == "Rencana Declined")
		{
			$data = $this->db->get_where('dtltrkrwy',array('NoTransaksi'=>$primary_key));
			$kodeUserTask = $data->row(1)->KodeUserTask;
			return site_url('cuti/CutiController/EditCuti/'.$kodeUserTask);
		}
		else
		{
			return site_url('cuti/CutiController/tidakbisa_ubah');
		}
	}
	
	function _insert($post_array)
	{
		$this->db->trans_begin();
		$dataCuti = array(
			"KodeCuti" => generateNo('CT'),
			"NPK" => $this->npkLogin,
			"StatusApproval" => 'PEP',
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s'),
			"Lokasi" => $post_array['Lokasi'],
			"TanggalMulai" => $post_array['TanggalMulai'],
			"TanggalSelesai" => $post_array['TanggalSelesai']			
		);
	
		$this->db->insert('cuti',$dataCuti);
		
		foreach($post_array['JenisTidakHadir'] as $rw)
		{
			$dataJenisTidakHadir = array(
				"KodeCuti" => $dataCuti['KodeCuti'],
				"KodeJenisTidakHadir" => $rw,
				"CreatedBy" => $this->npkLogin,
				"CreatedOn" => date('Y-m-d H:i:s')				
			);
			
			$this->db->insert('cutijenistidakhadir',$dataJenisTidakHadir);
		}
		
		$KodeCuti = $dataCuti['KodeCuti'];
		$KodeUserTask = generateNo('UT');
		$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeCuti,$KodeUserTask,"CT");
		
		if(!$hasilTambahUserTask)
		{
			//echo('gagal simpan usertask lembur');
			fire_print('log','insert cuti rollback');
			$this->db->trans_rollback();
			return false;
		}else{
			fire_print('log','insert cuti commit');
			$this->db->trans_commit();
			return true;
		}
	}
	
	/* function _insert($post_array)
	{	
		$post_array['KodeCuti'] = generateNo('CT');
		$post_array['NPK'] = $this->npkLogin;
		$post_array['StatusApproval'] = 'PEP'; //Pending Plan
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		fire_print('log','NPK insert cuti:'.$post_array['NPK']);
		fire_print('log','Kode cuti:'.$post_array['KodeCuti']);
		
		$this->db->trans_begin();
		$this->db->insert('cuti',$post_array);
		
		$KodeCuti = $post_array['KodeCuti'];
		$KodeUserTask = generateNo('UT');
		$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeCuti,$KodeUserTask,"CT");
		
		if(!$hasilTambahUserTask)
		{
			//echo('gagal simpan usertask lembur');
			fire_print('log','insert cuti rollback');
			$this->db->trans_rollback();
			return false;
		}else{
			fire_print('log','insert cuti commit');
			$this->db->trans_commit();
			return true;
		}
	} */
	
	function _update($post_array, $primary_key)
	{
		try
		{
			fire_print('log','Kode cuti:'.$primary_key);
			
			$dataCuti = $this->db->get_where('cuti',array('KodeCuti'=>$primary_key));
			$cuti = $dataCuti->row(1);
			
			if($cuti->StatusApproval == 'DEP'){
				$dataCuti = array(
					"StatusApproval" => 'PEP', //Pending Rencana Cuti
					"UpdatedBy" => $this->npkLogin,
					"UpdatedOn" => date('Y-m-d H:i:s'),
					"Lokasi" => $post_array['Lokasi'],
					"TanggalMulai" => $post_array['TanggalMulai'],
					"TanggalSelesai" => $post_array['TanggalSelesai']
				);
				
				$this->db->trans_start();
				
				$this->db->update('cuti',$dataCuti,array('KodeCuti'=>$primary_key));
				
				$this->db->update('cutijenistidakhadir',array('Deleted'=>'1'),array('KodeCuti'=>$primary_key));
				foreach($post_array['JenisTidakHadir'] as $rw)
				{
					$dataJenisTidakHadir = array(
						"KodeCuti" => $primary_key,
						"KodeJenisTidakHadir" => $rw,
						"CreatedBy" => $this->npkLogin,
						"CreatedOn" => date('Y-m-d H:i:s')				
					);
					
					$this->db->insert('cutijenistidakhadir',$dataJenisTidakHadir);
				}
				fire_print('log',print_r($post_array,true));
				
				
				$KodeCuti = $primary_key;
				$usertasklama = $this->usertask->getLastUserTask($KodeCuti);
				if($usertasklama){
					foreach($usertasklama as $row){
						$KodeUserTaskLama = $row->KodeUserTask;
					}
					$this->usertask->updateStatusUserTask($KodeUserTaskLama,'AP',$this->npkLogin);
				}
				
				//buat user task baru untuk proses approval cuti
				$KodeUserTask = generateNo('UT');
				if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeCuti,$KodeUserTask,"CT"))
				{
					echo('gagal simpan usertask cuti');
					return false;
				}  
				$this->db->trans_complete(); 
				return $this->db->trans_status();
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function _delete_cuti($primary_key){
		$KodeCuti = $primary_key;
		$cutilist = $this->db->get_where('cuti',array('KodeCuti'=>$KodeCuti));
		$cuti = $cutilist->row();
		if($cuti->StatusApproval == 'DEP' ||$cuti->StatusApproval == 'PEP' )
		{
			//usertask lama dijadikan statusnya DE
			$query = $this->usertask->getLastUserTask($KodeCuti);
			foreach($query as $row){
				$KodeUserTaskLama = $row->KodeUserTask;
			}
			$this->usertask->updateStatusUserTask($KodeUserTaskLama,'DE',$this->npkLogin);
			$this->db->update('cuti',array('deleted' => '1'),array('KodeCuti' => $primary_key));
		}
		else
		{
			return false;
		}		
	}
	
	function _callback_statuscuti($value, $row){
		switch($value)
		{
			case "PEP": $text = "Pending Approval Rencana"; break;
			case "DEP": $text = "Rencana Declined"; break;
			case "APP": $text = "Rencana Approved"; break;
			case "PER": $text = "Pending Approval Realisasi"; break;
			case "DER": $text = "Realisasi Declined"; break;
			case "APR": $text = "Realisasi Approved"; break;
			case "R": $text = ""; break;
		}
		return $text;
	}
	
	function _callback_catatan($value,$primary_key)
	{
		$catatan = $this->usertask->getCatatan($primary_key);
		return $catatan;
	}
	
	function getHariKerja($tanggalmulaiView='',$tanggalselesaiView='')
	{
		try
		{
			fire_print('log','getHariKerja.TanggalMulai : '.$tanggalmulaiView);
			if($tanggalmulaiView == '' && $tanggalselesaiView == ''){
				//if($_POST['tanggalmulai'] != null && $_POST['tanggalselesai'] != null){
				$tanggalmulai = $_POST['tanggalmulai'];
				$tanggalselesai = $_POST['tanggalselesai'];		
				//}
			}else
			{
				$tanggalmulai = $tanggalmulaiView;
				$tanggalselesai = $tanggalselesaiView;
			}
			$hariKerja = 0;
			$hariLibur = 0;
			$hariKerja = $this->cuti_model->getHariKerja($tanggalmulai,$tanggalselesai);
			
			if($tanggalmulaiView == '' && $tanggalselesaiView =='')
				echo $hariKerja . ' hari kerja'; //untuk proses ajax di input
			
			return $hariKerja; //untuk column callback di view
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function ajax_getTahunCutiBesar(){
		try
		{
			$NPK = $_POST['NPK'];
			$dataCutiBesar = $this->db->query('
				select * from mstrcutiuser where deleted = 0 and NPK = ? and akumulasicutibesar > 0
				order by tahun desc limit 1',array($NPK));
			if($dataCutiBesar)
				$tahunCutiBesar = $dataCutiBesar->row(1)->Tahun;
			echo $tahunCutiBesar;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function ajax_getTahunCutiTahunan(){
		try
		{
			$NPK = $_POST['NPK'];
			$dataCuti = $this->db->query('
				select * from mstrcutiuser where deleted = 0 and NPK = ? and akumulasicutitahunan > 0 and tahun > YEAR(now()) - 2
				order by tahun desc',array($NPK));
			$tahunCutiTahunan = "";
			if($dataCuti){
				foreach($dataCuti->result() as $dt){
					$tahunCutiTahunan .= ($dt->Tahun."|");
				}
			}
			echo rtrim($tahunCutiTahunan,"|");
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	/* Buat para head */
	function callback_action_cetak($primary_key,$row)
	{		
		
			$KodeCuti = $primary_key;				
			return site_url('cuti/CutiController/cetakFormCutiHead/Head/' .$KodeCuti);
		
	}
	/*Buat corp sec*/
	function callback_action_cetak_spesial($primary_key,$row)
	{		
		
			$KodeCuti = $primary_key;				
			return site_url('cuti/CutiController/cetakFormCutiHead/Spesial/' .$KodeCuti);
		
	}

	/* Buat para karyawan */
	function callback_action_cetak_nonhead($primary_key,$row)
	{		
		
			$KodeCuti = $primary_key;				
			return site_url('cuti/CutiController/cetakFormCutiHead/NonHead/' .$KodeCuti);
		
	}
	
	function findUserGrupCuti()
	{
		$session_data = $this->session->userdata('logged_in');
		$dataUser = $this->user->findUserGrupCutiByNPK($session_data['npk']);
		if($dataUser){
			foreach($dataUser as $dataUser){
				$dataUserDetail = array(
					'NamaGroup'=> $dataUser->NamaGroup
				);
			}
			$GroupName=  $dataUserDetail['NamaGroup'];
		}else{
			$GroupName=  "Nogroup";
		}
		return $GroupName;
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */