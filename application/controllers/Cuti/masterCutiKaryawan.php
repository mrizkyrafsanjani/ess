<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class MasterCutiKaryawan extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('cuti_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("40"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_mastercutikaryawan();
			}
		}else{
			redirect('login','refresh');
		}
    }
	
	public function _mastercutikaryawan($page='',$nama='',$tahun='',$bulan='',$KodeUserTask='')
    {
		$crud = new grocery_crud();
		$crud->set_subject('Master Cuti Karyawan');
		//$crud->set_theme('datatables');
		
        $crud->set_table('mstrcutiuser');
		$crud->set_relation('NPK','mstruser','Nama',array('Deleted'=>'0'));
		$crud->where('mstrcutiuser.deleted','0');
		
		$crud->columns('NPK','Tahun','JumlahCutiTahunanAwal','JumlahCutiBesarAwal');
		$crud->fields('NPK','Tahun','JumlahCutiTahunanAwal','JumlahCutiBesarAwal');
		
		$crud->display_as('NPK','Nama');
		$crud->display_as('JumlahCutiTahunanAwal','Jumlah Cuti Tahunan Awal');
		$crud->display_as('JumlahCutiBesarAwal','Jumlah Cuti Besar Awal');		
		
		$crud->required_fields('NPK','Tahun','JumlahCutiTahunanAwal','JumlahCutiBesarAwal');
				
		$crud->callback_insert(array($this,'_insert'));		
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete_mastercutikaryawan'));
		
        $output = $crud->render();
        $this-> _outputview($output,$page);
    }
 
    function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Master Cuti Karyawan',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		fire_print('log','page cuti: '.$page);
		
		$this->template->load('default','cuti/masterCuti_view',$data);
		
    }
	
	function _insert($post_array)
	{
		$this->db->trans_start();
		
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		$this->db->insert('mstrcutiuser',$post_array);
		
		$this->db->trans_complete(); 
		return $this->db->trans_status();
	}
	
	function _update($post_array, $primary_key)
	{
		try
		{
			$this->db->trans_start();
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			
			$this->db->update('mstrcutiuser',$post_array,array('KodeMasterCutiUser'=>$primary_key));
			
			$this->db->trans_complete(); 
			return $this->db->trans_status();
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function _delete_mastercutikaryawan($primary_key){
		$this->db->trans_start();
		$post_array['Deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		
		$this->db->update('mstrcutiuser',$post_array,array('KodeMasterCutiUser'=>$primary_key));
		
		$this->db->trans_complete(); 
		return $this->db->trans_status();
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */