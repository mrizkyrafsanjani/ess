<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanJenisTidakHadir extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		//$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("36"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_jenistidakhadir();
			}
		}else{
			redirect('login','refresh');
		}
    }
	
	public function _jenistidakhadir()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Jenis Tidak Hadir');
		//$crud->set_theme('datatables');
		
        $crud->set_table('jenistidakhadir');
		$crud->where('jenistidakhadir.deleted','0');		
		
		$crud->unset_columns('Deleted','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy');
		$crud->unset_fields('KodeJenisTidakHadir','Deleted','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy');
		
		//$crud->display_as('Menu','Menu Yang Dapat Diakses');
		
		$crud->required_fields('Deskripsi');
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete_jenistidakhadir'));
		
        $output = $crud->render();
   
        $this-> _outputview($output);
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Pengaturan Jenis Tidak Hadir',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert($post_array)
	{
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		return $this->db->insert('jenistidakhadir',$post_array);
	}
	
	function _update($post_array, $primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		
		return $this->db->update('jenistidakhadir',$post_array,array('KodeJenisTidakHadir'=>$primary_key));
	}
	
	function _delete_jenistidakhadir($primary_key){
		return $this->db->update('jenistidakhadir',array('deleted' => '1'),array('KodeJenisTidakHadir' => $primary_key));
	}
		
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */