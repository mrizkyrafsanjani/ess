<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class CutiJenisTidakHadir extends CI_Controller {
	var $npkLogin;
	var $kodeCutiGlobal;
	//var $NPKSelectedUser;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->model('cuti_model','',TRUE);
		$this->load->model('cutiJenisTidakHadir_model','',TRUE);
		$this->load->library('grocery_crud');
		$this->kodeCutiGlobal = '';
    }
 
	public function index($kodeUserTask='',$status='')
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			//$this->NPKSelectedUser = $NPKuser;
			$this->_cutiJenisTidakHadir($kodeUserTask,$status);
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _cutiJenisTidakHadir($kodeUserTask,$status)
    {
		$crud = new grocery_crud();
		$crud->set_subject('Cuti Tidak Hadir');
		//$crud->set_theme('datatables');
		
        $crud->set_table('cutijenistidakhadir');
		$crud->where('cutijenistidakhadir.deleted','0');
		
		
		if($status== 'approve' || $status == 'editmode')
		{
			fire_print('log','enter to status approve or editmode');
			$dtltrkrwylist = $this->db->get_where('dtltrkrwy',array('KodeUserTask'=>$kodeUserTask));
			$dtltrkrwy = $dtltrkrwylist->row(1);
			$crud->where('cutijenistidakhadir.KodeCuti',$dtltrkrwy->NoTransaksi);
			$this->kodeCutiGlobal = $dtltrkrwy->NoTransaksi;
			fire_print('log',"kodeCutiGlobal = $this->kodeCutiGlobal");
		}
		else //if($status == '')
		{
			fire_print('log','enter to status ' . $status . ' .Kodeusertask :'.$kodeUserTask);
			$crud->where('cutijenistidakhadir.CreatedBy',$this->npkLogin);
			$crud->where('cutijenistidakhadir.KodeCuti','');
		}
		$crud->set_relation('KodeJenisTidakHadir','jenistidakhadir','Deskripsi',array('Deleted'=>'0','KodeJenisTidakHadir !='=>'12'));
		$crud->columns('KodeJenisTidakHadir','TahunCutiYangDipakai',  'Lokasi', 'TanggalMulai', 'TanggalSelesai','JumlahHari');
		$crud->fields('KodeJenisTidakHadir','TahunCutiYangDipakai', 'Lokasi', 'TanggalMulai', 'TanggalSelesai','JumlahHari');
		
		$crud->required_fields('KodeJenisTidakHadir', 'TanggalMulai', 'TanggalSelesai');
		
		$crud->display_as('TahunCutiYangDipakai','Tahun Dipakai');
		$crud->display_as('KodeJenisTidakHadir','Jenis Tidak Hadir');
		
		$crud->callback_field('TahunCutiYangDipakai',array($this,'add_field_callback_tahun'));
		$crud->callback_field('Lokasi',array($this,'_callback_lokasi'));
		$crud->callback_field('JumlahHari',array($this,'_callback_jumlahhari'));
		
		$crud->callback_column('JumlahHari',array($this,'_callback_column_jumlahhari'));
		$crud->callback_column('Lokasi',array($this,'_callback_column_lokasi'));
		$crud->callback_column('TahunCutiYangDipakai',array($this,'_callback_column_tahuncutiyangdipakai'));
		
		$crud->callback_insert(array($this,'_insert'));
		//$crud->callback_delete(array($this,'_delete'));		
		$crud->unset_read();
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_edit();
		
		
		$crud->set_rules('KodeJenisTidakHadir','Jenis Cuti','callback_validasiJenisCuti');
		$crud->set_rules('TanggalMulai','Tanggal Cuti','callback_validasiTanggalMulaiCuti');
		$crud->set_rules('TanggalSelesai','Tanggal Cuti','callback_validasiTanggalCuti');
		$crud->set_rules('TahunCutiYangDipakai','Tahun Dipakai','callback_validasiTahunCutiDipakai');		
		
		if($status == "approve")
		{
			$crud->unset_operations();
		}
		
        $js = "
		<script>
			var spans = document.getElementsByTagName('span');					
			for(var i=0;i<spans.length;i++)
			{
				if(spans[i].innerHTML == '&nbsp;Edit')
					//alert(spans[i].visible);
					spans[i].style.display = 'none';
				if(spans[i].innerHTML == '&nbsp;Delete')
					spans[i].innerHTML = 'Hapus';
			}
			
			$(document).ready(function() {
				var TanggalMulai = $('#field-TanggalMulai');
				var TanggalSelesai = $('#field-TanggalSelesai');
				var JumlahHari = $('#field-JumlahHari');				
				var ComboJenisTidakHadir = $('#field-KodeJenisTidakHadir');
				var TahunComboBox = $('#TahunCutiYangDipakai');
				
				ComboJenisTidakHadir.change(function(){
					var KodeJenisTidakHadir = ComboJenisTidakHadir.val();					
					if(KodeJenisTidakHadir != '1' && KodeJenisTidakHadir != '2')
					{
						TanggalMulai.val('');
						TanggalSelesai.val('');						
					}
					
					if(KodeJenisTidakHadir == '2' || KodeJenisTidakHadir == '1')
					{
						//$('#TahunCutiYangDipakai_field_box').show();
						//$('#Lokasi_field_box').hide();
						tahun = new Date().getFullYear();
						if(KodeJenisTidakHadir == '2') //cuti besar
						{	
							$('#TahunCutiYangDipakai option[value=\"'+(tahun-1)+'\"]').hide();
							$('#TahunCutiYangDipakai option[value=\"'+tahun+'\"]').hide();
							$('#TahunCutiYangDipakai option[value=\"'+(tahun+1)+'\"]').hide();
							for(z=tahun-2;z>tahun-5;z--)
							{
								$('#TahunCutiYangDipakai option[value=\"'+z+'\"]').show();
							}

							$.ajax({
								url: '".base_url()."index.php/Cuti/Cuti/ajax_getTahunCutiBesar',
								type: 'POST',
								data: {NPK:'".$this->npkLogin."'},
								dataType: 'html'
							}).done(function(response){
								$('#TahunCutiYangDipakai option[value=\"'+response+'\"]').show();
							});
						}else{
							$('#TahunCutiYangDipakai option[value=\"'+(tahun+1)+'\"]').show();
							for(z=tahun;z>tahun-5;z--)
							{
								$('#TahunCutiYangDipakai option[value=\"'+z+'\"]').hide();
							}
							
							$.ajax({
								url: '".base_url()."index.php/Cuti/Cuti/ajax_getTahunCutiTahunan',
								type: 'POST',
								data: {NPK:'".$this->npkLogin."'},
								dataType: 'html'
							}).done(function(response){
								var responseSplit = response.split('|');
								for(i=0;i<response.length;i++){
									$('#TahunCutiYangDipakai option[value=\"'+responseSplit[i]+'\"]').show();
								}
							});
						}
					}else{
						//$('#TahunCutiYangDipakai_field_box').hide();
						//$('#Lokasi_field_box').show();
					}
				});
				
				TanggalSelesai.change(function(){
					//var selisihHari = DateDiff(new Date(TanggalSelesai.val()),new Date(TanggalMulai.val()))+1;
					$.ajax({
					  url: '" .base_url()."index.php/Cuti/Cuti/getHariKerja',
					  type: 'POST',
					  data: {tanggalmulai : TanggalMulai.val(), tanggalselesai: TanggalSelesai.val()},
					  dataType: 'html'
					}).done(function(response){
						JumlahHari.val(response);
					});					
				});
				
				TanggalMulai.change(function(){
					var RadioLokasi = $(\"input:radio[name=Lokasi]:checked\");
					var KodeJenisTidakHadir = ComboJenisTidakHadir.val();
					if(KodeJenisTidakHadir != '1' && KodeJenisTidakHadir != '2')
					{
						$.ajax({
						  url: '" .base_url()."index.php/Cuti/CutiController/getTanggalSelesaiIzin',
						  type: 'POST',
						  data: {tanggalmulai : TanggalMulai.val(), kodejenistidakhadir: KodeJenisTidakHadir, lokasi: RadioLokasi.val()},
						  dataType: 'html'
						}).done(function(response){
							TanggalSelesai.val(response);
						});
					}
				});
				
				
			
			}); 
			
			//source: http://stackoverflow.com/questions/12003660/javascript-datediff
			function DateDiff(date1, date2) {
				var datediff = date1.getTime() - date2.getTime(); //store the getTime diff - or +
				return (datediff / (24*60*60*1000)); //Convert values to -/+ days and return value      
			}
		</script>";
		
        $output = $crud->render();
		$output->output.=$js;   
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$data = array(
			'title' => '',
			'body' => $output
		); 
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		
        $this->load->view('Cuti/cutiJenisTidakHadirSimple_view',$data);
    }
	
	function _insert($post_array){
		try
		{
			//proses validasi terlebih dahulu
			if($post_array['KodeJenisTidakHadir'] != 1 && $post_array['KodeJenisTidakHadir'] != 2){
				//dapatkan jumlah hari maksimal
				$Lokasi = $post_array['Lokasi'];
				$MaxHariIzin = 0;
				
				$JenisTidakHadirList = $this->db->get_where('jenistidakhadir',array('Deleted'=>'0','KodeJenisTidakHadir'=>$post_array['KodeJenisTidakHadir']));
				$JenisTidakHadir = $JenisTidakHadirList->row(1);
				
				if($Lokasi == "LK") //Luar Kota
				{
					$MaxHariIzin = $JenisTidakHadir->JumlahHariLuarKota;
				}
				else if($Lokasi == "DK")
				{
					$MaxHariIzin = $JenisTidakHadir->JumlahHariDalamKota;
				}
				fire_print('log',"MaxHariIzin : $MaxHariIzin");
				//cek apakah jumlah hari yang diinput melebihi jumlah hari maksimal
				if($this->cuti_model->getHariKerja($post_array['TanggalMulai'],$post_array['TanggalSelesai']) > $MaxHariIzin)
				{
					return false;
				}
				
				
			}
			$tahunYangDipakai = date("Y");
			$post_array['CreatedOn'] = date('Y-m-d H:i:s');
			$post_array['CreatedBy'] = $this->npkLogin;
			
			/* $dataCutiUserList = $this->db->query("SELECT * FROM mstrcutiuser WHERE Deleted = 0 AND NPK = ?
			ORDER BY Tahun DESC LIMIT 5",$this->npkLogin);
			
			$i = 0;
			foreach($dataCutiUserList->result() as $dataCutiUser)
			{
				if($post_array['KodeJenisTidakHadir'] == 1) //cuti tahunan
				{
					if($dataCutiUser->AkumulasiCutiTahunan > 0 && $i < 2)
						$tahunYangDipakai = $dataCutiUser->Tahun;
				}
				else if($post_array['KodeJenisTidakHadir'] == 2) //cuti besar
				{
					if($dataCutiUser->JumlahCutiBesarAwal > 0)
						$tahunYangDipakai = $dataCutiUser->Tahun;
				}
				$i++;
			} */
			
			if($post_array['KodeJenisTidakHadir'] != 1 && $post_array['KodeJenisTidakHadir'] != 2 ){
				//$tahunYangDipakai = "";
				if( is_string( $post_array['TahunCutiYangDipakai'])) $DateTglMulai = date_create( $post_array['TahunCutiYangDipakai']);
				$post_array['TahunCutiYangDipakai'] = $DateTglMulai->format("Y");
			}
			
			//$post_array['TahunCutiYangDipakai'] = $tahunYangDipakai;
			$post_array['KodeCuti'] = $this->kodeCutiGlobal;
			fire_print('log','nilai post array insert : ' . print_r($post_array,true));
			return $this->db->insert('cutijenistidakhadir',$post_array);			
			
			//return false;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function validasiTanggalMulaiCuti($str)
	{
		try{
			fire_print('log',"validasiTanggalMulaiCuti: $str");
			$TanggalMulai = $this->input->post('TanggalMulai');
			$TanggalSelesai = $this->input->post('TanggalSelesai');
			fire_print('log',"validasiTanggalMulaiCuti.TanggalMulai: $TanggalMulai");
			$dataAllTanggalOnProgress = $this->cutiJenisTidakHadir_model->getAllTanggalOnProses($this->npkLogin);
			fire_print('log',"validasiTanggalMulaiCuti.dataAllTanggalOnProgress");
			if($dataAllTanggalOnProgress){
				foreach($dataAllTanggalOnProgress as $dtTgl){
					if($TanggalMulai >= $dtTgl->TanggalMulai && $TanggalMulai <= $dtTgl->TanggalSelesai){
						$this->form_validation->set_message('validasiTanggalMulaiCuti', 'Tanggal Mulai bertabrakan dengan tanggal lain yang sedang diajukan. Mohon pilih tanggal lain.');
						return false;
					}

					//untuk mengatasi jika diantara tanggal mulai dan tanggal selesai yang dipilih terdapat cuti lain
					if(($dtTgl->TanggalMulai >= $TanggalMulai && $dtTgl->TanggalMulai <= $TanggalSelesai)||($dtTgl->TanggalSelesai >= $TanggalMulai && $dtTgl->TanggalSelesai <= $TanggalSelesai))
					{
						$this->form_validation->set_message('validasiTanggalMulaiCuti', "Tanggal Mulai bertabrakan dengan tanggal lain yang sedang diajukan. Sudah ada request cuti antara tanggal $dtTgl->TanggalMulai dan $dtTgl->TanggalSelesai");
						return false;
					}
				}
			}else{
				return true;
			}
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function validasiTanggalCuti($str)
	{
		fire_print('log',"validasiTanggalCuti. str: $str");
		try
		{
			
			$KodeJenisTidakHadir = $this->input->post('KodeJenisTidakHadir');
			$Lokasi = $this->input->post('Lokasi');
			$Tahun = $this->input->post('TahunCutiYangDipakai');
			$TahunDepan = date("Y")+1;
			fire_print('log',"Lokasi : $Lokasi");
			$TanggalMulai = $this->input->post('TanggalMulai');
			$TanggalSelesai = $this->input->post('TanggalSelesai');
			if(!$TanggalSelesai)
			{
				$this->form_validation->set_message('validasiTanggalCuti', 'Tanggal Selesai is required.');
				return false;
			}
			
			if($TanggalSelesai < $TanggalMulai){
				$this->form_validation->set_message('validasiTanggalCuti', 'Tanggal Selesai harus lebih besar dari Tanggal Mulai.');
				return false;
			}
			
			$dataAllTanggalOnProgress = $this->cutiJenisTidakHadir_model->getAllTanggalOnProses($this->npkLogin);
			if($dataAllTanggalOnProgress){
				foreach($dataAllTanggalOnProgress as $dtTgl){
					if($TanggalSelesai >= $dtTgl->TanggalMulai && $TanggalSelesai <= $dtTgl->TanggalSelesai){
						$this->form_validation->set_message('validasiTanggalCuti', 'Tanggal Selesai bertabrakan dengan tanggal lain yang sedang diajukan. Mohon pilih tanggal lain.');
						return false;
					}
				}
			}
			
			if($KodeJenisTidakHadir){
				if($KodeJenisTidakHadir != 1 && $KodeJenisTidakHadir != 2){
					//dapatkan jumlah hari maksimal			
					$MaxHariIzin = 0;
					
					$JenisTidakHadirList = $this->db->get_where('jenistidakhadir',array('Deleted'=>'0','KodeJenisTidakHadir'=>$KodeJenisTidakHadir));
					$JenisTidakHadir = $JenisTidakHadirList->row(1);
					
					if($Lokasi == "LK") //Luar Kota
					{
						$MaxHariIzin = $JenisTidakHadir->JumlahHariLuarKota;
					}
					else if($Lokasi == "DK")
					{
						$MaxHariIzin = $JenisTidakHadir->JumlahHariDalamKota;
					}
					fire_print('log',"MaxHariIzin : $MaxHariIzin");
					//cek apakah jumlah hari yang diinput melebihi jumlah hari maksimal
					if($this->cuti_model->getHariKerja($TanggalMulai,$TanggalSelesai) > $MaxHariIzin)
					{
						$this->form_validation->set_message('validasiTanggalCuti', 'Anda tidak bisa memilih jangka waktu untuk '. $JenisTidakHadir->Deskripsi .' melebihi '. $MaxHariIzin .' hari (batas yang sudah ditentukan dalam peraturan perusahaan).');
						return false;
					}
				}
				else
				{
					$akumulasiCutiTahunanUser = 0;
					$akumulasiCutiBesarUser = 0;
					if($Tahun == $TahunDepan && $KodeJenisTidakHadir == 1) //jika merupakan hutang cuti tahunan, ini untuk supaya penghitungan akumulasi cuti besar sesuai.
					{
						$dataCutiUser = $this->cuti_model->getCutiUser($this->npkLogin,'');
						$dataCutiBesarUserYgSdgDiajukan = $this->cutiJenisTidakHadir_model->getCutiBesarOnProses($this->npkLogin,'');
					}
					else
					{
						$dataCutiUser = $this->cuti_model->getCutiUser($this->npkLogin,$Tahun);
						$dataCutiBesarUserYgSdgDiajukan = $this->cutiJenisTidakHadir_model->getCutiBesarOnProses($this->npkLogin,$Tahun);
					}
					
					$dataUser = $this->user->findUserByNPK($this->npkLogin);
					foreach($dataUser as $rw)
					{
						$BulanBekerjaKaryawan = date("n",strtotime($rw->TanggalBekerja));
					}
					
					$countData = 0;
					if($dataCutiUser){
						$hasil = $this->cuti_model->getJumlahCutiUser($this->npkLogin);
						$akumulasiCutiBesarUser = $hasil['JumlahCutiBesar'];

						$hasilpertahun = $this->cuti_model->getCutiUserForTrx($this->npkLogin);
						$arrayakumcutitahunan = $hasilpertahun['arraycutitahunan'];
						$akumulasiCutiTahunanUser = $arrayakumcutitahunan[$Tahun];
						fire_print('log',"akumulasiCutiBesarUser : $akumulasiCutiBesarUser");
					}
					
					if($dataCutiBesarUserYgSdgDiajukan){
						foreach($dataCutiBesarUserYgSdgDiajukan as $dataCtBesarOnProses)
						{
							$akumulasiCutiBesarUser -= $this->cuti_model->getHariKerja($dataCtBesarOnProses->TanggalMulai,$dataCtBesarOnProses->TanggalSelesai);
						}
					}
					
					if($KodeJenisTidakHadir == 1) //Cuti Tahunan
					{
						if($Tahun == $TahunDepan) //jika merupakan hutang cuti tahunan, ini untuk supaya penghitungan akumulasi cuti besar sesuai.
						{
							$dataCutiTahunanUserYgSdgDiajukan = $this->cutiJenisTidakHadir_model->getCutiTahunanOnProses($this->npkLogin,'');
						}
						else
						{
							$dataCutiTahunanUserYgSdgDiajukan = $this->cutiJenisTidakHadir_model->getCutiTahunanOnProses($this->npkLogin,$Tahun);
						}
						
						if($dataCutiTahunanUserYgSdgDiajukan){
							foreach($dataCutiTahunanUserYgSdgDiajukan as $dataCtTahunanOnProses)
							{
								$akumulasiCutiTahunanUser -= $this->cuti_model->getHariKerja($dataCtTahunanOnProses->TanggalMulai,$dataCtTahunanOnProses->TanggalSelesai);
							}
						}
						fire_print('log',"akumulasiCutiTahunanUser : $akumulasiCutiTahunanUser, TahunDepan : $TahunDepan, Tahun : $Tahun, AkumulasiCutiBesarUser : $akumulasiCutiBesarUser");
						if($akumulasiCutiTahunanUser > 0 && $Tahun == $TahunDepan)
						{
							$this->form_validation->set_message('validasiTanggalCuti',"Tidak boleh mengajukan hutang cuti tahunan, jika masih terdapat sisa cuti tahunan.");
							return false;
						}
						
						//kurang dgn data yg skrg di ajukan
						$akumulasiCutiTahunanUser -= $this->cuti_model->getHariKerja($TanggalMulai,$TanggalSelesai);
						
						fire_print('log',"akumulasiCutiTahunanUser : $akumulasiCutiTahunanUser, TahunDepan : $TahunDepan, Tahun : $Tahun, AkumulasiCutiBesarUser : $akumulasiCutiBesarUser");
						if($akumulasiCutiTahunanUser < 0 && $Tahun < $TahunDepan)
						{
							$this->form_validation->set_message('validasiTanggalCuti',"Sisa cuti tahunan $Tahun Anda tidak mencukupi.");
							return false;
						}						
						
						if($akumulasiCutiBesarUser > 0 && $Tahun == $TahunDepan)
						{
							$this->form_validation->set_message('validasiTanggalCuti',"Tidak boleh mengajukan hutang cuti tahunan, jika masih terdapat sisa cuti besar.");
							return false;
						}
					}
					else if ($KodeJenisTidakHadir == 2) //Cuti Besar
					{
						//kurang dgn data yg skrg di ajukan
						$akumulasiCutiBesarUser -= $this->cuti_model->getHariKerja($TanggalMulai,$TanggalSelesai);
						
						fire_print('log',"akumulasiCutiBesarUser : $akumulasiCutiBesarUser");
						if($akumulasiCutiBesarUser < 0)
						{
							$this->form_validation->set_message('validasiTanggalCuti',"Sisa cuti besar $Tahun Anda tidak mencukupi.");
							return false;
						}
					}
				}
			}
			return true;
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function validasiTahunCutiDipakai($value)
	{
		try
		{
			fire_print('log','validasiTahunCutiDipakai. value :'.$value);
			$KodeJenisTidakHadir = $this->input->post('KodeJenisTidakHadir');
			fire_print('log',"value validasi Tahun Cuti Dipakai: $value, KodeJenisTidakHadir : $KodeJenisTidakHadir");
			if($KodeJenisTidakHadir == 1 || $KodeJenisTidakHadir == 2 )
			{
				if(!$value)
				{
					$this->form_validation->set_message('validasiTahunCutiDipakai','Anda harus memasukkan cuti tahun berapa yang Anda gunakan.');
					return false;
				}
			}
			return true;
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);			
		}
	}
	
	function validasiJenisCuti($str)
	{
		fire_print('log','validasiJenisCuti. str :'.$str);
		if(!$str)
		{
			$this->form_validation->set_message('validasiJenisCuti', 'Jenis Tidak Hadir is required.');
			return false;
		}
		
		$dataUserYgCuti = $this->user->dataUser($this->npkLogin);
		foreach($dataUserYgCuti as $rw)
		{
			$BulanBekerjaKaryawan = date("n",strtotime($rw->TanggalBekerja));
		}
		
		$akumulasiCutiBesar = 0;
		$dataCutiUserList = $this->db->query("SELECT * FROM mstrcutiuser WHERE Deleted = 0 AND NPK = ?
			ORDER BY Tahun DESC LIMIT 6",$this->npkLogin);
		if($dataCutiUserList)
		{
			$hasil = $this->cuti_model->getJumlahCutiUser($this->npkLogin);
			fire_print('log','validasiJenisCuti.hasil : '.print_r($hasil,true));
			$akumulasiCutiBesar = $hasil['JumlahCutiBesar'];
			
			fire_print('log','str: '.$str);
			if($akumulasiCutiBesar <= 0 && $str == 2){
				$this->form_validation->set_message('validasiJenisCuti', 'Cuti Besar tidak dapat dipilih karena sisa cuti besar yang dimiliki Anda adalah 0.');
				return false;
			}
			else
			{
				return true;
			}
		}
		return true;
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('riwayatpendidikan',$post_array,array('KodeCutiJenisTidakHadir' => $primary_key));
	}
	
	function _callback_column_jumlahhari($value,$row)
	{
		$jumlahHari = $this->cuti_model->getHariKerja($row->TanggalMulai,$row->TanggalSelesai);
		//fire_print('log','Hari mulai cuti:'.$row->TanggalMulai.',hari selesai:'.$row->TanggalSelesai);
		//fire_print('log','jumlah hari kerja column:'.$jumlahHari);
		return $jumlahHari;
	}
	
	function _callback_column_lokasi($value,$row)
	{
		switch($row->Lokasi)
		{
			case "DK": return "Dalam Kota";
				break;
			case "LK": return "Luar Kota";
				break;
		}
	}
	
	function _callback_column_tahuncutiyangdipakai($value, $row)
	{
		if($value > date("Y"))
		{
			return "Hutang Cuti";
		}
		return $value;
	}
	
	function getHariKerja($tanggalmulaiView='',$tanggalselesaiView='')
	{
		try
		{
			fire_print('log','getHariKerja.TanggalMulai : '.$tanggalmulaiView);
			if($tanggalmulaiView == '' && $tanggalselesaiView == ''){
				//if($_POST['tanggalmulai'] != null && $_POST['tanggalselesai'] != null){
				$tanggalmulai = $_POST['tanggalmulai'];
				$tanggalselesai = $_POST['tanggalselesai'];		
				//}
			}else
			{
				$tanggalmulai = $tanggalmulaiView;
				$tanggalselesai = $tanggalselesaiView;
			}
			$hariKerja = 0;
			$hariLibur = 0;
			$hariKerja = $this->cuti_model->getHariKerja($tanggalmulai,$tanggalselesai);
			
			if($tanggalmulaiView == '' && $tanggalselesaiView =='')
				echo $hariKerja . ' hari kerja'; //untuk proses ajax di input
			
			return $hariKerja; //untuk column callback di view
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function _callback_lokasi($value='', $primary_key = null)
	{
		$selectedDN = '';
		$selectedLN = '';
		
		switch($value)
		{
			case "DK": $selectedDN = "checked"; break;
			case "LK" : $selectedLN = "checked"; break;
		}
		
		return ' <input type="radio" id="LokasiDK" name="Lokasi" value="DK" '.$selectedDN.' /> Dalam Kota
			<input type="radio" id="LokasiLK" name="Lokasi" value="LK" '.$selectedLN.' /> Luar Kota';
	}
	
	function add_field_callback_tahun($value = '', $primary_key = null)
	{
		$dataUser = $this->user->findUserByNPK($this->npkLogin);
		foreach($dataUser as $rw)
		{
			$BulanBekerjaKaryawan = date("n",strtotime($rw->TanggalBekerja));
		}
		$flagTahunSama = 0;
		$strSelectHTML = ' <select id="TahunCutiYangDipakai" name="TahunCutiYangDipakai">
			<option value = ""></option>';
		
		$dataCutiUserList = $this->db->query("SELECT * FROM mstrcutiuser WHERE Deleted = 0 AND NPK = ?
			ORDER BY Tahun DESC LIMIT 6",$this->npkLogin) ;
		$i = 0;
		$tahun = date("Y");
		$strSelectHTML .= '<option value ="'. ($tahun+1) .'">Hutang Cuti</option>';
		fire_print('log',"tahun : $tahun");
		foreach($dataCutiUserList->result() as $row)
		{
			if($row->AkumulasiCutiTahunan > 0 && $i < 2){
				$tahun = $row->Tahun;
				$selectedTahunKelulusan = '';
				if($row->Tahun == $value)
				{
					$selectedTahunKelulusan = 'selected';
				}
				$strSelectHTML .= '<option '.$selectedTahunKelulusan.' value ="'.$tahun.'">'.$tahun.'</option>';
			}
			if($row->AkumulasiCutiBesar > 0)
			{
				if($row->AkumulasiCutiTahunan <= 0){
					fire_print('log',"masuk ke akumulasi cuti tahunan jika cuti besar > 0 ");
					$tahun = $row->Tahun;
					$selectedTahunKelulusan = '';
					if($row->Tahun == $value)
					{
						$selectedTahunKelulusan = 'selected';
					}
					$strSelectHTML .= '<option '.$selectedTahunKelulusan.' value ="'.$tahun.'">'.$tahun.'</option>';
				}
			
				if(date("Y") == $row->Tahun)
				{
					if(date("n") > $BulanBekerjaKaryawan )
					{
						$flagTahunSama = 1;
					}
				}
				else
				{
					if($flagTahunSama == 0 && $tahun != $row->Tahun)
					{
						$tahun = $row->Tahun;
						$selectedTahunKelulusan = '';
						if($row->Tahun == $value)
						{
							$selectedTahunKelulusan = 'selected';
						}
						$strSelectHTML .= '<option '.$selectedTahunKelulusan.' value ="'.$tahun.'">'.$tahun.'</option>';
					}
				}
			}
			$i++;
		}
		
		
		$strSelectHTML .= '</select>';
		
		return $strSelectHTML;
	}
	
	function _callback_jumlahhari($value= '', $primary_key = null)
	{
		return '<input type="text" id="field-JumlahHari" style="border:0;" value="" readonly>';
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */