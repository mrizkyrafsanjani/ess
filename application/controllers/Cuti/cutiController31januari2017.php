<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class CutiController extends CI_Controller{
		var $npkLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('user','',TRUE);
			$this->load->model('cuti_model','',TRUE);
			$this->load->model('cutiJenisTidakHadir_model','',TRUE);
			$this->load->model('usertask','',TRUE);
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
		}
		
		function index() //viewAdmin
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("38"))
					{
						$data = array(
							'title' => 'View Cuti Izin Sakit',
							'admin' => '1',
							'databawahan' => $this->user->getDataBawahan($this->npkLogin),
							'npk' => ''
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Cuti/cuti_view',$data);
					}
				}
				else
				{
					redirect('login','refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ViewCutiUser($KodeUserTask='') //ViewUser
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("37"))
					{
						if($KodeUserTask == '')
							$KodeUserTask = 'non';
						$data = array(
							'title' => 'View Cuti User',
							'admin' => '0',							
							'npk' => $this->npkLogin,
							'kodeusertask' => $KodeUserTask
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Cuti/cuti_view',$data);
					}
				}
				else
				{
					redirect('login','refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function tidakbisa_ubah()
		{
			$this->session->set_flashdata('msg','Tidak bisa ubah, proses ubah hanya bisa dilakukan jika rencana cuti di decline');
			redirect('cuti/cuti/viewuser');
		}		
		
		function InputCuti()
		{
			try
			{
				$session_data = $this->session->userdata('logged_in');
				if($session_data){
					if(check_authorized("35"))
					{
						$this->npkLogin = $session_data['npk'];
						
						
						$departemen = '';
						$nama = '';
						$historycatatan = '';
												
						$hasil = $this->cuti_model->getCutiUserForTrx($this->npkLogin);
						$sisacutitahunan = $hasil['sisacutitahunan'];
						$sisacutibesar = $hasil['sisacutibesar'];
						$departemen = $hasil['departemen'];
						$nama = $hasil['nama'];
						
						$data = array(
							'editmode' => '0',
							'kodeusertask' => '',
							'title'=> 'Input Cuti Sakit Izin User',
							'npk' => $this->npkLogin,
							'nama' => $nama,
							'departemen' => $departemen,
							'sisacutitahunan' => $sisacutitahunan,
							'sisacutibesar' => $sisacutibesar,
							'historycatatan' => $historycatatan
						);
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','Cuti/inputCuti_view',$data);
						
					}
				}else{
					redirect('login','refresh');
				}
			}			
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function EditCuti($kodeUserTask)
		{
			try
			{
				$session_data = $this->session->userdata('logged_in');
				if($session_data){
					if(check_authorized("35"))
					{
						$this->npkLogin = $session_data['npk'];
						
						$dtCuti = $this->cuti_model->getCuti($kodeUserTask);
						$npkSelected = '';
						$kodeCuti = '';
						foreach($dtCuti as $dt)
						{
							$npkSelected = $dt->NPK;
							$kodeCuti = $dt->KodeCuti;
						}
						$historycatatan = $this->usertask->getCatatan($kodeCuti);
						
						$hasil = $this->cuti_model->getCutiUserForTrx($npkSelected);
						$sisacutitahunan = $hasil['sisacutitahunan'];
						$sisacutibesar = $hasil['sisacutibesar'];
						$departemen = $hasil['departemen'];
						$nama = $hasil['nama'];
						
						$data = array(
							'editmode' => '1',
							'kodeusertask' => $kodeUserTask,
							'title'=> 'Edit Cuti Sakit Izin User',
							'npk' => $npkSelected,
							'nama' => $nama,
							'departemen' => $departemen,
							'sisacutitahunan' => $sisacutitahunan,
							'sisacutibesar' => $sisacutibesar,
							'historycatatan' => $historycatatan
						);
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','Cuti/inputCuti_view',$data);						
					}
				}else{
					redirect("login?u=Cuti/cutiController/EditCuti/$kodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ApprovalRencanaCuti($KodeUserTask)
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("39"))
					{
						$datacuti = $this->cuti_model->getCuti($KodeUserTask);
						
						foreach($datacuti as $row){	
							$dataJenisTidakHadir = $this->cuti_model->getJenisTidakHadir($row->KodeCuti);
						
							$historycatatan = $this->usertask->getCatatan($row->KodeCuti);
							
							$hasil = $this->cuti_model->getCutiUserForTrx($row->NPK);
							$sisacutitahunan = $hasil['sisacutitahunan'];
							$sisacutibesar = $hasil['sisacutibesar'];
							
							$data = array(
								'realisasi' => '0',
								'title' => 'Approval Rencana Cuti',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'npk' => $row->NPK,
								'nama' => $row->Nama,
								'sisacutitahunan' => $sisacutitahunan,
								'sisacutibesar' => $sisacutibesar,
								//'tanggalmulai' => date('j F Y',strtotime($row->TanggalMulai)),
								//'tanggalselesai' => date('j F Y',strtotime($row->TanggalSelesai)),
								//'jumlahhari' => $this->getHariKerja($row->TanggalMulai,$row->TanggalSelesai),
								//'jenistidakhadir' => $dataJenisTidakHadir,
								'historycatatan' => $historycatatan
							);
						}
						$this->load->helper(array('form','url'));
						$this->template->load('default','Cuti/approvalCuti_view',$data);
					}
				}
				else
				{
					redirect("login?u=Cuti/cutiController/ApprovalRencanaCuti/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ApprovalRealisasiCuti($KodeUserTask)
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("39"))
					{
						$datacuti = $this->cuti_model->getCuti($KodeUserTask);
						
						foreach($datacuti as $row){								
							$historycatatan = $this->usertask->getCatatan($row->KodeCuti);
							
							$hasil = $this->cuti_model->getCutiUserForTrx($row->NPK);
							$sisacutitahunan = $hasil['sisacutitahunan'];
							$sisacutibesar = $hasil['sisacutibesar'];
							
							$data = array(
								'realisasi' => '1',
								'title' => 'Approval Realisasi Cuti',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'npk' => $row->NPK,
								'nama' => $row->Nama,								
								//'tanggalmulai' => date('j F Y',strtotime($row->TanggalMulai)),
								//'tanggalselesai' => date('j F Y',strtotime($row->TanggalSelesai)),
								//'jumlahhari' => $this->getHariKerja($row->TanggalMulai,$row->TanggalSelesai),
								//'jenistidakhadir' => $dataJenisTidakHadir,
								'sisacutitahunan' => $sisacutitahunan,
								'sisacutibesar' => $sisacutibesar,								
								'historycatatan' => $historycatatan
							);
						}
						$this->load->helper(array('form','url'));
						$this->template->load('default','Cuti/approvalCuti_view',$data);
					}
				}
				else
				{
					redirect("login?u=Cuti/cutiController/ApprovalRealisasiCuti/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function getTanggalSelesaiIzin()
		{
			try
			{
				$tanggalMulai = $this->input->post('tanggalmulai');
				$KodeJenisTidakHadir = $this->input->post('kodejenistidakhadir');
				$Lokasi = $this->input->post('lokasi');
				$MaxHariIzin = 0;
				if($KodeJenisTidakHadir != 1 && $KodeJenisTidakHadir != 2)
				{
					$JenisTidakHadirList = $this->db->get_where('jenistidakhadir',array('Deleted'=>'0','KodeJenisTidakHadir'=>$KodeJenisTidakHadir));
					$JenisTidakHadir = $JenisTidakHadirList->row(1);
					
					if($Lokasi == "LK") //Luar Kota
					{
						$MaxHariIzin = $JenisTidakHadir->JumlahHariLuarKota;
					}
					else if($Lokasi == "DK")
					{
						$MaxHariIzin = $JenisTidakHadir->JumlahHariDalamKota;
					}
					$MaxHariIzin--;
					fire_print('log',"MaxHariIzin : $MaxHariIzin");
					
					$tanggalSelesai = date('Y-m-d', strtotime($tanggalMulai. ' + '. $MaxHariIzin .' days'));
					
					while($this->cuti_model->getHariKerja($tanggalMulai,$tanggalSelesai) <= $MaxHariIzin)
					{
						$tanggalSelesai = date('Y-m-d', strtotime($tanggalSelesai. ' + 1 days'));
					}
					
					echo $tanggalSelesai;
					
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_prosesGenerateCuti()
		{
			$jumlahCutiTahunanAwal = 0;
			$JumlahCutiBesarAwal = 0;
			$akumulasiCutiTahunan = 0;
			$akumulasiCutiBesar = 0;
			$akumulasiCutiTahunLalu = 0;
			
			try
			{
				$success = "Berhasil";
				$tahunInput = $this->input->post('tahun');
				$globalParamlist = $this->db->get_where('globalparam',array('Name'=>'tanggalPenentuanBulanMasukCuti'));
				$tglPenentuCuti = $globalParamlist->row(1)->Value;
				fire_print('log','TanggalPenentuCuti :'.$tglPenentuCuti);
				$this->db->trans_begin();
				//cari semua karyawan, lalu masukkan. Validasi apakah tanggal sudah masuk 
				$dataUserList = $this->db->query("SELECT NPK,TanggalBekerja FROM mstruser 
					WHERE Deleted = 0 AND (TanggalBerhenti is null OR TanggalBerhenti = '0000-00-00')");
				fire_print('log','dataUserList : '. print_r($dataUserList->result(),true));
				
				$resultQ0 = $this->db->query("update mstrcutiuser set Deleted = 1,UpdatedOn = now(),UpdatedBy = ?
					where Tahun = ?",array($this->npkLogin,$tahunInput));
				$resultQ0 = $this->db->query("delete from cuti 
					where YEAR(TanggalMulai) = ? and KodeCuti LIKE 'CB%'",$tahunInput);
				$resultQ0 = $this->db->query("delete from cutijenistidakhadir 
					where YEAR(TanggalMulai) = ? and KodeCuti LIKE 'CB%'",$tahunInput);
				
				foreach($dataUserList->result() as $dataUser)
				{
					$jumlahCutiTahunanAwal = 0;
					$JumlahCutiBesarAwal = 0;
					$akumulasiCutiTahunan = 0;
					$akumulasiCutiBesar = 0;
					$tglBekerja = strtotime($dataUser->TanggalBekerja);
					if($tglPenentuCuti != 'now')
					{
						if(date('d',$tglBekerja) >= $tglPenentuCuti)
						{
							$tglBekerja = strtotime($dataUser->TanggalBekerja . ' first day of next month');
							fire_print('log','Not Now tgl Bekerja :'.date('Y-m-d',$tglBekerja));
						}
					}
					$tahunTanggalBekerja = date('Y',$tglBekerja);
					//fire_print('log','Tahun Tanggal Bekerja :'. $tahunTanggalBekerja);
					
					if($tahunTanggalBekerja == $tahunInput)
					{
						$jumlahCutiTahunanAwal = 0;
					}
					else if($tahunInput - $tahunTanggalBekerja > 1)
					{
						$jumlahCutiTahunanAwal = 12;
					}
					else if($tahunInput - $tahunTanggalBekerja == 1)
					{
						$jumlahCutiTahunanAwal = 13 - date('m',$tglBekerja);
					}
					fire_print('log','jumlahCutiTahunanAwal:'.$jumlahCutiTahunanAwal);
					
					$listCutiBersama = $this->db->get_where('harilibur',array('Deleted'=>'0','Hari'=>'cb','YEAR(Tanggal)'=>$tahunInput));
					$jumlahCutiBersama = $listCutiBersama->num_rows();
					
					fire_print('log','jumlahCutiBersama:'.$jumlahCutiBersama);
					
					//Hitung jumlah cuti karyawan yang hutang cuti pada tahun input.
					$jumlahHutangCuti = 0;					
					$listHutangCuti = $this->cutiJenisTidakHadir_model->getHutangCutiTahunan($dataUser->NPK,$tahunInput);
					if($listHutangCuti)
					{
						foreach($listHutangCuti as $hutangCuti)
						{
							$jumlahHutangCuti += $this->cuti_model->getHariKerja($hutangCuti->TanggalMulai,$hutangCuti->TanggalSelesai);
						}
					}
					
					/* $listCutiUserTahunLalu = $this->db->get_where('mstrcutiuser',array('Deleted'=>'0','NPK'=>$dataUser->NPK,'Tahun'=>$tahunInput-1));
					if($listCutiUserTahunLalu->num_rows() > 0)
					{
						$akumulasiCutiTahunLalu = $listCutiUserTahunLalu->row(1)->AkumulasiCutiTahunan;
						$akumulasiCutiBesar = $listCutiUserTahunLalu->row(1)->AkumulasiCutiBesar;
					} */
					
					$akumulasiCutiTahunan = $jumlahCutiTahunanAwal - $jumlahCutiBersama - $jumlahHutangCuti;
					fire_print('log','akumulasiCutiTahunan:'.$akumulasiCutiTahunan);
					//hitung apakah dapat cuti besar? jika tahun input - tahun masuk lalu mod 5 = 0, maka berarti dapat cuti besar
					fire_print('log',"penentu cuti besar.Rms: ( $tahunInput - $tahunTanggalBekerja ) % 5 = ". ($tahunInput - $tahunTanggalBekerja)%5 );
					$success .= ("\n $dataUser->NPK : ( $tahunInput - $tahunTanggalBekerja ) % 5 = ". ($tahunInput - $tahunTanggalBekerja)%5);
					if(($tahunInput - $tahunTanggalBekerja)%5 == 0)
					{
						$JumlahCutiBesarAwal = 22;
						$akumulasiCutiBesar = 22;
					}
					
					if($akumulasiCutiTahunan < 0)
					{
						if($listHutangCuti)
						{
							foreach($listHutangCuti as $hutangCuti)
							{
								if($akumulasiCutiTahunan < 0)
								{
									$this->db->update('cutijenistidakhadir',array('TahunCutiYangDipakai'=>$tahunInput+1),array('KodeCutiJenisTidakHadir'=>$hutangCuti->KodeCutiJenisTidakHadir));
								}
								$akumulasiCutiTahunan++;
							}
						}
						$akumulasiCutiTahunan = 0;
					}
					
					$data = array(
						"NPK"=> $dataUser->NPK,
						"Tahun" =>$tahunInput,
						"JumlahCutiTahunanAwal"=>$jumlahCutiTahunanAwal, 
						"AkumulasiCutiTahunan"=>$akumulasiCutiTahunan, 
						"JumlahCutiBesarAwal"=>$JumlahCutiBesarAwal, 
						"AkumulasiCutiBesar"=>$akumulasiCutiBesar, 
						"CreatedOn"=> date('Y-m-d H:i:s'), 
						"CreatedBy" => $this->npkLogin
					);
					$this->db->insert('mstrcutiuser',$data);					
				}
				$resultQ1 = $this->db->query("insert into cuti(KodeCuti,Deleted,NPK,TanggalMulai,TanggalSelesai,StatusApproval,CreatedOn,CreatedBy) 
				SELECT CONCAT('CB', DATE_FORMAT(l.Tanggal, '%y%m%d'), LEFT(NPK, 3) -- + case when NPK like '%OS' or NPK like '%PK%' then '900' else '100' end
)
						  AS KodeCuti,
					   '0' AS Deleted,
					   NPK,
					   l.Tanggal AS TanggalMulai,
					   l.Tanggal AS TanggalSelesai,
					   'APR' AS StatusApproval,
					   now() AS createdOn,
					   'Sys-CB' AS CreatedBy
				  FROM mstruser u CROSS JOIN harilibur l
				 WHERE     u.Deleted = 0
					   AND (u.TanggalBerhenti = '0000-00-00' OR u.TanggalBerhenti IS NULL)
					   AND l.Deleted = 0
					   AND l.Hari = 'cb'
					   AND YEAR(l.Tanggal) = ?
					   AND u.NPK NOT LIKE '%OS%'
             AND u.NPK NOT LIKE '%PKWT%'",$tahunInput);
				
				$resultQ2 = $this->db->query("insert into cutijenistidakhadir(Deleted,KodeCuti,KodeJenisTidakHadir,TahunCutiYangDipakai,Lokasi,TanggalMulai,TanggalSelesai,CreatedOn,CreatedBy)
				SELECT '0' AS Deleted,
					   CONCAT('CB', DATE_FORMAT(l.Tanggal, '%y%m%d'), LEFT(NPK, 3))
						  AS KodeCuti,
					   12 AS KodeJenisTidakHadir,
					   YEAR(CURDATE()) AS TahunCutiYangDipakai,
					   '' AS Lokasi,
					   l.Tanggal AS TanggalMulai,
					   l.Tanggal AS TanggalSelesai,
					   now() AS createdOn,
					   'Sys-CB' AS CreatedBy
				  FROM mstruser u CROSS JOIN harilibur l
				 WHERE     u.Deleted = 0
					   AND (u.TanggalBerhenti = '0000-00-00' OR u.TanggalBerhenti IS NULL)
					   AND l.Deleted = 0
					   AND l.Hari = 'cb'
					   AND YEAR(l.Tanggal) = ?
					   AND u.NPK NOT LIKE '%OS%'
             AND u.NPK NOT LIKE '%PKWT%'",$tahunInput);
				//$this->db->trans_rollback();
				$this->db->trans_commit();
				
				echo $success;
			}
			catch(Exception $e)
			{
				$this->db->trans_rollback();
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
				echo "Gagal";
			}
		}
		
		function ajax_submitCuti()
		{
			try
			{
				$success = "Berhasil";
				$npk = $this->input->post('npk');
				$editmode = $this->input->post('editmode');
				$kodeusertask = $this->input->post('kodeusertask');
				fire_print('log',"kodeusertask: $kodeusertask");
				if($editmode == 'true')
				{
					$dtCuti = $this->cutiJenisTidakHadir_model->getMinMaxTanggalCutiOnEdit($kodeusertask);
				}
				else
				{
					$dtCuti = $this->cutiJenisTidakHadir_model->getMinMaxTanggalCutiOnSubmit($npk);
				}
				
				fire_print('log','dtCuti : '.print_r($dtCuti,true));
				
				$this->db->trans_begin();
				$dataCuti = array(
					"KodeCuti" => generateNo('CT'),
					"NPK" => $this->npkLogin,
					"TanggalMulai" => $dtCuti['TanggalMulai'],
					"TanggalSelesai" => $dtCuti['TanggalSelesai'],
					"StatusApproval" => 'PEP',
					"CreatedBy" => $npk,
					"CreatedOn" => date('Y-m-d H:i:s')		
				);
				
				if($editmode == 'true')
				{
					$dataCuti['KodeCuti'] = $dtCuti['KodeCuti'];
					fire_print('log', 'dataCuti Edit :'. print_r($dataCuti,true));
					$this->db->update('cuti',$dataCuti,array('KodeCuti'=>$dtCuti['KodeCuti']));
					
					$this->usertask->updateStatusUserTask($kodeusertask,'AP',$this->npkLogin);
					
				}
				else
				{
					$this->db->insert('cuti',$dataCuti);
					
					$this->db->update('cutijenistidakhadir',array('KodeCuti'=>$dataCuti['KodeCuti']),array('CreatedBy'=>$npk,'KodeCuti'=>''));
				}
				
				$KodeCuti = $dataCuti['KodeCuti'];
				$KodeUserTask = generateNo('UT');
				$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeCuti,$KodeUserTask,"CT");
				
				if(!$hasilTambahUserTask)
				{
					//echo('gagal simpan usertask lembur');
					fire_print('log','insert cuti rollback');
					$this->db->trans_rollback();
					$success = 'Gagal menambahkan ke dalam sistem';
				}else{
					fire_print('log','insert cuti commit');
					$this->db->trans_commit();					
				}
				echo $success;
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_cancelCuti()
		{
			try
			{
				$success = "Berhasil";
				$npk = $this->input->post('npk');
				$this->db->trans_start();
				
				$this->db->delete('cutijenistidakhadir',array('CreatedBy'=>$npk,'KodeCuti'=>''));
				
				$this->db->trans_complete();
				
				if ($this->db->trans_status() === FALSE)
				{
					$success = "Gagal";
				}
				
				echo $success;
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}		
		
		function ajax_prosesApproval()
		{
			try
			{
				$success = "Berhasil";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				$query = $this->cuti_model->getCuti($KodeUserTask);
				if($query){
					foreach($query as $row){
						$statusRencana = substr($row->StatusApproval, -1);
						$KodeCuti = $row->KodeCuti;
						$pengajuCuti = $row->NPK;
						$statusCuti = substr($row->StatusApproval, 0, 2); //dapatkan PE
					}
				}else{
					$success = "Tidak ada data";
				}
				
				if($statusCuti == 'PE')
				{
					$this->db->trans_begin();
					$statusBaru = $status.$statusRencana;
					
					//update status cuti
					if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'CT') && $status == 'AP')
					{
						$this->cuti_model->updateStatusCuti($KodeUserTask,$status.$statusRencana,$this->npkLogin);
					}
					
					if($status == 'DE')
					{
						$this->cuti_model->updateStatusCuti($KodeUserTask,$status.$statusRencana,$this->npkLogin);
					}
					
					//update user task sekarang
					$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin, $catatan);
					
					//create user task baru untuk next proses
					$KodeUserTaskNext = generateNo('UT');			
					if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeCuti,$KodeUserTaskNext,"CT".$status,$pengajuCuti))
					{
						$success = "0";
					}
					
					if ($this->db->trans_status() === FALSE)
					{
						fire_print('log','trans rollback approve cuti ');
						$this->db->trans_rollback();
					}
					else
					{
						fire_print('log','trans commit approve cuti ');
						$this->db->trans_commit();
					}
					fire_print('log','Proses Trans Approve Cuti selesai ');
				}
				echo $success;
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_prosesApprovalRealisasi()
		{
			try
			{
				$success = "Gagal";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				
				$query = $this->cuti_model->getCuti($KodeUserTask);
				if($query){
					foreach($query as $row){
						$statusRencana = 'R'; //substr($row->StatusApproval, -1);
						$KodeCuti = $row->KodeCuti;
						$pengajuCuti = $row->NPK;
						$tanggalSelesaiCuti = $row->TanggalSelesai;
					}
				}else{
					$success = "Tidak ada data";
				}
				fire_print('log',"tanggalSelesaiCuti : $tanggalSelesaiCuti vs ". date("Y-m-d"));
				if($tanggalSelesaiCuti < date("Y-m-d")){
				
					$this->db->trans_start();
					$statusBaru = $status.$statusRencana;
					
					//update status cuti
					if($this->cuti_model->updateStatusCuti($KodeUserTask,$status.$statusRencana,$this->npkLogin))
					{
						//update user task sekarang
						$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin);
						
						//update akumulasi cuti jika Approve
						if($status == "AP"){
							$this->cuti_model->updateAkumulasiCuti($KodeUserTask,$this->npkLogin);		
							$success = "Berhasil";
						}else if($status == "DE"){
							$success = "Berhasil Decline";
						}
					}
					else
					{
						$success = "Sistem Gagal";
					}
					
					$this->db->trans_complete();
				}else{
					$success = "Realisasi harus dilakukan setelah tanggal cuti terakhir dilakukan";
				}
				
				echo $success;				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ViewRencanaCuti($KodeUserTask)
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("39"))
					{
						$datacuti = $this->cuti_model->getCuti($KodeUserTask);
						
						foreach($datacuti as $row){	
							$dataJenisTidakHadir = $this->cuti_model->getJenisTidakHadir($row->KodeCuti);
						
							$historycatatan = $this->usertask->getCatatan($row->KodeCuti);
							
							$hasil = $this->cuti_model->getCutiUserForTrx($row->NPK);
							$sisacutitahunan = $hasil['sisacutitahunan'];
							$sisacutibesar = $hasil['sisacutibesar'];
							
							$data = array(
								'realisasi' => '0',
								'title' => 'View Rencana Cuti',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'npk' => $row->NPK,
								'nama' => $row->Nama,
								'sisacutitahunan' => $sisacutitahunan,
								'sisacutibesar' => $sisacutibesar,
								//'tanggalmulai' => date('j F Y',strtotime($row->TanggalMulai)),
								//'tanggalselesai' => date('j F Y',strtotime($row->TanggalSelesai)),
								//'jumlahhari' => $this->getHariKerja($row->TanggalMulai,$row->TanggalSelesai),
								//'jenistidakhadir' => $dataJenisTidakHadir,
								'historycatatan' => $historycatatan
							);
						}
						$this->load->helper(array('form','url'));
						$this->template->load('default','Cuti/rencanaCuti_view',$data);
					}
				}
				else
				{
					redirect("login?u=Cuti/cutiController/ViewRencanaCuti/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function approvalMasterJatahCuti($KodeUserTask)
		{
			try
			{
				if(!$this->session->userdata('logged_in'))
				{
					redirect("login?u=Cuti/cutiController/approvalMasterJatahCuti/$KodeUserTask",'refresh');				
				}
				else
				{
					if(check_authorized("40"))
					{
						$dataTempMasterJatahCuti = $this->cuti_model->getTempMasterJatahCuti($KodeUserTask);
						//$historycatatan = $this->usertask->getCatatan($row->KodeTempMasterCutiUser);
						if($dataTempMasterJatahCuti)
						{
							foreach($dataTempMasterJatahCuti as $row)
							{
								$dataTempUserDetail = array(
									'KodeTempMasterCutiUser' => $row->KodeTempMasterCutiUser,
									'nama' => $row->Nama,
									'TempNPK' => $row->TempNPK,
									'tahun'=> $row->Tahun,
									'kodeusertask' => $KodeUserTask,
									'JumlahCutiTahunanAwal'=> $row->JumlahCutiTahunanAwal,
									'AkumulasiCutiTahunan'=> $row->AkumulasiCutiTahunan,
									'JumlahCutiBesarAwal'=> $row->JumlahCutiBesarAwal,
									'AkumulasiCutiBesar'=> $row->AkumulasiCutiBesar,
									'StatusTransaksi'=> $row->StatusTransaksi,
									'title' => 'Approval Jatah Cuti Karyawan'//,
									//'historycatatan' => $historycatatan
								);
								//fire_print('log',$row->StatusKawin);
								$NPKSelectedUser = $row->TempNPK;
								$TahunSelectedUser=$row->Tahun;
							}
						}
						
						$dataMasterCuti = $this->cuti_model->getCutiUserLast($NPKSelectedUser,$TahunSelectedUser);	

						
						if($dataMasterCuti)
						{
							foreach($dataMasterCuti as $dataMasterCuti)
							{
								$dataUserDetail = array(
									'KodeMasterCutiUser' => $dataMasterCuti->KodeMasterCutiUser,
									'npk' => $dataMasterCuti->NPK,
									'tahun'=> $dataMasterCuti->Tahun,
									'JumlahCutiTahunanAwal'=> $dataMasterCuti->JumlahCutiTahunanAwal,
									'AkumulasiCutiTahunan'=> $dataMasterCuti->AkumulasiCutiTahunan,
									'JumlahCutiBesarAwal'=> $dataMasterCuti->JumlahCutiBesarAwal,
									'AkumulasiCutiBesar'=> $dataMasterCuti->AkumulasiCutiBesar
									
									
								);
								//fire_print('log','data lama :' .$dataUser->StatusKawin);
							}
						}
						
						$data = array(
							'title' =>'Approval Jatah Cuti Karyawan '.$NPKSelectedUser,
							'databaru'=> $dataTempUserDetail,
							'datalama'=>$dataUserDetail 
							
						);
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','Cuti/approvalMasterJatahCuti_view',$data);
					}
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
			
		}
		
		function ProsesApprovalMasterCutiUser($KodeTempMasterCutiUser,$StatusApp,$npk,$tahunkary)
		{
			try
			{
				$query = $this->db->get_where('dtltrkrwy',array('NoTransaksi'=>$KodeTempMasterCutiUser));
				foreach($query->result() as $row)
				{
					$KodeUserTask = $row->KodeUserTask;
				}
				
				$this->db->trans_start();
				
				//tempmstruser di buat status AP/DE
				$this->db->where('KodeTempMasterCutiUser',$KodeTempMasterCutiUser);
				$this->db->update('tempmstrcutiuser',array(
					'StatusTransaksi' => $StatusApp,
					'UpdatedOn' => date('Y-m-d H:i:s'),
					'UpdatedBy' => $this->npkLogin
				));
				
				//insert data dari tempmaster ke master klo AP
				if($StatusApp == "AP")
				{
					$this->cuti_model->updateMasterCutiFromTemp($KodeTempMasterCutiUser,$StatusApp);
				}
				//klo decline insert data dari master ke temp 
				if($StatusApp == "DE")
				{
					$this->cuti_model->InsertMasterCutiToTemp($npk,$tahunkary);					
				}
				
				//user task dibuat status AP/DE
				$this->usertask->updateStatusUserTask($KodeUserTask,$StatusApp,$this->npkLogin);
				
				
				
				
				
				$this->db->trans_complete();
				//kirim email status perubahan ke requester
				/*if($this->db->trans_status() === TRUE)
				{
					$query = $this->db->get_where('usertasks',array('KodeUserTask'=>$KodeUserTask));
					foreach($query->result() as $row)
					{
						$Requester = $row->Requester;
					}
					if($this->config->item('enableEmail') == 'true')
					{
						if($this->sendEmailToUser($StatusApp,$Requester))
						{
							echo "Data sudah berhasil disimpan.";
						}
						else
						{
							echo "Data berhasil disimpan tapi gagal mengirim email.";
						}
					}
					else
					{
						echo "Data sudah berhasil disimpan.";
					}
				}*/
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function cetakFormCutiHead($Peruntukan,$NoTransaksi)
		{
			try
			{
				if(!$this->session->userdata('logged_in'))
				{
					redirect("login?u=Cuti/cutiController/cetakFormCutiHead/$NoTransaksi",'refresh');				
				}
				else
				{
					
						$dataUserYangCuti = $this->cuti_model->getCutiByNoTransaksi($NoTransaksi);						
						if($dataUserYangCuti)
						{
							foreach($dataUserYangCuti as $row)
							{
								$dataUser = array(
									'KodeCuti' => $row->KodeCuti,
									'Nama' => $row->Nama,
									'NPK' => $row->NPK,
									'NamaDepartemen'=> $row->NamaDepartemen,							
									'title' => 'Print Form Cuti'
								);								
								$NPKSelectedUser = $row->NPK;
							}
						}
						
						$dataCutiUser = $this->cuti_model->getCutiBreakDown($NoTransaksi);						
						if($dataCutiUser)
						{
							foreach($dataCutiUser as $row)
							{	
								$dataMinMaxTanggalCuti = $this->cutiJenisTidakHadir_model->getMinMaxTanggalCutiDetail($row->KodeCutiJenisTidakHadir);						
								$TanggalMulaiDetail = $dataMinMaxTanggalCuti['TanggalMulai'];
								$TanggalSelesaiDetail = $dataMinMaxTanggalCuti['TanggalSelesai'];						
								$jumlahHariDetail = $this->cuti_model->getHariKerja($TanggalMulaiDetail,$TanggalSelesaiDetail);
								$dataJenisTidakHadirDetail = $this->cuti_model->getJenisTidakHadirDetail($row->KodeCutiJenisTidakHadir);
								
								
								$dataCutiUserDetail[] = array(
									'KodeCuti' => $row->KodeCuti,
									'KodeCutiJenisTidakHadir' => $row->KodeCutiJenisTidakHadir,
									'KodeJenisTidakHadir' => $row->KodeJenisTidakHadir,
									'TahunCutiYangDipakai' => $row->TahunCutiYangDipakai,
									'TanggalMulaiDetail' => $TanggalMulaiDetail,
									'TanggalSelesaiDetail' => $TanggalSelesaiDetail,
									'jumlahHariDetail' => $jumlahHariDetail,
									'dataJenisTidakHadirDetail' => $dataJenisTidakHadirDetail
								);		
							}
						}
						
						
						$dataMinMaxTanggalCutiGlobal = $this->cutiJenisTidakHadir_model->getMinMaxTanggalCuti($NoTransaksi);						
						$TanggalMulai = $dataMinMaxTanggalCutiGlobal['TanggalMulai'];
						$TanggalSelesai = $dataMinMaxTanggalCutiGlobal['TanggalSelesai'];						
						$jumlahHariGlobal = $this->cuti_model->getHariKerja($TanggalMulai,$TanggalSelesai);
						
						
						$hasil = $this->cuti_model->getCutiUserForTrx($this->npkLogin);
						$sisacutitahunan = $hasil['sisacutitahunan'];
						$sisacutibesar = $hasil['sisacutibesar'];	

						$dataAtasanUser = $this->user->dataUser($NPKSelectedUser);	
						if($dataAtasanUser)
						{
							foreach($dataAtasanUser as $row){
							$dataAtasan = array(
								'Atasan'=> $row->atasan
							);
						}
						}
												
						
						$data = array(
							'title' =>'Cetak Form Cuti'.$NPKSelectedUser,
							'dataUserYangCuti'=> $dataUser,
							'datacutiuserdetail'=>$dataCutiUserDetail, 
							'TanggalMulai'=>$TanggalMulai,
							'TanggalSelesai'=>$TanggalSelesai,
							'jumlahHariGlobal'=>$jumlahHariGlobal,							
							'sisacutitahunan'=>$sisacutitahunan,
							'sisacutibesar'=>$sisacutibesar,
							'dataAtasan'=>$dataAtasan
						);
						
						$this->load->helper(array('form','url'));						
						if ($Peruntukan == "Head")
						{$this->load->view('Cuti/cetakFormCutiHead_view',$data);}
						else if ($Peruntukan == "Spesial")
						{$this->load->view('Cuti/cetakFormCutiSpesial_view',$data);}
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
			
		}
		
		
		
		
	}
?>