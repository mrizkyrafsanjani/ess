<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class TempMasterCutiKaryawan extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('cuti_model','',TRUE);
		$this->load->library('grocery_crud');
		$this->load->model('usertask','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('dtltrkrwy_model','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("40"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_tempmastercutikaryawan();
			}
		}else{
			redirect('login','refresh');
		}
    }
	
	public function _tempmastercutikaryawan($page='',$nama='',$tahun='',$bulan='',$KodeUserTask='')
    {
		$crud = new grocery_crud();
		$crud->set_subject('Master Cuti Karyawan');
		//$crud->set_theme('datatables');
		
        $crud->set_table('mstrcutiuser');
		$crud->set_relation('NPK','mstruser','Nama',array('Deleted'=>'0'));
		$crud->where('mstrcutiuser.deleted','0');
		//$crud->where('mstrcutiuser.StatusTransaksi','AP');
		
		$crud->columns('NPK','Tahun','JumlahCutiTahunanAwal','JumlahCutiBesarAwal','AkumulasiCutiTahunan','AkumulasiCutiBesar');
		$crud->fields('NPK','Tahun','JumlahCutiTahunanAwal','JumlahCutiBesarAwal','AkumulasiCutiTahunan','AkumulasiCutiBesar');
		
		$crud->display_as('NPK','Nama');
		$crud->display_as('JumlahCutiTahunanAwal','Jumlah Cuti Tahunan Awal');
		$crud->display_as('JumlahCutiBesarAwal','Jumlah Cuti Besar Awal');	
		$crud->display_as('AkumulasiCutiTahunan','Akumulasi Cuti Tahunan');	
		$crud->display_as('AkumulasiCutiBesar','Akumulasi Cuti Besar');			
		
		$crud->required_fields('NPK','Tahun','JumlahCutiTahunanAwal','JumlahCutiBesarAwal','AkumulasiCutiTahunan','AkumulasiCutiBesar');
				
		$crud->callback_insert(array($this,'_insert'));		
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete_tempmastercutikaryawan'));
		
		$crud->unset_delete();
		//$crud->unset_back_to_list();
		$crud->set_lang_string('insert_success_message',
			'Data sudah dikirimkan ke atasan Anda untuk diapproval.'
		);
		$crud->set_lang_string('update_success_message',
			'Data sudah dikirimkan ke atasan Anda untuk diapproval.'
		);

		$js="<script>
		$(document).ready(function() {
			$('#form-button-save').hide();
		});
		</script>";

		$output = $crud->render();
		$output->output.=$js;
        $this-> _outputview($output,$page);
    }
 
    function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Master Cuti Karyawan',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		fire_print('log','page cuti: '.$page);
		
		$this->template->load('default','cuti/tempMasterCuti_view',$data);
		
    }
	
	function _insert($post_array)
	{
		
		
		$this->db->trans_start();
		
		$KodeTempMasterCutiUser = generateNo('MC');
		$NPKyangDiubah = $post_array['NPK'];
		unset($post_array['NPK']);
		$post_array['TempNPK'] = $NPKyangDiubah;
		
		$post_array['KodeTempMasterCutiUser'] = $KodeTempMasterCutiUser;
		$post_array['StatusTransaksi'] = 'PE';
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		$this->db->insert('tempmstrcutiuser',$post_array);
		
		/*$this->db->trans_complete(); 
		return $this->db->trans_status();		*/

		$KodeUserTask = generateNo('UT');
		fire_print('log',$KodeUserTask);
		//$hasilTambahUserTask = $this->usertask->tambahUserTask($NPKyangDiubah,$KodeTempMasterCutiUser,$KodeUserTask,"MC");
		fire_print('log',$hasilTambahUserTask);
		
		$hasilTambahUserTask = $this->usertask->tambahUserTask($NPKyangDiubah,$KodeTempMasterCutiUser,$KodeUserTask,"MC");
		
		if(!$hasilTambahUserTask)
		{
			//echo('gagal simpan usertask ');
			fire_print('log','insert cuti rollback');
			$this->db->trans_rollback();
			return false;
		}else{
			fire_print('log','insert cuti commit');
			$this->db->trans_commit();
			return true;
		}
		
	}
	
	function _update($post_array, $primary_key)
	{
		try
		{
			$this->db->trans_start();
			$KodeTempMasterCutiUser = generateNo('MC');
			$NPKyangDiubah = $post_array['NPK'];
			unset($post_array['NPK']);
			$post_array['TempNPK'] = $NPKyangDiubah;
			$post_array['KodeTempMasterCutiUser'] = $KodeTempMasterCutiUser;
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			$post_array['StatusTransaksi'] = 'PE';
			
			$this->db->insert('tempmstrcutiuser',$post_array);
			//$this->db->update('tempmstrcutiuser',$post_array,array('IDTempMasterCutiUser'=>$primary_key));
			
			
			$KodeUserTask = generateNo('UT');
			fire_print('log',$KodeUserTask);
			
			fire_print('log',$hasilTambahUserTask);
			
			$hasilTambahUserTask = $this->usertask->tambahUserTask($NPKyangDiubah,$KodeTempMasterCutiUser,$KodeUserTask,"MC");
			
			if(!$hasilTambahUserTask)
			{
				//echo('gagal simpan usertask ');
				fire_print('log','insert cuti rollback');
				$this->db->trans_rollback();
				return false;
			}else{
				fire_print('log','insert cuti commit');
				$this->db->trans_commit();
				return true;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function _delete_tempmastercutikaryawan($primary_key){
		$this->db->trans_start();
		$post_array['Deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		
		$this->db->update('tempmstrcutiuser',$post_array,array('IDTempMasterCutiUser'=>$primary_key));
		
		$this->db->trans_complete(); 
		return $this->db->trans_status();
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */