<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class UbahPassword extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('menu','',TRUE);			
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				//$dataUser = $this->user->dataUser($session_data['npk']);
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}
				/*
				if($dataUser){
					$dataUser_array = array();
					foreach($dataUser as $dataUser){
						$dataUserDetail = array(
							'title'=> 'Input Form SPD',
							'menu_array' => $menu_array,
							'npk' => $dataUser->npk,
							 'golongan' => $dataUser->golongan,
							 'nama'=> $dataUser->nama,
							 'jabatan'=> $dataUser->jabatan,
							 'departemen'=> $dataUser->departemen,
							 'atasan'=> $dataUser->atasan,
							 'uangsaku'=> $dataUser->uangsaku,
							 'uangmakan'=> $dataUser->uangmakan					 
						);
					}
				}else{
					echo 'gagal2';
				}
				*/
				$data = array(
					   'title'=>'Ubah Password',
					   'npk' => $session_data['npk'],
					   'nama' => $session_data['nama'],
					   'menu_array' => $menu_array
				  );
				$this->load->helper(array('form','url'));
				//$this->load->view('home_view', $data);
				$this->template->load('default','ubahPassword_view',$data);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
	}
?>