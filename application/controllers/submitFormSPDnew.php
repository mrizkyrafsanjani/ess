<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');

class SubmitFormSPDnew extends CI_Controller {

	 function __construct()
	 {
	   parent::__construct();
	   $this->load->model('trkSPD','',TRUE);
	   $this->load->model('pelimpahanwewenang_model','',TRUE);
	   $this->load->model('uangmuka_model','',TRUE);
	   $this->load->model('user','',TRUE);
	   $session_data = $this->session->userdata('logged_in');
	 }

	 function index()
	 {
	   //This method will have the credentials validation
	   $this->load->library('form_validation');

	   $this->form_validation->set_rules('rbDPA', 'DPA', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtTanggalBerangkat', 'Tanggal Berangkat', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtTanggalKembali', 'Tanggal Kembali', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtTujuan', 'Tujuan', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtAlasan', 'Alasan Perjalanan', 'trim|required|xss_clean');   
	   $this->form_validation->set_rules('rbUangMuka', 'Uang Muka', 'trim|required|xss_clean');
	   
	   $this->form_validation->set_rules('txtKetUangSaku', 'Keterangan Uang Saku', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariUangSaku', 'Jumlah Hari Uang Saku', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetUangMakan', 'Keterangan Uang Makan', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianUangMakan', 'Beban Harian Uang Makan', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariUangMakan', 'Jumlah Hari Uang Makan', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetHotel', 'Keterangan Hotel', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianHotel', 'Beban Harian Hotel', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariHotel', 'Jumlah Hari Hotel', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetTaksi', 'Keterangan Taksi', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianTaksi', 'Beban Harian Taksi', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetAirport', 'Keterangan Airport', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianAirport', 'Beban Harian Airport', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetLain1', 'Keterangan Lain-lain 1', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain1', 'Beban Harian Lain-lain 1', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetLain2', 'Keterangan Lain-lain 2', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain2', 'Beban Harian Lain-lain 2', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKetLain3', 'Keterangan Lain-lain 3', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain3', 'Beban Harian Lain-lain 3', 'trim|xss_clean');
	   
	   /*
	   
	   $this->form_validation->set_rules('txtKetUangSaku', 'Keterangan Uang Saku', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariUangSaku', 'Jumlah Hari Uang Saku', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetUangMakan', 'Keterangan Uang Makan', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianUangMakan', 'Beban Harian Uang Makan', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariUangMakan', 'Jumlah Hari Uang Makan', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetHotel', 'Keterangan Hotel', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianHotel', 'Beban Harian Hotel', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtJmlhHariHotel', 'Jumlah Hari Hotel', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetTaksi', 'Keterangan Taksi', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianTaksi', 'Beban Harian Taksi', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetAirport', 'Keterangan Airport', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianAirport', 'Beban Harian Airport', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetLain1', 'Keterangan Lain-lain 1', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain1', 'Beban Harian Lain-lain 1', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetLain2', 'Keterangan Lain-lain 2', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain2', 'Beban Harian Lain-lain 2', 'trim|numeric|xss_clean');
	   $this->form_validation->set_rules('txtKetLain3', 'Keterangan Lain-lain 3', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBebanHarianLain3', 'Beban Harian Lain-lain 3', 'trim|numeric|xss_clean');
	   
	   */
	 	$session_data = $this->session->userdata('logged_in');
		$NPKPemohon = $session_data['npk'];

	   if($this->form_validation->run() == false)
	   {
		 //Field validation failed.  User redirected to login page
		 $this->load->view('formSPDnew_view');
	   }
	   else
	   {	 
		 //Go to private area
		 if($this->input->post('submitFormSPDnew')){
			$DPA = $this->input->post('rbDPA');
			$UM = $this->input->post('rbUangMuka');
			$PW = $this->input->post('rbPelimpahanWewenang');
			$noSPD = $this->generateNoSPD($DPA);
			$noPW = $this->generateNoPW();
			$noUangMuka = $this->generateNoUangMuka($DPA,$NPKPemohon);
			if($this->trkSPD->insertSPD($noSPD) && $this->trkSPD->insertDtlBiayaSPD($noSPD) ){
				//redirect('formSPD','refresh');
				if ($PW=='1')
				{	$this->trkSPD->insertPelimpahanWewenang($noSPD,$noPW);}		
				if ($UM=='1')
				{$this->uangmuka_model->insertUangMuka($noUangMuka,$noSPD);}
				redirect('cetakFormSPDnew?NoSPD='.$noSPD.'&NoUangMuka='.$noUangMuka,'refresh');				
			}else{
				echo('gagal save');
				}
		 }
	   }
	}

	function generateNoSPD($DPA){
		$noSPD_model = $this->trkSPD->getLastNoSPD($DPA);
		if($noSPD_model){
			$noSPD_array = array();
			foreach($noSPD_model as $row){
			   $noSPD_array = array(
				 'noSPD' => $row->noSPD
			   );
			}
			if (substr($noSPD_array['noSPD'],16,4) == date("Y")){
				$nomorBlkgSPD = substr($noSPD_array['noSPD'],9,3);
				$nomorBlkgSPD += 1;
				$noSPD = "SPD/DPA".$this->input->post('rbDPA')."/".str_pad($nomorBlkgSPD,3,"0",STR_PAD_LEFT).date("/m/Y");
			}else{
				$noSPD = "SPD/DPA".$this->input->post('rbDPA')."/001".date("/m/Y");
			}
		}else{
			$noSPD = "SPD/DPA".$this->input->post('rbDPA')."/001".date("/m/Y");
		}
		
		return $noSPD;
	}

	function generateNoPW(){
		$lastNo = $this->trkSPD->getLastKodePW();						
		if ($lastNo != "0")
		{				
			$array = explode("-", $lastNo);					
			$bulanFPWRoman = $array[2];
			$NoUrut = (int) $array[4];
			$bulanFPWInt = $this->trkSPD->NumberFromRoman($bulanFPWRoman);
		}

		if($bulanFPWInt != date('m') || $lastNo == "0")
		{
			$NextNo = "001";	
		}
		else
		{
			$NextNo = (int)$NoUrut + 1;
		}

		$noPW = 'FPW-DPA-'. $this->trkSPD->integerToRoman(date('n')).'-'.date('Y').'-'. str_pad($NextNo,3,"0",STR_PAD_LEFT);	
		
		return $noPW;
	}

	function generateNoUangMuka($DPA,$NPKPemohon){
		$lastNo = $this->uangmuka_model->getLastNoUangMuka();
		$initdept =	$this->uangmuka_model->getInitDept($NPKPemohon);	
		$bulanFUMInt =0 ;				
		if ($lastNo != "0")
		{				
			$array = explode("/", $lastNo);					
			$bulanFUMRoman = $array[4];
			$NoUrut = (int) $array[3];
			$bulanFUMInt = $this->uangmuka_model->NumberFromRoman($bulanFUMRoman);
		}


		if($bulanFUMInt != date('m') || $lastNo == "0")
		{
			$NextNo = "001";	
		}
		else
		{
			$NextNo = (int)$NoUrut + 1;
		}

		$noUM = 'UM/DPA'. $DPA.'/'. $initdept.'/'. str_pad($NextNo,3,"0",STR_PAD_LEFT).'/'. $this->uangmuka_model->integerToRoman(date('n')).'/'.date('Y');	
		
		return $noUM;
	}
	

}
?>