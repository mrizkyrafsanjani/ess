<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class MobilRuangMeetingController extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('requestMobilRuangMeeting_Model','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('departemen','',TRUE);
		$this->load->model('mstrgolongan','',TRUE);
		$this->load->model('user','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Peminjaman Mobil dan Ruang Meeting"))//76 Peminjaman Mobil dan Ruang Meeting
			{
				$this->npkLogin = $session_data['npk'];
				
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	function ApprovalMobilRuangMeeting($KodeUserTask='')
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Peminjaman Mobil dan Ruang Meeting"))//76
					{
						fire_print('log',"KodeUserTask : $KodeUserTask");
						$data = $this->requestMobilRuangMeeting_Model->getMobilRuangMeeting($KodeUserTask);
			
						foreach($data as $row){
							$historycatatan = $this->usertask->getCatatan($row->KodeRequestMobilRuangMeeting);
							
							$identitas = $this->user->dataUser($row->CreatedBy);
							foreach($identitas as $datauser){
								$nama = $datauser->nama;
								$departemen = $datauser->departemen;
							}
							$request = array(
								'realisasi' => '1',
								'title' => 'Realisasi Request Mobil dan Ruang Meeting',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'nama' => $nama,
								'departemen' => $departemen,
								'npk' => $row->NPK,
								'request' => $row->JenisRequest,
								'tanggal' => $row->Tanggal,
								'jammulai' => $row->JamMulai,
								'jamselesai' => $row->JamSelesai,
								'kegiatan' => $row->Kegiatan,
								'tujuan' => $row->Tujuan,
								'historycatatan' => $historycatatan
							);
							
						}
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','MobilRuangMeeting/requestMobilRuangMeeting_view',$request);
					}
				}
				else
				{
					redirect("login?u=MobilRuangMeeting/mobilRuangMeetingController/ApprovalMobilRuangMeeting/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		
		
		function ajax_prosesApprovalRealisasi()
		{
			try
			{
				$success = "Berhasil";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				
				$query = $this->requestMobilRuangMeeting_Model->getMobilRuangMeeting($KodeUserTask);
				if($query){
					foreach($query as $row){
						
						$KodeRequestMobilRuangMeeting = $row->KodeRequestMobilRuangMeeting;
						$requester = $row->NPK;
						$tanggal = $row->Tanggal;
						$jamSelesai = $row->JamSelesai;
					}
				}else{
					$success = "Tidak ada data";
				}
				fire_print('log',"tanggalSelesaiPinjamRM : $tanggal $jamSelesai vs ". date("Y-m-d H:i:s"));
				if(($tanggal . " ". $jamSelesai < date("Y-m-d H:i:s") && ($status == 'AP'))||$status == 'DE'){
					$this->db->trans_start();
					
					//update status request mobil dan ruang meeting
					if($this->requestMobilRuangMeeting_Model->updateStatusMobilRuangMeeting($KodeUserTask,$status,$this->npkLogin))
					{
						//update user task sekarang
						$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan);
						
					/* 	if($status == "DE"){
							
							
							//create user task baru untuk next proses
							$KodeUserTaskNext = generateNo('UT');			
							if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestMobilRuangMeeting,$KodeUserTaskNext,"RM".$status,$requester))
							{
								$success = "0";
							}
						} */
					}
					else
					{
						$success = "0";
					}
					$this->db->trans_complete();
				}
				else
				{
					$success = "Realisasi harus dilakukan setelah benar-benar realisasi ruang meeting/mobil terjadi";
				}
				
				echo $success;
				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function LaporanRequestKaryawan($KodeUserTask='') //View All Karyawan
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Peminjaman Mobil dan Ruang Meeting"))//76
					{
						if($KodeUserTask == '')
							$KodeUserTask = 'non';
						$data = array(
							'title' => 'Laporan Request All Karyawan',
							'admin' => '0',							
							'npk' => $this->npkLogin,
							'jenisrequest' => $this->requestMobilRuangMeeting_Model->getJenisRequest($this->npkLogin),
							'kodeusertask' => $KodeUserTask
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','MobilRuangMeeting/laporanRequest_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
	
		
}
?>