<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Mobil extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('usertask','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$this->load->library('grocery_crud');
		$this->load->helper('date');
		
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			
			if(check_authorizedByName("Master Mobil"))//60
			{
				$this->npkLogin = $session_data['npk'];
				$this->_Mobil();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	
	
	public function _Mobil()
    {
		
		try{
			
			
			$crud = new grocery_crud();
			//$crud->set_theme('datatables');
			
			$state = $crud->getState();
			
			$crud->set_subject('Mobil');
			$crud->set_table('mobil');
			$crud->columns('KodeMobil','NamaMobil','CreatedBy');
			$crud->fields('KodeMobil','NamaMobil');
			
			$crud->edit_fields('KodeMobil','NamaMobil');
			$crud->required_fields('NamaMobil');
			$crud->where('mobil.deleted','0');
			
			$crud->display_as('KodeMobil','Kode Mobil');
			$crud->display_as('NamaMobil','Nama Mobil');
			
			$crud->callback_field('KodeMobil',array($this,'field_callback_KodeMobil'));
			$crud->callback_insert(array($this,'_insert_mobil'));
			$crud->callback_update(array($this,'_update'));
			$crud->callback_delete(array($this,'_delete'));	
			
			
			$output = $crud->render();
			$this-> _outputview($output); 
		}
		
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
	
	function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Master Mobil',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert_mobil($post_array){
	
		try{
		$post_array['Deleted'] = '0';
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		
		$this->db->trans_begin();
		$this->db->insert('mobil',$post_array);
		
		$this->db->trans_commit();
	
			
		return true;
			
	}
	
		catch(Exception $e)
		{
			
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	
	}
	
	function _update($post_array,$primary_key){
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			
			$this->db->trans_commit();
			
			$this->db->update('mobil',$post_array,array('KodeMobil' => $primary_key));
			$KodeBarang = $primary_key;
			
		
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('mobil',$post_array,array('KodeMobil' => $primary_key));
	}
	
	function field_callback_KodeMobil($value= '', $primary_key = null)
	{
		return '<input type="text" name="KodeMobil" value="'.$value.'" readonly>';
	}
	

	
	

	
	
}
?>