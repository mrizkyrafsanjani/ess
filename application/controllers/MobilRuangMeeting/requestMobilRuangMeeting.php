<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class RequestMobilRuangMeeting extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('usertask','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->load->model('requestMobilRuangMeeting_Model','',TRUE);
		$this->npkLogin = $session_data['npk'];
		$this->load->library('grocery_crud');
		$this->load->helper('date');
		$this->load->library('email');

    }

    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Request Mobil / Ruang Meeting")) //83
			{
				$this->npkLogin = $session_data['npk'];
				$this->_RequestMobilRuangMeeting();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}


    }
	public function mobilRuangMeetingCalendar()
	{
		if($this->session->userdata('logged_in')){
			$data = array(
				'title' => 'Kalender Mobil Ruang Meeting',
				'body' => '',
				'tipe' => 'All'
				);

			$this->load->helper(array('form','url'));
			$this->template->load('default','MobilRuangMeeting/calendar_view',$data);
		}
		else
		{
			redirect(base_url().'?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}

	public function getCalendar($tipe = 'All')
	{
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Peminjaman Mobil dan Ruang Meeting"))//76
			{
				$session_data = $this->session->userdata('logged_in');
				//use this both as a parameter to get the event and place it to the calendar
				$start_time = $this->input->get('start');
				$end_time = $this->input->get('end');
				$tipe = str_replace("%20"," ", $tipe);
				$records = $this->requestMobilRuangMeeting_Model->getActiveRequest($start_time,$end_time, $tipe);
				foreach($records as $val){
					$style = "";
					$listUser = '';
					$status = "";
					$color = "#7F8C8D";
					$JenisRequest = $val->jenisrequest;
					$userTerkait = $this->requestMobilRuangMeeting_Model->getUserTerkait($val->id);
					if(is_array($userTerkait)){

						foreach ($userTerkait as $value) {
							# code...
							$listUser.= $value->Nama.',';
						}
						$listUser = substr($listUser,0,strlen($listUser)-1);

					}
					if($JenisRequest == "Mobil"){
						$style = $val->KodeMobilRuangMeeting;
						//if($style == 1){
						if($style == 1){
							$color = "#7F8C8D";
							$status = $val->Kegiatan.' | '.$listUser;
						//}else if($style == 2){
						}else if($style == 2){
							$color = "#2C3E50";
							$status = $val->Kegiatan.' | '.$listUser;
						}
					}
					if($JenisRequest == "Ruang Meeting"){
						$style = $val->KodeMobilRuangMeeting;
						if($style == 1){
							$color = "#C0392B";
							$status = $val->Kegiatan.' | '.$listUser;
						}else if($style == 2){
							$color = "#3498BD";
							$status = $val->Kegiatan.' | '.$listUser;
						}else if($style == 3){
							$color = "#2ECC71";
							$status = $val->Kegiatan.' | '.$listUser;
						}
					}

					$start = str_replace("-","/",substr($val->start,0,16));
					$end = str_replace("-","/",substr(str_replace("00:00:00","00:01:00",$val->end),0,16));
					$startend = substr($start, strlen($start)-5).'-'.substr($end, strlen($start)-5);
					$recordsfinal[] = array(
						'id' => $val->id,
						'title' => $val->title,
						'allDay' => false,
						'start' => $start,
						'end' => $end,
						'color' => $color,
						'status' => $startend.' '.$status,
						'url' => base_url()."index.php/MobilRuangMeeting/requestMobilRuangMeeting/index/read/". $val->id
					);
			}

			header("content-type: application/json");
			header("Access-Control-Allow-Origin: *");
			echo  json_encode($recordsfinal);
			}else{

				$message = "Maaf, anda tidak memiliki akses pada halaman ini.";
                echo "<script LANGUAGE='JavaScript'>
                    window.alert('$message');
                    window.location.href='".base_url()."';
                    </script>";
            }
		}else{
			redirect('Login?url='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	public function viewUser($tahun='',$bulan='',$JenisRequest='',$NPKuser='',$KodeUserTask='')
	{
		if($KodeUserTask == 'non')
			$KodeUserTask = '';
		if($JenisRequest == 'non')
			$JenisRequest = '';
		if($tahun == 'non')
			$tahun = date('Y');
		if($bulan == 'non')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Request Mobil / Ruang Meeting"))//83
			{
				$this->npkLogin = $session_data['npk'];
				$this->_RequestMobilRuangMeeting('viewUser',$JenisRequest,$tahun,$bulan,$KodeUserTask);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}

	public function viewAdmin($tahun='',$bulan='',$nama='non')
	{
		if($nama == 'non')
			$nama = 'xxx';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Request Mobil / Ruang Meeting"))//83
			{
				$this->npkLogin = $session_data['npk'];
				$this->_RequestMobilRuangMeeting('viewAdmin',$nama,$tahun,$bulan);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	public function _RequestMobilRuangMeeting($page='',$JenisRequest='',$tahun='',$bulan='',$KodeUserTask='',$tujuan='')
    {

		try{



			$crud = new grocery_crud();
			$crud->set_theme('datatables');

			$state = $crud->getState();
			/* $crud->columns('KodeRequestMobilRuangMeeting','NamaMobil','NamaRuangan','NamaRequester','Request','Status','Tanggal','JamMulai','JamSelesai','Kegiatan','Tujuan');	 */

			/* if($JenisRequest != '')
			{
				if($JenisRequest == "RuangMeeting"){
					$JenisRequest = "Ruang Meeting";
					$crud->columns('NPK','JenisRequest','KodeRuangMeeting','Tanggal','JamMulai','JamSelesai','Kegiatan');
					$crud->fields('NPK','JenisRequest','KodeRuangMeeting','Tanggal','JamMulai','JamSelesai','Kegiatan');
					$crud->add_fields('JenisRequest','KodeRuangMeeting','Tanggal','JamMulai','JamSelesai','Kegiatan');

				}
				else{
					$JenisRequest = "Mobil";
					$crud->columns('NPK','JenisRequest','KodeMobil','Tanggal','JamMulai','JamSelesai','Kegiatan','Tujuan');
					$crud->fields('NPK','JenisRequest','KodeMobil','Tanggal','JamMulai','JamSelesai','Kegiatan','Tujuan');
					$crud->add_fields('JenisRequest','KodeMobil','Tanggal','JamMulai','JamSelesai','Kegiatan','Tujuan');
				}
				$crud->like('requestmobilruangmeeting.JenisRequest',$JenisRequest);
			}
			 */

			$crud->set_subject('Request Mobil dan Ruang Meeting');
			$crud->set_table('requestmobilruangmeeting');


			$crud->fields('NPK','JenisRequest','KodeMobil','KodeRuangMeeting','Tanggal','JamMulai','JamSelesai','Kegiatan','Tujuan', 'UserTerkait');

			$crud->columns('NPK','JenisRequest','KodeMobil','KodeRuangMeeting','Tanggal','JamMulai','JamSelesai','Kegiatan','Tujuan','Status');
			//callback column untuk menginisialkan PE,DE,dan AP
			$crud->callback_column('Status',array($this,'_callback_statusRequest'));
			$crud->set_relation('KodeMobil','mobil','NamaMobil',array('deleted' => '0'));
			$crud->set_relation('KodeRuangMeeting','ruangmeeting','NamaRuang',array('deleted' => '0'));
			$crud->set_relation('NPK','mstruser','Nama',array('Deleted'=>'0'));
			$crud->set_relation_n_n('UserTerkait','userRequestMobilRuangMeeting','mstruser','KodeRequestMobilRuangMeeting','NPK','Nama','', array('deleted'=>'0','TanggalBerhenti'=>'0000-00-00') );

			$crud->where('requestmobilruangmeeting.deleted','0');
			//$crud->where("requestmobilruangmeeting.Status != 'DE'");


			$crud->add_fields('JenisRequest','KodeMobil','KodeRuangMeeting','Tanggal','JamMulai','JamSelesai','Kegiatan','Tujuan', 'UserTerkait');
			$crud->required_fields('JenisRequest','Tanggal','JamMulai','JamSelesai','Kegiatan', 'UserTerkait');


			$crud->display_as('NPK','Nama Requester');
			$crud->display_as('KodeMobil','Nama Mobil');
			$crud->display_as('KodeRuangMeeting','Nama Ruangan');
			$crud->display_as('JamMulai','Jam Mulai');
			$crud->display_as('JamSelesai','Jam Selesai');
			$crud->display_as('JenisRequest','Request');
			$crud->unset_texteditor('Kegiatan');
			$crud->unset_texteditor('Tujuan');


			$crud->callback_insert(array($this,'_insert_requestmobilruangmeeting'));
			$crud->callback_field('JenisRequest',array($this,'add_field_callback_JenisRequest'));


			$crud->callback_update(array($this,'_update'));
			$crud->callback_delete(array($this,'_delete'));

			$crud->callback_field('JamMulai',array($this,'field_callback_JamMulai'));
			$crud->callback_field('JamSelesai',array($this,'field_callback_JamSelesai'));

			$crud->set_rules('JamMulai','Jam Mulai','callback_validasiJamMulai');
			$crud->set_rules('JamSelesai','Jam Selesai','callback_validasiJamSelesai');
			$crud->set_rules('KodeMobil','Mobil','callback_validasiMobil');
			$crud->set_rules('KodeRuangMeeting','Ruang Meeting','callback_validasiRuangMeeting');
			$crud->set_rules('Tanggal','Tanggal','callback_validasiTanggal');
			//untuk akses berdasarkan kode role yang dimiliki user
			$currUserRole = $this->getCurrentUserKodeRole();
			if($currUserRole != "8" && $currUserRole != "1") //GA dan adminIT
			{
				$crud->unset_edit();
			}

			$crud->unset_delete();
			//$crud->unset_delete();
			$crud->unset_export();
			$crud->unset_print();
			//$crud->unset_back_to_list();
			/* $crud->unset_operations();
			$crud->unset_columns(arrays('Deleted','UpdateOn','UpdatedBy')); */


			$js = "
			<script>


			$(document).ready(function() {
				if('$state' == 'add') cekJam();
				$('#KodeRuangMeeting_field_box').hide();
				$('#KodeMobil_field_box').hide();
				var cmbJenisRequest = $('#field-JenisRequest');
				cmbJenisRequest.change(function(){
					if(cmbJenisRequest.val() == 'Mobil'){
						$('#KodeRuangMeeting_field_box').hide();
						$('#KodeMobil_field_box').show();
						$('#Tujuan_field_box').show();
					}else{
						$('#KodeRuangMeeting_field_box').show();
						$('#KodeMobil_field_box').hide();
						$('#Tujuan_field_box').hide();
					}
				});
			});

			function cekJam() {

				// setInterval(function () { time(); },100);

				Number.prototype.pad = function (len) {
					return (new Array(len+1).join('0') + this).slice(-len);
			}
				//function time(){
				var date= new Date();
				var dateh = date.getHours();
				var datem = date.getMinutes();
				var dates = date.getSeconds();
				var now = dateh.pad(2) + '' + datem.pad(2) + '' + dates.pad(2);
				var valfd = '2000-01-01 07:30:00 AM'
				var valf = new Date(valfd.substr(0, 4), valfd.substr(5, 2), valfd.substr(8, 2), valfd.substr(11, 2), valfd.substr(14, 2), valfd.substr(17, 2));
				var valfh = valf.getHours();
				var valfm = valf.getMinutes();
				var valfs = valf.getSeconds();
				var valfirst = valfh.pad(2) +''+ valfm.pad(2) +''+ valfs.pad(2);
				var valld = '2000-01-01 16:30:00 PM'
				var vall = new Date(valld.substr(0, 4), valld.substr(5, 2), valld.substr(8, 2), valld.substr(11, 2), valld.substr(14, 2), valld.substr(17, 2));
				var vallh = vall.getHours();
				var vallm = vall.getMinutes();
				var valls = vall.getSeconds();
				var vallast = vallh.pad(2) +''+ vallm.pad(2) +''+ valls.pad(2);
				$('#time').val(now);
				if(now < valfirst || now > vallast){
					$('#body').hide();
					alert('Input peminjaman mobil & ruangan hanya dapat dilakukan pada pukul 07.30 - 16.30');
					window.location.href='mobilruangmeetingcalendar';
				}

				//}
			 }



			</script>";

			fire_print('log',"jenis request: $JenisRequest, tahun: $tahun, bulan: $bulan");
			if($JenisRequest != '')
			{
				if($JenisRequest == "RuangMeeting"){
					$JenisRequest = "Ruang Meeting";
					$crud->columns('NPK','JenisRequest','KodeRuangMeeting','Tanggal','JamMulai','JamSelesai','Kegiatan','Status');
					$crud->edit_fields('JenisRequest','KodeRuangMeeting','Tanggal','JamMulai','JamSelesai','Kegiatan');
				}
				else{
					$crud->columns('NPK','JenisRequest','KodeMobil','Tanggal','JamMulai','JamSelesai','Kegiatan','Tujuan','Status');
					$crud->edit_fields('JenisRequest','KodeMobil','Tanggal','JamMulai','JamSelesai','Kegiatan','Tujuan');
				}
				$crud->like('requestmobilruangmeeting.JenisRequest',$JenisRequest);
			}

			if($tahun != '')
			{
				$crud->where('YEAR(requestmobilruangmeeting.Tanggal)',$tahun);
				$crud->unset_add();
			}
			if($bulan != '')
			{
				$crud->where('MONTH(requestmobilruangmeeting.Tanggal)', $bulan);
				$crud->unset_add();
			}

			if($state == 'add' || $state=="insert" || $state =="read"){
				$page = "add";
			}
			$output = $crud->render();
			$output->output.=$js;
			$this-> _outputview($output,$page);
		}

		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }

	function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		$this->load->helper(array('form','url'));

		$data = array(
			'title' => 'Master Request Mobil dan Ruang Meeting',
			'body' => $output,
			'tipe' => 'All',
			'page' => $page
		);
		if($page=='viewUser' || $page == 'viewAdmin')
		{
			$this->load->view('MobilRuangMeeting/mobilRuangMeetingSimple_view',$data);
		}else if($page == 'add' || $page == 'edit'){
			$this->template->load('default','templates/crud_view',$data);
		}
		else
		{
			$this->template->load('default','MobilRuangMeeting/calendar_view',$data);
		}


        //$this->load->view('pengaturanUser_view',$output);
    }

	function _insert_requestmobilruangmeeting($post_array){

		try{
			$post_array['NPK'] = $this->npkLogin;
			$post_array['Status'] = 'PE';
			$post_array['Deleted'] = '0';
			$post_array['CreatedOn'] = date('Y-m-d H:i:s');
			$post_array['CreatedBy'] = $this->npkLogin;

			$userTerkait_array = $post_array['UserTerkait'];
			unset($post_array['UserTerkait']);
			if($post_array['JenisRequest']=="Mobil"){
				$post_array['KodeRuangMeeting'] = 0;
			}
			if($post_array['JenisRequest']=="Ruang Meeting"){
				$post_array['KodeMobil'] = 0;
			}
			$KodeRequestMobilRuangMeeting = generateNo('RM');
			$post_array["KodeRequestMobilRuangMeeting"]= $KodeRequestMobilRuangMeeting;
			$this->db->trans_begin();
			$this->db->insert('requestmobilruangmeeting',$post_array);
			if($post_array['KodeMobil'] <> 0 || $post_array['KodeMobil'] <> null || $post_array['KodeMobil'] <> "" ){
			$NamaMobil = $this->requestMobilRuangMeeting_Model->getmobil($post_array['KodeMobil']);
			if($NamaMobil){
				$NamaMobil_array = array();
				foreach($NamaMobil as $row){
					 $NamaMobil_array[] = array(
					 'NamaMobil' => $row->NamaMobil,
					 );
				}
			}
			}
			if($post_array['KodeRuangMeeting'] <> 0 || $post_array['KodeRuangMeeting'] <> null || $post_array['KodeRuangMeeting'] <> "" ){
				$NamaRuang = $this->requestMobilRuangMeeting_Model->getRuangMeeting($post_array['KodeRuangMeeting']);
				if($NamaRuang){
					$NamaRuang_array = array();
					foreach($NamaRuang as $row){
						$NamaRuang_array[] = array(
						'NamaRuang' => $row->NamaRuang,
						);
					}
				}
			}

			//$KodeRequestMobilRuangMeeting = $this->requestMobilRuangMeeting_Model->getKodeRequest($this->npkLogin);
			fire_print('log',"Kode req mobil dan ruang meeting : $KodeRequestMobilRuangMeeting");

			$KodeUserTask = generateNo('UT');
			if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestMobilRuangMeeting,$KodeUserTask,"RM")){
				log_message( 'error', 'mohon periksa approval group, apakah user telah dimasukkan ke dalam approval group ApprovalRequestRuangMeetingMobil' );
				$this->db->trans_rollback();
				return false;
			}
			foreach ($userTerkait_array as $value) {
				$post_array2['Deleted'] = 0;
				$post_array2['KodeRequestMobilRuangMeeting'] = $post_array['KodeRequestMobilRuangMeeting'];
				$post_array2['NPK'] = $value;
				$post_array2['CreatedOn'] = date('Y-m-d H:i:s');
				$post_array2['CreatedBy'] = $this->npkLogin;
				$this->db->insert('userrequestmobilruangmeeting',$post_array2);
				$this->sendEmailToUser($post_array['NPK'],
					$post_array['Tujuan'],
					$post_array['NamaMobil']=$NamaMobil_array[0]['NamaMobil'],
					$post_array['NamaRuang']=$NamaRuang_array[0]['NamaRuang'],
					$post_array['Kegiatan'],
					$post_array['JenisRequest'],
					$post_array2['NPK'],
					$post_array['JamMulai'],
					$post_array['JamSelesai'],
					$post_array['Tanggal']);
			}
			$this->db->trans_commit();
			return true;

		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	function sendEmailToUser($peminjam, $Tujuan, $NamaMobil, $NamaRuang, $Kegiatan, $JenisRequest, $NPK, $JamMulai, $JamSelesai, $Tanggal){
		try
		{
			if($this->config->item('enableEmail') == 'true'){
				$query2= $this->db->get_where('mstruser',array('NPK'=>$NPK));
				$namaRequester = '';
				$emailRequester = '';
				foreach($query2->result() as $row)
				{
					$namaUserTerkait = $row->Nama;
					$emailUserTerkait = $row->EmailInternal;
				}

				$peminjam = $this->user->dataUser($peminjam);
				foreach($peminjam as $row){
					$namaRequester = $row->nama;
				}

				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				$this->email->to($emailUserTerkait);

				if($NamaMobil <> '' || $NamaMobil <> null ){
					$Tuju = '<li>Tujuan: '.$Tujuan.' </li>';
					$subject = $Kegiatan. '(Mobil '.$NamaMobil.')' ;
					$location = $Tujuan;
				} else {
					$subject = $Kegiatan;
					$location = $NamaRuang;
				}

				$message =
				'Dear '.$namaUserTerkait.',<br/>ada Peminjaman '.$JenisRequest.' yang mengikut sertakan anda kedalam jadwal tersebut<br/>
						Detail Peminjaman antara lain:
						<ul>
							<li>Peminjam: '.$namaRequester.' </li>
							<li>Kegiatan: '.$Kegiatan.' </li>'
							.$Tuju.
							'<li>Jenis Request: '.$JenisRequest.' - '.$NamaMobil.''.$NamaRuang.' </li>
							<li>Tanggal: '.$Tanggal.' </li>
							<li>Jam Mulai: '.$JamMulai.' </li>
							<li>Jam Selesai: '.$JamSelesai.' </li>
							<li>Dibuat Oleh: '.$namaRequester.' </li>
						</ul>
						Terima Kasih<br/> ESS
						';
			//	$this->email->subject($subject);
				$this->email->message($message);
				$from_name = 'Enterprise Self Service';
				$from_address='ess@dpa.co.id';
				$to_address = $emailUserTerkait;
				$to_name = $namaUserTerkait;
				$StartDate = (string)date('m/d/Y', strtotime($Tanggal));
				$StartTime = (string)date('h:i:s A', strtotime($JamMulai));
				$EndTime = (string)date('h:i:s A', strtotime($JamSelesai));
				$startTime = ''.$StartDate.' '.$StartTime.'';
				$endTime = ''.$StartDate.' '.$EndTime.'';
				$description = $message;
				if(!$this->sendIcalEvent($from_name, $from_address, $to_name, $to_address, $startTime, $endTime, $subject, $description, $location)){
					log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
					throw new Exception( 'Something really gone wrong', 0, $e);
					return false;
				}
				/*if( ! $this->email->send())
				{
					log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
					throw new Exception( 'Something really gone wrong', 0, $e);
					return false;
				}*/
			}

		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	function sendIcalEvent($from_name, $from_address, $to_name, $to_address, $startTime, $endTime, $subject, $description, $location)
{
	try{
				$domain = 'dpa.co.id';

				//Create Email Headers
				$mime_boundary = "----Meeting Booking----".MD5(TIME());

				$headers = "From: ".$from_name." <".$from_address.">\n";
				$headers .= "Reply-To: ".$from_name." <".$from_address.">\n";
				$headers .= "MIME-Version: 1.0\n";
				$headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
				$headers .= "Content-class: urn:content-classes:calendarmessage\n";

				//Create Email Body (HTML)
				$message = "--$mime_boundary\r\n";
				$message .= "Content-Type: text/html; charset=UTF-8\n";
				$message .= "Content-Transfer-Encoding: 8bit\n\n";
				$message .= "<html>\n";
				$message .= "<body>\n";
				//$message .= '<p>Dear '.$to_name.',</p>';
				$message .= '<p>'.$description.'</p>';
				$message .= "</body>\n";
				$message .= "</html>\n";
				$message .= "--$mime_boundary\r\n";

				$ical = 'BEGIN:VCALENDAR' . "\r\n" .
				'PRODID:-//Microsoft Corporation//Outlook 10.0 MIMEDIR//EN' . "\r\n" .
				'VERSION:2.0' . "\r\n" .
				'METHOD:REQUEST' . "\r\n" .
				'BEGIN:VTIMEZONE' . "\r\n" .
				'TZID:Asia/Jakarta' . "\r\n" .
				'BEGIN:STANDARD' . "\r\n" .
				'DTSTART:20091101T020000' . "\r\n" .
				'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=1SU;BYMONTH=11' . "\r\n" .
				'TZOFFSETFROM:-0400' . "\r\n" .
				'TZOFFSETTO:-0500' . "\r\n" .
				'TZNAME:EST' . "\r\n" .
				'END:STANDARD' . "\r\n" .
				'BEGIN:DAYLIGHT' . "\r\n" .
				'DTSTART:20090301T020000' . "\r\n" .
				'RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=2SU;BYMONTH=3' . "\r\n" .
				'TZOFFSETFROM:-0500' . "\r\n" .
				'TZOFFSETTO:-0400' . "\r\n" .
				'TZNAME:EDST' . "\r\n" .
				'END:DAYLIGHT' . "\r\n" .
				'END:VTIMEZONE' . "\r\n" .
				'BEGIN:VEVENT' . "\r\n" .
				'ORGANIZER;CN="'.$from_name.'":MAILTO:'.$from_address. "\r\n" .
				'ATTENDEE;CN="'.$to_name.'";ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:'.$to_address. "\r\n" .
				'LAST-MODIFIED:' . date("Ymd\TGis") . "\r\n" .
				'UID:'.date("Ymd\TGis", strtotime($startTime)).rand()."@".$domain."\r\n" .
				'DTSTAMP:'.date("Ymd\TGis"). "\r\n" .
				'DTSTART:'.date("Ymd\THis", strtotime($startTime)). "\r\n" .
				'DTEND:'.date("Ymd\THis", strtotime($endTime)). "\r\n" .
				'TRANSP:OPAQUE'. "\r\n" .
				'SEQUENCE:1'. "\r\n" .
				'SUMMARY:' . $subject . "\r\n" .
				'LOCATION:' . $location . "\r\n" .
				'CLASS:PUBLIC'. "\r\n" .
				'PRIORITY:5'. "\r\n" .
				'BEGIN:VALARM' . "\r\n" .
				'TRIGGER:-PT15M' . "\r\n" .
				'ACTION:DISPLAY' . "\r\n" .
				'DESCRIPTION:Reminder' . "\r\n" .
				'END:VALARM' . "\r\n" .
				'END:VEVENT'. "\r\n" .
				'END:VCALENDAR'. "\r\n";
				$message .= 'Content-Type: text/calendar;name="meeting.ics";method=REQUEST'."\n";
				$message .= "Content-Transfer-Encoding: 8bit\n\n";
				$message .= $ical;

				$mailsent = mail($to_address, $subject, $message, $headers);

				return ($mailsent)?(true):(false);

	}
	catch(Exception $e)
	{
		log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
		throw new Exception( 'Something really gone wrong', 0, $e);
	}
}
	function _update($post_array,$primary_key){
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			$userTerkait_array = $post_array['UserTerkait'];
			unset($post_array['UserTerkait']);
			$this->db->trans_start();
			foreach ($userTerkait_array as $value) {
				$post_array2['Deleted'] = 0;
				$post_array2['KodeRequestMobilRuangMeeting'] = $post_array['KodeRequestMobilRuangMeeting'];
				$post_array2['NPK'] = $value;
				$post_array2['CreatedOn'] = date('Y-m-d H:i:s');
				$post_array2['CreatedBy'] = $this->npkLogin;
				$this->db->update('userrequestmobilruangmeeting',$post_array2, array('KodeRequestMobilRuangMeeting' => $primary_key));
			}
			$this->db->update('requestmobilruangmeeting',$post_array,array('KodeRequestMobilRuangMeeting' => $primary_key));
			$KodeRequestMobilRuangMeeting = $primary_key;

			//usertask lama di nyatakan statusnya AP
			$usertasklama = $this->usertask->getLastUserTask($KodeRequestMobilRuangMeeting);
			$KodeUserTaskLama = '';
			if($usertasklama){
				foreach($usertasklama as $row){
					$KodeUserTaskLama = $row->KodeUserTask;

				}
				$this->usertask->updateStatusUserTask($KodeUserTaskLama,'AP',$this->npkLogin);
			}
			fire_print('log',$KodeUserTaskLama);
			//buat user task baru untuk proses approval request mobil dan ruang meeting
			$KodeUserTask = generateNo('UT');


			if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestMobilRuangMeeting,$KodeUserTask,"RM"))
			{
				echo('gagal simpan usertask request mobil dan ruang meeting');
				return false;
			}
			$this->db->trans_complete();
			return $this->db->trans_status();


	}

	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('requestmobilruangmeeting',$post_array,array('KodeRequestMobilRuangMeeting' => $primary_key));
	}

	function getRequest()
	{
		$session_data = $this->session->userdata('logged_in');
		$dataUser = $this->user->dataUser($session_data['npk']);
		if($dataUser){
			foreach($dataUser as $dataUser){
				$dataUserDetail = array(
					'departemen'=> $dataUser->departemen
				);
			}
		}else{
			echo 'gagal get current user departemen';
		}
		return $dataUserDetail['departemen'];
	}

	function add_field_callback_JenisRequest($value = '', $primary_key = null)
	{
		$selected1 = '';
		$selected2 = '';
		if($value == "Mobil"){
			$selected1 = 'selected';
		}else if($value == "Ruang Meeting"){
			$selected2 = 'selected';
		}

		return ' <select id="field-JenisRequest" name="JenisRequest" class = "chosen-select">
			<option value = ""></option>
			<option '.$selected1.' value="Mobil">Mobil</option>
			<option '.$selected2.' value="Ruang Meeting">Ruang Meeting</option>
		</select>';
	}

	/* function add_field_callback_KodeMobil($value = '', $primary_key = null)
	{
		$htmlText = '<select id="field-KodeMobil" name="KodeMobil">';
		$Mobil = $this->requestMobilRuangMeeting_Model->getMobil('');
		$count = 0;
		if($Mobil){
			foreach($Mobil as $row){
				$htmlText .= '<option ';
				if($value == $count + 1)
					$htmlText .= ' selected ';
				$htmlText .= ' value="'.$row->KodeMobil.'">'.$row->NamaMobil.'</option>';
				$count++;
			}
		}
		$htmlText .= '</select>';
		return $htmlText;
	}

	function add_field_callback_KodeRuangMeeting($value = '', $primary_key = null)
	{
		$htmlText = '<select id="field-KodeRuangMeeting" name="KodeRuangMeeting">';
		$ruangmeeting = $this->requestMobilRuangMeeting_Model->getRuangMeeting('');
		$count = 0;
		if($ruangmeeting){
			foreach($ruangmeeting as $row){
				$htmlText .= '<option ';
				if($value == $count + 1)
					$htmlText .= ' selected ';
				$htmlText .= ' value="'.$row->KodeRuangMeeting.'">'.$row->NamaRuang.'</option>';
				$count++;
			}
		}
		$htmlText .= '</select>';
		return $htmlText;
	} */

	function field_callback_JamMulai($value='',$primary_key=null)
	{
		return "<input name='JamMulai' type='time' value='$value'>";
	}

	function field_callback_JamSelesai($value='',$primary_key=null)
	{
		return "<input name='JamSelesai' type='time' value='$value'>";
	}

	function validasiTanggal($value){
		try
		{
			if($value < date("Y-m-d")){
				fire_print('log',"nilai value : $value");
				$this->form_validation->set_message('validasiTanggal',"Tanggal minimal yang bisa Anda masukkan adalah ". date("Y-m-d"));
				return false;
			}
			return true;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	function validasiMobil($str)
	{
		$JenisRequest = $this->input->post('JenisRequest');
		if($JenisRequest == "Mobil" && $str == ""){
			$this->form_validation->set_message('validasiMobil', "Harap pilih mobil yang akan dipakai");
			return false;
		}
	}

	function validasiRuangMeeting($str)
	{
		$JenisRequest = $this->input->post('JenisRequest');
		if($JenisRequest == "Ruang Meeting" && $str == ""){
			$this->form_validation->set_message('validasiRuangMeeting', "Harap pilih ruang meeting yang akan dipakai");
			return false;
		}
	}

	function validasiJamMulai($str)
	{
		try
		{
			$JenisRequest = $this->input->post('JenisRequest');
			$KodeMobil = $this->input->post('KodeMobil');
			$KodeRuangMeeting = $this->input->post('KodeRuangMeeting');
			$Tanggal = $this->input->post('Tanggal');
			$Kegiatan = $this->input->post('Kegiatan');
			$Tujuan = $this->input->post('Tujuan');
			//tambahan tgl 1 agustus 2016
			$JamMulai = $this->input->post('JamMulai');
			$JamSelesai = $this->input->post('JamSelesai');

			//cek apakah mobil inova telah digunakan pada tanggal tersebut

			fire_print('log',"JenisRequest: $JenisRequest.Kode Mobil request : $KodeMobil");
			fire_print('log',"Tanggal request : $Tanggal. JamMulai:$JamMulai. JamSelesai:$JamSelesai");

			if($JenisRequest == "Mobil")
			{
				$Kode = $KodeMobil;
			}else{
				$Kode = $KodeRuangMeeting;
			}

			$dataRequestMobil = $this->requestMobilRuangMeeting_Model->getRequest($Tanggal,$Kode,$JenisRequest);
			if($dataRequestMobil != null)
			{
				foreach($dataRequestMobil as $dtRw)
				{
					fire_print('log',"jamMulai: $JamMulai. jammulaiselesai dtrw : $dtRw->JamMulai $dtRw->JamSelesai");
					if($str >= substr($dtRw->JamMulai,0,5) && $str < substr($dtRw->JamSelesai,0,5)){
						$this->form_validation->set_message('validasiJamMulai', "$JenisRequest sudah dipakai pada jam mulai yang dipilih");
						return false;
					}

					//untuk mengatasi jika diantara jam mulai dan jam selesai yang dipilih terdapat jadwal lain
					if((substr($dtRw->JamMulai,0,5) >= $JamMulai && substr($dtRw->JamMulai,0,5) <= $JamSelesai)||(substr($dtRw->JamSelesai,0,5) >= $JamMulai && substr($dtRw->JamSelesai,0,5) <= $JamSelesai))
					{
						$this->form_validation->set_message('validasiJamMulai', "$JenisRequest sudah ada yang request antara jam $dtRw->JamMulai dan $dtRw->JamSelesai");
						return false;
					}
				}
			}

			return true;
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}

	}

	function validasiJamSelesai($str)
	{
		try
		{
			$JenisRequest = $this->input->post('JenisRequest');
			$KodeMobil = $this->input->post('KodeMobil');
			$KodeRuangMeeting = $this->input->post('KodeRuangMeeting');
			$Tanggal = $this->input->post('Tanggal');
			$Kegiatan = $this->input->post('Kegiatan');
			$Tujuan = $this->input->post('Tujuan');
			$JamMulai = $this->input->post('JamMulai');

			//cek apakah mobil inova telah digunakan pada tanggal tersebut

			fire_print('log',"JenisRequest: $JenisRequest.Kode Mobil request : $KodeMobil");
			fire_print('log',"Tanggal request : $Tanggal. JamSelesai:$str");

			if($JenisRequest == "Mobil")
			{
				$Kode = $KodeMobil;
			}else{
				$Kode = $KodeRuangMeeting;
			}

			$dataRequestMobil = $this->requestMobilRuangMeeting_Model->getRequest($Tanggal,$Kode,$JenisRequest);
			if($dataRequestMobil != null){
				foreach($dataRequestMobil as $dtRw)
				{
					fire_print('log',"jamSelesai: $str. jammulaiselesai dtrw : $dtRw->JamMulai $dtRw->JamSelesai");
					if($str > substr($dtRw->JamMulai,0,5) && $str <= substr($dtRw->JamSelesai,0,5)){
						$this->form_validation->set_message('validasiJamSelesai', "$JenisRequest sudah dipakai pada jam selesai yang dipilih");
						return false;
					}
				}
			}

			if($str <= substr($JamMulai,0,5))
			{
				$this->form_validation->set_message('validasiJamSelesai', "Jam Selesai harus lebih besar daripada Jam Mulai");
				return false;
			}

			return true;
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}

	}
	function getCurrentUserKodeRole()
	{
		$session_data = $this->session->userdata('logged_in');
		$dataUser = $this->user->dataUser($session_data['npk']);
		if($dataUser){
			foreach($dataUser as $dataUser){
				$dataUserDetail = array(
					'KodeRole'=> $dataUser->KodeRole
				);
			}
		}else{
			echo 'gagal get current user kode role';
		}
		return $dataUserDetail['KodeRole'];
	}

		function _callback_statusRequest($value, $row){
		switch($value)
		{
			case "PE": $text = "Belum Realisasi"; break;
			case "DE": $text = "Realisasi Dibatalkan"; break;
			case "AP": $text = "Sudah Realisasi"; break;
		}
		return $text;
	}


}
?>