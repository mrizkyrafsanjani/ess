<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Issue extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		//$this->load->model('issue','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Issue"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_issue();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _issue()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Issue');
		//$crud->set_theme('datatables');
		
        $crud->set_table('issue');
		$crud->where('issue.deleted','0');
		
		//$crud->set_relation_n_n('Menu','rolemenu','menu','KodeRole','KodeMenu','MenuName');
		
		$crud->columns('KodeIssue','Issue','Pelapor','Penyebab','Akibat','Tindakan','TanggalLapor','TanggalTindakan','TanggalSelesai','Keterangan');
		$crud->fields('KodeIssue','Issue','Pelapor','Penyebab','Akibat','Tindakan','TanggalLapor','TanggalTindakan','TanggalSelesai','Keterangan');
		
		//$crud->display_as('Menu','Menu Yang Dapat Diakses');
		
		//$crud->required_fields('Deskripsi','Menu');
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		$crud->callback_insert(array($this,'_insert'));
		//$crud->callback_after_insert(array($this,'_after_insert_issue'));
		$crud->callback_after_update(array($this,'_after_update_issue'));
		$crud->callback_delete(array($this,'_delete_issue'));		
		$crud->unset_texteditor('Penyebab','Akibat','Tindakan','Keterangan');		
		$crud->field_type('KodeIssue', 'readonly');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Issue',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert($post_array){
		try{
			$post_array['KodeIssue'] = generateNo('IS');
			$post_array['CreatedOn'] = date('Y-m-d H:i:s');
			$post_array['CreatedBy'] = $this->npkLogin;
			
			$this->db->trans_begin();
			$this->db->insert('issue',$post_array);
			$this->db->trans_commit();
			return true;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function _after_insert_issue($post_array,$primary_key)
	{
		$createDate = array(
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s')
		);
		fire_print('log',print_r($post_array,true));
		$this->db->update('issue',$createDate,array('KodeIssue'=>$primary_key));
		return true;
	}
	
	function _after_update_issue($post_array,$primary_key)
	{
		$updateDate = array(
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s')
		);
		
		fire_print('log',print_r($post_array,true));
		fire_print('log','pk issue : '.$primary_key);
		
		$this->db->update('issue',$updateDate,array('KodeIssue'=>$primary_key));		
		return true;
	}
	
	function _delete_issue($primary_key){
		return $this->db->update('issue',array('deleted' => '1'),array('KodeIssue' => $primary_key));
	}
		
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */