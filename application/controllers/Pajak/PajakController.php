<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
session_start();
class PajakController extends CI_Controller{
    var $npkLogin;
    function __construct()
    {
        parent::__construct();    
		$this->load->model('menu','',TRUE);
        $this->load->model('user','',TRUE);		
        $this->load->model('pajak_model','',TRUE);	        
        $this->load->helper('date');
        $session_data = $this->session->userdata('logged_in');
        $this->npkLogin = $session_data['npk'];
    }

    public function index()
    {
        try
        {
            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }

    

    public function ViewReportPajak()
    {
        $session_data = $this->session->userdata('logged_in');
        if($this->session->userdata('logged_in')){
            if(check_authorizedByName("Pajak"))
            {
                $this->npkLogin = $session_data['npk'];
                $data = array(
                    'title' => 'Pajak'
                );
                $this->load->helper(array('form','url'));
                $this->template->load('default','Pajak/Pajak_view',$data);            
            }
        }else{
            //redirect('login','refresh');
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }

    function CetakPajak($masa,$tahun,$dpa)
    {
        try
        {
            if(!$this->session->userdata('logged_in'))
            {
                redirect("login?u=Pajak/PajakController/CetakPajak/$masa/$tahun/$dpa",'refresh');				
            }
            else
            {
                    
                    $dataPajakPPH4 = $this->pajak_model->getDataPajakPPH42($masa,$tahun,$dpa );						
                    $dataPajakPPH4data =array();					
                    if($dataPajakPPH4)
                    {
                        foreach($dataPajakPPH4 as $row)
                        {	
                            $dataPajakPPH4data[] = array(
                                'kodepajak' => $row->kodepajak,
                                'KodeSetor' => $row->KodeSetor,
                                'JenisPenghasilan' => $row->JenisPenghasilan,
                                'Nama' => $row->Nama,
                                'bruto' => $row->bruto,
                                'pph' => $row->pph,
                                'brutonf' => $row->brutonf,
                                'pphnf' => $row->pphnf
                            );		
                        }
                    }

                    $dataPajakPPH4jasa = $this->pajak_model->getDataPajakPPH42jasa($masa,$tahun,$dpa );						
                    $dataPajakPPH4Jasadata =array();					
                    if($dataPajakPPH4jasa)
                    {
                        foreach($dataPajakPPH4jasa as $row)
                        {	
                            $dataPajakPPH4Jasadata[] = array(
                                'kodepajak' => $row->kodepajak,
                                'KodeSetor' => $row->KodeSetor,
                                'JenisPenghasilan' => $row->JenisPenghasilan,
                                'Nama' => $row->Nama,
                                'bruto' => $row->bruto,
                                'pph' => $row->pph,
                                'brutonf' => $row->brutonf,
                                'pphnf' => $row->pphnf
                            );		
                        }
                    }

                                       
                    $dataPajakPPH23 = $this->pajak_model->getDataPajakPPH23($masa,$tahun,$dpa );						
                    $dataPajakPPH23data =array(); 
                    if($dataPajakPPH23)
                    {
                        foreach($dataPajakPPH23 as $row)
                        {	
                            $dataPajakPPH23data[] = array(
                                'kodepajak' => $row->kodepajak,
                                'KodeSetor' => $row->KodeSetor,
                                'JenisPenghasilan' => $row->JenisPenghasilan,
                                'Nama' => $row->Nama,
                                'bruto' => $row->bruto,
                                'pph' => $row->pph,
                                'brutonf' => $row->brutonf,
                                'pphnf' => $row->pphnf
                            );		
                        }
                    }

                    $dataPajakPPH231 = $this->pajak_model->getDataPajakPPH231($masa,$tahun,$dpa );						
                    $dataPajakPPH23data1 =array(); 
                    if($dataPajakPPH231)
                    {
                        foreach($dataPajakPPH231 as $row)
                        {	
                            $dataPajakPPH23data1[] = array(
                                'kodepajak' => $row->kodepajak,
                                'KodeSetor' => $row->KodeSetor,
                                'JenisPenghasilan' => $row->JenisPenghasilan,
                                'Nama' => $row->Nama,
                                'bruto' => $row->bruto,
                                'pph' => $row->pph,
                                'brutonf' => $row->brutonf,
                                'pphnf' => $row->pphnf
                            );		
                        }
                    }

                    $dataPajakPPH26 = $this->pajak_model->getDataPajakPPH26($masa,$tahun,$dpa );						
                    $dataPajakPPH26data =array(); 
                    if($dataPajakPPH26)
                    {
                        foreach($dataPajakPPH26 as $row)
                        {	
                            $dataPajakPPH26data[] = array(
                                'kodepajak' => $row->kodepajak,
                                'KodeSetor' => $row->KodeSetor,
                                'JenisPenghasilan' => $row->JenisPenghasilan,
                                'Nama' => $row->Nama,
                                'bruto' => $row->bruto,
                                'pph' => $row->pph,
                                'brutonf' => $row->brutonf,
                                'pphnf' => $row->pphnf
                            );		
                        }
                    }

                    
                    $dataPajakPPH21 = $this->pajak_model->getDataPajakPPH21($masa,$tahun,$dpa );						
                    $dataPajakPPH21data=array();
                    if($dataPajakPPH21)
                    {
                        foreach($dataPajakPPH21 as $row)
                        {	
                            $dataPajakPPH21data[] = array(
                                'kodepajak' => $row->kodepajak,
                                'KodeSetor' => $row->KodeSetor,
                                'JenisPenghasilan' => $row->JenisPenghasilan,
                                'Nama' => $row->Nama,
                                'bruto' => $row->bruto,
                                'pph' => $row->pph,
                                'brutonf' => $row->brutonf,
                                'pphnf' => $row->pphnf
                            );		
                        }
                    }

                    $dataPajakPPH21GAJI = $this->pajak_model->getDataPajakPPH21GAJI($masa,$tahun,$dpa );						
                    $dataPajakPPH21GAJIdata=array();
                    if($dataPajakPPH21GAJI)
                    {
                        foreach($dataPajakPPH21GAJI as $row)
                        {	
                            $dataPajakPPH21GAJIdata[] = array(
                                'kodepajak' => $row->kodepajak,
                                'KodeSetor' => $row->KodeSetor,
                                'JenisPenghasilan' => $row->JenisPenghasilan,
                                'Nama' => $row->Nama,
                                'bruto' => $row->bruto,
                                'pph' => $row->pph,
                                'brutonf' => $row->brutonf,
                                'pphnf' => $row->pphnf
                            );		
                        }
                    }

                    $dataPajakPPH21PESANGON = $this->pajak_model->getDataPajakPPH21PESANGON($masa,$tahun,$dpa );						
                    $dataPajakPPH21PESANGONdata=array();
                    if($dataPajakPPH21PESANGON)
                    {
                        foreach($dataPajakPPH21PESANGON as $row)
                        {	
                            $dataPajakPPH21PESANGONdata[] = array(
                                'kodepajak' => $row->kodepajak,
                                'KodeSetor' => $row->KodeSetor,
                                'JenisPenghasilan' => $row->JenisPenghasilan,
                                'Nama' => $row->Nama,
                                'bruto' => $row->bruto,
                                'pph' => $row->pph,
                                'brutonf' => $row->brutonf,
                                'pphnf' => $row->pphnf
                            );		
                        }
                    }
                    
                    
                   
                        $data = array(
                            'title' =>'Cetak Pajak',
                            'dataPajakPPH4data'=> $dataPajakPPH4data,
                            'dataPajakPPH4Jasadata'=> $dataPajakPPH4Jasadata,
                            'dataPajakPPH23data'=> $dataPajakPPH23data,
                            'dataPajakPPH21data'=> $dataPajakPPH21data,
                            'masa'=> $masa,
                            'tahun'=> $tahun,
                            'dataPajakPPH23data1'=> $dataPajakPPH23data1,
                            'dataPajakPPH26data'=> $dataPajakPPH26data,
                            'dataPajakPPH21PESANGONdata'=> $dataPajakPPH21PESANGONdata,
                            'dataPajakPPH21GAJIdata'=> $dataPajakPPH21GAJIdata
                        );
                                     
                    
                    $this->load->helper(array('form','url'));						
                    $this->load->view('Pajak/cetakFormPajak_view',$data);
                    
                    
            }
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            throw new Exception( 'Something really gone wrong', 0, $e);
        }
        
    }
    
    
    
    
}
?>