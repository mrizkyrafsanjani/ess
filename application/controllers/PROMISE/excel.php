<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Excel extends CI_Controller {
    function __construct(){
        parent::__construct();
        require_once APPPATH . "third_party/PHPExcel/PHPExcel.php";
        require_once APPPATH . "third_party/PHPExcel/PHPExcel/IOFactory.php";
        $this->excel = new PHPExcel();    
        $this->load->model('promise_model','',TRUE);
        $this->load->model('user','',TRUE);
        $this->load->library('email');
        $this->load->helper('file');
    }
    public function index()
    {
        $this->template->load('default','/PROMISE/excel_view');
    }
    public function upload(){
        $session_data = $this->session->userdata('logged_in');
	    $NPKlogin = $session_data['npk'];
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = './assets/uploads/excel/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = './assets/uploads/excel/'.$media['file_name'];
         
        try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            $IDProject=$this->promise_model->getLastID();
            if($IDProject){
                $IDProject_array = array();
                foreach($IDProject as $row){
                    $IDProject_array[] = array(
                        'ProjectID' => $row->ProjectID   
                    );
                }
            }else{
                echo 'gagal';
            }
            $ProjectID=$IDProject_array[0]['ProjectID'] + 1;

            for ($row = 7; $row <= $highestRow; $row++){  //  Read a row of data into an array  
                                              
                $rowData = $sheet->rangeToArray('D' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    "ProjectID"=> $ProjectID,
                    "NamaProject"=> $rowData[0][0],
                    "KPI"=> $rowData[0][1],
                    "StartDate"=>$rowData[0][2],
                    "Target"=> $rowData[0][3],
                    "DeadlineUREQ"=> $rowData[0][4],
                    "Deadline"=> $rowData[0][5],
                    "PIC"=> $rowData[0][6],
                    "Budget"=> $rowData[0][7],
                    "Departemen"=> $rowData[0][8],
                    "Keterangan"=> $rowData[0][9],
                    "ReminderDate"=> $rowData[0][2],
                    "CreatedOn" => date('Y-m-d H:i:s'),
                    "CreatedBy" => $NPKlogin,
                    "UpdatedOn" => date('Y-m-d H:i:s'),
                    "UpdatedBy" => $NPKlogin
                );
               
                 if($rowData[0][0]==null){
                    unlink('./assets/uploads/excel/'.$media['file_name']);
                    redirect('PROMISE/PROMISEController/ViewListPROMISEAdmin');
                 }else{
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("pmtrekap",$data); 
                
                //send email
                $this->load->library('email');
                $this->email->from('ess@dpa.co.id', 'PROMISE');
                $nama = $this->promise_model->getDataUserByNama($rowData[0][6]);
                $namaAt = $this->promise_model->getAtasan($rowData[0][6]);
                $namaAta = $namaAt[0]->NamaAtasan;
                $namaAtasan = $this->promise_model->getDataUserByNama($namaAta);
                $CcArr=$namaAtasan[0]->email;
                $recipientArr = array($nama[0]->email);
                $this->email->to($recipientArr);
                $this->email->cc($CcArr);
                $this->email->subject('NEW PROMISE');
                
                $DeadlineUREQ = '';
                $Budget ='';
                $Keterangan ='';

                if($rowData[0][4]<>''){
                    $DeadlineUREQ = '<li>Deadline UREQ: <b>'.$rowData[0][4].'</b></li>';
                }
                if($rowData[0][7]<>''){
                    $Budget = '<li>Budget: <b>Rp ' .number_format($rowData[0][7],2,',','.').'</b></li>';
                }
                if($rowData[0][9]<>''){
                    $Keterangan = ' <li>Keterangan: <b>'.$rowData[0][9].'</b></li>';
                }
                $message = 'Dear ' .$rowData[0][6].',
                    <br/><br/>Terdapat project baru dengan detail sebagai berikut:
                    <ul>
                    <li>Nama Project: <b>'.$rowData[0][0].'</b></li>
                    <li>Target: <b>'.$rowData[0][3].'</b></li>
                    <li>Start Date: <b>'.$rowData[0][2].'</b></li>'
                    .$DeadlineUREQ.'
                    <li>Deadline: <b>'.$rowData[0][5].'</b></li>
                    <li>PIC: <b>'.$rowData[0][6].'</b></li>'
                    .$Budget.'
                    <li>Departemen: <b>'.$rowData[0][8].'</b></li>
                    '.$Keterangan.'
                    <li><a href="'.base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID.'">Lihat Detail Project</a></li>
                    </ul>
                    <br/>Terima kasih
                    <br/>PROMISE
                ';
                $this->email->message($message);
                //$this->email->send();
                 }
                $ProjectID=$ProjectID+1;
            }
            unlink('./assets/uploads/excel/'.$media['file_name']);
        redirect('PROMISE/PROMISEController/ViewListPROMISEAdmin');
    }
    public function insert(){
       error_reporting(0);
        $inputFileName = './assets/uploads/excel/InsertProject.xlsm';
         
        try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                redirect('PROMISE/PROMISEController/ViewListPROMISEAdmin');
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            $IDProject=$this->promise_model->getLastID();
            if($IDProject){
                $IDProject_array = array();
                foreach($IDProject as $row){
                    $IDProject_array[] = array(
                        'ProjectID' => $row->ProjectID   
                    );
                }
            }else{
                echo 'gagal';
            }
            $ProjectID=$IDProject_array[0]['ProjectID'] + 1;

            for ($row = 7; $row <= $highestRow; $row++){  //  Read a row of data into an array  
                                              
                $rowData = $sheet->rangeToArray('D' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    "ProjectID"=> $ProjectID,
                    "NamaProject"=> $rowData[0][0],
                    "KPI"=> $rowData[0][1],
                    "Target"=> $rowData[0][2],
                    "DeadlineUREQ"=> $rowData[0][3],
                    "Deadline"=> $rowData[0][4],
                    "PIC"=> $rowData[0][5],
                    "Budget"=> $rowData[0][6],
                    "Departemen"=> $rowData[0][7],
                    "Keterangan"=> $rowData[0][8],
                );
                 if($rowData[0][0]==null){
                    unlink('./assets/uploads/excel/InsertProject.xlsm');
                    redirect('PROMISE/PROMISEController/ViewListPROMISEAdmin');
                 }else{
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("pmtrekap",$data);
                 }
                 
                $ProjectID=$ProjectID+1;
            }
            unlink('./assets/uploads/excel/InsertProject.xlsm');
        redirect('PROMISE/PROMISEController/ViewListPROMISEAdmin');
    }
}
