<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
session_start();
class PROMISEController extends CI_Controller{
    var $npkLogin;
    function __construct()
    {
        parent::__construct();    
		$this->load->model('menu','',TRUE);
        $this->load->model('user','',TRUE);		
        $this->load->model('promise_model','',TRUE);
        $this->load->model('usertask','',TRUE);
        $this->load->library('email');	

        $this->load->helper('date');
        $session_data = $this->session->userdata('logged_in');
        $this->npkLogin = $session_data['npk'];
    }

    public function index()
    {
        try
        {
            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }

    public function ViewListPROMISEHead()
    {
        error_reporting(0);
        $session_data = $this->session->userdata('logged_in');
        if($this->session->userdata('logged_in')){
            if(check_authorizedByName("PROMISE Admin"))
            {
                $npkLogin = $session_data['npk'];
                $getDepartemen = $this->promise_model->getDept();
                if($getDepartemen){
                    $getDepartemen_array = array();
                    foreach($getDepartemen as $row){
                        $getDepartemen_array[] = array(
                            'KodeDepartemen' => $row->KodeDepartemen,
                            'NamaDepartemen' => $row->NamaDepartemen,
                        );
                    }
                }else{
                    //echo 'gagal';
                }

                $NamaSingleDept = $this->promise_model->getNamaJabatan($npkLogin);
                if($NamaSingleDept){
                        $NamaSingleDept_array = array();
                    foreach($NamaSingleDept as $row){
                        $NamaSingleDept_array[] = array(
                            'departemen' => $row->departemen,
                            'jabatan' => $row->jabatan,
                        );
                    }
                }
               // echo $NamaJabatan_array[0]['departemen'];
                $getRekapAdmin = $this->promise_model->getRekapAdmin();
                if($getRekapAdmin){
                    $getRekapAdmin_array = array();
                    foreach($getRekapAdmin as $row){
                        $getRekapAdmin_array[] = array(
                            'ProjectID' => $row->ProjectID,
							'NamaProject' => $row->NamaProject,
                            'DetilInfo' => $row->DetilInfo,
							'Deadline' => $row->Deadline,
							'PIC' => $row->PIC,
                            'Departemen' => $row->Departemen,
							'Status' => $row->Status,
                            'KetAdmin' => $row->KetAdmin,
                            'CreatedOn' => $row->CreatedOn,
                            'EndDate' => $row->EndDate
                        );
                    }
                }else{
                   // echo 'gagal';
                }
                

                $ProjectID=$getRekapAdmin_array[0]['ProjectID'];
				$getProgress = $this->promise_model->getProgress();
                if($getProgress){
                    $getProgress_array = array();
                    foreach($getProgress as $row){
                        $getProgress_array[] = array(
                            'ProjectID' => $row->ProjectID,
							'Presentase' => $row->Presentase  
                        );
                    }
                }else{
                  //  echo 'gagal';
                }

                $this->npkLogin = $session_data['npk'];

                $data = array(
                    'title' => 'PROMISE Head',
                    'npk' => $session_data['npk'],
					'nama' => $session_data['nama'],
                    'getDepartemen_array' => $getDepartemen_array,
                    'getRekapAdmin_array' => $getRekapAdmin_array,
                    'getProgress_array' => $getProgress_array,
                    'JenisUser' => 'Head',
                    'NamaSingleDept_array' => $NamaSingleDept_array

                );
                
	
                $this->load->helper(array('form','url'));
                $this->template->load('default','PROMISE/PROMISEHead_view',$data);            
            }
        }else{
            //redirect('login','refresh');
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }

    public function ViewListPROMISEUser()
    {
        error_reporting(0);
        $session_data = $this->session->userdata('logged_in');
        if($this->session->userdata('logged_in')){
                $npkLogin = $session_data['npk'];
                $getDepartemen = $this->promise_model->getDept();
                if($getDepartemen){
                    $getDepartemen_array = array();
                    foreach($getDepartemen as $row){
                        $getDepartemen_array[] = array(
                            'KodeDepartemen' => $row->KodeDepartemen,
                            'NamaDepartemen' => $row->NamaDepartemen,
                        );
                    }
                }else{
                    //echo 'gagal';
                }
                $NamaUser = $this->promise_model->getNamaJabatan($npkLogin);
                if($NamaUser){
                        $NamaUser_array = array();
                    foreach($NamaUser as $row){
                        $NamaUser_array[] = array(
                            'nama' => $row->nama,
                            'departemen' => $row->departemen,
                            'jabatan' => $row->jabatan,
                        );
                    }
                }
                $getRekapAdmin = $this->promise_model->getRekapAdmin();
                if($getRekapAdmin){
                    $getRekapAdmin_array = array();
                    foreach($getRekapAdmin as $row){
                        $getRekapAdmin_array[] = array(
                            'ProjectID' => $row->ProjectID,
							'NamaProject' => $row->NamaProject,
                            'DetilInfo' => $row->DetilInfo,
							'Deadline' => $row->Deadline,
							'PIC' => $row->PIC,
                            'Departemen' => $row->Departemen,
							'Status' => $row->Status,
                            'KetAdmin' => $row->KetAdmin,
                            'CreatedOn' => $row->CreatedOn,
                            'EndDate' => $row->EndDate
                        );
                    }
                }else{
                   // echo 'gagal';
                }
                

                $ProjectID=$getRekapAdmin_array[0]['ProjectID'];
				$getProgress = $this->promise_model->getProgress();
                if($getProgress){
                    $getProgress_array = array();
                    foreach($getProgress as $row){
                        $getProgress_array[] = array(
                            'ProjectID' => $row->ProjectID,
							'Presentase' => $row->Presentase  
                        );
                    }
                }else{
                  //  echo 'gagal';
                }

                $this->npkLogin = $session_data['npk'];
                $data = array(
                    'title' => 'PROMISE User',
                    'getDepartemen_array' => $getDepartemen_array,
                    'getRekapAdmin_array' => $getRekapAdmin_array,
                    'getProgress_array' => $getProgress_array,
                    'JenisUser' => 'User',
                    'NamaUser_array' => $NamaUser_array

                );
                $this->load->helper(array('form','url'));
                $this->template->load('default','PROMISE/PROMISEUser_view',$data);            
            
        }else{
            //redirect('login','refresh');
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }
    
    public function ViewListPROMISEAdmin()
    {
        error_reporting(0);
        $session_data = $this->session->userdata('logged_in');
        if($this->session->userdata('logged_in')){
            if(check_authorizedByName("PROMISE Admin"))
            {
                
                $getDepartemen = $this->promise_model->getDept();
                if($getDepartemen){
                    $getDepartemen_array = array();
                    foreach($getDepartemen as $row){
                        $getDepartemen_array[] = array(
                            'KodeDepartemen' => $row->KodeDepartemen,
                            'NamaDepartemen' => $row->NamaDepartemen,
                        );
                    }
                }else{
                    //echo 'gagal';
                }
                $getRekapAdmin = $this->promise_model->getRekapAdmin();
                if($getRekapAdmin){
                    $getRekapAdmin_array = array();
                    foreach($getRekapAdmin as $row){
                        $getRekapAdmin_array[] = array(
                            'ProjectID' => $row->ProjectID,
							'NamaProject' => $row->NamaProject,
                            'DetilInfo' => $row->DetilInfo,
							'Deadline' => $row->Deadline,
							'PIC' => $row->PIC,
                            'Departemen' => $row->Departemen,
							'Status' => $row->Status,
                            'KetAdmin' => $row->KetAdmin,
                            'CreatedOn' => $row->CreatedOn,
                            'ProjectType' => $row->ProjectType,
                            'EndDate' => $row->EndDate
                        );
                    }
                }else{
                   // echo 'gagal';
                }
                

                $ProjectID=$getRekapAdmin_array[0]['ProjectID'];
				$getProgress = $this->promise_model->getProgress();
                if($getProgress){
                    $getProgress_array = array();
                    foreach($getProgress as $row){
                        $getProgress_array[] = array(
                            'ProjectID' => $row->ProjectID,
							'Presentase' => $row->Presentase  
                        );
                    }
                }else{
                  //  echo 'gagal';
                }

                $this->npkLogin = $session_data['npk'];
                $data = array(
                    'title' => 'PROMISE Admin',
                    'getDepartemen_array' => $getDepartemen_array,
                    'getRekapAdmin_array' => $getRekapAdmin_array,
                    'getProgress_array' => $getProgress_array,
                    'JenisUser' => 'Administrator'

                );
                $this->load->helper(array('form','url'));
                $this->template->load('default','PROMISE/PROMISEAdmin_view',$data);            
            }
        }else{
            //redirect('login','refresh');
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }
    function Upload($JenisUser = ' ',$ID = ''){
        try
        {
            if($this->session->userdata('logged_in'))
            {
                $session_data = $this->session->userdata('logged_in');
                $this->npkLogin = $session_data['npk'];
                
                $user = $this->user->dataUser($this->npkLogin);
                
                    $sqlSelect =
                        "SELECT * from pmtprmtr where ParameterID = '$ID'";
                    $data = $this->db->query($sqlSelect);
                    if($data->num_rows() != 0){
                        $dataPROMISE = array(
                            'title'=> 'Upload Dokumen Progress PROMISE',
                            'data' => $data->result(),
                            'JenisUser' => $JenisUser
                        );
                        $this->load->helper(array('form','url'));
                        $this->template->load('default','PROMISE/UploadProgress_view', $dataPROMISE);
                   
                }
            }
            else
            {
                redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
            }
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            throw new Exception( 'Something really gone wrong', 0, $e);
        }
    }

    function UploadPROMISE($JenisUser, $ID = '',$Status=''){
        $config['upload_path']          = './assets/uploads/files/PROMISE/';
        $config['allowed_types']        = 'jpg|png|pdf';
        $config['max_size']             = 1000000000;
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('uploadScanPROMISE'))
        {
            $error = array('error' => $this->upload->display_errors());
            $session_data = $this->session->userdata('logged_in');
            $this->npkLogin = $session_data['npk'];
            $sqlSelect = "SELECT * from pmtprmtr where ParameterID = '$ID'";
            $data = $this->db->query($sqlSelect);
            $user = $this->user->dataUser($this->npkLogin);
            $filename = $_FILES['uploadScanPROMISE']['name'];
            $data = array('upload_data' => $this->upload->data());
            $filename = $data['upload_data']['file_name'];
            

                $sqlUpdate = "UPDATE pmtprmtr SET FilePathParameter = '$filename', Progress = '$ID' where ParameterID = $ID";
                $this->db->query($sqlUpdate);
             
              if($JenisUser<>'Admin'){
                $lastNo = $this->promise_model->getLastKodePA();
									$NoUrut = substr($lastNo,2,10);
									$NoUrut = (int)$NoUrut;
									$NoUrut = $NoUrut + 1;
									$KodeRequest = "PA$NoUrut";
									$KodeRequestID = $NoUrut;
									if($lastNo=='' || $lastNo==null || $lastNo=='0'){
										$Notransaksi = generateNo('PA');
										$NoUruta = substr($Notransaksi,2,10);
										$NoUruta = (int)$NoUruta;
										$NoUruta = $NoUruta + 1;
										$KodeRequest = "PA$NoUruta";
										$KodeRequestID = $NoUruta;
									}
                                    $post_array2['KodeRequestDetail'] = $KodeRequest;
                                    $post_array2['CreatedBy'] = $this->npkLogin;
									$this->promise_model->ApprovalProgressParameter($ID);
									$array = array(
										'Progress' => $ID,
                                        'StatusRequest' => '0');
                                        $this->db->order_by("ID", "DESC");
                                    $this->db->limit(1); 
									$this->db->update('pmtprmtrequest',$post_array2,$array);
                                        
									$KodeUserTask = generateNo('UT');
                                    $this->usertask->tambahUserTask($this->npkLogin,$post_array2['KodeRequestDetail'],$KodeUserTask,"PA");
                                    if($JenisUser=='Head'){
										redirect('PROMISE/PROMISEController/ViewListPROMISEHead','refresh');				
									}else if($JenisUser=='User'){
										redirect('PROMISE/PROMISEController/ViewListPROMISEUser','refresh');				
									}
              }else{
                redirect('PROMISE/PROMISEController/ViewListPROMISEAdmin/');
              }

            
            
           
        }
    }
    function ApprovalNewPROMISE($KodeUserTask)
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
                        $KodeRequestDetail = $this->promise_model->getKodeRequestDetail($KodeUserTask);
                        if($KodeRequestDetail){
                            $KodeRequestDetail_array = array();
                            foreach($KodeRequestDetail as $row){
                                $KodeRequestDetail_array[] = array(
                                    'NoTransaksi' => $row->NoTransaksi, 
                                    'KodeUserTask' => $row->KodeUserTask,
                                );
                            }
                        }
                     
						$data = $this->promise_model->getApprovalRequest($KodeRequestDetail_array[0]['NoTransaksi']);
                        if($data){
                        $data_array = array();
						foreach($data as $row){
			
							$data_array = array(
                                'NamaProject' => $row->NamaProject,
                                'KPI' => $row->KPI,
                                'Target' => $row->Target,
                                'StartDate' => $row->StartDate,
                                'DeadlineUREQ' => $row->DeadlineUREQ,
                                'Deadline' => $row->Deadline,
                                'PIC' => $row->PIC,
                                'Budget' => $row->Budget,
                                'Departemen' => $row->Departemen,
                                'Keterangan' => $row->Keterangan,
                                'CreatedOn' => $row->CreatedOn,
                                'KodeRequestDetail' => $row->KodeRequestDetail
							);
                           
                        }
                    }
                        $data2 = array (
                            'title' => 'Approval New Promise',
                            'data_array'=>$data_array,
                            'KodeRequestDetail_array'=>$KodeRequestDetail_array
                        );
						$this->load->helper(array('form','url'));
						$this->template->load('default','PROMISE/ApprovalNewPromise_view',$data2);
					//}
				}
				else
				{
					redirect("login?u=PROMISE/PROMISEController/ApprovalNewPROMISE/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
	}
	
	function ajax_prosesApprovalNewPromise()
	{	
			$error = 0;
            $success = "Data Berhasil Disimpan";
            $KodeRequestDetail = $this->input->post('koderequestdetail');
			$KodeUserTask = $this->input->post('kodeusertask');
			$status = $this->input->post('status');
			$catatan = $this->input->post('catatan');
			$query = $this->promise_model->getApprovalRequest($KodeRequestDetail);
			if($query){
				foreach($query as $row){
					$KodeRequest = $row->KodeRequestDetail;
					$Requester = $row->CreatedBy;
                    $StatusRequest = $row->StatusRequest;
                    $NamaProject = $row->NamaProject;
                    
                }
                $IDProject=$this->promise_model->getLastID();
                if($IDProject){
                    $IDProject_array = array();
                    foreach($IDProject as $row){
                        $IDProject_array[] = array(
                            'ProjectID' => $row->ProjectID   
                        );
                        }
                    }
                $ProjectID = $IDProject_array[0]['ProjectID'];
                $link = "<a href='".base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID."'>Detail</a>";
                $user = $this->user->dataUser($Requester);
                if($user){
                    foreach($user as $row){
                        $to= $row->email;
                        $nama= $row->nama;
                    }
                }
				if($StatusRequest == '')
				{
					$this->db->trans_begin();
					
					//update status Undangan
					if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'PR') && $status == 'AP')
					{								
                    $this->promise_model->updateStatusNewPromiseRequest($KodeRequestDetail,$status,$this->npkLogin);
                    $this->addpromise($KodeRequestDetail);
                   
                    
                    $subject = "Approval New Promise";
                    $message = "Dear ". $nama .",
                    <br/><br/>Project ". $NamaProject ." telah disetujui dan telah terdaftar pada PROMISE. 
                    <br/><br/>Click ". $link ." untuk melihat detail.
                    <br/><br/>Terima Kasih.
                    <br/>PROMISE.";
                    $this->emailPromise($to,$subject,$message);
					}
					
					if($status == 'DE')
					{
						
                    $this->promise_model->updateStatusNewPromiseRequest($KodeRequestDetail,$status,$this->npkLogin);	
                    $subject = "Approval New Promise";
                    $message = "Dear ". $nama .",
                    <br/><br/>Project ". $NamaProject ." belum disetujui.
                    <br/>Catatan:  ". $catatan ."
                    <br/><br/>Click ". $link ." untuk melihat detail.
                    <br/><br/>Terima Kasih.
                    <br/>PROMISE.";
                    $this->emailPromise($to,$subject,$message);
					}
					
					//update user task sekarang
					if(!$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan))
						$error += 1;
                    if(!$this->usertask->isAlreadyMaxSequence($this->npkLogin,'PR')&& $status=='AP')
                    {	
					//create user task baru untuk next proses
					$KodeUserTaskNext = generateNo('UT');			
					if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestDetail,$KodeUserTaskNext,"PR".$status,$Requester))
					{
						$success = "GAGAL USERTASK";
						$error += 1;
                    } 
                    }
					if ($error > 0)
					{
						fire_print('log','trans rollback approve Undangan ');
						$this->db->trans_rollback();
					}
					else
					{
						fire_print('log','trans commit approve Undangan ');
						$this->db->trans_commit();
					}
					fire_print('log','Proses Trans Approve Undangan selesai ');
				}
				else
				{
					$success = "GAGAL UPDATE STATUS";
                }
			}else{
				$success = "Tidak ada data";
			}
            echo $success;
    }
    function addpromise($KodeRequestDetail){
        $IDProject=$this->promise_model->getLastID();
        if($IDProject){
            $IDProject_array = array();
            foreach($IDProject as $row){
                $IDProject_array[] = array(
                    'ProjectID' => $row->ProjectID   
                );
                }
            }else{
            echo 'gagal';
            }

        $post_array['ProjectID']=$IDProject_array[0]['ProjectID'] + 1;
        $query = $this->promise_model->getApprovalRequest($KodeRequestDetail);
			if($query){
				foreach($query as $row){
                    $post_array['NamaProject'] = $row->NamaProject;
                    $post_array['KPI'] = $row->KPI;
                    $post_array['Target'] = $row->Target;
                    $post_array['DetilInfo'] = $row->DetilInfo;
                    $post_array['StartDate'] = $row->StartDate;
                    $post_array['DeadlineUREQ'] = $row->DeadlineUREQ;
                    $post_array['Deadline'] = $row->Deadline;
                    $post_array['PIC'] = $row->PIC;
                    $post_array['Budget'] = $row->Budget;
                    $post_array['Keterangan'] = $row->Keterangan;
                    $post_array['ReminderDate'] = $row->ReminderDate;
                    $post_array['Departemen'] = $row->Departemen;
                    $post_array['KetAdmin'] = $row->KetAdmin;
                    $post_array['CreatedOn'] = date("Y-m-d h:i:s A");
                    $post_array['CreatedBy'] = $row->CreatedBy;
                }
                $this->db->insert('pmtrekap',$post_array);
    
            } 
         }
         function editpromise($KodeRequestDetail){
            $query = $this->promise_model->getApprovalRequest($KodeRequestDetail);
                if($query){
                    foreach($query as $row){
                        $post_array['ProjectID'] = $row->ProjectID;
                        $post_array['NamaProject'] = $row->NamaProject;
                        $post_array['KPI'] = $row->KPI;
                        $post_array['Target'] = $row->Target;
                        $post_array['DetilInfo'] = $row->DetilInfo;
                        $post_array['StartDate'] = $row->StartDate;
                        $post_array['DeadlineUREQ'] = $row->DeadlineUREQ;
                        $post_array['Deadline'] = $row->Deadline;
                        $post_array['PIC'] = $row->PIC;
                        $post_array['Budget'] = $row->Budget;
                        $post_array['Keterangan'] = $row->Keterangan;
                        $post_array['ReminderDate'] = $row->ReminderDate;
                        $post_array['Departemen'] = $row->Departemen;
                        $post_array['KetAdmin'] = $row->KetAdmin;
                        $post_array['CreatedOn'] = date("Y-m-d h:i:s A");
                        $post_array['CreatedBy'] = $row->CreatedBy;
                    }
                    $this->db->update('pmtrekap',$post_array,array('ProjectID'=> $post_array['ProjectID']));
        
                } 
             }
         function ApprovalEditPROMISE($KodeUserTask)
         {
                 try
                 {
                     if($this->session->userdata('logged_in'))
                     {
                             $KodeRequestDetail = $this->promise_model->getKodeRequestDetail($KodeUserTask);
                             if($KodeRequestDetail){
                                 $KodeRequestDetail_array = array();
                                 foreach($KodeRequestDetail as $row){
                                     $KodeRequestDetail_array[] = array(
                                         'NoTransaksi' => $row->NoTransaksi, 
                                         'KodeUserTask' => $row->KodeUserTask,
                                     );
                                 }
                             }
                          
                             $data = $this->promise_model->getApprovalRequest($KodeRequestDetail_array[0]['NoTransaksi']);
                             if($data){
                             $data_array = array();
                             foreach($data as $row){
                 
                                 $data_array = array(
                                     'ProjectID' => $row->ProjectID,
                                     'NamaProject' => $row->NamaProject,
                                     'KPI' => $row->KPI,
                                     'Target' => $row->Target,
                                     'StartDate' => $row->StartDate,
                                     'DeadlineUREQ' => $row->DeadlineUREQ,
                                     'Deadline' => $row->Deadline,
                                     'PIC' => $row->PIC,
                                     'Budget' => $row->Budget,
                                     'Departemen' => $row->Departemen,
                                     'Keterangan' => $row->Keterangan,
                                     'CreatedOn' => $row->CreatedOn,
                                     'KodeRequestDetail' => $row->KodeRequestDetail
                                 );
                                
                             }
                         }
                        error_reporting(0);
                        $parameter = $this->promise_model->getApprovalParameter($data_array['KodeRequestDetail']);
                             if($parameter){
                                $parameter_array = array();
                             foreach($parameter as $row){
                                $parameter_array[] = array(
                                     'ProjectID' => $row->ProjectID,
                                     'Parameter' => $row->Parameter,
                                     'Presentase' => $row->Presentase,
                                     'DeadlinePara' => $row->DeadlinePara,
                                     'KoreksiParameter' => $row->KoreksiParameter,
                                     'KoreksiPresentase' => $row->KoreksiPresentase,
                                     'KoreksiDeadline' => $row->KoreksiDeadline,
                                     'Status' => $row->Status,
                                     'FilePathParameter' => $row->FilePathParameter,
                                     'CreatedBy' => $row->CreatedBy,
                                     'CreatedOn' => $row->CreatedOn,
                                     'KodeRequestDetail' => $row->KodeRequestDetail,
                                     'Inserts' => $row->Inserts,
                                     'Updates' => $row->Updates,
                                     'Deletes' => $row->Deletes,
                                 );
                                
                             }
                         }
                             $data2 = array (
                                 'data_array'=>$data_array,
                                 'KodeRequestDetail_array'=>$KodeRequestDetail_array,
                                 'parameter_array' => $parameter_array
                             );
                             $this->load->helper(array('form','url'));
                             $this->template->load('default','PROMISE/ApprovalEditPromise_view',$data2);
                         //}
                     }
                     else
                     {
                         redirect("login?u=PROMISE/PROMISEController/ApprovalEditPROMISE/$KodeUserTask",'refresh');
                     }
                 }
                 catch(Exception $e)
                 {
                     log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
                     throw new Exception( 'Something really gone wrong', 0, $e);
                 }
         }
         
         function ajax_prosesApprovalEditPromise()
         {	
                 $error = 0;
                 $success = "Data Berhasil Disimpan";
                 $KodeRequestDetail = $this->input->post('koderequestdetail');
                 $KodeUserTask = $this->input->post('kodeusertask');
                 $status = $this->input->post('status');
                 $catatan = $this->input->post('catatan');
                 $ProjectID = $this->input->post('projectID');
                 $Action = $this->input->post('action');
                 $query = $this->promise_model->getApprovalRequest($KodeRequestDetail);
                 if($query){
                     foreach($query as $row){
                         $KodeRequest = $row->KodeRequestDetail;
                         $Requester = $row->CreatedBy;
                         $StatusRequest = $row->StatusRequest;
                     }
                     
                     if($StatusRequest == 0)
                     {
                         $this->db->trans_begin();
                         
                         //update status Undangan
                         if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'EP') && $status == 'AP')
                         {								
                         $this->promise_model->updateStatusNewPromiseRequest($KodeRequestDetail,$status,$this->npkLogin);
                         $this->editpromise($KodeRequestDetail);
                        error_reporting(0);
                        $updatedData=$this->promise_model->getUpdatedData($KodeRequestDetail,$ProjectID,$status,$this->npkLogin);
                        if($updatedData){
                            $updatedData_array = array();
                         foreach($updatedData as $row){
                            $updatedData_array[] = array(
                                 'KoreksiParameter' => $row->KoreksiParameter,
                                 'KoreksiPresentase' => $row->KoreksiPresentase,
                                 'KoreksiDeadline' => $row->KoreksiDeadline,
                            );
                        }
                        }
                        for($i=0;$i<count($updatedData_array);$i++){
                        $post_array['Parameter'] = $updatedData_array[$i]['KoreksiParameter'];
                        $post_array['Presentase'] = $updatedData_array[$i]['KoreksiPresentase'];
                        $post_array['DeadlinePara'] = $updatedData_array[$i]['KoreksiDeadline'];
                        $array = array(
                            'KoreksiParameter' => $updatedData_array[$i]['KoreksiParameter'],
                            'KoreksiParameter !=' => '');
                        $this->db->where($array);
                        $this->db->update('pmtprmtrequest',$post_array);
                        }
                        $this->promise_model->DeletedData($KodeRequestDetail,$ProjectID,$status,$this->npkLogin);
                        $this->promise_model->updateStatusParameterRequest($KodeRequestDetail,$ProjectID,$status,$this->npkLogin);
                      
                            $updateAllParameter=$this->promise_model->UpdateAllParameter($KodeRequestDetail);
                           if($updateAllParameter){
                               $updateAllParameter_array = array();
                            foreach($updateAllParameter as $row){
                               $updateAllParameter_array[] = array(
                                    'ParameterID' => $row->ParameterID,
                                    'Deleted' => $row->Deleted,
                                    'ProjectID' => $row->ProjectID,
                                    'Parameter' => $row->Parameter,
                                    'Presentase' => $row->Presentase,
                                    'DeadlinePara' => $row->DeadlinePara,
                                    'Inserts' => $row->Inserts,
                                    'Updates' => $row->Updates,
                                    'Deletes' => $row->Deletes,
                                    'Status' => $row->Status,
                                    'FilePathParameter' => $row->FilePathParameter,
                                    'KoreksiParameter' => $row->KoreksiParameter,
                                    'KoreksiPresentase' => $row->KoreksiPresentase,
                                    'KoreksiDeadline' => $row->KoreksiDeadline,
                                    'UpdatedOn' => $row->UpdatedOn,
                                    'UpdatedBy' => $row->UpdatedBy,
                               );
                           }
                           }
                           for($i=0;$i<count($updateAllParameter_array);$i++){
                            $post_array2['Deleted'] =  $updateAllParameter_array[$i]['Deleted'];
                            $post_array2['ProjectID'] =  $updateAllParameter_array[$i]['ProjectID'];
                            $post_array2['Parameter'] =  $updateAllParameter_array[$i]['Parameter'];
                            $post_array2['Presentase'] =  $updateAllParameter_array[$i]['Presentase'];
                            $post_array2['DeadlinePara'] =  $updateAllParameter_array[$i]['DeadlinePara'];
                            $post_array2['Inserts'] =  '0';
                            $post_array2['Deletes'] =  '0';
                            $post_array2['Updates'] =  '0';
                            $post_array2['Status'] =  $updateAllParameter_array[$i]['Status'];
                            $post_array2['FilePathParameter'] =  $updateAllParameter_array[$i]['FilePathParameter'];
                            $post_array2['KoreksiParameter'] =  $updateAllParameter_array[$i]['KoreksiParameter'];
                            $post_array2['KoreksiPresentase'] =  $updateAllParameter_array[$i]['KoreksiPresentase'];
                            $post_array2['KoreksiDeadline'] =  $updateAllParameter_array[$i]['KoreksiDeadline'];
                            $post_array2['UpdatedOn'] =  $updateAllParameter_array[$i]['UpdatedOn'];
                            $post_array2['UpdatedBy'] =  $updateAllParameter_array[$i]['UpdatedBy'];
                            $array2 = array(
                                'ParameterID' =>  $updateAllParameter_array[$i]['ParameterID'],
                            );
                            $this->db->where($array2);
                            $this->db->update('pmtprmtr',$post_array2);
                            $array3 = array(
                                'StatusRequest' => 'AP',
                            );
                            $post_array4['Deleted'] =  '1';
                            $this->db->update('pmtprmtrequest',$post_array4,$array3);
                            $this->db->update('pmtdetailrequest',$post_array4,$array3);
                            }
                            $projectdetail = $this->promise_model->getApprovalRequest($KodeRequestDetail);
                            if($projectdetail){
                                foreach($projectdetail as $row){
                                    $ProjectID = $row->ProjectID;
                                    $NamaProject = $row->NamaProject;
                                }
                            }
                            $link = "<a href='".base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID."'>Detail</a>";
                            $user = $this->user->dataUser($Requester);
                            if($user){
                                foreach($user as $row){
                                    $to= $row->email;
                                    $nama= $row->nama;
                                }
                            }
                            $subject = "Approval Edit Promise";
                            $message = "Dear ". $nama .",
                            <br/><br/>Request perubahan pada project ". $NamaProject ." telah disetujui. 
                            <br/><br/>Click ". $link ." untuk melihat detail.
                            <br/><br/>Terima Kasih.
                            <br/>PROMISE.";
                            $this->emailPromise($to,$subject,$message);   
                      
                         }
                         
                         if($status == 'DE')
                         {
                             
                         $this->promise_model->updateStatusParameterRequest($KodeRequestDetail, $ProjectID, $status,$this->npkLogin);
                         $array3 = array(
                            'StatusRequest' => 'DE',
                        );
                        $post_array4['Deleted'] =  '1';
                        $this->db->update('pmtprmtrequest',$post_array4,$array3);
                        $this->db->update('pmtdetailrequest',$post_array4,$array3);
                        $projectdetail = $this->promise_model->getLastApprovalRequest($KodeRequestDetail);
                            if($projectdetail){
                                foreach($projectdetail as $row){
                                    $ProjectID = $row->ProjectID;
                                    $NamaProject = $row->NamaProject;
                                }
                            }
                            $link = "<a href='".base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID."'>Detail</a>";
                            $user = $this->user->dataUser($Requester);
                            if($user){
                                foreach($user as $row){
                                    $to= $row->email;
                                    $nama= $row->nama;
                                }
                            }
                            $subject = "Approval Edit Promise";
                            $message = "Dear ". $nama .",
                            <br/><br/>Request perubahan pada project ". $NamaProject ." belum disetujui. 
                            <br/>Catatan: ". $catatan ."
                            <br/><br/>Click ". $link ." untuk melihat detail.
                            <br/><br/>Terima Kasih.
                            <br/>PROMISE.";
                            $this->emailPromise($to,$subject,$message);  
                        }
                         //update user task sekarang
                         if(!$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan))
                             $error += 1;
                         if(!$this->usertask->isAlreadyMaxSequence($this->npkLogin,'EP')&& $status=='AP')
                         {	
                         //create user task baru untuk next proses
                         $KodeUserTaskNext = generateNo('UT');			
                         if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestDetail,$KodeUserTaskNext,"EP".$status,$Requester))
                         {
                             $success = "GAGAL USERTASK";
                             $error += 1;
                         } 
                         }
                         if ($error > 0)
                         {
                             fire_print('log','trans rollback approve Undangan ');
                             $this->db->trans_rollback();
                         }
                         else
                         {
                             fire_print('log','trans commit approve Undangan ');
                             $this->db->trans_commit();
                         }
                         fire_print('log','Proses Trans Approve Undangan selesai ');
                     }
                     else
                     {
                         $success = "GAGAL UPDATE STATUS";
                     }
                 }else{
                     $success = "Tidak ada data";
                 }
                 echo $success;
             //}
         }
         function ParameterApproval($KodeUserTask)
         {
                 try
                 {
                     if($this->session->userdata('logged_in'))
                     {
                             $KodeRequestDetail = $this->promise_model->getKodeRequestDetail($KodeUserTask);
                             if($KodeRequestDetail){
                                 $KodeRequestDetail_array = array();
                                 foreach($KodeRequestDetail as $row){
                                     $KodeRequestDetail_array[] = array(
                                         'NoTransaksi' => $row->NoTransaksi, 
                                         'KodeUserTask' => $row->KodeUserTask,
                                     );
                                 }
                             }
                          
                
                        error_reporting(0);
                        $progress = $this->promise_model->getApprovalProgress($KodeRequestDetail_array[0]['NoTransaksi']);
                             if($progress){
                                $progress_array = array();
                             foreach($progress as $row){
                                $progress_array = array(
                                     'ProjectID' => $row->ProjectID,
                                     'Parameter' => $row->Parameter,
                                     'Presentase' => $row->Presentase,
                                     'DeadlinePara' => $row->DeadlinePara,
                                     'KoreksiParameter' => $row->KoreksiParameter,
                                     'KoreksiPresentase' => $row->KoreksiPresentase,
                                     'KoreksiDeadline' => $row->KoreksiDeadline,
                                     'Status' => $row->Status,
                                     'FilePathParameter' => $row->FilePathParameter,
                                     'CreatedBy' => $row->CreatedBy,
                                     'CreatedOn' => $row->CreatedOn,
                                     'KodeRequestDetail' => $row->KodeRequestDetail,
                                     'Progress' => $row->Progress,
                                    
                                 );
                                
                             }
                         }
                        $data = $this->promise_model->getDetailProject($progress_array['ProjectID']);
                        if($data){
                        $data_array = array();
						foreach($data as $row){
			
							$data_array = array(
                                'NamaProject' => $row->NamaProject,
							);
                           
                        }
                    }
                             $data2 = array (
                                 'title' => 'Progress Approval',
                                 'data_array' => $data_array,
                                 'KodeRequestDetail_array'=>$KodeRequestDetail_array,
                                 'progress_array' => $progress_array
                             );
                             $this->load->helper(array('form','url'));
                             $this->template->load('default','PROMISE/ApprovalProgressPromise_view',$data2);
                         //}
                     }
                     else
                     {
                         redirect("login?u=PROMISE/PROMISEController/ApprovalEditPROMISE/$KodeUserTask",'refresh');
                     }
                 }
                 catch(Exception $e)
                 {
                     log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
                     throw new Exception( 'Something really gone wrong', 0, $e);
                 }
         }
         
         function ajax_prosesParameterApproval()
         {	
                 $error = 0;
                 $success = "Data Berhasil Disimpan";
                 $KodeRequestDetail = $this->input->post('koderequestdetail');
                 $KodeUserTask = $this->input->post('kodeusertask');
                 $status = $this->input->post('status');
                 $catatan = $this->input->post('catatan');
                 $ProjectID = $this->input->post('projectID');
                 $query = $this->promise_model->getApprovalProgress($KodeRequestDetail);
                 if($query){
                     foreach($query as $row){
                         $KodeRequest = $row->KodeRequestDetail;
                         $Requester = $row->CreatedBy;
                         $StatusRequest = $row->StatusRequest;
                     }
             
                     if($StatusRequest == 0)
                     {
                         $this->db->trans_begin();
                         
                         //update status Undangan
                         if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'PA') && $status == 'AP')
                         {								
                         //$this->promise_model->updateStatusNewPromiseRequest($KodeRequestDetail,$status,$this->npkLogin);
                        
                        error_reporting(0);
                        
                        $this->promise_model->updateStatusProgressRequest($KodeRequestDetail,$ProjectID,$status,$this->npkLogin);
                      
                            $updateProgress=$this->promise_model->UpdateProgressApproved($KodeRequestDetail);
                           if($updateProgress){
                            $updateProgress_array = array();
                            foreach($updateProgress as $row){
                                $updateProgress_array = array(
                                    'ParameterID' => $row->ParameterID,
                                    'Deleted' => $row->Deleted,
                                    'ProjectID' => $row->ProjectID,
                                    'Parameter' => $row->Parameter,
                                    'Presentase' => $row->Presentase,
                                    'DeadlinePara' => $row->DeadlinePara,
                                    'Inserts' => $row->Inserts,
                                    'Updates' => $row->Updates,
                                    'Deletes' => $row->Deletes,
                                    'Status' => $row->Status,
                                    'Progress' => $row->Progress,
                                    'FilePathParameter' => $row->FilePathParameter,
                                    'KoreksiParameter' => $row->KoreksiParameter,
                                    'KoreksiPresentase' => $row->KoreksiPresentase,
                                    'KoreksiDeadline' => $row->KoreksiDeadline,
                                    'UpdatedOn' => $row->UpdatedOn,
                                    'UpdatedBy' => $row->UpdatedBy,
                               );
                           }
                           }
        
                            $post_array2['Deleted'] =  $updateProgress_array['Deleted'];
                            $post_array2['ProjectID'] =  $updateProgress_array['ProjectID'];
                            $post_array2['Parameter'] =  $updateProgress_array['Parameter'];
                            $post_array2['Presentase'] =  $updateProgress_array['Presentase'];
                            $post_array2['DeadlinePara'] =  $updateProgress_array['DeadlinePara'];
                            $post_array2['Inserts'] =  '0';
                            $post_array2['Deletes'] =  '0';
                            $post_array2['Updates'] =  '0';
                            $post_array2['Progress'] =  '0';
                            $post_array2['Status'] =  $updateProgress_array['Status'];
                            $post_array2['FilePathParameter'] =  $updateProgress_array['FilePathParameter'];
                            $post_array2['KoreksiParameter'] =  $updateProgress_array['KoreksiParameter'];
                            $post_array2['KoreksiPresentase'] =  $updateProgress_array['KoreksiPresentase'];
                            $post_array2['KoreksiDeadline'] =  $updateProgress_['UpdatedOn'];
                            $post_array2['UpdatedBy'] =  $updateProgress_array['UpdatedBy'];
                            $array2 = array(
                                'ParameterID' =>  $updateProgress_array['ParameterID'],
                            );
                            $this->db->where($array2);
                            $this->db->update('pmtprmtr',$post_array2);
                            $post_array3['Deleted'] =  '1';
                            $this->db->where($array2);
                            $this->db->update('pmtprmtrequest',$post_array3);
                            $projectdetail = $this->promise_model->getDetailProject($post_array2['ProjectID']);
                            if($projectdetail){
                                foreach($projectdetail as $row){
                                    $ProjectID = $row->ProjectID;
                                    $NamaProject = $row->NamaProject;
                                }
                            }
                            $link = "<a href='".base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID."'>Detail</a>";
                            $user = $this->user->dataUser($Requester);
                            if($user){
                                foreach($user as $row){
                                    $to= $row->email;
                                    $nama= $row->nama;
                                }
                            }
                            $subject = "Approval Progress Promise";
                            $message = "Dear ". $nama .",
                            <br/><br/>Progress Indikator ". $post_array2['Parameter'] ." pada project ". $NamaProject ." telah disetujui. 
                            <br/><br/>Click ". $link ." untuk melihat detail.
                            <br/><br/>Terima Kasih.
                            <br/>PROMISE.";
                            $this->emailPromise($to,$subject,$message);   
                           
                           
                           
                      
                         }
                         
                         if($status == 'DE')
                         {
                             
                         $this->promise_model->updateStatusProgressRequest($KodeRequestDetail, $ProjectID, $status,$this->npkLogin);
                         $array3 = array(
                            'StatusRequest' => 'DE',
                        );
                        $post_array4['Deleted'] =  '1';
                        $this->db->update('pmtprmtrequest',$post_array4,$array3);
                        
                        $updateProgress=$this->promise_model->UpdateProgressApproved($KodeRequestDetail);
                        if($updateProgress){
                         $updateProgress_array = array();
                         foreach($updateProgress as $row){
                             $updateProgress_array = array(
                                 'ProjectID' => $row->ProjectID,
                                 'Parameter' => $row->Parameter,
                            );
                        }
                        }
                         $post_array2['ProjectID'] =  $updateProgress_array['ProjectID'];
                         $post_array2['Parameter'] =  $updateProgress_array['Parameter'];
                         $projectdetail = $this->promise_model->getDetailProject($post_array2['ProjectID']);
                         
                         if($projectdetail){
                             foreach($projectdetail as $row){
                                 $ProjectID = $row->ProjectID;
                                 $NamaProject = $row->NamaProject;
                             }
                         }
                         $link = "<a href='".base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID."'>Detail</a>";
                         $user = $this->user->dataUser($Requester);
                         if($user){
                             foreach($user as $row){
                                 $to= $row->email;
                                 $nama= $row->nama;
                             }
                         }

                        $subject = "Approval Progress Promise";
                        $message = "Dear ". $nama .",
                        <br/><br/>Progress Indikator ". $post_array2['Parameter'] ." pada project ". $NamaProject ." belum disetujui. 
                        <br/>Catatan: ". $catatan ."
                        <br/><br/>Click ". $link ." untuk melihat detail.
                        <br/><br/>Terima Kasih.
                        <br/>PROMISE.";
                        $this->emailPromise($to,$subject,$message);   
                        }
                         //update user task sekarang
                         if(!$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan))
                             $error += 1;
                         if(!$this->usertask->isAlreadyMaxSequence($this->npkLogin,'PA')&& $status=='AP')
                         {	
                         //create user task baru untuk next proses
                         $KodeUserTaskNext = generateNo('UT');			
                         if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestDetail,$KodeUserTaskNext,"PA".$status,$Requester))
                         {
                             $success = "GAGAL USERTASK";
                             $error += 1;
                         } 
                         }
                         if ($error > 0)
                         {
                             fire_print('log','trans rollback approve Undangan ');
                             $this->db->trans_rollback();
                         }
                         else
                         {
                             fire_print('log','trans commit approve Undangan ');
                             $this->db->trans_commit();
                         }
                         fire_print('log','Proses Trans Approve Undangan selesai ');
                     }
                     else
                     {
                         $success = "GAGAL UPDATE STATUS";
                     }
                      $ProjectProgress = $this->promise_model->getProgressDetail($ProjectID);
                            if($ProjectProgress){
                                $ProjectProgress_array = array();
                                foreach($ProjectProgress as $row){
                                    $ProjectProgress_array = array(
                                        'Presentase' => $row->Presentase,
                                    );
                                }
                            }	
                     if($ProjectProgress_array['Presentase']>=100 && $this->usertask->isAlreadyMaxSequence($this->npkLogin,'PA') && $status = 'AP'){
                        $lastNo = $this->promise_model->getLastKodePR();
                        $NoUrut = substr($lastNo,2,10);
                        $NoUrut = (int)$NoUrut;
                        $NoUrut = $NoUrut + 1;
                        $KodeRequest = "FP$NoUrut";
                        $KodeRequestID = $NoUrut;
                        if($lastNo=='' || $lastNo==null || $lastNo=='0'){
                            $Notransaksi = generateNo('PR');
                            $NoUruta = substr($Notransaksi,2,10);
                            $NoUruta = (int)$NoUruta;
                            $NoUruta = $NoUruta + 1;
                            $KodeRequest = "FP$NoUruta";
                            $KodeRequestID = $NoUruta;
                        }  
                        $data = $this->promise_model->getDetailProject($ProjectID);
                        if($data){
                        $data_array = array();
                        foreach($data as $row){
            
                            $data_array = array(
                                $post_array['ProjectID'] = $row->ProjectID,
                                $post_array['NamaProject'] = $row->NamaProject,
                                $post_array['KPI'] = $row->KPI,
                                $post_array['Target'] = $row->Target,
                                $post_array['StartDate'] = $row->StartDate,
                                $post_array['EndDate'] = $row->EndDate,
                                $post_array['DeadlineUREQ'] = $row->DeadlineUREQ,
                                $post_array['Deadline'] = $row->Deadline,
                                $post_array['PIC'] = $row->PIC,
                                $post_array['Budget'] = $row->Budget,
                                $post_array['Departemen'] = $row->Departemen,
                                $post_array['ProjectType'] = $row->ProjectType,
                                $post_array['Keterangan'] = $row->Keterangan,
                                $post_array['ReminderDate'] = $row->ReminderDate,
                                $post_array['CreatedOn'] = $row->CreatedOn,
                                $post_array['CreatedBy'] = $row->CreatedBy,
                                $post_array['KodeRequestID'] = $KodeRequestID,
                                $post_array['KodeRequestDetail'] = $KodeRequest, 
                                
                           
                            );
                            $this->db->insert('pmtdetailrequest',$post_array);
                           
                      }
                    }
                    // $getnpk = $this->promise_model->getDataUserByNama($post_array['PIC']);
                    //     if($getnpk){
                    //     $getnpk_array = array();
                    //     foreach($getnpk as $row){
            
                    //         $getnpk_array = array(                      
                    //             $post_array2['NPK'] = $row->npk,
                    //         );  
                           
                    //   }
                    // }

                     $KodeUserTask = generateNo('UT');
                     $this->usertask->tambahUserTask($Requester,$post_array['KodeRequestDetail'],$KodeUserTask,"FP");
                    }	
                 }else{
                     $success = "Tidak ada data";
                 }
                 echo $success;
             //}
         }
         function ApprovalMOMPROMISE($KodeUserTask)
         {
                 try
                 {
                     if($this->session->userdata('logged_in'))
                     {
                             $KodeRequestMOM = $this->promise_model->getKodeRequestDetail($KodeUserTask);
                             if($KodeRequestMOM){
                                 $KodeRequestMOM_array = array();
                                 foreach($KodeRequestMOM as $row){
                                     $KodeRequestMOM_array[] = array(
                                         'NoTransaksi' => $row->NoTransaksi, 
                                         'KodeUserTask' => $row->KodeUserTask,
                                     );
                                 }
                             }
                          
                        error_reporting(0);
                        $MOM = $this->promise_model->getApprovalMOM($KodeRequestMOM_array[0]['NoTransaksi']);
                             if($MOM){
                                $MOM_array = array();
                             foreach($MOM as $row){
                                $MOM_array[] = array(
                                     'ProjectID' => $row->ProjectID,
                                     'Judul' => $row->Judul,
                                     'Deskripsi' => $row->Deskripsi,
                                     'KoreksiJudul' => $row->KoreksiJudul,
                                     'KoreksiDeskripsi' => $row->KoreksiDeskripsi,
                                     'CreatedBy' => $row->CreatedBy,
                                     'CreatedOn' => $row->CreatedOn,
                                     'KodeRequestMOM' => $row->KodeRequestMOM,
                                     'Inserts' => $row->Inserts,
                                     'Updates' => $row->Updates,
                                     'Deletes' => $row->Deletes,
                                 );
                                
                             }
                         }
                             $data2 = array (
                                 'KodeRequestMOM_array'=>$KodeRequestMOM_array,
                                 'MOM_array' => $MOM_array
                             );
                             $this->load->helper(array('form','url'));
                             $this->template->load('default','PROMISE/ApprovalMOMPromise_view',$data2);
                         //}
                     }
                     else
                     {
                         redirect("login?u=PROMISE/PROMISEController/ApprovalMOMPROMISE/$KodeUserTask",'refresh');
                     }
                 }
                 catch(Exception $e)
                 {
                     log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
                     throw new Exception( 'Something really gone wrong', 0, $e);
                 }
         }
         
         function ajax_prosesApprovalMOMPromise()
         {	
                 $error = 0;
                 $success = "Data Berhasil Disimpan";
                 $KodeRequestMOM = $this->input->post('koderequestmom');
                 $KodeUserTask = $this->input->post('kodeusertask');
                 $status = $this->input->post('status');
                 $catatan = $this->input->post('catatan');
                 $ProjectID = $this->input->post('projectID');
                 $Action = $this->input->post('action');
                 $query = $this->promise_model->getApprovalMOM($KodeRequestMOM);
                 if($query){
                     foreach($query as $row){
                         $KodeRequest = $row->KodeRequestMOM;
                         $Requester = $row->CreatedBy;
                         $StatusRequest = $row->StatusRequest;
                     }
                     
                     if($StatusRequest == 0)
                     {
                         $this->db->trans_begin();
                         
                         //update status Undangan
                         if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'PM') && $status == 'AP')
                         {								
                         //$this->promise_model->updateStatusNewPromiseRequest($KodeRequestDetail,$status,$this->npkLogin);
                        
                        //error_reporting(0);
                        $updatedData=$this->promise_model->getUpdatedMOM($KodeRequestMOM,$ProjectID,$status,$this->npkLogin);
                        if($updatedData){
                            $updatedData_array = array();
                         foreach($updatedData as $row){
                            $updatedData_array[] = array(
                                 'KoreksiJudul' => $row->KoreksiJudul,
                                 'KoreksiDeskripsi' => $row->KoreksiDeskripsi,
                            );
                        }
                        }
                        for($i=0;$i<count($updatedData_array);$i++){
                        $post_array['Judul'] = $updatedData_array[$i]['KoreksiJudul'];
                        $post_array['Deskripsi'] = $updatedData_array[$i]['KoreksiDeskripsi'];
                        $array = array(
                            'KoreksiJudul' => $updatedData_array[$i]['KoreksiJudul'],
                            'KoreksiJudul !=' => '');
                        $this->db->where($array);
                        $this->db->update('pmtmomrequest',$post_array);
                        }
                        $this->promise_model->DeletedMOM($KodeRequestMOM,$ProjectID,$status,$this->npkLogin);
                        $this->promise_model->updateStatusMOMRequest($KodeRequestMOM,$ProjectID,$status,$this->npkLogin);
                      
                            $updateAllMOM=$this->promise_model->UpdateAllMOM($KodeRequestMOM);
                           if($updateAllMOM){
                               $updateAllMOM_array = array();
                            foreach($updateAllMOM as $row){
                               $updateAllMOM_array[] = array(
                                    'MomID' => $row->MomID,
                                    'Deleted' => $row->Deleted,
                                    'ProjectID' => $row->ProjectID,
                                    'Judul' => $row->Judul,
                                    'Deskripsi' => $row->Deskripsi,
                                    'Inserts' => $row->Inserts,
                                    'Updates' => $row->Updates,
                                    'Deletes' => $row->Deletes,
                                    'KoreksiJudul' => $row->KoreksiJudul,
                                    'KoreksiDeskripsi' => $row->KoreksiDeskripsi,
                                    'UpdatedOn' => $row->UpdatedOn,
                                    'UpdatedBy' => $row->UpdatedBy,
                               );
                           }
                           }
                           for($i=0;$i<count($updateAllMOM_array);$i++){
                            $post_array2['Deleted'] =  $updateAllMOM_array[$i]['Deleted'];
                            $post_array2['ProjectID'] =  $updateAllMOM_array[$i]['ProjectID'];
                            $post_array2['Judul'] =  $updateAllMOM_array[$i]['Judul'];
                            $post_array2['Deskripsi'] =  $updateAllMOM_array[$i]['Deskripsi'];
                            $post_array2['Inserts'] =  '0';
                            $post_array2['Deletes'] =  '0';
                            $post_array2['Updates'] =  '0';
                            $post_array2['KoreksiJudul'] =  $updateAllMOM_array[$i]['KoreksiJudul'];
                            $post_array2['KoreksiDeskripsi'] =  $updateAllMOM_array[$i]['KoreksiDeskripsi'];
                            $post_array2['UpdatedOn'] =  $updateAllMOM_array[$i]['UpdatedOn'];
                            $post_array2['UpdatedBy'] =  $updateAllMOM_array[$i]['UpdatedBy'];
                            $array2 = array(
                                'MomID' =>  $updateAllMOM_array[$i]['MomID'],
                            );
                            $this->db->where($array2);
                            $this->db->update('pmtmom',$post_array2);
                            $post_array3['Deleted'] =  '1';
                            $this->db->where($array2);
                            $this->db->update('pmtmomrequest',$post_array3);
                            $projectdetail = $this->promise_model->getDetailProject($post_array2['ProjectID']);
                            if($projectdetail){
                                foreach($projectdetail as $row){
                                    $ProjectID = $row->ProjectID;
                                    $NamaProject = $row->NamaProject;
                                }
                            }
                            $link = "<a href='".base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID."'>Detail</a>";
                            $user = $this->user->dataUser($Requester);
                            if($user){
                                foreach($user as $row){
                                    $to= $row->email;
                                    $nama= $row->nama;
                                }
                            }
                            $subject = "Approval MOM Promise";
                            $message = "Dear ". $nama .",
                            <br/><br/>Request Perubahan MOM ". $post_array2['Judul'] ." pada project ". $NamaProject ." telah disetujui. 
                            <br/><br/>Click ". $link ." untuk melihat detail.
                            <br/><br/>Terima Kasih.
                            <br/>PROMISE.";
                            $this->emailPromise($to,$subject,$message);   
                      
                      
                            }   
                      
                         }
                         
                         if($status == 'DE')
                         {
                             
                         $this->promise_model->updateStatusMOMRequest($KodeRequestMOM, $ProjectID, $status,$this->npkLogin);
                         $array3 = array(
                            'StatusRequest' => 'DE',
                        );
                        $post_array4['Deleted'] =  '1';
                        $this->db->update('pmtmomrequest',$post_array4,$array3);
                        $updateAllMOM=$this->promise_model->UpdateAllMOM($KodeRequestMOM);
                        if($updateAllMOM){
                            $updateAllMOM_array = array();
                         foreach($updateAllMOM as $row){
                            $updateAllMOM_array[] = array(
                                 'ProjectID' => $row->ProjectID,
                                 'Judul' => $row->Judul,
                                 'Deskripsi' => $row->Deskripsi
                            );
                        }
                        }
                        for($i=0;$i<count($updateAllMOM_array);$i++){
                         $post_array2['ProjectID'] =  $updateAllMOM_array[$i]['ProjectID'];
                         $post_array2['Judul'] =  $updateAllMOM_array[$i]['Judul'];
                         $post_array2['Deskripsi'] =  $updateAllMOM_array[$i]['Deskripsi'];
                        
                         $projectdetail = $this->promise_model->getDetailProject($post_array2['ProjectID']);
                         if($projectdetail){
                             foreach($projectdetail as $row){
                                 $ProjectID = $row->ProjectID;
                                 $NamaProject = $row->NamaProject;
                             }
                         }
                         $link = "<a href='".base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID."'>Detail</a>";
                         $user = $this->user->dataUser($Requester);
                         if($user){
                             foreach($user as $row){
                                 $to= $row->email;
                                 $nama= $row->nama;
                             }
                         }
                         $subject = "Approval MOM Promise";
                         $message = "Dear ". $nama .",
                         <br/><br/>Request Perubahan MOM ". $post_array2['Judul'] ." pada project ". $NamaProject ." belum disetujui. 
                         <br/>Catatan: ". $catatan ."
                         <br/><br/>Click ". $link ." untuk melihat detail.
                         <br/><br/>Terima Kasih.
                         <br/>PROMISE.";
                         $this->emailPromise($to,$subject,$message);   
                   
                   
                         }   
                        }
                         //update user task sekarang
                         if(!$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan))
                             $error += 1;
                         if(!$this->usertask->isAlreadyMaxSequence($this->npkLogin,'PM')&& $status=='AP')
                         {	
                         //create user task baru untuk next proses
                         $KodeUserTaskNext = generateNo('UT');			
                         if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestMOM,$KodeUserTaskNext,"PM".$status,$Requester))
                         {
                             $success = "GAGAL USERTASK";
                             $error += 1;
                         } 
                         }
                         if ($error > 0)
                         {
                             fire_print('log','trans rollback approve Undangan ');
                             $this->db->trans_rollback();
                         }
                         else
                         {
                             fire_print('log','trans commit approve Undangan ');
                             $this->db->trans_commit();
                         }
                         fire_print('log','Proses Trans Approve Undangan selesai ');
                     }
                     else
                     {
                         $success = "GAGAL UPDATE STATUS";
                     }
                 }else{
                     $success = "Tidak ada data";
                 }
                 echo $success;
             //}
         }
         
         function ApprovalFinalPROMISE($KodeUserTask)
         {
                 try
                 {
                     if($this->session->userdata('logged_in'))
                     {
                             $KodeRequestDetail = $this->promise_model->getKodeRequestDetail($KodeUserTask);
                             if($KodeRequestDetail){
                                 $KodeRequestDetail_array = array();
                                 foreach($KodeRequestDetail as $row){
                                     $KodeRequestDetail_array[] = array(
                                         'NoTransaksi' => $row->NoTransaksi, 
                                         'KodeUserTask' => $row->KodeUserTask,
                                     );
                                 }
                             }
                          
                             $data = $this->promise_model->getApprovalRequest($KodeRequestDetail_array[0]['NoTransaksi']);
                             if($data){
                             $data_array = array();
                             foreach($data as $row){
                 
                                 $data_array = array(
                                     'ProjectID' => $row->ProjectID,
                                     'NamaProject' => $row->NamaProject,
                                     'KPI' => $row->KPI,
                                     'Target' => $row->Target,
                                     'StartDate' => $row->StartDate,
                                     'DeadlineUREQ' => $row->DeadlineUREQ,
                                     'Deadline' => $row->Deadline,
                                     'PIC' => $row->PIC,
                                     'Budget' => $row->Budget,
                                     'Departemen' => $row->Departemen,
                                     'Keterangan' => $row->Keterangan,
                                     'CreatedOn' => $row->CreatedOn,
                                     'FilePath' => $row->FilePath,
                                     'KodeRequestDetail' => $row->KodeRequestDetail
                                 );
                                
                             }
                         }
                         $datarekap = $this->promise_model->getDetailProject($data_array['ProjectID']);
                             $datarekap_array = array();
                             foreach($datarekap as $row){
                 
                                 $datarekap_array = array(
                                     'ProjectID' => $row->ProjectID,
                                     'NamaProject' => $row->NamaProject,
                                     'KPI' => $row->KPI,
                                     'Target' => $row->Target,
                                     'StartDate' => $row->StartDate,
                                     'DeadlineUREQ' => $row->DeadlineUREQ,
                                     'Deadline' => $row->Deadline,
                                     'PIC' => $row->PIC,
                                     'Budget' => $row->Budget,
                                     'Departemen' => $row->Departemen,
                                     'Keterangan' => $row->Keterangan,
                                     'CreatedOn' => $row->CreatedOn,
                                     'FilePath' => $row->FilePath,
                                 );
                                
                             }
                         
                        error_reporting(0);
                        $parameter = $this->promise_model->getAllPara($data_array['ProjectID']);
                             if($parameter){
                                $parameter_array = array();
                             foreach($parameter as $row){
                                $parameter_array[] = array(
                                     'Parameter' => $row->Parameter,
                                     'Presentase' => $row->Presentase,
                                     'DeadlinePara' => $row->DeadlinePara,
                                     'Status' => $row->Status,
                                     'FilePathParameter' => $row->FilePathParameter,
                                 );
                                
                             }
                         }
                             $data2 = array (
                                 'data_array'=>$data_array,
                                 'datarekap_array' => $datarekap_array,
                                 'KodeRequestDetail_array'=>$KodeRequestDetail_array,
                                 'parameter_array' => $parameter_array
                             );
                             $this->load->helper(array('form','url'));
                             $this->template->load('default','PROMISE/ApprovalFinalPromise_view',$data2);
                         //}
                     }
                     else
                     {
                         redirect("login?u=PROMISE/PROMISEController/ApprovalFinalPROMISE/$KodeUserTask",'refresh');
                     }
                 }
                 catch(Exception $e)
                 {
                     log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
                     throw new Exception( 'Something really gone wrong', 0, $e);
                 }
         }
         
         function ajax_prosesApprovalFinalPromise()
         {	
                 $error = 0;
                 $success = "Data Berhasil Disimpan";
                 $KodeRequestDetail = $this->input->post('koderequestdetail');
                 $KodeUserTask = $this->input->post('kodeusertask');
                 $status = $this->input->post('status');
                 $catatan = $this->input->post('catatan');
                 $ProjectID = $this->input->post('projectID');
                 $query = $this->promise_model->getApprovalRequest($KodeRequestDetail);
                 if($query){
                     foreach($query as $row){
                         $ProjectID = $row->ProjectID;
                         $KodeRequest = $row->KodeRequestDetail;
                         $Requester = $row->CreatedBy;
                         $StatusRequest = $row->StatusRequest;
                     }
                     
                     if($StatusRequest == 0)
                     {
                         $this->db->trans_begin();
                         
                         //update status Undangan
                         if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'EP') && $status == 'AP')
                         {				
                         $statusProject = "DONE";
                         $this->promise_model->updateStatusFinalPromiseRequest($KodeRequestDetail,$status,$this->npkLogin,$statusProject);
                        
                            $post_array2['Status'] = "DONE";
                            $array2 = array(
                                'ProjectID' => $ProjectID,
                            );
                            $this->db->where($array2);
                            $this->db->update('pmtrekap',$post_array2);
                        

                            $projectdetail = $this->promise_model->getApprovalRequest($KodeRequestDetail);
                            if($projectdetail){
                                foreach($projectdetail as $row){
                                    $ProjectID = $row->ProjectID;
                                    $NamaProject = $row->NamaProject;
                                }
                            }
                            $link = "<a href='".base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID."'>Detail</a>";
                            $user = $this->user->dataUser($Requester);
                            if($user){
                                foreach($user as $row){
                                    $to= $row->email;
                                    $nama= $row->nama;
                                }
                            }
                            $subject = "Final Approval Promise";
                            $message = "Dear ". $nama .",
                            <br/><br/>Project ". $NamaProject ." telah selesai, terima kasih telah menyelesaikan project ini dengan baik. 
                            <br/><br/>Click ". $link ." untuk melihat detail.
                            <br/><br/>Terima Kasih.
                            <br/>PROMISE.";
                            $this->emailPromise($to,$subject,$message);   
                      
                         }
                         
                         if($status == 'DE')
                         {
                        $statusProject = "";
                        $this->promise_model->updateStatusFinalPromiseRequest($KodeRequestDetail,$status,$this->npkLogin,$statusProject);
                         $array3 = array(
                            'StatusRequest' => 'DE',
                        );
                        $post_array4['Deleted'] =  '1';
                        $this->db->update('pmtdetailrequest',$post_array4,$array3);
                        $projectdetail = $this->promise_model->getLastApprovalRequest($KodeRequestDetail);
                            if($projectdetail){
                                foreach($projectdetail as $row){
                                    $ProjectID = $row->ProjectID;
                                    $NamaProject = $row->NamaProject;
                                }
                            }
                            $link = "<a href='".base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID."'>Detail</a>";
                            $user = $this->user->dataUser($Requester);
                            if($user){
                                foreach($user as $row){
                                    $to= $row->email;
                                    $nama= $row->nama;
                                }
                            }
                            $subject = "Final Approval Promise";
                            $message = "Dear ". $nama .",
                            <br/><br/>Final Approval Project ". $NamaProject ." belum disetujui. 
                            <br/>Catatan: ". $catatan ."
                            <br/><br/>Click ". $link ." untuk melihat detail.
                            <br/><br/>Terima Kasih.
                            <br/>PROMISE.";
                            $this->emailPromise($to,$subject,$message);  
                        }
                         //update user task sekarang
                         if(!$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan))
                             $error += 1;
                         if(!$this->usertask->isAlreadyMaxSequence($this->npkLogin,'FP')&& $status=='AP')
                         {	
                         //create user task baru untuk next proses
                         $KodeUserTaskNext = generateNo('UT');			
                         if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestDetail,$KodeUserTaskNext,"FP".$status,$Requester))
                         {
                             $success = "GAGAL USERTASK";
                             $error += 1;
                         } 
                         }
                         if ($error > 0)
                         {
                             fire_print('log','trans rollback approve Undangan ');
                             $this->db->trans_rollback();
                         }
                         else
                         {
                             fire_print('log','trans commit approve Undangan ');
                             $this->db->trans_commit();
                         }
                         fire_print('log','Proses Trans Approve Undangan selesai ');
                     }
                     else
                     {
                         $success = "GAGAL UPDATE STATUS";
                     }
                 }else{
                     $success = "Tidak ada data";
                 }
                 echo $success;
             //}
         }

         function emailPromise($to, $subject, $message)
         {
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'PROMISE');
				$this->email->to($to); 
				$this->email->subject($subject);
				$this->email->message($message);	

				if( ! $this->email->send())
				{	
					return false;
				}else{
					return true;
				}
         }
         function UploadFinalDokumen($KodeUserTask){
            try
            {
                if($this->session->userdata('logged_in'))
                {
                    $session_data = $this->session->userdata('logged_in');
                    $this->npkLogin = $session_data['npk'];
                    
                    $user = $this->user->dataUser($this->npkLogin);
                    $KodeRequestDetail = $this->promise_model->getKodeRequestDetail($KodeUserTask);
                             if($KodeRequestDetail){
                                 $KodeRequestDetail_array = array();
                                 foreach($KodeRequestDetail as $row){
                                     $KodeRequestDetail_array[] = array(
                                         'NoTransaksi' => $row->NoTransaksi, 
                                         'KodeUserTask' => $row->KodeUserTask,
                                     );
                                 }
                             }

                             $data = $this->promise_model->getApprovalRequest($KodeRequestDetail_array[0]['NoTransaksi']);
                             if($data){
                             $data_array = array();
                             foreach($data as $row){
                 
                                 $data_array = array(
                                     'ProjectID' => $row->ProjectID,
                                     'NamaProject' => $row->NamaProject,
                                     'KPI' => $row->KPI,
                                     'Target' => $row->Target,
                                     'StartDate' => $row->StartDate,
                                     'DeadlineUREQ' => $row->DeadlineUREQ,
                                     'Deadline' => $row->Deadline,
                                     'PIC' => $row->PIC,
                                     'Budget' => $row->Budget,
                                     'Departemen' => $row->Departemen,
                                     'Keterangan' => $row->Keterangan,
                                     'CreatedOn' => $row->CreatedOn,
                                     'KodeRequestDetail' => $row->KodeRequestDetail,
                                     'FilePath' => $row->FilePath
                                 );
                                
                             }
                         }

                            $dataProject = array(
                                'title'=> 'Upload Dokumen Final PROMISE',
                                'data_array' => $data_array,
                                'KodeRequestDetail_array' => $KodeRequestDetail_array
                            );
                            $this->load->helper(array('form','url'));
                            $this->template->load('default','PROMISE/UploadFinalDokumen_view', $dataProject);
                       
                    
                }
                else
                {
                    redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
                }
            }
            catch(Exception $e)
            {
                log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
                throw new Exception( 'Something really gone wrong', 0, $e);
            }
        }
    
        function UploadFinalPROMISE($ID = '', $KodeUserTask = '', $KodeRequestDetail = ''){
            $config['upload_path']          = './assets/uploads/files/PROMISE/';
            $config['allowed_types']        = 'jpg|png|pdf';
            $config['max_size']             = 1000000000;
            $this->load->library('upload', $config);
            
            if ($this->upload->do_upload('uploadScanPROMISE'))
            {
                $error = array('error' => $this->upload->display_errors());
                $session_data = $this->session->userdata('logged_in');
                $this->npkLogin = $session_data['npk'];
                $sqlSelect = "SELECT * from pmtrekap where ProjectID = '$ID'";
                $data = $this->db->query($sqlSelect);
                $user = $this->user->dataUser($this->npkLogin);
                $filename = $_FILES['uploadScanPROMISE']['name'];
                $data = array('upload_data' => $this->upload->data());
                $filename = $data['upload_data']['file_name'];
                
        
                 //$KodeUserTask = $this->input->post('kodeUsertask');

                    $status = 'AP';
                    $sqlUpdate = "UPDATE pmtrekap SET FilePath = '$filename' where ProjectID = $ID";
                    $this->db->query($sqlUpdate);
        
                    $this->usertask->updateStatusUserTask($KodeUserTask,'AP',$this->npkLogin,'');

                        $lastNo = $this->promise_model->getLastKodePR();
                        $NoUrut = substr($lastNo,2,10);
                        $NoUrut = (int)$NoUrut;
                        $NoUrut = $NoUrut + 1;
                        $KodeRequest = "FP$NoUrut";
                        $KodeRequestID = $NoUrut;
                        if($lastNo=='' || $lastNo==null || $lastNo=='0'){
                            $Notransaksi = generateNo('PR');
                            $NoUruta = substr($Notransaksi,2,10);
                            $NoUruta = (int)$NoUruta;
                            $NoUruta = $NoUruta + 1;
                            $KodeRequest = "FP$NoUruta";
                            $KodeRequestID = $NoUruta;
                        }  

                    $KodeUserTask = generateNo('UT');
                    $this->usertask->tambahUserTask($this->npkLogin ,$KodeRequest,$KodeUserTask,"FP");
                    
                    //echo $KodeUserTask;
                    $query = $this->promise_model->getApprovalRequest($KodeRequestDetail);
                    if($query){
                        foreach($query as $row){
                            $KodeRequest = $row->KodeRequestDetail;
                            $Requester = $row->CreatedBy;
                            $StatusRequest = $row->StatusRequest;
                            $NamaProject = $row->NamaProject;
                            
                        }

                    $this->usertask->updateStatusUserTask($KodeUserTask,'AP',$this->npkLogin,'');
									
					$KodeUserTask = generateNo('UT');
					$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestDetail,$KodeUserTask,"FP");
                    redirect('PROMISE/PROMISEController/ViewListPROMISEUser','refresh');	
     
                }
            }
        }
}
?>