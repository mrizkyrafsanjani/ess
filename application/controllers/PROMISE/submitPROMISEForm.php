<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');

class submitPROMISEForm extends CI_Controller {

	
	 function __construct()
	 {
	   parent::__construct();
	   $this->load->model('promise_model','',TRUE);
	   $this->load->model('user','',TRUE);
	   $this->load->model('usertask','',TRUE);
	   $this->load->library('email');
	   $session_data = $this->session->userdata('logged_in');
	   $this->npkLogin = $session_data['npk']; 
	}

	 function index($post_array)
	 {
	   //This method will have the credentials validation
	   $this->load->library('form_validation');
	   
	   /*$this->form_validation->set_rules('rbDPA', 'DPA', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtKeterangan', 'Keterangan', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtNoPP', 'No PP', 'trim|xss_clean');	 
	   $this->form_validation->set_rules('txtTanggalMulai', 'Tanggal Mulai', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtTanggalSelesai', 'Tanggal Selesai', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtDisplayPP', 'No PP', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKegiatan', 'Jenis Kegiatan', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtOutstanding', 'Outstanding', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtPilihBayar', 'Tipe Bayar Uang Muka', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtWaktuBayarCepat', 'Waktu Pembayaran Tercepat', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBank', 'Bank', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtWaktuSelesaiUM', 'Waktu Penyelesian Paling Lambat', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtNoRek', 'No Rekening', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtPenerima', 'Penerima', 'trim|xss_clean');*/
	  
	   
	  
	  /* if($this->form_validation->run() == false)
	   {
		 //Field validation failed.  User redirected to login page
		 $this->load->view('UangMuka/formUMLain_view');
	   }
	   else
	   {	 */
		 //Go to private area
		 
		 $query = $this->db->get_where('globalparam',array('Name'=>'HeadHRGA'));
		 foreach($query->result() as $row)
		 {
			 $headHRGA = $row->Value;
		 }
		 $query = $this->db->get_where('globalparam',array('Name'=>'HeadMRC'));
		 foreach($query->result() as $row)
		 {
			 $headMRC = $row->Value;
		 }
		 $query = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
		 foreach($query->result() as $row)
		 {
			 $headIT = $row->Value;
		 }
		 $query = $this->db->get_where('globalparam',array('Name'=>'HeadFinInv'));
		 foreach($query->result() as $row)
		 {
			 $headFIINV = $row->Value;
		 }
		 $query = $this->db->get_where('globalparam',array('Name'=>'HeadACT'));
		 foreach($query->result() as $row)
		 {
			 $headACC = $row->Value;
		 }
		 $query = $this->db->get_where('globalparam',array('Name'=>'HeadCA'));
		 foreach($query->result() as $row)
		 {
			 $headCA = $row->Value;
		 }
		 $PresDirDPA1 =$this->user->getSingleUserBasedJabatan("President Director DPA 1")->NPK;
		 $PresDirDPA2 =$this->user->getSingleUserBasedJabatan("President Director DPA 2")->NPK;
		 $DirDPA1 =$this->user->getSingleUserBasedJabatan("Director DPA 1")->NPK;
		 $DirDPA2 =$this->user->getSingleUserBasedJabatan("Director DPA 2")->NPK;
		 $CorpSec =$this->user->getSingleUserBasedJabatan("Corporate Secretary Leader")->NPK;
		 $JenisUser = '';
		 if($this->npkLogin==$CorpSec || $this->npkLogin==$PresDirDPA1 || $this->npkLogin==$PresDirDPA2 || $this->npkLogin==$DirDPA1 || $this->npkLogin==$DirDPA2){
			 $JenisUser = 'Admin';
		 }else if($this->npkLogin==$headHRGA || $this->npkLogin==$headACC || $this->npkLogin==$headCA || $this->npkLogin==$headFIINV || $this->npkLogin==$headIT || $this->npkLogin==$headMRC){
			 $JenisUser = 'Head';
		 }else{
			 $JenisUser = 'User';
		 }

		
				$this->load->library('email');
				if($JenisUser == 'User' || $JenisUser == 'Head'){
				$ProjectID = $this->input->post('txtProjectID');
				$post_array['NamaProject'] = $this->input->post('txtNmProject');
				$post_array['KPI'] = $this->input->post('txtKPI');
				$post_array['Target']  = $this->input->post('txtTarget');
				$post_array['StartDate']  = $this->input->post('txtStartDate');
				$post_array['DeadlineUREQ']  = $this->input->post('txtDeadlineUREQ');
				$post_array['Deadline']  = $this->input->post('txtDeadline');
				$post_array['PIC']  = $this->input->post('txtPIC');
				$post_array['Budget']  = $this->input->post('txtBudget');
				$post_array['Departemen']  = $this->input->post('txtDepartemen');
				$post_array['Keterangan']  = $this->input->post('txtKeterangan');
				$post_array['ReminderDate']  = $this->input->post('txtStartDate');
				$post_array['CreatedOn']  = date('Y-m-d');
				$post_array['CreatedBy']  = $this->npkLogin;
				$lastNo = $this->promise_model->getLastKodePR();
				$NoUrut = substr($lastNo,2,10);
				$NoUrut = (int)$NoUrut;
				$NoUrut = $NoUrut + 1;
				$KodeRequest = "PR$NoUrut";
				$KodeRequestID = $NoUrut;
				if($lastNo=='' || $lastNo==null || $lastNo=='0'){
					$Notransaksi = generateNo('PR');
					$NoUruta = substr($Notransaksi,2,10);
					$NoUruta = (int)$NoUruta;
					$NoUruta = $NoUruta + 1;
					$KodeRequest = "PR$NoUruta";
					$KodeRequestID = $NoUruta;
				}
				$post_array['KodeRequestID'] = $KodeRequestID; 
				$post_array['KodeRequestDetail'] = $KodeRequest; 
				$this->db->insert('pmtdetailrequest',$post_array);
				$KodeUserTask = generateNo('UT');
				$this->usertask->tambahUserTask($this->npkLogin,$post_array['KodeRequestDetail'],$KodeUserTask,"PR");
				
			}else if($JenisUser == 'Admin'){
				
					$ProjectID = $this->input->post('txtProjectID');
					$post_array['NamaProject'] = $this->input->post('txtNmProject');
					$post_array['KPI'] = $this->input->post('txtKPI');
					$post_array['Target']  = $this->input->post('txtTarget');
					$post_array['StartDate']  = $this->input->post('txtStartDate');
					$post_array['DeadlineUREQ']  = $this->input->post('txtDeadlineUREQ');
					$post_array['Deadline']  = $this->input->post('txtDeadline');
					$post_array['PIC']  = $this->input->post('txtPIC');
					$post_array['Budget']  = $this->input->post('txtBudget');
					$post_array['Departemen']  = $this->input->post('txtDepartemen');
					$post_array['Keterangan']  = $this->input->post('txtKeterangan');
					$post_array['ReminderDate']  = $this->input->post('txtStartDate');
					$post_array['CreatedOn']  = date('Y-m-d h:i:s A');
					$post_array['CreatedBy']  = $this->npkLogin;
					$this->promise_model->AddProject();
					//Send Email
					$this->email->from('ess@dpa.co.id', 'PROMISE');
					$nama = $this->promise_model->getDataUserByNama($post_array['PIC']);
					$namaAt = $this->promise_model->getAtasan($post_array['PIC']);
					$namaAta = $namaAt[0]->NamaAtasan;
					$namaAtasan = $this->promise_model->getDataUserByNama($namaAta);
					$CcArr=$namaAtasan[0]->email;
					$recipientArr = array($nama[0]->email);
					$this->email->to($recipientArr);
					$this->email->cc($CcArr);
					$this->email->subject('NEW PROMISE');
					
					$DeadlineUREQDat = '';
					$BudgetDat ='';
					$KeteranganDat ='';
	
					if($post_array['DeadlineUREQ'] <>''){
						$DeadlineUREQDat = '<li>Deadline UREQ: <b>'.$post_array['DeadlineUREQ'] .'</b></li>';
					}
					if($post_array['Budget'] <>''){
						$BudgetDat = '<li>Budget: <b>Rp ' .number_format($post_array['Budget'],2,',','.').'</b></li>';
					}
					if($post_array['Keterangan'] <>''){
						$KeteranganDat = ' <li>Keterangan: <b>'.$post_array['Keterangan'] .'</b></li>';
					}
					$message = 'Dear ' .$post_array['PIC'] .',
						<br/><br/>Terdapat project baru dengan detail sebagai berikut:
						<ul>
						<li>Nama Project: <b>'.$post_array['NamaProject'].'</b></li>
						<li>Target: <b>'.$post_array['Target'] .'</b></li>
						<li>Start Date: <b>'.$post_array['StartDate'] .'</b></li>'
						.$DeadlineUREQDat.'
						<li>Deadline: <b>'.$post_array['Deadline'] .'</b></li>
						<li>PIC: <b>'.$post_array['PIC'].'</b></li>'
						.$BudgetDat.'
						<li>Departemen: <b>'.$post_array['Departemen'] .'</b></li>
						'.$KeteranganDat.'
						<li><a href="'.base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID.'">Lihat Detail Project</a></li>
						</ul>
						Terima kasih.
						<br/>PROMISE.
					';
					$this->email->message($message);
					$this->email->send();
			
				 
				}
				if($JenisUser=='Admin'){
					redirect('PROMISE/PROMISEController/ViewListPROMISEAdmin');				
				}else if($JenisUser=='Head'){
					redirect('PROMISE/PROMISEController/ViewListPROMISEHead');				
				}else{
					redirect('PROMISE/PROMISEController/ViewListPROMISEUser');				
				}

	 }
}
?>
