<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class ListPROMISE extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
	
		$this->load->library('grocery_crud');
		$this->load->model('promise_model','',TRUE);
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("PROMISE") || check_authorizedByName("PROMISE Admin") )
			{
				$this->npkLogin = $session_data['npk'];				
			}
		}else{
			redirect('login','refresh');
		}
    }
	

	public function _listPROMISElihat($JenisUser,$ProjectID)
    {
				
		$crud = new grocery_crud();
		$crud->set_subject('Lihat List PROMISE');
		$crud->set_theme('flexigrid');
		
		
		$crud->set_model('custom_query_model');
		$crud->set_table('pmtprmtr');
		 //Change mo your table name

		$session_data = $this->session->userdata('logged_in');
		$npkLogin = $session_data['npk'];

		$sqlquery = "select ParameterID, ProjectID, Parameter, Presentase, DeadlinePara, Status,FilePathParameter
		from pmtprmtr
		where  deleted=0 and ProjectID = $ProjectID
		order by ParameterID
		";	
		$crud->basic_model->set_query_str($sqlquery); //Query text here
	
			$crud->columns('Parameter','Presentase','DeadlinePara','Status','FilePathParameter');
			$crud->fields('ProjectID','Parameter','Presentase','DeadlinePara','Status','Lokasi');
			
			$crud->display_as('Parameter','Indikator Keberhasilan'); 
			$crud->display_as('Presentase','Presentase');
			$crud->display_as('DeadlinePara','Deadline');
			$crud->display_as('FilePathParameter','Lokasi File');
			$crud->display_as('Status','Status');
			$crud->callback_column('FilePathParameter',array($this,'_callback_lokasi'));
			if($JenisUser=='Admin'){
				$crud->add_action('Upload/Lihat Dokumen Permohonan', base_url('/assets/images/upload.png'), '','',array($this,'callback_action_uploadadmin'));
			}else if($JenisUser=='Head'){
				$crud->add_action('Upload/Lihat Dokumen Permohonan', base_url('/assets/images/upload.png'), '','',array($this,'callback_action_uploadhead'));
			}else{	
				$crud->add_action('Upload/Lihat Dokumen Permohonan', base_url('/assets/images/upload.png'), '','',array($this,'callback_action_uploaduser'));
			}
			$crud->unset_edit();
			$crud->unset_delete();	
			$crud->unset_add();
			$crud->unset_read();
			$crud->unset_print();	
			$crud->unset_export();

		$output = $crud->render();
   
        $this-> _outputview_listPROMISEadminlihat($output);	
    }
 
	public function search_PROMISE_adminlihat($JenisUser,$ProjectID)
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("PROMISE") || check_authorizedByName("PROMISE Admin"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_listPROMISElihat($JenisUser,$ProjectID);
			}
		}else{
			redirect('login','refresh');
		}
	}
 
    function _outputview_listPROMISEadminlihat($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('PROMISE/ListPROMISE_view',$data);
		    
	}
	
	
	public function _listPROMISEedit($JenisUser,$ProjectID)
    {
				
		$crud = new grocery_crud();
		$crud->set_subject('Parameter');
		$crud->set_theme('flexigrid');
		
		
		$crud->set_model('custom_query_model');
		$crud->set_table('pmtprmtr');
		 //Change mo your table name
		$this->ProjectID = $ProjectID;
		$session_data = $this->session->userdata('logged_in');
		$npkLogin = $session_data['npk'];

		$sqlquery = "select ParameterID, ProjectID, Parameter, Presentase, DeadlinePara, Status
		from pmtprmtr
		where  deleted=0 and Inserts = $ProjectID or deleted=0 and Updates = $ProjectID or deleted=0 and Deletes = $ProjectID or deleted=0 and ProjectID = $ProjectID
		order by ParameterID
		";	
		$crud->basic_model->set_query_str($sqlquery); //Query text here
	
			$crud->columns('ParameterID','Parameter','Presentase','DeadlinePara');
			$crud->fields('ParameterID','Parameter','Presentase','DeadlinePara');
			
			$crud->display_as('ParameterID','Parameter ID');
			$crud->display_as('Parameter','Indikator Keberhasilan'); 
			$crud->display_as('Presentase','Presentase');
			$crud->display_as('DeadlinePara','Deadline');
		
			$query = $this->db->get_where('globalparam',array('Name'=>'HeadHRGA'));
					foreach($query->result() as $row)
					{
						$headHRGA = $row->Value;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadMRC'));
					foreach($query->result() as $row)
					{
						$headMRC = $row->Value;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
					foreach($query->result() as $row)
					{
						$headIT = $row->Value;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadFinInv'));
					foreach($query->result() as $row)
					{
						$headFIINV = $row->Value;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadACT'));
					foreach($query->result() as $row)
					{
						$headACC = $row->Value;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadCA'));
					foreach($query->result() as $row)
					{
						$headCA = $row->Value;
					}
					$PresDirDPA1 =$this->user->getSingleUserBasedJabatan("President Director DPA 1")->NPK;
					$PresDirDPA2 =$this->user->getSingleUserBasedJabatan("President Director DPA 2")->NPK;
					$DirDPA1 =$this->user->getSingleUserBasedJabatan("Director DPA 1")->NPK;
					$DirDPA2 =$this->user->getSingleUserBasedJabatan("Director DPA 2")->NPK;
					$CorpSec =$this->user->getSingleUserBasedJabatan("Corporate Secretary Leader")->NPK;
					$JenisUser = '';
					if($this->npkLogin==$CorpSec || $this->npkLogin==$PresDirDPA1 || $this->npkLogin==$PresDirDPA2 || $this->npkLogin==$DirDPA1 || $this->npkLogin==$DirDPA2){
						$JenisUser = 'Admin';
					}else if($this->npkLogin==$headHRGA || $this->npkLogin==$headACC || $this->npkLogin==$headCA || $this->npkLogin==$headFIINV || $this->npkLogin==$headIT || $this->npkLogin==$headMRC){
						$JenisUser = 'Head';
					}else{
						$JenisUser = 'User';
					}
			if($JenisUser <> "Admin"){
			$crud->callback_insert(array($this,'_insertDetailProject'));
			$crud->callback_update(array($this,'_updateDetailProject'));
			$crud->callback_delete(array($this,'_deleteDetailProject'));
			$crud->set_lang_string('delete_success_message',
			'Request Penghapusan Data Sedang Di Proses, Harap Menghubungi Atasan Anda dan Administrator');
			$crud->set_lang_string('insert_success_message',
			'Request Penambahan atau Penambahan Data Sedang Di Proses, Harap Menghubungi Atasan Anda dan Administrator');
			} else {
				$crud->callback_insert(array($this,'_insertDetailProject'));
				$crud->callback_update(array($this,'_updateAdmin'));
				$crud->callback_delete(array($this,'_deleteAdmin'));
			}
			$crud->unset_read();
			$crud->unset_print();	
			$crud->unset_export();

		$output = $crud->render();
   
        $this-> _outputview_listPROMISEadminedit($output);	
    }
 
	public function search_PROMISE_adminedit($JenisUser,$ProjectID)
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("PROMISE") || check_authorizedByName("PROMISE Admin"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_listPROMISEedit($JenisUser,$ProjectID);
			}
		}else{
			redirect('login','refresh');
		}
	}
 
    function _outputview_listPROMISEadminedit($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('PROMISE/ListPROMISE_view',$data);
		    
	}
	public function _callback_lokasi($value, $row)
{
	if ($value<>''){
		return "<a target='_blank' href='".$this->config->base_url()."assets/uploads/files/PROMISE/".$value."'>Lokasi File</a>";
	}
	
}
 
	function add_field_callback_Status($value = '', $primary_key = null)
	{		
		return '<label name="Status">'.$value.'</label>';
	}
	function add_field_callback_CreatedOn($value = '', $primary_key = null)
	{		
		return '<label name="CreatedOn">'.$value.'</label>';
	}
	function _insertDetailProject($post_array)
		{
		$detailProject = array(			
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s'),
			//"ProjectID" => $post_array['ProjectID'],
			"Parameter" => $post_array['Parameter'],
			"DeadlinePara" => $post_array['DeadlinePara'],
			"Inserts" => $this->ProjectID,	
			"Updates" => '0',	
			"Deletes" => '0',	
			"Presentase" => $post_array['Presentase']
		);
	
		$this->db->insert('pmtprmtr',$detailProject);	
	}

	function _updateDetailProject($post_array,$primarykey)
	{
		$detailProject = array(			
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s'),
			//"ProjectID" => $post_array['ProjectID'],
			"KoreksiParameter" => $post_array['Parameter'],
			"KoreksiDeadline" => $post_array['DeadlinePara'],
			"Updates" => $this->ProjectID,	
			"Deletes" => '',	
			"KoreksiPresentase" => $post_array['Presentase']
		);
	
		//$this->db->update('pmtprmtr',$detailProject);	
		$this->db->update('pmtprmtr',$detailProject,array('ParameterID' => $post_array['ParameterID']));	
	}
	function _deleteDetailProject($primary_key)
	{
		$detailProject = array(			
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s'),
			//"ProjectID" => $post_array['ProjectID'],
			"Deletes" => $this->ProjectID,	
			"Updates" => '',	
			"Inserts" => '',	
		);
	
		//$this->db->update('pmtprmtr',$detailProject);
		$this->db->update('pmtprmtr',$detailProject,array('ParameterID' => $primary_key));	
	}
	
	function _updateAdmin($post_array,$primarykey)
	{
		$detailProject = array(			
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s'),
			//"ProjectID" => $post_array['ProjectID'],
			"Parameter" => $post_array['Parameter'],
			"Presentase" => $post_array['Presentase'],
			"DeadlinePara" => $post_array['DeadlinePara'],
		);
	
		//$this->db->update('pmtprmtr',$detailProject);	
		$this->db->update('pmtprmtr',$detailProject,array('ParameterID' => $post_array['ParameterID']));	
	}
	function _deleteAdmin($primary_key)
	{
		$detailProject = array(			
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s'),
			"Deleted" => '1',		
		);
	
		//$this->db->update('pmtprmtr',$detailProject);
		$this->db->update('pmtprmtr',$detailProject,array('ParameterID' => $primary_key));	
	}

	public function _listPROMISEMOMAdmin($JenisUser,$ProjectID)
	{
			
	$crud = new grocery_crud();
	$crud->set_subject('MOM');
	$crud->set_theme('flexigrid');
	
	
	$crud->set_model('custom_query_model');
	$crud->set_table('pmtmom');
	 //Change mo your table name

	$session_data = $this->session->userdata('logged_in');
	$npkLogin = $session_data['npk'];

	$sqlquery = "select MomID, ProjectID, Judul, Deskripsi, CreatedOn
	from pmtmom
	where  deleted=0 and Inserts = $ProjectID or deleted=0 and Updates = $ProjectID or deleted=0 and Deletes = $ProjectID or deleted=0 and ProjectID = $ProjectID
	order by MomID
	";	
			$crud->basic_model->set_query_str($sqlquery); //Query text here
			$this->ProjectID = $ProjectID;
			$crud->columns('MomID','Judul','Deskripsi','CreatedOn');
			$crud->fields('MomID','Judul','Deskripsi','CreatedOn');
			
			$crud->display_as('MomID','Mom ID'); 
			$crud->display_as('Judul','Judul'); 
			$crud->display_as('Deskripsi','Deskripsi');
			$crud->display_as('CreatedOn','Created On');

			$crud->callback_field('CreatedOn',array($this,'add_field_callback_CreatedOn'));
			$query = $this->db->get_where('globalparam',array('Name'=>'HeadHRGA'));
			foreach($query->result() as $row)
			{
				$headHRGA = $row->Value;
			}
			$query = $this->db->get_where('globalparam',array('Name'=>'HeadMRC'));
			foreach($query->result() as $row)
			{
				$headMRC = $row->Value;
			}
			$query = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
			foreach($query->result() as $row)
			{
				$headIT = $row->Value;
			}
			$query = $this->db->get_where('globalparam',array('Name'=>'HeadFinInv'));
			foreach($query->result() as $row)
			{
				$headFIINV = $row->Value;
			}
			$query = $this->db->get_where('globalparam',array('Name'=>'HeadACT'));
			foreach($query->result() as $row)
			{
				$headACC = $row->Value;
			}
			$query = $this->db->get_where('globalparam',array('Name'=>'HeadCA'));
			foreach($query->result() as $row)
			{
				$headCA = $row->Value;
			}
			$PresDirDPA1 =$this->user->getSingleUserBasedJabatan("President Director DPA 1")->NPK;
			$PresDirDPA2 =$this->user->getSingleUserBasedJabatan("President Director DPA 2")->NPK;
			$DirDPA1 =$this->user->getSingleUserBasedJabatan("Director DPA 1")->NPK;
			$DirDPA2 =$this->user->getSingleUserBasedJabatan("Director DPA 2")->NPK;
			$CorpSec =$this->user->getSingleUserBasedJabatan("Corporate Secretary Leader")->NPK;
			$JenisUser = '';
			if($this->npkLogin==$CorpSec || $this->npkLogin==$PresDirDPA1 || $this->npkLogin==$PresDirDPA2 || $this->npkLogin==$DirDPA1 || $this->npkLogin==$DirDPA2){
				$JenisUser = 'Admin';
			}else if($this->npkLogin==$headHRGA || $this->npkLogin==$headACC || $this->npkLogin==$headCA || $this->npkLogin==$headFIINV || $this->npkLogin==$headIT || $this->npkLogin==$headMRC){
				$JenisUser = 'Head';
			}else{
				$JenisUser = 'User';
			}
	if($JenisUser <> "Admin"){
	$crud->callback_insert(array($this,'_insertMOM'));
	$crud->callback_update(array($this,'_updateMOM'));
	$crud->callback_delete(array($this,'_deleteMOM'));
	$crud->set_lang_string('delete_success_message',
	'Request Penghapusan Data Sedang Di Proses, Harap Menghubungi Atasan Anda dan Administrator');
	$crud->set_lang_string('insert_success_message',
	'Request Penambahan atau Perubahan Data Sedang Di Proses, Harap Menghubungi Atasan Anda dan Administrator');
	} else {
		$crud->callback_insert(array($this,'_insertMOM'));
		$crud->callback_update(array($this,'_updateMOMAdmin'));
		$crud->callback_delete(array($this,'_deleteMOMAdmin'));
	}

		$crud->unset_texteditor(array('Deskripsi','full_text'));
		$crud->unset_read();
		$crud->unset_print();	
		$crud->unset_export();

	$output = $crud->render();
 
			$this-> _outputview_listPROMISEMOMAdmin($output);	
	}

	public function search_PROMISE_MOMAdmin($JenisUser,$ProjectID)
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("PROMISE") || check_authorizedByName("PROMISE Admin"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_listPROMISEMOMAdmin($JenisUser,$ProjectID);
			}
		}else{
			redirect('login','refresh');
		}
	}

		function _outputview_listPROMISEMOMAdmin($output = null)
		{
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
				'body' => $output
			);
		$this->load->helper(array('form','url'));
		$this->load->view('PROMISE/ListPROMISE_view',$data);
				
	}
	function callback_action_uploadadmin($primary_key,$row)
	{	
		$id = $primary_key;				
		return "javascript:window.open('" . site_url('PROMISE/PROMISEController/Upload/Admin') . '/' . $id . "','_parent')";
		
	}
	function callback_action_uploadhead($primary_key,$row)
	{	
		$id = $primary_key;				
		return "javascript:window.open('" . site_url('PROMISE/PROMISEController/Upload/Head') . '/' . $id . "','_parent')";
		
	}
	function callback_action_uploaduser($primary_key,$row)
	{	
		$id = $primary_key;				
		return "javascript:window.open('" . site_url('PROMISE/PROMISEController/Upload/User') . '/' . $id . "','_parent')";
		
	}
	function _insertMOM($post_array)
	{
		$detailProject = array(			
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s'),
			//"ProjectID" => $post_array['ProjectID'],
			"Judul" => $post_array['Judul'],
			"Deskripsi" => $post_array['Deskripsi'],
			"Inserts" => $this->ProjectID,	
			"Updates" => '0',	
			"Deletes" => '0',	
		);

		$this->db->insert('pmtmom',$detailProject);	
	}

	function _updateMOM($post_array,$primarykey)
	{
		$detailProject = array(			
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s'),
			//"ProjectID" => $post_array['ProjectID'],
			"KoreksiJudul" => $post_array['Judul'],
			"KoreksiDeskripsi" => $post_array['Deskripsi'],
			"Updates" => $this->ProjectID,	
			"Deletes" => '0',	
		);

		//$this->db->update('pmtprmtr',$detailProject);	
		$this->db->update('pmtmom',$detailProject,array('MomID' => $post_array['MomID']));	
	}
	function _deleteMOM($primary_key)
	{
		$detailProject = array(			
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s'),
			//"ProjectID" => $post_array['ProjectID'],
			"Deletes" => $this->ProjectID,	
			"Updates" => '0',	
			"Inserts" => '0',	
		);

		//$this->db->update('pmtprmtr',$detailProject);
		$this->db->update('pmtmom',$detailProject,array('MomID' => $primary_key));	
	}
	function _updateMOMAdmin($post_array,$primarykey)
	{
		$detailProject = array(			
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s'),
			//"ProjectID" => $post_array['ProjectID'],
			"Judul" => $post_array['Judul'],
			"Deskripsi" => $post_array['Deskripsi'],
		);

		//$this->db->update('pmtprmtr',$detailProject);	
		$this->db->update('pmtmom',$detailProject,array('MomID' => $post_array['MomID']));	
	}
	function _deleteMOMAdmin($primary_key)
	{
		$detailProject = array(			
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s'),
			"Deleted" => '1',		
		);

		//$this->db->update('pmtprmtr',$detailProject);
		$this->db->update('pmtmom',$detailProject,array('MomID' => $primary_key));	
	}
}