<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class InsertExcel extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('promise_model','',TRUE);
        $this->load->helper('file');
        $session_data = $this->session->userdata('logged_in');
        $this->npkLogin = $session_data['npk'];
    }
    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        if($this->session->userdata('logged_in')){
        $excel = new COM("Excel.Application") or die ("ERROR: Unable to instantiate COM!\r\n");
			$excel->WorkBooks->Open(dirname(__FILE__).'\InsertProject.xlsm');	
			$excel->Visible = True;
			//$excel->ActiveWorkBook->Save();
			//$excel->Quit();
            redirect('PROMISE/PROMISEController/ViewListPROMISEAdmin');
        //redirect('login','refresh');
        }else{
        redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }
    
}
