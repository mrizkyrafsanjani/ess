<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class RekapPROMISE extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->library('table');
			$this->load->model('trkSPD','',TRUE);
			$this->load->model('promise_model','',TRUE);
			$this->load->model('menu','',TRUE);
			$this->load->model('user','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				//$dataUser = $this->user->dataUser($session_data['npk']);
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($session_data['koderole']==1 OR $session_data['koderole']==3 OR $session_data['koderole']==13 ){
					$departemen = $this->user->get_departemen();
				}else{
					$departemen = $this->user->get_departemen_by_npk($session_data['npk']);
				}
				$year = $this->trkSPD->get_tahun();
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}
				error_reporting(0);
				$getRekapAdmin = $this->promise_model->getRekapAdmin();
                if($getRekapAdmin){
                    $getRekapAdmin_array = array();
                    foreach($getRekapAdmin as $row){
                        $getRekapAdmin_array[] = array(
							'ProjectID' => $row->ProjectID,
							'NamaProject' => $row->NamaProject,
                            'DetilInfo' => $row->DetilInfo,
							'Deadline' => $row->Deadline,
							'PIC' => $row->PIC,
                            'Departemen' => $row->Departemen,
							'Status' => $row->Status,
							'KetAdmin' => $row->KetAdmin,
							'CreatedOn' => $row->CreatedOn,
							'FilePath' => $row->FilePath
                        );
                    }
                }else{
                    //echo 'gagal';
				}
				$ProjectID=$getRekapAdmin_array[0]['ProjectID'];
				$getProgress = $this->promise_model->getProgress();
                if($getProgress){
                    $getProgress_array = array();
                    foreach($getProgress as $row){
                        $getProgress_array[] = array(
							'Presentase' => $row->Presentase  
                        );
                    }
                }else{
                    //echo 'gagal';
                }
				/*
				if($dataUser){
					$dataUser_array = array();
					foreach($dataUser as $dataUser){
						$dataUserDetail = array(
							'title'=> 'Input Form SPD',
							'menu_array' => $menu_array,
							'npk' => $dataUser->npk,
							 'golongan' => $dataUser->golongan,
							 'nama'=> $dataUser->nama,
							 'jabatan'=> $dataUser->jabatan,
							 'departemen'=> $dataUser->departemen,
							 'atasan'=> $dataUser->atasan,
							 'uangsaku'=> $dataUser->uangsaku,
							 'uangmakan'=> $dataUser->uangmakan					 
						);
					}
				}else{
					echo 'gagal2';
				}
				*/
				if($session_data['koderole'] == 4) //jika head
				{
					$departemen_send = $session_data['departemen'];
				}else{				
					$departemen_send = $this->input->get('cmbDepartemen');
				}
				$data = array(
						'title'=>'Rekap PROMISE',
					   'npk' => $session_data['npk'],
					   'nama' => $session_data['nama'],
					   'role' => $session_data['koderole'],
					   'menu_array' => $menu_array,
					   'table' => $this->trkSPD->getAll($session_data['npk'],$session_data['koderole'],$departemen_send,$this->input->get('cmbTahun')),
					   'departemen_array' => $departemen,
					   'year_array' => $year,
					   'getRekapAdmin_array' => $getRekapAdmin_array,
					   'getProgress_array' => $getProgress_array
				  );
				$this->load->helper(array('form','url'));
				//$this->load->view('home_view', $data);
				$this->template->load('default','PROMISE/RekapPROMISE_view',$data);
				
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
	}
?>