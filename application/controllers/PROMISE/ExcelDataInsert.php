<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "third_party/PHPExcel/PHPExcel.php";
require_once APPPATH . "third_party/PHPExcel/PHPExcel/IOFactory.php";
class ExcelDataInsert extends CI_Controller
{

public function __construct() {
        parent::__construct();
                $this->load->library('excel');//load PHPExcel library 
		//$this->load->model('upload_model');//To Upload file in a directory
                $this->load->model('promise_model');
}	
	
public	function ExcelDataAdd()	{  
//Path of files were you want to upload on localhost (C:/xampp/htdocs/ProjectName/uploads/excel/)	 
         $configUpload['upload_path'] = 'http://localhost/esspmtdev/index.php/assets/uploads/excel/';
         $configUpload['allowed_types'] = 'xls|xlsx|csv';
         $configUpload['max_size'] = '5000';
         $this->load->library('upload', $configUpload);
         $this->upload->do_upload('userfile');	
         $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
         $file_name = $upload_data['file_name']; //uploded file name
		 $extension=$upload_data['file_ext'];    // uploded file extension
		
//$objReader =PHPExcel_IOFactory::createReader('Excel5');     //For excel 2003 
 $objReader= PHPExcel_IOFactory::createReader('Excel2007');	// For excel 2007 	  
          //Set to read only
          $objReader->setReadDataOnly(true); 		  
        //Load excel file
		 $objPHPExcel=$objReader->load('http://localhost/esspmtdev/index.php/assets/uploads/excel/'.$file_name);		 
         $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel      	 
         $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
          //loop from first data untill last data
          for($i=7;$i<=$totalrows;$i++)
          {
              $NamaProject= $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();			
              $KPI= $objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); //Excel Column 1
			  $Target= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue(); //Excel Column 2
			  $DeadlineUREQ=$objWorksheet->getCellByColumnAndRow(7,$i)->getValue(); //Excel Column 3
              $Deadline=$objWorksheet->getCellByColumnAndRow(8,$i)->getValue(); //Excel Column 4
              $PIC= $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();			
              $Budget= $objWorksheet->getCellByColumnAndRow(10,$i)->getValue(); //Excel Column 1
			  $Departemen= $objWorksheet->getCellByColumnAndRow(11,$i)->getValue(); //Excel Column 2
			  $Keterangan=$objWorksheet->getCellByColumnAndRow(12,$i)->getValue(); //Excel Column 3
			  $data_user=array(
                  'NamaProject'=>$NamaProject, 
                  'KPI'=>$KPI,
                  'Target'=>$Target,
                  'DeadlineUREQ'=>$DeadlineUREQ, 
                  'Deadline'=>$Deadline,
                  'PIC'=>$PIC,
                  'Budget'=>$Budget,
                  'Departemen'=>$Departemen,
                  'Keterangan'=>$Keterangan
                );
			  $this->promise_model->Add_User($data_user);
              
						  
          }
           //  unlink('././uploads/excel/'.$file_name); //File Deleted After uploading in database .			 
             redirect(base_url() . "put link were you want to redirect");
	           
       
     }
	
}
	
?>