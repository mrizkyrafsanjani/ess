<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
session_start();
class PROMISEReminder extends CI_Controller{
    var $npkLogin;
    function __construct()
    {
        parent::__construct();    
		$this->load->model('menu','',TRUE);
        $this->load->model('user','',TRUE);		
        $this->load->model('promise_model','',TRUE);
        $this->load->library('email');	

        $this->load->helper('date');
        $session_data = $this->session->userdata('logged_in');
        $this->npkLogin = $session_data['npk'];
    }

    public function index()
    {
        $allDetail = $this->promise_model->getRekapAdmin();
        if($allDetail){
            $allDetail_array = array();
            foreach($allDetail as $row){
                $allDetail_array[] = array(
                            'ProjectID' => $row->ProjectID,
                            'NamaProject' => $row->NamaProject,
                            'KPI' => $row->KPI,
                            'Target' => $row->Target,
                            'DetilInfo' => $row->DetilInfo,
                            'StartDate' => $row->StartDate,
                            'Deadline' => $row->Deadline,
                            'DeadlineUREQ' => $row->DeadlineUREQ,
                            'PIC' => $row->PIC,
                            'Budget' => $row->Budget,
                            'Departemen' => $row->Departemen,
							'Status' => $row->Status,
                            'KetAdmin' => $row->KetAdmin,
                            'Keterangan' => $row->Keterangan,
                            'ReminderDate' => $row->ReminderDate,
                            'CreatedOn' => $row->CreatedOn 
                );
            }
            $today = date("Y-m-d");
            for($i=0;$i<count($allDetail_array);$i++){
                $flagBolehEmail = false;     
                $startNotifikasi = $allDetail_array[$i]['ReminderDate'];
                $startNotifikasi = strtotime($startNotifikasi);
                $firstDayNotifikasi = date("Y-m-d", $startNotifikasi);
                if($today >= $firstDayNotifikasi && $allDetail_array[$i]['Status']<>'DONE'){

                    $ProjectID = $allDetail_array[$i]['ProjectID'];
                    $NmProject = $allDetail_array[$i]['NamaProject'];
                    $KPI = $allDetail_array[$i]['KPI'];
                    $Target = $allDetail_array[$i]['Target'];
                    $StartDate = $allDetail_array[$i]['StartDate'];
                    $DeadlineUREQ = $allDetail_array[$i]['DeadlineUREQ'];
                    $Deadline = $allDetail_array[$i]['Deadline'];
                    $PIC = $allDetail_array[$i]['PIC'];
                    $Budget = $allDetail_array[$i]['Budget'];
                    $Departemen = $allDetail_array[$i]['Departemen'];
                    $Keterangan = $allDetail_array[$i]['Keterangan'];
                    $Reminder = $allDetail_array[$i]['ReminderDate'];
                
                    error_reporting(0);
                    $this->email->from('ess@dpa.co.id', 'PROMISE');
                    $ProjectProgress = $this->promise_model->getProgressDetail($ProjectID);
                    $Prog = $ProjectProgress[0]->Presentase;
                    $nama = $this->promise_model->getDataUserByNama($PIC);
                    $namaAt = $this->promise_model->getAtasan($PIC);
                    $namaAta = $namaAt[0]->NamaAtasan;
                    $namaAtasan = $this->promise_model->getDataUserByNama($namaAta);
                    $CcArr=$namaAtasan[0]->email;
                    $recipientArr = array($nama[0]->email);
                    $this->email->to($recipientArr);
                    $this->email->cc($CcArr);
                    $this->email->subject('PROMISE REMINDER');
                    
                    $DeadlineUREQDat = '';
                    $BudgetDat ='';
                    $KeteranganDat ='';

                if($DeadlineUREQ<>''){
                    $DeadlineUREQDat = '<li>Deadline UREQ: <b>'.$DeadlineUREQ.'</b></li>';
                }
                if($Budget<>''){
                    $BudgetDat = '<li>Budget: <b>Rp ' .number_format($Budget,2,',','.').'</b></li>';
                }
                if($Keterangan<>''){
                    $KeteranganDat = ' <li>Keterangan: <b>'.$Keterangan.'</b></li>';
                }
                $message = 'REMINDER:
                <br/><br/>Dear ' .$PIC.',<br/>
                Harap mengerjakan project atau aktivitas dengan detail sebagai berikut :
                <ul>
                <li>Nama Project: <b>'.$NmProject.'</b></li>
                <li>Target: <b>'.$Target.'</b></li>
                <li>Start Date: <b>'.$StartDate.'</b></li>'
                .$DeadlineUREQDat.'
                <li>Deadline: <b>'.$Deadline.'</b></li>
                <li>PIC: <b>'.$PIC.'</b></li>'
                .$BudgetDat.'
                <li>Departemen: <b>'.$Departemen.'</b></li>
                '.$KeteranganDat.'
                <li>Progress: <b>'.$Prog.'%</b></li>
                <li><a href="'.base_url().'index.php/PROMISE/PROMISEDetail/getNamaProject/User/Lihat/'.$ProjectID.'">Lihat Detail Project</a></li>
                </ul>
                <br/>Terima kasih
                <br/>PROMISE
            ';
            $this->email->message($message);
            if ($Reminder <> null){
            $this->email->send();
            $ReminderD = strtotime($Reminder.'+2 week');
            $ReminderDate = date("Y-m-d", $ReminderD);
            $sqlupdate = "UPDATE pmtrekap SET ReminderDate =?
            WHERE ProjectID = ? and Deleted='0'";
            $this->db->query($sqlupdate,array($ReminderDate, $ProjectID));
            }
            }
            }
        }else{
            echo 'Tidak Email';
        }
    }
    

}
?>