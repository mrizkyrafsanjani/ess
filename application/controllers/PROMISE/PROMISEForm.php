<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class PROMISEForm extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('number');
			$this->load->model('menu','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('promise_model','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$npkLogin = $session_data['npk'];
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}else{
					//echo 'gagal';
				}
				$getDepartemen = $this->promise_model->getDept();
                if($getDepartemen){
                    $getDepartemen_array = array();
                    foreach($getDepartemen as $row){
                        $getDepartemen_array[] = array(
                            'KodeDepartemen' => $row->KodeDepartemen,
                            'NamaDepartemen' => $row->NamaDepartemen,
                        );
                    }
                }else{
                    echo 'gagal';
                }
				$query = $this->db->get_where('globalparam',array('Name'=>'HeadHRGA'));
				foreach($query->result() as $row)
				{
					$headHRGA = $row->Value;
				}
				$query = $this->db->get_where('globalparam',array('Name'=>'HeadMRC'));
				foreach($query->result() as $row)
				{
					$headMRC = $row->Value;
				}
				$query = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
				foreach($query->result() as $row)
				{
					$headIT = $row->Value;
				}
				$query = $this->db->get_where('globalparam',array('Name'=>'HeadFinInv'));
				foreach($query->result() as $row)
				{
					$headFIINV = $row->Value;
				}
				$query = $this->db->get_where('globalparam',array('Name'=>'HeadACT'));
				foreach($query->result() as $row)
				{
					$headACC = $row->Value;
				}
				$query = $this->db->get_where('globalparam',array('Name'=>'HeadCA'));
				foreach($query->result() as $row)
				{
					$headCA = $row->Value;
				}
				$PresDirDPA1 =$this->user->getSingleUserBasedJabatan("President Director DPA 1")->NPK;
				$PresDirDPA2 =$this->user->getSingleUserBasedJabatan("President Director DPA 2")->NPK;
				$DirDPA1 =$this->user->getSingleUserBasedJabatan("Director DPA 1")->NPK;
				$DirDPA2 =$this->user->getSingleUserBasedJabatan("Director DPA 2")->NPK;
				$CorpSec =$this->user->getSingleUserBasedJabatan("Corporate Secretary Leader")->NPK;
				$JenisUser = '';
				if($npkLogin==$CorpSec || $npkLogin==$PresDirDPA1 || $npkLogin==$PresDirDPA2 || $npkLogin==$DirDPA1 || $npkLogin==$DirDPA2){
					$JenisUser = 'Admin';
				}else if($npkLogin==$headHRGA || $npkLogin==$headACC || $npkLogin==$headCA || $npkLogin==$headFIINV || $npkLogin==$headIT || $npkLogin==$headMRC){
					$JenisUser = 'Head';
				}else{
					$JenisUser = 'User';
				}
				//echo $JenisUser;
				$LastProjectID = $this->promise_model->getLastID();
				if($LastProjectID){
					$LastProjectID_array=array();
					foreach($LastProjectID as $row){
						$LastProjectID_array[] = array(
						  'ProjectID' => $row->ProjectID
						);					   
					 }
					}else{
						//echo 'gagal';
					}
				error_reporting(0);
				

							$data2 = array(
								'title' => 'PROMISE Detail',
								'npk' => $npkLogin,
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'LastProjectID_array' => $LastProjectID_array,
								'getDepartemen_array' => $getDepartemen_array
							);
								
				$this->load->helper(array('form','url'));
				$this->template->load('default','PROMISE/PROMISEForm_view',$data2);
				//$this->template->load('default','cetakFormUangMuka_view',$data2);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		
	}

?>