<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class PROMISEDetail extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('number');
			$this->load->model('menu','',TRUE);
			$this->load->model('usertask','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('promise_model','',TRUE);
		}
		
		function index($JenisUser,$Order, $ProjectID){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}else{
					echo 'gagal';
				}
				
							
				error_reporting(0);
				$User = $JenisUser;
				$IDProject= $ProjectID;
				$PageOrder = $Order;
				//$NamaProject = $NmProject;
				$Parameter= $this->promise_model->getPara($IDProject);
				if($Parameter){
					$Parameter_array = array();
					foreach($Parameter as $row){
						$Parameter_array = array(
							'Parameter' => $row->Parameter,
							'DeadlinePara' => $row->DeadlinePara
						);
						}	
					}else{
						$Parameter_array = array();
						foreach($Parameter as $row){
							$Parameter_array = array(
								'Parameter' => '0',
								'DeadlinePara' => '0'
							);
						}
						}		

				$ProjectDetail = $this->promise_model->getDetailProject($IDProject);
				if($ProjectDetail){
					$ProjectDetail_array = array();
					foreach($ProjectDetail as $row){
						$ProjectDetail_array = array(
							'ProjectID' => $row->ProjectID,
							'NamaProject' => $row->NamaProject,
							'KPI' => $row->KPI,
							'StartDate' => $row->StartDate,
							'Target' => $row->Target,
							'DeadlineUREQ' => $row->DeadlineUREQ,
							 'Deadline' => $row->Deadline,
							 'PIC'=> $row->PIC,
							 'Budget'=> $row->Budget,
							 'Departemen'=> $row->Departemen,
							 'Keterangan'=> $row->Keterangan,
							 'DetilInfo'=> $row->DetilInfo,
							 'KetAdmin'=> $row->KetAdmin,
							 'Status'=> $row->Status,
							 'EndDate'=> $row->EndDate
						);
					}
				}else{
					echo 'gagalheader';
				}		
				$ProjectProgress = $this->promise_model->getProgressDetail($IDProject);
				if($ProjectProgress){
					$ProjectProgress_array = array();
					foreach($ProjectProgress as $row){
						$ProjectProgress_array = array(
							'Presentase' => $row->Presentase,
						);
					}
				}			

							$data2 = array(
								'title' => 'PROMISE Detail',
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'ProjectDetail_array' => $ProjectDetail_array,
								'ProjectProgress_array' => $ProjectProgress_array,
								'PageOrder' => $PageOrder,
								'Parameter_array' => $Parameter_array,
								'User' => $User
							);
							

				

				$this->load->helper(array('form','url'));
				$this->template->load('default','PROMISE/PROMISEDetail_view',$data2);
				//$this->template->load('default','cetakFormUangMuka_view',$data2);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		public function getNamaProject($JenisUser,$Order,$ProjectID)
		{
			
			$session_data = $this->session->userdata('logged_in');
			if($this->session->userdata('logged_in')){
					$this->npkLogin = $session_data['npk'];
							 
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadHRGA'));
					foreach($query->result() as $row)
					{
						$headHRGA = $row->Value;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadMRC'));
					foreach($query->result() as $row)
					{
						$headMRC = $row->Value;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
					foreach($query->result() as $row)
					{
						$headIT = $row->Value;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadFinInv'));
					foreach($query->result() as $row)
					{
						$headFIINV = $row->Value;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadACT'));
					foreach($query->result() as $row)
					{
						$headACC = $row->Value;
					}
					$query = $this->db->get_where('globalparam',array('Name'=>'HeadCA'));
					foreach($query->result() as $row)
					{
						$headCA = $row->Value;
					}
					$PresDirDPA1 =$this->user->getSingleUserBasedJabatan("President Director DPA 1")->NPK;
					$PresDirDPA2 =$this->user->getSingleUserBasedJabatan("President Director DPA 2")->NPK;
					$DirDPA1 =$this->user->getSingleUserBasedJabatan("Director DPA 1")->NPK;
					$DirDPA2 =$this->user->getSingleUserBasedJabatan("Director DPA 2")->NPK;
					$CorpSec =$this->user->getSingleUserBasedJabatan("Corporate Secretary Leader")->NPK;
					$JenisUser = '';
					if($this->npkLogin==$CorpSec || $this->npkLogin==$PresDirDPA1 || $this->npkLogin==$PresDirDPA2 || $this->npkLogin==$DirDPA1 || $this->npkLogin==$DirDPA2){
						$JenisUser = 'Admin';
					}else if($this->npkLogin==$headHRGA || $this->npkLogin==$headACC || $this->npkLogin==$headCA || $this->npkLogin==$headFIINV || $this->npkLogin==$headIT || $this->npkLogin==$headMRC){
						$JenisUser = 'Head';
					}else{
						$JenisUser = 'User';
					}
					if($ProjectID=="submitUbahPROMISE"){
						if($this->input->post('submitUbahPROMISE')){
							$ProjectID = $this->input->post('txtProjectID');
										
								if($JenisUser=="Admin"){
									$this->promise_model->UbahDetail($ProjectID);
									$this->promise_model->UbahParameter($ProjectID);
									redirect('PROMISE/PROMISEController/ViewListPROMISEAdmin','refresh');
								}else{
									$post_array['ProjectID'] = $this->input->post('txtProjectID');
									$post_array['NamaProject'] = $this->input->post('txtNmProject');
									$post_array['KPI'] = $this->input->post('txtKPI');
									$post_array['Target']  = $this->input->post('txtTarget');
									$post_array['StartDate']  = $this->input->post('txtStartDate');
									$post_array['DeadlineUREQ']  = $this->input->post('txtDeadlineUREQ');
									$post_array['Deadline']  = $this->input->post('txtDeadline');
									$post_array['PIC']  = $this->input->post('txtPIC');
									$post_array['Budget']  = $this->input->post('txtBudget');
									$post_array['Departemen']  = $this->input->post('txtDepartemen');
									$post_array['Keterangan']  = $this->input->post('txtKeterangan');
									$post_array['ReminderDate']  = $this->input->post('txtStartDate');
									$post_array['CreatedOn']  = date('Y-m-d');
									$post_array['CreatedBy']  = $this->npkLogin;
									$lastNo = $this->promise_model->getLastKodePR();
									$NoUrut = substr($lastNo,2,10);
									$NoUrut = (int)$NoUrut;
									$NoUrut = $NoUrut + 1;
									$KodeRequest = "EP$NoUrut";
									$KodeRequestID = $NoUrut;
									if($lastNo=='' || $lastNo==null || $lastNo=='0'){
										$Notransaksi = generateNo('EP');
										$NoUruta = substr($Notransaksi,2,10);
										$NoUruta = (int)$NoUruta;
										$NoUruta = $NoUruta + 1;
										$KodeRequest = "EP$NoUruta";
										$KodeRequestID = $NoUruta;
									}
									$post_array2['KodeRequestDetail'] = $KodeRequest;
									$this->promise_model->ApprovalUbahParameter($post_array['ProjectID']);
									$array = array(
										'Inserts' => $post_array['ProjectID'],
										'StatusRequest' => '0');
									$array2 = array(
										'Updates' => $post_array['ProjectID'],
										'StatusRequest' => '0');
									$array3 = array(
										'Deletes' => $post_array['ProjectID'],
										'StatusRequest' => '0');
									$this->db->update('pmtprmtrequest',$post_array2,$array);
									$this->db->update('pmtprmtrequest',$post_array2,$array2);
									$this->db->update('pmtprmtrequest',$post_array2,$array3);
									$post_array['KodeRequestID'] = $KodeRequestID;
									$post_array['KodeRequestDetail'] = $KodeRequest; 
									$this->db->insert('pmtdetailrequest',$post_array);
									$KodeUserTask = generateNo('UT');
									$this->usertask->tambahUserTask($this->npkLogin,$post_array['KodeRequestDetail'],$KodeUserTask,"EP");
									if($JenisUser=='Head'){
										redirect('PROMISE/PROMISEController/ViewListPROMISEHead','refresh');				
									}else if($JenisUser=='User'){
										redirect('PROMISE/PROMISEController/ViewListPROMISEUser','refresh');				
									}
								}
								}
					}else if($ProjectID=="submitTambahMOM"){
							$ProjectID = $this->input->post('txtProjectID');				
								if($JenisUser=="Admin"){
									$this->promise_model->TambahMOM($ProjectID);
									redirect('PROMISE/PROMISEController/ViewListPROMISEAdmin','refresh');
								}else{
									$this->promise_model->TambahMOM($ProjectID);
									$post_array['ProjectID'] = $this->input->post('txtProjectID');
									$this->promise_model->ApprovalUbahMOM($ProjectID);
									$lastNo = $this->promise_model->getLastKodePM();
									$NoUrut = substr($lastNo,2,10);
									$NoUrut = (int)$NoUrut;
									$NoUrut = $NoUrut + 1;
									$KodeRequest = "PM$NoUrut";
									$KodeRequestID = $NoUrut;
									if($lastNo=='' || $lastNo==null || $lastNo=='0'){
										$Notransaksi = generateNo('PM');
										$NoUruta = substr($Notransaksi,2,10);
										$NoUruta = (int)$NoUruta;
										$NoUruta = $NoUruta + 1;
										$KodeRequest = "PM$NoUruta";
										$KodeRequestID = $NoUruta;
									}
									$post_array2['KodeRequestMOM'] = $KodeRequest;
									$this->promise_model->ApprovalUbahParameter($post_array['ProjectID']);
									$array = array(
										'Inserts' => $post_array['ProjectID'],
										'StatusRequest' => '0');
									$array2 = array(
										'Updates' => $post_array['ProjectID'],
										'StatusRequest' => '0');
									$array3 = array(
										'Deletes' => $post_array['ProjectID'],
										'StatusRequest' => '0');
									$this->db->update('pmtmomrequest',$post_array2,$array);
									$this->db->update('pmtmomrequest',$post_array2,$array2);
									$this->db->update('pmtmomrequest',$post_array2,$array3);
									$KodeUserTask = generateNo('UT');
									$this->usertask->tambahUserTask($this->npkLogin,$post_array2['KodeRequestMOM'],$KodeUserTask,"PM");
									if($JenisUser=='Head'){
										redirect('PROMISE/PROMISEController/ViewListPROMISEHead','refresh');				
									}else if($JenisUser=='User'){
										redirect('PROMISE/PROMISEController/ViewListPROMISEUser','refresh');				
									}
								}
						
		}else{
						$this->index($JenisUser,$Order,$ProjectID);

				
			}
			
			}else{
				redirect('login','refresh');
			}
		}
	}

?>