<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class LaporanSPD extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('menu','',TRUE);
			$this->load->model('trkSPD','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('uangmuka_model','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');			
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}				
				if($this->input->get('cmbNoSPD')==""){
					$noSPD = $this->trkSPD->getNoSPDBelumLapor($session_data['npk']);
				}else{
					$noSPD = $this->trkSPD->getOneNoSPD($this->input->get('cmbNoSPD'));
				}
				
				if($noSPD){
					$noSPD_array = array();
					foreach($noSPD as $row){
						$noSPD_array[] = array(
							'NoSPD' => $row->NoSPD
						);
					}			
				
					$this->load->helper(array('form','url'));
					if(count($noSPD_array)>1){			
						$data = array(
								'title' => 'Input Laporan SPD',
								'menu_array' => $menu_array,
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'noSPD'=> $noSPD_array
							);
						$this->template->load('default','pilihLapSPD_view.php',$data);
					}else if(count($noSPD_array)==1){					
						$this->loadSPD($noSPD_array);
					}
				}else{
					$data = array(
						'title' => 'Tidak Ada SPD yang Belum Dilaporkan',
						'menu_array' => $menu_array,
						'npk' => $session_data['npk'],
						'nama' => $session_data['nama'],
						'body' => 'Tidak Ada SPD Yang Harus Dilaporkan'
					);
					$this->template->load('default',null,$data);
				}
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		
		function loadSPD($noSPD){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$dataUser = $this->user->dataUser($session_data['npk']);
				$trkSPD = $this->trkSPD->getSPDbyNoSPDEdit($noSPD[0]);
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}
				
				if($trkSPD){
					$trkSPD_array = array();
					foreach($trkSPD as $row){
						$trkSPD_array[] = array(
							'DPA' => $row->DPA,
							'uangmuka' => $row->UangMuka,
							'nospd' => $row->NoSPD,
							'tanggalspd' => $row->TanggalSPD,
							'npk' => $row->NPK,
							 'golongan' => $row->golongan,
							 'nama'=> $row->nama,
							 'jabatan'=> $row->jabatan,
							 'departemen'=> $row->departemen,
							 'atasan'=> $row->atasan,
							 'tanggalberangkatspd'=> $row->TanggalBerangkatSPD,
							 'tanggalkembalispd'=> $row->TanggalKembaliSPD,
							 'tujuan'=> $row->Tujuan,
							 'alasan'=> $row->AlasanPerjalanan,
							 'deskripsi'=> $row->Deskripsi,
							 'keteranganspd'=> $row->KeteranganSPD,
							 'bebanharianspd'=> $row->BebanHarianSPD,
							 'jumlahharispd'=> $row->JumlahHariSPD,
							 'totalSPD' => $row->TotalSPD
							
						);
					}
				}else{
					echo 'gagal2';
				}
				/*
				$data = array(
					   'npk' => $session_data['npk'],
					   'nama' => $session_data['nama'],
					   'menu_array' => $menu_array
				  );
				  */
				if($dataUser){
					$dataUser_array = array();
					foreach($dataUser as $dataUser){
						$dataUserDetail = array(
							'title'=> 'Input Form SPD',
							'menu_array' => $menu_array,
							'npk' => $dataUser->npk,
							 'golongan' => $dataUser->golongan,
							 'nama'=> $dataUser->nama,
							 'jabatan'=> $dataUser->jabatan,
							 'departemen'=> $dataUser->departemen,
							 'atasan'=> $dataUser->atasan,
							 'uangsaku'=> $dataUser->uangsaku,
							 'uangmakan'=> $dataUser->uangmakan					 
						);
					}
				}else{
					echo 'gagal';
				}
				$TanggalTerima=1;
				$TglTerimaPermohonan=$this->trkSPD->TglTerimaPermohonan($noSPD[0]['NoSPD']);
				if($TglTerimaPermohonan){
					$TglTerimaPermohonan_array = array();
					foreach($TglTerimaPermohonan as $rows){
						$TglTerimaPermohonan_array = array(
							'TglTerimaHRGAPermohonan' =>$rows->TglTerimaHRGAPermohonan,
							'TglTerimaFinancePermohonan' =>$rows->TglTerimaFinancePermohonan,
							'TglTerimaFinancePermohonan' =>$rows->TglTerimaFinancePermohonan,
							'TglBayarFinancePermohonan' =>$rows->TglBayarFinancePermohonan
						);
					}
				}else{
					$TanggalTerima=0;
				}
				$UangMuka=1;
				$joinUangMuka=$this->trkSPD->joinUangMuka($noSPD[0]['NoSPD']);
				if($joinUangMuka){
					$joinUangMuka_array = array();
					foreach($joinUangMuka as $rows){
						$joinUangMuka_array = array(
							'Nouangmuka' => $rows->Nouangmuka,
							 'WaktuPenyelesaianTerlambat' => $rows->WaktuPenyelesaianTerlambat,
							 'noPP'=> $rows->NoPP,
							 'TanggalPermohonan'=> $rows->TanggalPermohonan,
							 'KeteranganPermohonan'=> $rows->KeteranganPermohonan,
							 'DPA' => $rows->DPA,
							 'JenisKegiatanUangMuka' => $rows->JenisKegiatanUangMuka
									 
						);
					}
				}else{
					$UangMuka=0;
				}
				$query2 = $this->db->get_where('globalparam',array('Name'=>'DIC_ACC'));
				foreach($query2->result() as $row)
				{
					$npkDICACC = $row->Value;
				}

				$query2 = $this->db->get_where('globalparam',array('Name'=>'DIC_HRGA'));
				foreach($query2->result() as $row)
				{
					$npkDICHRGA = $row->Value;
				}

				$query2 = $this->db->get_where('globalparam',array('Name'=>'Chief'));
				foreach($query2->result() as $row)
				{
					$npkChief = $row->Value;
				}
				
				foreach($this->user->findUserByNPK($npkDICHRGA) as $row)
				{
					$dicHrga = $row->Nama;
				}
				foreach($this->user->findUserByNPK($npkDICACC) as $row)
				{
					$dicAccounting = $row->Nama;
				}
				foreach($this->user->findUserByNPK($npkChief) as $row)
				{
					$Chief = $row->Nama;
				}

				
				$dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
				if($dataOutstanding){
					$dataOutstanding_array = array();
					foreach($dataOutstanding as $dataOutstanding){
						$dataOutstandingDetail[] = array(
							'title'=> 'Input Form Uang Muka Lainnya',
							//'menu_array' => $menu_array,
							'NoUangMuka' => $dataOutstanding->NoUangMuka,
							'KeteranganPermohonan' => $dataOutstanding->KeteranganPermohonan,
							'Total' => $dataOutstanding->Total,
							'TotalSPD' => $dataOutstanding->TotalSPD,
							'ReasonOutstanding'=> $dataOutstanding->ReasonOutStanding									 
						);
					}
				}				
				
				$headHrga = $this->user->getSingleUserBasedJabatan("HRGA Dept Head")->Nama;
				//$dicHrga = $this->user->findUserByNPK($npkDICHRGA)->Nama;
				//$dicAccounting = $this->user->getSingleUserBasedJabatan("President Director DPA 2")->Nama;
				if ($UangMuka<>0 || $TanggalTerima<>0){
				if($dataOutstanding){
				$data2 = array(
						'title' => 'Input Laporan SPD',
						'menu_array' => $menu_array,
						'npk' => $session_data['npk'],
					    'nama' => $session_data['nama'],
						'noSPD' => $noSPD,
						'trkSPD_array' => $trkSPD_array,
						'dataUser' => $dataUserDetail,
						'joinUangMuka_array' => $joinUangMuka_array,
						'TglTerimaPermohonan_array' => $TglTerimaPermohonan_array,
						'dataOutstanding'=> $dataOutstandingDetail,
						'flagOutStanding' => '1',
						'UangMuka' => $UangMuka,
						'TanggalTerima' => $TanggalTerima
					);
				}else{
				$data2 = array(
						'title' => 'Input Laporan SPD',
						'menu_array' => $menu_array,
						'npk' => $session_data['npk'],
					    'nama' => $session_data['nama'],
						'noSPD' => $noSPD,
						'trkSPD_array' => $trkSPD_array,
						'dataUser' => $dataUserDetail,
						'joinUangMuka_array' => $joinUangMuka_array,
						'TglTerimaPermohonan_array' => $TglTerimaPermohonan_array,
						'dataOutstanding'=> '0',
						'flagOutStanding' => '0',
						'UangMuka' => $UangMuka,
						'TanggalTerima' => $TanggalTerima		
					);
				}
			}else{
				$data2 = array(
					'title' => 'Input Laporan SPD',
					'menu_array' => $menu_array,
					'npk' => $session_data['npk'],
					'nama' => $session_data['nama'],
					'noSPD' => $noSPD,
					'trkSPD_array' => $trkSPD_array,
					'dataUser' => $dataUserDetail,
					'UangMuka' => $UangMuka,
					'TanggalTerima' => $TanggalTerima		
				);
			}
				$data2['HeadHRGA'] = $headHrga;
				$data2['DICHRGA'] = $dicHrga;
				$data2['DICACC'] = $dicAccounting;
				$data2['CHIEF'] = $Chief;
				$this->load->helper(array('form','url'));
				//$this->load->view('home_view', $data);
				$this->template->load('default','laporanSPD_view',$data2);
				
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
	}
?>