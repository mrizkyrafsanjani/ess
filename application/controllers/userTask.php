<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class UserTask extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->_userTask();
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _userTask()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Task Saya');
		$crud->set_theme('datatables');
		
        $crud->set_table('usertasks');
		$crud->where('usertasks.deleted','0');
		$crud->where('usertasks.StatusApproval','PE');
		$crud->where('usertasks.Username',$this->npkLogin);
		$crud->order_by('CreatedOn','asc');
		//$crud->set_relation('KodeUsxerTask','dtltrkrwy','NoTransaksi',array('deleted' => '0'));
		//$crud->set_relation_n_n('NoTransaksi','usertasks','dtltrkrwy','KodeUserTask','KodeUserTask','NoTransaksi');
		
		$crud->columns('KodeUserTask','Requester','CreatedOn', 'Keterangan');
		$crud->unset_operations();
		
		$crud->add_action('Lihat','','','',array($this,'_paramurl'));
		
        $output = $crud->render();
   
        $this-> _outputview($output);
    }
	
	function _paramurl($primary_key, $row)
	{
		return $row->Params."/".$row->KodeUserTask;
	}
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		
		$data = array(
				'title' => 'Task Saya',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
    }
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */