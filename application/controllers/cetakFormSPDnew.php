<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class CetakFormSPDnew extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('number');
			$this->load->model('menu','',TRUE);
			$this->load->model('trkSPD','',TRUE);
			$this->load->model('pelimpahanwewenang_model','',TRUE);
			$this->load->model('uangmuka_model','',TRUE);
			$this->load->model('user','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}else{
					echo 'gagal';
				}
				//$trkSPD = $this->trkSPD->getSPDbyNPK($session_data['npk']);
				$get_noSPD = $this->input->get('NoSPD');
				$trkSPD = $this->trkSPD->getSPDbyNoSPD($get_noSPD);
				if($trkSPD){
					$trkSPD_array = array();
					foreach($trkSPD as $row){
						$trkSPD_array[] = array(
							'DPA' => $row->DPA,
							'uangmuka' => $row->UangMuka,
							'nospd' => $row->NoSPD,
							'tanggalspd' => $row->TanggalSPD,
							'npk' => $row->NPK,
							 'golongan' => $row->golongan,
							 'nama'=> $row->nama,
							 'jabatan'=> $row->jabatan,
							 'departemen'=> $row->departemen,
							 'atasan'=> $row->atasan,
							 'tanggalberangkatspd'=> $row->TanggalBerangkatSPD,
							 'tanggalkembalispd'=> $row->TanggalKembaliSPD,
							 'tujuan'=> $row->Tujuan,
							 'alasan'=> $row->AlasanPerjalanan,
							 'deskripsi'=> $row->Deskripsi,
							 'keteranganspd'=> $row->KeteranganSPD,
							 'bebanharianspd'=> $row->BebanHarianSPD,
							 'jumlahharispd'=> $row->JumlahHariSPD,
							 'totalspd'=> $row->TotalSPD
						);
					}
				}else{
					echo 'gagal2';
				}
				/*
				$data = array(
					   'npk' => $session_data['npk'],
					   'nama' => $session_data['nama'],
					   'menu_array' => $menu_array
				  );
				  */
				/*data pelimpahan wewenang jika ada*/
						//data yang di limpahkan
				$hasilPW = $this->pelimpahanwewenang_model->getHeaderPelimpahanWewenang($get_noSPD);
				$KodePelimpahanWewenang = $hasilPW['KodePelimpahanWewenang'];
				$JenisTransaksi = $hasilPW['JenisTransaksi'];

				$dataPelimpahanWewenang = $this->pelimpahanwewenang_model->getDetailPelimpahanByNoTransaksi($KodePelimpahanWewenang);						
				if($dataPelimpahanWewenang)
				{
					foreach($dataPelimpahanWewenang as $row)
					{	
						$dataPelimpahanWewenangDetail[] = array(
							'KodePelimpahanWewenang' => $row->KodePelimpahanWewenang,
							'Keterangan' => $row->Keterangan,
							'NPKYangDilimpahkan' => $row->NPKYangDilimpahkan,
							'NamaYangDilimpahkan' => $row->NamaYangDilimpahkan
						);		
					}
				}

						//email untuk di forward
				$dataEmailPelimpahanWewenang = $this->pelimpahanwewenang_model->getEmailPelimpahanByNoTransaksi($KodePelimpahanWewenang);						
				if($dataEmailPelimpahanWewenang)
				{
					foreach($dataEmailPelimpahanWewenang as $row)
					{	
						$dataEmailPelimpahanWewenangDetail[] = array(
						'Nama' => $row->Nama,
						'EmailYangDiTuju' => $row->EmailYangDiTuju
						);		
					}
				}

				//get data uang muka
				$get_NoUangMuka = $this->uangmuka_model->getNoUangMukaByNoSPD($get_noSPD);
				$trxuangmuka = $this->uangmuka_model->getHeaderTrxUangMukabyNoUM($get_NoUangMuka);
				if($trxuangmuka){
					$trxuangmuka_array = array();
					foreach($trxuangmuka as $row){
						$trxuangmuka_array[] = array(
							'DPA' => $row->DPA,
							'NoUangMuka' => $row->NoUangMuka,
							'TanggalUangMuka' => $row->TanggalUangMuka,
							'TanggalMulai' => $row->TanggalMulai,
							'TanggalSelesai' => $row->TanggalSelesai,
							 'NPKPemohon' => $row->NPKPemohon,
							 'namaCreatedBy'=> $row->nama,
							 'namaNPKPemohon'=> $row->atasan,
							 'TipeBayarUangMuka'=> $row->TipeBayarUangMuka,
							 'BankBayarUangMuka'=> $row->BankBayarUangMuka,
							 'NoRekBayarUangMuka'=> $row->NoRekBayarUangMuka,
							 'NmPenerimaBayarUangMuka'=> $row->NmPenerimaBayarUangMuka,
							 'WaktuBayarTercepat'=> $row->WaktuBayarTercepat,
							 'WaktuPenyelesaianTerlambat'=> $row->WaktuPenyelesaianTerlambat,
							 'NoPP'=> $row->NoPP,
							 'Keterangan'=> $row->KeteranganPermohonan
						);
					}
				}else{
					echo 'gagalheader';
				}

				$dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
				$dataOutstandingDetail = array();
				$flagoutstanding = "0";
				if ($dataOutstanding)
				{
					$flagoutstanding = "1";
					foreach($dataOutstanding as $row)
					{	
						$dataOutstandingDetail[] = array(
							'NoUangMuka' => $row->NoUangMuka,
							'KeteranganPermohonan' => $row->KeteranganPermohonan,
							'Total' => $row->Total,
							'TotalSPD' => $row->TotalSPD,
							'ReasonOutStanding' => $row->ReasonOutStanding
						);		
					}
				}

						
				if($dataPelimpahanWewenang && $dataEmailPelimpahanWewenang) //// SPD + pelimpahan wewenang + email
				{
							$data2 = array(
								'title' => 'Cetak Form SPD',
								'trkSPD_array' => $trkSPD_array,
								'trxuangmuka_array' => $trxuangmuka_array,
								'terbilang' => terbilang($trkSPD_array[0]['totalspd']),
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'dataPelimpahanWewenangDetail'=>$dataPelimpahanWewenangDetail,
								'dataEmailPelimpahanWewenangDetail'=>$dataEmailPelimpahanWewenangDetail,
								'KodePelimpahanWewenang'=>$KodePelimpahanWewenang,
								'flagemail' => '2',
								'dataOutstanding' => $dataOutstandingDetail
							);
				}
				else	
				if ($dataPelimpahanWewenang) //// SPD + pelimpahan wewenang saja
				{
							$data2 = array(
								'title' => 'Cetak Form SPD',
								'trkSPD_array' => $trkSPD_array,
								'trxuangmuka_array' => $trxuangmuka_array,
								'flagoutstanding' => $flagoutstanding,
								'terbilang' => terbilang($trkSPD_array[0]['totalspd']),
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'dataPelimpahanWewenangDetail'=>$dataPelimpahanWewenangDetail,
								'KodePelimpahanWewenang'=>$KodePelimpahanWewenang,
								'flagemail' => '1',
								'dataOutstanding' => $dataOutstandingDetail
							);
				}
				else
				{ // SPD biasa							

							$data2 = array(
								'title' => 'Cetak Form SPD',
								'trkSPD_array' => $trkSPD_array,
								'trxuangmuka_array' => $trxuangmuka_array,
								'flagoutstanding' => $flagoutstanding,
								'dataOutstanding' => $dataOutstandingDetail,
								'terbilang' => terbilang($trkSPD_array[0]['totalspd']),
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'flagemail' => '0'
							);
				}
				$query2 = $this->db->get_where('globalparam',array('Name'=>'DIC_ACC'));
				foreach($query2->result() as $row)
				{
					$npkDICACC = $row->Value;
				}

				$query2 = $this->db->get_where('globalparam',array('Name'=>'DIC_HRGA'));
				foreach($query2->result() as $row)
				{
					$npkDICHRGA = $row->Value;
				}
				
				foreach($this->user->findUserByNPK($npkDICHRGA) as $row)
				{
					$dicHrga = $row->Nama;
				}
				foreach($this->user->findUserByNPK($npkDICACC) as $row)
				{
					$dicAccounting = $row->Nama;
				}
				$headHrga = $this->user->getSingleUserBasedJabatan("HRGA Dept Head")->Nama;
				//$dicHrga = $this->user->getSingleUserBasedJabatan("Director DPA 1")->Nama;
				//$dicAccounting = $this->user->getSingleUserBasedJabatan("President Director DPA 2")->Nama;

				$data2['HeadHRGA'] = $headHrga;
				$data2['DICHRGA'] = $dicHrga;
				$data2['DICACC'] = $dicAccounting;
				

				$this->load->helper(array('form','url'));
				//$this->load->view('home_view', $data);
				$this->template->load('default','cetakFormSPDnew_view',$data2);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
	}
?>