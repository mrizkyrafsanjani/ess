<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ListTagihan extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);		
		$this->load->model('departemen','',TRUE);
		$this->load->model('LokasiKaryawan','',TRUE);
		$this->load->model('register','',TRUE);	
		$this->load->library('grocery_crud');
		$this->load->library('gc_dependent_select');
		//$this->load->helper('authorized_helper');
    }
 
    public function index()
    {
		try{
			$session_data = $this->session->userdata('logged_in');
			if($session_data){			
				if(check_authorizedByName("List Tagihan"))
				{
					$this->_listtagihan();
				}
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );		 
		}
    }
	
	public function _listtagihan()
    {
		
		$crud = new grocery_crud();
		$crud->set_subject('List Tagihan');
		$crud->set_theme('datatables');		
		
        $crud->set_table('tagihanvendor');
		$crud->where('tagihanvendor.deleted','0');
		$crud->set_relation('NPKuser','mstruser','Nama',array('deleted' => '0','TanggalBerhenti' => '0000-00-00'));
		$crud->set_relation('KodeDepartemen','departemen','NamaDepartemen',array('deleted' => '0'));
				
		$crud->field_type('TglTerimaDPA','date');
		$crud->field_type('TglTerimaUser','date');
		$crud->field_type('TglInvoice','date');
		$crud->field_type('DueDate','date');
		$crud->field_type('TglTerimaHRGA','date');
		$crud->field_type('TglTerimaFinance','date');
		$crud->field_type('TglBayar','date');
		$crud->field_type('NilaiInvoice','integer');
		$currUserDepartemen = $this->getCurrentUserKodeDepartemen();
		if($currUserDepartemen != "2" && $currUserDepartemen != "4")
		{
			$crud->where('tagihanvendor.KodeDepartemen',$currUserDepartemen);
			$crud->unset_delete();
			$crud->unset_edit();
		}
		
		/* $query = $this->db->get_where('globalparam',array('Name'=>'AdminGA'));
		foreach($query->result() as $row)
		{
			$NPK_GA = $row->Value;
		} */
		$crud->columns('NoRegister','TglTerimaDPA','NPKuser','KodeDepartemen','DPA','TglTerimaUser','NamaVendor','NoInvoice','TglInvoice','NilaiInvoice','DueDate','TglTerimaHRGA','TglTerimaFinance','TglBayar','NoVoucher');
		$session_data = $this->session->userdata('logged_in');
		if ($session_data['npk']=="RECEPT")
		{
			$crud->columns('NoRegister','TglTerimaDPA','NPKuser','KodeDepartemen','TglTerimaUser');
			$crud->fields('NoRegister','TglTerimaDPA','NPKuser','KodeDepartemen','TglTerimaUser');
			$crud->required_fields('TglTerimaDPA','NPKuser','KodeDepartemen','TglTerimaUser');
			$crud->unset_delete();
			$crud->unset_edit();
			$crud->unset_read();			
		}
		else if($currUserDepartemen == "2") //hrga
		{
			$crud->fields('NoRegister','TglTerimaDPA','NPKuser','KodeDepartemen','TglTerimaUser','DPA','NamaVendor','NoInvoice','TglInvoice','NilaiInvoice','DueDate','TglTerimaHRGA','TglTerimaFinance');
			$crud->required_fields('TglTerimaDPA','NPKuser','KodeDepartemen','TglTerimaUser','DPA','NamaVendor','NoInvoice','TglInvoice','NilaiInvoice','TglTerimaHRGA');
		}
		else if($currUserDepartemen == '4') //accounting
		{
			$crud->where('tagihanvendor.TglTerimaFinance !=','0000-00-00 00:00:00');
			$crud->unset_add();
			$crud->unset_delete();
			$crud->unset_read();
			$crud->fields('NoRegister','NamaVendor','NoInvoice','TglInvoice','NilaiInvoice','DueDate','TglBayar','NoVoucher');
			$crud->field_type('NamaVendor','readonly');
			$crud->field_type('NoInvoice','readonly');
			$crud->field_type('TglInvoice','readonly');
			$crud->field_type('NilaiInvoice','readonly');
			$crud->field_type('DueDate','readonly');
		}
		else
		{
			$crud->unset_add();
			
			$crud->fields('NoRegister','TglTerimaDPA','NPKuser','KodeDepartemen','DPA','NamaVendor','NoInvoice','TglInvoice','NilaiInvoice','DueDate','TglTerimaHRGA','TglTerimaFinance','TglBayar','NoVoucher');
			$crud->unset_read();
		}
		
		
		
		
		$crud->display_as('NoRegister','No. Reg');
        $crud->display_as('TglTerimaDPA','Tanggal Terima DPA');
		$crud->display_as('NamaVendor','Nama Vendor');
		$crud->display_as('NoInvoice','No Invoice');
		$crud->display_as('TglInvoice','Tanggal Invoice');
		$crud->display_as('NilaiInvoice','Nilai Invoice');
		$crud->display_as('NPKuser','Nama User');
		$crud->display_as('TglTerimaUser','Tgl Terima User');
		$crud->display_as('TglTerimaHRGA','Tgl Terima HRGA');
		$crud->display_as('TglTerimaFinance','Tgl Terima Finance');
		$crud->display_as('TglBayar','Tgl Bayar');
		$crud->display_as('NoVoucher','No Voucher');
		$crud->display_as('KodeDepartemen','Dept');
		
		
		//$crud->required_fields('KodeAsset');
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		$crud->callback_column('NilaiInvoice',array($this,'_callback_column_NilaiInvoice'));
		$crud->callback_field('NoRegister',array($this,'field_callback_NoRegister'));	
		$crud->callback_field('DPA',array($this,'add_field_callback_DPA'));
		$crud->callback_field('KodeDepartemen',array($this,'add_field_callback_KodeDepartemen'));
		$crud->callback_delete(array($this,'_delete_tagihan'));
		$crud->callback_update(array($this,'_update_tagihan'));
		$crud->callback_insert(array($this,'_insert_tagihan'));

			
		
		$js = " 		
		<script>
		$(document).ready(function() {
		var NPKuser = $('#field-NPKuser');
		var KodeDepartemen = $('#field-KodeDepartemen');
		NPKuser.change(function(){
			$.ajax({
			  url: '" .base_url()."index.php/TagihanVendor/ListTagihan/getDepartemenLokasi',
			  type: 'POST',
			  data: {npkuser : NPKuser.val()},
			  dataType: 'html'
			}).done(function(response){
				 KodeDepartemen.val(response);				 
			});
			//KodeDepartemen.val(KodeLokasiAsset.val());
		});
		
	}); </script>";
		
        $output = $crud->render();   
		$output->output.= $js;
        $this-> _outputview($output);        
    }
	
	function _callback_column_NilaiInvoice($value, $row)
	{
		return number_format($value, 0, '.', ',');;
	}
	
	function field_callback_NoRegister($value= '', $primary_key = null)
	{
		return '<input type="text" name="NoRegister" value="'.$value.'" readonly style="border:0px;background-color:white;box-shadow : none;">';
	}
	
	function add_field_callback_DPA($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selected1 = '';
		$selected2 = '';
		if($value == "1"){
			$selected1 = 'selected';
		}else if($value == "2"){
			$selected2 = 'selected';
		}
		
		return ' <select name="DPA">
			<option value = ""></option>
			<option '.$selected1.' value="1">Satu</option>
			<option '.$selected2.' value="2">Dua</option>
		</select>';
	}

	
	function add_field_callback_KodeDepartemen($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$htmlText = '<select id="field-KodeDepartemen" name="KodeDepartemen">';
		$departemen = $this->departemen->getDepartemen('');
		$count = 0;
		if($departemen){
			foreach($departemen as $row){
				$htmlText .= '<option ';
				if($value == $count + 1)
					$htmlText .= ' selected ';
				$htmlText .= ' value="'.$row->KodeDepartemen.'">'.$row->NamaDepartemen.'</option>';
				$count++;
			}
		}
		$htmlText .= '</select>';
		return $htmlText;
	}
	
    function _outputview($output = null)
    {
		$data = array(
			'title' => 'Tagihan Vendor',
			'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _generateNoReg($post_array)
	{
		$kodeAsset = "INV/";
		
		/*		
		if($post_array['DPA'] == '1')
		{
			$kodeAsset .= 'DPA1/';
		}
		else if($post_array['DPA'] == '2')
		{
			$kodeAsset .= 'DPA2/';
		}
		*/
		
		$kodeAsset .= $this->getLastNumberReg($post_array['DPA']).'/';
		
		if(date('n')==1)
			$kodeAsset .= 'I';
		if(date('n')==2)
			$kodeAsset .= 'II';
		if(date('n')==3)
			$kodeAsset .= 'III';
		if(date('n')==4)
			$kodeAsset .= 'IV';
		if(date('n')==5)
			$kodeAsset .= 'V';
		if(date('n')==6)
			$kodeAsset .= 'VI';
		if(date('n')==7)
			$kodeAsset .= 'VII';
		if(date('n')==8)
			$kodeAsset .= 'VIII';
		if(date('n')==9)
			$kodeAsset .= 'IX';
		if(date('n')==10)
			$kodeAsset .= 'X';
		if(date('n')==11)
			$kodeAsset .= 'XI';
		if(date('n')==12)
			$kodeAsset .= 'XII';
		
		$kodeAsset .= '/'.date('Y');
		
		return $kodeAsset;
	}
	
	function _update_tagihan($post_array, $primary_key)
	{
		if($post_array['NoRegister'] == "")
		{
			$post_array['NoRegister'] = $this->_generateNoReg($post_array);
		}
		//$post_array['TanggalPembelian'] = '05/03/2014';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		
		$session_data = $this->session->userdata('logged_in');
		$post_array['UpdatedBy'] = $session_data['npk'];
		return $this->db->update('tagihanvendor',$post_array,array('KodeTagihanVendor' => $primary_key));
	}
	
	function _insert_tagihan($post_array)
	{
		//vvv.xxx.yyy.zz
		/* vvv = Kepemilikan Asset
          300 = IT
          400 = GA
		xxx = Tipe Asset
		yyy = Nomor Urut Asset
		zz   = DPA
          01   = Satu
          02   = Dua */
		
		//$post_array['KodeDepartemen'] = "1";
		//if($post_array['NoRegister'] == "" && $post_array['DPA'] != "")
		//{
		$post_array['NoRegister'] = $this->_generateNoReg($post_array);
		//}
		
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		
		$session_data = $this->session->userdata('logged_in');
		$post_array['CreatedBy'] = $session_data['npk'];
		
		return $this->db->insert('TagihanVendor',$post_array);
	}
	
	function getLastNumberReg($DPA)
	{
		$LastKodeNumber = $this->register->getLastNumberReg($DPA);
		if($LastKodeNumber){
			foreach($LastKodeNumber as $row){
				$lastKode = $row->NoRegister;
			}
		}else{
			$lastKode = '00000000000000';
		}
		//fire_print('log',"DPA:$DPA, LastKodeNumber:".print_r($LastKodeNumber,true).", lastKode: $lastKode");
		$lastNumber = substr($lastKode,4,3);		
		$lastNumber++;
		return str_pad($lastNumber,3,"0",STR_PAD_LEFT);
	}
	
	function _delete_tagihan($primary_key)
	{
		return $this->db->update('TagihanVendor',array('deleted' => '1'),array('KodeTagihanVendor' => $primary_key));
	}
	
	function getDepartemenLokasi()
	{
		$deptKaryawan = $this->LokasiKaryawan->getLokasiKaryawan($_POST['npkuser']);
		if($deptKaryawan){
			$deptKaryawan_array = array();
			foreach($deptKaryawan as $row){
			   $deptKaryawan_array[] = array(
				 'KodeDepartemen' => $row->KodeDepartemen,
				 'NamaDepartemen' => $row->NamaDepartemen
			   );
			}
		}
		echo $deptKaryawan_array[0]['KodeDepartemen'];
	}
	
	function getCurrentUserKodeDepartemen()
	{
		$session_data = $this->session->userdata('logged_in');
		$dataUser = $this->user->dataUser($session_data['npk']);
		if($dataUser){
			foreach($dataUser as $dataUser){
				$dataUserDetail = array(
					'kodedepartemen'=> $dataUser->KodeDepartemen
				);
			}
		}else{
			echo 'gagal get current user departemen';
		}
		return $dataUserDetail['kodedepartemen'];
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */