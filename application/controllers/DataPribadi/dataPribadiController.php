<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class DataPribadiController extends CI_Controller{
		var $npkLogin;
		function __construct(){
			parent::__construct();
			$this->load->model('user','',TRUE);
			$this->load->model('departemen','',TRUE);
			$this->load->model('mstrgolongan','',TRUE);
			$this->load->model('usertask','',TRUE);
			$this->load->model('cutiJenisTidakHadir_model','',TRUE);
			$this->load->model('cuti_model','',TRUE);
			//$this->load->model('benefit_model','',TRUE);
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
			$this->load->library('grocery_crud');
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				if(check_authorized("16")){
					$session_data = $this->session->userdata('logged_in');
					$dataUser = $this->user->dataUser($session_data['npk']);
					
					if($dataUser){
						foreach($dataUser as $dataUser){
							$dataUserDetail = array(
								'title'=> 'Data Pribadi',
								'npk' => $dataUser->npk,
								'golongan' => $dataUser->golongan,
								'nama'=> $dataUser->nama,
								'jabatan'=> $dataUser->jabatan,
								'departemen'=> $dataUser->departemen,
								'atasan'=> $dataUser->atasan,
								'uangsaku'=> $dataUser->uangsaku,
								'uangmakan'=> $dataUser->uangmakan,
								'dataDepartemen'=> $this->_getAllListDepartemen(),
								'dataGolongan' => $this->_getAllListGolongan()
							);
						}
					}
					
					$this->load->helper(array('form','url'));
					//$this->load->view('home_view', $data);				
					$this->template->load('default','DataPribadi/dataPribadi_insert',$dataUserDetail);
				}
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		
		function ajax_batalEdit(){
			try
			{
				echo "Pembatalan berhasil";								
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_batalSimpan(){
			try
			{
				if(!$this->user->batalTambahKaryawan())
				{
					echo('Pembatalan gagal');
				}
				else
				{						
					echo "Pembatalan berhasil";
				}				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_lookUpNPK(){
			try{
				$NPK = $this->input->post('NPK');
				$this->db->where('NPK', $NPK);
				$query = $this->db->get('mstruser');
				if ($query->num_rows() > 0)
				{
				   echo 0;
				}
				else 
				{
				   echo 1;
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_SaveLangsung(){
			try
			{
				$NPK = $this->input->post('NPK');
				$AlamatTinggal = $this->input->post('AlamatTinggal');
				$NoTelp = $this->input->post('NoTelp');
				$NoHP = $this->input->post('NoHP');
				$msg = "";
				$error = 0;
				if($NoTelp != '' & !is_numeric($NoTelp)){
					$msg .= "No Telepon harus dalam bentuk angka\n";
					$error = 1;
				}
				
				if($NoHP != '' & !is_numeric($NoHP)){
					$msg .= "No HP harus dalam bentuk angka\n";
					$error = 1;
				}
				
				if($error == 0)
				{
					$result = $this->user->updateLangsungUser($NPK,$AlamatTinggal,$NoTelp,$NoHP);
					if($result){
						$userdata = $this->user->dataUser($this->input->post('NPK'));

						if($userdata)
						{
							$dtuser_array = array();
							foreach($userdata as $row)
							{
								$dtuser_array = array(
								'npk' => $row->npk,
								'nama' => $row->nama,
								'email' => $row->email
								);
							}
						}
						$query = $this->db->get_where('globalparam',array('Name'=>'EmailHRGA'));
						foreach($query->result() as $row)
						{
							$email = $row->Value;
						}
					
						$this->load->library('email');
						$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
						$this->email->to($email); 

						$this->email->subject($dtuser_array['nama']. ' ('.$this->input->post('NPK').') perubahan no HP/Telp/Alamat Surat Menyurat');
						$message = $dtuser_array['nama'] . " melakukan perubahan data pada Alamat Surat Menyurat/HP/Telp.";
						$this->email->message($message);	

						if( ! $this->email->send())
						{	
							echo "Data berhasil disimpan, tapi proses pengiriman email terjadi kegagalan";
						}
						else
						{
							echo "Data berhasil di simpan";
						}
					}else{
						echo "Data gagal disimpan";
					}
				}else{
					echo $msg;
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function _getAllListDepartemen()
		{
			$dataDepartemen = $this->departemen->getDepartemen("");
			if($dataDepartemen){
				$departemen_array = array();
				foreach($dataDepartemen as $row){
				   $departemen_array[] = array(
					 'KodeDepartemen' => $row->KodeDepartemen,
					 'NamaDepartemen' => $row->NamaDepartemen
				   );
				}
			}			
			return $departemen_array;
		}
		
		function _getAllListGolongan()
		{
			$dataGolongan = $this->mstrgolongan->getGolongan("");
			if($dataGolongan){
				$golongan_array = array();
				foreach($dataGolongan as $row){
				   $golongan_array[] = array(
					 'Golongan' => $row->Golongan						 
				   );
				}
			}
			
			return $golongan_array;
		}
		
		function approvalDataPribadi($KodeUserTask)
		{
			try
			{
				if(!$this->session->userdata('logged_in'))
				{
					redirect("login?u=DataPribadi/dataPribadiController/approvalDataPribadi/$KodeUserTask",'refresh');				
				}
				else
				{
					if(check_authorized("17"))
					{
						$dataTempUser = $this->usertask->getTempPeserta($KodeUserTask);
						if($dataTempUser)
						{
							foreach($dataTempUser as $row)
							{
								$dataTempUserDetail = array(
									'kodetempmstruser' => $row->KodeTempMstrUser,
									'npk' => $row->TempNPK,
									'nama'=> $row->Nama,
									'alamattinggal'=> $row->AlamatTinggal,
									'notelp'=> $row->NoTelp,
									'nohp'=> $row->NoHP,
									'statuskawin'=> $row->StatusKawin,
									'namakontakkeluarga'=> $row->NamaKontakKeluarga,
									'notelpkontakkeluarga'=> $row->NoTelpKontakKeluarga,
									'hubungankeluarga'=> $row->HubunganKeluarga,
									'nohpkontakkeluarga'=> $row->NoHPKontakKeluarga
								);
								fire_print('log',$row->StatusKawin);
								$NPKSelectedUser = $row->TempNPK;
							}
						}
						
						$dataUser = $this->user->findUserByNPK($NPKSelectedUser);					
						
						if($dataUser)
						{
							foreach($dataUser as $dataUser){
								$dataUserDetail = array(
									'npk' => $dataUser->NPK,
									'nama'=> $dataUser->Nama,
									'alamattinggal'=> $dataUser->AlamatTinggal,
									'notelp'=> $dataUser->NoTelp,
									'nohp'=> $dataUser->NoHP,
									'statuskawin'=> $dataUser->StatusKawin,
									'namakontakkeluarga'=> $dataUser->NamaKontakKeluarga,
									'notelpkontakkeluarga'=> $dataUser->NoTelpKontakKeluarga,
									'hubungankeluarga'=> $dataUser->HubunganKeluarga,
									'nohpkontakkeluarga'=> $dataUser->NoHPKontakKeluarga
								);
								fire_print('log','data lama :' .$dataUser->StatusKawin);
							}
						}
						
						$data = array(
							'title' =>'Approval Data Pribadi '.$NPKSelectedUser,
							'datalama'=>$dataUserDetail, 
							'databaru'=> $dataTempUserDetail
						);
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','DataPribadi/approvalDataPribadi_view',$data);
					}
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
			
		}
		
		function prosesApprovalDataPribadiUser($KodeTempMstrUser,$StatusApp)
		{
			try
			{
				$query = $this->db->get_where('dtltrkrwy',array('NoTransaksi'=>$KodeTempMstrUser));
				foreach($query->result() as $row)
				{
					$KodeUserTask = $row->KodeUserTask;
				}
				
				$this->db->trans_start();
				
				//insert data dari tempdatapribadi ke datapribadi klo AP
				if($StatusApp == "AP")
				{
					$this->user->updateDataPribadiFromTemp($KodeTempMstrUser,$StatusApp);
				}
				
				//user task dibuat status AP/DE
				$this->usertask->updateStatusUserTask($KodeUserTask,$StatusApp,$this->npkLogin);
				
				//tempmstruser di buat status AP/DE
				$this->db->where('KodeTempMstrUser',$KodeTempMstrUser);
				$this->db->update('tempmstruser',array(
					'StatusTransaksi' => $StatusApp,
					'UpdatedOn' => date('Y-m-d H:i:s'),
					'UpdatedBy' => $this->npkLogin
				));
				
				//detil rwypendidikan dibuat status AP/DE 
				/* $this->db->where('KodeTempMstrUser',$KodeTempMstrUser);
				$this->db->update('tempriwayatpendidikan',array(
					'StatusTransaksi' => $StatusApp,
					'UpdatedOn' => date('Y-m-d H:i:s'),
					'UpdatedBy' => $this->npkLogin
				)); */
				
				//detil keluarga inti dibuat status AP/DE
				$this->db->where('KodeTempMstrUser',$KodeTempMstrUser);
				$this->db->update('tempkeluarga',array(
					'StatusTransaksi' => $StatusApp,
					'UpdatedOn' => date('Y-m-d H:i:s'),
					'UpdatedBy' => $this->npkLogin
				));
				
				$this->db->trans_complete();
				//kirim email status perubahan ke requester
				if($this->db->trans_status() === TRUE)
				{
					$query = $this->db->get_where('usertasks',array('KodeUserTask'=>$KodeUserTask));
					foreach($query->result() as $row)
					{
						$Requester = $row->Requester;
					}
					if($this->config->item('enableEmail') == 'true')
					{
						if($this->sendEmailToUser($StatusApp,$Requester))
						{
							echo "Data sudah berhasil disimpan.";
						}
						else
						{
							echo "Data berhasil disimpan tapi gagal mengirim email.";
						}
					}
					else
					{
						echo "Data sudah berhasil disimpan.";
					}
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function sendEmailTandaSubmit($to)
		{
			try
			{
				$this->load->library('email');
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				$this->email->to($to); 

				$this->email->subject('Submit Approval Perubahan Data Pribadi');
				$message = "Terdapat karyawan yang mengirim perubahan data. Mohon di review pada Task Saya ESS.";
				$this->email->message($message);	

				if( ! $this->email->send())
				{	
					return false;
				}
				else
				{
					return true;
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function sendEmailToUser($StatusApp,$Requester)
		{
			try
			{
				$this->load->library('email');
				$result = $this->user->dataUser($Requester);

				if($result)
				{
					$sess_array = array();
					foreach($result as $row)
					{
						$sess_array = array(
						'npk' => $row->npk,
						'nama' => $row->nama,
						'email' => $row->email
						);
					}

					$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
					$this->email->to($sess_array['email']); 

					$this->email->subject('Status Perubahan Data Pribadi');
					
					if($StatusApp == "AP")
					{
						$message = 'Perubahan Data Pribadi Anda sudah diapprove, silahkan cek pada ESS.';
					}
					else
					{
						$message = 'Perubahan Data Pribadi Anda telah didecline, silahkan konfirmasi pada HRD, dan cek data apa yang perlu diperbaiki.';
					}
					
					$this->email->message($message);	

					if( ! $this->email->send())
					{	
						return false;
					}
					else
					{
						return true;
					}
				}
			}			
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function viewDataPribadi()
		{
			try{
				if(!$this->session->userdata('logged_in'))
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
				else
				{
					if(check_authorized("18"))					
					{
						$crud = new grocery_crud();
						$crud->set_subject('Data Pribadi');
						$crud->set_theme('datatables');
						
						$crud->set_table('mstruser');
						$crud->where('mstruser.deleted','0');
						$crud->set_relation('Departemen','departemen','NamaDepartemen',array('deleted' => '0'));
						//$crud->where('riwayatrotasipekerjaandpa.NPK',$this->NPKSelectedUser);

						$crud->columns('Nama','NPK','Departemen', 'Golongan', 'StatusKaryawan','TanggalBerhenti','JumlahCutiTahunanSkrg','JumlahCutiBesarSkrg');
						$crud->fields('Nama','NPK','Departemen', 'Golongan', 'StatusKaryawan','TanggalBerhenti');
									
						$crud->required_fields('Nama','NPK','Departemen', 'Golongan', 'StatusKaryawan');
						
						$crud->display_as('TanggalBerhenti','Tanggal Berhenti');
						$crud->display_as('KodeDepartemen','Departemen')->display_as('Nama','Nama Karyawan')->display_as('StatusKaryawan','Status Karyawan');
						$crud->display_as('JumlahCutiTahunanSkrg','Jumlah Cuti Tahunan Sekarang');
						$crud->display_as('JumlahCutiBesarSkrg','Jumlah Cuti Besar Sekarang');	
						$crud->add_action('Ubah','','DataPribadi/DataPribadiController/EditDataPribadi','ui-icon-image');
						$crud->callback_column('JumlahCutiTahunanSkrg',array($this,'_callback_jumlahCutiTahunanSkrg'));
						$crud->callback_column('JumlahCutiBesarSkrg',array($this,'_callback_jumlahCutiBesarSkrg'));
						
						$crud->unset_read();
						$crud->unset_print();					
						//$crud->unset_export();
						$crud->unset_delete();
						$crud->unset_add();
						$crud->unset_edit();
						$output = $crud->render();
				   
						$data = array(
							'title' => 'View Data Pribadi',
							'body' => $output
						); 
						$this->load->helper(array('form','url'));
						$this->template->load('default','templates/CRUD_view',$data);
					}
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		public function _callback_jumlahCutiTahunanSkrg($value, $row)
		{		
			$jumlahCuti = $this->cuti_model->getJumlahCutiUser($row->NPK);
			return $jumlahCuti['JumlahCutiTahunan'];
		}
		
		public function _callback_jumlahCutiBesarSkrg($value, $row)
		{
			$jumlahCuti = $this->cuti_model->getJumlahCutiUser($row->NPK);
			return $jumlahCuti['JumlahCutiBesar'];
		}
		
		function editDataPribadi($NPKSelectedUser="")
		{
			try
			{
				if($NPKSelectedUser == ''){
					$NPKSelectedUser = $this->npkLogin;
				}
				if(!$this->session->userdata('logged_in'))
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');				
				}
				else
				{
					if(check_authorized("17"))
					{
						$dataUser = $this->user->findUserByNPK($NPKSelectedUser);					
						
						if($dataUser){
							$jumlahMedical = 0;//$this->benefit_model->getSisaJatahMedical($NPKSelectedUser);
							
							 $cutiUser = $this->cuti_model->getJumlahCutiUser($NPKSelectedUser);
							$jumlahCutiTahunan = $cutiUser['JumlahCutiTahunan'];
							$jumlahCutiBesar =$cutiUser['JumlahCutiBesar'];
							 
							foreach($dataUser as $dataUser){
								$dataUserDetail = array(
									'title'=> 'Data Pribadi',
									'npk' => $dataUser->NPK,
									'golongan' => $dataUser->Golongan,
									'nama'=> $dataUser->Nama,
									'jabatan'=> $dataUser->Jabatan,
									'departemen'=> $dataUser->Departemen,
									'emailinternal'=> $dataUser->EmailInternal,
									'jeniskelamin'=> $dataUser->JenisKelamin,
									'tempatlahir'=> $dataUser->TempatLahir,
									'tanggallahir'=> $dataUser->TanggalLahir,
									'agama'=> $dataUser->Agama,
									'alamatktp'=> $dataUser->AlamatKTP,
									'alamattinggal'=> $dataUser->AlamatTinggal,
									'notelp'=> $dataUser->NoTelp,
									'nohp'=> $dataUser->NoHP,
									'namakontakkeluarga'=> $dataUser->NamaKontakKeluarga,
									'notelpkontakkeluarga'=> $dataUser->NoTelpKontakKeluarga,
									'hubungankeluarga'=> $dataUser->HubunganKeluarga,
									'nohpkontakkeluarga'=> $dataUser->NoHPKontakKeluarga,
									'statuskawin'=> $dataUser->StatusKawin,
									'noktp'=> $dataUser->NoKTP,
									'urlktp'=> $dataUser->urlKTP,
									'tanggalbekerja'=> $dataUser->TanggalBekerja,
									'npwp'=> $dataUser->NPWP,
									'urlnpwp'=> $dataUser->urlNPWP,
									'iddanapensiun'=> $dataUser->IDDanaPensiun,
									'urldanapensiun'=> $dataUser->urlDanaPensiun,
									'urlfoto'=> $dataUser->urlFoto,
									'tanggalberhenti'=> $dataUser->TanggalBerhenti,
									'alasanberhenti'=> $dataUser->AlasanBerhenti,
									'jeniskaryawan'=> $dataUser->JenisKaryawan,
									'dpa'=> $dataUser->DPA,
									'keterangan'=> $dataUser->Keterangan,
									'noext'=> $dataUser->NoExt,
									'norek'=> $dataUser->NoRek,
									'idjamsostek'=> $dataUser->IDJamsostek,
									'urljamsostek'=> $dataUser->urlJamsostek,
									'idgardamedika'=> $dataUser->IDGardaMedika,
									'urlgardamedika'=> $dataUser->urlGardaMedika,
									'atasnama'=> $dataUser->AtasNama,
									'namabank'=> $dataUser->NamaBank,
									'cabang'=> $dataUser->Cabang,
									'statuskaryawan'=> $dataUser->StatusKaryawan,
									'cutiTahunan' => $jumlahCutiTahunan,
									'cutiBesar' => $jumlahCutiBesar,
									'medical' => $jumlahMedical,								
									'dataDepartemen'=> $this->_getAllListDepartemen(),
									'dataGolongan' => $this->_getAllListGolongan()
								);
							}
						}else{
							echo $this->npkLogin;
						}
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','DataPribadi/dataPribadi_edit',$dataUserDetail);
					}
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		
		function editDataPribadiUser($NPKSelectedUser="",$message="")
		{
			try
			{
				if($NPKSelectedUser == ''){
					$NPKSelectedUser = $this->npkLogin;
				}
				if(!$this->session->userdata('logged_in'))
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');				
				}
				else
				{
					if(check_authorized("19"))
					{
						$dataUser = $this->user->findUserByNPK($NPKSelectedUser);					
						
						if($dataUser){							
							$jumlahMedical = 0;//$this->benefit_model->getSisaJatahMedical($NPKSelectedUser);
							
							$jumlahCutiTahunan = 0;
							$jumlahCutiBesar = 0;
							
							$cutiUser = $this->cuti_model->getJumlahCutiUser($NPKSelectedUser);
							fire_print('log','cuti User'.print_r($cutiUser,true));
							$jumlahCutiTahunan = $cutiUser['JumlahCutiTahunan'];
							$jumlahCutiBesar =$cutiUser['JumlahCutiBesar'];
							
							fire_print('log','after hitung jumlah cuti besar dan tahunan');
							foreach($dataUser as $dataUser){
								$dataUserDetail = array(
									'title'=> 'Data Pribadi',
									'npk' => $dataUser->NPK,
									'golongan' => $dataUser->Golongan,
									'nama'=> $dataUser->Nama,
									'jabatan'=> $dataUser->Jabatan,
									'departemen'=> $dataUser->Departemen,
									'emailinternal'=> $dataUser->EmailInternal,
									'jeniskelamin'=> $dataUser->JenisKelamin,
									'tempatlahir'=> $dataUser->TempatLahir,
									'tanggallahir'=> $dataUser->TanggalLahir,
									'agama'=> $dataUser->Agama,
									'alamatktp'=> $dataUser->AlamatKTP,
									'alamattinggal'=> $dataUser->AlamatTinggal,
									'notelp'=> $dataUser->NoTelp,
									'nohp'=> $dataUser->NoHP,
									'namakontakkeluarga'=> $dataUser->NamaKontakKeluarga,
									'notelpkontakkeluarga'=> $dataUser->NoTelpKontakKeluarga,
									'hubungankeluarga'=> $dataUser->HubunganKeluarga,
									'nohpkontakkeluarga'=> $dataUser->NoHPKontakKeluarga,
									'statuskawin'=> $dataUser->StatusKawin,
									'noktp'=> $dataUser->NoKTP,
									'urlktp'=> $dataUser->urlKTP,
									'tanggalbekerja'=> $dataUser->TanggalBekerja,
									'npwp'=> $dataUser->NPWP,
									'urlnpwp'=> $dataUser->urlNPWP,
									'iddanapensiun'=> $dataUser->IDDanaPensiun,
									'urldanapensiun'=> $dataUser->urlDanaPensiun,
									'urlfoto'=> $dataUser->urlFoto,
									'tanggalberhenti'=> $dataUser->TanggalBerhenti,
									'alasanberhenti'=> $dataUser->AlasanBerhenti,
									'jeniskaryawan'=> $dataUser->JenisKaryawan,
									'dpa'=> $dataUser->DPA,
									'keterangan'=> $dataUser->Keterangan,
									'noext'=> $dataUser->NoExt,
									'norek'=> $dataUser->NoRek,
									'idjamsostek'=> $dataUser->IDJamsostek,
									'urljamsostek'=> $dataUser->urlJamsostek,
									'idgardamedika'=> $dataUser->IDGardaMedika,
									'urlgardamedika'=> $dataUser->urlGardaMedika,
									'atasnama'=> $dataUser->AtasNama,
									'namabank'=> $dataUser->NamaBank,
									'cabang'=> $dataUser->Cabang,
									'statuskaryawan'=> $dataUser->StatusKaryawan,
									'cutiTahunan' => $jumlahCutiTahunan,
									'cutiBesar' => $jumlahCutiBesar,
									'medical' => $jumlahMedical,
									'dataDepartemen'=> $this->_getAllListDepartemen(),
									'dataGolongan' => $this->_getAllListGolongan(),
									'message' => $message
								);
							}
						}else{
							echo $this->npkLogin;
						}
						
						//cek dulu apakah ada transaksi. Jika status AP berarti bisa dilakukan perubahan, dan data bisa direfresh.
						//if( ! $this->user->cekTransaksiPerubahanDataKaryawan($NPKSelectedUser)){
							$this->user->refreshTempUser($NPKSelectedUser);
						//}
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','DataPribadi/dataPribadiUser_edit',$dataUserDetail);
					}
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function submitEditDataPribadiUser($NPK)
		{
			try
			{				
				$this->load->library('form_validation');			   
				$this->form_validation->set_error_delimiters('<div id="divError">', '</div>');
				
				$this->form_validation->set_rules('txtAlamatTinggal', 'Alamat Tinggal', 'trim|xss_clean');
				$this->form_validation->set_rules('txtNoTelp', 'Nomor Telepon', 'trim|xss_clean|numeric');
				$this->form_validation->set_rules('txtNoHP', 'Nomor HP', 'trim|xss_clean|numeric');
				$this->form_validation->set_rules('rbStatusKawin', 'Status Kawin', 'trim|required|xss_clean');
				
				$this->form_validation->set_rules('txtNamaKontak', 'Nama Kontak', 'trim|xss_clean');
				$this->form_validation->set_rules('txtHubunganKeluarga', 'Hubungan Keluarga', 'trim|xss_clean');
				$this->form_validation->set_rules('txtNoTelpKontakKeluarga', 'Nomor Telepon Kontak Keluarga', 'trim|xss_clean');
				$this->form_validation->set_rules('txtNoHPKontakKeluarga', 'Nomor HP Kontak Keluarga', 'trim|xss_clean');
				
				if($this->form_validation->run() == false)
				{
					$this->editDataPribadiUser($NPK,'Terdapat validasi yang salah');
				}
				else
				{
					if($this->user->cekTransaksiPerubahanDataKaryawan($NPK))
					{
						//$this->session->set_flashdata('msg','Tidak dapat disubmit, karena perubahan sebelumnya belum di approve.');
						$this->editDataPribadiUser($NPK,'Tidak dapat disubmit, karena perubahan sebelumnya belum di approve.');
					}
					else
					{
						if($this->input->post())
						{
							$this->db->trans_start();
							$KodeTempMstrUser = generateNo('DP');
							if(!$this->user->tambahTempMstrUser($NPK,$KodeTempMstrUser))
							{
								//$this->session->set_flashdata('msg','Gagal menyimpan perubahan data.');
								$this->editDataPribadiUser($NPK,'Gagal menyimpan perubahan data.');
							}
							else
							{						
								$KodeUserTask = generateNo('UT');
								if(!$this->usertask->tambahUserTask($NPK,$KodeTempMstrUser,$KodeUserTask,"DP"))
								{
									//$this->session->set_flashdata('msg','Gagal proses data ke user task');
									$this->editDataPribadiUser($NPK,'Gagal proses data ke user task');
								}
								else
								{
									$this->session->set_flashdata('msg','Terima kasih. Data telah terkirim ke HRD. Perubahan data akan dilakukan setelah HRD Admin Approve');
									$query = $this->db->get_where('globalparam',array('Name'=>'EmailHRGA'));
									foreach($query->result() as $row)
									{
										$email = $row->Value;
									}
									
									if($this->config->item('enableEmail') == 'true')
									{
										if($this->sendEmailTandaSubmit($email))
										{
											redirect('DataPribadi/DataPribadiController/EditDataPribadiUser/'.$NPK,'refresh');
										}
									}									
									redirect('DataPribadi/DataPribadiController/EditDataPribadiUser/'.$NPK,'refresh');
								}
							}
							$this->db->trans_complete();
						}
					}
				}
				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}

		
		function tambahDataPribadi($NPKSelectedUser="")
		{
			try{
			   $config['upload_path'] = './assets/uploads/files/';
			   $config['allowed_types'] = 'gif|jpg|png';
			   $config['max_size']    = '100';
			   $config['max_width']  = '1200';
			   $config['overwrite']  = FALSE;
			   $config['remove_spaces']  = TRUE;			   
			   
			   $proses = $_POST['txtJenisProses'];
			   
			   $this->load->library('upload', $config);
			   $this->load->library('form_validation');			   
			   $this->form_validation->set_error_delimiters('<div id="divError">', '</div>');
			   
			   $this->form_validation->set_rules('rbDPA', 'DPA', 'trim|required|xss_clean');
			   $this->form_validation->set_rules('txtNamaLengkap', 'Nama Lengkap', 'trim|required|xss_clean');
			   $this->form_validation->set_rules('txtTempatLahir', 'Tempat Lahir', 'trim|required|xss_clean');
			   $this->form_validation->set_rules('txtTanggalLahir', 'Tanggal Lahir', 'trim|required|xss_clean');
			   $this->form_validation->set_rules('rbJenisKelamin', 'Jenis Kelamin', 'trim|required|xss_clean');   
			   $this->form_validation->set_rules('txtAlamatKTP', 'Alamat KTP', 'trim|required|xss_clean');
			   
			   $this->form_validation->set_rules('txtAlamatTinggal', 'Alamat Tinggal', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtNoTelp', 'Nomor Telepon', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtNoHP', 'Nomor HP', 'trim|xss_clean');
			   $this->form_validation->set_rules('rbStatusKawin', 'Status Kawin', 'trim|required|xss_clean');
			   $this->form_validation->set_rules('txtNoKTP', 'Nomor KTP', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtNoNPWP', 'Nomor NPWP', 'trim|xss_clean');
			   
			   if($proses == "input")
			   {
				$this->form_validation->set_rules('txtNPK', 'NPK', 'trim|required|xss_clean|is_unique[mstruser.NPK]');
			   }
			   //$this->form_validation->set_rules('cmbDepartemen', 'Departemen', 'trim|required|xss_clean');
			   //$this->form_validation->set_rules('txtJabatan', 'Jabatan', 'trim|required|xss_clean');
			   //$this->form_validation->set_rules('cmbGolongan', 'Golongan', 'trim|required|xss_clean');
			   $this->form_validation->set_rules('txtTanggalBekerja', 'Tanggal Bekerja', 'trim|required|xss_clean');
			   $this->form_validation->set_rules('cmbStatusKaryawan', 'Status Karyawan', 'trim|required|xss_clean');
			   $this->form_validation->set_rules('txtNoExt', 'Nomor Ext', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtEmailInternal', 'Email Internal', 'trim|xss_clean|valid_email');
			   $this->form_validation->set_rules('txtIDManulife', 'ID Manulife', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtIDJamsostek', 'ID Jamsostek', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtIDGardaMedika', 'ID Garda Medika', 'trim|xss_clean');
			   
			   $this->form_validation->set_rules('txtNoRekening', 'Nomor Rekening', 'trim|xss_clean|required');
			   $this->form_validation->set_rules('txtAtasNama', 'Atas Nama', 'trim|xss_clean|required');
			   $this->form_validation->set_rules('txtBank', 'Bank', 'trim|xss_clean|required');
			   $this->form_validation->set_rules('txtCabang', 'Cabang', 'trim|xss_clean|required');
			   
			   $this->form_validation->set_rules('txtNamaKontak', 'Nama Kontak', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtHubunganKeluarga', 'Hubungan Keluarga', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtNoTelpKontakKeluarga', 'Nomor Telepon Kontak Keluarga', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtNoHPKontakKeluarga', 'Nomor HP Kontak Keluarga', 'trim|xss_clean');
			   
			   //$this->form_validation->set_rules('txtMedical', 'Medical', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtCutiTahunan', 'Cuti Tahunan', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtCutiBesar', 'Cuti Besar', 'trim|xss_clean');
			   
			   $this->form_validation->set_rules('txtTanggalBerhenti', 'Tanggal Berhenti', 'trim|xss_clean');
			   $this->form_validation->set_rules('txtAlasanBerhenti', 'Alasan Berhenti', 'trim|xss_clean');
			   
			   if($this->form_validation->run() == false)
			   {
					if($proses == "input")
					{
						$this->index();
					}
					else if($proses == "edit")
					{
						$this->editDataPribadi($NPKSelectedUser);
					}
			   }
			   else
			   {
				 if($this->input->post())
				 {
					$error = array();
					$namaFileUpload = array(
						"DanaPensiun" => "",
						"Foto" => "",
						"GardaMedika" => "",
						"Jamsostek" => "",
						"KTP" => "",
						"NPWP" => ""
					);
					
					if (!empty($_FILES["inputFileFoto"]['name'])) {						
						if( ! $this->upload->do_upload("inputFileFoto"))
						{
							$error = array('error' => 'File Foto : '.$this->upload->display_errors());
						}
						else
						{
							$uploadData = $this->upload->data();
							$namaFileUpload["Foto"] = $uploadData['file_name'];
						}
					}
					
					if (!empty($_FILES["inputFileKTP"]['name'])) {						
						if( ! $this->upload->do_upload("inputFileKTP"))
						{							
							$error = array('error' => $this->upload->display_errors('\n', '\n'));
						}
						else
						{
							$uploadData = $this->upload->data();
							$namaFileUpload["KTP"] = $uploadData['file_name'];
						}
					}
					
					if (!empty($_FILES["inputFileNPWP"]['name'])) {						
						if( ! $this->upload->do_upload("inputFileNPWP"))
						{							
							$error = array('error' => $this->upload->display_errors('\n', '\n'));
						}
						else
						{
							$uploadData = $this->upload->data();
							$namaFileUpload["NPWP"] = $uploadData['file_name'];
						}
					}
					
					if (!empty($_FILES["inputFileManulife"]['name'])) {						
						if( ! $this->upload->do_upload("inputFileManulife"))
						{
							$error = array('error' => $this->upload->display_errors('\n', '\n'));
						}
						else
						{
							$uploadData = $this->upload->data();
							$namaFileUpload["DanaPensiun"] = $uploadData['file_name'];
						}
					}
					
					if (!empty($_FILES["inputFileJamsostek"]['name'])) {						
						if( ! $this->upload->do_upload("inputFileJamsostek"))
						{
							$error = array('error' => $this->upload->display_errors('\n', '\n'));
						}
						else
						{
							$uploadData = $this->upload->data();
							$namaFileUpload["Jamsostek"] = $uploadData['file_name'];
							fire_print('log','namafileupload:'.$namaFileUpload["Jamsostek"]);
						}
					}
					
					if (!empty($_FILES["inputFileGardaMedika"]['name'])) {						
						if( ! $this->upload->do_upload("inputFileGardaMedika"))
						{
							$error = array('error' => $this->upload->display_errors('\n', '\n'));
						}
						else
						{
							$uploadData = $this->upload->data();
							$namaFileUpload["GardaMedika"] = $uploadData['file_name'];
						}
					}
					
					if(count($error) > 0)
					{
						$this->session->set_flashdata('msg','Terjadi error pada proses upload. Perlu diingat, ukuran maksimal yang bisa diupload adalah 100 Kb. Berikut pesan error dari sistem : \n'.$error['error']);
						fire_print('log','error upload:'.print_r($error,true));
						
						if($proses == "input")
						{
							$this->index();
						}
						else if($proses == "edit")
						{
							redirect('DataPribadi/DataPribadiController/EditDataPribadi/'.$NPKSelectedUser);
						}
						
						
					}
					else
					{
						if ($proses == "input")
						{
							if(!$this->user->tambahKaryawan($proses,$namaFileUpload))
							{
								$this->session->set_flashdata('msg','Sistem gagal menyimpan data');
								$this->index();								
							}
							else
							{						
								redirect('DataPribadi/DataPribadiController','refresh');
							}
						}
						else if($proses == "edit")
						{
							if(!$this->user->tambahKaryawan($proses,$namaFileUpload))
							{
								$this->session->set_flashdata('msg','Sistem gagal menyimpan perubahan data');
								redirect('DataPribadi/DataPribadiController/EditDataPribadi/'.$NPKSelectedUser);
							}
							else
							{
								$this->session->set_flashdata('msg','Sistem sukses menyimpan data!');
								redirect('DataPribadi/DataPribadiController/EditDataPribadi/'.$NPKSelectedUser,'refresh');
							}
						}
					}
				 }
			   }
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
	}
?>