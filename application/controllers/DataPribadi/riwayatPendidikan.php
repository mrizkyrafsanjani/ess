<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class RiwayatPendidikan extends CI_Controller {
	var $npkLogin;
	var $NPKSelectedUser;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index($NPKuser,$status='')
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->NPKSelectedUser = $NPKuser;
			$this->_riwayatPendidikan($status);
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _riwayatPendidikan($status)
    {
		$crud = new grocery_crud();
		$crud->set_subject('Riwayat Pendidikan');
		//$crud->set_theme('datatables');
		
        $crud->set_table('riwayatpendidikan');
		$crud->where('riwayatpendidikan.deleted','0');
		$crud->where('riwayatpendidikan.NPK',$this->NPKSelectedUser);

		$crud->columns('Pendidikan', 'Institusi', 'Jurusan', 'TahunKelulusan');
		$crud->fields('Pendidikan', 'Institusi', 'Jurusan', 'TahunKelulusan');
				
		$crud->required_fields('Pendidikan', 'Institusi', 'Jurusan', 'TahunKelulusan');
		$crud->display_as('TahunKelulusan','Tahun Kelulusan');
		
		$crud->callback_field('Pendidikan',array($this,'add_field_callback_pendidikan'));
		$crud->callback_field('TahunKelulusan',array($this,'add_field_callback_tahunkelulusan'));
		
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_delete(array($this,'_delete'));		
		$crud->unset_read();
		$crud->unset_print();
		$crud->unset_export();
		
		if($status == "approve")
		{
			$crud->unset_operations();
		}
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$data = array(
			'title' => 'Pengaturan Menu',
			'body' => $output
		); 
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		
        $this->load->view('DataPribadi/riwayatPendidikan_view',$data);
    }
	
	function _insert($post_array){
		try{
			$post_array['NPK'] = $this->NPKSelectedUser;
			$post_array['CreatedOn'] = date('Y-m-d H:i:s');
			$post_array['CreatedBy'] = $this->npkLogin;
			
			return $this->db->insert('riwayatpendidikan',$post_array);
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('riwayatpendidikan',$post_array,array('KodeRiwayatPendidikan' => $primary_key));
	}
	
	function add_field_callback_pendidikan($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedSMA = '';
		$selectedD1 = '';
		$selectedD2 = '';
		$selectedD3 = '';
		$selectedD4 = '';
		$selectedS1 = '';
		$selectedS2 = '';
		$selectedS3 = '';
		
		switch($value)
		{
			case "SMA": $selectedSMA = 'selected'; break;
			case "D1": $selectedD1 = 'selected'; break;
			case "D2": $selectedD2 = 'selected'; break;
			case "D3": $selectedD3 = 'selected'; break;
			case "D4": $selectedD4 = 'selected'; break;
			case "S1": $selectedS1 = 'selected'; break;
			case "S2": $selectedS2 = 'selected'; break;
			case "S3": $selectedS3 = 'selected'; break;
		}
		
		return ' <select name="Pendidikan">
			<option value = ""></option>
			<option '.$selectedSMA.' value="SMA">SMA - Setara</option>
			<option '.$selectedD1.' value="D1">D1</option>
			<option '.$selectedD2.' value="D2">D2</option>
			<option '.$selectedD3.' value="D3">D3</option>
			<option '.$selectedD4.' value="D4">D4</option>
			<option '.$selectedS1.' value="S1">S1</option>
			<option '.$selectedS2.' value="S2">S2</option>
			<option '.$selectedS3.' value="S3">S3</option>
		</select>';
	}
	
	function add_field_callback_tahunkelulusan($value = '', $primary_key = null)
	{
		$strSelectHTML = ' <select name="TahunKelulusan">
			<option value = ""></option>';
		for($i=date("Y");$i>date("Y")-100;$i--)
		{
			$selectedTahunKelulusan = '';
			if($i == $value)
			{
				$selectedTahunKelulusan = 'selected';
			}
			$strSelectHTML .= '<option '.$selectedTahunKelulusan.' value ="'.$i.'">'.$i.'</option>';
		}
		$strSelectHTML .= '</select>';
		
		return $strSelectHTML;
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */