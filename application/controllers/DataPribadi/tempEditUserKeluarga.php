<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class TempEditUserKeluarga extends CI_Controller {
	var $npkLogin;
	var $NPKUser;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {		
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->_tempedituserkeluarga();
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function KeluargaBesar()
	{
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->_tempedituserkeluarga('B');
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function KeluargaInti($NPKUser='',$status='')
	{
		//$NPKUser baru akan terisi jika dibuka dari page DataPribadiUser_edit.php untuk menandakan bahwa diisi oleh user.
		//$NPKUser akan kosong jika di buka dari page DataPribadi_edit.php yang merupakan hak admin.
		//$NPKUser akan berisi kodeTempMstrUser jika berasal dari transaksi perubahan data oleh peserta.
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->NPKUser = $NPKUser;
			$this->_tempedituserkeluarga('I',$status);
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function _tempedituserkeluarga($statusKeluarga,$status='')
    {
		try{
			$crud = new grocery_crud();
			
			//$crud->set_theme('datatables');
			
			$crud->set_table('tempedituserkeluarga');
			$crud->where('tempedituserkeluarga.deleted','0');
			$crud->where('JenisKeluarga',$statusKeluarga);
			if($status == "approval")
			{
				$crud->unset_operations();
				$crud->where('KodeTempMstrUser', $this->NPKUser);
			}
			else
			{
				if($this->NPKUser == ''){
					$crud->where('StatusTransaksi','PE');
					$crud->where('createdBy',$this->npkLogin);				
					if($statusKeluarga == "B")
					{
						$crud->set_subject('Keluarga Besar');
						$crud->callback_insert(array($this,'_insertKeluargaBesar'));
						$crud->callback_field('HubunganKeluarga',array($this,'add_field_callback_hubungankeluargabesar'));
					}
					else
					{
						$crud->set_subject('Keluarga Inti');
						$crud->callback_insert(array($this,'_insertKeluargaInti'));
						$crud->callback_field('HubunganKeluarga',array($this,'add_field_callback_hubungankeluargainti'));
					}
				}
				else
				{
					$crud->where('TempNPK',$this->NPKUser);
					if($statusKeluarga == "I")
					{
						$crud->set_subject('Keluarga Inti');
						$crud->callback_insert(array($this,'_insertbyuser'));
						$crud->callback_update(array($this,'_updatebyuser'));
						$crud->callback_field('HubunganKeluarga',array($this,'add_field_callback_hubungankeluargainti'));
					}				
				}
			}
			$crud->columns('HubunganKeluarga', 'Nama', 'JenisKelamin', 'TempatLahir','TanggalLahir','Pendidikan','Pekerjaan');
			$crud->fields('HubunganKeluarga', 'Nama', 'JenisKelamin', 'TempatLahir','TanggalLahir','Pendidikan','Pekerjaan');
			
			$crud->required_fields('HubunganKeluarga', 'Nama', 'JenisKelamin', 'TempatLahir','TanggalLahir');
			
			$crud->display_as('HubunganKeluarga','Hubungan Keluarga')->display_as('JenisKelamin','Jenis Kelamin')->display_as('TempatLahir','Tempat Lahir')->display_as('TanggalLahir','Tanggal Lahir');
			
			
			$crud->callback_field('JenisKelamin',array($this,'add_field_callback_jeniskelamin'));
			$crud->callback_field('Pendidikan',array($this,'add_field_callback_pendidikan'));
			$crud->callback_delete(array($this,'_delete'));
			$crud->unset_read();
			$crud->unset_print();
			$crud->unset_export();
			
			
			$output = $crud->render();
			$js = " <script>
				$(document).ready(function() {
				var HubunganKeluarga = $('#HubunganKeluarga');
				var radioLaki = $('#radio_L');
				var radioPerempuan = $('#radio_P');
				HubunganKeluarga.change(function(){
					if(HubunganKeluarga.val() == 'Ayah' || HubunganKeluarga.val() == 'Suami')
						radioLaki.prop('checked', true);
					if(HubunganKeluarga.val() == 'Ibu' || HubunganKeluarga.val() == 'Istri')
						radioPerempuan.prop('checked', true);					
				});
				
			}); </script>";
			$output->output.= $js;
			$this-> _outputview($output,$statusKeluarga,$status);
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
 
    function _outputview($output = null,$statusKeluarga,$status ='')
    {
		$data = array(
			'title' => 'Pengaturan Temp Keluarga',
			'body' => $output
		); 
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		if($status == "approval")
		{
			$this->load->view('DataPribadi/tempedituserkeluargaInti_view',$data);
		}
		else
		{
			if($statusKeluarga == "B"){
				$this->load->view('DataPribadi/keluargaBesar_view',$data);
			}else{
				$this->load->view('DataPribadi/keluargaInti_view',$data);
			}
		}
    }
	
	function _insertKeluargaBesar($post_array){
		$post_array['JenisKeluarga'] = "B";
		$post_array['StatusTransaksi'] = "PE";
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('tempedituserkeluarga',$post_array);
	}
	
	function _insertKeluargaInti($post_array){
		$post_array['JenisKeluarga'] = "I";
		$post_array['StatusTransaksi'] = "PE";
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('tempedituserkeluarga',$post_array);
	}
	
	function _insertbyuser($post_array){
		$post_array['JenisKeluarga'] = "I";
		$post_array['TempNPK'] = $this->NPKUser;
		$post_array['StatusTransaksi'] = "PE";
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');		
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('tempedituserkeluarga',$post_array);
	}
	
	function _updatebyuser($post_array,$primary_key)
	{
		//$post_array['deleted'] = "0";
		$post_array['JenisKeluarga'] = "I";
		$post_array['TempNPK'] = $this->NPKUser;
		$post_array['StatusTransaksi'] = "PE";
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');		
		$post_array['UpdatedBy'] = $this->npkLogin;		
		$this->db->update('tempedituserkeluarga',$post_array,array('Kodetempedituserkeluarga' => $primary_key));
		return true;
	}
	
	function _delete($primary_key){
		try
		{	$post_array = array(
				'Deleted' => '1',
				'UpdatedOn' => date('Y-m-d H:i:s'),
				'UpdatedBy' => $this->npkLogin
			);
			/* $post_array['Deleted'] = '1';
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin; */
			fire_print('log',"primary_key: $primary_key");
			return $this->db->update('tempedituserkeluarga',$post_array,array('KodeTempEditUserKeluarga' => $primary_key));
		}
		catch(Exception $e)
		{
			log_message( 'error','Error dri delete temp keluarga user : '. $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function add_field_callback_hubungankeluargabesar($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedAyah = '';
		$selectedIbu = '';
		$selectedKakak = '';
		$selectedAdik = '';
		
		switch($value)
		{
			case "Ayah": $selectedAyah = 'selected'; break;
			case "Ibu": $selectedIbu = 'selected'; break;
			case "Kakak": $selectedKakak = 'selected'; break;
			case "Adik": $selectedAdik = 'selected'; break;
		}
		
		return ' <select id="HubunganKeluarga" name="HubunganKeluarga">
			<option value = ""></option>
			<option '.$selectedAyah.' value="Ayah">Ayah</option>
			<option '.$selectedIbu.' value="Ibu">Ibu</option>
			<option '.$selectedKakak.' value="Kakak">Kakak</option>
			<option '.$selectedAdik.' value="Adik">Adik</option>
		</select>';
	}
	
	function add_field_callback_hubungankeluargainti($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedSuami = '';
		$selectedIstri = '';
		$selectedAnak = '';
		
		switch($value)
		{
			case "Suami": $selectedSuami = 'selected'; break;
			case "Istri": $selectedIstri = 'selected'; break;
			case "Anak": $selectedAnak = 'selected'; break;
		}
		
		return ' <select id="HubunganKeluarga" name="HubunganKeluarga">
			<option value = ""></option>
			<option '.$selectedSuami.' value="Suami">Suami</option>
			<option '.$selectedIstri.' value="Istri">Istri</option>
			<option '.$selectedAnak.' value="Anak">Anak</option>
		</select>';
	}
	
	function add_field_callback_jeniskelamin($value = '', $primary_key = null)
	{
		$selectedL = '';
		$selectedP = '';
		switch($value)
		{
			case "L": $selectedL = 'checked'; break;
			case "P": $selectedP = 'checked'; break;
		}
		return '<input id="radio_L" type="radio" name="JenisKelamin" value="L" ' . $selectedL . ' >Laki-laki
			<input id="radio_P" type="radio" name="JenisKelamin" value="P" ' . $selectedP . ' >Perempuan';
	}
	
	function add_field_callback_pendidikan($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedSD = '';
		$selectedSMP = '';
		$selectedSMA = '';
		$selectedD1 = '';
		$selectedD2 = '';
		$selectedD3 = '';
		$selectedD4 = '';
		$selectedS1 = '';
		$selectedS2 = '';
		$selectedS3 = '';
		
		switch($value)
		{
			case "SD": $selectedSD = 'selected'; break;
			case "SMP": $selectedSMP = 'selected'; break;
			case "SMA": $selectedSMA = 'selected'; break;
			case "D1": $selectedD1 = 'selected'; break;
			case "D2": $selectedD2 = 'selected'; break;
			case "D3": $selectedD3 = 'selected'; break;
			case "D4": $selectedD4 = 'selected'; break;
			case "S1": $selectedS1 = 'selected'; break;
			case "S2": $selectedS2 = 'selected'; break;
			case "S3": $selectedS3 = 'selected'; break;
		}
		
		return ' <select name="Pendidikan">
			<option value = ""></option>
			<option '.$selectedSD.' value="SD">SD - Setara</option>
			<option '.$selectedSMP.' value="SMP">SMP - Setara</option>
			<option '.$selectedSMA.' value="SMA">SMA - Setara</option>
			<option '.$selectedD1.' value="D1">D1</option>
			<option '.$selectedD2.' value="D2">D2</option>
			<option '.$selectedD3.' value="D3">D3</option>
			<option '.$selectedD4.' value="D4">D4</option>
			<option '.$selectedS1.' value="S1">S1</option>
			<option '.$selectedS2.' value="S2">S2</option>
			<option '.$selectedS3.' value="S3">S3</option>
		</select>';
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */