<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class TempRiwayatTraining extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->_temptraining();
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _temptraining()
    {
		try{
			$crud = new grocery_crud();
			$crud->set_subject('Training dan Seminar');
			//$crud->set_theme('datatables');
			
			$crud->set_table('tempriwayattraining');
			$crud->where('tempriwayattraining.deleted','0');
			$crud->where('tempriwayattraining.StatusTransaksi','PE');
			$crud->where('tempriwayattraining.createdBy',$this->npkLogin);

			$crud->columns('NamaTraining', 'Lembaga', 'DariTanggal', 'SampaiTanggal','Biaya','SifatTraining','Sertifikasi','Nilai');
			$crud->fields('NamaTraining', 'Lembaga', 'DariTanggal', 'SampaiTanggal','Biaya','SifatTraining','Sertifikasi','Nilai');
			
			$crud->required_fields('NamaTraining', 'Lembaga', 'DariTanggal', 'SampaiTanggal','SifatTraining','Sertifikasi');
			
			$crud->display_as('NamaTraining','Nama Training')->display_as('SifatTraining','Sifat Training');
			$crud->display_as('DariTanggal','Dari Tanggal')->display_as('SampaiTanggal','Sampai Tanggal');
			
			$crud->callback_field('SifatTraining',array($this,'add_field_callback_sifattraining'));
			$crud->callback_field('Sertifikasi',array($this,'add_field_callback_sertifikasi'));
			$crud->callback_delete(array($this,'_delete'));		
			$crud->callback_insert(array($this,'_insert'));
			$crud->unset_read();
			$crud->unset_print();
			$crud->unset_export();
			$output = $crud->render();
	   
			$this-> _outputview($output);
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
 
    function _outputview($output = null)
    {
		$data = array(
			'title' => 'Pengaturan Riwayat Training',
			'body' => $output
		); 
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		
        $this->load->view('DataPribadi/riwayatTraining_view',$data);
    }
	
	function _insert($post_array){
		$post_array['StatusTransaksi'] = "PE";
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');		
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('tempriwayattraining',$post_array);
	}
	
	function _delete($primary_key){
		return $this->db->update('tempriwayattraining',array('deleted' => '1'),array('KodeTempRiwayatTraining' => $primary_key));
	}
	
	function add_field_callback_sifattraining($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedHardskill = '';
		$selectedSoftskill = '';
		
		switch($value)
		{
			case "Hardskill": $selectedHardskill = 'selected'; break;
			case "Softskill": $selectedSoftskill = 'selected'; break;
		}
		
		return ' <select name="SifatTraining">
			<option value = ""></option>
			<option '.$selectedHardskill.' value="Hardskill">Hardskill</option>
			<option '.$selectedSoftskill.' value="Softskill">Softskill</option>
		</select>';
	}
	
	function add_field_callback_sertifikasi($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedYa = '';
		$selectedTidak = '';
		
		switch($value)
		{
			case "Y": $selectedYa = 'selected'; break;
			case "T": $selectedTidak = 'selected'; break;
		}
		
		return ' <select name="Sertifikasi">
			<option value = ""></option>
			<option '.$selectedYa.' value="Y">Ya</option>
			<option '.$selectedTidak.' value="T">Tidak</option>
		</select>';
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */