<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class TempRiwayatRotasiPekerjaan extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('mstrgolongan','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->_temptempriwayatrotasipekerjaandpa();
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _temptempriwayatrotasipekerjaandpa()
    {
		try{
			$crud = new grocery_crud();
			$crud->set_subject('Riwayat Mutasi/Rotasi/Demosi/Promosi Pekerjaan DPA');
			//$crud->set_theme('datatables');
			
			$crud->set_table('tempriwayatrotasipekerjaandpa');
			$crud->where('tempriwayatrotasipekerjaandpa.deleted','0');
			$crud->set_relation('KodeDepartemen','departemen','NamaDepartemen',array('deleted' => '0'));
			$crud->set_relation('KodeJabatan','jabatan','NamaJabatan',array('deleted'=> '0'));
			$crud->set_relation('Golongan','mstrgolongan','Golongan',array('deleted'=> '0'));
			$crud->where('tempriwayatrotasipekerjaandpa.StatusTransaksi','PE');
			$crud->where('tempriwayatrotasipekerjaandpa.createdBy',$this->npkLogin);
			

			$crud->columns('KodeDepartemen', 'KodeJabatan','Golongan','Subgolongan', 'BulanRotasi', 'TahunRotasi');
			$crud->fields('KodeDepartemen', 'KodeJabatan','Golongan','Subgolongan', 'BulanRotasi', 'TahunRotasi');
			
			$crud->required_fields('KodeDepartemen', 'KodeJabatan','Golongan','Subgolongan', 'BulanRotasi', 'TahunRotasi');
			
			$crud->display_as('KodeJabatan','Jabatan');
			$crud->display_as('KodeDepartemen','Departemen')->display_as('BulanRotasi','Bulan')->display_as('TahunRotasi','Tahun');
			$crud->callback_field('Subgolongan',array($this,'add_field_callback_subgolongan'));
			$crud->callback_field('BulanRotasi',array($this,'add_field_callback_bulanrotasi'));
			$crud->callback_field('TahunRotasi',array($this,'add_field_callback_tahunrotasi'));
			
			$crud->callback_delete(array($this,'_delete'));		
			$crud->callback_insert(array($this,'_insert'));
			$crud->callback_update(array($this,'_update'));
			$crud->unset_read();
			$crud->unset_print();
			$crud->unset_export();
			$output = $crud->render();
	   
			$this-> _outputview($output);       
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
 
    function _outputview($output = null)
    {
		$data = array(
			'title' => 'Pengaturan Riwayat Rotasi Pekerjaan',
			'body' => $output
		); 
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		
        $this->load->view('DataPribadi/riwayatRotasi_view',$data);
    }
	
	function _insert($post_array){
		//$post_array['Golongan'] = $_POST['Golongan']. $_POST['Subgolongan'];
		$post_array['StatusTransaksi'] = "PE";
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');		
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('tempriwayatrotasipekerjaandpa',$post_array);
	}
	
	function _update($post_array,$primary_key){		
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');		
		$post_array['UpdatedBy'] = $this->npkLogin;		
		return $this->db->update('tempriwayatrotasipekerjaandpa',$post_array,array('KodeTempRiwayatRotasiPekerjaanDPA' => $primary_key));
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('tempriwayatrotasipekerjaandpa',$post_array,array('KodeTempRiwayatRotasiPekerjaanDPA' => $primary_key));
	}
	
	function add_field_callback_subgolongan($value = '', $primary_key = null)
	{
		/* $strGolongan = "";
		$dataGolongan = $this->mstrgolongan->getGolongan("");
		if($dataGolongan){
			$golongan_array = array();
			foreach($dataGolongan as $row){
				$strGolongan .= "<option value='" . $row->Golongan . "'>".$row->Golongan."</option>";			   
			}
		}
		
		$strSelectHTML = '<select name="Golongan" id="Golongan">
			<option value=""></option>
			'. $strGolongan .'
		</select>'; */
		$A = '';
		$B = '';
		$C = '';
		$D = '';
		$E = '';
		$F = '';
		switch($value){
			case "A": $A = "selected"; break;
			case "B": $B = "selected"; break;
			case "C": $C = "selected"; break;
			case "D": $D = "selected"; break;
			case "E": $E = "selected"; break;
			case "F": $F = "selected"; break;
		}
		$strSelectHTML = '<select name="Subgolongan">
			<option value=""></option>
			<option value="A" '.$A.'>A</option>
			<option value="B" '.$B.'>B</option>
			<option value="C" '.$C.'>C</option>
			<option value="D" '.$D.'>D</option>
			<option value="E" '.$E.'>E</option>
			<option value="F" '.$F.'>F</option>
		</select>';
		
		return $strSelectHTML;
	}
	
	function add_field_callback_bulanrotasi($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		
		$strSelectHTML = '<select name="BulanRotasi">
			<option value = ""></option>';
		for($i=1;$i<=12;$i++)
		{
			$selected = '';
			if($i == $value)
			{
				$selected = 'selected';
			}
			$strSelectHTML .= '<option '.$selected.' value ="'.$i.'">'.date("F",mktime(0,0,0,$i,10)).'</option>';
		}
		$strSelectHTML .= '/<select>';
		
		return $strSelectHTML;
	}
	
	function add_field_callback_tahunrotasi($value = '', $primary_key = null)
	{
		$strSelectHTML = ' <select name="TahunRotasi">
			<option value = ""></option>';
		for($i=date("Y");$i>date("Y")-100;$i--)
		{
			$selectedTahunKelulusan = '';
			if($i == $value)
			{
				$selectedTahunKelulusan = 'selected';
			}
			$strSelectHTML .= '<option '.$selectedTahunKelulusan.' value ="'.$i.'">'.$i.'</option>';
		}
		$strSelectHTML .= '</select>';
		
		return $strSelectHTML;
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */