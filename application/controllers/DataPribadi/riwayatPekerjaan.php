<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class RiwayatPekerjaan extends CI_Controller {
	var $npkLogin;
	var $NPKSelectedUser;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index($NPKuser,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->NPKSelectedUser = $NPKuser;
			$this->_riwayatPekerjaan($page);
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _riwayatPekerjaan($page)
    {
		try{
			$crud = new grocery_crud();
			$crud->set_subject('Riwayat Pekerjaan');
			//$crud->set_theme('datatables');
			
			$crud->set_table('riwayatpekerjaan');
			$crud->where('riwayatpekerjaan.deleted','0');
			$crud->where('riwayatpekerjaan.NPK',$this->NPKSelectedUser);

			$crud->columns('NamaPerusahaan', 'Jabatan', 'BulanMasuk', 'TahunMasuk','BulanKeluar','TahunKeluar');
			$crud->fields('NamaPerusahaan', 'Jabatan', 'BulanMasuk', 'TahunMasuk','BulanKeluar','TahunKeluar');
			
			$crud->required_fields('NamaPerusahaan', 'Jabatan', 'BulanMasuk', 'TahunMasuk','BulanKeluar','TahunKeluar');
			
			$crud->display_as('NamaPerusahaan','Nama Perusahaan')->display_as('BulanMasuk','Bulan Masuk');
			$crud->display_as('TahunMasuk','Tahun Masuk')->display_as('BulanKeluar','Bulan Keluar')->display_as('TahunKeluar','Tahun Keluar');
			$crud->callback_field('BulanMasuk',array($this,'add_field_callback_bulanmasuk'));
			$crud->callback_field('BulanKeluar',array($this,'add_field_callback_bulankeluar'));
			$crud->callback_field('TahunMasuk',array($this,'add_field_callback_tahunmasuk'));
			$crud->callback_field('TahunKeluar',array($this,'add_field_callback_tahunkeluar'));
			
			$crud->callback_insert(array($this,'_insert'));
			$crud->callback_delete(array($this,'_delete'));		
			$crud->unset_read();
			$crud->unset_print();
			$crud->unset_export();	
			
			//$crud->unset_jquery();
			$crud->unset_jquery_ui();
			if($page=="user"){
				$crud->unset_operations();
			}
			$output = $crud->render();
	   
			$this-> _outputview($output);
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
 
    function _outputview($output = null)
    {
		$data = array(
			'title' => 'Pengaturan Riwayat Pekerjaan',
			'body' => $output
		); 
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		
        $this->load->view('DataPribadi/riwayatPekerjaan_view',$data);
    }
	
	function _insert($post_array){
		try{
			$post_array['NPK'] = $this->NPKSelectedUser;
			$post_array['CreatedOn'] = date('Y-m-d H:i:s');
			$post_array['CreatedBy'] = $this->npkLogin;
			
			return $this->db->insert('riwayatpekerjaan',$post_array);
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('riwayatpekerjaan',$post_array,array('KodeRiwayatPekerjaan' => $primary_key));
	}
	
	function add_field_callback_bulanmasuk($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		
		$strSelectHTML = '<select name="BulanMasuk">
			<option value = ""></option>';
		for($i=1;$i<=12;$i++)
		{
			$selected = '';
			if($i == $value)
			{
				$selected = 'selected';
			}
			$strSelectHTML .= '<option '.$selected.' value ="'.$i.'">'.date("F",mktime(0,0,0,$i,10)).'</option>';
		}
		$strSelectHTML .= '/<select>';
		
		return $strSelectHTML;
	}
	
	function add_field_callback_bulankeluar($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		
		$strSelectHTML = '<select name="BulanKeluar">
			<option value = ""></option>';
		for($i=1;$i<=12;$i++)
		{
			$selected = '';
			if($i == $value)
			{
				$selected = 'selected';
			}
			$strSelectHTML .= '<option '.$selected.' value ="'.$i.'">'.date("F",mktime(0,0,0,$i,10)).'</option>';
		}
		$strSelectHTML .= '/<select>';
		
		return $strSelectHTML;
	}
	
	function add_field_callback_tahunmasuk($value = '', $primary_key = null)
	{
		$strSelectHTML = ' <select name="TahunMasuk">
			<option value = ""></option>';
		for($i=date("Y");$i>date("Y")-100;$i--)
		{
			$selectedTahunKelulusan = '';
			if($i == $value)
			{
				$selectedTahunKelulusan = 'selected';
			}
			$strSelectHTML .= '<option '.$selectedTahunKelulusan.' value ="'.$i.'">'.$i.'</option>';
		}
		$strSelectHTML .= '</select>';
		
		return $strSelectHTML;
	}
	
	function add_field_callback_tahunkeluar($value = '', $primary_key = null)
	{
		$strSelectHTML = ' <select name="TahunKeluar">
			<option value = ""></option>';
		for($i=date("Y");$i>date("Y")-100;$i--)
		{
			$selectedTahunKelulusan = '';
			if($i == $value)
			{
				$selectedTahunKelulusan = 'selected';
			}
			$strSelectHTML .= '<option '.$selectedTahunKelulusan.' value ="'.$i.'">'.$i.'</option>';
		}
		$strSelectHTML .= '</select>';
		
		return $strSelectHTML;
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */