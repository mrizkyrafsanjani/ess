<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class TempEditUserRiwayatPendidikan extends CI_Controller {
	var $npkLogin;
	var $NPKUser;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index($NPKUser='',$status='')
    {
		//$NPKUser baru akan terisi jika dibuka dari page DataPribadiUser_edit.php untuk menandakan bahwa diisi oleh user.
		//$NPKUser akan kosong jika di buka dari page DataPribadi_edit.php yang merupakan hak admin.
		//$NPKUser akan berisi kodeTempMstrUser jika berasal dari transaksi perubahan data oleh peserta.
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->NPKUser = $NPKUser;
			$this->_tempRwayatPendidikan($status);
		}
		else
		{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _tempRwayatPendidikan($status)
    {
		try{
			$crud = new grocery_crud();
			$crud->set_subject('Riwayat Pendidikan');
			//$crud->set_theme('datatables');
			
			$crud->set_table('tempedituserriwayatpendidikan');
			$crud->where('tempedituserriwayatpendidikan.deleted','0');
			if($status == "approval")
			{
				$crud->where('KodeTempMstrUser',$this->NPKUser);
				$crud->unset_operations();
			}
			else
			{
				if($this->NPKUser == ''){
					$crud->where('StatusTransaksi','PE');
					$crud->where('createdBy',$this->npkLogin);
					$crud->callback_insert(array($this,'_insert'));
				}else{
					//$crud->where('StatusTransaksi','AP');
					//$crud->where('createdBy','System');
					$crud->where('TempNPK',$this->NPKUser);
					$crud->callback_insert(array($this,'_insertbyuser'));
					$crud->callback_update(array($this,'_updatebyuser'));
				}
			}
			$crud->columns('Pendidikan', 'Institusi', 'Jurusan', 'TahunKelulusan');
			$crud->fields('Pendidikan', 'Institusi', 'Jurusan', 'TahunKelulusan');
					
			$crud->required_fields('Pendidikan', 'Institusi', 'Jurusan', 'TahunKelulusan');
			$crud->display_as('TahunKelulusan','Tahun Kelulusan');
			 
			$crud->callback_field('Pendidikan',array($this,'add_field_callback_pendidikan'));
			$crud->callback_field('TahunKelulusan',array($this,'add_field_callback_tahunkelulusan'));
			$crud->callback_delete(array($this,'_delete'));
			
			$crud->unset_read();
			$crud->unset_print();
			$crud->unset_export();
			$crud->unset_operations();
			//$crud->unset_jquery();
			$crud->unset_jquery_ui();
			$output = $crud->render();
	   
			$this-> _outputview($output,$status);        
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
 
    function _outputview($output = null,$status)
    {
		$data = array(
			'title' => 'Temp Riwayat Pendidikan',
			'body' => $output
		); 
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		if($status == "approval")
		{
			$this->load->view('DataPribadi/tempedituserriwayatpendidikan_view',$data);
		}
		else
		{
			$this->load->view('DataPribadi/riwayatPendidikan_view',$data);
		}
    }
	
	function _insert($post_array){
		$post_array['StatusTransaksi'] = "PE";
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');		
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('tempedituserriwayatpendidikan',$post_array);
	}
	
	function _insertbyuser($post_array){
		$post_array['TempNPK'] = $this->NPKUser;
		$post_array['StatusTransaksi'] = "PE";
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');		
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('tempedituserriwayatpendidikan',$post_array);
	}
	
	function _updatebyuser($post_array,$primary_key)
	{
		//$post_array['deleted'] = "0";
		$post_array['TempNPK'] = $this->NPKUser;
		$post_array['StatusTransaksi'] = "PE";
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');		
		$post_array['UpdatedBy'] = $this->npkLogin;		
		$this->db->update('tempedituserriwayatpendidikan',$post_array,array('Kodetempedituserriwayatpendidikan' => $primary_key));
		return true;
	}
	
	function _delete($primary_key){
		return $this->db->update('tempedituserriwayatpendidikan',array('deleted' => '1'),array('Kodetempedituserriwayatpendidikan' => $primary_key));
	}
	
	function add_field_callback_pendidikan($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedSMA = '';
		$selectedD1 = '';
		$selectedD2 = '';
		$selectedD3 = '';
		$selectedD4 = '';
		$selectedS1 = '';
		$selectedS2 = '';
		$selectedS3 = '';
		
		switch($value)
		{
			case "SMA": $selectedSMA = 'selected'; break;
			case "D1": $selectedD1 = 'selected'; break;
			case "D2": $selectedD2 = 'selected'; break;
			case "D3": $selectedD3 = 'selected'; break;
			case "D4": $selectedD4 = 'selected'; break;
			case "S1": $selectedS1 = 'selected'; break;
			case "S2": $selectedS2 = 'selected'; break;
			case "S3": $selectedS3 = 'selected'; break;
		}
		
		return ' <select name="Pendidikan">
			<option value = ""></option>
			<option '.$selectedSMA.' value="SMA">SMA - Setara</option>
			<option '.$selectedD1.' value="D1">D1</option>
			<option '.$selectedD2.' value="D2">D2</option>
			<option '.$selectedD3.' value="D3">D3</option>
			<option '.$selectedD4.' value="D4">D4</option>
			<option '.$selectedS1.' value="S1">S1</option>
			<option '.$selectedS2.' value="S2">S2</option>
			<option '.$selectedS3.' value="S3">S3</option>
		</select>';
	}
	
	function add_field_callback_tahunkelulusan($value = '', $primary_key = null)
	{
		$strSelectHTML = ' <select name="TahunKelulusan">
			<option value = ""></option>';
		for($i=date("Y");$i>date("Y")-100;$i--)
		{
			$selectedTahunKelulusan = '';
			if($i == $value)
			{
				$selectedTahunKelulusan = 'selected';
			}
			$strSelectHTML .= '<option '.$selectedTahunKelulusan.' value ="'.$i.'">'.$i.'</option>';
		}
		$strSelectHTML .= '</select>';
		
		return $strSelectHTML;
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */