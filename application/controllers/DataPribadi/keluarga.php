<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Keluarga extends CI_Controller {
 	var $npkLogin;
	var $NPKSelectedUser;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index($NPKuser)
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->NPKSelectedUser = $NPKuser;
			$this->_keluarga();
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function keluargaBesar($NPKuser,$page='')
	{
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->NPKSelectedUser = $NPKuser;
			$this->_keluarga('B',$page);
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function keluargaInti($NPKuser,$page='')
	{
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->NPKSelectedUser = $NPKuser;
			$this->_keluarga('I',$page);
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function _keluarga($statusKeluarga,$page)
    {
		try{
			$crud = new grocery_crud();
			$crud->set_subject('Keluarga');
			//$crud->set_theme('datatables');
			
			$crud->set_table('keluarga');
			$crud->where('keluarga.deleted','0');
			$crud->where('keluarga.NPK',$this->NPKSelectedUser);
			$crud->where('JenisKeluarga',$statusKeluarga);
			
			if($statusKeluarga == "B")
			{
				$crud->set_subject('Keluarga Besar');
				$crud->callback_insert(array($this,'_insertKeluargaBesar'));
				$crud->callback_field('HubunganKeluarga',array($this,'add_field_callback_hubungankeluargabesar'));
			}
			else
			{
				$crud->set_subject('Keluarga Inti');
				$crud->callback_insert(array($this,'_insertKeluargaInti'));
				$crud->callback_field('HubunganKeluarga',array($this,'add_field_callback_hubungankeluargainti'));
			}
			$crud->columns('HubunganKeluarga', 'Nama', 'JenisKelamin', 'TempatLahir','TanggalLahir','Pendidikan','Pekerjaan','Keterangan');
			$crud->fields('HubunganKeluarga', 'Nama', 'JenisKelamin', 'TempatLahir','TanggalLahir','Pendidikan','Pekerjaan','Keterangan');
			
			$crud->required_fields('HubunganKeluarga', 'Nama', 'JenisKelamin', 'TempatLahir','TanggalLahir');
			$crud->display_as('HubunganKeluarga','Hubungan Keluarga')->display_as('JenisKelamin','Jenis Kelamin')->display_as('TempatLahir','Tempat Lahir')->display_as('TanggalLahir','Tanggal Lahir');
			
			$crud->callback_field('JenisKelamin',array($this,'add_field_callback_jeniskelamin'));
			$crud->callback_field('Pendidikan',array($this,'add_field_callback_pendidikan'));
			$crud->callback_field('Keterangan',array($this,'add_field_callback_keterangan'));
			$crud->callback_delete(array($this,'_delete'));		
			$crud->unset_read();
			$crud->unset_print();
			$crud->unset_export();
			//$crud->unset_jquery();
			//$crud->unset_jquery_ui();
			if($page=="user"){
				$crud->unset_operations();
			}
			$output = $crud->render();
			$js = " <script>
				$(document).ready(function() {
				var HubunganKeluarga = $('#HubunganKeluarga');
				var radioLaki = $('#radio_L');
				var radioPerempuan = $('#radio_P');
				HubunganKeluarga.change(function(){
					if(HubunganKeluarga.val() == 'Ayah' || HubunganKeluarga.val() == 'Suami')
						radioLaki.prop('checked', true);
					if(HubunganKeluarga.val() == 'Ibu' || HubunganKeluarga.val() == 'Istri')
						radioPerempuan.prop('checked', true);					
				});
				
			}); </script>";
			$output->output.= $js;
			$this-> _outputview($output,$statusKeluarga);
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
 
    function _outputview($output = null,$statusKeluarga)
    {
		$data = array(
			'title' => 'Pengaturan Keluarga',
			'body' => $output
		); 
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		if($statusKeluarga == "B"){
			$this->load->view('DataPribadi/keluargaBesar_view',$data);
		}else{
			$this->load->view('DataPribadi/keluargaInti_view',$data);
		}
    }
	
	function _insertKeluargaBesar($post_array){
		$post_array['JenisKeluarga'] = "B";
		$post_array['NPK'] = $this->NPKSelectedUser;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('keluarga',$post_array);
	}
	
	function _insertKeluargaInti($post_array){
		$post_array['JenisKeluarga'] = "I";
		$post_array['NPK'] = $this->NPKSelectedUser;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('keluarga',$post_array);
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('keluarga',$post_array,array('KodeKeluarga' => $primary_key));
	}
	
	function add_field_callback_hubungankeluargabesar($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedAyah = '';
		$selectedIbu = '';
		$selectedKakak = '';
		$selectedAdik = '';
		
		switch($value)
		{
			case "Ayah": $selectedAyah = 'selected'; break;
			case "Ibu": $selectedIbu = 'selected'; break;
			case "Kakak": $selectedKakak = 'selected'; break;
			case "Adik": $selectedAdik = 'selected'; break;
		}
		
		return ' <select id="HubunganKeluarga" name="HubunganKeluarga">
			<option value = ""></option>
			<option '.$selectedAyah.' value="Ayah">Ayah</option>
			<option '.$selectedIbu.' value="Ibu">Ibu</option>
			<option '.$selectedKakak.' value="Kakak">Kakak</option>
			<option '.$selectedAdik.' value="Adik">Adik</option>
		</select>';
	}
	
	function add_field_callback_keterangan($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedAlmarhum = '';
		$selectedAlmarhumah = '';
		
		switch($value)
		{
			case "Almarhum": $selectedAlmarhum = 'selected'; break;
			case "Almarhumah": $selectedAlmarhumah = 'selected'; break;
		}
		
		return ' <select id="Keterangan" name="Keterangan">
			<option value = ""></option>
			<option '.$selectedAlmarhum.' value="Almarhum">Almarhum</option>
			<option '.$selectedAlmarhumah.' value="Almarhumah">Almarhumah</option>
		</select>';
	}
	
	function add_field_callback_hubungankeluargainti($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedSuami = '';
		$selectedIstri = '';
		$selectedAnak = '';
		
		switch($value)
		{
			case "Suami": $selectedSuami = 'selected'; break;
			case "Istri": $selectedIstri = 'selected'; break;
			case "Anak": $selectedAnak = 'selected'; break;
		}
		
		return ' <select id="HubunganKeluarga" name="HubunganKeluarga">
			<option value = ""></option>
			<option '.$selectedSuami.' value="Suami">Suami</option>
			<option '.$selectedIstri.' value="Istri">Istri</option>
			<option '.$selectedAnak.' value="Anak">Anak</option>
		</select>';
	}
	
	function add_field_callback_jeniskelamin($value = '', $primary_key = null)
	{
		$selectedL = '';
		$selectedP = '';
		switch($value)
		{
			case "L": $selectedL = 'checked'; break;
			case "P": $selectedP = 'checked'; break;
		}
		return '<input id="radio_L" type="radio" name="JenisKelamin" value="L" ' . $selectedL . ' >Laki-laki
			<input id="radio_P" type="radio" name="JenisKelamin" value="P" ' . $selectedP . ' >Perempuan';
	}
	
	function add_field_callback_pendidikan($value = '', $primary_key = null)
	{
		/* return ' <input type="radio" name="DPA" value="Satu" /> Satu
			<input type="radio" name="DPA" value="Dua" /> Dua'; */
		$selectedSD = '';
		$selectedSMP = '';
		$selectedSMA = '';
		$selectedD1 = '';
		$selectedD2 = '';
		$selectedD3 = '';
		$selectedD4 = '';
		$selectedS1 = '';
		$selectedS2 = '';
		$selectedS3 = '';
		
		switch($value)
		{
			case "SD": $selectedSD = 'selected'; break;
			case "SMP": $selectedSMP = 'selected'; break;
			case "SMA": $selectedSMA = 'selected'; break;
			case "D1": $selectedD1 = 'selected'; break;
			case "D2": $selectedD2 = 'selected'; break;
			case "D3": $selectedD3 = 'selected'; break;
			case "D4": $selectedD4 = 'selected'; break;
			case "S1": $selectedS1 = 'selected'; break;
			case "S2": $selectedS2 = 'selected'; break;
			case "S3": $selectedS3 = 'selected'; break;
		}
		
		return ' <select name="Pendidikan">
			<option value = ""></option>
			<option '.$selectedSD.' value="SD">SD - Setara</option>
			<option '.$selectedSMP.' value="SMP">SMP - Setara</option>
			<option '.$selectedSMA.' value="SMA">SMA - Setara</option>
			<option '.$selectedD1.' value="D1">D1</option>
			<option '.$selectedD2.' value="D2">D2</option>
			<option '.$selectedD3.' value="D3">D3</option>
			<option '.$selectedD4.' value="D4">D4</option>
			<option '.$selectedS1.' value="S1">S1</option>
			<option '.$selectedS2.' value="S2">S2</option>
			<option '.$selectedS3.' value="S3">S3</option>
		</select>';
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */