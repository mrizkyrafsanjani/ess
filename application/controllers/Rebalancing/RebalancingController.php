<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class RebalancingController extends CI_Controller{
		var $npkLogin;
		function __construct() {
			parent::__construct();
			
			$this->load->model('Rebalancing_model','',TRUE);
			//$this->load->model('sprebalancing_model','',TRUE);
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
		}
		
		function index() 
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{       
					$data = array(
						'title' => 'Rebalancing'	
					);
					$this->load->helper(array('form','url'));
					$this->template->load('default','Rebalancing/RebalancingFilter_view',$data);
					
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}

		public function ViewData($TglData)
		{
			
			$session_data = $this->session->userdata('logged_in');
			if($this->session->userdata('logged_in')){
				if(check_authorizedByName("Rebalancing"))
				{
					$this->npkLogin = $session_data['npk'];
					$this->_viewDataRebalancing($TglData);
				}
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}	
		}

		

		function _viewDataRebalancing($TglData)
		{
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
			
				//index

				$IndexToday = $this->Rebalancing_model->getIndexIDX($TglData);
				$IndexLastMonth = $this->Rebalancing_model->getIndexIDXLastMonth($TglData);
				$IndexLastYear = $this->Rebalancing_model->getIndexIDXLastYear($TglData);
				$IndexTodayFormat = number_format( $IndexToday	,2,",",".");

				if ( (  $IndexToday	/ $IndexLastMonth ) - 1  < 0 ) 
					$MTDIndex = number_format(((  $IndexToday	/ $IndexLastMonth ) - 1) * 100,2,",",".") .' %';
				else
					$MTDIndex = '+' .number_format(((  $IndexToday	/ $IndexLastMonth ) - 1)*100,2,",",".") . '%';
				
				if ( (  $IndexToday	/ $IndexLastYear ) - 1  < 0 ) 
					$YTDIndex = number_format(((  $IndexToday	/ $IndexLastYear ) - 1)*100,2,",",".") . '%'  ;
				else
					$YTDIndex = '+' .number_format(((  $IndexToday	/ $IndexLastYear ) - 1)*100,2,",",".") . '%' ;

				//LQ45
				
				$LQ45Today = $this->Rebalancing_model->getIndexLQ45($TglData);
				$LQ45LastMonth = $this->Rebalancing_model->getIndexLQ45LastMonth($TglData);
				$LQ45LastYear = $this->Rebalancing_model->getIndexLQ45LastYear($TglData);
				$LQ25TodayFormat = number_format( $LQ45Today,2,",",".") ;
				

				if ( (  $LQ45Today	/ $LQ45LastMonth ) - 1  < 0 ) 
					$MTDLQ45 = number_format(((  $LQ45Today	/ $LQ45LastMonth ) - 1) * 100,2,",",".") .' %';
				else
					$MTDLQ45 = '+' .number_format(((  $LQ45Today	/ $LQ45LastMonth ) - 1)*100,2,",",".") . '%';
				
				if ( (  $IndexToday	/ $IndexLastYear ) - 1  < 0 ) 
					$YTDLQ45 = number_format(((  $LQ45Today	/ $LQ45LastYear ) - 1)*100,2,",",".") . '%'  ;
				else
					$YTDLQ45 = '+' .number_format(((  $LQ45Today	/ $LQ45LastYear ) - 1)*100,2,",",".") . '%' ;

				
				//Yield
				$Yield10Today = $this->Rebalancing_model->getYield10($TglData);
				$Yield10LastMonth = $this->Rebalancing_model->getYield10LastMonth($TglData);
				$Yield10LastYear = $this->Rebalancing_model->getYield10LastYear($TglData);
				$Yield10TodayFormat = number_format( $Yield10Today,2,",",".") . "%" ;

				if ( (  $Yield10Today - $Yield10LastMonth ) * 100  < 0 ) 
					$MTDYield10 = number_format((  $Yield10Today - $Yield10LastMonth ) * 100 ,0,",",".") .' bps';
				else
					$MTDYield10 = '+' .number_format((  $Yield10Today - $Yield10LastMonth ) * 100,0,",",".") . ' bps';
				
				if (   ($Yield10Today - $Yield10LastYear ) * 100  < 0 ) 
					$YTDYield10 = number_format(($Yield10Today - $Yield10LastYear ) * 100,0,",",".") . ' bps'  ;
				else
					$YTDYield10 = '+' .number_format(($Yield10Today - $Yield10LastYear ) * 100,0,",",".") . ' bps' ;


				$Yield3Today = $this->Rebalancing_model->getYield3($TglData);
				$Yield3LastMonth = $this->Rebalancing_model->getYield3LastMonth($TglData);
				$Yield3LastYear = $this->Rebalancing_model->getYield3LastYear($TglData);
				$Yield3TodayFormat = number_format( $Yield3Today,2,",",".") . "%" ;

				if ( (  $Yield3Today - $Yield3LastMonth ) * 100  < 0 ) 
					$MTDYield3 = number_format((  $Yield3Today - $Yield3LastMonth ) * 100 ,0,",",".") .' bps';
				else
					$MTDYield3 = '+' .number_format((  $Yield3Today - $Yield3LastMonth ) * 100,0,",",".") . ' bps';
				
				if (   ($Yield3Today - $Yield3LastYear ) * 100  < 0 ) 
					$YTDYield3 = number_format(($Yield3Today - $Yield3LastYear ) * 100,0,",",".") . ' bps'  ;
				else
					$YTDYield3 = '+' .number_format(($Yield3Today - $Yield3LastYear ) * 100,0,",",".") . ' bps' ;

				//rate
				$RateBIToday = $this->Rebalancing_model->getRateBI($TglData);
				$RateBILastMonth = $this->Rebalancing_model->getRateBILastMonth($TglData);
				$RateBILastYear = $this->Rebalancing_model->getRateBILastYear($TglData);
			

				if ( (  $RateBIToday - $RateBILastMonth ) * 100  < 0 ) 
					$MTDRateBI = number_format((  $RateBIToday - $RateBILastMonth ) * 100 ,0,",",".") .' bps';
				else
					$MTDRateBI = '+' .number_format((  $RateBIToday - $RateBILastMonth ) * 100,0,",",".") . ' bps';
				
				if (   ($RateBIToday - $RateBILastYear ) * 100  < 0 ) 
					$YTDRateBI = number_format(($RateBIToday - $RateBILastYear ) * 100,0,",",".") . ' bps'  ;
				else
					$YTDRateBI = '+' .number_format(($RateBIToday - $RateBILastYear ) * 100,0,",",".") . ' bps' ;

				$RateKursToday = $this->Rebalancing_model->getIDR($TglData);
				$RateKursLastMonth = $this->Rebalancing_model->getIDRLastMonth($TglData);
				$RateKursLastYear = $this->Rebalancing_model->getIDRLastYear($TglData);
				$RateKursTodayFormat = number_format( $RateKursToday,2,",",".") ;
		

				if ( (  $RateKursToday	/ $RateKursLastMonth ) - 1  < 0 ) 
					$MTDRateKurs = number_format(((  $RateKursToday	/ $RateKursLastMonth ) - 1) * 100,2,",",".") .' %';
				else
					$MTDRateKurs = '+' .number_format(((  $RateKursToday	/ $RateKursLastMonth ) - 1)*100,2,",",".") . '%';
				
				if ( (  $IndexToday	/ $IndexLastYear ) - 1  < 0 ) 
					$YTDRateKurs = number_format(((  $RateKursToday	/ $RateKursLastMonth ) - 1)*100,2,",",".") . '%'  ;
				else
					$YTDRateKurs = '+' .number_format(((  $RateKursToday	/ $RateKursLastMonth ) - 1)*100,2,",",".") . '%' ;

				//data aset dpa1
				/*$assetDPA1 = $this->Rebalancing_model->getDataAset($TglData,'1');
				
				if($assetDPA1){
					$assetDPA1_array = array();
					foreach($assetDPA1 as $row){
						$assetDPA1_array[] = array(
							'Instruments' => $row->Instruments,
							'JumKemarin' => $row->Jumkemarin
						);
					}
				}else{
					echo 'gagal111';
				}*/

				// $dbname='01/01/2019';
				// //$outp=0.0;
				// $params = array( 
				// 	array($dbname, SQLSRV_PARAM_IN)				
				// );       
				// $result["sprdata"]=$this->sprebalancing_model->sqlsrv_runSP("sp_get_all_AsetDPA1",$params);
				// $result["sprdata"]=array($dbname);
				// //$this->load->view('sptest',$result);


				
				$data = array(
					'title' => 'Rebalancing',
					'IndexToday' => $IndexTodayFormat,
					'MTDIndex' => $MTDIndex,
					'YTDIndex' => $YTDIndex,
					'LQ45Today' => $LQ25TodayFormat,
					'MTDLQ45' => $MTDLQ45,
					'YTDLQ45' => $YTDLQ45,
					'Yield10Today' => $Yield10TodayFormat,
					'MTDYield10' => $MTDYield10,
					'YTDYield10' => $YTDYield10,
					'Yield3Today' => $Yield3TodayFormat,
					'MTDYield3' => $MTDYield3,
					'YTDYield3' => $YTDYield3,
					'RateBIToday' => $RateBIToday,
					'MTDRateBI' => $MTDRateBI,
					'YTDRateBI' => $YTDRateBI,
					'RateKursToday' => $RateKursTodayFormat,
					'MTDRateKurs' => $MTDRateKurs,
					'YTDRateKurs' => $YTDRateKurs
				);

			//	$data['listDPA1']=$this->Rebalancing_model->tampil();

				$this->load->helper(array('form','url'));			
			//	$this->load->view('Rebalancing/Rebalancing_view',$data);
			$this->template->load("default","Rebalancing/RebalancingFilter_View",$data);
				
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}

		function simpanInvestmentPlan(){
			try{
				$sukses = "Data berhasil disimpan!";
				$data = $_POST['data'];
				if(!$this->Rebalancing_model->insertInvestmentPlan($data,$this->npkLogin)){
					$sukses = "Proses simpan gagal!";
				}
				echo $sukses;
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
				echo "Error!";
			}
		}
	}
		
		
	
?>