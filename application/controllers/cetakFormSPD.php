<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class CetakFormSPD extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('number');
			$this->load->model('menu','',TRUE);
			$this->load->model('trkSPD','',TRUE);
			$this->load->model('pelimpahanwewenang_model','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}else{
					echo 'gagal';
				}
				//$trkSPD = $this->trkSPD->getSPDbyNPK($session_data['npk']);
				$get_noSPD = $this->input->get('NoSPD');
				$trkSPD = $this->trkSPD->getSPDbyNoSPD($get_noSPD);
				if($trkSPD){
					$trkSPD_array = array();
					foreach($trkSPD as $row){
						$trkSPD_array[] = array(
							'DPA' => $row->DPA,
							'uangmuka' => $row->UangMuka,
							'nospd' => $row->NoSPD,
							'tanggalspd' => $row->TanggalSPD,
							'npk' => $row->NPK,
							 'golongan' => $row->golongan,
							 'nama'=> $row->nama,
							 'jabatan'=> $row->jabatan,
							 'departemen'=> $row->departemen,
							 'atasan'=> $row->atasan,
							 'tanggalberangkatspd'=> $row->TanggalBerangkatSPD,
							 'tanggalkembalispd'=> $row->TanggalKembaliSPD,
							 'tujuan'=> $row->Tujuan,
							 'alasan'=> $row->AlasanPerjalanan,
							 'deskripsi'=> $row->Deskripsi,
							 'keteranganspd'=> $row->KeteranganSPD,
							 'bebanharianspd'=> $row->BebanHarianSPD,
							 'jumlahharispd'=> $row->JumlahHariSPD,
							 'totalspd'=> $row->TotalSPD
						);
					}
				}else{
					echo 'gagal2';
				}
				/*
				$data = array(
					   'npk' => $session_data['npk'],
					   'nama' => $session_data['nama'],
					   'menu_array' => $menu_array
				  );
				  */
				/*data pelimpahan wewenang jika ada*/
						//data yang di limpahkan
				$hasilPW = $this->pelimpahanwewenang_model->getHeaderPelimpahanWewenang($get_noSPD);
				$KodePelimpahanWewenang = $hasilPW['KodePelimpahanWewenang'];
				$JenisTransaksi = $hasilPW['JenisTransaksi'];

				$dataPelimpahanWewenang = $this->pelimpahanwewenang_model->getDetailPelimpahanByNoTransaksi($KodePelimpahanWewenang);						
				if($dataPelimpahanWewenang)
				{
					foreach($dataPelimpahanWewenang as $row)
					{	
						$dataPelimpahanWewenangDetail[] = array(
							'KodePelimpahanWewenang' => $row->KodePelimpahanWewenang,
							'Keterangan' => $row->Keterangan,
							'NPKYangDilimpahkan' => $row->NPKYangDilimpahkan,
							'NamaYangDilimpahkan' => $row->NamaYangDilimpahkan
						);		
					}
				}

						//email untuk di forward
				$dataEmailPelimpahanWewenang = $this->pelimpahanwewenang_model->getEmailPelimpahanByNoTransaksi($KodePelimpahanWewenang);						
				if($dataEmailPelimpahanWewenang)
				{
					foreach($dataEmailPelimpahanWewenang as $row)
					{	
						$dataEmailPelimpahanWewenangDetail[] = array(
						'Nama' => $row->Nama,
						'EmailYangDiTuju' => $row->EmailYangDiTuju
						);		
					}
				}

						
				if($dataPelimpahanWewenang && $dataEmailPelimpahanWewenang) //// SPD + pelimpahan wewenang + email
				{
							$data2 = array(
								'title' => 'Cetak Form SPD',
								'trkSPD_array' => $trkSPD_array,
								'terbilang' => terbilang($trkSPD_array[0]['totalspd']),
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'dataPelimpahanWewenangDetail'=>$dataPelimpahanWewenangDetail,
								'dataEmailPelimpahanWewenangDetail'=>$dataEmailPelimpahanWewenangDetail,
								'KodePelimpahanWewenang'=>$KodePelimpahanWewenang,
								'flagemail' => '2'
							);
				}
				else	
				if ($dataPelimpahanWewenang) //// SPD + pelimpahan wewenang saja
				{
							$data2 = array(
								'title' => 'Cetak Form SPD',
								'trkSPD_array' => $trkSPD_array,
								'terbilang' => terbilang($trkSPD_array[0]['totalspd']),
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'dataPelimpahanWewenangDetail'=>$dataPelimpahanWewenangDetail,
								'KodePelimpahanWewenang'=>$KodePelimpahanWewenang,
								'flagemail' => '1'
							);
				}
				else
				{ // SPD biasa							

							$data2 = array(
								'title' => 'Cetak Form SPD',
								'trkSPD_array' => $trkSPD_array,
								'terbilang' => terbilang($trkSPD_array[0]['totalspd']),
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'flagemail' => '0'
							);
				}					
				

				$this->load->helper(array('form','url'));
				//$this->load->view('home_view', $data);
				$this->template->load('default','cetakFormSPD_view',$data2);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
	}
?>