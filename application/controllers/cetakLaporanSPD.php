<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class CetakLaporanSPD extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('number');
			$this->load->model('menu','',TRUE);
			$this->load->model('trkSPD','',TRUE);
			$this->load->model('uangmuka_model','',TRUE);
			$this->load->model('user','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}else{
					echo 'gagal';
				}
				
				$noSPD = $this->input->get('NoSPD');
				$trkSPD = $this->trkSPD->getSPDbyNoSPD($noSPD);
				
				if($trkSPD){
					$trkSPD_array = array();
					foreach($trkSPD as $row){
						$trkSPD_array[] = array(
							'DPA' => $row->DPA,
							'uangmuka' => $row->UangMuka,
							'nospd' => $row->NoSPD,
							'tanggallap' => $row->TanggalLap,
							'npk' => $row->NPK,
							 'golongan' => $row->golongan,
							 'nama'=> $row->nama,
							 'jabatan'=> $row->jabatan,
							 'departemen'=> $row->departemen,
							 'atasan'=> $row->atasan,
							 'tanggalberangkatlap'=> $row->TanggalBerangkatLap,
							 'tanggalkembalilap'=> $row->TanggalKembaliLap,
							 'tanggalkembalikantorlap'=> $row->TanggalKembaliKantorLap,
							 'tujuan'=> $row->Tujuan,
							 'alasan'=> $row->AlasanPerjalanan,
							 'deskripsi'=> $row->Deskripsi,
							 'keteranganlap'=> $row->KeteranganLap,
							 'bebanharianlap'=> $row->BebanHarianLap,
							 'jumlahharilap'=> $row->JumlahHariLap,
							 'totalspd'=>$row->TotalSPD,
							 'totallap'=>$row->TotalLap
						);
					}
				}else{
					echo 'gagal2';
				}
				$UangMuka=1;
				$joinUangMuka=$this->trkSPD->joinUangMuka($noSPD);
				if($joinUangMuka){
					$joinUangMuka_array = array();
					foreach($joinUangMuka as $rows){
						$joinUangMuka_array = array(
							'Nouangmuka' => $rows->Nouangmuka,
							 'WaktuPenyelesaianTerlambat' => $rows->WaktuPenyelesaianTerlambat,
							 'noPP'=> $rows->NoPP,
							 'TanggalPermohonan'=> $rows->TanggalPermohonan,
							 'TanggalRealisasi'=> $rows->TanggalRealisasi,
							 'KeteranganPermohonan'=> $rows->KeteranganPermohonan,
							 'DPA' => $rows->DPA,
							 'TipeKembaliUangMuka'=> $rows->TipeKembaliUangMuka,
							 'BankKembaliUangMuka'=> $rows->BankKembaliUangMuka,
							 'NoRekKembaliUangMuka'=> $rows->NoRekKembaliUangMuka,
							 'NmPenerimaUangMuka'=> $rows->NmPenerimaUangMuka
									 
						);
					}
				}else{
					$UangMuka=0;
				}
				/*
				$data = array(
					   'npk' => $session_data['npk'],
					   'nama' => $session_data['nama'],
					   'menu_array' => $menu_array
				  );
				  */
				  $query2 = $this->db->get_where('globalparam',array('Name'=>'DIC_ACC'));
				  foreach($query2->result() as $row)
				  {
					  $npkDICACC = $row->Value;
				  }
  
				  $query2 = $this->db->get_where('globalparam',array('Name'=>'DIC_HRGA'));
				  foreach($query2->result() as $row)
				  {
					  $npkDICHRGA = $row->Value;
				  }
				  
				  foreach($this->user->findUserByNPK($npkDICHRGA) as $row)
				  {
					  $dicHrga = $row->Nama;
				  }
				  foreach($this->user->findUserByNPK($npkDICACC) as $row)
				  {
					  $dicAccounting = $row->Nama;
				  }
				  $headHrga = $this->user->getSingleUserBasedJabatan("HRGA Dept Head")->Nama;
				  //$dicHrga = $this->user->getSingleUserBasedJabatan("Director DPA 1")->Nama;
				  //$dicAccounting = $this->user->getSingleUserBasedJabatan("President Director DPA 2")->Nama;
  
				  
				$selisih = $trkSPD_array[0]['totallap']-$trkSPD_array[0]['totalspd'];
				if($selisih < 0){
					$selisih = $selisih*-1;
				} 
				if ($UangMuka<>0){
				$dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
				if ($dataOutstanding)
				{
					foreach($dataOutstanding as $row)
					{	
						$dataOutstandingDetail[] = array(
							'NoUangMuka' => $row->NoUangMuka,
							'KeteranganPermohonan' => $row->KeteranganPermohonan,
							'Total' => $row->Total,
							'TotalSPD' => $row->TotalSPD,
							'ReasonOutStanding' => $row->ReasonOutStanding
						);		
					}
				}
				if($dataOutstanding){
				$data2 = array(
					'title' => 'Cetak Laporan SPD',
					'trkSPD_array' => $trkSPD_array,
					'terbilang' => terbilang($selisih),
					'npk' => $session_data['npk'],
						'nama' => $session_data['nama'],
					'menu_array' => $menu_array,
					'joinUangMuka_array' => $joinUangMuka_array,
					'flagOutStanding' => '1',
					'dataOutstanding' =>$dataOutstandingDetail,
					'UangMuka' => $UangMuka
					);
				}else{
					$data2 = array(
						'title' => 'Cetak Laporan SPD',
						'trkSPD_array' => $trkSPD_array,
						'terbilang' => terbilang($selisih),
						'npk' => $session_data['npk'],
					  'nama' => $session_data['nama'],
						'menu_array' => $menu_array,
						'joinUangMuka_array' => $joinUangMuka_array,
						'dataOutstanding' => '0',
						'flagOutStanding' => '0',
						'UangMuka' => $UangMuka
					);
				}
			}else{
				$data2 = array(
					'title' => 'Cetak Laporan SPD',
					'trkSPD_array' => $trkSPD_array,
					'terbilang' => terbilang($selisih),
					'npk' => $session_data['npk'],
					'nama' => $session_data['nama'],
					'menu_array' => $menu_array,
					'UangMuka' => $UangMuka
				);
			}
				  $data2['HeadHRGA'] = $headHrga;
				  $data2['DICHRGA'] = $dicHrga;
				  $data2['DICACC'] = $dicAccounting;
				$this->load->helper(array('form','url'));
				//$this->load->view('home_view', $data);
				$this->template->load('default','cetakLaporanSPD_view',$data2);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
	}
?>