<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class LDArchive extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->model('ldarchive_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
	public function index()
	{
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("319"))
            {
                $this->npkLogin = $session_data['npk'];
				$this->_LDArchive();
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }	
	
	
	public function _LDArchive($dpa='',$tahun='',$jenis='',$katakunci='',$page='')
    {
        $session_data = $this->session->userdata('logged_in');

		$this->npkLogin = $session_data['npk'];
		$crud = new grocery_crud();
		$crud->set_subject('Dokumen Legal');
		//$crud->set_theme('datatables');
		
        $crud->set_table('mstrdokumenlegal');
        $crud->where('mstrdokumenlegal.deleted','0');
        
		if($dpa != '')
		{
			$crud->where("mstrdokumenlegal.dpa", $dpa);
		}
		if($tahun != '')
		{
			$crud->where('mstrdokumenlegal.tahun',$tahun);				
		}
		if($jenis != '')
		{
			$crud->where('mstrdokumenlegal.kodejenis',$jenis);
		}
		if($katakunci != '')
		{
			$crud->like('mstrdokumenlegal.keyword', $katakunci);
		}

		switch($page)
			{
				case "add":
					break;
				case "view":
					$crud->unset_add();
					break;
				default:
					break;
			}	
		$query = $this->db->get_where('globalparam',array('Name'=>'Admin_DokumenLegal'));
		foreach($query->result() as $row)
		{
			$npkAdminDokumenLegal = $row->Value;
		}
		if($this->user->isAdmin($this->npkLogin)){
			//Admin DPA
			
		}else if(strpos($npkAdminDokumenLegal, $this->npkLogin) != false){
			//berarti dia admin DokumenLegal
		}else{
			//brarti user biasa / PIC
			$crud->unset_edit();
			$crud->unset_delete();
		}

		$crud->set_relation('KodeJenis','jenisdokumenlegal','JenisDokumenLegal',array('deleted' => '0'));
        
		$crud->set_relation_n_n('PIC','picdokumenlegal','mstruser','KodeDokumen','NPK','Nama');
		
		//$crud->set_relation('NPKYangDilimpahkan','mstruser','Nama',array('Deleted' => 0,'TanggalBerhenti' => '0000-00-00'));
		$state = $crud->getState();
		
		$crud->unset_back_to_list();
		$crud->set_relation_n_n('PeraturanTerkait','peraturanterkaitdokumenlegal','mstrdokumenlegal','KodeDokumenParent','KodeDokumen','{DocumentName} DPA {DPA}', null, array('mstrdokumenlegal.deleted'=>0));

		$crud->columns('KodeJenis','DPA','Tahun','DocumentNo', 'DocumentName', 'Keyword', 'PIC','TanggalPengesahan','TanggalMulaiBerlaku','TanggalKadaluarsa','Status','DokumenYangDiganti', 'PeraturanTerkait');
		
		$crud->callback_column('PeraturanTerkait', array($this,'_callback_peraturan_terkait'));
		$crud->callback_column('TanggalPengesahan', array($this,'_callback_formatTanggalPengesahan'));
		$crud->callback_column('TanggalMulaiBerlaku', array($this,'_callback_formatTanggalMulaiBerlaku'));
		$crud->callback_column('TanggalKadaluarsa', array($this,'_callback_formatTanggalKadaluarsa'));
		// $crud->set_relation('DokumenYangDiganti','mstrdokumenlegal','{DocumentName} DPA {DPA}',array('deleted' => '0'));
		$crud->set_relation('DokumenYangDiganti','mstrdokumenlegal','{DocumentName} DPA {DPA}',array('deleted' => '0'));

		// $crud->callback_column('DokumenYangDiganti', array($this,'_callback_dokumen_yang_diganti'));
		$crud->callback_column($this->unique_field_name('DokumenYangDiganti'), array($this,'_callback_dokumen_yang_diganti'));
		$crud->callback_column('DocumentName', array($this,'_callback_show_document'));
		$crud->fields('FilePath','KodeJenis','DocumentNo','DocumentName','DPA','Tahun','Keyword','PIC','TanggalPengesahan','TanggalMulaiBerlaku','TanggalKadaluarsa','Status','DokumenYangDiganti', 'PeraturanTerkait');
		$crud->required_fields('FilePath','KodeJenis','DocumentNo','DocumentName','DPA','Tahun','Keyword','PIC','TanggalPengesahan','TanggalMulaiBerlaku','Status');
		$crud->set_field_upload('FilePath','./assets/uploads/files/Dokumen Legal',"pdf|doc|docx|xls|xlsx");
        $crud->change_field_type('Status', 'true_false');
        $crud->field_type('DPA','dropdown',
        array('1' => '1', '2' => '2'));
        $crud->display_as('DocumentNo','Nomor');
        $crud->display_as('DocumentName','Nama Dokumen');
        $crud->display_as('FilePath','File Dokumen');
        $crud->display_as('KodeJenis','Jenis Dokumen');
        $crud->display_as('DokumenYangDiganti','Dokumen Sebelum');
		$crud->display_as('PeraturanTerkait','Peraturan Terkait');
		$crud->callback_after_insert(array($this,'_after_insert_dokumenlegal'));
		$crud->callback_after_update(array($this,'_after_update_dokumenlegal'));
		$crud->callback_delete(array($this,'_delete_dokumenlegal'));
		$crud->set_lang_string('insert_success_message',
		'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
		<script type="text/javascript">
		window.location = "'.$this->config->base_url().'index.php/LDArchive/LDArchiveController/";
		</script>
		<div style="display:none">');
		$crud->set_lang_string('update_success_message',
		'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
		<script type="text/javascript">
		window.location = "'.$this->config->base_url().'index.php/LDArchive/LDArchive/searchAndViewDokumenLegal/";
		</script>
		<div style="display:none">');
        $output = $crud->render();
        $this-> _outputview($output, $page);      
	}
	function unique_field_name($field_name) {
	    return 's'.substr(md5($field_name),0,8); //This s is because is better for a string to begin with a letter and not with a number
    }

    public function addDokumenLegal($dpa='',$tahun='',$jenis='',$katakunci='', $page='')
    {
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("319"))
            {
                $this->npkLogin = $session_data['npk'];
                if($dpa == 'non'){
				    $dpa = '';
                }if($tahun == 'non'){
					$tahun = '';
                }if($jenis == 'non'){
					$jenis = '';
                }if($katakunci == 'non'){
                    $katakunci = '';
                }
				$this->_LDArchive($dpa, $tahun, $jenis, $katakunci, 'add');
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}	
	public function searchAndViewDokumenLegal($dpa='',$tahun='',$jenis='',$katakunci='', $page='')
    {
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("319"))
            {
                $this->npkLogin = $session_data['npk'];
                if($dpa == 'non'){
				    $dpa = '';
                }if($tahun == 'non'){
					$tahun = '';
                }if($jenis == 'non'){
					$jenis = '';
                }if($katakunci == 'non'){
                    $katakunci = '';
                }
				$this->_LDArchive($dpa, $tahun, $jenis, $katakunci, 'view');
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}	
	
    function _outputview($output = null, $page = '')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Dokumen Legal',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		if($page == 'view')
		{
			$this->load->view('LDArchive/LDArchive_iframe',$data);
		}
		else
		{		
			$this->template->load('default','templates/CRUD_view',$data);
		}
    }
	
	function _delete_dokumenlegal($primary_key){
		return $this->db->update('mstrdokumenlegal',array('deleted' => '1'),array('KodeDokumen' => $primary_key));
	}

	function _callback_peraturan_terkait($value, $row){
		$rs = $this->db->query("Select KodeDokumen from peraturanterkaitdokumenlegal where KodeDokumenParent = '$row->KodeDokumen'");
		if($result = $rs->result()){
			$string = '';
			$counter = 1;
			foreach($result as $row2){
				$rs2 = $this->db->query("Select DocumentName from mstrdokumenlegal where KodeDokumen = '$row2->KodeDokumen'");
				$result2 = $rs2->result();
				foreach($result2 as $row3){
					if($counter == count($result)){
						$string .= "<a target='_parent' href='".site_url('LDArchive/LDArchiveController/read/'.$row2->KodeDokumen)."'>$row3->DocumentName</a>";
					}else{
						$string .= "<a target='_parent' href='".site_url('LDArchive/LDArchiveController/read/'.$row2->KodeDokumen)."'>$row3->DocumentName</a>".", ";
					}
					$counter++;
				}
			}
			return $string;
		}
	
	}

	function _callback_show_document($value, $row)
	{
		return "<a target='_parent' href='".site_url('LDArchive/LDArchiveController/read/'.$row->KodeDokumen)."'>$value</a>";
	}
	
	function _callback_dokumen_yang_diganti($value, $row){
		$rs2 = $this->db->query("Select DocumentName from mstrdokumenlegal where KodeDokumen = '$row->DokumenYangDiganti'");
		$result2 = $rs2->result();
		$string = '';
		foreach($result2 as $row3){
			$string = "<a target='_parent' href='".site_url('LDArchive/LDArchiveController/read/'.$row->DokumenYangDiganti)."'>$row3->DocumentName</a>";
		}
		return $string;
	}
	function _callback_formatTanggalPengesahan($value, $row){
	
		return substr($value, 8,2).'-'.substr($value, 5,2).'-'.substr($value, 0,4);
	}
	function _callback_formatTanggalMulaiBerlaku($value, $row){
	
		return substr($value, 8,2).'-'.substr($value, 5,2).'-'.substr($value, 0,4);
	}
	function _callback_formatTanggalKadaluarsa($value, $row){
	
		return substr($value, 8,2).'-'.substr($value, 5,2).'-'.substr($value, 0,4);
	}
	function _after_insert_dokumenlegal($post_array,$primary_key)
	{
		$createDate = array(
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s')
		);
		fire_print('log',print_r($post_array,true));
		$this->db->update('mstrdokumenlegal',$createDate,array('KodeDokumen'=>$primary_key));
		$dokumenSebelum = $this->ldarchive_model->dataDokumen($primary_key);
		$dokumenYangDiganti = $this->ldarchive_model->dataDokumen($dokumenSebelum[0]->DokumenYangDiganti);
		
		if($dokumenYangDiganti){
			$this->db->query('UPDATE mstrdokumenlegal SET Status = 0 where KodeDokumen = '.$dokumenSebelum[0]->DokumenYangDiganti);
		}
		//$this->db->query('UPDATE mstrdokumenlegal SET OneMonthReminder = DATE_SUB(TanggalKadaluarsa, INTERVAL 30 DAY), ThreeMonthReminder = DATE_SUB(TanggalKadaluarsa, INTERVAL 90 DAY) where KodeDokumen = '.$primary_key);
		return true;
	}
    function _after_update_dokumenlegal($post_array,$primary_key)
	{
		$updateDate = array(
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s')
		);
		fire_print('log',print_r($post_array,true));
		fire_print('log','pk dokumen_legal : '.$primary_key);
		$dokumenSebelum = $this->ldarchive_model->dataDokumen($primary_key);
		$dokumenYangDiganti = $this->ldarchive_model->dataDokumen($dokumenSebelum[0]->DokumenYangDiganti);
		
		if($dokumenYangDiganti){
			$this->db->query('UPDATE mstrdokumenlegal SET Status = 0 where KodeDokumen = '.$dokumenSebelum[0]->DokumenYangDiganti);
		}
		$this->db->update('mstrdokumenlegal',$updateDate,array('KodeDokumen'=>$primary_key));
		//$this->db->query('UPDATE mstrdokumenlegal SET OneMonthReminder = DATE_SUB(TanggalKadaluarsa, INTERVAL 30 DAY), ThreeMonthReminder = DATE_SUB(TanggalKadaluarsa, INTERVAL 90 DAY) where KodeDokumen = '.$primary_key);
		return true;
    }
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */