<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PengaturanJenisDokumenLegal extends CI_Controller {
    var $npkLogin;
    function __construct()
    {
        parent::__construct();
        $this->load->model('menu','',TRUE);
        $this->load->library('grocery_crud');
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("319"))
            {
                $this->npkLogin = $session_data['npk'];
                $this->_jenis();
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }

    public function _jenis()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Jenis');
		//$crud->set_theme('datatables');
		
        $crud->set_table('jenisdokumenlegal');
		$crud->set_relation('CreatedBy','mstruser','Nama',array('deleted' => '0'));
		$crud->where('jenisdokumenlegal.deleted','0');
		
		
		$crud->columns('KodeJenisDokumenLegal', 'JenisDokumenLegal', 'CreatedBy');
		$crud->fields('JenisDokumenLegal');
		
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete'));		
				
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');		
		
		$data = array(
				'title' => 'Pengaturan Jenis Dokumen Legal',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
    	//$this->load->view('LDArchive/pengaturanJenis_view',$output);    
    }
	
	function _insert($post_array){
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');		
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('jenisdokumenlegal',$post_array);
	}
	
	function _update($post_array,$primary_key)
	{
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');		
		$post_array['UpdatedBy'] = $this->npkLogin;		
		$this->db->update('jenisdokumenlegal',$post_array,array('KodeJenisDokumenLegal' => $primary_key));
		return true;
	}
	
	function _delete($primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');		
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['Deleted'] = '1';
		return $this->db->update('jenisdokumenlegal',$post_array,array('KodeJenisDokumenLegal' => $primary_key));
	}

}

/* End of file main.php */
/* Location: ./application/controllers/main.php */
/**
 * Created by PhpStorm.
 * User: gerry
 * Date: 4/18/2018
 * Time: 11:30 AM
 */