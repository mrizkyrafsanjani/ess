<?php  
class NotifDokumen extends CI_Controller {
    var $npkLogin;
    function __construct()
    {
        parent::__construct();
        $this->load->model('menu','',TRUE);
        $this->load->model('user','',TRUE);
        $this->load->model('ldarchive_model','',TRUE);
		$this->load->library('email');
        $this->load->library('grocery_crud');
    }

    public function index()
    {
		//$this->test();
		$this->notifikasiReminderDokumen();
		$this->notifikasiReminderLaporan();
	}

	
	public function notifikasiReminderDokumen()
	{
		$searchAllDokumen = 'SELECT KodeDokumen, DocumentName, TanggalKadaluarsa 
		from mstrdokumenlegal where deleted = 0 and status = 1';
		$allDokumen = $this->db->query($searchAllDokumen)->result();
		if($allDokumen){
			foreach($allDokumen as $dokumen){
				//> 3 bulan, do nothing
				//> 1 <= 3 bulan email 1x/30 hari
				//< 1 bulan email 1x/7 hari
				//flag email
				$flag1Bulan = false;
				$flag3Bulan = false;
				$flagBolehEmail = false;
				$today = date("Y-m-d");
				$TanggalKadaluarsa = $dokumen->TanggalKadaluarsa;
				$oneMonthReminder = strtotime($TanggalKadaluarsa.'-1 months');
				$oneMonthReminder = date("Y-m-d", $oneMonthReminder);
				$threeMonthReminder = strtotime($TanggalKadaluarsa.'-3 months');
				$threeMonthReminder = date("Y-m-d", $threeMonthReminder);
				var_dump($dokumen);
				if($TanggalKadaluarsa <= $today){
					//expired nih
					echo 'expired nih';
				}else if($threeMonthReminder <= $today){
					if($oneMonthReminder <= $today){
						echo 'masa 1 bulan sebelum';
						$flag1Bulan = true;
					}else{
						echo 'masa 3 bulan sebelum';
						$flag3Bulan = true;
					}
				}else{
					echo 'belum masuk masa reminder';
				}
				if($today == $threeMonthReminder || $today == $oneMonthReminder){
					$flagBolehEmail = true;
				}else if($flag3Bulan){
					$Hmin2Bulan = strtotime($threeMonthReminder.'+1 months');
					$Hmin2Bulan = date("Y-m-d", $Hmin2Bulan);
					echo $Hmin2Bulan;
					if($today == $Hmin2Bulan){
						$flagBolehEmail = true;
					}
				}else if($flag1Bulan){
					$hmin1minggu = strtotime($TanggalKadaluarsa.'-1 weeks');
					$hmin1minggu = date("Y-m-d", $hmin1minggu);
					if($today == $hmin1minggu){
						$flagBolehEmail = true;
					}
					$hmin2minggu = strtotime($TanggalKadaluarsa.'-2 weeks');
					$hmin2minggu = date("Y-m-d", $hmin2minggu);
					if($today == $hmin2minggu){
						$flagBolehEmail = true;
					}
					$hmin3minggu = strtotime($TanggalKadaluarsa.'-3 weeks');
					$hmin3minggu = date("Y-m-d", $hmin3minggu);
					if($today == $hmin3minggu){
						$flagBolehEmail = true;
					}
				}

				if($flagBolehEmail){
					$dokumenIni = $this->ldarchive_model->dataDokumen($dokumen->KodeDokumen);
					$picdokumenIni = $this->ldarchive_model->picDokumen($dokumen->KodeDokumen);
			
					$query = $this->db->get_where('globalparam',array('Name'=>'Admin_DokumenLegal'));
					foreach($query->result() as $row)
					{
						$npkAdminDokumenLegal = explode(', ',$row->Value);
					}
					foreach($npkAdminDokumenLegal as $user){
						$legalbpanalyst = $this->user->dataUser($user);
						
						$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
						$this->email->to($legalbpanalyst[0]->email); 
		
						$this->email->subject('Notifikasi Kadaluarsa Dokumen Legal!');
						if($legalbpanalyst[0]->KodeRole == 4){
							$message = 'Dear Corporate Secretary Leader,<br/><br/>';
						}else{
							$message = 'Dear Legal & BP Analyst,<br/><br/>';
						}
						$message .= 'Sehubungan dengan akan berakhirnya '.$dokumenIni[0]->DocumentName.', maka diharapkan untuk dilakukan update dokumen tersebut sebelum tanggal jatuh tempo<br/><br/>
						Dokumen kadarluarsa pada tanggal : '.$dokumenIni[0]->TanggalKadaluarsa.'<br/><br/>
						http://DPA2/ESS/index.php/LDArchive/LDArchiveController/<br/><br/>
						Terima kasih<br/><br/>
						ESS';
						// $message .= $dokumenIni[0]->Namadokumen.', sudah diupload ke dalam system pada '.$dokumenIni[0]->UpdatedOn.'<br/><br/>
						// 			'.$dokumenIni[0]->Namadokumen.' jatuh tempo pada tanggal : '.$dokumenIni[0]->TanggalKadaluarsa.'<br/><br/>
						// 			https://DPA/ESS/index.php/LDArchive/LDArchiveController/<br/><br/>
						// 			Terima kasih<br/><br/>
						// 			ESS';
						$this->email->message($message);	
						$this->email->send();
					}
					foreach($picdokumenIni as $pic){
						$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
						$this->email->to($pic->EmailInternal); 
						$this->email->subject('Notifikasi Kadaluarsa Dokumen Legal!');
						$message = 'Dear PIC,<br/><br/>';
						$message .= 'Sehubungan dengan akan berakhirnya '.$dokumenIni[0]->DocumentName.', maka diharapkan untuk dilakukan update dokumen tersebut sebelum tanggal jatuh tempo<br/><br/>
						Dokumen kadarluarsa pada tanggal : '.$dokumenIni[0]->TanggalKadaluarsa.'<br/><br/>
						http://DPA2/ESS/index.php/LDArchive/ReminderdokumenController<br/><br/>
						Terima kasih<br/><br/>
						ESS';
						
						$this->email->message($message);	
						$this->email->send();
					}
				}
			}
			
			$message= 'email expired date dokumen legal';
			$this->reportIT($message);
		}else{
			$message= 'tidak email expired date dokumen legal';
			$this->reportIT($message);
		}
		
	}
	public function notifikasiReminderLaporan()
	{
		$searchAllLaporan = 'SELECT KodeLaporan, NamaLaporan, TanggalBatasAkhirPenyampaian 
		from mstrreminderlaporan where deleted = 0 and FilePath IS NULL';
		$allLaporan = $this->db->query($searchAllLaporan)->result();
		if($allLaporan){
			foreach($allLaporan as $laporan){
				//> 3 bulan, do nothing
				//> 1 <= 3 bulan email 1x/30 hari
				//< 1 bulan email 1x/7 hari
				//flag email
				$flag1Bulan = false;
				$flag3Bulan = false;
				$flagBolehEmail = false;
				$today = date("Y-m-d");
				$TanggalBatasAkhirPenyampaian = $laporan->TanggalBatasAkhirPenyampaian;
				$oneMonthReminder = strtotime($TanggalBatasAkhirPenyampaian.'-1 months');
				$oneMonthReminder = date("Y-m-d", $oneMonthReminder);
				$threeMonthReminder = strtotime($TanggalBatasAkhirPenyampaian.'-3 months');
				$threeMonthReminder = date("Y-m-d", $threeMonthReminder);
				var_dump($laporan);
				if($TanggalBatasAkhirPenyampaian <= $today){
					$flagBolehEmail = true;
				}else if($threeMonthReminder <= $today){
					if($oneMonthReminder <= $today){
						echo 'masa 1 bulan sebelum';
						$flag1Bulan = true;
					}else{
						echo 'masa 3 bulan sebelum';
						$flag3Bulan = true;
					}
				}else{
					echo 'belum masuk masa reminder';
				}
				if($today == $threeMonthReminder || $today == $oneMonthReminder){
					$flagBolehEmail = true;
				}else if($flag3Bulan){
					$Hmin2Bulan = strtotime($threeMonthReminder.'-1 months');
					$Hmin2Bulan = date("Y-m-d", $Hmin2Bulan);
					if($today == $Hmin2Bulan){
						$flagBolehEmail = true;
						echo 'hmin2bulan';
					}
				}else if($flag1Bulan){
					$hmin1minggu = strtotime($TanggalBatasAkhirPenyampaian.'-1 weeks');
					$hmin1minggu = date("Y-m-d", $hmin1minggu);
					if($today == $hmin1minggu){
						$flagBolehEmail = true;
					}
					$hmin2minggu = strtotime($TanggalBatasAkhirPenyampaian.'-2 weeks');
					$hmin2minggu = date("Y-m-d", $hmin2minggu);
					if($today == $hmin2minggu){
						$flagBolehEmail = true;
					}
					$hmin3minggu = strtotime($TanggalBatasAkhirPenyampaian.'-3 weeks');
					$hmin3minggu = date("Y-m-d", $hmin3minggu);
					if($today == $hmin3minggu){
						$flagBolehEmail = true;
					}
					echo $hmin3minggu;
					echo $hmin2minggu;
					echo $hmin1minggu;
				}

				if($flagBolehEmail){
					$laporanIni = $this->ldarchive_model->dataLaporan($laporan->KodeLaporan);
					$picLaporanIni = $this->ldarchive_model->picLaporan($laporan->KodeLaporan);
			
					$query = $this->db->get_where('globalparam',array('Name'=>'Admin_DokumenLegal'));
					foreach($query->result() as $row)
					{
						$npkAdminDokumenLegal = explode(', ',$row->Value);
					}
					foreach($npkAdminDokumenLegal as $user){
						$legalbpanalyst = $this->user->dataUser($user);
						
						$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
						$this->email->to($legalbpanalyst[0]->email); 
		
						$this->email->subject('Notifikasi Deadline Laporan!');
						if($legalbpanalyst[0]->KodeRole == 4){
							$message = 'Dear Corporate Secretary Leader,<br/><br/>';
						}else{
							$message = 'Dear Legal & BP Analyst,<br/><br/>';
						}
						$message .= 'Sehubungan dengan deadline laporan '.$laporanIni[0]->NamaLaporan.', maka diharapkan untuk dilakukan upload dokumen tersebut sebelum tanggal jatuh tempo<br/><br/>
						Deadline penyampaian laporan pada tanggal : '.$laporanIni[0]->TanggalBatasAkhirPenyampaian.'<br/><br/>
						http://DPA2/ESS/index.php/LDArchive/ReminderLaporanController<br/><br/>
						Terima kasih<br/><br/>
						ESS';
						// $message .= $laporanIni[0]->NamaLaporan.', sudah diupload ke dalam system pada '.$laporanIni[0]->UpdatedOn.'<br/><br/>
						// 			'.$laporanIni[0]->NamaLaporan.' jatuh tempo pada tanggal : '.$laporanIni[0]->TanggalBatasAkhirPenyampaian.'<br/><br/>
						// 			https://DPA/ESS/index.php/LDArchive/ReminderLaporanController<br/><br/>
						// 			Terima kasih<br/><br/>
						// 			ESS';
						$this->email->message($message);	
						$this->email->send();
					}
					foreach($picLaporanIni as $pic){
						$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
						$this->email->to($pic->EmailInternal); 
						$this->email->subject('Notifikasi Deadline Laporan!');
						$message = 'Dear PIC,<br/><br/>';
						$message .= 'Sehubungan dengan deadline laporan '.$laporanIni[0]->NamaLaporan.', maka diharapkan untuk dilakukan upload dokumen tersebut sebelum tanggal jatuh tempo<br/><br/>
						Deadline penyampaian laporan pada tanggal : '.$laporanIni[0]->TanggalBatasAkhirPenyampaian.'<br/><br/>
						http://DPA2/ESS/index.php/LDArchive/ReminderLaporanController<br/><br/>
						Terima kasih<br/><br/>
						ESS';
						
						$this->email->message($message);	
						$this->email->send();
					}
				}
			}
			$message = 'email reminder laporan';
			$this->reportIT($message);
			return true;
		}else{
			$message = 'tidak email reminder laporan';
			$this->reportIT($message);
			return false;
			
		}
	}
	public function reportIT($message){
		$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
		$this->email->to('gerry.gabriel@dpa.co.id'); 
		$this->email->subject('Scheduler Dokumen Legal');
		
		$this->email->message($message);	
		$this->email->send();
	}
	
}
