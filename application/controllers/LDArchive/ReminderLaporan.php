<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class ReminderLaporan extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
        $this->load->model('ldarchive_model','',TRUE);
		$this->load->library('email');
		$this->load->library('grocery_crud');
    }
 
	public function index()
	{
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("322"))
            {
                $this->npkLogin = $session_data['npk'];
				$this->_reminderLaporan();
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }	
	
	public function _reminderLaporan($dpa='',$tahun='',$namalaporan='',$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		
		$crud = new grocery_crud();
		$crud->set_subject('Reminder Laporan');
		$crud->set_theme('flexigrid');
		
        $crud->set_table('mstrreminderlaporan');
        $crud->where('mstrreminderlaporan.deleted','0');
        
		if($dpa != '')
		{
			$crud->where("mstrreminderlaporan.dpa", $dpa);
		}
		if($tahun != '')
		{
			$crud->where('year(mstrreminderlaporan.TanggalBatasAkhirPenyampaian)',$tahun);				
		}
		if($namalaporan != '')
		{
			$crud->like('mstrreminderlaporan.namalaporan', $namalaporan);
		}
		$query = $this->db->get_where('globalparam',array('Name'=>'Admin_DokumenLegal'));
		foreach($query->result() as $row)
		{
			$npkAdminDokumenLegal = $row->Value;
		}
		if($this->user->isAdmin($this->npkLogin)){
			//Admin DPA
			$crud->add_action('Decline Reminder', base_url('/assets/images/decline.png'), '','',array($this,'decline_reminder'));
			$crud->add_action('Ubah Reminder', base_url('/assets/images/edit.png'), '','',array($this,'ubah_reminder'));
			$crud->add_action('Upload Laporan', base_url('/assets/images/upload.png'), '', '' ,array($this,'upload_laporan'));
		
		}else if(strpos($npkAdminDokumenLegal, $this->npkLogin) != false){
			//berarti dia admin DokumenLegal
			$crud->add_action('Upload Laporan', base_url('/assets/images/upload.png'), '', '' ,array($this,'upload_laporan'));
			$crud->add_action('Decline Reminder', base_url('/assets/images/decline.png'), '','',array($this,'decline_reminder'));
			$crud->add_action('Ubah Reminder', base_url('/assets/images/edit.png'), '','',array($this,'ubah_reminder'));
		}else{
			//brarti user biasa / PIC
			$crud->unset_delete();
			$crud->where("KodeLaporan IN (SELECT KodeLaporan FROM picreminderlaporan where NPK = '$this->npkLogin')");
			$crud->add_action('Upload Laporan', base_url('/assets/images/upload.png'), '', '' ,array($this,'upload_laporan'));
		}
		
		$crud->columns('NamaLaporan','PeriodeLaporan','DPA','TanggalBatasAkhirPenyampaian', 'PIC', 'TanggalPengirimanLaporan','FilePath');
		$crud->callback_column('TanggalBatasAkhirPenyampaian', array($this,'_callback_formatTanggalPengesahan'));
		$crud->unset_back_to_list();
		switch($page)
		{
			case "uploadLaporan":
				$crud->fields('NamaLaporan','PeriodeLaporan','DPA','TanggalBatasAkhirPenyampaian','PIC', 'FilePath','TanggalPengirimanLaporan');
				$crud->required_fields('FilePath','TanggalPengirimanLaporan');
				$crud->callback_edit_field('PIC', array($this,'_callback_PIC'));
				$crud->field_type('DPA','dropdown', array('1' => '1', '2' => '2'));
				$crud->field_type('PeriodeLaporan','dropdown', array('Bulanan' => 'Bulanan', 'Tahunan' => 'Tahunan', 'Kwartalan' => 'Kwartalan', 'Semesteran' => 'Semesteran'));
				$crud->change_field_type('NamaLaporan', 'readonly');
				$crud->change_field_type('PeriodeLaporan', 'readonly');
				$crud->change_field_type('DPA', 'readonly');
				$crud->change_field_type('TanggalBatasAkhirPenyampaian', 'readonly');
				$crud->change_field_type('PIC', 'readonly');
				break;
			case "view":
				$crud->fields('NamaLaporan','PeriodeLaporan','DPA','TanggalBatasAkhirPenyampaian','PIC', 'FilePath','TanggalPengirimanLaporan');
				$crud->set_relation_n_n('PIC','picreminderlaporan','mstruser','KodeLaporan','NPK','Nama');
				$crud->unset_add();
				$crud->unset_edit();
				break;
			case "ubahReminder":
				$crud->unset_add();
				$crud->fields('NamaLaporan','PeriodeLaporan','DPA','TanggalBatasAkhirPenyampaian', 'PIC');
				$crud->required_fields('NamaLaporan','PeriodeLaporan','DPA','TanggalBatasAkhirPenyampaian', 'PIC');
				$crud->field_type('DPA','dropdown', array('1' => '1', '2' => '2'));
				$crud->field_type('PeriodeLaporan','dropdown', array('Bulanan' => 'Bulanan', 'Tahunan' => 'Tahunan'));
				$crud->set_relation_n_n('PIC','picreminderlaporan','mstruser','KodeLaporan','NPK','Nama');
				break;
			default:
				$crud->fields('NamaLaporan','PeriodeLaporan','DPA','TanggalBatasAkhirPenyampaian', 'PIC');
				$crud->required_fields('NamaLaporan','PeriodeLaporan','DPA','TanggalBatasAkhirPenyampaian', 'PIC');
				
				$crud->field_type('DPA','dropdown', array('1' => '1', '2' => '2'));
				$crud->field_type('PeriodeLaporan','dropdown', array('Bulanan' => 'Bulanan', 'Kuartalan' => 'Kuartalan', 'Semesteran' => 'Semesteran', 'Tahunan' => 'Tahunan'));
				$crud->set_relation_n_n('PIC','picreminderlaporan','mstruser','KodeLaporan','NPK','Nama');
				break;
		}	
		$crud->set_field_upload('FilePath','./assets/uploads/files/Reminder Laporan',"pdf|doc|docx|xls|xlsx");
        $crud->callback_column('TanggalBatasAkhirPenyampaian', array($this,'_callback_formatTanggalBatasAkhirPenyampaian'));
		
        $crud->display_as('TanggalBatasAkhirPenyampaian','Deadline');
        $crud->display_as('TanggalPengirimanLaporan','Submit Date');
        $crud->display_as('NamaLaporan','Nama Laporan');
        $crud->display_as('PeriodeLaporan','Periode Laporan');
        $crud->display_as('FilePath','File Laporan');
		$crud->callback_after_insert(array($this,'_after_insert_reminderlaporan'));
		$crud->callback_after_update(array($this,'_after_update_reminderlaporan'));
		$crud->callback_delete(array($this,'_delete_reminderlaporan'));
		$crud->set_lang_string('insert_success_message',
		'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
		<script type="text/javascript">
		window.location = "'.$this->config->base_url().'index.php/LDArchive/ReminderLaporanController/";
		</script>
		<div style="display:none">');
		$crud->set_lang_string('update_success_message',
		'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
		<script type="text/javascript">
		window.location = "'.$this->config->base_url().'index.php/LDArchive/ReminderLaporan/searchAndViewReminderLaporan/";
		</script>
		<div style="display:none">');
		
        $output = $crud->render();
        $this-> _outputview($output, $page);      
	}

	function upload_laporan($primary_key , $row)
	{
		return site_url('LDArchive/ReminderLaporan/uploadLaporan').'/edit/'.$row->KodeLaporan;
	}
	function decline_reminder($primary_key , $row)
	{
		if($row->FilePath != ''){
			return site_url('LDArchive/ReminderLaporan/declineReminder').'/'.$row->KodeLaporan;
		}else{
			return '';
		}
	}
	
	function ubah_reminder($primary_key , $row)
	{
		return site_url('LDArchive/ReminderLaporan/ubahReminder').'/edit/'.$row->KodeLaporan;
	}
	public function readReminderLaporan($dpa='',$tahun='',$namalaporan='', $page=''){
		$session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("322"))
            {
                $this->npkLogin = $session_data['npk'];
                if($dpa == 'non'){
				    $dpa = '';
                }if($tahun == 'non'){
					$tahun = '';
                }if($namalaporan == 'non'){
                    $namalaporan = '';
                }
				$this->_reminderLaporan($dpa, $tahun, $namalaporan, 'readLaporan');
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}
	public function deleteReminderLaporan($dpa='',$tahun='',$namalaporan='', $page=''){
		$session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("322"))
            {
                $this->npkLogin = $session_data['npk'];
                if($dpa == 'non'){
				    $dpa = '';
                }if($tahun == 'non'){
					$tahun = '';
                }if($namalaporan == 'non'){
                    $namalaporan = '';
                }
				$this->_reminderLaporan($dpa, $tahun, $namalaporan, 'deleteLaporan');
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}
	public function uploadLaporan($dpa='',$tahun='',$namalaporan='', $page=''){
		$session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("322"))
            {
                $this->npkLogin = $session_data['npk'];
                if($dpa == 'non'){
				    $dpa = '';
                }if($tahun == 'non'){
					$tahun = '';
                }if($namalaporan == 'non'){
                    $namalaporan = '';
                }
				$this->_reminderLaporan($dpa, $tahun, $namalaporan, 'uploadLaporan');
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}
	public function declineReminder($primary_key){
		
		$session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("322"))
            {
                $this->npkLogin = $session_data['npk'];
				$updateDate = array(
					"UpdatedBy" => $this->npkLogin,
					"UpdatedOn" => date('Y-m-d H:i:s')
				);
				//kalo upload post_array cuma ada filepath sama tanggalpengirimanlaporan
				//kalo ubah, post array ada banyak
				fire_print('log','pk dokumen_legal : '.$primary_key);
				$this->db->update('mstrreminderlaporan',$updateDate,array('KodeLaporan'=>$primary_key));
				$this->db->query("UPDATE mstrreminderlaporan SET filepath = '', TanggalPengirimanLaporan = '' where KodeLaporan = ".$primary_key);
				$laporanIni = $this->ldarchive_model->dataLaporan($primary_key);
				$picLaporanIni = $this->ldarchive_model->picLaporan($primary_key);
				
 				foreach($picLaporanIni as $pic){
					$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
					$this->email->to($pic->EmailInternal); 
	
					$this->email->subject('Decline Laporan Dokumen Legal');
					$message = 'Dear PIC,<br/><br/>';
					
					$message .= $laporanIni[0]->NamaLaporan.' yang di upload memerlukan perbaikan, mohon untuk mereview kembali dokumen yang di upload. <br/><br/>
								'.$laporanIni[0]->NamaLaporan.' jatuh tempo pada tanggal : '.$laporanIni[0]->TanggalBatasAkhirPenyampaian.'<br/><br/>
								https://DPA/ESS/index.php/LDArchive/ReminderLaporanController<br/><br/>
								Terima kasih<br/><br/>
								ESS';
					
					$this->email->message($message);	
					$this->email->send();
				}
				$this->_reminderLaporan('', '', '', 'view');
				return true;
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}
	public function ubahReminder($dpa='',$tahun='',$namalaporan='', $page=''){
		$session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("322"))
            {
                $this->npkLogin = $session_data['npk'];
                if($dpa == 'non'){
				    $dpa = '';
                }if($tahun == 'non'){
					$tahun = '';
                }if($namalaporan == 'non'){
                    $namalaporan = '';
                }
				$this->_reminderLaporan($dpa, $tahun, $namalaporan, 'ubahReminder');
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}
    public function addReminderLaporan($dpa='',$tahun='',$namalaporan='', $page='')
    {
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("322"))
            {
                $this->npkLogin = $session_data['npk'];
                if($dpa == 'non'){
				    $dpa = '';
                }if($tahun == 'non'){
					$tahun = '';
                }if($namalaporan == 'non'){
                    $namalaporan = '';
                }
				$this->_reminderLaporan($dpa, $tahun, $namalaporan, 'add');
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}	 
	public function searchAndViewReminderLaporan($dpa='',$tahun='',$namalaporan='', $page='')
    {
        $session_data = $this->session->userdata('logged_in');
        if($session_data){
            if(check_authorized("322"))
            {
                $this->npkLogin = $session_data['npk'];
                if($dpa == 'non'){
				    $dpa = '';
                }if($tahun == 'non'){
					$tahun = '';
                }if($namalaporan == 'non'){
					$namalaporan = '';
                }else{
					$namalaporan = str_replace('%20', ' ', $namalaporan);
				}
				$this->_reminderLaporan($dpa, $tahun, $namalaporan, 'view');
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
	}	
	
    function _outputview($output = null, $page = '')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Dokumen Legal',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		if($page == 'view')
		{
			$this->load->view('LDArchive/ReminderLaporan_iframe',$data);
		}else if($page == 'uploadLaporan')
		{
			$this->load->view('LDArchive/ReminderLaporan_iframe',$data);
		}else if($page == 'ubahReminder')
		{
			$this->load->view('LDArchive/ReminderLaporan_iframe',$data);
		}else
		{
			$this->template->load('default','templates/CRUD_view',$data);
		}
    }
	
	function _delete_reminderlaporan($primary_key){
		return $this->db->update('mstrreminderlaporan',array('deleted' => '1'),array('KodeLaporan' => $primary_key));
	}
	function _callback_formatTanggalBatasAkhirPenyampaian($value, $row){
	
		return substr($value, 8,2).'-'.substr($value, 5,2).'-'.substr($value, 0,4);
	}
	function _callback_PIC($value, $row){
		$rs = $this->db->query("Select NPK from picreminderlaporan where KodeLaporan = '$row'");
		if($result = $rs->result()){
			$string = '';
			$counter = 1;
			foreach($result as $row2){
				$rs2 = $this->db->query("Select Nama from mstruser where NPK = '$row2->NPK'");
				$result2 = $rs2->result();
				foreach($result2 as $row3){
					if($counter == count($result)){
						$string .= $row3->Nama;
					}else{
						$string .= $row3->Nama . ', ';
					}
					$counter++;
				}
			}
			return $string;
		}
		
	}
	
	function _after_insert_reminderlaporan($post_array,$primary_key)
	{
		$createDate = array(
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s')
		);
		fire_print('log',print_r($post_array,true));
		$this->db->update('mstrreminderlaporan',$createDate,array('KodeLaporan'=>$primary_key));
		$this->db->update('picreminderlaporan',$createDate,array('KodeLaporan'=>$primary_key));
		
		//$this->db->query('UPDATE mstrreminderlaporan SET OneMonthReminder = DATE_SUB(TanggalBatasAkhirPenyampaian, INTERVAL 30 DAY), ThreeMonthReminder = DATE_SUB(TanggalBatasAkhirPenyampaian, INTERVAL 90 DAY) where KodeLaporan = '.$primary_key);
		
		return true;
	}
    function _after_update_reminderlaporan($post_array,$primary_key)
	{
		$session_data = $this->session->userdata('logged_in');
		$updateDate = array(
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s')
		);
		fire_print('log',print_r($post_array,true));
		//kalo upload post_array cuma ada filepath sama tanggalpengirimanlaporan
		//kalo ubah, post array ada banyak
		fire_print('log','pk dokumen_legal : '.$primary_key);
		$this->db->update('mstrreminderlaporan',$updateDate,array('KodeLaporan'=>$primary_key));
		$this->db->update('picreminderlaporan',$updateDate,array('KodeLaporan'=>$primary_key));
		
		//$this->db->query('UPDATE mstrreminderlaporan SET OneMonthReminder = DATE_SUB(TanggalBatasAkhirPenyampaian, INTERVAL 30 DAY), ThreeMonthReminder = DATE_SUB(TanggalBatasAkhirPenyampaian, INTERVAL 90 DAY) where KodeLaporan = '.$primary_key);
		$laporanIni = $this->ldarchive_model->dataLaporan($primary_key);
		$picLaporanIni = $this->ldarchive_model->picLaporan($primary_key);
		$this->npkLogin = $session_data['npk'];
		$catetKeRiwayat = "INSERT INTO rwyUploadLaporan (KodeLaporan, NPK, FilePath, CreatedOn, CreatedBy) VALUES (".$primary_key.", ".$this->npkLogin.", '', now(), ".$this->npkLogin." )";
		$this->db->query($catetKeRiwayat);
		if(count($post_array) == 3){
			//upload reminder	
			$query = $this->db->get_where('globalparam',array('Name'=>'Admin_DokumenLegal'));
			foreach($query->result() as $row)
			{
				$npkAdminDokumenLegal = explode(', ',$row->Value);
			}
			foreach($npkAdminDokumenLegal as $user){
				$legalbpanalyst = $this->user->dataUser($user);
				
				$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
				$this->email->to($legalbpanalyst[0]->email); 

				$this->email->subject('Upload Laporan Dokumen Legal');
				if($legalbpanalyst[0]->KodeRole == 4){
					$message = 'Dear Corporate Secretary Leader,<br/><br/>';
				}else{
					$message = 'Dear Legal & BP Analyst,<br/><br/>';
				}
				$message .= $laporanIni[0]->NamaLaporan.', sudah diupload ke dalam system pada '.$laporanIni[0]->UpdatedOn.'<br/><br/>
							'.$laporanIni[0]->NamaLaporan.' jatuh tempo pada tanggal : '.$laporanIni[0]->TanggalBatasAkhirPenyampaian.'<br/><br/>
							https://DPA/ESS/index.php/LDArchive/ReminderLaporanController<br/><br/>
							Terima kasih<br/><br/>
							ESS';
				$this->email->message($message);	
				$this->email->send();
			}
		}else{
			//ubah reminder
			foreach($picLaporanIni as $pic){
				$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
				$this->email->to($pic->EmailInternal); 

				$this->email->subject('Perubahan Reminder Laporan Dokumen Legal');
				$message = 'Dear PIC,<br/><br/>';
				
				$message .= $laporanIni[0]->NamaLaporan.', telah diubah oleh Corporate Secretary pada ESS pada tanggal '.$laporanIni[0]->UpdatedOn.'<br/><br/>
							'.$laporanIni[0]->NamaLaporan.' jatuh tempo pada tanggal : '.$laporanIni[0]->TanggalBatasAkhirPenyampaian.'<br/><br/>
							https://DPA/ESS/index.php/LDArchive/ReminderLaporanController<br/><br/>
							Terima kasih<br/><br/>
							ESS';
				
				$this->email->message($message);	
				$this->email->send();
			}
		}
		return true;
    }
	
   
	public function copyAllReminder(){
		$session_data = $this->session->userdata('logged_in');
        if($session_data){
			$this->npkLogin = $session_data['npk'];
			
			$query = $this->db->get_where('globalparam',array('Name'=>'Admin_DokumenLegal'));
			foreach($query->result() as $row)
			{
				$npkAdminDokumenLegal = $row->Value;
			}
			if(strpos($npkAdminDokumenLegal, $this->npkLogin) != false){
                $searchAllLaporan = "SELECT KodeLaporan, NamaLaporan, TanggalBatasAkhirPenyampaian 
				from mstrreminderlaporan where deleted = 0 and updatedby !='' ";
				$allLaporan = $this->db->query($searchAllLaporan)->result();
				foreach($allLaporan as $laporan){
					$laporanIni = $this->ldarchive_model->dataLaporan($laporan->KodeLaporan);
					$picLaporanIni = $this->ldarchive_model->picLaporan($laporan->KodeLaporan);
					$TanggalBatasAkhirPenyampaian = $laporanIni[0]->TanggalBatasAkhirPenyampaian;
					$TanggalBatasAkhirPenyampaianTahunDepan = strtotime($TanggalBatasAkhirPenyampaian.'+1 years');
					$TanggalBatasAkhirPenyampaianTahunDepan = date("Y-m-d", $TanggalBatasAkhirPenyampaianTahunDepan);
					
					$sql = "INSERT INTO mstrreminderlaporan (Deleted, NamaLaporan, PeriodeLaporan, DPA, TanggalBatasAkhirPenyampaian, CreatedBy, CreatedOn) 
					VALUES (0,?,?,?,?,now(),?)";
					$query = $this->db->query($sql, array(
							$laporanIni[0]->NamaLaporan, 
							$laporanIni[0]->PeriodeLaporan, 
							$laporanIni[0]->DPA,
							$TanggalBatasAkhirPenyampaianTahunDepan,
							'System'));
					$newestKodeLaporan = "SELECT max(KodeLaporan) as KodeLaporan from mstrreminderlaporan";
					
					$rs = $this->db->query($newestKodeLaporan)->result();
					foreach($picLaporanIni as $pic){
						$sql = "UPDATE picreminderlaporan set Deleted = 0 where KodeLaporan = ? and NPK = ?";
						$query = $this->db->query($sql, array(
							$laporan->KodeLaporan,
							$pic->NPK
						));
						$sql = "INSERT INTO picreminderlaporan (Deleted, KodeLaporan, NPK) VALUES (0, ?, ?)";
						$query = $this->db->query($sql, array(
							$rs[0]->KodeLaporan,
							$pic->NPK
						));
					}
					
					$sql = "UPDATE mstrreminderlaporan set DELETED = 0 where KodeLaporan = ?";
					$query = $this->db->query($sql, $laporanIni[0]->KodeLaporan);
				}
				redirect('LDArchive/ReminderLaporanController/');
			}else{
				//brarti user biasa / PIC
				$crud->unset_delete();
				$crud->where("KodeLaporan IN (SELECT KodeLaporan FROM picreminderlaporan where NPK = '$this->npkLogin')");
				$crud->add_action('Upload Laporan', base_url('/assets/images/upload.png'), '', '' ,array($this,'upload_laporan'));
			}
            
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
		
	}
}
/* End of file main.php */
/* Location: ./application/controllers/main.php */