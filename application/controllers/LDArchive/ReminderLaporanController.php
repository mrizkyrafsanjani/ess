<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ReminderLaporanController extends CI_Controller {
    var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('user','',TRUE);
        $this->load->model('menu','',TRUE);
        $this->load->model('ldarchive_model','',TRUE);
		$this->load->library('email');
        $this->load->library('grocery_crud');
    }

    public function index()
    {
        try
		{
			if($this->session->userdata('logged_in'))
			{
				if(check_authorized("319"))
				{
					$allJenis = "SELECT KodeJenisDokumenLegal, JenisDokumenLegal from jenisdokumenlegal where Deleted = 0";
					$allJenisRs = $this->db->query($allJenis)->result();
					
					$data = array(
						'title' => 'Search and View Legal Document',
						'allJenis' => $allJenisRs
					);
					$this->load->helper(array('form','url'));
					$this->template->load('default','LDArchive/ReminderLaporan_view',$data);
				}
			}
			else
			{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	

}
