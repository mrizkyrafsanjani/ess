<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LDArchiveController extends CI_Controller {
    var $npkLogin;
    function __construct()
    {
        parent::__construct();
        $this->load->model('menu','',TRUE);
        $this->load->model('user','',TRUE);
        $this->load->model('ldarchive_model','',TRUE);
		$this->load->library('email');
        $this->load->library('grocery_crud');
    }

    public function index()
    {
        try
		{
			if($this->session->userdata('logged_in'))
			{
				if(check_authorized("319"))
				{
					$allJenis = "SELECT KodeJenisDokumenLegal, JenisDokumenLegal from jenisdokumenlegal where Deleted = 0";
					$allJenisRs = $this->db->query($allJenis)->result();
					$data = array(
						'title' => 'Search and View Legal Document',
						'allJenis' => $allJenisRs
					);
					$this->load->helper(array('form','url'));
					$this->template->load('default','LDArchive/LDArchive_view',$data);
				}
			}
			else
			{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	public function read($KodeDokumen = "")
	{
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorized("319"))
			{
				$detail = $this->ldarchive_model->dataDokumen($KodeDokumen);
				// $related = array();
				// if($detail = $this->ldarchive_model->dataDokumen($KodeDokumen)){
				// 	foreach ($detail as $value) {
				// 		$related = ($this->ldarchive_model->dataDokumen($value->DokumenYangDiganti));
				// 	}
				// }
				$GLOBALS['history'] = array();
				$flagLoop = true;
				$counter = 0;
				$status = 'Aktif';
				if($detail[0]->Status == 0){
					$status = 'Tidak Aktif';
				}else{
					$status = 'Aktif';
				}
				while($flagLoop){
					//keatas
					if($counter == 0){
						$row = $this->ldarchive_model->dataDokumen($detail[0]->DokumenYangDiganti);
					}else{
						$row = $this->ldarchive_model->dataDokumen($GLOBALS['history'][$counter-1]->DokumenYangDiganti);
					}
					if($row != null){
						array_push($GLOBALS['history'], $row[0]);
						$counter++;
					}else{
						$flagLoop = false;
					}
				}
				$flagLoop = true;
				$counter2 = 0;
				while($flagLoop){
					//keatas
					if($counter2 == 0){
						$row = $this->ldarchive_model->dataHistory($detail[0]->KodeDokumen);
					}else{
						//2+1-1
						$row = $this->ldarchive_model->dataHistory($GLOBALS['history'][$counter+$counter2-1]->KodeDokumen);
					}
					if($row != null){
						array_push($GLOBALS['history'], $row[0]);
						$counter2++;
					}else{
						$flagLoop = false;
					}
				}
				$data = array(
					'title' => 'Dokumen Legal',
					'detail' => $detail,
					'status' => $status,
					'related' => $GLOBALS['history']
			  	);
				$this->npkLogin = $session_data['npk'];
				$this->template->load('default','LDArchive/LDArchive_read',$data);
			}
		}else{
			redirect('login','refresh');
		}
	}
	function historyKeatas($KodeDokumen){
		$row = $this->ldarchive_model->dataDokumen($KodeDokumen);
		if($row != null){
			array_push($GLOBALS['history'], $row[0]);
			$this->historyKeatas($row[0]->DokumenYangDiganti);
		}else{
			var_dump($GLOBALS['history']);
			return $GLOBALS['history'];
		}
	}
	public function notifikasiReminderDokumen()
	{
		$searchAllDokumen = 'SELECT KodeDokumen, DocumentName, TanggalKadaluarsa 
		from mstrdokumenlegal where deleted = 0 and status = 1';
		$allDokumen = $this->db->query($searchAllDokumen)->result();
		if($allDokumen){
			foreach($allDokumen as $dokumen){
				//> 3 bulan, do nothing
				//> 1 <= 3 bulan email 1x/30 hari
				//< 1 bulan email 1x/7 hari
				//flag email
				$flag1Bulan = false;
				$flag3Bulan = false;
				$flagBolehEmail = false;
				$today = date("Y-m-d");
				$TanggalKadaluarsa = $dokumen->TanggalKadaluarsa;
				$oneMonthReminder = strtotime($TanggalKadaluarsa.'-1 months');
				$oneMonthReminder = date("Y-m-d", $oneMonthReminder);
				$threeMonthReminder = strtotime($TanggalKadaluarsa.'-3 months');
				$threeMonthReminder = date("Y-m-d", $threeMonthReminder);
				var_dump($dokumen);
				if($TanggalKadaluarsa <= $today){
					//expired nih
					echo 'expired nih';
				}else if($threeMonthReminder <= $today){
					if($oneMonthReminder <= $today){
						echo 'masa 1 bulan sebelum';
						$flag1Bulan = true;
					}else{
						echo 'masa 3 bulan sebelum';
						$flag3Bulan = true;
					}
				}else{
					echo 'belum masuk masa reminder';
				}
				if($today == $threeMonthReminder || $today == $oneMonthReminder){
					$flagBolehEmail = true;
				}else if($flag3Bulan){
					$Hmin2Bulan = strtotime($threeMonthReminder.'+1 months');
					$Hmin2Bulan = date("Y-m-d", $Hmin2Bulan);
					echo $Hmin2Bulan;
					if($today == $Hmin2Bulan){
						$flagBolehEmail = true;
					}
				}else if($flag1Bulan){
					$hmin1minggu = strtotime($TanggalKadaluarsa.'-1 weeks');
					$hmin1minggu = date("Y-m-d", $hmin1minggu);
					if($today == $hmin1minggu){
						$flagBolehEmail = true;
					}
					$hmin2minggu = strtotime($TanggalKadaluarsa.'-2 weeks');
					$hmin2minggu = date("Y-m-d", $hmin2minggu);
					if($today == $hmin2minggu){
						$flagBolehEmail = true;
					}
					$hmin3minggu = strtotime($TanggalKadaluarsa.'-3 weeks');
					$hmin3minggu = date("Y-m-d", $hmin3minggu);
					if($today == $hmin3minggu){
						$flagBolehEmail = true;
					}
				}

				if($flagBolehEmail){
					$dokumenIni = $this->ldarchive_model->dataDokumen($dokumen->KodeDokumen);
					$picdokumenIni = $this->ldarchive_model->picDokumen($dokumen->KodeDokumen);
			
					$query = $this->db->get_where('globalparam',array('Name'=>'Admin_DokumenLegal'));
					foreach($query->result() as $row)
					{
						$npkAdminDokumenLegal = explode(', ',$row->Value);
					}
					foreach($npkAdminDokumenLegal as $user){
						$legalbpanalyst = $this->user->dataUser($user);
						
						$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
						$this->email->to($legalbpanalyst[0]->email); 
		
						$this->email->subject('Notifikasi Kadaluarsa Dokumen Legal!');
						if($legalbpanalyst[0]->KodeRole == 4){
							$message = 'Dear Corporate Secretary Leader,<br/><br/>';
						}else{
							$message = 'Dear Legal & BP Analyst,<br/><br/>';
						}
						$message .= 'Sehubungan dengan akan berakhirnya '.$dokumenIni[0]->DocumentName.', maka diharapkan untuk dilakukan update dokumen tersebut sebelum tanggal jatuh tempo<br/><br/>
						Dokumen kadarluarsa pada tanggal : '.$dokumenIni[0]->TanggalKadaluarsa.'<br/><br/>
						http://DPA2/ESS/index.php/LDArchive/LDArchiveController/<br/><br/>
						Terima kasih<br/><br/>
						ESS';
						// $message .= $dokumenIni[0]->Namadokumen.', sudah diupload ke dalam system pada '.$dokumenIni[0]->UpdatedOn.'<br/><br/>
						// 			'.$dokumenIni[0]->Namadokumen.' jatuh tempo pada tanggal : '.$dokumenIni[0]->TanggalKadaluarsa.'<br/><br/>
						// 			https://DPA/ESS/index.php/LDArchive/LDArchiveController/<br/><br/>
						// 			Terima kasih<br/><br/>
						// 			ESS';
						$this->email->message($message);	
						$this->email->send();
					}
					foreach($picdokumenIni as $pic){
						$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
						$this->email->to($pic->EmailInternal); 
						$this->email->subject('Notifikasi Kadaluarsa Dokumen Legal!');
						$message = 'Dear PIC,<br/><br/>';
						$message .= 'Sehubungan dengan akan berakhirnya '.$dokumenIni[0]->DocumentName.', maka diharapkan untuk dilakukan update dokumen tersebut sebelum tanggal jatuh tempo<br/><br/>
						Dokumen kadarluarsa pada tanggal : '.$dokumenIni[0]->TanggalKadaluarsa.'<br/><br/>
						http://DPA2/ESS/index.php/LDArchive/ReminderdokumenController<br/><br/>
						Terima kasih<br/><br/>
						ESS';
						
						$this->email->message($message);	
						$this->email->send();
					}
				}
			}
			
			echo 'email';
			return true;
		}else{
			echo 'tidak email';
			return false;
		}
		
	}
	
	public function test(){
		$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
		$this->email->to('anggit.gumilang@dpa.co.id'); 
		$this->email->subject('Hanya test');
		$message = 'test success';
		
		$this->email->message($message);	
		$this->email->send();
	}
	
}
