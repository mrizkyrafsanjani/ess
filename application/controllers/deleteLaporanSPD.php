<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');

class DeleteLaporanSPD extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('trkSPD','',TRUE);
 }

 function index()
 {
	$noSPD = $this->input->get('NoSPD');
   //This method will have the credentials validation
   if($this->trkSPD->deleteLaporanSPD($noSPD))
   {
     redirect('rekapSPD', 'refresh');
   }

 }
 
 	
}
?>