<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class ResetPasswordWindows extends CI_Controller {	
    function __construct()
    {
        parent::__construct();
    }
 
    public function index()
    {	
        $this->load->helper(array('form','url'));
		$this->load->view('ResetPasswordWindows/resetPasswordWindows_view');		
    }

    public function resetpasswin()
    {
        $tokenenc = $this->input->get('token');
        $username = $this->input->get('uname');

        if($this->_checkToken($username,$tokenenc))
        {
            $data = array(
                    'uname'=>$username,
                    'tokenenc'=>$tokenenc
                );
            $this->load->helper(array('form','url'));
            $this->load->view('ResetPasswordWindows/resetPasswordWinFinal_view',$data);
        }
    }

    private function _checkToken($username,$tokenenc)
    {
        $kodetoken_db = '';
        $tokenreset_rs = $this->db->query("select * from tokenreset
            where Deleted = 0 AND Username = ? AND CreatedOn > DATE_SUB(NOW(), INTERVAL 5 MINUTE)
            ORDER BY CreatedOn DESC
            limit 1;",array($username));
        if($tokenreset_rs){
            foreach($tokenreset_rs->result() as $dt)
            {
                $kodetoken_db = $dt->KodeToken;
            }
        }

        if(md5($kodetoken_db."3177s3cr3t")===$tokenenc)
        {
            return true;
        }
        else
        {
            //kode tidak benar, kembalikan ke halaman reset password awal
            $this->load->helper(array('form','url'));
            redirect('ResetPasswordWindows/resetPasswordWindows', 'refresh');
        }
    }

    public function changeWinPass()
    {
        try
        {
            $username = $this->input->post('username');
            $tokenenc = $this->input->post('tokenenc');
            $passwordnew = $this->input->post('passwordbaru');
            $passwordconf = $this->input->post('passwordconfirm');

            if($this->_checkToken($username,$tokenenc))
            {
                //validasi password
                if($passwordnew == $passwordconf)
                {
                    if(strlen($passwordnew) >= 7)
                    {
                        if(strpos($passwordnew,$username) !== false)
                        {
                            echo "Password tidak boleh mengandung username!";
                        }
                        else
                        {
                            $containsLowerCaseLetter  = preg_match('/[a-z]/',$passwordnew);
                            $containsUpperCaseLetter  = preg_match('/[A-Z]/',$passwordnew);
                            $containsDigit   = preg_match('/\d/',          $passwordnew);
                            $containsSpecial = preg_match('/[^a-zA-Z\d]/', $passwordnew);
                            $jumlahPassValid = 0;
                            if($containsLowerCaseLetter) $jumlahPassValid++;
                            if($containsUpperCaseLetter) $jumlahPassValid++;
                            if($containsDigit) $jumlahPassValid++;
                            if($containsSpecial) $jumlahPassValid++;
                            if($jumlahPassValid >= 3){
                                //proses reset password dimulai
                                if($this->_change_ad_password($username,$passwordnew))
                                {
                                    //hapus kode verifikasi terakhir
                                    $ip = $this->input->ip_address();
                                    $ip = ($this->input->valid_ip($ip)?$ip:'IP Not Valid');
                                    $this->db->query("update tokenreset set Deleted = 1, UpdatedOn = NOW(), UpdatedBy = ?
                                        where Deleted = 0 AND Username = ?",array($ip,$username));
                                    echo "Password Anda berhasil dirubah!";
                                }
                                else
                                {
                                    echo "Gagal merubah password!";
                                }
                            }
                            else
                            {
                                echo "Password baru harus mengandung huruf besar, huruf kecil, angka dan simbol!";
                            }
                        }
                    }
                    else
                    {
                        echo "Panjang password minimal 7 karakter!";
                    }
                }
                else
                {
                    echo "Password baru tidak sama dengan konfirmasi password!";
                }
            }
            else
            {
                echo "redirect";
            }
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            throw new Exception( 'Something really gone wrong', 0, $e);
        }
    }

    public function sendSMSToken()
    {
        $username = $this->input->post('username');
        //check username apakah ada di windows        
        if($this->_checkUsername($username) == false)
        {
            echo "Username ".$username." tidak ada!";
        }
        else
        {
            //proses kirim sms
            $user_rs = $this->db->query("select case when NoHP like '0%' then CONCAT('62',substring(NoHP,2,length(NoHP)-1)) else NoHP end as NoHP 
                from mstruser where Deleted = 0 AND WinID = ? limit 1;",array($username));
            if($user_rs){
                foreach($user_rs->result() as $user)
                {
                    $nohp_user = $user->NoHP;
                }
                $kodetoken = $this->_generateToken($username);
                if($this->_kirimSMSviaAWO($nohp_user,$kodetoken))
                {
                    echo 'SMS telah dikirimkan ke nomor hp Anda, mohon tunggu sebentar';
                }
                else
                {
                    echo 'Terjadi kegagalan proses pengiriman SMS. Mohon dicoba kembali';
                }
            }
            //echo 'Harap masukkan kode '. $kodetoken . '. Anda hanya mempunyai waktu 5 menit sebelum kode ini expire.';
        }
    }

    function curl_post_old($url, array $post = NULL, array $options = array()) 
    { 
        $defaults = array( 
            CURLOPT_POST => 1, 
            CURLOPT_HEADER => 0, 
            CURLOPT_URL => $url, 
            CURLOPT_FRESH_CONNECT => 1, 
            CURLOPT_RETURNTRANSFER => 1, 
            CURLOPT_FORBID_REUSE => 1, 
            CURLOPT_TIMEOUT => 4, 
            CURLOPT_POSTFIELDS => json_encode($post)             
        );        
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt_array($ch, ($options + $defaults)); 
        if( ! $result = curl_exec($ch)) 
        { 
            log_message( 'error', curl_error($ch) );
            //trigger_error(curl_error($ch)); 
        } 
        curl_close($ch); 
        return $result; 
    }
    function curl_post($url, array $post = NULL, array $options = array()) 
    { 
        $ch = curl_init($url);
        $data_string = json_encode($post);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        if( ! $result = curl_exec($ch)) 
        { 
            log_message( 'error', curl_error($ch) );
            //trigger_error(curl_error($ch)); 
        } 
        curl_close($ch); 
        return $result; 
    }


    private function _kirimSMSviaAWO($nomorhptujuan, $kodetoken)
    {
        $expire_stamp = date('H:i', strtotime("+5 min"));
        $now_stamp    = date("d/m/Y H:i");
        
        $curl_post_data = array(
                'user' => $this->config->item('userAPISMS'),
                'pwd' => $this->config->item('passwordAPISMS'),
                'sender' => $this->config->item('senderAPISMS'),
                'msisdn' => $nomorhptujuan,
                'message' => "ESS DPA - $kodetoken adalah kode verifikasi Anda, hingga $expire_stamp WIB. PENTING: Mohon tidak menyebarkan kode ke orang lain, demi keamanan akun Anda.",
                'description' => 'resetPasswordWindows',
                'schedule' => $now_stamp,
                'campaign' => 'ESS_resetPass'
        );
        $service_url = $this->config->item('urlAPISMS');
        
        return $this->curl_post($service_url,$curl_post_data);        
    }

    public function sendEmailIT()
    {
        $username = $this->input->post('username');
        $jenis = $this->input->post('jenis');
        try
		{	
            $to = $this->config->item('emailIT');
						
			$this->load->library('email');
			$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
			$this->email->to($to); 

            $ip = $this->input->ip_address();
            $ip = ($this->input->valid_ip($ip)?$ip:'IP Not Valid');
			$this->email->subject("IP ". $ip ." Melakukan lebih dari 3x ".$jenis);
			$message = "IP ". $ip ." Melakukan lebih dari 3x ".$jenis . ". Username yang diinput adalah $username";
			$this->email->message($message);

			if( ! $this->email->send())
			{	
				return false;
			}
			else
			{
				return true;
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }

    public function verifikasiToken()
    {
        $username = $this->input->post('username');
        $kodetoken = $this->input->post('kodetoken');
        $kodetoken_db = '';
        $tokenreset_rs = $this->db->query("select * from tokenreset
            where Deleted = 0 AND Username = ? AND CreatedOn > DATE_SUB(NOW(), INTERVAL 5 MINUTE)
            ORDER BY CreatedOn DESC
            limit 1;",array($username));
        if($tokenreset_rs){
            foreach($tokenreset_rs->result() as $dt)
            {
                $kodetoken_db = $dt->KodeToken;
            }
        }

        if($kodetoken_db===$kodetoken)
        {
            echo md5($kodetoken ."3177s3cr3t");
        }
        else
        {
            echo "false";
        }
    }

    private function _generateToken($username)
    {
        try
        {
            $kodeToken = mt_rand(100000, 999999);
            //simpan ke database nilai token yang digenerate
            $ip = $this->input->ip_address();

            $data_array['Username'] = $username;
            $data_array['KodeToken'] = $kodeToken;
            $data_array['CreatedOn'] = date('Y-m-d H:i:s');
			$data_array['CreatedBy'] = ($this->input->valid_ip($ip)?$ip:'IP Not Valid');

            $this->db->insert('tokenreset',$data_array);

            return $kodeToken;
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            throw new Exception( 'Something really gone wrong', 0, $e);
        }
    }

    private function _checkUsername($username)
    {
        //Application Configuration
        $ldaps_url = $this->config->item('ldapsurl');
        $ldap_port = $this->config->item('ldapsport');
        $admin_user = $this->config->item('adminuser');
        $admin_pass = $this->config->item('adminpassword');
        $bind_dn = $this->config->item('bind_dn');
        $base_dn = $this->config->item('base_dn');
        //comma seperated list of OUs
        $ou_list = $this->config->item('ou_list');
        $filter = "(|(SamAccountName=$username*))";

        $ldap_conn = $this->create_ldap_connection($ldaps_url, $ldap_port, $admin_user, $admin_pass, $bind_dn);
        $userDn = $this->get_user_dn($ldap_conn, $filter, $base_dn, $ou_list);

        if($userDn)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private function _change_ad_password($username, $password) {
        //Application Configuration
        $ldaps_url = $this->config->item('ldapsurl');
        $ldap_port = $this->config->item('ldapsport');
        $admin_user = $this->config->item('adminuser');
        $admin_pass = $this->config->item('adminpassword');
        $bind_dn = $this->config->item('bind_dn');
        $base_dn = $this->config->item('base_dn');
        //comma seperated list of OUs
        $ou_list = $this->config->item('ou_list');
        $filter = "(|(SamAccountName=$username*))";

        $ldap_conn = $this->create_ldap_connection($ldaps_url, $ldap_port, $admin_user, $admin_pass, $bind_dn);
        $userDn = $this->get_user_dn($ldap_conn, $filter, $base_dn, $ou_list);
        $userdata = $this->pwd_encryption($password);
        
        $result = ldap_modify($ldap_conn, $userDn , $userdata);
        if($result) 
        {
            //unlock user
            $add['lockoutTime'] = array(0);
            $result = ldap_mod_replace($ldap_conn, $userDn, $add);
            //echo "Success attempting to modify password in AD";
            return true;
        }
        else 
        {            
            //echo "Error: Please try again later!<br/>";
            $e = ldap_error($ldap_conn);
            $e_no = ldap_errno($ldap_conn);
            log_message('error',"ldap_error:".$e." error_no:".$e_no);
            return false;
        }
    }

    private function create_ldap_connection($ldaps_url, $port, $admin_user, $admin_pass, $bind_dn) {
        $ldap_conn = ldap_connect($ldaps_url, $port) or die("Fatal Error: " . ldap_error($ldap_conn));
        /*if(!ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3)){
                echo ldap_error($ldap_conn);
                die("Could not set LDAPv3\r\n");
        }
        else if (!ldap_start_tls($ldap_conn)) {
                echo ldap_error($ldap_conn);
                die("Could not start secure TLS connection");
        }*/
        $binddn = "CN=$admin_user," . $bind_dn;
        $result = ldap_bind($ldap_conn, $binddn, $admin_pass);
        if($result) {
                return $ldap_conn;
        }
        else {
                print ldap_error($ldap_conn) . "<br/>";
                print ldap_errno($ldap_conn) . "<br/>";
                die("Error: Couldn't bind to server with supplied credentials!");
        }
    }
    private function get_user_dn($ldap_conn, $user_name, $base_dn, $ou_list) {
        $info = array();
        $ou_arr = explode(",", $ou_list);
        for($i = 0; $i < count($ou_arr); $i++) {
            /*Write the below details as per your AD setting*/
            $basedn = "OU=" . $ou_arr[$i]  . ",$base_dn";
            /*Search the user details in AD server*/
            $searchResults = ldap_search($ldap_conn, $basedn, $user_name);
            if(!is_resource($searchResults)) die('Error in search results.');
            /*Get the first entry from the searched result*/
            $entry = ldap_first_entry($ldap_conn, $searchResults);
            $info = ldap_get_entries($ldap_conn, $searchResults);
            //echo $info["count"]." entries returned for OU " . $ou_arr[$i]  . " \n";
            //print "\n\n";
            if($info["count"] > 0) {
                break;
            }
        }
        if($info["count"] > 0)
        {
            return ldap_get_dn($ldap_conn, $entry);            
        }
        else
        {
            return false;
        }
    }
    private function pwd_encryption($newPassword) {
        $newPassword = "\"" . $newPassword . "\"";
        $newPassw = mb_convert_encoding($newPassword, "UTF-16LE");
        $userdata["unicodePwd"] = $newPassw;
        return $userdata;
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */