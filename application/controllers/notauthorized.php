<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	class Notauthorized extends CI_Controller {
		function __construct(){
			parent::__construct();
		}
		
		function index(){
			$data = array(
				'title' => 'Not Authorized',
				'body' => 'Oppsss.. Gak boleh masuk. Bukan hak akses Anda. :( '
			  );
			
			$this->load->helper(array('form','url'));			
			$this->template->load('default','notauthorized_view',$data);
		}
	}
?>