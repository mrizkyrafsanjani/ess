<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Undangan extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('undangan_model','',TRUE);
		$this->load->model('menu','',TRUE);
		$this->load->model('usertask','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$this->load->library('grocery_crud');
		$this->load->helper('date');
		
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("70"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_Undangan();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function Edit()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("70"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_Undangan('Edit');
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	public function viewUser($tahun='',$bulan='',$nama='',$NPKuser='',$KodeUserTask='')
	{
		if($KodeUserTask == 'non')
			$KodeUserTask = '';
		if($nama == 'non')
			$nama = '';
		if($tahun == 'non')
			$tahun = date('Y');
		if($bulan == 'non')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("66"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_Undangan('viewUser',$nama,$tahun,$bulan,$KodeUserTask);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function viewAdmin($tahun='',$bulan='',$nama='non')
	{
		if($nama == 'non')
			$nama = 'xxx';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("66"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_Undangan('viewAdmin',$nama,$tahun,$bulan);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function _Undangan($page='',$nama='',$tahun='',$bulan='',$KodeUserTask='')
    {
		try{
			
			
			$crud = new grocery_crud();
			
			$state = $crud->getState();
			
			$crud->set_subject('Undangan Training');
			$crud->set_table('undangan');
			$crud->columns('NPK','KodeTraining','Penyelenggara','Tanggal','Pukul','JumlahHari','AlamatTraining','Status','Sertifikasi','Nilai');
			//$crud->columns('NPK','KodeTraining','Penyelenggara','TanggalMulai','TanggalSelesai','AlamatTraining','Status');
			$crud->fields('NPK','NPKX','Departemen','Jabatan','KodeTraining','Penyelenggara','TanggalMulai','TanggalSelesai','JumlahHari','AlamatTraining');
			
			$crud->edit_fields('NPK','NPKX','Departemen','Jabatan','KodeTraining','Penyelenggara','TanggalMulai','TanggalSelesai','JumlahHari','AlamatTraining','Catatan');
			
			$crud->set_read_fields('NPK','KodeTraining','Penyelenggara','TanggalMulai','TanggalSelesai','AlamatTraining','Status','Sertifikasi','Nilai');
			
			$crud->required_fields('NPK','KodeTraining','Penyelenggara','TanggalMulai','TanggalSelesai','AlamatTraining');
			$crud->where('undangan.deleted','0');
			$crud->where('undangan.status','APR');
			$crud->set_relation('KodeTraining','training','ProgramTraining',array('deleted' => '0','Status' => 'APR'));
			$crud->set_relation('NPK','mstruser','Nama',array('deleted' => '0','TanggalBerhenti'=>'0000-00-00'));
			
			$crud->display_as('NPK','Nama');
			//$crud->display_as('NamaKaryawan','Nama');
			$crud->display_as('KodeTraining','Program Training');
			$crud->display_as('NPKX','NPK');
			$crud->display_as('AlamatTraining','Lokasi');
			$crud->display_as('JamMulai','Jam Mulai');
			$crud->display_as('JamSelesai','Jam Selesai');
			$crud->display_as('JumlahHari','Jumlah hari');
			if($state=='read')
			{
				$crud->display_as('TanggalMulai','Tanggal');
				$crud->display_as('TanggalSelesai','Pukul');
			}
			else
			{
				$crud->display_as('TanggalMulai','Tanggal Mulai');
				$crud->display_as('TanggalSelesai','Tanggal Selesai');
			}
			$crud->unset_texteditor('AlamatTraining');
			//$crud->unset_read();
			//$crud->unset_edit();
			$crud->unset_delete();
			$crud->unset_export();
			$crud->unset_print();
			$crud->unset_back_to_list();
			
			$crud->callback_column('JumlahHari',array($this,'_callback_column_jumlahhari'));
			$crud->callback_column('Status',array($this,'_callback_Status'));
			$crud->callback_column('Tanggal',array($this,'_callback_Tanggal'));
			$crud->callback_column('Pukul',array($this,'_callback_Pukul'));
			
			$crud->callback_field('NPKX',array($this,'_callback_npkx'));
			$crud->callback_field('Departemen',array($this,'_callback_departemen'));
			$crud->callback_field('Jabatan',array($this,'add_field_callback_Jabatan'));
			$crud->callback_field('JumlahHari',array($this,'_callback_jumlahhari'));
			$crud->callback_field('JamMulai',array($this,'field_callback_JamMulai'));
			$crud->callback_field('JamSelesai',array($this,'field_callback_JamSelesai'));
			$crud->callback_field('Catatan',array($this,'field_callback_Catatan'));
			$crud->callback_read_field('Status',array($this,'field_callback_read_Status'));
			$crud->callback_read_field('TanggalMulai',array($this,'field_callback_read_Tanggal'));
			$crud->callback_read_field('TanggalSelesai',array($this,'field_callback_read_Pukul'));
			
			$crud->callback_insert(array($this,'_insert_undangan'));
			$crud->callback_update(array($this,'_update'));
			$crud->callback_delete(array($this,'_delete'));	
			
			if($nama != '')
			{
				$crud->like('undangan.CreatedBy',$nama);
			}
			if($tahun != '')
			{
				$crud->where('YEAR(undangan.TanggalMulai)',$tahun);
				$crud->unset_edit();
				$crud->unset_add();
			}
			if($bulan != '')
			{
				$crud->where('MONTH(undangan.TanggalMulai)', $bulan);
				$crud->unset_edit();
				$crud->unset_add();
			}
			
			$js = "
			<script>
				var spans = document.getElementsByTagName('span');					
				for(var i=0;i<spans.length;i++)
				{
					if(spans[i].innerHTML == '&nbsp;Edit'){
						//alert(spans[i].visible);
						spans[i].style.display = 'none';
					}
					if(spans[i].innerHTML == '&nbsp;Delete'){				
						spans[i].innerHTML = 'Hapus';		
					}
				}
				
				function loadJumlahHari(){
					var TanggalMulai = $('#field-TanggalMulai');
					var TanggalSelesai = $('#field-TanggalSelesai');
					var JumlahHari = $('#field-JumlahHari');
					//var selisihHari = DateDiff(new Date(TanggalSelesai.val()),new Date(TanggalMulai.val()))+1;
					$.ajax({
					  url: '" .base_url()."index.php/Training/Undangan/getJumlahHari',
					  type: 'POST',
					  data: {tanggalmulai : TanggalMulai.val(), tanggalselesai: TanggalSelesai.val()},
					  dataType: 'html'
					}).done(function(response){
						JumlahHari.val(response);
					});
				}
				
				function loadDataKaryawan(){
					var cmbNamaKaryawan = $('#field-NPK');
					var Departemen = $('#field-Departemen'); 
					var Jabatan = $('#field-Jabatan');
					var ProgramTraining = $('#field_KodeTraining_chzn');
					$('#field-NPKX').val(cmbNamaKaryawan.val());
					$.ajax({
						 type: 'POST',
						 url: '" .base_url()."index.php/training/trainingController/ajax_getDepartemen',
						 data: {NPK: cmbNamaKaryawan.val(), Departemen : Departemen.val()},
						}).done(function(response){
							Departemen.val(response);
					});
					$.ajax({
						 type: 'POST',
						 url: '" .base_url()."index.php/training/trainingController/ajax_getJabatan',
						 data: {NPK: cmbNamaKaryawan.val(), Jabatan: Jabatan.val() },
						}).done(function(response){
							Jabatan.val(response);
					});
					$.ajax({
						 type: 'POST',
						 url: '" .base_url()."index.php/training/trainingController/ajax_getProgramTraining',
						 data: {NPK: cmbNamaKaryawan.val(), ProgramTraining: ProgramTraining.val() },
						}).done(function(response){
							ProgramTraining.html(response);
					});
				}
				
				$(document).ready(function() {
					var TanggalSelesai = $('#field-TanggalSelesai');
					var cmbNamaKaryawan = $('#field-NPK');
					
					TanggalSelesai.change(function(){
						loadJumlahHari();
					});
					cmbNamaKaryawan.change(function(){
						loadDataKaryawan();
					});
					
					if(TanggalSelesai.val() != ''){
						loadJumlahHari();
					}
					if(cmbNamaKaryawan.val() != ''){
						loadDataKaryawan();
					}
				}); 
				
				//source: http://stackoverflow.com/questions/12003660/javascript-datediff
				function DateDiff(date1, date2) {
					var datediff = date1.getTime() - date2.getTime(); //store the getTime diff - or +
					return (datediff / (24*60*60*1000)); //Convert values to -/+ days and return value      
				}							
			</script>";
			
			
			$output = $crud->render();
			$output->output.=$js;  
			$this-> _outputview($output,$page); 
		}
		
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
	
	function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Request Undangan',
				'body' => $output
		  );
		if($page == ''){
			$this->load->helper(array('form','url'));
			$this->template->load('default','templates/CRUD_view',$data);
		}
		else
		{
			$this->load->view('Training/undanganSimple_view',$data);
		}
    }
	
	function getJumlahHari(){
		$TanggalMulai = $this->input->post('tanggalmulai');
		$TanggalSelesai = $this->input->post('tanggalselesai');
		echo s_datediff('d',substr($TanggalMulai,0,10),substr($TanggalSelesai,0,10))+1;
	}
	
	function _insert_undangan($post_array){
		try
		{
			$KodeUndangan = generateNo('TS');
			fire_print('log',"Kode undangan : $KodeUndangan");
			$post_array['KodeUndangan'] = $KodeUndangan;
			$post_array['Status'] = 'PEP'; //Pending Plan
			$post_array['Deleted'] = '0';
			$post_array['CreatedOn'] = date('Y-m-d H:i:s');
			$post_array['CreatedBy'] = $this->npkLogin;
			unset($post_array['Jabatan']);
			unset($post_array['NPKX']);
			unset($post_array['Departemen']);
			
			$this->db->trans_begin();
			$hasilInsertUndangan = $this->db->insert('undangan',$post_array);
			
			$KodeUserTask = generateNo('UT');
			$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeUndangan,$KodeUserTask,"TS");
			if($hasilTambahUserTask && $hasilInsertUndangan){
				$this->db->trans_commit();
				return true;
			}else{
				$this->db->trans_rollback();
				return false;
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function _update($post_array,$primary_key){
		$error = 0;
		$query = $this->db->get_where('undangan',array('KodeUndangan'=>$primary_key));
		$dataundangan = $query->row(1);
		if(substr($dataundangan->Status,0,2) == 'DE'){
			$post_array['Status'] = 'PEP'; //Pending Plan
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			unset($post_array['Jabatan']);
			unset($post_array['NPKX']);
			unset($post_array['Departemen']);
			
			$this->db->trans_begin();
			fire_print('log',"isi post array update undangan.". print_r($post_array,true));
			$hasilUpdate = $this->db->update('undangan',$post_array,array('KodeUndangan' => $primary_key));
			$KodeUndangan = $primary_key;
			//usertask lama di nyatakan statusnya AP
			
			$usertasklama = $this->usertask->getLastUserTask($KodeUndangan);
			$KodeUserTaskLama = '';
			if($usertasklama){
				foreach($usertasklama as $row){
					$KodeUserTaskLama = $row->KodeUserTask;
					
				}
				$hasilUpdateUserTask = $this->usertask->updateStatusUserTask($KodeUserTaskLama,'AP',$this->npkLogin);
			}
			fire_print('log',$KodeUserTaskLama);
			//buat user task baru untuk proses approval Undangan
			$KodeUserTask = generateNo('UT');
			
			
			if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeUndangan,$KodeUserTask,"TS"))
			{
				echo('gagal simpan usertask Undangan');
				$this->db->trans_rollback();
				return false;
			}
			
			fire_print('log',"Submit edit undangan. hasilUpdate : $hasilUpdate , hasilUpdateUserTask: $hasilUpdateUserTask");
			
			if($hasilUpdate && $hasilUpdateUserTask){
				$this->db->trans_commit();
				return true;
			}else{
				$this->db->trans_rollback();
				return false;
			}			
		}
		else
		{
			return false;
		}
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('undangan',$post_array,array('KodeUndangan' => $primary_key));
	}
	
	function field_callback_Catatan($value,$primary_key)
	{
		$catatan = $this->usertask->getCatatan($primary_key);
		return $catatan;
	}
	
	function _callback_jumlahhari($value= '', $primary_key = null)
	{
		return '<input type="text" id="field-JumlahHari" style="border:0;" value="" readonly>';
	}
	
	function _callback_npkx($value= '', $primary_key = null)
	{
		return '<input type="text" id="field-NPKX" style="border:0;" value="" readonly>';
	}
	
	function _callback_departemen($value= '', $primary_key = null)
	{
		return '<input type="text" id="field-Departemen" style="border:0;" value="" readonly>';
	}
	
	function add_field_callback_Jabatan($value= '', $primary_key = null)
	{
		return '<input type="text" id="field-Jabatan" style="border:0;" value="" readonly>';
	}
	
	function _callback_column_jumlahhari($value,$row)
	{
		fire_print('log','jumlah hari model');
		$jumlahHari = $this->undangan_model->getJumlahHari(substr($row->TanggalMulai,0,10),substr($row->TanggalSelesai,0,10));
		fire_print('log','Hari mulai :'.$row->TanggalMulai.',hari selesai:'.$row->TanggalSelesai);
		fire_print('log','jumlah hari kerja column:'.$jumlahHari);
		return $jumlahHari;
	}
	
	function getCurrentUserDepartemen()
	{
		$session_data = $this->session->userdata('logged_in');
		$dataUser = $this->user->dataUser($session_data['npk']);
		if($dataUser){
			foreach($dataUser as $dataUser){
				$dataUserDetail = array(
					'jabatan'=> $dataUser->jabatan
				);
			}
		}else{
			echo 'gagal get current user';
		}
		return $dataUserDetail['jabatan'];
			
	}
	
	function _callback_status($value, $row){
		$text="test";
		switch($value)
		{
			case "PEP": $text = "Pending Approval Training Head"; break;
			case "DEP": $text = "Rencana Declined Training Head"; break;
			case "APP": $text = "Rencana Approved  HR Head"; break;
			case "PER": $text = "Pending Approval Realisasi Head"; break;
			case "DER": $text = "Realisasi Declined Training"; break;
			case "APR": $text = "Realisasi Approved Training HR Head"; break;
			
		}
		return $text;
	}
	
	function field_callback_read_Status($value='',$primary_key=null)
	{
		$text="test";
		switch($value)
		{
			case "PEP": $text = "Pending Approval Training Head"; break;
			case "DEP": $text = "Rencana Declined Training Head"; break;
			case "APP": $text = "Rencana Approved  HR Head"; break;
			case "PER": $text = "Pending Approval Realisasi Head"; break;
			case "DER": $text = "Realisasi Declined Training"; break;
			case "APR": $text = "Realisasi Approved Training HR Head"; break;
			
		}
		return $text;
	}
	
	function field_callback_read_Tanggal($value, $primary_key=null){
		$dataUndangan = $this->db->get_where('undangan',array("KodeUndangan"=>$primary_key));		
		$dateStart = new DateTime($value);
		$dateEnd = new DateTime($dataUndangan->row(1)->TanggalSelesai);
		$tanggalmulai = $dateStart->format('j M Y');
		$tanggalselesai = $dateEnd->format('j M Y');
		if($tanggalmulai == $tanggalselesai){
			$tanggal = $tanggalmulai;
		}else{
			$tanggal = $tanggalmulai." s/d ".$tanggalselesai;
		}
		$pukul = $dateStart->format('H:i') ." - ". $dateEnd->format('H:i');
		return $tanggal;
	}
	
	function field_callback_read_Pukul($value, $primary_key=null){
		$dataUndangan = $this->db->get_where('undangan',array("KodeUndangan"=>$primary_key));		
		$dateStart = new DateTime($dataUndangan->row(1)->TanggalMulai);
		$dateEnd = new DateTime($value);
		$tanggalmulai = $dateStart->format('j M Y');
		$tanggalselesai = $dateEnd->format('j M Y');
		if($tanggalmulai == $tanggalselesai){
			$tanggal = $tanggalmulai;
		}else{
			$tanggal = $tanggalmulai." s/d ".$tanggalselesai;
		}
		$pukul = $dateStart->format('H:i') ." - ". $dateEnd->format('H:i');
		return $pukul;
	}
	
	function field_callback_JamMulai($value='',$primary_key=null)
	{
		return "<input name='JamMulai' type='time' value='$value'>";
	}
	
	function field_callback_JamSelesai($value='',$primary_key=null)
	{
		return "<input name='JamSelesai' type='time' value='$value'>";
	}
	
	function _callback_Tanggal($value, $row){
		$dateStart = new DateTime($row->TanggalMulai);
		$dateEnd = new DateTime($row->TanggalSelesai);
		$tanggalmulai = $dateStart->format('j M Y');
		$tanggalselesai = $dateEnd->format('j M Y');
		if($tanggalmulai == $tanggalselesai){
			$tanggal = $tanggalmulai;
		}else{
			$tanggal = $tanggalmulai." s/d ".$tanggalselesai;
		}
		$pukul = $dateStart->format('H:i') ." - ". $dateEnd->format('H:i');
		return $tanggal;
	}
	
	function _callback_Pukul($value, $row){
		$dateStart = new DateTime($row->TanggalMulai);
		$dateEnd = new DateTime($row->TanggalSelesai);
		$tanggalmulai = $dateStart->format('j M Y');
		$tanggalselesai = $dateEnd->format('j M Y');
		if($tanggalmulai == $tanggalselesai){
			$tanggal = $tanggalmulai;
		}else{
			$tanggal = $tanggalmulai." s/d ".$tanggalselesai;
		}
		$pukul = $dateStart->format('H:i') ." - ". $dateEnd->format('H:i');
		return $pukul;
	}
}
?>