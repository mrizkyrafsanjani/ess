<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class TrainingController extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('undangan_model','',TRUE);
		$this->load->model('cuti_model','',TRUE);
		$this->load->model('training_model','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('departemen','',TRUE);
		$this->load->model('mstrgolongan','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->helper('date');
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("66"))
			{
				$this->npkLogin = $session_data['npk'];
				
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
    }
	
	function ViewTrainingUser($KodeUserTask='') //ViewUser
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("66"))
					{
						if($KodeUserTask == '')
							$KodeUserTask = 'non';
						$data = array(
							'title' => 'View Training User',
							'admin' => '0',							
							'npk' => $this->npkLogin,
							'kodeusertask' => $KodeUserTask
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Training/trainingUser_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
	}
	
	function ViewUndanganUser($KodeUserTask='') //ViewUser
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("66"))
					{
						if($KodeUserTask == '')
							$KodeUserTask = 'non';
						$data = array(
							'title' => 'View Undangan User',
							'admin' => '0',							
							'npk' => $this->npkLogin,
							'kodeusertask' => $KodeUserTask
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Training/undanganUser_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
	}
	
	function ApprovalAwalTraining($KodeUserTask)
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("69"))
					{
						fire_print('log',"KodeUserTask : $KodeUserTask");
						$data = $this->training_model->getTraining($KodeUserTask);
			
						foreach($data as $row){
							$historycatatan = $this->usertask->getCatatan($row->KodeTraining);
							
							$identitas = $this->user->dataUser($row->NPK);
							foreach($identitas as $datauser){
								$nama = $datauser->nama;
								$departemen = $datauser->departemen;
								$jabatan = $datauser->jabatan;
							}
							$request = array(
								'realisasi' => '0',
								'title' => 'Approval Awal Request Kebutuhan Training',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'nama' => $nama,
								'departemen' => $departemen,
								'jabatan'=>$jabatan,
								'npk' => $row->NPK,
								'programtraining' => $row->ProgramTraining,
								'penyelenggara' => $row->Penyelenggara,
								'alasantraining' => $row->AlasanTraining,
								'detailalasantraining' => $row->DetailAlasanTraining,
								'tuntutanpekerjaan' => $row->TuntutanPekerjaan,
								'kemampuanyangdiharapkan' => $row->KemampuanYangDiharapkan,
								'jobdescription' => $row->JobDescription,
								'biayatraining' => $row->BiayaTraining,
								'historycatatan' => $historycatatan
							);
							
						}
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','Training/requestTraining_view',$request);
					}
				}
				else
				{
					redirect("login?u=Training/trainingController/ApprovalAwalTraining/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
	}
	
	function ApprovalTraining($KodeUserTask)
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("69"))
					{
						fire_print('log',"KodeUserTask : $KodeUserTask");
						$data = $this->training_model->getTraining($KodeUserTask);
			
						foreach($data as $row){
							$historycatatan = $this->usertask->getCatatan($row->KodeTraining);
							
							$identitas = $this->user->dataUser($row->NPK);
							foreach($identitas as $datauser){
								$nama = $datauser->nama;
								$departemen = $datauser->departemen;
								$jabatan = $datauser->jabatan;
							}
							$request = array(
								'realisasi' => '1',
								'title' => 'Approval Request Kebutuhan Training',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'nama' => $nama,
								'departemen' => $departemen,
								'jabatan'=>$jabatan,
								'npk' => $row->NPK,
								'programtraining' => $row->ProgramTraining,
								'penyelenggara' => $row->Penyelenggara,
								'alasantraining' => $row->AlasanTraining,
								'detailalasantraining' => $row->DetailAlasanTraining,
								'tuntutanpekerjaan' => $row->TuntutanPekerjaan,
								'kemampuanyangdiharapkan' => $row->KemampuanYangDiharapkan,
								'jobdescription' => $row->JobDescription,
								'biayatraining' => $row->BiayaTraining,
								'historycatatan' => $historycatatan
							);
							
						}
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','Training/requestTrainingApprove_view',$request);
					}
				}
				else
				{
					redirect("login?u=Training/trainingController/ApprovalTraining/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
	}
	
	function CetakTraining($KodeTraining)
	{
			try
			{
				$KodeUserTask = $this->input->post('kodeusertask');
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("66"))
					{
						//fire_print('log',"KodeUserTask Cetak: $KodeUserTask");
						$data = $this->training_model->getTraining($KodeTraining);
			
						foreach($data as $row){
							$historycatatan = $this->usertask->getCatatan($row->KodeTraining);
							
							$identitas = $this->user->dataUser($row->NPK);
							foreach($identitas as $datauser){
								$nama = $datauser->nama;
								$departemen = $datauser->departemen;
								$jabatan = $datauser->jabatan;
								$atasan = $datauser->atasan;
								
								$dic = $this->user->getDIC($datauser->KodeDepartemen)->Nama;
								
							}
							$query = $this->db->get_where('mstruser',array('Jabatan'=>'6'));
							$hrgahead = $query->row(1)->Nama;
							$request = array(
								'realisasi' => '0',
								'title' => 'Approval Awal Request Kebutuhan Training',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'nama' => $nama,
								'departemen' => $departemen,
								'jabatan'=>$jabatan,
								'npk' => $row->NPK,
								'programtraining' => $row->ProgramTraining,
								'penyelenggara' => $row->Penyelenggara,
								'alasantraining' => $row->AlasanTraining,
								'detailalasantraining' => $row->DetailAlasanTraining,
								'tuntutanpekerjaan' => $row->TuntutanPekerjaan,
								'kemampuanyangdiharapkan' => $row->KemampuanYangDiharapkan,
								'jobdescription' => $row->JobDescription,
								'biayatraining' => $row->BiayaTraining,
								'historycatatan' => $historycatatan,
								'hrgahead' =>$hrgahead,
								'atasan'=>$atasan,
								'dic' =>$dic
							);
							
						}
						
						$this->load->helper(array('form','url'));
						$this->load->view('Training/requestTrainingCetak_view',$request);
						//$this->template->load('default','Training/requestTrainingCetak_view',$request);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
	
	
	
	}
	
	function ajax_prosesApproval()
	{
			try
			{
				$error = 0;
				$success = "Berhasil";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				$query = $this->training_model->getTraining($KodeUserTask);
				
				if($query){
					foreach($query as $row){
						$statusRencana = substr($row->Status, -1);
						$KodeTraining = $row->KodeTraining;
						$pengajuTraining = $row->NPK;
						$statusTraining = substr($row->Status, 0, 2); //dapatkan PE
					}
				}else{
					$success = "Tidak ada data";
				}
				
				if($statusTraining == 'PE')
				{
					$this->db->trans_begin();
					
					//update status Training
					if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'TN') && $status == 'AP')
					{
						$this->training_model->updateStatusTraining($KodeUserTask,$status.$statusRencana,$this->npkLogin);
					}
					
					if($status == 'DE')
					{
						$this->training_model->updateStatusTraining($KodeUserTask,$status.$statusRencana,$this->npkLogin);
					}
					
					//update user task sekarang
					$this->usertask->updateStatusUserTask($KodeUserTask,$status.$statusRencana,$this->npkLogin,$catatan);
					
					//create user task baru untuk next proses
					$KodeUserTaskNext = generateNo('UT');			
					if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeTraining,$KodeUserTaskNext,"TN".$status,$pengajuTraining))
					{
						$success = "GAGAL USERTASK";
						$error += 1;
					}
					
					if ($error > 0)
					{
						fire_print('log','trans rollback approve Training ');
						$this->db->trans_rollback();
					}
					else
					{
						fire_print('log','trans commit approve Training ');
						$this->db->trans_commit();
					}
					fire_print('log','Proses Trans Approve Training selesai ');
				}
				else
				{
					$success = "GAGAL UPDATE STATUS";
				}
				
				echo $success;
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
	}
		
		function ajax_prosesApprovalRealisasi()
		{
			try
			{
				$error = 0;
				$success = "Berhasil";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				
				$query = $this->training_model->getTraining($KodeUserTask);
				if($query){
					foreach($query as $row){
						$statusRencana = 'R'; //substr($row->StatusApproval, -1);
						$KodeTraining = $row->KodeTraining;
						$pengajuTraining = $row->CreatedBy;
					}
				}else{
					$success = "Tidak ada data";
				}
				$this->db->trans_begin();
				//update status ATK
				if($this->training_model->updateStatusTraining($KodeUserTask,$status.$statusRencana,$this->npkLogin))
				{
					//update user task sekarang
					$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan);
						
					if($status == "DE"){
						//create user task baru untuk next proses
						$KodeUserTaskNext = generateNo('UT');			
						if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeTraining,$KodeUserTaskNext,"TN".$status,$pengajuTraining))
						{
							$success = "0";
							$error += 1;
						}
					}
				}
				else
				{
					$success = "Sistem Gagal";
					$error += 1;
				}
				
				if ($error > 0)
				{
					fire_print('log','trans rollback approve Training ');
					$this->db->trans_rollback();
				}
				else
				{
					fire_print('log','trans commit approve Training ');
					$this->db->trans_commit();
				}
				echo $success;				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function EditTraining($KodeUserTask='') //user
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					$KodeTraining = $this->training_model->getEdit($KodeUserTask);
					
					fire_print('log',$KodeTraining);
					redirect('Training/Training/Edit/edit/'.$KodeTraining,'refresh');
				}
				else
				{
					redirect("login?u=Training/trainingController/EditTraining/$KodeUserTask",'refresh');
				}
				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
	function ApprovalAwalUndangan($KodeUserTask)
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("69"))
					{
						fire_print('log',"KodeUserTask : $KodeUserTask");
						$data = $this->undangan_model->getUndangan($KodeUserTask);
			
						foreach($data as $row){
							$historycatatan = $this->usertask->getCatatan($row->KodeUndangan);
							
							$identitas = $this->user->dataUser($row->NPK);
							foreach($identitas as $datauser){
								$nama = $datauser->nama;
								$departemen = $datauser->departemen;
								$jabatan = $datauser->jabatan;
							}
							
							$dateStart = new DateTime($row->TanggalMulai);
							$dateEnd = new DateTime($row->TanggalSelesai);
							$tanggalmulai = $dateStart->format('j M Y');
							$tanggalselesai = $dateEnd->format('j M Y');
							if($tanggalmulai == $tanggalselesai){
								$tanggal = $tanggalmulai;
							}else{
								$tanggal = $tanggalmulai." s/d ".$tanggalselesai;
							}
							$pukul = $dateStart->format('H:i') ." - ". $dateEnd->format('H:i');
							$request = array(
								'realisasi' => '0',
								'title' => 'Approval Awal Undangan',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'nama' => $nama,
								'departemen' => $departemen,
								'jabatan'=>$jabatan,
								'npk' => $row->NPK,
								'penyelenggara' => $row->Penyelenggara,
								'programtraining' => $row->ProgramTraining,
								'tanggalmulai' => $row->TanggalMulai,
								'tanggalselesai' => $row->TanggalSelesai,
								'tanggal' => $tanggal,
								'pukul' => $pukul,
								'selisihhari'=> s_datediff('d',substr($row->TanggalMulai,0,10),substr($row->TanggalSelesai,0,10))+1,
								'alamattraining' => $row->AlamatTraining,
								'historycatatan' => $historycatatan
							);
							
						}
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','Training/requestUndangan_view',$request);
					}
				}
				else
				{
					redirect("login?u=Training/trainingController/ApprovalAwalUndangan/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
	}
	
	function ApprovalUndangan($KodeUserTask)
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("69"))
					{
						fire_print('log',"KodeUserTask : $KodeUserTask");
						$data = $this->undangan_model->getUndangan($KodeUserTask);
			
						foreach($data as $row){
							$historycatatan = $this->usertask->getCatatan($row->KodeUndangan);
							
							$identitas = $this->user->dataUser($row->NPK);
							foreach($identitas as $datauser){
								$nama = $datauser->nama;
								$departemen = $datauser->departemen;
								$jabatan = $datauser->jabatan;
							}

							$dateStart = new DateTime($row->TanggalMulai);
							$dateEnd = new DateTime($row->TanggalSelesai);
							$tanggalmulai = $dateStart->format('j M Y');
							$tanggalselesai = $dateEnd->format('j M Y');
							if($tanggalmulai == $tanggalselesai){
								$tanggal = $tanggalmulai;
							}else{
								$tanggal = $tanggalmulai." s/d ".$tanggalselesai;
							}
							$pukul = $dateStart->format('H:i') ." - ". $dateEnd->format('H:i');

							$request = array(
								'realisasi' => '1',
								'title' => 'Approval Undangan',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'nama' => $nama,
								'departemen' => $departemen,
								'jabatan'=>$jabatan,
								'npk' => $row->NPK,
								'penyelenggara' => $row->Penyelenggara,
								'selisihhari'=> s_datediff('d',substr($row->TanggalMulai,0,10),substr($row->TanggalSelesai,0,10))+1,
								'programtraining' => $row->ProgramTraining,
								'tanggalmulai' => $row->TanggalMulai,
								'tanggalselesai' => $row->TanggalSelesai,
								'tanggal'=>$tanggal,
								'pukul'=>$pukul,
								'alamattraining' => $row->AlamatTraining,
								'historycatatan' => $historycatatan
							);
							
						}
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','Training/requestUndanganApproval_view',$request);
					}
				}
				else
				{
					redirect("login?u=Training/trainingController/ApprovalUndangan/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
	}
	
	function ajax_prosesApprovalUndangan()
	{
		try
		{
			$error = 0;
			$success = "Berhasil";
			$KodeUserTask = $this->input->post('kodeusertask');
			$status = $this->input->post('status');
			$catatan = $this->input->post('catatan');
			$query = $this->undangan_model->getUndangan($KodeUserTask);
			
			if($query){
				foreach($query as $row){
					$statusRencana = substr($row->Status, -1);
					$KodeUndangan = $row->KodeUndangan;
					$pengajuUndangan = $row->CreatedBy;
					$statusUndangan = substr($row->Status, 0, 2); //dapatkan PE
				}
			
			
				if($statusUndangan == 'PE')
				{
					$this->db->trans_begin();
					
					//update status Undangan
					if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'TS') && $status == 'AP')
					{
						
						
						$this->undangan_model->updateStatusUndangan($KodeUserTask,$status.$statusRencana, $this->npkLogin);
						
								
					}
					
					if($status == 'DE')
					{
						
						$this->undangan_model->updateStatusUndangan($KodeUserTask,$status.$statusRencana, $this->npkLogin);
						
					}
					
					//update user task sekarang
					if(!$this->usertask->updateStatusUserTask($KodeUserTask,$status.$statusRencana,$this->npkLogin,$catatan))
						$error += 1;
					
					//create user task baru untuk next proses
					$KodeUserTaskNext = generateNo('UT');			
					if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeUndangan,$KodeUserTaskNext,"TS".$status,$pengajuUndangan))
					{
						$success = "GAGAL USERTASK";
						$error += 1;
					}
					
					if ($error > 0)
					{
						fire_print('log','trans rollback approve Undangan ');
						$this->db->trans_rollback();
					}
					else
					{
						fire_print('log','trans commit approve Undangan ');
						$this->db->trans_commit();
					}
					fire_print('log','Proses Trans Approve Undangan selesai ');
				}
				else
				{
					$success = "GAGAL UPDATE STATUS";
				}
			}else{
				$success = "Tidak ada data";
			}
			echo $success;
		}
		catch(Exception $e)
		{
			$this->db->trans_rollback();
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
		
		function ajax_prosesApprovalRealisasiUndangan()
		{
			try
			{
				$error = 0;
				$success = "Berhasil";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				$biayatraining = $this->input->post('BiayaTraining');
				$sertifikasi = $this->input->post('Sertifikasi');
				$nilai = $this->input->post('Nilai');
				$keterangan = $this->input->post('Keterangan');
				
				$this->db->trans_begin();
				
				$query = $this->undangan_model->getUndangan($KodeUserTask);
				if($query){
					foreach($query as $row){
						$statusRencana = 'R'; //substr($row->StatusApproval, -1);
						$KodeUndangan = $row->KodeUndangan;
						$pengajuUndangan = $row->CreatedBy;
					}
					
					fire_print('log','update undangan realisasi');
					if(!$this->undangan_model->updateStatusUndanganRealisasi($KodeUserTask,$status.$statusRencana, $this->npkLogin, $biayatraining, $nilai, $sertifikasi, $keterangan, $KodeUndangan))
					{
						$error += 1;
						$success = "Gagal mengupdate status undangan training.";
					}
					//update user task sekarang
					if(!$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan)){
						$error += 1;
						$success .= " Gagal mengupdate status user task undangan training";
					}
				}else{
					$success = "Tidak ada data";
				}
				
				
				if($error > 0){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				echo $success;				
			}
			catch(Exception $e)
			{
				$this->db->trans_rollback();
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function EditUndangan($KodeUserTask='') //user
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					$KodeUndangan = $this->undangan_model->getEdit($KodeUserTask);
				
					fire_print('log',$KodeUndangan);
					redirect('Training/Undangan/Edit/edit/'.$KodeUndangan,'refresh');
				}
				else
				{
					redirect("login?u=Training/trainingController/EditUndangan/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_getDepartemen()
	{
		try
		{
			
			$npk = $this->input->post('NPK');
			$departemenList =  $this->user->dataUser($npk);
			foreach($departemenList as $rw)
			{
				$departemen = $rw->departemen;
				$nama = $rw->nama;
				$jabatan = $rw->jabatan;
			
			}

			
			echo $departemen; //untuk column callback di view
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function ajax_getJabatan()
	{
		try
		{
			
			$npk = $this->input->post('NPK');
			$departemenList =  $this->user->dataUser($npk);
			foreach($departemenList as $rw)
			{
				$departemen = $rw->departemen;
				$nama = $rw->nama;
				$jabatan = $rw->jabatan;
			
			}

			echo $jabatan; //untuk column callback di view
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function ajax_getProgramTraining()
	{
		try
		{
			
			$npk = $this->input->post('NPK');
			$kodeProgramTraining = $this->input->post('ProgramTraining');
			$htmlText = '<select id="field_KodeTraining_chzn" name="KodeTraining">';
			$programList =  $this->undangan_model->getProgramTraining($npk);
			$count = 0;
			if($programList){
				foreach($programList as $row){
					$selected = "";
					$htmlText .= '<option ';
					if($kodeProgramTraining == $row->KodeTraining){
						$selected = "selected";
					}
					$htmlText .= " value='".$row->KodeTraining."' $selected>".$row->ProgramTraining."</option>";
					$count++;
				}
			}
			$htmlText .= '</select>';
			echo $htmlText;
			
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
		
	
	
	
		
		
		
		
		
}
?>