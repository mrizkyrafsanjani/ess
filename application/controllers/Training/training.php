<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Training extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('training_model','',TRUE);
		$this->load->model('menu','',TRUE);
		$this->load->model('usertask','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$this->load->library('grocery_crud');
		$this->load->helper('date');
		
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("66"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_Training();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	public function Edit()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("66"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_Training('');
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	public function viewUser($tahun='',$bulan='',$nama='',$NPKuser='',$KodeUserTask='')
	{
		if($KodeUserTask == 'non')
			$KodeUserTask = '';
		if($nama == 'non')
			$nama = '';
		if($tahun == 'non')
			$tahun = date('Y');
		if($bulan == 'non')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("66"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_Training('viewUser',$nama,$tahun,$bulan,$KodeUserTask);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function viewAdmin($tahun='',$bulan='',$nama='non')
	{
		if($nama == 'non')
			$nama = 'xxx';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("66"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_Training('viewAdmin',$nama,$tahun,$bulan);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function _Training($page='',$nama='',$tahun='',$bulan='',$KodeUserTask='')
    {
		
		try{
			$jabatanUserLogin = $this->getJabatan($this->npkLogin);			
			$crud = new grocery_crud();			
			$state = $crud->getState();			
			
			$crud->set_table('training');
			$crud->columns('ProgramTraining','Penyelenggara','KodeAlasanTraining','Status');
			
			
			
			$crud->where('training.deleted','0');
			$crud->set_relation('KodeAlasanTraining','alasantraining','AlasanTraining',array('deleted' => '0'));
			
			$crud->set_rules('BiayaTraining','Biaya Training','integer');
			
			$crud->display_as('NamaKaryawan','Nama');
			$crud->display_as('ProgramTraining','Program Training');
			$crud->display_as('KodeAlasanTraining','Alasan Training');
			$crud->display_as('DetailAlasanTraining','Detail Alasan Training');
			$crud->display_as('TuntutanPekerjaan','Tuntutan pekerjaan yang belum dapat dipenuhi');
			$crud->display_as('KemampuanYangDiharapkan','Kemampuan karyawan yang diharapkan diperoleh melalui training');
			$crud->display_as('JobDescription','Job Description (uraian jabatan) dan job qualification (kualifikasi jabatan / tuntutan jabatan) dari pekerjaan yang didukung oleh training yang diusulkan');
			$crud->display_as('BiayaTraining','Biaya Training');
			$crud->unset_texteditor('DetailAlasanTraining');
			$crud->unset_texteditor('TuntutanPekerjaan');
			$crud->unset_texteditor('KemampuanYangDiharapkan');
			$crud->unset_texteditor('JobDescription');
			$crud->unset_read();
			//$crud->unset_edit();
			$crud->unset_delete();
			$crud->unset_export();
			$crud->unset_print();
			$crud->unset_back_to_list();
			
			if($jabatanUserLogin == 26){ //jika HR Analyst
				$crud->set_subject('Request Training Umum');
				$crud->set_relation('NPK','mstruser','Nama',array('deleted'=>'0','TanggalBerhenti'=>'0000-00-00'));
				$crud->fields('NPK','Departemen','Jabatan','ProgramTraining','Penyelenggara','BiayaTraining');
				$crud->required_fields('ProgramTraining','Penyelenggara');
			}
			else
			{
				$crud->set_subject('Request Training Needs');
				$crud->fields('NPK','NamaKaryawan','Departemen','Jabatan','ProgramTraining','Penyelenggara','KodeAlasanTraining','DetailAlasanTraining','TuntutanPekerjaan','KemampuanYangDiharapkan','JobDescription','BiayaTraining');
				$crud->callback_field('NamaKaryawan',array($this,'_callback_nama'));
				$crud->callback_field('NPK',array($this,'_callback_npk'));
				$crud->callback_field('Departemen',array($this,'_callback_departemen'));
				$crud->callback_field('Jabatan',array($this,'add_field_callback_Jabatan'));
				$crud->required_fields('ProgramTraining','Penyelenggara','KodeAlasanTraining','TuntutanPekerjaan','KemampuanYangDiharapkan');
			}
			
			$crud->callback_column('Status',array($this,'_callback_Status'));			
			$crud->callback_insert(array($this,'_insert_training'));
			$crud->callback_update(array($this,'_update'));
			$crud->callback_delete(array($this,'_delete'));	
			
			$crud->where('training.CreatedBy',$this->npkLogin);
			
			
			if($nama != '')
			{
				$crud->like('training.CreatedBy',$nama);
			}
			if($tahun != '')
			{
				$crud->where('YEAR(training.CreatedOn)',$tahun);
				$crud->unset_edit();
				$crud->unset_add();
			}
			if($bulan != '')
			{
				$crud->where('MONTH(training.CreatedOn)', $bulan);
				$crud->unset_edit();
				$crud->unset_add();
			}
			
			fire_print('log',"nama: $nama, tahun: $tahun, bulan: $bulan");
			
			$js = "
			<script>
				function loadDataKaryawan(){
					var cmbNamaKaryawan = $('#field-NPK');
					var Departemen = $('#field-Departemen'); 
					var Jabatan = $('#field-Jabatan');
					$.ajax({
						 type: 'POST',
						 url: '" .base_url()."index.php/training/trainingController/ajax_getDepartemen',
						 data: {NPK: cmbNamaKaryawan.val(), Departemen : Departemen.val()},
						}).done(function(response){
							Departemen.val(response);
					});
					$.ajax({
						 type: 'POST',
						 url: '" .base_url()."index.php/training/trainingController/ajax_getJabatan',
						 data: {NPK: cmbNamaKaryawan.val(), Jabatan: Jabatan.val() },
						}).done(function(response){
							Jabatan.val(response);
					});
				}
				
				$(document).ready(function() {
					var cmbNamaKaryawan = $('#field-NPK');
					cmbNamaKaryawan.change(loadDataKaryawan);
					if(cmbNamaKaryawan.val() != ''){
						loadDataKaryawan();
					}
				});
			</script>";
			
			$output = $crud->render();
			$output->output.=$js;
			$this-> _outputview($output,$page); 
		}
		
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
	
	function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Request Training',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
	
		if($page!='')
		{
			$this->load->view('Training/trainingSimple_view',$data);
		}
		else
		{
			$this->template->load('default','templates/CRUD_view',$data);
		}
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	public function getJabatan($NPK)
	{
		$dataUser = $this->user->dataUser($NPK);
		foreach($dataUser as $dtU){
			$jabatanUserLogin = $dtU->KodeJabatan;
		}
		return $jabatanUserLogin;
	}
	
	function _insert_training($post_array){	
		try
		{
			$error = 0;
			$KodeTraining = generateNo('TN');
			$post_array['KodeTraining'] = $KodeTraining;
			
			$jabatanUserLogin = $this->getJabatan($this->npkLogin);
			if($jabatanUserLogin != 26){ //jika bukan HR Analyst
				$post_array['NPK'] = $this->npkLogin;
			}
			$post_array['Status'] = 'PEP'; //Pending Plan
			$post_array['Deleted'] = '0';
			$post_array['CreatedOn'] = date('Y-m-d H:i:s');
			$post_array['CreatedBy'] = $this->npkLogin;
			unset($post_array['Jabatan']);
			unset($post_array['Departemen']);
			
			$this->db->trans_begin();
			if(!$this->db->insert('training',$post_array))
				$error += 1;
			fire_print('log',"Kode training : $KodeTraining");
			
			$KodeUserTask = generateNo('UT');
			$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeTraining,$KodeUserTask,"TN");
			if(!$hasilTambahUserTask)
				$error += 1;
			fire_print('log',"insert training. Error: $error");
			if($error > 0)
			{
				$this->db->trans_rollback();
				return false;
			}
			else
			{
				$this->db->trans_commit();
				return true;
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function _update($post_array,$primary_key){
		$error = 0;
		$query = $this->db->get_where('training',array('KodeTraining'=>$primary_key));
		$datatraining = $query->row(1);
		if(substr($datatraining->Status,0,2) == 'DE'){
			$post_array['Status'] = 'PEP'; //Pending Plan
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			unset($post_array['Jabatan']);
			unset($post_array['Departemen']);
			
			$this->db->trans_begin();
			
			if(!$this->db->update('training',$post_array,array('KodeTraining' => $primary_key)))
				$error += 1;
			$KodeTraining = $primary_key;
			//usertask lama di nyatakan statusnya AP
			
			$usertasklama = $this->usertask->getLastUserTask($KodeTraining);
			$KodeUserTaskLama = '';
			if($usertasklama){
				foreach($usertasklama as $row){
					$KodeUserTaskLama = $row->KodeUserTask;
					
				}
				if(!$this->usertask->updateStatusUserTask($KodeUserTaskLama,'AP',$this->npkLogin))
					$error += 1;
			}
			fire_print('log',$KodeUserTaskLama);
			//buat user task baru untuk proses approval training
			$KodeUserTask = generateNo('UT');
			
			if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeTraining,$KodeUserTask,"TN"))
			{
				$error += 1;
			}
			
			if($error > 0){
				$this->db->trans_rollback(); 
				return false;
			}else{
				$this->db->trans_commit(); 
				return true;
			}
		}
		else
		{
			return false;
		}
			
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('training',$post_array,array('KodeTraining' => $primary_key));
	}
	
	function getCurrentUserDepartemen()
	{
		$session_data = $this->session->userdata('logged_in');
		$dataUser = $this->user->dataUser($session_data['npk']);
		if($dataUser){
			foreach($dataUser as $dataUser){
				$dataUserDetail = array(
					'jabatan'=> $dataUser->jabatan
				);
			}
		}else{
			echo 'gagal get current user';
		}
		return $dataUserDetail['jabatan'];
			
	}
	
	function _callback_nama($value= '', $primary_key = null)
	{
		$datauser = $this->user->findUserByNPK($this->npkLogin);
		$nama = '';
		foreach($datauser as $dt)
		{
			$nama = $dt->Nama;
		}
		//return '<input type="text" style="border:0;" value="'.$nama.' ( '.$this->npkLogin .' )" readonly>';
		return '<input type="text" style="border:0;" value="'.$nama.'" readonly>';
	}
	
	function _callback_npk($value= '', $primary_key = null)
	{
		return '<input type="text" style="border:0;" value="'.$this->npkLogin.'" readonly>';
	}
	
	function _callback_departemen($value= '', $primary_key = null)
	{
		$datauser = $this->user->get_departemen_by_npk($this->npkLogin);
		$departemen = '';
		foreach($datauser as $dt)
		{
			$departemen = $dt->departemen;
		}
		return '<input type="text" style="border:0;" value="'.$departemen.'" readonly>';
	}
	
	function add_field_callback_Jabatan($value= '', $primary_key = null)
	{
		$value = $this->getCurrentUserDepartemen();
		return '<input type="text" name="Jabatan" value="'.$value.'" readonly>';
	}
	
	function _callback_status($value, $row){
		$text="test";
		switch($value)
		{
			case "PEP": $text = "Pending Approval Training Head"; break;
			case "DEP": $text = "Rencana Declined Training Head"; break;
			case "APP": $text = "Rencana Approved  HR Head"; break;
			case "PER": $text = "Pending Approval Realisasi Head"; break;
			case "DER": $text = "Realisasi Declined Training"; break;
			case "APR": $text = "Realisasi Approved Training HR Head"; break;
			
		}
		return $text;
	}
	
		
	
	
	

	
	

	
	
}
?>