<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
session_start();
class TempBudgetController extends CI_Controller
{
	var $npkLogin;
	function __construct()
	{
		parent::__construct();
		$this->load->model('bm_activity_model', '', TRUE);
		$this->load->model('bm_tempbudget_model', '', TRUE);
		$this->load->model('bm_tempbudgetheader_model', '', TRUE);
		$this->load->model('bm_budget_model', '', TRUE);
		$this->load->model('dtltrkrwy_model', '', TRUE);
		$this->load->model('usertask', '', TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
	}

	function index()
	{
		try { } catch (Exception $e) {
			log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
		}
	}

	function uploadBudget()
	{
		if ($this->session->userdata('logged_in')) {
			if (check_authorizedByName("Upload Budget")) {
				$data = array(
					'title' => 'Upload Budget'
				);
				$this->load->helper(array('form', 'url'));
				$this->template->load('default', 'BudgetMonitoring/uploadBudget_view', $data);
			}
		} else {
			redirect('login?u=' . substr($_SERVER["REQUEST_URI"], stripos($_SERVER["REQUEST_URI"], "index.php/") + 10), 'refresh');
		}
	}

	function approvalTempBudget($KodeUserTask)
	{
		try {
			if ($this->session->userdata('logged_in')) {
				if (check_authorizedByName("Approval Budget")) {
					$data = array(
						'title' => 'Approval Rencana Lembur User',
						'kodeusertask' => $KodeUserTask
					);

					$this->load->helper(array('form', 'url'));
					$this->template->load('default', 'BudgetMonitoring/approvalTempBudget_view', $data);
				}
			} else {
				redirect("login?u=BudgetMonitoring/TempBudgetController/ApprovalTempBudget/$KodeUserTask", 'refresh');
			}
		} catch (Exception $e) {
			log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
		}
	}

	private function _validateRow($rowdata)
	{
		$error = "";
		try {
			//0:kode activity, 1:lock/unlock, 2:tahun, 3-14:nominal pada bulan x
			if (count($rowdata) != 15) {
				$error .= "Jumlah kolom file upload harus berjumlah 15 \n";
			} else {
				if (!$this->bm_activity_model->getActivity($rowdata[0])) {
					$error .= "Tidak ada kode activity " . $rowdata[0];
				}

				if ($rowdata[1] != "Unlock" && $rowdata[1] != "Lock") {
					$error .= "Kolom status budget harus bernilai Lock atau Unlock \n";
				}

				for ($i = 2; $i < 15; $i++) {
					if (!is_numeric($rowdata[$i])) {
						$error .= "Kolom tahun dan nominal budget tiap bulan harus berisi angka \n";
					}
				}
			}
			return $error;
		} catch (Exception $e) {
			log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
			throw new Exception('Something really gone wrong', 0, $e);
		}
	}

	public function ajax_uploadBudget()
	{
		require 'application/third_party/PHPExcel/PHPExcel/IOFactory.php';
		try {
			$data = $_POST['data'];
			$filename = $_POST['name'];


			$uploadpath = "./assets/uploads/temp/";
			$inputfilename = $uploadpath . $filename;
			$fp = fopen($inputfilename, 'w');
			fwrite($fp, $data);
			fclose($fp);

			$inputfiletype = PHPExcel_IOFactory::identify($inputfilename);
			$objReader = PHPExcel_IOFactory::createReader($inputfiletype);
			$objPHPExcel = $objReader->load($inputfilename);

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			//cek apakah ada yang pending
			if (!$this->bm_tempbudgetheader_model->isexistpending($this->npkLogin)) {
				$this->db->trans_start();

				$KodeTempBudgetHeader = generateNo('TB');
				$this->bm_tempbudgetheader_model->insertoverwrite($this->npkLogin, $KodeTempBudgetHeader);

				//  Loop through each row of the worksheet in turn
				for ($row = 2; $row <= $highestRow; $row++) {
					//  Read a row of data into an array
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
					$exceldata[] = $rowData[0];

					$error = $this->_validateRow($rowData[0]);
					if ($error != "") {
						echo "Error pada file upload di baris " . $row . ": \n\n" . $error;
						throw new Exception("Error pada file upload : " . $error);
					}

					if ($this->bm_tempbudget_model->insertoverwrite($rowData[0], $this->npkLogin, $KodeTempBudgetHeader)) {
						echo "row" . $row;
					} else {
						echo "Error!";
					}
				}
				$this->db->trans_complete();
			} else {
				echo "Error! Terdapat master budget yang belum di approve/decline!";
			}
		} catch (Exception $e) {
			log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
		}
	}

	public function generateHtmlUploadData($result, $dpa = '')
	{
		$totalOpex = 0;
		$totalCapex = 0;
		if ($dpa != '') {
			$htmlDPA = "<h1>DPA $dpa</h1>";
		} else {
			$htmlDPA = "<h1>Semua DPA</h1>";
		}
		$htmlOpex = "$htmlDPA<div class='col-sm-6'><h3>Opex</h3><table class='table table-bordered'><tbody>
				<tr><th style='text-align:center'>Kategori</th><th style='text-align:center'>Total MB</th></tr>";
		$htmlCapex = "<div class='col-sm-6'><h3>Capex</h3><table class='table table-bordered'><tbody>
				<tr><th style='text-align:center'>Kategori</th><th style='text-align:center'>Total MB</th></tr>";
		foreach ($result as $res) {
			if ($res->JenisPengeluaran == "Opex") {
				$htmlOpex .= "<tr><td>" . $res->NamaKategori . "</td><td style='text-align:right'>" . number_format($res->TotalBudget, 2) . "</td></tr>";
				$totalOpex += $res->TotalBudget;
			} else {
				$htmlCapex .= "<tr><td>" . $res->NamaKategori . "</td><td style='text-align:right'>" . number_format($res->TotalBudget, 2) . "</td></tr>";
				$totalCapex += $res->TotalBudget;
			}
		}
		$htmlOpex .= "<tr style='font-weight:bold'><td style='text-align:center'>Total</td><td style='text-align:right'>" . number_format($totalOpex, 2) . "</td></tr>";
		$htmlCapex .= "<tr style='font-weight:bold'><td style='text-align:center'>Total</td><td style='text-align:right'>" . number_format($totalCapex, 2) . "</td></tr>";
		$htmlOpex .= "</tbody></table></div>";
		$htmlCapex .= "</tbody></table></div>";

		return "<div class='col-sm-12 box box-default box-solid'>" . $htmlOpex . $htmlCapex . "</div>";
	}

	public function ajax_getUploadData($KodeUserTask = '')
	{
		try {
			$result = $this->bm_tempbudget_model->getUploadData($this->npkLogin, $KodeUserTask);
			if (!$result) {
				echo "Tidak ada data!";
			} else {
				$resultDPA1 = $this->bm_tempbudget_model->getUploadData($this->npkLogin, $KodeUserTask, 1);
				$resultDPA2 = $this->bm_tempbudget_model->getUploadData($this->npkLogin, $KodeUserTask, 2);

				$html = $this->generateHtmlUploadData($result);
				if ($resultDPA1)
					$htmlDPA1 = $this->generateHtmlUploadData($resultDPA1, 1);
				if ($resultDPA2)
					$htmlDPA2 = $this->generateHtmlUploadData($resultDPA2, 2);

				echo $htmlDPA1 . $htmlDPA2 . $html;
			}
		} catch (Exception $e) {
			log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
		}
	}

	public function ajax_submitUploadData()
	{
		$success = true;
		try {
			$dataSubmit = $this->bm_tempbudget_model->getUploadData($this->npkLogin);
			if ($dataSubmit) {
				$KodeTempBudgetHeader = "";
				foreach ($dataSubmit as $dt) {
					$KodeTempBudgetHeader = $dt->KodeTempBudgetHeader;
				}
				$this->db->trans_begin();
				//generate usertask
				$KodeUserTask = generateNo('UT');
				$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin, $KodeTempBudgetHeader, $KodeUserTask, 'TB');

				if ($hasilTambahUserTask) {
					//update status transaksi menjadi PE
					$this->bm_tempbudgetheader_model->updatestatustransaksi($KodeTempBudgetHeader, $this->npkLogin, "PE");
					$success = true;
				} else {
					$success = false;
				}

				$this->db->trans_complete();
				if ($success) {
					echo "Master budget telah terkirim, mohon segera diapprove oleh atasan Anda.";
				} else {
					echo "Terdapat kesalahan! Proses tidak berhasil jalan. Mohon dilakukan pemeriksaan matrix approval dll.";
				}
			} else {
				echo "Mohon upload terlebih dahulu file masterbudget!";
			}
		} catch (Exception $e) {
			log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
		}
	}

	public function ajax_ApprovalUploadData()
	{
		$success = true;
		$KodeUserTask = $_POST['kodeusertask'];
		$statusTransaksi = $_POST['statustransaksi'];
		try {
			$this->db->trans_begin();

			$KodeTempBudgetHeader = $this->dtltrkrwy_model->getNoTransaksi($KodeUserTask);

			if ($this->bm_tempbudgetheader_model->updatestatustransaksi($KodeTempBudgetHeader, $this->npkLogin, $statusTransaksi)) {
				if ($resultUpdateUT = $this->usertask->updateStatusUserTask($KodeUserTask, $statusTransaksi, $this->npkLogin)) {
					if ($statusTransaksi == "AP") {
						//proses pemindahan dari temp ke master budget sesungguhnya
						if (!$this->bm_budget_model->insertoverwrite($KodeTempBudgetHeader, $this->npkLogin)) {
							$success = false;
						};
					}
				} else {
					$success = false;
				}
			} else {
				$success = false;
			}
			$this->db->trans_complete();

			if ($success) {
				if ($statusTransaksi == "AP") {
					echo "Master budget telah diapprove";
				} else {
					echo "Master budget telah didecline";
				}
			} else {
				echo "Gagal proses! Mohon diperiksa!";
			}
		} catch (Exception $e) {
			log_message('error', $e->getMessage() . ' in ' . $e->getFile() . ':' . $e->getLine());
		}
	}
}
