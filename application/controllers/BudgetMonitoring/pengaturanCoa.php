<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanCoa extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		//$this->load->model('bm_coa_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Pengaturan COA"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_coa();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _coa()
    {
		$crud = new grocery_crud();
		$crud->set_subject('COA');
		//$crud->set_theme('datatables');
		
        $crud->set_table('bm_coa');
		$crud->where('bm_coa.deleted','0');
		
		$crud->unset_columns('Deleted');
		$crud->unset_fields('Deleted','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy');
		
		//$crud->display_as('MenuName','Nama Menu');
        //$crud->display_as('MenuOrder','Urutan Menu');
		
		$crud->required_fields('KodeCoa','NamaCoa');
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		$crud->callback_insert(array($this,'_insert_coa'));
		$crud->callback_update(array($this,'_update_coa'));
		$crud->callback_delete(array($this,'_delete_coa'));
		//$crud->unset_texteditor('Description','Url');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan COA',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);		
    }
	
	function _delete_coa($primary_key){		
		return $this->db->update('bm_coa',array('deleted' => '1','UpdatedBy' => $this->npkLogin,'UpdatedOn'=> date('Y-m-d H:i:s')),array('KodeCoa' => $primary_key));
	}
	
	function _insert_coa($post_array){
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		
		return $this->db->insert('bm_coa',$post_array);
	}
	
	function _update_coa($post_array,$primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		return $this->db->update('bm_coa',$post_array,array('KodeCoa'=>$primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */