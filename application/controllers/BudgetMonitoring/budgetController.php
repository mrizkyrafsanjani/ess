<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
session_start();
class BudgetController extends CI_Controller{
    var $npkLogin;
    function __construct()
    {
        parent::__construct();
        $this->load->helper('date');
        $this->load->model('bm_budget_model','',TRUE);
        $session_data = $this->session->userdata('logged_in');
        $this->npkLogin = $session_data['npk'];
    }

    public function index()
    {
        try
        {
            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }

    function rekapMasterBudget(){
        if($this->session->userdata('logged_in'))
        {
            if(check_authorizedByName("Rekap Master Budget"))
            {
                $data = array(
                    'title' => 'Rekap Master Budget'
                );
                $this->load->helper(array('form','url'));
                $this->template->load('default','BudgetMonitoring/rekapMasterBudget_view',$data);
            }
        }
        else
        {
            redirect("login?u=BudgetMonitoring/budgetController/RekapMasterBudget",'refresh');                
        }
    }

    public function ajax_loadRekapMasterBudget()
    {
        try{
            $dpa = $_POST['dpa'];
            $tanggal = explode('/',$_POST['tanggal']);
            $dtTanggal = $tanggal[2]."-".$tanggal[1]."-".$tanggal[0];
            $jenispengeluaran = $_POST['jenispengeluaran'];

            $result = $this->bm_budget_model->getRekapMasterBudget($dpa,$dtTanggal,$jenispengeluaran);
            if($result){
                $total = 0;

                 $htmlOpex = "
                 <div class='col-sm-6'>
                 <h3>".$jenispengeluaran."</h3>
                 <table class='table table-bordered'>
                 <tbody>
                        <tr>
                        <th style='text-align:center'>Kategori</th>
                        <th style='text-align:center'>Budget</th>
                        </tr>";
                echo $htmlOpex;
                foreach($result as $row)
                {
                    echo "<tr><td>".$row->NamaKategori."</td>";
                    echo "<td class = 'text-right'>".number_format($row->JumlahBudget)."</td>";

                    echo "</tr>";
                    $total+=$row->JumlahBudget;
                }
                echo '<tr style="background-color:lightgray;"><td>Total</td><td class="text-right">'.number_format($total).'</td></tr>';
                echo "</tbody></table>";
            }else{
                echo "Tidak ada data";
            }
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }

    public function getAllActivityDPA2()
    {
        try
        {
            $this->load->model('bm_activity_model','',TRUE);
            $result = $this->bm_activity_model->getDataActivity('',1,2);
            if($result){
                echo json_encode($result);
            }else{
                echo "kosong";
            }
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }
   

    public function getBudgetTotalRealisasiSisa()
    {
        try{
            $kodeActivity = $_GET["kodeActivity"];
            $this->load->model('bm_realisasi_model','',TRUE);
            $resultBudget = $this->bm_budget_model->getBudgetTotal($kodeActivity);
            $resultRealisasi = $this->bm_realisasi_model->getRealisasiTotal($kodeActivity);

            if($resultBudget)
            {
                foreach($resultBudget as $bu)
                {
                    $totalBudget = $bu->TotalBudget;
                    $statusBudget = $bu->Status; //Lock atau Unlock
                }
            }
            else
            {
                $totalBudget = 0;
                $statusBudget = "Unlock";
            }

            if($resultRealisasi)
            {
                foreach($resultRealisasi as $re)
                {
                    $totalRealisasi = $re->TotalRealisasi;
                }
            }
            else
            {
                $totalRealisasi = 0;
            }
            $res[] = $totalBudget == null?"0":$totalBudget;
            $res[] = $totalRealisasi == null?"0":$totalRealisasi;
            $res[] = $statusBudget;
            echo json_encode($res);
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }
}
?>