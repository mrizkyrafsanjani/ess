<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Realisasi extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('bm_activity_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Realisasi"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_realisasi();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }

	public function ViewUser($tahun='',$bulanMulai='',$bulanSelesai='',$status='',$kodeActivity='',$kodeCoa='')
	{
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Realisasi"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_realisasi("ViewUser",$tahun,$bulanMulai,$bulanSelesai,$status,$kodeActivity,$kodeCoa);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}

	public function ViewDetil($tahun='',$bulanMulai='',$bulanSelesai='',$status='',$kodeActivity='',$kodeCoa='')
	{
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Budget Monitor"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_realisasi("ViewDetil",$tahun,$bulanMulai,$bulanSelesai,$status,$kodeActivity,$kodeCoa);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function _realisasi($view = "",$tahun,$bulanMulai,$bulanSelesai,$status,$kodeActivity,$kodeCoa)
    {
		$crud = new grocery_crud();
		$crud->set_subject('Realisasi');
		$crud->set_theme('datatables');
		
        $crud->set_table('bm_realisasi');
		$crud->set_relation('KodeActivity','bm_activity','NamaActivity',array('Deleted'=>'0'));
		$crud->where('bm_realisasi.deleted','0');
		if($tahun != "all")
		{
			$crud->where('YEAR(bm_realisasi.Tanggal)',$tahun);
		}
		if($bulanMulai != "all" && $bulanSelesai != "all")
		{
			$crud->where("MONTH(bm_realisasi.Tanggal) BETWEEN '$bulanMulai' AND '$bulanSelesai'");
		}
		if($bulanMulai != "all" && $bulanSelesai == "all")
		{
			$crud->where("MONTH(bm_realisasi.Tanggal) >=",$bulanMulai);
		}
		if($bulanMulai == "all" && $bulanSelesai != "all")
		{
			$crud->where("MONTH(bm_realisasi.Tanggal) <=",$bulanSelesai);
		}

		if($status != "all")
		{
			if($status == "valid"){
				$crud->where('bm_realisasi.KodeActivity IN (SELECT kodeActivity FROM bm_activity WHERE Deleted = 0)');
			}else{
				$crud->where('bm_realisasi.KodeActivity NOT IN (SELECT kodeActivity FROM bm_activity WHERE Deleted = 0)');
			}
		}
		if($kodeActivity != "all")
		{
			$crud->like('bm_realisasi.KodeActivity',$kodeActivity);
		}
		if($kodeCoa != "all")
		{
			$crud->like('KodeCoa',$kodeCoa);
		}
		$crud->columns(array('Tanggal','Keterangan','NoBatch','Nominal','KodeActivity','TanggalLaporan','StatusActivityValid'));
		$crud->unset_columns('Deleted');
		$crud->unset_fields('Deleted','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy');		
		
		$crud->display_as('KodeActivity','Kode Activity')->display_as('TanggalLaporan','Tanggal Laporan')->display_as('StatusActivityValid','Status Activity Valid');
        //$crud->display_as('MenuOrder','Urutan Menu');
		
		//$crud->required_fields('NamaCoa');
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		$crud->unset_export();
		if($view == "ViewDetil"){
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->where("bm_realisasi.Tanggal <",date("Y-m-d"));
			//$crud->set_theme('flexigrid');
		}

		$crud->field_type('Nominal', 'integer');		
		$crud->field_type('JenisPengeluaran', 'dropdown',array('OPEX'=>'OPEX','CAPEX'=>'CAPEX'));

		$crud->callback_column('StatusActivityValid',array($this,'_callbackStatusActivityValid'));		
		$crud->callback_column('Nominal',array($this,'_callbackNominal'));	

		$crud->callback_insert(array($this,'_insert_realisasi'));
		$crud->callback_update(array($this,'_update_realisasi'));
		$crud->callback_delete(array($this,'_delete_realisasi'));
		//$crud->unset_texteditor('Description','Url');
		
        $output = $crud->render();
   
        $this-> _outputview($output,$view);        
    }
 
    function _outputview($output = null,$view)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Realisasi',
			   'body' => $output,
			   'view' => $view
		  );
		$this->load->helper(array('form','url'));
		if($view == "1"){
			$this->template->load('default','templates/CRUD_view',$data);		
		}
		else{
			$this->load->view('BudgetMonitoring/realisasiSimple_view',$data);
		}
    }
	
	public function _callbackStatusActivityValid($value, $row)
	{
		$valid = "tidak sesuai";
		$activity = $this->bm_activity_model->getActivity($row->KodeActivity);
		if($activity)
			$valid = "valid";
		return $valid;
	}

	public function _callbackNominal($value, $row)
	{
		return number_format($value);
	}

	function _delete_realisasi($primary_key){		
		return $this->db->update('bm_realisasi',array('deleted' => '1','UpdatedBy' => $this->npkLogin,'UpdatedOn'=> date('Y-m-d H:i:s')),array('KodeRealisasi' => $primary_key));
	}
	
	function _insert_realisasi($post_array){
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		
		return $this->db->insert('bm_realisasi',$post_array);
	}
	
	function _update_realisasi($post_array,$primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		return $this->db->update('bm_realisasi',$post_array,array('KodeRealisasi'=>$primary_key));
	}

	function downloadCSV()
    {
        $this->load->dbutil();
        $this->load->helper('download');
        try{
			$tahun = $_GET['tahun'];
			$bulanMulai = $_GET['bulanMulai'];
			$bulanSelesai = $_GET['bulanSelesai'];
			$status = $_GET['status'];
			$kodeActivity = $_GET['kodeActivity'];
			$kodeCoa = $_GET['kodeCoa'];
            

			$this->db->select("Tanggal,Keterangan,NoBatch,Nominal,bm_realisasi.KodeActivity,bm_activity.NamaActivity,bm_activity.KodeCoa,bm_coa.NamaCoa,TanggalLaporan,IF(bm_activity.KodeActivity IS NULL,'Tidak Valid','Valid') as StatusActivityValid",false);
			$this->db->from('bm_realisasi');
			$this->db->join('bm_activity','bm_activity.KodeActivity = bm_realisasi.KodeActivity','left');
			$this->db->join('bm_coa','bm_coa.KodeCoa=bm_activity.KodeCoa');
			$this->db->where('bm_realisasi.deleted','0');
			if($tahun != "all")
			{
				$this->db->where('YEAR(bm_realisasi.Tanggal)',$tahun);
			}
			if($bulanMulai != "all" && $bulanSelesai != "all")
			{
				$this->db->where("MONTH(bm_realisasi.Tanggal) BETWEEN '$bulanMulai' AND '$bulanSelesai'");
			}
			if($bulanMulai != "all" && $bulanSelesai == "all")
			{
				$this->db->where("MONTH(bm_realisasi.Tanggal) >=",$bulanMulai);
			}
			if($bulanMulai == "all" && $bulanSelesai != "all")
			{
				$this->db->where("MONTH(bm_realisasi.Tanggal) <=",$bulanSelesai);
			}
	
			if($status != "all")
			{
				if($status == "valid"){
					$this->db->where('bm_realisasi.KodeActivity IN (SELECT kodeActivity FROM bm_activity WHERE Deleted = 0)');
				}else{
					$this->db->where('bm_realisasi.KodeActivity NOT IN (SELECT kodeActivity FROM bm_activity WHERE Deleted = 0)');
				}
			}
			if($kodeActivity != "all")
			{
				$this->db->like('bm_realisasi.KodeActivity',$kodeActivity);
			}
			if($kodeCoa != "all")
			{
				$this->db->like('bm_activity.KodeCoa',$kodeCoa);
			}
			$this->db->order_by('Tanggal','ASC');
            $result = $this->db->get();
            $new_report = $this->dbutil->csv_from_result($result);
            force_download("Realisasi_". $tahun ."_".$bulanMulai."_".$bulanSelesai."_".$status."_".$kodeActivity."_".$kodeCoa.".csv",$new_report);            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */