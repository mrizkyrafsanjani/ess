<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanActivity extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		//$this->load->model('bm_activity_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Pengaturan Activity"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_activity();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _activity()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Activity');
		//$crud->set_theme('datatables');
		
        $crud->set_table('bm_activity');
		$crud->where('bm_activity.deleted','0');
		$crud->set_relation('KodeCoa','bm_coa','KodeCoa');
		$crud->set_relation('KodeDepartemen','departemen','NamaDepartemen');
		$crud->set_relation('KodeKategori','bm_kategori','NamaKategori');
		
		$crud->unset_columns('Deleted');
		$crud->unset_fields('Deleted','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy');
		
		//$crud->display_as('MenuName','Nama Menu');
        //$crud->display_as('MenuOrder','Urutan Menu');
		
		$crud->required_fields('KodeActivity','NamaActivity','DPA','KodeCoa','KodeDepartemen');
		$crud->field_type('DPA', 'dropdown',array('1'=>'1','2'=>'2'));
		$crud->field_type('JenisPengeluaran','dropdown',array('Opex'=>'Opex','Capex'=>'Capex'));
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		$crud->callback_insert(array($this,'_insert_activity'));
		$crud->callback_update(array($this,'_update_activity'));
		$crud->callback_delete(array($this,'_delete_activity'));
		//$crud->unset_texteditor('Description','Url');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Activity',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);		
    }
	
	function _delete_activity($primary_key){		
		return $this->db->update('bm_activity',array('deleted' => '1','UpdatedBy' => $this->npkLogin,'UpdatedOn'=> date('Y-m-d H:i:s')),array('KodeActivity' => $primary_key));
	}
	
	function _insert_activity($post_array){
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		
		return $this->db->insert('bm_activity',$post_array);
	}
	
	function _update_activity($post_array,$primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		return $this->db->update('bm_activity',$post_array,array('KodeActivity'=>$primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */