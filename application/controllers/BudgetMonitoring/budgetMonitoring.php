<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class BudgetMonitoring extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('bm_summarybudget_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Budget Monitor"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_budgetMonitoring();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function search($Admin='',$DPA='', $KodeActivity='')
	{
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Budget Monitor"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_budgetMonitoring($DPA, $KodeActivity, $Admin);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function _budgetMonitoring($DPA='', $KodeActivity='',$Admin='0')
    {
		$crud = new grocery_crud();
		$crud->set_subject('Budget Monitor');
		$crud->set_theme('datatables');
		
		$crud->set_model('custom_query_model');
		$crud->set_table('bm_summarybudget'); //Change to your table name
		
		if($DPA == 'all')
		{
			$sqlquery = "SELECT '' as KodeSummaryBudget,LEFT(ba.KodeActivity,5) as KodeActivity, SUBSTRING(ba.NamaActivity,1,LOCATE('DPA',ba.NamaActivity)-2) as NamaActivity,ba.JenisPengeluaran,format(SUM(BudgetYTD),0) as BudgetYTD,format(SUM(RealisasiYTD),0) as RealisasiYTD,format((SUM(BudgetYTD) - SUM(RealisasiYTD)),0) as SisaBudgetYTD,format(SUM(BudgetFullYear),0) as BudgetFullYear, format((SUM(BudgetFullYear)-SUM(RealisasiYTD)),0) as SisaFullYear, MAX(TanggalLaporan) as TanggalLaporan
				FROM bm_summarybudget sb 
					left join bm_coa c on sb.KodeCoa = c.KodeCoa
					left join bm_activity ba on sb.KodeActivity = ba.KodeActivity
				WHERE (BudgetFullYear != 0 OR RealisasiYTD != 0) and sb.CreatedBy = '". $this->npkLogin ."' ";
		}
		else
		{
			$sqlquery = "SELECT KodeSummaryBudget, sb.KodeCoa,c.NamaCoa,ba.KodeActivity,ba.NamaActivity,ba.JenisPengeluaran,
				format(BudgetYTD,0) as BudgetYTD,format(RealisasiYTD,0) as RealisasiYTD,
				format((BudgetYTD - RealisasiYTD),0) as SisaBudgetYTD,format(BudgetFullYear,0) as BudgetFullYear, 
				format((BudgetFullYear-RealisasiYTD),0) as SisaFullYear, TanggalLaporan
			FROM bm_summarybudget sb 
				left join bm_coa c on sb.KodeCoa = c.KodeCoa
				left join bm_activity ba on sb.KodeActivity = ba.KodeActivity
			WHERE (BudgetFullYear != 0 OR RealisasiYTD != 0) and sb.CreatedBy = '". $this->npkLogin ."' ";
		}
		if($DPA == '' && $KodeActivity == '')
		{
			$sqlquery = $sqlquery . " AND sb.KodeSummaryBudget = 0";
		}
		
		if($DPA == 'all')
		{
			$sqlquery .= " AND sb.DPA IN (1,2) ";
		}
		else if($DPA != '')
		{
			$sqlquery .= " AND sb.DPA = '$DPA'";
		}

		if($KodeActivity != '')
		{
			if($DPA == 'all')
			{
				$sqlquery .= " AND LEFT(sb.KodeActivity,5) = '$KodeActivity'";
			}else{
				$sqlquery .= " AND sb.KodeActivity = '$KodeActivity'";
			}
		}

		if($Admin == "1")
		{
			check_authorizedByName("Budget Monitor Admin");			
		}
		else
		{			
			$sqlquery = $sqlquery . " AND ba.KodeDepartemen = (select Departemen from mstruser where NPK = '". $this->npkLogin ."')";
		}
		//fire_print('log',$sqlquery);		
		
		if($DPA == 'all')
		{
			$sqlquery .= "GROUP BY LEFT(ba.KodeActivity,5),SUBSTRING(ba.NamaActivity,1,LOCATE('DPA',ba.NamaActivity)-2),ba.JenisPengeluaran";
			$crud->columns('KodeActivity','NamaActivity','JenisPengeluaran','BudgetYTD','RealisasiYTD','SisaBudgetYTD','BudgetFullYear','SisaFullYear','TanggalLaporan');			
		}
		else
		{
			if($KodeActivity == '')
			{			
				$crud->columns('KodeCoa','NamaCoa','NamaActivity','JenisPengeluaran','BudgetYTD','RealisasiYTD','SisaBudgetYTD','BudgetFullYear','SisaFullYear','TanggalLaporan');			
			}
			else
			{
				$crud->columns('KodeCoa','NamaCoa','JenisPengeluaran','BudgetYTD','RealisasiYTD','SisaBudgetYTD','BudgetFullYear','SisaFullYear','TanggalLaporan');
				//$crud->fields('KodeMenu','MenuName','Description','Url','Parent','MenuOrder');
			}
		}
		$crud->display_as('KodeCoa','Kode COA')->display_as('NamaCoa','Nama Coa')->display_as('JenisPengeluaran','Jenis Pengeluaran');
        $crud->display_as('KodeActivity','Kode Activity')->display_as('NamaActivity','Nama Activity')->display_as('BudgetYTD','Budget YTD');
		$crud->display_as('RealisasiYTD','Realisasi YTD')->display_as('SisaBudgetYTD','Sisa Budget YTD')->display_as('BudgetFullYear','Budget Full Year');
		$crud->display_as('SisaFullYear','Sisa Full Year')->display_as('TanggalLaporan','Tanggal Laporan');
		

		$crud->basic_model->set_query_str($sqlquery); //Query text here

		$crud->callback_column('SisaBudgetYTD',array($this,'_callback_SisaYTD'));
		$crud->callback_column('SisaFullYear',array($this,'_callback_SisaFullYear'));
		$crud->callback_column('NamaActivity',array($this,'_callback_NamaActivity'));
		$crud->callback_column('RealisasiYTD',array($this,'_callback_RealisasiYTD'));
		//$crud->display_as('MenuOrder','Urutan Menu');
		
		//$crud->required_fields('MenuName','Url','MenuOrder');
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		$crud->unset_delete();
		$crud->unset_edit();
		$crud->unset_add();
		$crud->unset_read();
		$output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('BudgetMonitoring/budgetMonitoringSimple_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _callback_SisaYTD($value,$row)
	{
		if($value < 0){
			return "<font color='red'>$value</font>";
		}else{
			return $value;
		}
	}

	function _callback_SisaFullYear($value,$row)
	{
		if($value < 0){
			return "<font color='red'>$value</font>";
		}else{
			return $value;
		}
	}
	
	function _callback_NamaActivity($value,$row)
	{
		if($row->SisaFullYear < 0){
			return "<font color='red'>$value</font>";
		}else{
			return $value;
		}
	}

	function _callback_RealisasiYTD($value,$row)
	{
		if($value != 0)
		{
			$url = $this->config->base_url()."index.php/budgetmonitoring/realisasi/viewdetil/".date("Y")."/all/".date("m")."/valid/".$row->KodeActivity."/all";
			return "<a href='' onclick=\"window.open('$url','_blank','location=yes,height=600,width=1200,scrollbars=yes,status=yes');return false;\">$value</a>";
		}
		else{
			return $value;
		}
	}
		
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */