<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
session_start();
class RealisasiController extends CI_Controller{
    var $npkLogin;
    function __construct()
    {
        parent::__construct();
        $this->load->model('bm_realisasi_model','',TRUE);
        $this->load->model('bm_activity_model','',TRUE);
        $this->load->model('bm_coa_model','',TRUE);
        $this->load->helper('date');
        $session_data = $this->session->userdata('logged_in');
        $this->npkLogin = $session_data['npk'];
    }

    public function index()
    {
        try
        {
            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }

    function ViewRealisasi()
    {
        try
        {
            if($this->session->userdata('logged_in'))
            {
                if(check_authorizedByName("Realisasi"))
                {
                    $data = array(
                        'title' => 'Realisasi',
                        'dataActivityDPA1' => $this->bm_activity_model->getDataActivity($this->npkLogin,'1','1'),
                        'dataActivityDPA2' => $this->bm_activity_model->getDataActivity($this->npkLogin,'1','2'),
                        'dataCoa' => $this->bm_coa_model->getDataCoa()
                    );
                    $this->load->helper(array('form','url'));
                    $this->template->load('default','BudgetMonitoring/realisasi_view',$data);
                }
            }
            else
            {
                redirect('login?u=BudgetMonitoring/realisasiController/ViewRealisasi','refresh');
            }
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );            
        }
    }

    function uploadRealisasi()
    {
        if($this->session->userdata('logged_in'))
        {
            if(check_authorizedByName("Upload Realisasi"))
            {
                $data = array(
                    'title' => 'Upload Realisasi'
                );
                $this->load->helper(array('form','url'));
                $this->template->load('default','BudgetMonitoring/uploadRealisasi_view',$data);
            }
        }
        else
        {
            redirect("login?u=BudgetMonitoring/realisasiController/uploadRealisasi",'refresh');                
        }
    }

    private function _validateRow($rowdata)
    {
        $error = "";
        try{
            //0:tanggal, 1:keterangan, 2:no batch, 3:nominal, 4:kodeactivity, 5:tanggallaporan
            if(count($rowdata) != 6)
            {
                $error .= "Jumlah kolom file upload harus berjumlah 6 \n";
            }
            else
            {
                //harus berformat m/d/y
                if(!validatedate($rowdata[0]) )
                {
                    $error .= "Kolom tanggal harus berformat mm/dd/yyyy \n";
                }
                
                if(trim($rowdata[1]) == '')
                {
                    $error .= "Kolom No Batch harus diisi \n";
                }

                if(trim($rowdata[2]) == '')
                {
                    $error .= "Kolom Keterangan harus diisi \n";
                }

                if(!is_numeric($rowdata[3]))
                {
                    $error .= "Kolom nominal harus berisi angka \n";
                }

                if(!$this->bm_activity_model->getActivity($rowdata[4]))
                {
                    $error .= "Tidak ada kode activity ".$rowdata[4] ."\n";
                }

                if(!validatedate($rowdata[5]))
                {
                    $error .= "Kolom tanggal laporan harus berformat mm/dd/yyyy \n";
                }
            }
            return $error;
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            throw new Exception( 'Something really gone wrong', 0, $e);
        }
    }

    public function ajax_uploadRealisasi()
    {
        require 'application/third_party/PHPExcel/PHPExcel/IOFactory.php';
        try
        {
            $data = $_POST['data'];
            $filename = $_POST['name'];
            
            
            $uploadpath = "./assets/uploads/temp/";
            $inputfilename = $uploadpath.$filename;
            $fp = fopen($inputfilename,'w');
            fwrite($fp, $data);
            fclose($fp);
                            
            $inputfiletype = PHPExcel_IOFactory::identify($inputfilename);
            $objReader = PHPExcel_IOFactory::createReader($inputfiletype);
            $objPHPExcel = $objReader->load($inputfilename);
            
            //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            $htmlHasil = "<div class='col-sm-12'><h3>Realisasi</h3><table class='table table-bordered'><tbody>
                    <tr><th style='text-align:center'>Tanggal</th><th style='text-align:center'>Keterangan</th><th style='text-align:center'>No Batch</th><th style='text-align:center'>Nominal</th><th style='text-align:center'>Kode Activity</th><th style='text-align:center'>Tanggal Laporan</th></tr>";
            $totalRealisasi = 0;
            //  Loop through each row of the worksheet in turn
            $totalRecord = 0;
            for ($row = 2; $row <= $highestRow; $row++)
            { 
                //  Read a row of data into an array
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                $exceldata[] = $rowData[0];
                $error = $this->_validateRow($rowData[0]);
                if($error != "")
                {
                    echo "Error pada file upload di baris " . $row . ": \n\n".$error;
                    throw new Exception("Error pada file upload : ".$error);
                }
                $htmlHasil .= "<tr><td>" . $rowData[0][0] . "</td><td>". $rowData[0][1] . "</td><td>". $rowData[0][2] . "</td><td style='text-align:right'>". number_format($rowData[0][3],2) . "</td><td>". $rowData[0][4] . "</td><td>". $rowData[0][5] . "</td></tr>";
                $totalRealisasi += $rowData[0][3];
                $totalRecord++;                    
            }

            $htmlHasil .= "<tr style='font-weight:bold'><td style='text-align:center' colspan='3'>Total $totalRecord record</td><td style='text-align:right'>" . number_format($totalRealisasi,2) . "</td><td colspan='2'></td></tr>";
            $htmlHasil .= "</tbody></table></div>";
            
            $result[] = $htmlHasil;
            $result[] = $exceldata;                
            echo json_encode($result);
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            echo "Error!!";
        }			
    }

    public function ajax_submitUploadData()
    {
        try{
            $success = true;
            $data = $_POST['data'];
            $this->db->trans_begin();
            $recordProcessed = 0;
            foreach ($data as $row)
            {
                if(!$this->bm_realisasi_model->insertoverwrite($row,$this->npkLogin)){
                    echo "Error!";
                    $success = false;                    
                }
                $recordProcessed++;
            }
            if($success)
            {
                echo "Sebanyak $recordProcessed record Realisasi telah dimasukkan.";
                $this->db->trans_commit();
            }
            else
            {
                echo "Terdapat kesalahan, mohon lakukan kembali!";
                $this->db->trans_rollback();
            }                
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }

    public function laporanRealisasi()
    {
        $session_data = $this->session->userdata('logged_in');
        if($this->session->userdata('logged_in')){
            if(check_authorizedByName("Laporan Realisasi"))
            {
                $this->npkLogin = $session_data['npk'];
                
                $realisasiNonValid = $this->bm_realisasi_model->getRealisasiNonValid();
                $boolRealisasiNonValid = 'false';
                if($realisasiNonValid){
                    $boolRealisasiNonValid = 'true';
                }

                $data = array(
                    'title' => 'Laporan Realisasi Budget',
                    'boolRealisasiNonValid' => $boolRealisasiNonValid
                );
                $this->load->helper(array('form','url'));
                $this->template->load('default','BudgetMonitoring/laporanRealisasi_view',$data);            
            }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }

    public function ajax_loadLaporanRealisasi()
    {
        try{
            $dpa = $_POST['dpa'];
            $tanggal = explode('/',$_POST['tanggal']);
            $dtTanggal = $tanggal[2]."-".$tanggal[1]."-".$tanggal[0];
            $jenispengeluaran = $_POST['jenispengeluaran'];
           
            $result = $this->bm_realisasi_model->getLaporanRealisasi($dpa,$dtTanggal,$jenispengeluaran);
            if(!$result){
                $res[] = "Tidak ada data";
                echo json_encode($res);
            }else{
                $html = "<div class='col-sm-12'>
                    <div class='text-right'><button id='btnDownload' class='btn btn-default'><i class='fa fa-download'></i>Download</button></div>                    
                    <div id='divTglUpdate' class='text-right'></div>
                    <table class='table table-bordered'><tbody>
						<tr><th>No</th><th>Kode Activity</th><th>Nama Activity</th>".($dpa != 0 ? "<th>COA</th>": "")."<th>Kategori</th><th>Budget ". $tanggal[2] ."</th><th>Realisasi ". $tanggal[2] ."</th><th>Sisa Budget</th><th>Realisasi ". ($tanggal[2]-1) ."</th></tr>";
                $i = 0;
                foreach($result->result() as $row)
                {
                    $i++;
                    $html .= "<tr><td>". $i ."</td><td>". $row->KodeActivity ."</td><td>" . $row->NamaActivity . "</td>
                        ".($dpa != 0 ? "<td>" . $row->KodeCoa . "</td>":"")."<td>" . $row->NamaKategori . "</td><td class='text-right'>" . number_format($row->BudgetYTD,2) . "</td>
                        <td class='text-right'>" . number_format($row->RealisasiYTD,2) . "</td><td class='text-right'>" . number_format($row->SisaBudget,2) . "</td><td class='text-right'>" . number_format($row->RealisasiYTDLalu,2) . "</td></tr>";
                    $tglLaporan = $row->TanggalLaporan;
                }
                $html .= "</tbody></table></div>";
                
                $res[] = $html;
                $res[] = "Tanggal Update : " . $tglLaporan;
                
                //$res[] = $result;
                echo json_encode($res);
            }
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }

    function downloadCSV()
    {
        $this->load->dbutil();
        $this->load->helper('download');
        try{
            $dpa = $_GET['dpa'];
            $tanggal = explode('/',$_GET['tanggal']);
            $dtTanggal = $tanggal[2]."-".$tanggal[1]."-".$tanggal[0];
            $jenispengeluaran = $_GET['jenispengeluaran'];

            $result = $this->bm_realisasi_model->getLaporanRealisasi($dpa,$dtTanggal,$jenispengeluaran);
            $new_report = $this->dbutil->csv_from_result($result);
            force_download("laporan_realisasi_". $dpa ."_".$dtTanggal."_".$jenispengeluaran.".csv",$new_report);            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }
}
?>