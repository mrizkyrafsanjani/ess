<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class AnalisaBudget extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('bm_analisa_model','',TRUE);
		// $this->load->model('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Analisa Budget"))
			{
				$this->npkLogin = $session_data['npk'];
				// $this->_analisa();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }

	public function ViewUser($DPA='',$tahun='',$bulan='',$Departemen='',$Jenis='')
	{
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){

			if(check_authorizedByName("Analisa Budget"))
			{
				$this->npkLogin = $session_data['npk'];
				if($DPA =="all"){
					$DPA = "(1,2) ";
				}else{
					$DPA = "(".$DPA.")";
				}
				if($Departemen =="all"){
					$Departemen = "(1,2,3,4,5,6,7,8,9,10,11)";
				}else{
					$Departemen = "(".$Departemen.")";
				}
				if($Jenis =="all"){
					
					$Jenis = "('OPEX','CAPEX')";
				}else{
					$Jenis = "('".$Jenis."')";
				}
					

				$this->_analisa2($DPA,$Departemen,$Jenis,$tahun,$bulan,$Jenis);
				// $mydata = $this->bm_analisa_model->getListAnalisa($where);
				// $this->template->load('BudgetMonitoring/analisa_view');				
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}

	public function ViewList($where = ''){
		$session_data = $this->session->userdata['logged_in'];
		if($this->session->userdata['logged_in']){
			if(check_authorizedByName("Analisa Budget"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_analisa($where);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function _analisa($where)
    {
		$data['results'] = $this->bm_analisa_model->getListAnalisa($where);
		$this->load->view("BudgetMonitoring/Analisa_List",$data);

	}
	
	public function _analisa2($DPA,$Departemen,$Jenis,$tahun,$bulan){
		$data['results'] =$this->bm_analisa_model->getListAnalisa_2($DPA,$Departemen,$Jenis,$tahun,$bulan);
		$this->load->view("BudgetMonitoring/Analisa_List",$data);
	}
	

	function downloadCSV()
    {
        $this->load->dbutil();
        $this->load->helper('download');
        try{
			$tahun = $_GET['tahun'];
			$bulanMulai = $_GET['bulanMulai'];
			$bulanSelesai = $_GET['bulanSelesai'];
			$status = $_GET['status'];
			$kodeActivity = $_GET['kodeActivity'];
			$kodeCoa = $_GET['kodeCoa'];
            

			$this->db->select("Tanggal,Keterangan,NoBatch,Nominal,bm_realisasi.KodeActivity,bm_activity.NamaActivity,bm_activity.KodeCoa,bm_coa.NamaCoa,TanggalLaporan,IF(bm_activity.KodeActivity IS NULL,'Tidak Valid','Valid') as StatusActivityValid",false);
			$this->db->from('bm_realisasi');
			$this->db->join('bm_activity','bm_activity.KodeActivity = bm_realisasi.KodeActivity','left');
			$this->db->join('bm_coa','bm_coa.KodeCoa=bm_activity.KodeCoa');
			$this->db->where('bm_realisasi.deleted','0');
			if($tahun != "all")
			{
				$this->db->where('YEAR(bm_realisasi.Tanggal)',$tahun);
			}
			if($bulanMulai != "all" && $bulanSelesai != "all")
			{
				$this->db->where("MONTH(bm_realisasi.Tanggal) BETWEEN '$bulanMulai' AND '$bulanSelesai'");
			}
			if($bulanMulai != "all" && $bulanSelesai == "all")
			{
				$this->db->where("MONTH(bm_realisasi.Tanggal) >=",$bulanMulai);
			}
			if($bulanMulai == "all" && $bulanSelesai != "all")
			{
				$this->db->where("MONTH(bm_realisasi.Tanggal) <=",$bulanSelesai);
			}
	
			if($status != "all")
			{
				if($status == "valid"){
					$this->db->where('bm_realisasi.KodeActivity IN (SELECT kodeActivity FROM bm_activity WHERE Deleted = 0)');
				}else{
					$this->db->where('bm_realisasi.KodeActivity NOT IN (SELECT kodeActivity FROM bm_activity WHERE Deleted = 0)');
				}
			}
			if($kodeActivity != "all")
			{
				$this->db->like('bm_realisasi.KodeActivity',$kodeActivity);
			}
			if($kodeCoa != "all")
			{
				$this->db->like('bm_activity.KodeCoa',$kodeCoa);
			}
			$this->db->order_by('Tanggal','ASC');
            $result = $this->db->get();
            $new_report = $this->dbutil->csv_from_result($result);
            force_download("Realisasi_". $tahun ."_".$bulanMulai."_".$bulanSelesai."_".$status."_".$kodeActivity."_".$kodeCoa.".csv",$new_report);            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */