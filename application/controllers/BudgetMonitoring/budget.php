<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Budget extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		//$this->load->model('bm_budget_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Master Budget"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_budget();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _budget()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Budget');
		//$crud->set_theme('datatables');
		
        $crud->set_table('bm_budget');
		$crud->where('bm_budget.deleted','0');
		//$crud->set_relation('KodeCoa','bm_coa','NamaCoa');
		//$crud->set_relation('KodeDepartemen','departemen','NamaDepartemen');
		
		$crud->unset_columns('Deleted');
		$crud->unset_fields('Deleted','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy');
		
		//$crud->display_as('MenuName','Nama Menu');
        //$crud->display_as('MenuOrder','Urutan Menu');
		
		$crud->required_fields('KodeActivity','Periode','JumlahBudget','JenisPengeluaran');
		$crud->field_type('JenisPengeluaran', 'dropdown',array('Opex'=>'Opex','Capex'=>'Capex'));
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		$crud->callback_insert(array($this,'_insert_budget'));
		$crud->callback_update(array($this,'_update_budget'));
		$crud->callback_delete(array($this,'_delete_budget'));
		//$crud->unset_texteditor('Description','Url');
		$crud->callback_column('JumlahBudget',array($this,'_callback_JumlahBudget'));
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
	
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Budget',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);		
    }
	
	function _callback_JumlahBudget($value, $row)
	{
		return number_format($value, 2, '.', ',');;
	}
	
	function _delete_budget($primary_key){		
		return $this->db->update('bm_budget',array('deleted' => '1','UpdatedBy' => $this->npkLogin,'UpdatedOn'=> date('Y-m-d H:i:s')),array('KodeBudget' => $primary_key));
	}
	
	function _insert_budget($post_array){
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		
		return $this->db->insert('bm_budget',$post_array);
	}
	
	function _update_budget($post_array,$primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		return $this->db->update('bm_budget',$post_array,array('KodeBudget'=>$primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */