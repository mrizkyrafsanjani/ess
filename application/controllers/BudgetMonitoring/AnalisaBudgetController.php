<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
session_start();
class AnalisaBudgetController extends CI_Controller{
    var $npkLogin;
    function __construct()
    {
        parent::__construct();
        $this->load->model('user','',TRUE);
        $this->load->model('bm_activity_model','',TRUE);
        $this->load->model('bm_coa_model','',TRUE);
        $this->load->model('bm_analisa_model','',TRUE);
        $this->load->helper('date');
        $session_data = $this->session->userdata('logged_in');
        $this->npkLogin = $session_data['npk'];
    }

    public function index()
    {
        try
        {
            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }

    function ViewAll()
    {
        try
        {
            if($this->session->userdata('logged_in'))
            {
                if(check_authorizedByName("Analisa Budget"))
                {
                    // $DPA = $this->input('DPA');
                    // $Tanggal = $this->input('Tanggal');
                    // $Departemen = $this->input('Departemen');
                    // $JenisPengeluaran = $this->input('JenisPengeluaran');


                    $data = array(
                        'title' => 'Analisa Budget',
                        'Departemen' => $this->bm_analisa_model->getDepartemen()
                        // 'dataAnalisa' => $this->bm_analisa_model->getListAnalisa($this->$DPA,$Tanggal,$Departemen,$JenisPengeluaran)
                        // 'dataCoa' => $this->bm_coa_model->getDataCoa(),
                        // 'dataBudget' => $this->bm_budget_model->getDataCoa()
                    );
                    $this->load->helper(array('form','url'));
                    $this->template->load('default','BudgetMonitoring/analisa_view',$data);
                }
            }
            else
            {
                redirect('login?u=BudgetMonitoring/AnalisaBudgetController/ViewAll','refresh');
            }
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );            
        }
    }

    function LihatDetil($KodeActivity= '',$periode = ''){
        try{
            if($this->session->userdata('logged_in'))
            {
                if(check_authorizedByName("Analisa Budget"))
                {
                    $session_data = $this->session->userdata('logged_in');
                    $this->npkLogin = $session_data['npk'];
                    $myData = array(
                        'title'=>'AnalisaDetil',
                        'KodeActivity'=>$KodeActivity,
                        'periode'=>$periode
                    );
                    if($periode ==''){
                        $periode = "curdate()";
                    }
                    // echo($periode);
                    $datas['detil'] = $this->bm_analisa_model->getDetil($myData);
                    $datas['myHead'] = $this->bm_analisa_model->getHeaderInfo($KodeActivity,$periode);
                    $datas['history'] = $this->bm_analisa_model->getHistory($KodeActivity,$periode);
                    $datas['periode'] = "$periode";
                    // $datas = 'Data nya';
                    // var_dump($datas['periode']);
                    // die();
                    $this->load->view('BudgetMonitoring/AnalisaDetil',$datas);

                }
            }else{
                redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
            }
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
        }
    }

    function TambahAnalisa(){
        if($this->session->userdata('logged_in')){
            if(check_authorizedByName('Analisa Budget')){
                $session_data = $this->session->userdata('logged_in');
                $this->npkLogin = $session_data['npk'];
                $KodeActivity = $this->input->post('kodeActivity');
                $analisa = $this->input->post('analisa');
                $periode = $this->input->post('periode');
                $session_data = $this->session->userdata('logged_in');
                if($session_data){
                    $npk = $session_data['npk'];
                }
                    
                $this->db->trans_start();
                $this->bm_analisa_model->UpdateAnalisa($KodeActivity,$periode,$npk);
                $inserted =$this->bm_analisa_model->TambahAnalisa($KodeActivity,$periode,$analisa,$npk);
                if(!$inserted){
                    $this->db->trans_rollback();
                    $this->LihatDetil($KodeActivity,$periode);
                    echo "gagal";
                }else{
                    $this->db->trans_complete();
                    $this->LihatDetil($KodeActivity,$periode);
                    echo "sukses";
                }
            }
        }
    }

    
    function downloadCSV()
    {
        $this->load->dbutil();
        $this->load->helper('download');
        try{
            $dpa = $_GET['dpa'];
            $tanggal = explode('/',$_GET['tanggal']);
            $dtTanggal = $tanggal[2]."-".$tanggal[1]."-".$tanggal[0];
            $jenispengeluaran = $_GET['jenispengeluaran'];

            $result = $this->bm_realisasi_model->getLaporanRealisasi($dpa,$dtTanggal,$jenispengeluaran);
            $new_report = $this->dbutil->csv_from_result($result);
            force_download("laporan_realisasi_". $dpa ."_".$dtTanggal."_".$jenispengeluaran.".csv",$new_report);            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }
}
?>