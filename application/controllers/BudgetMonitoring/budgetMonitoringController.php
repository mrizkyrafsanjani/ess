<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class BudgetMonitoringController extends CI_Controller{
		var $npkLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('bm_activity_model','',TRUE);
			$this->load->model('bm_summarybudget_model','',TRUE);			
			$this->load->model('usertask','',TRUE);
			$this->load->model('lembur_model','',TRUE);
			$this->load->model('dtltrkrwy_model','',TRUE);
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
		}
		
		function index() //untuk input
		{
			try
			{
				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function viewBMAdmin() //admin
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Budget Monitor Admin"))
					{
						$data = array(
							'title' => 'Budget Monitor',
							'admin' => '1',
							'dataActivity' => $this->bm_activity_model->getDataActivity($this->npkLogin,'1'),
							'npk' => ''
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','BudgetMonitoring/budgetMonitoring_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function viewBMUser($KodeUserTask='') //user
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Budget Monitor"))
					{
						$data = array(
							'title' => 'Budget Monitor',
							'admin' => '0',
							'dataActivity' => $this->bm_activity_model->getDataActivity($this->npkLogin,'0'),
							'npk' => ''
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','BudgetMonitoring/budgetMonitoring_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_KalkulasiBudgetRealisasi()
		{			
			$kodeActivity = $_POST['kodeActivity'];
			$DPA = $_POST['DPA'];
			$return = $this->bm_summarybudget_model->kalkulasiBudgetSummary($kodeActivity, $DPA, $this->npkLogin);
			if($return)
			{
				echo "success";
			}else{
				echo "fail";
			}
		}
		
		function ajax_GetNamaActivity()
		{
			$namaActivity = '';
			$kodeActivity = $_POST['kodeActivity'];
			$activityList = $this->bm_activity_model->getActivity($kodeActivity);
			if($activityList)
			{
				foreach($activityList as $row)
				{
					$namaActivity = $row->NamaActivity;
				}
			}
			echo $namaActivity;
		}
		
		function ajax_loadActivityBasedDPA()
		{
			try
			{
				$DPA = $_POST['DPA'];
				$admin = $_POST['admin'];
				$dataActivity = $this->bm_activity_model->getDataActivity($this->npkLogin,$admin,$DPA);
				
				$dirArray = array();
				if($dataActivity)
				{
					foreach($dataActivity as $row)
					{
						$arr = array('KodeActivity' => $row->KodeActivity, 'NamaActivity' => $row->NamaActivity);						
						array_push($dirArray,$arr);
					}
				}
				
				//fire_print('log',print_r($dirArray,true));
				echo json_encode($dirArray);
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
	}
?>