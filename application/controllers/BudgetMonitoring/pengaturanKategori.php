<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanKategori extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		//$this->load->model('bm_kategori_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Pengaturan Kategori"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_activity();
			}
		}else{
			redirect('login?u=BudgetMonitoring/PengaturanKategori','refresh');
		}
    }
	
	public function _activity()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Kategori Budget');
		//$crud->set_theme('datatables');
		
        $crud->set_table('bm_kategori');
		$crud->where('bm_kategori.deleted','0');
		
		$crud->unset_columns('Deleted');
		$crud->unset_fields('Deleted','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy');
		
		$crud->callback_insert(array($this,'_insert_kategori'));
		$crud->callback_update(array($this,'_update_kategori'));
		$crud->callback_delete(array($this,'_delete_kategori'));
		//$crud->unset_texteditor('Description','Url');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Kategori',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);		
    }
	
	function _delete_kategori($primary_key){		
		return $this->db->update('bm_kategori',array('deleted' => '1','UpdatedBy' => $this->npkLogin,'UpdatedOn'=> date('Y-m-d H:i:s')),array('KodeKategori' => $primary_key));
	}
	
	function _insert_kategori($post_array){
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		
		return $this->db->insert('bm_kategori',$post_array);
	}
	
	function _update_kategori($post_array,$primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		return $this->db->update('bm_kategori',$post_array,array('KodeKategori'=>$primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */