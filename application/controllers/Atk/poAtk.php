<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PoAtk extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('atk_model','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$this->load->library('grocery_crud');
		$this->load->helper('date');
		
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("53"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_PoATK();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	public function viewUser($tahun='',$bulan='',$nama='',$NPKuser='',$KodeUserTask='')
	{
		if($KodeUserTask == 'non')
			$KodeUserTask = '';
		if($nama == 'non')
			$nama = '';
		if($tahun == 'non')
			$tahun = date('Y');
		if($bulan == 'non')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("53"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_PoATK($nama,$tahun,$bulan,$KodeUserTask);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function viewAdmin($tahun='',$bulan='',$nama='non')
	{
		if($nama == 'non')
			$nama = 'xxx';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("52"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_PoATK('viewAdmin',$nama,$tahun,$bulan);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	
	public function _PoATK($nama='',$tahun='',$bulan='',$KodeUserTask='')
    {
		
		try{
			
			
			$crud = new grocery_crud();
			//$crud->set_theme('datatables');
			
			$state = $crud->getState();
			
			$crud->set_subject('Po Atk');
			$crud->set_table('poatk');
			$crud->columns('KodeBarang','Stock','Permintaan','Selisih');
			$crud->fields('KodeBarang','Stock');
			
			$crud->edit_fields('KodeBarang','Stock');
			$crud->required_fields('KodeBarang','Stock');
			$crud->where('poatk.deleted','0');
			
			$crud->set_relation('KodeBarang','mstratk','NamaBarang',array('deleted' => '0'));
			$crud->display_as('KodeBarang','Nama Barang');
			
			
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->unset_add();
			$crud->unset_read();
			$crud->callback_insert(array($this,'_insert_poatk'));
			$crud->callback_update(array($this,'_update'));
			$crud->callback_delete(array($this,'_delete'));	
			
			//validasi field unik
			//$crud->set_rules('KodeBarang', 'Kode Barang', 'trim|required|xss_clean|is_unique[mstratk.KodeBarang]');
			
			if($nama != '')
			{
				$crud->like('poatk.CreatedBy',$nama);
			}
			if($tahun != '' && $bulan != '')
			{
				$periode = $tahun. substr("0".$bulan,-2);
				$crud->where('poatk.Periode',$periode);
			}
			
			fire_print('log',"nama: $nama, tahun: $tahun, bulan: $bulan");
			$output = $crud->render();
			$this-> _outputview($output); 
		}
		
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
	
	function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Laporan ATK GA Head',
			   'body' => $output
		  );
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		
       $this->load->view('Atk/atkSimple_view',$data);
    }
	
	function _insert_poatk($post_array){
	
		try{
		$post_array['Deleted'] = '0';
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		
		$this->db->trans_begin();
		$this->db->insert('poatk',$post_array);
		
		$this->db->trans_commit();
	
		//fire_print('log','No Transaksi '.$KodeArtikel);
		
		/* $KodeUserTask = generateNo('UT');
		$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeArtikel,$KodeUserTask,"KM"); */
			
		return true;
			
	}
	
		catch(Exception $e)
		{
			
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	
	}
	
	function _update($post_array,$primary_key){
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			
			$this->db->trans_commit();
			
			$this->db->update('poatk',$post_array,array('KodePO' => $primary_key));
			$KodePO = $primary_key;
			
			/* //usertask lama di nyatakan statusnya AP
			$usertasklama = $this->usertask->getLastUserTask($KodeArtikel);
			$KodeUserTaskLama = '';
			if($usertasklama){
				foreach($usertasklama as $row){
					$KodeUserTaskLama = $row->KodeUserTask;
					
				}
				$this->usertask->updateStatusUserTask($KodeUserTaskLama,'AP',$this->npkLogin);
			}
			fire_print('log',$KodeUserTaskLama);
			//buat user task baru untuk proses approval artikel
			$KodeUserTask = generateNo('UT');
			
			
			if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeArtikel,$KodeUserTask,"KM"))
			{
				echo('gagal simpan usertask artikel');
				return false;
			}
			$this->db->trans_complete(); 
			return $this->db->trans_status(); */
			
		
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('poatk',$post_array,array('KodePO' => $primary_key));
	}
	

	

	
	

	
	
}
?>