<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class MasterAtk extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('atk_model','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$this->load->library('grocery_crud');
		$this->load->helper('date');
		
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("47"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_ATK();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	
	
	public function _ATK($page='')
    {
		
		try{
			
			
			$crud = new grocery_crud();
			//$crud->set_theme('datatables');
			
			$state = $crud->getState();
			
			$crud->set_subject('Master Atk');
			$crud->set_table('mstratk');
			$crud->columns('KodeBarang','NamaBarang','Stock','Satuan');
			$crud->fields('KodeBarang','NamaBarang','Stock','Satuan');
			
			$crud->edit_fields('KodeBarang','NamaBarang','Stock','Satuan');
			$crud->required_fields('KodeBarang','NamaBarang','Stock','Satuan');
			$crud->where('mstratk.deleted','0');
			
			$crud->display_as('KodeBarang','Kode Barang');
			$crud->display_as('NamaBarang','Nama Barang');
			
			
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->unset_read();
			$crud->callback_insert(array($this,'_insert_masteratk'));
			$crud->callback_update(array($this,'_update'));
			$crud->callback_delete(array($this,'_delete'));	
			$crud->callback_field('Satuan',array($this,'add_field_callback_satuan'));
			
			//validasi field unik
			$crud->set_rules('KodeBarang', 'Kode Barang', 'trim|required|xss_clean|is_unique[mstratk.KodeBarang]');
			
			
			$output = $crud->render();
			$this-> _outputview($output,$page); 
		}
		
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
	
	function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Master ATK',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		if($page!='')
		{
			$this->load->view('Atk/atkSimple_view',$data);
		}
		else
		{
			$this->template->load('default','templates/CRUD_view',$data);
		}
		
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert_masteratk($post_array){
	
		try{
		$post_array['Deleted'] = '0';
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		
		$this->db->trans_begin();
		$this->db->insert('mstratk',$post_array);
		
		$this->db->trans_commit();
	
			
		return true;
			
	}
	
		catch(Exception $e)
		{
			
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	
	}
	
	function _update($post_array,$primary_key){
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			
			$this->db->trans_commit();
			
			$this->db->update('mstratk',$post_array,array('KodeBarang' => $primary_key));
			$KodeBarang = $primary_key;
			
		
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('mstratk',$post_array,array('KodeBarang' => $primary_key));
	}
	
	function add_field_callback_satuan($value = '', $primary_key = null)
	{
		
		$selectedPcs = '';
		$selectedRim = '';
		$selectedLusin = '';
		$selectedSet = '';
		$selectedRoll = '';
		$selectedBox = '';
		$selectedSBotol = '';
		$selectedPack = '';
		
		switch($value)
		{
			case "Pcs": $selectedPcs = 'selected'; break;
			case "Rim": $selectedRim = 'selected'; break;
			case "Lusin": $selectedLusin = 'selected'; break;
			case "Set": $selectedSet = 'selected'; break;
			case "Roll": $selectedRoll = 'selected'; break;
			case "Box": $selectedBox = 'selected'; break;
			case "Botol": $selectedSBotol = 'selected'; break;
			case "Pack": $selectedPack = 'selected'; break;
		}
		
		return ' <select name="Satuan">
			<option value = ""></option>
			<option '.$selectedPcs.' value="Pcs">Pcs</option>
			<option '.$selectedRim.' value="Rim">Rim</option>
			<option '.$selectedLusin.' value="Lusin">Lusin</option>
			<option '.$selectedSet.' value="Set">Set</option>
			<option '.$selectedRoll.' value="Roll">Roll</option>
			<option '.$selectedBox.' value="Box">Box</option>
			<option '.$selectedSBotol.' value="Botol">Botol</option>
			<option '.$selectedPack.' value="Pack">pack</option>
		</select>';
	}
	
	

	
	

	
	
}
?>