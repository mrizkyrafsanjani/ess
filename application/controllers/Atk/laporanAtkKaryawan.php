<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class LaporanAtkKaryawan extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('atk_model','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		if($this->session->userdata('logged_in')){
			if(check_authorized("47"))
			{
				$this->_laporanAtk();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function viewUser($tahun='',$bulan='',$nama='',$NPKuser='',$KodeUserTask='')
	{
		if($KodeUserTask == 'non')
			$KodeUserTask = '';
		if($nama == 'non')
			$nama = '';
		if($tahun == 'non')
			$tahun = date('Y');
		if($bulan == 'non')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("51"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_laporanAtk('viewUser',$nama,$tahun,$bulan,$KodeUserTask);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function viewAdmin($tahun='',$bulan='',$nama='non')
	{
		if($nama == 'non')
			$nama = 'xxx';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("52"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_laporanAtk($nama,$tahun,$bulan);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	
	public function _laporanAtk($nama='',$tahun='',$bulan='',$KodeUserTask='',$KodeRequestAtk='')
    {
		$crud = new grocery_crud();
		$crud->set_subject('Laporan ATK Karyawan');
		//$crud->set_theme('datatables');
		
		$this->db->select('detailrequestatk.KodeBarang,sum(JumlahBarang) as TotalBarang')->group_by('detailrequestatk.KodeBarang');		
		
		$crud->set_table('detailrequestatk');
		$crud->set_relation('KodeRequestAtk','headerrequestatk','Periode,Status',array('deleted' => '0'));
		$crud->set_relation('KodeBarang','mstratk','NamaBarang',array('deleted' => '0'));
		
		//$crud->columns('NamaBarang','JumlahBarang','Status');
		$crud->columns('KodeBarang','saf1794e4','TotalBarang');
		$crud->callback_column('saf1794e4',array($this,'_callback_Status'));
			
		$crud->unset_read();
		$crud->unset_edit();
		$crud->unset_delete();
		//$crud->unset_print();
		$crud->unset_add();
		//$crud->unset_export();
		$crud->display_as('KodeBarang','Nama Barang');
		$crud->display_as('JumlahBarang','Jumlah');
		$crud->display_as('saf1794e4','Status');
		
		
		fire_print('log',"nama: $nama, tahun: $tahun, bulan: $bulan");
		if($nama != '')
		{
			$crud->like('detailrequestatk.NPK',$nama);
		}
		
		if($tahun != '' && $bulan != '0')
		{
			$crud->where('Periode',$tahun.substr("0".$bulan,-2));
		}
		
		fire_print('log',"tahun: $tahun, bulan: $bulan");
		if($tahun != '' && $bulan =='0')
		{
			$crud->where('left(Periode,4)',$tahun);
		}
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Laporan ATK Karyawan',
			   'body' => $output
		  );
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		
       $this->load->view('Atk/atkSimple_view',$data);
    }
	
	function _callback_status($value, $row){
		//untuk cek array di dalam  grocery crud
		//$text="test ".print_r($row,true);
		$text = $value;
		switch($value)
		{
			case "PEP": $text = "Pending Approval ATK Dept. Head"; break;
			case "DEP": $text = "Rencana Declined ATK Dept. Head"; break;
			case "APP": $text = "Rencana Approved  GA"; break;
			case "PER": $text = "Pending Approval Realisasi ATK"; break;
			case "DER": $text = "Realisasi Declined ATK"; break;
			case "APR": $text = "Realisasi Approved ATK GA Head"; break;
			
		}
		return $text;
	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */