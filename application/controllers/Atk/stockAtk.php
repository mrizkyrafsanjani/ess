<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class StockAtk extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('atk_model','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$this->load->library('grocery_crud');
		$this->load->helper('date');
		
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("47"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_StockAtk();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	
	
	public function _StockAtk($page='')
    {
		
		try{
			
			
			$crud = new grocery_crud();
			//$crud->set_theme('datatables');
			
			$state = $crud->getState();
			
			$crud->set_subject('Stock Atk');
			$crud->set_table('stockatk');
			$crud->columns('KodeBarang','Harga','Jumlah','CreatedBy','CreatedOn');
			$crud->fields('KodeBarang','Harga','Jumlah','Tahun','Bulan');
			
			$crud->edit_fields('KodeBarang','Harga','Jumlah','Tahun','Bulan');
			$crud->required_fields('KodeBarang','Harga','Jumlah');
			$crud->where('stockatk.deleted','0');
			
			$crud->set_relation('KodeBarang','mstratk','NamaBarang',array('deleted' => '0'));
			$crud->set_relation('CreatedBy','mstruser','Nama',array('deleted'=>'0'));
			
			$crud->display_as('KodeBarang','Nama Barang');
			
			$crud->callback_insert(array($this,'_insert_stockatk'));
			$crud->callback_update(array($this,'_update'));
			$crud->callback_delete(array($this,'_delete'));	
			$crud->callback_field('Tahun',array($this,'field_callback_PeriodeTahun'));
			$crud->callback_field('Bulan',array($this,'field_callback_PeriodeBulan'));
			$crud->set_rules('Harga','Harga','integer');
			
			$crud->unset_edit();
			$crud->unset_delete();
			
			$output = $crud->render();
			$this-> _outputview($output,$page); 
		}
		
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
	
	function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Stock ATK',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		if($page!='')
		{
			$this->load->view('Atk/atkSimple_view',$data);
		}
		else
		{
			$this->template->load('default','templates/CRUD_view',$data);
		}
		
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert_stockatk($post_array){
		try{
			$error = 0;
			$post_array['Deleted'] = '0';
			$post_array['CreatedOn'] = date('Y-m-d H:i:s');
			$post_array['CreatedBy'] = $this->npkLogin;
			
			fire_print('log','insertStock Tahun: '.$post_array['Tahun']);
			fire_print('log','insertStock Bulan: '.$post_array['Bulan']);
			
			$post_array['Periode'] =  $post_array['Tahun'].substr ("0".$post_array['Bulan'],-2);
			unset($post_array['Tahun']);
			unset($post_array['Bulan']);
			fire_print('log','insertStock postarray: '. print_r($post_array,true));
			$this->db->trans_begin();
			
			if(!$this->atk_model->updateStock($this->npkLogin,$post_array['Jumlah'],$post_array['KodeBarang']))
				$error += 1;
			if(!$this->db->insert('stockatk',$post_array))
				$error += 1;
			fire_print('log',"_insert_stockatk. error: $error");
			if($error == 0){
				$this->db->trans_commit();
				return true;
			}else{
				$this->db->trans_rollback();
				return false;
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function _update($post_array,$primary_key){
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			
			$this->db->trans_commit();
			
			$this->db->update('stockatk',$post_array,array('KodeStock' => $primary_key));
			$KodeBarang = $primary_key;
			
		
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('stockatk',$post_array,array('KodeStock' => $primary_key));
	}
	
	function field_callback_PeriodeTahun($value='',$primary_key=null)
	{
		$strSelectHTML = ' <select name="Tahun">
			<option value = ""></option>';
		for($i=date("Y");$i<date("Y")+11;$i++)
		{
			$selectedTahun = '';
			if($i == $value)
			{
				$selectedTahunKelulusan = 'selected';
			}
			$strSelectHTML .= '<option '.$selectedTahun.' value ="'.$i.'">'.$i.'</option>';
		}
		$strSelectHTML .= '</select>';
		
		return $strSelectHTML;
		
	}
	
	function field_callback_PeriodeBulan($value='',$primary_key=null)
	{
		$strSelectHTML = '<select name="Bulan">
			<option value = ""></option>';
		for($i=1;$i<=12;$i++)
		{
			$selected = '';
			if($i == $value)
			{
				$selected = 'selected';
			}
			$strSelectHTML .= '<option '.$selected.' value ="'.$i.'">'.date("F",mktime(0,0,0,$i,10)).'</option>';
		}
		$strSelectHTML .= '/<select>';
		
		return $strSelectHTML;
		
	}
	
	
	

	
	

	
	
}
?>