<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class HeaderAtk extends CI_Controller {
	var $npkLogin;
	var $kodeAtkGlobal;
	
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('atk_model','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$this->load->library('grocery_crud');
		$this->load->helper('date');
		$this->kodeAtkGlobal = '';
		
    }
 
    public function index($kodeUserTask='',$status='')
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("51"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_HeaderATK();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	public function viewUser($tahun='',$bulan='',$nama='',$NPKuser='',$KodeUserTask='')
	{
		if($KodeUserTask == 'non')
			$KodeUserTask = '';
		if($nama == 'non')
			$nama = '';
		if($tahun == 'non')
			$tahun = date('Y');
		if($bulan == 'non')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("51"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_HeaderATK('viewUser',$NPKuser,$tahun,$bulan,$KodeUserTask);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function viewAdmin($tahun='',$bulan='',$nama='non')
	{
		if($nama == 'non')
			$nama = 'xxx';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("52"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_HeaderATK('viewAdmin',$nama,$tahun,$bulan);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function _HeaderATK($page='',$nama='',$tahun='',$bulan='',$KodeUserTask='')
    {
		
		try{
			
			
			$crud = new grocery_crud();
			//$crud->set_theme('datatables');
			
			$state = $crud->getState();
			
			$crud->set_subject('Header Atk');
			$crud->set_table('headerrequestatk');
			$crud->where('headerrequestatk.deleted','0');
			$crud->columns('KodeRequestAtk','Status','CreatedOn');
			$crud-> order_by('headerrequestatk.UpdatedOn','desc');
			$crud->fields('KodeRequestAtk','CreatedOn');
			$crud->where('headerrequestatk.CreatedBy',$this->npkLogin);
			$crud->display_as('KodeRequestAtk','Kode Requester');
			$crud->display_as('CreatedOn','Tanggal Request');
			$crud->unset_read();
			$crud->unset_delete();
			$crud->unset_edit();
			$crud->unset_print();
			$crud->unset_add();
			$crud->unset_export();
			
			
			$crud->callback_column('Status',array($this,'_callback_Status'));
			
			if($nama != '')
			{
				$crud->where('headerrequestatk.CreatedBy',$nama);
			}
			if($tahun != '' && $bulan != '0')
			{
				$crud->where('Periode',$tahun.substr("0".$bulan,-2));
			}
			
			if($KodeUserTask != '')
			{				
				$ATK = $this->atk_model->getAtk($KodeUserTask);
				foreach($ATK as $rowATK){
					$KodeRequestAtk = $rowATK->KodeRequestAtk;
				}
				
				$crud->where('headerrequestatk.KodeRequestAtk',$KodeRequestAtk);
			}
			
			fire_print('log',"nama: $nama, tahun: $tahun, bulan: $bulan");
			$output = $crud->render();
			$this-> _outputview($output,$page);  
		}
		
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
	
	function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Header Atk',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		fire_print('log','page cuti: '.$page);
		if($page!='')
		{
			$this->load->view('Atk/atkSimple_view',$data);
		}
		else
		{
			$this->template->load('default','templates/CRUD_view',$data);
		}
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _callback_status($value, $row){
		$text="test";
		switch($value)
		{
			case "PEP": $text = "Pending Approval ATK Dept. Head"; break;
			case "DEP": $text = "Rencana Declined ATK Dept. Head"; break;
			case "APP": $text = "Rencana Approved  GA"; break;
			case "PER": $text = "Pending Approval Realisasi ATK"; break;
			case "DER": $text = "Realisasi Declined ATK"; break;
			case "APR": $text = "Realisasi Approved ATK GA Head"; break;
			
		}
		return $text;
	}
	
	
	

	
	

	
	
}
?>