<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class AtkController extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('atk_model','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('departemen','',TRUE);
		$this->load->model('mstrgolongan','',TRUE);
		$this->load->model('user','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("47"))
			{
				$this->npkLogin = $session_data['npk'];
				
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
    }
	
	function InputATK()
	{
		try
		{
			$session_data = $this->session->userdata('logged_in');
			if($session_data){
				if(check_authorized("47"))
				{
					$this->npkLogin = $session_data['npk'];
					
					
					
					$headeratk = $this->atk_model->getDepartemen($this->npkLogin);
					$departemen = $headeratk['departemen'];
					$historycatatan = '';
					
					
						$atk = array(
							'title' => 'Input ATK - PIC User',
							'editmode' => '0',
							'kodeusertask' => '',
							'departemen' => $departemen,
							'tanggal' => date('Y-m-d H:i:s'),
							'npk' => $this->npkLogin,
							'historycatatan' => $historycatatan
						);
					
					
					
					$this->load->helper(array('form','url'));
					$this->template->load('default','Atk/atk_input_view',$atk);
				}
			}
			else
			{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function EditATK($kodeUserTask)
	{
		try
		{
			$session_data = $this->session->userdata('logged_in');
			if($session_data){
				if(check_authorized("47"))
				{
					$this->npkLogin = $session_data['npk'];
					
					$dtAtk = $this->atk_model->getAtk($kodeUserTask);
					$npkSelected = '';
					$KodeRequestAtk = '';
					foreach($dtAtk as $dt)
					{
						$npkSelected = $dt->CreatedBy;
						$KodeRequestAtk = $dt->KodeRequestAtk;
					}
					
					$historycatatan = $this->usertask->getCatatan($KodeRequestAtk);
					$headeratk = $this->atk_model->getDepartemen($this->npkLogin);
					$departemen = $headeratk['departemen'];
					
					
						$atk = array(
							'title' => 'Edit ATK - PIC User',
							'editmode' => '1',
							'kodeusertask' => $kodeUserTask,
							'departemen' => $departemen,
							'tanggal' => date('Y-m-d H:i:s'),
							'npk' => $this->npkLogin,
							'historycatatan' => $historycatatan
						);
					
					
					
					$this->load->helper(array('form','url'));
					$this->template->load('default','Atk/atk_input_view',$atk);
				}
			}
			else
			{
				redirect("login?u=Atk/atkController/EditATK/$kodeUserTask",'refresh');
			}
		
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	

	function ajax_submitAtk()
	{
		try
		{
			$success = "Berhasil";
			$npk = $this->input->post('npk');
			$editmode = $this->input->post('editmode');
			$kodeusertask = $this->input->post('kodeusertask');
			$catatan = $this->input->post('catatan');
			fire_print('log',"kodeusertask: $kodeusertask");
			
			if($editmode == 'true')
			{
				$dtAtk = $this->atk_model->getDtAtk($kodeusertask);
			}
			
			$this->db->trans_begin();
			$dataAtk = array(
				"KodeRequestAtk" => generateNo('AT'),
				"Status" => 'PEP',
				"CreatedBy" => $npk,
				"CreatedOn" => date('Y-m-d H:i:s')		
			);
			
			if($editmode == 'true')
			{
				$dataAtk['KodeRequestAtk'] = $dtAtk['KodeRequestAtk'];
				fire_print('log', 'dataAtk Edit :'. print_r($dataAtk,true));
				$this->db->update('headerrequestatk',$dataAtk,array('KodeRequestAtk'=>$dtAtk['KodeRequestAtk']));
				$this->usertask->updateStatusUserTask($kodeusertask,'AP',$this->npkLogin);
				
			}
			else
			{
				$this->db->insert('headerrequestatk',$dataAtk);
				$this->db->update('detailrequestatk',array('KodeRequestAtk'=>$dataAtk['KodeRequestAtk']),array('CreatedBy'=>$npk,'KodeRequestAtk'=>''));
			}
			
			$KodeRequestAtk = $dataAtk['KodeRequestAtk'];
			$KodeUserTask = generateNo('UT');
			$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeRequestAtk,$KodeUserTask,"AT");
			if(!$hasilTambahUserTask)
			{
				//echo('gagal simpan usertask atk');
				fire_print('log','insert atk rollback');
				$this->db->trans_rollback();
				$success = 'Gagal menambahkan ke dalam sistem';
			}else{
				fire_print('log','insert atk commit');
				$this->db->trans_commit();					
			}
			
			echo $success;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function ajax_cancelAtk()
	{
		try
		{
			$success = "Berhasil";
			$npk = $this->input->post('npk');
			$this->db->trans_start();
			
			$this->db->delete('detailrequestatk',array('CreatedBy'=>$npk,'KodeRequestAtk'=>''));
			
			$this->db->trans_complete();
			
			if ($this->db->trans_status() === FALSE)
			{
				$success = "Gagal";
			}
			
			echo $success;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}		
	
	function ApprovalRencanaAtk($KodeUserTask)
	{
		try
		{
			if($this->session->userdata('logged_in'))
			{
				if(check_authorized("75"))
				{
					$dataAtk= $this->atk_model->getHeaderAtk($KodeUserTask);
					
					foreach($dataAtk as $row){	
						$historycatatan = $this->usertask->getCatatan($row->KodeRequestAtk);
						$identitas = $this->user->dataUser($row->CreatedBy);
						foreach($identitas as $datauser){
							$departemen = $datauser->departemen;
						}
						
						$data = array(
							'realisasi' => '0',
							'title' => 'Approval Rencana ATK',
							'admin' => '0',
							'kodeusertask' => $KodeUserTask,
							"npk" => $this->npkLogin,
							'departemen' => $departemen,
							'koderequestatk' => $row->KodeRequestAtk,
							'tanggal' => $row->CreatedOn,
							'historycatatan' => $historycatatan
						);
					}
					$this->load->helper(array('form','url'));
					$this->template->load('default','Atk/approvalAtk_view',$data);
				}
			}
			else
			{
				redirect("login?u=Atk/atkController/ApprovalRencanaAtk/$KodeUserTask",'refresh');
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function ApprovalRealisasiAtk($KodeUserTask)
	{
		try
		{
			if($this->session->userdata('logged_in'))
			{
				if(check_authorized("75"))
				{
					$dataAtk= $this->atk_model->getHeaderAtk($KodeUserTask);
					foreach($dataAtk as $row){	
						$historycatatan = $this->usertask->getCatatan($row->KodeRequestAtk);
						$identitas = $this->user->dataUser($row->CreatedBy);
						foreach($identitas as $datauser){
							$departemen = $datauser->departemen;
						}
						
						$data = array(
							'realisasi' => '1',
							'title' => 'Approval Realisasi ATK',
							'admin' => '0',
							'kodeusertask' => $KodeUserTask,
							"npk" => $this->npkLogin,
							'departemen' => $departemen,
							'koderequestatk' => $row->KodeRequestAtk,
							'tanggal' => $row->CreatedOn,
							'historycatatan' => $historycatatan
						);
					}
					$this->load->helper(array('form','url'));
					$this->template->load('default','Atk/approvalAtk_view',$data);
				}
			}
			else
			{
				redirect("login?u=Atk/atkController/ApprovalRealisasiAtk/$KodeUserTask",'refresh');
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function ajax_prosesApproval()
	{
		try
		{
			$success = "Berhasil";
			$KodeUserTask = $this->input->post('kodeusertask');
			$status = $this->input->post('status');
			$catatan = $this->input->post('catatan');
			$query = $this->atk_model->getAtk($KodeUserTask);
			
			if($query){
				foreach($query as $row){
					$statusRencana = substr($row->Status, -1);
					$KodeRequestAtk = $row->KodeRequestAtk;
					$pengajuAtk = $row->CreatedBy;
					$statusATK = substr($row->Status, 0, 2); //dapatkan PE
				}
			}else{
				$success = "Tidak ada data";
			}
			
			if($statusATK == 'PE')
			{
				//update status ATK
				$this->db->trans_begin();
				if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'AT') && $status == 'AP')
				{
					$this->atk_model->updateStatusATK($KodeUserTask,$status.$statusRencana,$this->npkLogin);
				}
				
				if($status == 'DE')
				{
					$this->atk_model->updateStatusATK($KodeUserTask,$status.$statusRencana,$this->npkLogin);
				}
				
				//update user task sekarang
				$this->usertask->updateStatusUserTask($KodeUserTask,$status.$statusRencana,$this->npkLogin,$catatan);
				
				//create user task baru untuk next proses
				$KodeUserTaskNext = generateNo('UT');			
				if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestAtk,$KodeUserTaskNext,"AT".$status,$pengajuAtk))
				{
					$success = "GAGAL USERTASK";
					fire_print('log','trans rollback approve atk ');
					$this->db->trans_rollback();
				}
				else
				{
					fire_print('log','trans commit approve atk ');
					$this->db->trans_commit();
				}
				
				fire_print('log','Proses Trans Approve atk selesai ');
			}
			else
			{
				$success = "GAGAL UPDATE STATUS";
			}
			echo $success;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function ajax_prosesApprovalRealisasi()
	{
		try
		{
			$error = 0;
			$success = "Berhasil";
			$KodeUserTask = $this->input->post('kodeusertask');
			$status = $this->input->post('status');
			$catatan = $this->input->post('catatan');
			//$npk = $this->input->post('npk');
			$this->db->trans_begin();
			$query = $this->atk_model->getAtk($KodeUserTask);
			if($query)
			{
				foreach($query as $row){
					$statusRencana = 'R'; //substr($row->StatusApproval, -1);
					$KodeRequestAtk = $row->KodeRequestAtk;
					$pengajuAtk = $row->CreatedBy;
				}
			}
			else
			{
				$success = "Tidak ada data";
			}
			
			//update status ATK
			if($this->atk_model->updateStatusATK($KodeUserTask,$status.$statusRencana,$this->npkLogin))
			{
				//update user task sekarang
				$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan);
					
				if($status == "DE"){
					//create user task baru untuk next proses
					$KodeUserTaskNext = generateNo('UT');			
					if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeRequestAtk,$KodeUserTaskNext,"AT".$status,$pengajuAtk))
					{
						$success = "0";
						$error += 1;
					}
				}
			}
			else
			{
				$success = "Sistem Gagal";
				$error += 1;
			}
			
			if($error > 0){
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
			}
			
			echo $success;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function ViewAtkUser($KodeUserTask='') //ViewUser
	{
		try
		{
			if($this->session->userdata('logged_in'))
			{
				if(check_authorized("51"))
				{
					if($KodeUserTask == '')
						$KodeUserTask = 'non';
					$data = array(
						'title' => 'View Atk User',
						'admin' => '0',							
						'npk' => $this->npkLogin,
						'kodeusertask' => $KodeUserTask
					);
					$this->load->helper(array('form','url'));
					$this->template->load('default','Atk/atk_view',$data);
				}
			}
			else
			{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function LaporanAtkKaryawan() //ViewATK Karyawan
	{
		try
		{
			if($this->session->userdata('logged_in'))
			{
				if(check_authorized("52"))
				{
					$data = array(
						'title' => 'View Laporan ATK Karyawan',
						'admin' => '1',
						'databawahan' => $this->user->getDataBawahan($this->npkLogin),
						'npk' => ''
					);
					$this->load->helper(array('form','url'));
					$this->template->load('default','Atk/laporanAtk_view',$data);
				}
			}
			else
			{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function LaporanPoAtk($KodeUserTask='') //View PO ATK
	{
		try
		{
			if($this->session->userdata('logged_in'))
			{
				if(check_authorized("53"))
				{
					if($KodeUserTask == '')
						$KodeUserTask = 'non';
					$data = array(
						'title' => 'Laporan PO ATK',
						'admin' => '0',							
						'npk' => $this->npkLogin,
						'kodeusertask' => $KodeUserTask
					);
					$this->load->helper(array('form','url'));
					$this->template->load('default','Atk/laporanPoAtk_view',$data);
				}
			}
			else
			{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function ajax_prosesGenerateATK()
	{
		$error = 0;
		$periodeTahun = $this->input->post('tahun');
		$periodeBulan = $this->input->post('bulan');
		
		$periode = $periodeTahun. substr("0".$periodeBulan,-2);
		fire_print('log',"periodeBulan: $periodeBulan . periode:$periode");
		
		//cek apakah periode sudah ada di dalam database
		if($this->atk_model->getPeriode($periode))
		{
			fire_print('log','Gagal karena ada periode yang sama');
			echo "Gagal karena ada periode yang sama";
		}
		else
		{
			//dapatkan data ATK direquest
			$dataATKDetailRequest = $this->atk_model->getDetailRequest();
			if($dataATKDetailRequest)
			{
				$this->db->trans_begin();
				foreach($dataATKDetailRequest as $dtATKdetil)
				{
					//insert ke dalam tabel poatk
					$dataPoAtk = array(
						"KodeBarang" => $dtATKdetil->KodeBarang,
						"Permintaan" => $dtATKdetil->Permintaan,
						"Stock" => $dtATKdetil->Stock,
						"Selisih" => $dtATKdetil->Selisih,
						"Periode" => $periode,
						"CreatedOn" => date('Y-m-d H:i:s'),
						"CreatedBy" => $this->npkLogin
					);
					
					fire_print('log',"masuk ke insert data atkPO. DataPoATK:".print_r($dataPoAtk,true));
					if(!$this->db->insert('poatk',$dataPoAtk))
						$error += 1;
					if(!$this->db->update('headerrequestatk',array('Periode'=>$dataPoAtk['Periode']),array('Periode'=>null)))
						$error += 1;
				}
				if($error > 0)
				{
					$this->db->trans_rollback();
					echo "PO ATK untuk periode $periode gagal terbentuk";
				}
				else
				{
					$this->db->trans_commit();
					echo "PO ATK untuk periode $periode telah sukses terbentuk";
				}
			}
			else
			{
				fire_print('log','tidak terdapat request ATK');
				echo "tidak terdapat request ATK";
			}
		}
	}
}
?>