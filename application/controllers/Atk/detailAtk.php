<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class DetailAtk extends CI_Controller {
	var $npkLogin;
	var $kodeAtkGlobal;
	
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('atk_model','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$this->load->library('grocery_crud');
		$this->load->helper('date');
		$this->kodeAtkGlobal = '';
		
    }
 
    public function index($kodeUserTask='',$status='')
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			
				$this->npkLogin = $session_data['npk'];
				$this->_ATK($kodeUserTask,$status);
			
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	public function _ATK($kodeUserTask,$status)
    {
		try{
			$crud = new grocery_crud();
			//$crud->set_theme('datatables');
			
			$state = $crud->getState();
			
			$crud->set_subject(' Detail Atk');
			$crud->set_table('detailrequestatk');
			$crud->set_relation('KodeBarang','mstratk','NamaBarang',array('deleted' => '0'));
			$crud->set_relation('KodeRequestAtkDetail','mstratk','KodeBarang',array('deleted' => '0'));
			$crud->where('detailrequestatk.deleted','0');
			$crud-> order_by('detailrequestatk.KodeBarang','asc');
			
			
			
			if($status== 'approve' || $status == 'editmode')
			{
				fire_print('log','enter to status approve or editmode');
				$dtltrkrwylist = $this->db->get_where('dtltrkrwy',array('KodeUserTask'=>$kodeUserTask));
				$dtltrkrwy = $dtltrkrwylist->row(1);
				$crud->where('detailrequestatk.KodeRequestAtk',$dtltrkrwy->NoTransaksi);
				$this->kodeAtkGlobal = $dtltrkrwy->NoTransaksi;
				fire_print('log',"kodeAtkGlobal = $this->kodeAtkGlobal");
				
			}
			else //if($status == '')
			{
				fire_print('log','enter to status ' . $status . ' .Kodeusertask :'.$kodeUserTask);
				$crud->where('detailrequestatk.CreatedBy',$this->npkLogin);
				$crud->where('detailrequestatk.KodeRequestAtk','');
			}
		
			$crud->columns('KodeBarang','NPK','JumlahBarang');
			$crud->fields('NPK','KodeBarang','JumlahBarang');
			
			$crud->display_as('NPK','Nama Requester');
			$crud->display_as('KodeBarang','Nama Barang');
			$crud->display_as('JumlahBarang','Jumlah');
			$crud->required_fields('NPK','KodeBarang','JumlahBarang');
			$crud->unset_print();
			$crud->unset_export();
			$crud->unset_read();
			//$crud->unset_edit();
			//$crud->unset_delete();
			
			$crud->set_rules('JumlahBarang','Jumlah Barang','integer');
			$crud->callback_insert(array($this,'_insert_atkdetail'));
			
			$crud->callback_update(array($this,'_update'));
			$crud->callback_delete(array($this,'_delete'));	
			$crud->callback_field('JumlahBarang',array($this,'_field_callback_Jumlah'));
			$crud->callback_field('NPK',array($this,'_field_callback_nama'));
			$crud->callback_column('NPK',array($this,'_column_callback_NPK'));
			
			if($status == "approve")
			{
				$crud->unset_operations();
			}
			$output = $crud->render();			
			$this-> _outputview($output);  
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
	
	function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'ATK',
			   'body' => $output
		  );
		//
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		
		$this->load->view('Atk/atkSimple_view',$data);
		
		
		
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert_atkdetail($post_array){
	
		try{
		$post_array['Deleted'] = '0';
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		
		$this->db->trans_begin();
		$this->db->insert('detailrequestatk',$post_array);
		
		$this->db->trans_commit();
			
		return true;
			
		}
	
		catch(Exception $e)
		{
			
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	
	}
	
	function _update($post_array,$primary_key){
			
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			
			$this->db->trans_begin();			
			if($this->db->update('detailrequestatk',$post_array,array('KodeRequestAtkDetail' => $primary_key))){
				$this->db->trans_commit();
				return true;
			}else{
				$this->db->trans_rollback();
				return false;
			}
		
	}
	
	function _delete($primary_key){
		
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('detailrequestatk',$post_array,array('KodeRequestAtkDetail' => $primary_key));
	}
	
	function _column_callback_NPK($value, $row)
	{
		$dataUser = $this->db->get_where('mstruser',array('NPK'=>$value));
		$namaKaryawan = $dataUser->row(1)->Nama;
		return $namaKaryawan;
	}
	
	function _field_callback_Jumlah($value='',$primary_key=null)
	{
		return "<input name='JumlahBarang' style='width:100px' type='text' value='$value'>";
	}
	
	function _field_callback_nama($value = '', $primary_key = null)
	{
		fire_print('log','enter to callbackNama');
		$dataUser = $this->db->get_where('mstruser',array('NPK'=>$this->npkLogin));
		$departemenUserLogin = $dataUser->row(1)->Departemen;
		
		$dataUserSamaDepartemen = $this->db->get_where('mstruser',array('departemen'=>$departemenUserLogin,"Deleted"=>"0","TanggalBerhenti"=>"0000-00-00"));
		
		$strSelectHTML = '<select name="NPK" class="chosen-select">';		
		foreach($dataUserSamaDepartemen->result() as $rw)
		{
			$selected = "";
			if($rw->NPK == $value){
				$selected = "selected";
			}
			$strSelectHTML .= "<option value = '$rw->NPK' $selected>$rw->Nama</option>";
		}
		return $strSelectHTML . "</select>";
	}
	

	
	

	
	
}
?>