<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	class Login extends CI_Controller {
		function __construct(){
			parent::__construct();
		}
		
		function index(){
			$this->load->helper(array('form','url'));
			$data = array(
				'urlTujuan'=>$this->input->get('u')
			);
			$this->load->view('login_view',$data);
		}
	}
?>