<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	class LupaPassword extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->library('email');
			$this->load->model('user','',TRUE);
			$this->load->helper('string');
		}
		
		 function index()
		 {
		   //This method will have the credentials validation
		   $this->load->library('form_validation');

		   $this->form_validation->set_rules('npk', 'NPK', 'trim|required|xss_clean|callback_check_npk');

		   if($this->form_validation->run() == false)
		   {
			 
		   }

		 }

		 function check_npk($npk)
		 {
		   //Field validation succeeded.  Validate against database
		   $npk = $this->input->post('npk');
			
		   //query the database
		   $result = $this->user->dataUser($npk);

		   if($result)
		   {
			 $random =  random_string('alnum', 8);
			 $this->user->insertPasswordTemp($npk,$random);
			 $sess_array = array();
			 foreach($result as $row)
			 {
			   $sess_array = array(
				 'npk' => $row->npk,
				 'nama' => $row->nama,
				 'email' => $row->email
			   );
			 }
			
			$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
			$this->email->to($sess_array['email']); 
			
			$this->email->subject('Lupa Password ESS');
			$message = 'Seseorang (mungkin Anda) meminta perubahan password melalui verifikasi email. Jika ini bukan Anda, abaikan pesan ini dan tidak akan terjadi apa-apa. 
Jika Anda meminta verifikasi ini, harap masukkan password ini sebagai password Anda dan segera rubah password Anda : 
	
	Password : '. $random ;
			$this->email->message($message);	
			
			if($this->email->send()){
				echo "Password sudah terkirim ke email Anda";
				return true;
			} else {
				echo "Something wrong<br/>";
				echo $this->email->print_debugger();
				return false;
			}
		   }
		   else
		   {
			 echo "NPK yang Anda masukkan tidak ada di dalam database";
			 return false;
		   }
		 }
		

	}
?>