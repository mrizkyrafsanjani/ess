<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');

class submitUpdateUMLain extends CI_Controller {

	 function __construct()
	 {
	   parent::__construct();
	   $this->load->model('uangmuka_model','',TRUE);
	 }

	 function index()
	 {
	  
		 
		 if($this->input->post('submitUpdateUMLain')){

			if ($this->input->post('txtMode')=='edit') 
			{
				$noUM = $this->input->post('txtNoUM');
				if($this->uangmuka_model->updateUMLain($noUM) && $this->uangmuka_model->InsertDtlUangMuka($noUM)){
					redirect('UangMuka/cetakFormUangMuka?NoUangMuka='.$noUM,'refresh');				
				}else{
					echo 'gagal Update ';
				}
			}
			else
			if ($this->input->post('txtMode')=='inputreason') 
			{
				$noUM = $this->input->post('txtNoUM');
				if( $this->uangmuka_model->updateReasonByNoUM($noUM) ){
					redirect('UangMuka/UangMukaController/ViewListUangMukaUser');
				}
			
		 }
	   
	}
}
?>