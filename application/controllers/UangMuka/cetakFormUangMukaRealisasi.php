<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class CetakFormUangMukaRealisasi extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('number');
			$this->load->model('menu','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('uangmuka_model','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}else{
					echo 'gagal';
				}

				$query2 = $this->db->get_where('globalparam',array('Name'=>'DIC_ACC'));
				foreach($query2->result() as $row)
				{
					$npkDICACC = $row->Value;
				}

				$query2 = $this->db->get_where('globalparam',array('Name'=>'DIC_HRGA'));
				foreach($query2->result() as $row)
				{
					$npkDICHRGA = $row->Value;
				}
				
				foreach($this->user->findUserByNPK($npkDICHRGA) as $row)
				{
					$dicHrga = $row->Nama;
				}
				foreach($this->user->findUserByNPK($npkDICACC) as $row)
				{
					$dicAccounting = $row->Nama;
				}
				$headHrga = $this->user->getSingleUserBasedJabatan("HRGA Dept Head")->Nama;
				//$dicHrga = $this->user->getSingleUserBasedJabatan("Director DPA 1")->Nama;
				//$dicAccounting = $this->user->getSingleUserBasedJabatan("President Director DPA 2")->Nama;

				$get_NoUangMuka = $this->input->get('NoUangMuka');
				$trxuangmuka = $this->uangmuka_model->getHeaderTrxUangMukabyNoUMRealisasibyNoUM($get_NoUangMuka);
				if($trxuangmuka){
					$trxuangmuka_array = array();
					foreach($trxuangmuka as $row){
						$trxuangmuka_array[] = array(
							'DPA' => $row->DPA,
							'NoUangMuka' => $row->NoUangMuka,
							'TanggalPermohonan' => $row->TanggalPermohonan,
							'TanggalRealisasi' => $row->TanggalRealisasi,
							'NoPP' => $row->NoPP,
							'KeteranganPermohonan' => $row->KeteranganPermohonan,
							'Status' => $row->Status,
							'TipeBayarUangMuka'=> $row->TipeBayarUangMuka,
							'BankBayarUangMuka'=> $row->BankBayarUangMuka,
							'NoRekBayarUangMuka'=> $row->NoRekBayarUangMuka,
							'NmPenerimaBayarUangMuka'=> $row->NmPenerimaBayarUangMuka,
							'AlasanOutstanding'=> $row->AlasanOutstanding,
							'NoSPD'=> $row->NoSPD,
							'NoAkomodasiTiket'=> $row->NoAkomodasiTiket,
							'TglTerimaHRGAPermohonan'=> $row->TglTerimaHRGAPermohonan,
							'TglTerimaFinancePermohonan'=> $row->TglTerimaFinancePermohonan,
							'TglBayarFinancePermohonan'=> $row->TglBayarFinancePermohonan,
							'TglTerimaHRGARealisasi'=> $row->TglTerimaHRGARealisasi,
							'TglTerimaFinanceRealisasi'=> $row->TglTerimaFinanceRealisasi,
							'TglBayarFinanceRealisasi' => $row->TglBayarFinanceRealisasi,
							'NoBEPermohonan' => $row->NoBEPermohonan,
							'NoBERealisasi' => $row->NoBERealisasi,
							'TipeKembaliUangMuka' => $row->TipeKembaliUangMuka,
							'BankKembaliUangMuka' => $row->BankKembaliUangMuka,
							'NoRekKembaliUangMuka' => $row->NoRekKembaliUangMuka,
							'NmPenerimaUangMuka' => $row->NmPenerimaUangMuka,
							'LebihKurangRealisasi' => $row->LebihKurangRealisasi,
							'CreatedBy' => $row->CreatedBy,
							'CreatedOn' => $row->CreatedOn,
							'JenisKegiatanUangMuka' => $row->JenisKegiatanUangMuka,
							'NPKPemohon' => $row->NPKPemohon,
							'namaNPKPemohon' => $row->atasan,
							'ReasonOutstanding' => $row->ReasonOutstanding,
							'TanggalMulai' => $row->TanggalMulai,
							'TanggalSelesai' => $row->TanggalSelesai,
							'WaktuBayarTercepat' => $row->WaktuBayarTercepat,
							'WaktuPenyelesaianTerlambat' => $row->WaktuPenyelesaianTerlambat,
							'TanggalRealisasi' => $row->TanggalRealisasi,
							'FilePathPermohonan' => $row->FilePathPermohonan,
							'FilePathRealisasi' => $row->FilePathRealisasi,
							'id' => $row->id,
							'NamaCreatedBy' => $row->nama,
							'NamaAtasan' => $row->atasan,
							'NamaJenisKegiatan' =>  $row->JenisKegiatan,
							'TotalPermohonan' => $row->TotalPermohonan,
							'TotalRealisasi' => $row->TotalRealisasi,
							'Selisih' =>  $row->Selisih,
							'terbilang' => terbilang($row->selisihpositip),
						);
					}
				}else{
					echo 'gagalheader';
				}

				$detailuangmuka = $this->uangmuka_model->getDetailTrxUangMukaRealisasibyNoUM($get_NoUangMuka);
				if($detailuangmuka){
					$detailuangmuka_array = array();
					foreach($detailuangmuka as $row){
						$detailuangmuka_array[] = array(
							'iddtluangmuka' => $row->iddtluangmuka,
							'JenisBiayaRealisasi' => $row->JenisBiayaRealisasi,
							'KeteranganRealisasi' => $row->KeteranganRealisasi,
							'JumlahRealisasi' => $row->JumlahRealisasi,
							'HargaRealisasi' => $row->HargaRealisasi,
							 'TotalRealisasi' => $row->TotalRealisasi							 
						);
					}
				}else{
					echo 'gagaldetil';
				}

				$NPKAtasan = $this->uangmuka_model->getAtasan($session_data['npk']);
				$NPKDIC = $this->uangmuka_model->getNPKDIC($NPKAtasan);
				$DICPemohon = $this->uangmuka_model->getNamaDIC($NPKDIC);
				if($DICPemohon){
					$DICPemohon_array = array();
					foreach($DICPemohon as $row){
						$DICPemohon_array[] = array(
							'NamaDIC' => $row->Nama
						);
					}
				}else{
					echo 'gagaldetil';
				}			
				$dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
				if($dataOutstanding){
					$dataOutstanding_array = array();
					foreach($dataOutstanding as $dataOutstanding){
						$dataOutstandingDetail[] = array(
							'title'=> 'Input Form Uang Muka Lainnya',
							//'menu_array' => $menu_array,
							'NoUangMuka' => $dataOutstanding->NoUangMuka,
							'KeteranganPermohonan' => $dataOutstanding->KeteranganPermohonan,
							'Total' => $dataOutstanding->Total,
							'TotalSPD' => $dataOutstanding->TotalSPD,
							'ReasonOutstanding'=> $dataOutstanding->ReasonOutStanding									 
						);
					}
				}		
				if($dataOutstanding){
							$data2 = array(
								'title' => 'Cetak Realisasi Form Uang Muka',
								'trxuangmuka_array' => $trxuangmuka_array,
								'detailuangmuka_array' => $detailuangmuka_array,								
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'flagoutstanding' => '1',
								'dataOutstanding' => $dataOutstandingDetail,
								'DICPemohon_array' => $DICPemohon_array
							);
					}else{
							$data2 = array(
								'title' => 'Cetak Realisasi Form Uang Muka',
								'trxuangmuka_array' => $trxuangmuka_array,
								'detailuangmuka_array' => $detailuangmuka_array,								
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'dataOutstanding' => '0',
								'flagoutstanding' => '0',
								'DICPemohon_array' => $DICPemohon_array
							);
						}
									
				$data2['HeadHRGA'] = $headHrga;
				$data2['DICHRGA'] = $dicHrga;
				$data2['DICACC'] = $dicAccounting;

				$this->load->helper(array('form','url'));
				$this->template->load('default','UangMuka/cetakFormUangMukaRealisasi_view',$data2);
				//$this->template->load('default','cetakFormUangMuka_view',$data2);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
	}
?>