<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class AkomodasiTiketController extends CI_Controller{
		var $npkLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('user','',TRUE);
			$this->load->model('uangmuka_model','',TRUE);
			$this->load->model('akomodasitiket_model','',TRUE);
			$this->load->model('usertask','',TRUE);
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
		}
		
		function index() 
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		
		function ApproveAkomodasiTiket($KodeUserTask)
		{
			try
			{
				if(!$this->session->userdata('logged_in'))
				{
					redirect("login?u=UangMuka/AkomodasiTiketController/ApproveAkomodasiTiket/$KodeUserTask",'refresh');				
				}
				else
				{
					
						$dataApprovalAT = $this->akomodasitiket_model->getDataATByKodeUT($KodeUserTask);
						//$historycatatan = $this->usertask->getCatatan($row->KodeTempMasterCutiUser);
						if($dataApprovalAT)
						{
							foreach($dataApprovalAT as $row)
							{
								$dataApprovalDetail = array(
									'NoAkomodasiTiket' => $row->NoAkomodasiTiket,
									'KodeAkomodasiTiket' => $row->KodeAkomodasiTiket,
									'namapemohon' => $row->namapemohon,
									'npkpemohon' => $row->npkpemohon,
									'TanggalBerangkat'=> $row->TanggalBerangkat,
									'KodeUserTask' => $KodeUserTask,
									'TanggalKembali'=> $row->TanggalKembali,
									'Tujuan'=> $row->Tujuan,
									'Alasan'=> $row->Alasan,
									'NPKAtasan'=> $row->NPKAtasan,
									'NamaHotel'=> $row->NamaHotel,
									'TanggalCheckIn'=> $row->TanggalCheckIn,
									'TanggalCheckOut'=> $row->TanggalCheckOut,
									'TiketPesawat'=> $row->TiketPesawat,
									'TanggalBerangkatPesawat'=> $row->TanggalBerangkatPesawat,
									'JamBerangkatPesawat'=> $row->JamBerangkatPesawat,
									'TanggalKepulanganPesawat'=> $row->TanggalKepulanganPesawat,
									'JamKepulanganPesawat'=> $row->JamKepulanganPesawat,
									'DPA'=> $row->DPA,
									'CreatedOn'=> $row->CreatedOn,
									'StatusApprove'=> $row->StatusApprove,
									'title' => 'Approval Pengajuan Akomodasi Tiket'//,
									//'historycatatan' => $historycatatan
								);
								//fire_print('log',$row->StatusKawin);
								$NamaSelectedUser = $row->namapemohon;							
							}
						}						
						
						
						$data = array(
							'title' =>'Approval Pengajuan Akomodasi Tiket '.$NamaSelectedUser,
							'data'=> $dataApprovalDetail							
						);
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','UangMuka/approvalAkomodasiTiket_view',$data);
					
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
			
		}
		
		function ProsesApprovalATUser($KodeAkomodasiTiket,$StatusApp,$npk)
		{
			try
			{
				$query = $this->db->get_where('dtltrkrwy',array('NoTransaksi'=>$KodeAkomodasiTiket));
				foreach($query->result() as $row)
				{
					$KodeUserTask = $row->KodeUserTask;
				}
				
				$this->db->trans_start();
				
				//tempmstruser di buat status AP/DE
				$this->db->where('KodeAkomodasiTiket',$KodeAkomodasiTiket);
				$this->db->update('akomodasitiket',array(
					'StatusApprove' => $StatusApp,
					'UpdatedOn' => date('Y-m-d H:i:s'),
					'UpdatedBy' => $this->npkLogin
				));

				//user task dibuat status AP/DE
				$this->usertask->updateStatusUserTask($KodeUserTask,$StatusApp,$this->npkLogin);		
				
				
				$this->db->trans_complete();
				echo "Data sudah berhasil di". ($StatusApp == "AP"? "-Approve" : "-Decline") .".";
				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}

		

		

		
		

		
		

	}
?>