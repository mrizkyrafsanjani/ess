<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class LihatStatusAkomodasiTiket extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('number');
			$this->load->model('menu','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('akomodasitiket_model','',TRUE);
			$this->load->model('usertask','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);				
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}else{
					echo 'gagal';
				}

				$dataUser = $this->user->dataUser($session_data['npk']);
				if($dataUser){
					$dataUser_array = array();
					foreach($dataUser as $dataUser){
						$dataUserDetail = array(
							'title'=> 'Input Form Uang Muka Akomodasi & Tiket',
							//'menu_array' => $menu_array,
							'npk' => $dataUser->npk,
							'golongan' => $dataUser->golongan,
							'nama'=> $dataUser->nama,
							'jabatan'=> $dataUser->jabatan,
							'departemen'=> $dataUser->departemen,
							'atasan'=> $dataUser->atasan,
							'NPKAtasan'=> $dataUser->NPKAtasan			 
						);
					}
				}else{
					echo 'gagaldatauser';
				}
			
				$get_KodeAkomodasiTiket = $this->input->get('KodeAkomodasiTiket');
				$get_For = $this->input->get('for');
				$trxAT = $this->akomodasitiket_model->getTrxATbyKode($get_KodeAkomodasiTiket);
				if($trxAT){
					$trxAT_array = array();
					foreach($trxAT as $row){
						$trxAT_array = array(
							'DPA' => $row->DPA,
							'NoAkomodasiTiket' => $row->NoAkomodasiTiket,
							'KodeAkomodasiTiket' => $row->KodeAkomodasiTiket,
							'TanggalBerangkat' => $row->TanggalBerangkat,
							'TanggalKembali' => $row->TanggalKembali,
							'TanggalCheckOut' => $row->TanggalCheckOut,
							 'TanggalCheckIn' => $row->TanggalCheckIn,
							 'TanggalBerangkatPesawat'=> $row->TanggalBerangkatPesawat,
							 'TanggalKepulanganPesawat'=> $row->TanggalKepulanganPesawat,
							 'Tujuan'=> $row->Tujuan,
							 'Alasan'=> $row->Alasan,
							 'NPKAtasan'=> $row->NPKAtasan,
							 'NamaHotel'=> $row->NamaHotel,
							 'TiketPesawat'=> $row->TiketPesawat,
							 'JamBerangkatPesawat'=> $row->JamBerangkatPesawat,
							 'JamKepulanganPesawat'=> $row->JamKepulanganPesawat,
							 'KeteranganStatus'=> $row->StatusApprove,
							 'AlasanDecline'=> $row->AlasanDecline
						);
					}
				}else{
					echo 'gagalAT';
				}

				$NPKGA = $this->akomodasitiket_model->getGlobalParam('AdminGA');

							$data2 = array(
								'title' => 'Cetak Form Uang Muka',
								'trxAT_array' => $trxAT_array,	
								'dataUserDetail' => $dataUserDetail	,							
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'NPKGA' => $NPKGA
							);


				$this->load->helper(array('form','url'));
				$this->template->load('default','UangMuka/lihatStatusAkomodasiTiket_view',$data2);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}

		function ProsesApprovalAkomodasiTiket($KodeAkomodasiTiket,$StatusApp)
		{
			try
			{
				$query = $this->db->get_where('dtltrkrwy',array('NoTransaksi'=>$KodeAkomodasiTiket));
				foreach($query->result() as $row)
				{
					$KodeUserTask = $row->KodeUserTask;
				}
				
				$this->db->trans_start();
				
				//akomodasitiket di buat status AP/DE
				$this->db->where('akomodasitiket',$KodeAkomodasiTiket);
				$this->db->update('akomodasitiket',array(
					'StatusTransaksi' => $StatusApp,
					'UpdatedOn' => date('Y-m-d H:i:s'),
					'UpdatedBy' => $this->npkLogin
				));
				
				//user task dibuat status AP/DE
				$this->usertask->updateStatusUserTask($KodeUserTask,$StatusApp,$this->npkLogin);
				//kirim notif ke user
				$this->usertask->sendEmailNotifAT($StatusApp,$this->npkLogin,'',$KodeAkomodasiTiket);
				$this->usertask->sendEmailHRGA($StatusApp,$this->npkLogin,'',$KodeAkomodasiTiket);
				
				
				$this->db->trans_complete();
				echo "Data sudah berhasil di". ($StatusApp == "AP"? "-Approve" : "-Decline") .".";				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
	}
?>