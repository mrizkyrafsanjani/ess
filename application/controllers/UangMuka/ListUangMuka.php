<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class ListUangMuka extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
	
		$this->load->library('grocery_crud');
		$this->load->model('uangmuka_model','',TRUE);
		$this->load->model('akomodasitiket_model','',TRUE);
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Uang Muka") || check_authorizedByName("Uang Muka Admin") )
			{
				$this->npkLogin = $session_data['npk'];				
			}
		}else{
			redirect('login','refresh');
		}
    }
	
	public function search_pengajuan_user($Bulan='', $Tahun='', $PilihanTglAwal ='',$PilihanTglAkhir ='',$Status='')
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Uang Muka"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_listpengajuan( $Bulan, $Tahun,$PilihanTglAwal,$PilihanTglAkhir,'user',$Status);
			}
		}else{
			redirect('login','refresh');
		}
	}
	
	

	public function _listpengajuan( $Bulan='',$Tahun='',$PilihanTglAwal ='',$PilihanTglAkhir ='',$JenisUser='',$Status='')
    {
				
		$crud = new grocery_crud();
		$crud->set_subject('List Pengajuan');
		$crud->set_theme('flexigrid');
		
		
		$crud->set_model('custom_query_model');
		$crud->set_table('trxuangmuka');
		 //Change to your table name

		
		$begin =$PilihanTglAwal;
		$end = $PilihanTglAkhir;

		$session_data = $this->session->userdata('logged_in');
		$npkLogin = $session_data['npk'];
	
		
		if ($JenisUser=='user')
			{
				$praquery1=  "tum.TanggalPermohonan between '".$begin."' and '".$end."' and tum.CreatedBy='".$npkLogin."'
						   and tum.TanggalRealisasi is null and Status='P' " ;
			}
		else
			{
				$praquery1=  "tum.TanggalPermohonan between '".$begin."' and '".$end."' and tum.TanggalRealisasi is null and Status='P' " ;
				
			}
		
		switch($Status)
		{
			case 1: $praquery1 .= " and tum.TglTerimaHRGAPermohonan is null "; break;
			case 2: $praquery1 .= " and tum.TglTerimaFinancePermohonan is null "; break;
			case 3: $praquery1 .= " and tum.TglBayarFinancePermohonan is null "; break;
			case 4: $praquery1 .= " and tum.NoBEPermohonan is null "; break;
		}
		
		
		$sqlquery = "select tum.id,tum.NoUangMuka,tum.NoSPD,tum.KeteranganPermohonan,JenisKegiatanUangMuka ,mj.JenisKegiatan, tum.TanggalPermohonan, 
								tum.WaktuPenyelesaianTerlambat, tum.WaktuBayarTercepat,tum.TglTerimaHRGAPermohonan,tum.TglTerimaFinancePermohonan, tum.KodeVendor,
								tum.TglBayarFinancePermohonan, case when length(tum.NoSPD) > 0 then format(spd.TotalSPD,0) else format(dtl.TotalPermohonan,0) end as TotalPermohonan,
								case when tum.NoSPD<>'' then 'UM Perjalanan Dinas' else
								case when NoAkomodasiTiket<>'' then 'UM Akomodasi Tiket' else
								'UM Lainnya' end end as TipeUangMuka, tum.CreatedBy,tum.TanggalRealisasi,tum.FilePathPermohonan, tum.status,m.nama as NamaPemohon,
								tum.NoBEPermohonan
						from trxuangmuka tum join mstrjnskegiatanum mj on mj.IdKegiatan=tum.JenisKegiatanUangMuka
							left join (select NoUangMuka, sum(TotalPermohonan) as TotalPermohonan from dtluangmuka where deleted = 0 group by NoUangMuka) dtl on dtl.NoUangMuka = tum.NoUangMuka
							join mstruser m on m.NPK=tum.NPKPemohon
							left join (select * from trkspd where deleted = 0) spd on spd.NoSPD = tum.NoSPD
						where ".$praquery1." and tum.Deleted = 0 
						order by tum.NoUangMuka
					  ";	
	
		
		$crud->basic_model->set_query_str($sqlquery); //Query text here
	
		if ($JenisUser=='user')
		{
			$crud->columns('NoUangMuka','KeteranganPermohonan','TanggalPermohonan','TipeUangMuka','TotalPermohonan','WaktuBayarTercepat','WaktuPenyelesaianTerlambat','KodeVendor','TglTerimaHRGAPermohonan','TglTerimaFinancePermohonan','TglBayarFinancePermohonan','NoBEPermohonan');
			$crud->fields('id','NoUangMuka','KeteranganPermohonan','TanggalPermohonan','TipeUangMuka','TotalPermohonan','WaktuBayarTercepat','WaktuPenyelesaianTerlambat','KodeVendor','TglTerimaHRGAPermohonan','TglTerimaFinancePermohonan','TglBayarFinancePermohonan','NoBEPermohonan');
			
			$crud->display_as('NoUangMuka','No Uang Muka'); $crud->display_as('KeteranganPermohonan','Keterangan');$crud->display_as('TanggalPermohonan','Tgl Permohonan');$crud->display_as('TipeUangMuka','Tipe');$crud->display_as('TotalPermohonan','Total');
			$crud->display_as('WaktuBayarTercepat','Waktu Bayar');$crud->display_as('WaktuPenyelesaianTerlambat','Waktu Penyelesaian');$crud->display_as('TglTerimaHRGAPermohonan','Tgl Terima HRGA');$crud->display_as('TglTerimaFinancePermohonan','Tgl Terima Finance');$crud->display_as('TglBayarFinancePermohonan','Tgl Bayar Finance');
			$crud->display_as('NoBEPermohonan','No BE');$crud->display_as('KodeVendor','Kode Vendor');
			
			$crud->add_action('Upload/Lihat Dokumen Permohonan', base_url('/assets/images/upload.png'), '','',array($this,'callback_action_upload'));
			$crud->add_action('Edit/Lihat Permohonan', base_url('/assets/images/edit.png'), '','',array($this,'callback_action_edit'));	
			$crud->add_action('Realisasi Permohonan', base_url('/assets/images/nextblack.png'), '','',array($this,'callback_action_realisasi'));		
			
			$crud->callback_delete(array($this,'_deletepengajuan'));
			$crud->set_lang_string('delete_error_message', 'Data tidak dapat dihapus karena telah diterima oleh HRGA.');
			//$crud->unset_delete();
			$crud->unset_edit();
			$crud->unset_add();
			$crud->unset_read();
			$crud->unset_print();	
		//	$crud->unset_export();
		}
		else //admin
		{
			$crud->columns('NoUangMuka','NamaPemohon','KeteranganPermohonan','TanggalPermohonan','TipeUangMuka','TotalPermohonan','WaktuBayarTercepat','WaktuPenyelesaianTerlambat','KodeVendor','TglTerimaHRGAPermohonan','TglTerimaFinancePermohonan','TglBayarFinancePermohonan','NoBEPermohonan');
			$crud->fields('id','NoUangMuka','KeteranganPermohonan','TanggalPermohonan','KodeVendor','TglTerimaHRGAPermohonan','TglTerimaFinancePermohonan','TglBayarFinancePermohonan','NoBEPermohonan');

			$crud->display_as('NoUangMuka','No Uang Muka'); $crud->display_as('KeteranganPermohonan','Keterangan');$crud->display_as('TanggalPermohonan','Tgl Permohonan');$crud->display_as('TipeUangMuka','Tipe');$crud->display_as('TotalPermohonan','Total');
			$crud->display_as('WaktuBayarTercepat','Waktu Bayar');$crud->display_as('WaktuPenyelesaianTerlambat','Waktu Penyelesaian');$crud->display_as('TglTerimaHRGAPermohonan','Tgl Terima HRGA');$crud->display_as('TglTerimaFinancePermohonan','Tgl Terima Finance');$crud->display_as('TglBayarFinancePermohonan','Tgl Bayar Finance');
			$crud->display_as('KodeVendor','Kode Vendor');

			$crud->add_action('Upload/Lihat Dokumen Permohonan', base_url('/assets/images/upload.png'), '','',array($this,'callback_action_upload'));
				
			$crud->callback_field('KeteranganPermohonan',array($this,'add_field_callback_KeteranganPermohonan'));
			$crud->callback_field('TanggalPermohonan',array($this,'add_field_callback_TanggalPermohonan'));
			//$crud->callback_field('TotalPermohonan',array($this,'add_field_callback_TotalPermohonan'));
			$crud->callback_field('id',array($this,'add_field_callback_id'));
			$crud->callback_field('NoUangMuka',array($this,'add_field_callback_NoUangMuka'));
			$crud->add_action('Lihat Permohonan', base_url('/assets/images/read.png'), '','',array($this,'callback_action_lihat'));	
		
			
			$initdept =	$this->uangmuka_model->getInitDept($npkLogin);
			if ($initdept=='HRGA' )
			{
				$crud->callback_field('KodeVendor',array($this,'add_field_callback_KodeVendor'));
				$crud->callback_field('TglTerimaFinancePermohonan',array($this,'add_field_callback_TglTerimaFinancePermohonan'));
				$crud->callback_field('TglBayarFinancePermohonan',array($this,'add_field_callback_TglBayarFinancePermohonan'));
				$crud->callback_field('NoBEPermohonan',array($this,'add_field_callback_NoBEPermohonan'));
				$crud->callback_delete(array($this,'_deletepengajuanfinv'));
				$crud->set_lang_string('delete_error_message', 'Data tidak dapat dihapus karena telah diterima oleh FINANCE.');	
			}
			else
			if ($initdept=='FIINV' )
			{
				$crud->callback_field('KodeVendor',array($this,'add_field_callback_BlockKodeVendor'));
				$crud->callback_field('TglTerimaHRGAPermohonan',array($this,'add_field_callback_TglTerimaHRGAPermohonan'));	
				$crud->set_rules('TglBayarFinancePermohonan','TglBayarFinancePermohonan','callback_validasiTanggal');		
			}
			else
			{
				$crud->callback_field('KodeVendor',array($this,'add_field_callback_BlockKodeVendor'));
				$crud->callback_field('TglTerimaFinancePermohonan',array($this,'add_field_callback_TglTerimaFinancePermohonan'));
				$crud->callback_field('TglBayarFinancePermohonan',array($this,'add_field_callback_TglBayarFinancePermohonan'));
				$crud->callback_field('NoBEPermohonan',array($this,'add_field_callback_NoBEPermohonan'));
				$crud->callback_field('TglTerimaHRGAPermohonan',array($this,'add_field_callback_TglTerimaHRGAPermohonan'));
				$crud->callback_delete(array($this,'_deletepengajuan'));
				$crud->set_lang_string('delete_error_message', 'Data tidak dapat dihapus karena telah diterima oleh HRGA.');	
			}
			

			//$crud->unset_delete();	
			$crud->unset_add();
			$crud->unset_read();
			$crud->unset_print();	
			//$crud->unset_export();
		}


		$output = $crud->render();
   
        $this-> _outputview_listpengajuanuser($output);	
    }
 
    function _outputview_listpengajuanuser($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('UangMuka/ListPengajuanUserTable_view',$data);
		    
	}
	function add_field_callback_KodeVendor($value='', $primary_key=null)
	{
			$url = $this->config->item('urlWebService');
			$client = new SoapClient($url);

			$result = $client->GetAllVendor();
			$result = ($result->GetAllVendorResult);
			$result_array =json_decode($result, true);

			$js1 = '';
			$selected = '';
			for($i=0;$i<count($result_array);$i++){
				if($value==$result_array[$i]['Kode']) {
					$selected = 'selected';
				}else{
					$selected ='';
				}
				$js1 .= '<option '.$selected.' value='.$result_array[$i]['Kode'].' >'.$result_array[$i]['Kode'].' - '.$result_array[$i]['Deskripsi'].'</option>';
			}
			echo $value;
		return '<select id="KodeVendor" name="KodeVendor" class="form-control select2">
		'.$js1.'
		</select>';

	}
	function _deletepengajuan($primary_key){
		$post_array['LastUpdateOn'] = date('Y-m-d H:i:s');
		$post_array['LastUpdateBy'] = $this->npkLogin;
		$post_array['Deleted'] = '1';
		$rowData = $this->db->where('id', $primary_key)->get('trxuangmuka')->row();
		$TglTerimaHRGAPermohonan = $rowData->TglTerimaHRGAPermohonan;
		if($TglTerimaHRGAPermohonan==''){
		$returnvalue = $this->db->update('trxuangmuka',$post_array,array('id' => $primary_key));	
		}
		if($returnvalue){
			return true;
		}else{
			return false;
		}
		
	}
	function _deletepengajuanfinv($primary_key){
		$post_array['LastUpdateOn'] = date('Y-m-d H:i:s');
		$post_array['LastUpdateBy'] = $this->npkLogin;
		$post_array['Deleted'] = '1';
		$rowData = $this->db->where('id', $primary_key)->get('trxuangmuka')->row();
		$TglTerimaFinancePermohonan = $rowData->TglTerimaFinancePermohonan;
		if($TglTerimaFinancePermohonan==''){
		$returnvalue = $this->db->update('trxuangmuka',$post_array,array('id' => $primary_key));	
			}
			if($returnvalue){
				return true;
			}else{
				return false;
			}
		
	}
	function _deleterealisasi($primary_key){
		$post_array['LastUpdateOn'] = date('Y-m-d H:i:s');
		$post_array['LastUpdateBy'] = $this->npkLogin;
		$post_array['Deleted'] = '1';
		$rowData = $this->db->where('id', $primary_key)->get('trxuangmuka')->row();
		$TglTerimaHRGARealisasi = $rowData->TglTerimaHRGARealisasi;
		if($TglTerimaHRGARealisasi==''){
		$returnvalue = $this->db->update('trxuangmuka',$post_array,array('id' => $primary_key));	
			}
			if($returnvalue){
				return true;
			}else{
				return false;
			}
		
	}
	function _deleterealisasifinv($primary_key){
		$post_array['LastUpdateOn'] = date('Y-m-d H:i:s');
		$post_array['LastUpdateBy'] = $this->npkLogin;
		$post_array['Deleted'] = '1';
		$rowData = $this->db->where('id', $primary_key)->get('trxuangmuka')->row();
		$TglTerimaFinanceRealisasi = $rowData->TglTerimaFinanceRealisasi;
		if($TglTerimaFinanceRealisasi==''){
		$returnvalue = $this->db->update('trxuangmuka',$post_array,array('id' => $primary_key));	
			}
			if($returnvalue){
				return true;
			}else{
				return false;
			}
		
	}
	function add_field_callback_BlockKodeVendor($value = '', $primary_key = null)
	{		
		return '<label name="KodeVendor">'.$value.'</label>';
	}
	function add_field_callback_KeteranganPermohonan($value = '', $primary_key = null)
	{		
		return '<label name="KeteranganPermohonan">'.$value.'</label>';
	}

	function add_field_callback_TanggalPermohonan($value = '', $primary_key = null)
	{		
		return '<label name="TanggalPermohonan">'.$value.'</label>';
	}

	function add_field_callback_id($value = '', $primary_key = null)
	{		
		return '<input type="text" readonly id="idPermohonan" name="idPermohonan" value="'.$value.'">';
	}

	function add_field_callback_NoUangMuka($value = '', $primary_key = null)
	{		
		return '<label name="NoUangMukaPermohonan">'.$value.'</label>';
	}

	function add_field_callback_TglTerimaFinancePermohonan($value = '', $primary_key = null)
	{		
		return '<label name="TglTerimaFinancePermohonan">'.$value.'</label>';
	}
	function add_field_callback_TglBayarFinancePermohonan($value = '', $primary_key = null)
	{		
		return '<label name="TglBayarFinancePermohonan">'.$value.'</label>';
	}
	function add_field_callback_NoBEPermohonan($value = '', $primary_key = null)
	{		
		return '<label name="NoBEPermohonan">'.$value.'</label>';
	}
	function add_field_callback_TglTerimaHRGAPermohonan($value = '', $primary_key = null)
	{		
		return '<label name="TglTerimaHRGAPermohonan">'.$value.'</label>';
	}

	//realisasi
	function add_field_callback_TglTerimaFinanceRealisasi($value = '', $primary_key = null)
	{		
		return '<label name="TglTerimaFinanceRealisasi">'.$value.'</label>';
	}
	function add_field_callback_TglBayarFinanceRealisasi($value = '', $primary_key = null)
	{		
		return '<label name="TglBayarFinanceRealisasi">'.$value.'</label>';
	}
	function add_field_callback_NoBERealisasi($value = '', $primary_key = null)
	{		
		return '<label name="NoBERealisasi">'.$value.'</label>';
	}
	function add_field_callback_TglTerimaHRGARealisasi($value = '', $primary_key = null)
	{		
		return '<label name="TglTerimaHRGARealisasi">'.$value.'</label>';
	}
	function add_field_callback_TanggalRealisasi($value = '', $primary_key = null)
	{		
		return '<label name="TanggalRealisasi">'.$value.'</label>';
	}

	//cek tanggal bayar finance harus >= tanggal bayar tercepat
	function validasiTanggal($value){
		try
		{
			$id = $this->input->post('idPermohonan');
			$dataUangMuka=$this->uangmuka_model->getHeaderTrxUangMukabyNoUMEdit($id);

			if($dataUangMuka != null)
			{
				foreach($dataUangMuka as $dtRw)
				{
					if($value <= $dtRw->WaktuBayarTercepat){
						$this->form_validation->set_message('validasiTanggal', "Tanggal minimal yang bisa Anda masukkan adalah ". $dtRw->WaktuBayarTercepat. " (Waktu Pembayaran Tercepat)" );
						return false;
					}					
				}
			}
			return true;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}

	
	
	public function search_realisasi_user($Bulan='', $Tahun='', $PilihanTglAwal ='',$PilihanTglAkhir ='',$Status='')
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Uang Muka"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_listrealisasi( $Bulan, $Tahun,$PilihanTglAwal,$PilihanTglAkhir,'user',$Status);
			}
		}else{
			redirect('login','refresh');
		}
	}
	
	

	public function _listrealisasi( $Bulan='',$Tahun='',$PilihanTglAwal ='',$PilihanTglAkhir ='',$JenisUser='',$Status='')
    {
		$crud = new grocery_crud();
		$crud->set_subject('List Realisasi User');
		
		
		$crud->set_model('custom_query_model');
		$crud->set_table('trxuangmuka'); //Change to your table name

		
		$begin =$PilihanTglAwal;
		$end = $PilihanTglAkhir;


		
		$session_data = $this->session->userdata('logged_in');
		$npkLogin = $session_data['npk'];

		if ($JenisUser=='user')
		{
			$praquery1=  "tum.TanggalRealisasi between '".$begin."' and '".$end."' and tum.CreatedBy='".$npkLogin."'";
		}
	else
		{
			$praquery1=  "tum.TanggalRealisasi between '".$begin."' and '".$end."' ";
		}
		
		switch($Status)
		{
			case 1: $praquery1 .= " and tum.TglTerimaHRGARealisasi is null "; break;
			case 2: $praquery1 .= " and tum.TglTerimaFinanceRealisasi is null "; break;
			case 3: $praquery1 .= " and tum.TglBayarFinanceRealisasi is null "; break;
			case 4: $praquery1 .= " and tum.NoBERealisasi is null "; break;
		}

		$sqlquery = "select tum.id,tum.NoUangMuka,tum.NoSPD,tum.KeteranganPermohonan,JenisKegiatanUangMuka ,mj.JenisKegiatan, tum.TanggalPermohonan, 
							tum.WaktuPenyelesaianTerlambat, tum.WaktuBayarTercepat,tum.TglTerimaHRGARealisasi,tum.TglTerimaFinanceRealisasi, tum.KodeVendor,
							tum.TglBayarFinanceRealisasi,tum.TanggalRealisasi,tum.ReasonOutstanding,tum.LebihKurangRealisasi, tum.status,
							case when tum.NoSPD<>'' then 'UM Perjalanan Dinas' else
							case when NoAkomodasiTiket<>'' then 'UM Akomodasi Tiket' else
							'UM Lainnya' end end as TipeUangMuka,
							case when length(tum.NoSPD) > 0 then format(spd.TotalSPD,0) else format(TotalPermohonan,0) end as TotalPermohonan,
					case when length(tum.NoSPD) > 0 then format(spd.TotalLap,0) else format(TotalRealisasi,0) end as totalrealisasi, 
					tum.FilePathRealisasi,
							tum.NoBERealisasi
					from trxuangmuka tum join mstrjnskegiatanum mj on mj.IdKegiatan=tum.JenisKegiatanUangMuka
					left join (select NoUangMuka, sum(ifnull(TotalPermohonan,0)) as TotalPermohonan,sum(ifnull(TotalRealisasi,0)) as TotalRealisasi from dtluangmuka where deleted = 0 group by NoUangMuka) dtl on dtl.NoUangMuka = tum.NoUangMuka
					left join (select * from trkspd where deleted = 0) spd on spd.NoSPD = tum.NoSPD
					where ".$praquery1." and tum.Deleted = 0
					order by tum.NoUangMuka
					  ";	
	
		
		$crud->basic_model->set_query_str($sqlquery); //Query text here

		$crud->columns('NoUangMuka','KeteranganPermohonan','TanggalRealisasi','TipeUangMuka','JenisKegiatan','TotalPermohonan','WaktuPenyelesaianTerlambat','KodeVendor','TglTerimaHRGARealisasi','TglTerimaFinanceRealisasi','TglBayarFinanceRealisasi','totalrealisasi','status','NoBERealisasi');
		$crud->fields('id','NoUangMuka','TanggalRealisasi','KodeVendor','TglTerimaHRGARealisasi','TglTerimaFinanceRealisasi','TglBayarFinanceRealisasi','NoBERealisasi');
		
		$crud->display_as('NoUangMuka','No Uang Muka'); $crud->display_as('KeteranganPermohonan','Keterangan');$crud->display_as('TanggalRealisasi','Tgl Realisasi');$crud->display_as('TipeUangMuka','Tipe');$crud->display_as('TotalPermohonan','Total Permohonan');$crud->display_as('totalrealisasi','Total Realisasi');
		$crud->display_as('WaktuPenyelesaianTerlambat','Waktu Penyelesaian');$crud->display_as('TglTerimaHRGARealisasi','Tgl Terima HRGA');$crud->display_as('TglTerimaFinanceRealisasi','Tgl Terima Finance');$crud->display_as('TglBayarFinanceRealisasi','Tgl Bayar Finance');
		$crud->display_as('NoBERealisasi','No BE Realisasi');
		$crud->display_as('KodeVendor','Kode Vendor');
		
		
	

		if ($JenisUser=='user')
		{
			
					
			//$crud->add_action('Upload/Lihat Dokumen Realisasi', base_url('/assets/images/upload.png'), '','',array($this,'callback_action_upload'));
			$crud->add_action('Edit/Lihat Realisasi', base_url('/assets/images/edit.png'), '','',array($this,'callback_action_edit_realisasi'));	
				
			$crud->callback_delete(array($this,'_deleterealisasi'));
			$crud->set_lang_string('delete_error_message', 'Data tidak dapat dihapus karena telah diterima oleh HRGA.');
			//$crud->unset_delete();
			$crud->unset_edit();
			$crud->unset_add();
			$crud->unset_read();
			$crud->unset_print();	
		//	$crud->unset_export();
		}
		else //admin
		{
					
			//$crud->add_action('Upload/Lihat Dokumen Permohonan', base_url('/assets/images/upload.png'), '','',array($this,'callback_action_upload'));
				
			//$crud->callback_field('KeteranganRealisasi',array($this,'add_field_callback_KeteranganPermohonan'));
			$crud->callback_field('TanggalRealisasi',array($this,'add_field_callback_TanggalRealisasi'));
			//$crud->callback_field('TotalPermohonan',array($this,'add_field_callback_TotalPermohonan'));
			$crud->callback_field('id',array($this,'add_field_callback_id'));
			$crud->callback_field('NoUangMuka',array($this,'add_field_callback_NoUangMuka'));
			$crud->add_action('Lihat Realisasi', base_url('/assets/images/read.png'), '','',array($this,'callback_action_lihat_realisasi'));	

			$initdept =	$this->uangmuka_model->getInitDept($npkLogin);
			if ($initdept=='HRGA' )
			{
				$crud->callback_field('KodeVendor',array($this,'add_field_callback_KodeVendor'));
				$crud->callback_field('TglTerimaFinanceRealisasi',array($this,'add_field_callback_TglTerimaFinanceRealisasi'));
				$crud->callback_field('TglBayarFinanceRealisasi',array($this,'add_field_callback_TglBayarFinanceRealisasi'));
				$crud->callback_field('NoBERealisasi',array($this,'add_field_callback_NoBERealisasi'));			
				$crud->callback_delete(array($this,'_deleterealisasifinv'));
				$crud->set_lang_string('delete_error_message', 'Data tidak dapat dihapus karena telah diterima oleh FINANCE.');	
			}
			else
			if ($initdept=='FIINV' )
			{
				$crud->callback_field('KodeVendor',array($this,'add_field_callback_BlockKodeVendor'));
				$crud->callback_field('TglTerimaHRGARealisasi',array($this,'add_field_callback_TglTerimaHRGARealisasi'));	
			}
			else
			{
				$crud->callback_field('KodeVendor',array($this,'add_field_callback_BlockKodeVendor'));
				$crud->callback_field('TglTerimaFinanceRealisasi',array($this,'add_field_callback_TglTerimaFinanceRealisasi'));
				$crud->callback_field('TglBayarFinanceRealisasi',array($this,'add_field_callback_TglBayarFinanceRealisasi'));
				$crud->callback_field('NoBERealisasi',array($this,'add_field_callback_NoBERealisasi'));
				$crud->callback_field('TglTerimaHRGARealisasi',array($this,'add_field_callback_TglTerimaHRGARealisasi'));
				$crud->callback_delete(array($this,'_deleterealisasi'));
				$crud->set_lang_string('delete_error_message', 'Data tidak dapat dihapus karena telah diterima oleh HRGA.');
			}			
			//$crud->unset_delete();	
			$crud->unset_add();
			$crud->unset_read();
			$crud->unset_print();	
		//	$crud->unset_export();
		}

		
		
		$output = $crud->render();
   
        $this-> _outputview_listrealisasi($output);	
	}
	
	function callback_action_upload($primary_key,$row)
	{	
		$id = $primary_key;				
		return "javascript:window.open('" . site_url('UangMuka/UangMukaController/Upload/') . '/' . $id . "','_parent')";
		
	}

	function callback_action_lihat($primary_key,$row)
	{	
		$id = $primary_key;	
		$NoUangMuka = $row->NoUangMuka;
		$TipeUangMuka =$row->TipeUangMuka;
		$noSPD = $row->NoSPD;		

		if ($TipeUangMuka <> "UM Perjalanan Dinas"){
			return "javascript:window.open('" . site_url('UangMuka/cetakFormUangMuka?NoUangMuka=') . 	$NoUangMuka. "','_parent')";
		}else{
			return "javascript:window.open('" . site_url('cetakFormSPDnew?NoSPD=') . $noSPD. "','_parent')";
		}
	}

	function callback_action_edit($primary_key,$row)
	{	
		$id = $primary_key;	
		$NoUangMuka = $row->NoUangMuka;
		$TipeUangMuka =$row->TipeUangMuka;
		$noSPD = $row->NoSPD;		

		if ($TipeUangMuka <> "UM Perjalanan Dinas"){
			return "javascript:window.open('" . site_url('UangMuka/UangMukaController/Edit/') . '/' . $id . "','_parent')";
		}else{
			return "javascript:window.open('" . site_url('EditRekapSPDnew?NoSPD=') . $noSPD. "','_parent')";
		}
	}

	function callback_action_lihat_realisasi($primary_key,$row)
	{	
		$id = $primary_key;	
		$NoUangMuka = $row->NoUangMuka;
		$TipeUangMuka =$row->TipeUangMuka;
		$noSPD = $row->NoSPD;		

		if ($TipeUangMuka <> "UM Perjalanan Dinas"){
			return "javascript:window.open('" . site_url('UangMuka/cetakFormUangMukaRealisasi?NoUangMuka=') . $NoUangMuka. "','_parent')";
		}else{
			return "javascript:window.open('" . site_url('cetakLaporanSPD?NoSPD=') . $noSPD. "','_parent')";
		}
	}

	function callback_action_edit_realisasi($primary_key,$row)
	{	
		$id = $primary_key;	
	
			return "javascript:window.open('" . site_url('UangMuka/UangMukaController/Realisasi/') . '/' . $id . "/edit','_parent')";

	}

	function callback_action_realisasi($primary_key,$row)
	{	
		$id = $primary_key;
		$NoUangMuka = $row->NoUangMuka;
		$TipeUangMuka =$row->TipeUangMuka;
		$noSPD = $row->NoSPD;

		if ($TipeUangMuka <> "UM Perjalanan Dinas"){
		return "javascript:window.open('" . site_url('UangMuka/UangMukaController/Realisasi/') . '/' . $id . "/create','_parent')";
	}else{
		return "javascript:window.open('" . site_url('laporanSPD?cmbNoSPD=') . $noSPD. ('&submitFormSPD=OK') . "','_parent')";
	}
	}
 
    function _outputview_listrealisasi($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('UangMuka/ListRealisasiUserTable_view',$data);
		    
    }
	
	// admin
	public function search_pengajuan_admin($Bulan='', $Tahun='', $PilihanTglAwal ='',$PilihanTglAkhir ='',$Status='')
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Uang Muka") || check_authorizedByName("Uang Muka Admin"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_listpengajuan( $Bulan, $Tahun,$PilihanTglAwal,$PilihanTglAkhir,'admin',$Status);
			}
		}else{
			redirect('login','refresh');
		}
	}
	
	

	
 
    function _outputview_listpengajuanadmin($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('UangMuka/ListPengajuanUserTable_view',$data);
		    
	}

	
	
	public function search_realisasi_admin($Bulan='', $Tahun='', $PilihanTglAwal ='',$PilihanTglAkhir ='',$Status='')
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Uang Muka"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_listrealisasi( $Bulan, $Tahun,$PilihanTglAwal,$PilihanTglAkhir,'admin',$Status);
			}
		}else{
			redirect('login','refresh');
		}
	}
	
	

    function _outputview_listrealisasiadmin($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('UangMuka/ListRealisasiUserTable_view',$data);
		    
	}
	
	public function search_akomodasi_tiket_admin($Bulan='', $Tahun='', $PilihanTglAwal ='',$PilihanTglAkhir ='')
	{
		
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Uang Muka Admin"))
			{
				$this->npkLogin = $session_data['npk'];
				$NPKGA = $this->akomodasitiket_model->getGlobalParam('AdminGA');
				if ($this->npkLogin==$NPKGA )
					$JenisUser='GA';
				else
					$JenisUser='NONGA';
				
				$this->_listakomodasitiket( $Bulan, $Tahun,$PilihanTglAwal,$PilihanTglAkhir,$JenisUser);
				
			}
		}else{
			redirect('login','refresh');
		}
	}
	
	

	public function _listakomodasitiket( $Bulan='',$Tahun='',$PilihanTglAwal ='',$PilihanTglAkhir ='',$JenisUser='')
	{
		$crud = new grocery_crud();
		$crud->set_subject('List Akomodasi Tiket');
		$crud->set_theme('flexigrid');
		
		
		$crud->set_model('custom_query_model');
		$crud->set_table('akomodasitiket'); //Change to your table name

		
		
		$begin =$PilihanTglAwal;
		$end = $PilihanTglAkhir;


		
		$session_data = $this->session->userdata('logged_in');
		$npkLogin = $session_data['npk'];

		

		if ($JenisUser=='GA' )
			$praquery1=  "a.deleted = 0  and a.TanggalBerangkat between '".$begin."' and '".$end."' ";	
		else	
			$praquery1=  "a.deleted = 0 and a.CreatedBy='".$npkLogin."'  and a.TanggalBerangkat between '".$begin."' and '".$end."' ";	

		$sqlquery = "select     KodeAkomodasiTiket, NoAkomodasiTiket,   
								 TanggalBerangkat,
								 TanggalKembali,
								 TanggalCheckOut,
								 TanggalCheckIn,
								 TanggalBerangkatPesawat,
								 TanggalKepulanganPesawat,
							Tujuan, Alasan, a.NPKAtasan, NamaHotel, JamCheckIn,  JamCheckOut, TiketPesawat,  
								JamBerangkatPesawat,  JamKepulanganPesawat, a.CreatedOn, a.CreatedBy, StatusApprove, a.DPA,
								case when StatusApprove='PE' then 'Pending Approval' else case when StatusApprove='AP' then 'Approve' else  'Decline' end end  as KeteranganStatus,
								AlasanDecline, m.Nama as NamaRequester
						from akomodasitiket a join mstruser m on a.Createdby=m.NPK											
						where ".$praquery1." order by NoAkomodasiTiket";	
	
		
		$crud->basic_model->set_query_str($sqlquery); //Query text here
	
		$crud->columns('NoAkomodasiTiket','NamaRequester','Tujuan','Alasan','TanggalBerangkat','TanggalKembali','KeteranganStatus','AlasanDecline');
		$crud->fields('KodeAkomodasiTiket','NoAkomodasiTiket','NamaRequester','Tujuan','Alasan','TanggalBerangkat','TanggalKembali','KeteranganStatus','AlasanDecline');
		
		$crud->display_as('NoAkomodasiTiket','No Akomodasi Tiket'); $crud->display_as('NamaRequester','Nama Pemohon');
		$crud->display_as('TanggalBerangkat','Tgl Berangkat');$crud->display_as('TanggalKembali','Tgl Kembali');
		$crud->display_as('KeteranganStatus','Status');$crud->display_as('AlasanDecline','Alasan Decline');
		
		
		if ($JenisUser=='GA'){
			$crud->add_action('Create Uang Muka', base_url('/assets/images/desc.gif'), '','',array($this,'callback_action_createUM'));
		}
		
		$crud->unset_delete();
		$crud->unset_edit();
		$crud->unset_add();
		$crud->unset_read();
		$output = $crud->render();
   
		$this-> _outputview_listakomodasitiket($output);	
	}

	function _outputview_listakomodasitiket($output = null)
	{
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->load->view('UangMuka/ListAkomodasiTiket_view',$data);
			
	}

	function callback_action_createUM($primary_key,$row)
	{	
		$kode = $primary_key;	
		$Status = $row->StatusApprove;

		if($Status=='AP')	{
		return "javascript:window.open('" . site_url('UangMuka/UangMukaController/CreateAkomodasiTiket/') . '/' . $kode . "','_parent')";
		}else{
		return "javascript:alert('Permintaan Pembuatan Uang Muka Ditolak. Harap Menghubungi Atasan Pemohon')";
		}
	}
	


}



/* End of file main.php */
/* Location: ./application/controllers/main.php */