<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class FormUMAkomodasiTiket extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model('menu','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('uangmuka_model','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){

				$session_data = $this->session->userdata('logged_in');
				$dataUser = $this->user->dataUser($session_data['npk']);
				$NPKAtasan = $this->uangmuka_model->getAtasan($session_data['npk']);
				$dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url,
						 'KodeMenu' => $row->KodeMenu,
						 'ParentMenu' => $row->ParentMenu,
						 'MenuOrder' => $row->MenuOrder
					   );					   
					}
				}else{
					echo 'gagal';
				}
				
				if($dataUser){
					$dataUser_array = array();
					foreach($dataUser as $dataUser){
						$dataUserDetail = array(
							'title'=> 'Input Form Uang Muka Akomodasi & Tiket',
							//'menu_array' => $menu_array,
							'npk' => $dataUser->npk,
							'golongan' => $dataUser->golongan,
							'nama'=> $dataUser->nama,
							'jabatan'=> $dataUser->jabatan,
							'departemen'=> $dataUser->departemen,
							'atasan'=> $dataUser->atasan,
							'NPKAtasan'=> $dataUser->NPKAtasan			 
						);
					}
				}else{
					echo 'gagaldatauser';
				}

				if($dataOutstanding){
					$dataOutstanding_array = array();
					foreach($dataOutstanding as $dataOutstanding){
						$dataOutstandingDetail[] = array(
							'title'=> 'Input Form Uang Muka Lainnya',
							//'menu_array' => $menu_array,
							'NoUangMuka' => $dataOutstanding->NoUangMuka,
							'KeteranganPermohonan' => $dataOutstanding->KeteranganPermohonan,
							'Total' => $dataOutstanding->Total,
							'TotalSPD' => $dataOutstanding->TotalSPD,
							'ReasonOutstanding'=> $dataOutstanding->ReasonOutStanding									 
						);
					}
				}
				
				
				if($dataOutstanding){
							$data = array(
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'NPKAtasan' => $NPKAtasan,
								'menu_array' => $menu_array	,
								'dataUser' => $dataUserDetail,
								'dataOutstanding' => $dataOutstandingDetail,
								'dataNoPP' => $this->uangmuka_model->getDataPP()	,
								'dataJenisKeterangan' => $this->uangmuka_model->getJenisKegiatanUM()	      
							);
						}else
						{
							$data = array(
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'NPKAtasan' => $NPKAtasan,
								'menu_array' => $menu_array	,
								'dataUser' => $dataUserDetail,
								'dataOutstanding' =>'0',
								'dataNoPP' => $this->uangmuka_model->getDataPP()	,
								'dataJenisKeterangan' => $this->uangmuka_model->getJenisKegiatanUM()	      
							);
						}

				 
				  $this->load->helper(array('form','url'));
				//$this->load->view('home_view', $data);				
				$this->template->load('default','UangMuka/formUMAkomodasiTiket_view',$data);
				
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}

		
		function ajax_loadWaktuJenisKegiatanDPA()
		{
			$idKegiatan = $_POST['idKegiatan'];
			$tglMulai = $_POST['tglMulai'];
			$tglSelesai = $_POST['tglSelesai'];
			
		
			$sqlquery = "select DATE_FORMAT(fnGetWaktuPembayaranTercepat(". $idKegiatan .",'". $tglMulai ."'),'%Y-%m-%d') as WaktuBayarUangMuka,DATE_FORMAT(fnGetWaktuPenyelesianMax(". $idKegiatan .",'".  $tglSelesai ."'),'%Y-%m-%d') as WaktuSelesaiUangMuka ";
		//	echo 	$sqlquery;
			$rs = $this->db->query($sqlquery);
			$result = $rs->result();
			
			array_push($result,'1');
			
			echo json_encode($result);
		}
	}
?>