<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');

class submitUangMukaLain extends CI_Controller {

	 function __construct()
	 {
	   parent::__construct();
	   $this->load->model('trkSPD','',TRUE);
	   $this->load->model('uangmuka_model','',TRUE);
	   $this->load->model('user','',TRUE);
	   $session_data = $this->session->userdata('logged_in');
	 }

	 function index()
	 {
	   //This method will have the credentials validation
	   $this->load->library('form_validation');

	   /*$this->form_validation->set_rules('rbDPA', 'DPA', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtKeterangan', 'Keterangan', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtNoPP', 'No PP', 'trim|xss_clean');	 
	   $this->form_validation->set_rules('txtTanggalMulai', 'Tanggal Mulai', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtTanggalSelesai', 'Tanggal Selesai', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtDisplayPP', 'No PP', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKegiatan', 'Jenis Kegiatan', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtOutstanding', 'Outstanding', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtPilihBayar', 'Tipe Bayar Uang Muka', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtWaktuBayarCepat', 'Waktu Pembayaran Tercepat', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBank', 'Bank', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtWaktuSelesaiUM', 'Waktu Penyelesian Paling Lambat', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtNoRek', 'No Rekening', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtPenerima', 'Penerima', 'trim|xss_clean');*/
	  
	   
	  
	  /* if($this->form_validation->run() == false)
	   {
		 //Field validation failed.  User redirected to login page
		 $this->load->view('UangMuka/formUMLain_view');
	   }
	   else
	   {	 */
		 //Go to private area
		 if($this->input->post('submitUangMukaLain')){
			$DPA = $this->input->post('rbDPA');	
			$NPKPemohon = $this->input->post('txtNPK');		
			$NoAT = $this->input->post('txtNoAT');	
			$noUangMuka = $this->generateNoUangMuka($DPA,$NPKPemohon);			
			if($this->uangmuka_model->insertUangMuka($noUangMuka,$NoAT) && $this->uangmuka_model->InsertDtlUangMuka($noUangMuka) ){
				redirect('UangMuka/cetakFormUangMuka?NoUangMuka='.$noUangMuka,'refresh');				
			}else{
				echo('gagal save');
				}
	/*	 }*/
	   }
	}

	

	function generateNoUangMuka($DPA,$NPKPemohon){
		$lastNo = $this->uangmuka_model->getLastNoUangMuka();
		$initdept =	$this->uangmuka_model->getInitDept($NPKPemohon);	
		$bulanFUMInt =0 ;				
		if ($lastNo != "0")
		{				
			$array = explode("/", $lastNo);					
			$bulanFUMRoman = $array[4];
			$NoUrut = (int) $array[3];
			$bulanFUMInt = $this->uangmuka_model->NumberFromRoman($bulanFUMRoman);
		}


		if($bulanFUMInt != date('m') || $lastNo == "0")
		{
			$NextNo = "001";	
		}
		else
		{
			$NextNo = (int)$NoUrut + 1;
		}

		$noUM = 'UM/DPA'. $DPA.'/'. $initdept.'/'. str_pad($NextNo,3,"0",STR_PAD_LEFT).'/'. $this->uangmuka_model->integerToRoman(date('n')).'/'.date('Y');	
		
		return $noUM;
	}
	

}
?>