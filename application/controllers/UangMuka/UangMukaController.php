<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
session_start();
class UangMukaController extends CI_Controller{
    var $npkLogin;
    function __construct()
    {
        parent::__construct();    
		$this->load->model('menu','',TRUE);
        $this->load->model('user','',TRUE);		
        $this->load->model('uangmuka_model','',TRUE);	
        $this->load->model('akomodasitiket_model','',TRUE);	
        $this->load->helper('date');
        $session_data = $this->session->userdata('logged_in');
        $this->npkLogin = $session_data['npk'];
    }

    public function index()
    {
        try
        {
            
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
        }
    }

    

    public function ViewListUangMukaUser()
    {
        $session_data = $this->session->userdata('logged_in');
        if($this->session->userdata('logged_in')){
            if(check_authorizedByName("Uang Muka"))
            {
                $this->npkLogin = $session_data['npk'];
                $data = array(
                    'title' => 'Uang Muka User'
                );
                $this->load->helper(array('form','url'));
                $this->template->load('default','UangMuka/UangMukaUser_view',$data);            
            }
        }else{
            //redirect('login','refresh');
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }
    
    public function ViewListUangMukaAdmin()
    {
        $session_data = $this->session->userdata('logged_in');
        if($this->session->userdata('logged_in')){
            if(check_authorizedByName("Uang Muka Admin"))
            {
                $this->npkLogin = $session_data['npk'];
                $data = array(
                    'title' => 'Uang Muka Admin'
                );
                $this->load->helper(array('form','url'));
                $this->template->load('default','UangMuka/UangMukaAdmin_view',$data);            
            }
        }else{
            //redirect('login','refresh');
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }

    function Upload($ID = ''){
        try
        {
            if($this->session->userdata('logged_in'))
            {
                $session_data = $this->session->userdata('logged_in');
                $this->npkLogin = $session_data['npk'];
                
                $user = $this->user->dataUser($this->npkLogin);
                if($user[0]->departemen == 'HRGA' || $user[0]->departemen == 'Accounting Tax & Control'){
                    //klo HRGA atau ada data user tsb
                    $sqlSelect =
                        "SELECT * from trxuangmuka where 
                        id = '$ID'
                        ";
                    $data = $this->db->query($sqlSelect);
                    $dataUM = array(
                        'title'=> 'Upload Persetujuan Permohonan Uang Muka',
                        'data' => $data->result()
                    );
                    $this->load->helper(array('form','url'));
                    $this->template->load('default','UangMuka/UploadUangMuka_view', $dataUM);
                }else{
                    $sqlSelect =
                        "SELECT * from trxuangmuka where 
                        CreatedBy = '$this->npkLogin'
                        and id = '$ID'
                        ";
                    $data = $this->db->query($sqlSelect);
                    if($data->num_rows() != 0){
                        $dataUM = array(
                            'title'=> 'Upload Persetujuan Permohonan Uang Muka',
                            'data' => $data->result()
                        );
                        $this->load->helper(array('form','url'));
                        $this->template->load('default','UangMuka/UploadUangMuka_view', $dataUM);
                    }else{
                        redirect("UangMuka/UangMukaController/ViewListUangMukaUser",'refresh');
                    }
                }
            }
            else
            {
                redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
            }
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            throw new Exception( 'Something really gone wrong', 0, $e);
        }
    }

    function UploadUM($ID = '',$Status=''){
        $config['upload_path']          = './assets/uploads/files/UangMuka/';
        $config['allowed_types']        = 'gif|jpg|png|pdf';
        $config['max_size']             = 1000000000;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('uploadScanUM'))
        {
            $error = array('error' => $this->upload->display_errors());
            $session_data = $this->session->userdata('logged_in');
            $this->npkLogin = $session_data['npk'];
            $sqlSelect =
            "SELECT * from trxuangmuka where 
            CreatedBy = '$this->npkLogin'
            and id = '$ID'
            ";
            $data = $this->db->query($sqlSelect);
            $user = $this->user->dataUser($this->npkLogin);
            if($user[0]->departemen == 'HRGA' || $user[0]->departemen == 'Accounting Tax & Control' || $data->num_rows() != 0){
                //klo HRGA atau ada data user tsb
                $dataUM = array(
                    'title'=> 'Upload Persetujuan Permohonan Uang Muka',
                    'data' => $data->result(),
                    'error' => $error
                );
                $this->load->helper(array('form','url'));
                $this->template->load('default','UangMuka/UploadUangMuka_view', $dataUM);
            }else{
                redirect("UangMuka/UangMukaController/ViewListUangMukaUser",'refresh');
            }
        
        }
        else
        {
            $filename = $_FILES['uploadScanUM']['name'];
            $data = array('upload_data' => $this->upload->data());
            $filename = $data['upload_data']['file_name'];
            
            if ($Status=='P')            
            {    
                $sqlUpdate = "UPDATE trxuangmuka SET FilePathPermohonan = '$filename' where ID = $ID ";
               // $this->template->load('default','UangMuka/UploadSuccessPermohonan', $data);
              echo '<script type="text/javascript">';
              echo 'alert("Your File Was Successfully Uploaded");';
              echo "window.location = '" . $this->config->base_url() . "index.php/UangMuka/UangMukaController/ViewListUangMukaUser/';";
              echo '</script>';
              //redirect("UangMuka/UangMukaController/ViewListUangMukaUser");
            }
            else
            {
                $sqlUpdate = "UPDATE trxuangmuka SET FilePathRealisasi = '$filename' where ID = $ID ";
                echo '<script type="text/javascript">';
                echo 'alert("Your File Was Successfully Uploaded");';
                echo "window.location = '" . $this->config->base_url() . "index.php/UangMuka/UangMukaController/ViewListUangMukaUser/';";
                echo '</script>';
              //  $this->template->load('default','UangMuka/UploadSuccessRealisasi', $data);
              //redirect("UangMuka/UangMukaController/ViewListUangMukaUser");
            }

            $this->db->query($sqlUpdate);
            
           
        }
    }

    function Edit($ID = ''){
        try
        {
            if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');			
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
                }
                
                if ($ID=='submitUpdateUMLain')
                {
                    if($this->input->post('submitUpdateUMLain')){
                        if ($this->input->post('txtMode')=='edit') 
                        {
                            $noUM = $this->input->post('txtNoUM');
                            $this->uangmuka_model->EditDtlUangMuka($noUM);
                            if($this->uangmuka_model->updateUMLain($noUM)){
                                redirect('UangMuka/cetakFormUangMuka?NoUangMuka='.$noUM,'refresh');				
                            }else{
                                echo 'gagal Update ';
                            }
                        }
                        else
                        if ($this->input->post('txtMode')=='inputreason') 
                        {
                            $noUM = $this->input->post('txtNoUM');
                            if( $this->uangmuka_model->updateReasonByNoUM($noUM) ){
                                redirect('UangMuka/UangMukaController/ViewListUangMukaUser');
                            }
                        }
                     }                     
                     
                  }
                
                else
                {
				
                    $noUangMuka = $this->uangmuka_model->getNoUangMukaByID($ID);                
                    
                    if($noUangMuka){
                            $dataUser = $this->user->dataUser($session_data['npk']);
                            $trkUangMuka = $this->uangmuka_model->getHeaderTrxUangMukabyNoUMEdit($ID);

                            $NPKAtasan = $this->uangmuka_model->getAtasan($session_data['npk']);
                            $dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
            
                            if($dataOutstanding){
                                $dataOutstanding_array = array();
                                foreach($dataOutstanding as $dataOutstanding){
                                    $dataOutstandingDetail[] = array(
                                        'title'=> 'Input Form Uang Muka Lainnya',
                                        //'menu_array' => $menu_array,
                                        'NoUangMuka' => $dataOutstanding->NoUangMuka,
                                        'KeteranganPermohonan' => $dataOutstanding->KeteranganPermohonan,
                                        'Total' => $dataOutstanding->Total,
                                        'TotalSPD' => $dataOutstanding->TotalSPD,
                                        'ReasonOutstanding'=> $dataOutstanding->ReasonOutStanding									 
                                    );
                                }
                            }
                            
            
                            if($trkUangMuka){
                                $trkUangMuka_array = array();
                                foreach($trkUangMuka as $row){
                                    $trkUangMuka_array[] = array(
                                        'DPA' => $row->DPA,
                                        'NoUangMuka' => $row->NoUangMuka,
                                        'TanggalPermohonan' => $row->TanggalPermohonan,
                                        'NoPP' => $row->NoPP,
                                        'KeteranganPermohonan' => $row->KeteranganPermohonan,
                                        'Status' => $row->Status,
                                        'TipeBayarUangMuka'=> $row->TipeBayarUangMuka,
                                        'BankBayarUangMuka'=> $row->BankBayarUangMuka,
                                        'NoRekBayarUangMuka'=> $row->NoRekBayarUangMuka,
                                        'NmPenerimaBayarUangMuka'=> $row->NmPenerimaBayarUangMuka,
                                        'AlasanOutstanding'=> $row->AlasanOutstanding,
                                        'NoSPD'=> $row->NoSPD,
                                        'NoAkomodasiTiket'=> $row->NoAkomodasiTiket,
                                        'TglTerimaHRGAPermohonan'=> $row->TglTerimaHRGAPermohonan,
                                        'TglTerimaFinancePermohonan'=> $row->TglTerimaFinancePermohonan,
                                        'TglBayarFinancePermohonan'=> $row->TglBayarFinancePermohonan,
                                        'TglTerimaHRGARealisasi'=> $row->TglTerimaHRGARealisasi,
                                        'TglTerimaFinanceRealisasi'=> $row->TglTerimaFinanceRealisasi,
                                        'TglBayarFinanceRealisasi' => $row->TglBayarFinanceRealisasi,
                                        'NoBEPermohonan' => $row->NoBEPermohonan,
                                        'NoBERealisasi' => $row->NoBERealisasi,
                                        'TipeKembaliUangMuka' => $row->TipeKembaliUangMuka,
                                        'BankKembaliUangMuka' => $row->BankKembaliUangMuka,
                                        'NoRekKembaliUangMuka' => $row->NoRekKembaliUangMuka,
                                        'NmPenerimaUangMuka' => $row->NmPenerimaUangMuka,
                                        'LebihKurangRealisasi' => $row->LebihKurangRealisasi,
                                        'CreatedBy' => $row->CreatedBy,
                                        'CreatedOn' => $row->CreatedOn,
                                        'JenisKegiatanUangMuka' => $row->JenisKegiatanUangMuka,
                                        'NPKPemohon' => $row->NPKPemohon,
                                        'ReasonOutstanding' => $row->ReasonOutstanding,
                                        'TanggalMulai' => $row->TanggalMulai,
                                        'TanggalSelesai' => $row->TanggalSelesai,
                                        'WaktuBayarTercepat' => $row->WaktuBayarTercepat,
                                        'WaktuPenyelesaianTerlambat' => $row->WaktuPenyelesaianTerlambat,
                                        'TanggalRealisasi' => $row->TanggalRealisasi,
                                        'FilePathPermohonan' => $row->FilePathPermohonan,
                                        'FilePathRealisasi' => $row->FilePathRealisasi,
                                        'id' => $row->id,
                                        'NamaCreatedBy' => $row->nama,
                                        'NamaPemohon' => $row->atasan,
                                        'NamaJenisKegiatan' =>  $row->JenisKegiatan

                                    );
                                }
                            }else{
                                echo 'gagal111';
                            }
                            /*
                            $data = array(
                                'npk' => $session_data['npk'],
                                'nama' => $session_data['nama'],
                                'menu_array' => $menu_array
                            );
                            */
                            if($dataUser){
                                $dataUser_array = array();
                                foreach($dataUser as $dataUser){
                                    $dataUserDetail = array(
                                        'title'=> 'Input Form SPD',
                                        'menu_array' => $menu_array,
                                        'npk' => $dataUser->npk,
                                        'golongan' => $dataUser->golongan,
                                        'nama'=> $dataUser->nama,
                                        'jabatan'=> $dataUser->jabatan,
                                        'departemen'=> $dataUser->departemen,
                                        'atasan'=> $dataUser->atasan,
                                        'uangsaku'=> $dataUser->uangsaku,
                                        'uangmakan'=> $dataUser->uangmakan					 
                                    );
                                }
                            }else{
                                echo 'gagal';
                            }
                            
                            if($dataOutstanding){    
                                $data2 = array(
                                    'title' => 'Edit Permohonan Uang Muka',
                                    'menu_array' => $menu_array,
                                    'npk' => $session_data['npk'],
                                    'nama' => $session_data['nama'],
                                    'id' => $ID,
                                    'trkUangMuka_array' => $trkUangMuka_array,
                                    'dataUser' => $dataUserDetail,
                                    'dataNoPP' => $this->uangmuka_model->getDataPP(),
                                    'dataJenisKeterangan' => $this->uangmuka_model->getJenisKegiatanUM(),
                                    'dataOutstanding' => $dataOutstandingDetail	
                                );
                            }else
                            {    
                                $data2 = array(
                                    'title' => 'Edit Permohonan Uang Muka',
                                    'menu_array' => $menu_array,
                                    'npk' => $session_data['npk'],
                                    'nama' => $session_data['nama'],
                                    'id' => $ID,
                                    'trkUangMuka_array' => $trkUangMuka_array,
                                    'dataUser' => $dataUserDetail,
                                    'dataNoPP' => $this->uangmuka_model->getDataPP(),
                                    'dataJenisKeterangan' => $this->uangmuka_model->getJenisKegiatanUM(),
                                    'dataOutstanding' => '0'	
                                );
                            }
                            $this->load->helper(array('form','url'));
                            //$this->load->view('home_view', $data);
                            $this->template->load('default','UangMuka/edit_pengajuanUM_view',$data2);
                        
                    }
                }
                
             
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            throw new Exception( 'Something really gone wrong', 0, $e);
        }
    }

    function Realisasi($ID = '',$action=''){
        try
        {
            if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');			
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
                }
                
                if ($action=='submitRealisasiUMLain')
                {
                    if($this->input->post('submitRealisasiUMLain')){
                        $noUM = $this->input->post('txtNoUM');
                      if($this->uangmuka_model->RealisasiUM($noUM) ){
                            redirect('UangMuka/cetakFormUangMukaRealisasi?NoUangMuka='.$noUM,'refresh');				
                     }else{
                         echo 'gagal Update ';
                     }
                     }
                }
                else
                {
				
                    $noUangMuka = $this->uangmuka_model->getNoUangMukaByID($ID);                
                    
                    if($noUangMuka){
                            $dataUser = $this->user->dataUser($session_data['npk']);
                            $trkUangMuka = $this->uangmuka_model->getHeaderTrxUangMukabyNoUMRealisasi($ID);

                            $NPKAtasan = $this->uangmuka_model->getAtasan($session_data['npk']);
                                                        
            
                            if($trkUangMuka){
                                $trkUangMuka_array = array();
                                foreach($trkUangMuka as $row){
                                    $trkUangMuka_array[] = array(
                                        'DPA' => $row->DPA,
                                        'NoUangMuka' => $row->NoUangMuka,
                                        'TanggalPermohonan' => $row->TanggalPermohonan,
                                        'NoPP' => $row->NoPP,
                                        'KeteranganPermohonan' => $row->KeteranganPermohonan,
                                        'Status' => $row->Status,
                                        'TipeBayarUangMuka'=> $row->TipeBayarUangMuka,
                                        'BankBayarUangMuka'=> $row->BankBayarUangMuka,
                                        'NoRekBayarUangMuka'=> $row->NoRekBayarUangMuka,
                                        'NmPenerimaBayarUangMuka'=> $row->NmPenerimaBayarUangMuka,
                                        'AlasanOutstanding'=> $row->AlasanOutstanding,
                                        'NoSPD'=> $row->NoSPD,
                                        'NoAkomodasiTiket'=> $row->NoAkomodasiTiket,
                                        'TglTerimaHRGAPermohonan'=> $row->TglTerimaHRGAPermohonan,
                                        'TglTerimaFinancePermohonan'=> $row->TglTerimaFinancePermohonan,
                                        'TglBayarFinancePermohonan'=> $row->TglBayarFinancePermohonan,
                                        'TglTerimaHRGARealisasi'=> $row->TglTerimaHRGARealisasi,
                                        'TglTerimaFinanceRealisasi'=> $row->TglTerimaFinanceRealisasi,
                                        'TglBayarFinanceRealisasi' => $row->TglBayarFinanceRealisasi,
                                        'NoBEPermohonan' => $row->NoBEPermohonan,
                                        'NoBERealisasi' => $row->NoBERealisasi,
                                        'TipeKembaliUangMuka' => $row->TipeKembaliUangMuka,
                                        'BankKembaliUangMuka' => $row->BankKembaliUangMuka,
                                        'NoRekKembaliUangMuka' => $row->NoRekKembaliUangMuka,
                                        'NmPenerimaUangMuka' => $row->NmPenerimaUangMuka,
                                        'LebihKurangRealisasi' => $row->LebihKurangRealisasi,
                                        'CreatedBy' => $row->CreatedBy,
                                        'CreatedOn' => $row->CreatedOn,
                                        'JenisKegiatanUangMuka' => $row->JenisKegiatanUangMuka,
                                        'NPKPemohon' => $row->NPKPemohon,
                                        'ReasonOutstanding' => $row->ReasonOutstanding,
                                        'TanggalMulai' => $row->TanggalMulai,
                                        'TanggalSelesai' => $row->TanggalSelesai,
                                        'WaktuBayarTercepat' => $row->WaktuBayarTercepat,
                                        'WaktuPenyelesaianTerlambat' => $row->WaktuPenyelesaianTerlambat,
                                        'TanggalRealisasi' => $row->TanggalRealisasi,
                                        'FilePathPermohonan' => $row->FilePathPermohonan,
                                        'FilePathRealisasi' => $row->FilePathRealisasi,
                                        'id' => $row->id,
                                        'NamaCreatedBy' => $row->nama,
                                        'NamaPemohon' => $row->atasan,
                                        'NamaJenisKegiatan' =>  $row->JenisKegiatan,
                                        'TotalPermohonan' => $row->TotalPermohonan,
                                        'TotalRealisasi' => $row->TotalRealisasi,
                                        'Selisih' =>  $row->Selisih

                                    );
                                }
                            }else{
                                echo 'gagal111';
                            }
                           
                            if($dataUser){
                                $dataUser_array = array();
                                foreach($dataUser as $dataUser){
                                    $dataUserDetail = array(
                                        'title'=> 'Realisasi Uang Muka',
                                        'menu_array' => $menu_array,
                                        'npk' => $dataUser->npk,
                                        'golongan' => $dataUser->golongan,
                                        'nama'=> $dataUser->nama,
                                        'jabatan'=> $dataUser->jabatan,
                                        'departemen'=> $dataUser->departemen,
                                        'atasan'=> $dataUser->atasan,
                                        'uangsaku'=> $dataUser->uangsaku,
                                        'uangmakan'=> $dataUser->uangmakan,	
                                        'NPKAtasan'=> $dataUser->NPKAtasan				 
                                    );
                                }
                            }else{
                                echo 'gagal';
                            }
                            
                            $dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
                            if($dataOutstanding){
                                $dataOutstanding_array = array();
                                foreach($dataOutstanding as $dataOutstanding){
                                    $dataOutstandingDetail[] = array(
                                        'title'=> 'Input Form Uang Muka Lainnya',
                                        //'menu_array' => $menu_array,
                                        'NoUangMuka' => $dataOutstanding->NoUangMuka,
                                        'KeteranganPermohonan' => $dataOutstanding->KeteranganPermohonan,
                                        'Total' => $dataOutstanding->Total,
                                        'TotalSPD' => $dataOutstanding->TotalSPD,
                                        'ReasonOutstanding'=> $dataOutstanding->ReasonOutStanding,									 
                                    );
                                }
                            }		

                            if($dataOutstanding){
                                $data2 = array(
                                    'title' => 'Realisasi Permohonan Uang Muka',
                                    'menu_array' => $menu_array,
                                    'npk' => $session_data['npk'],
                                    'nama' => $session_data['nama'],
                                    'id' => $ID,
                                    'trkUangMuka_array' => $trkUangMuka_array,
                                    'dataUser' => $dataUserDetail,
                                    'dataNoPP' => $this->uangmuka_model->getDataPP(),
                                    'dataJenisKeterangan' => $this->uangmuka_model->getJenisKegiatanUM(),
                                    'dataOutstanding' => $dataOutstandingDetail,
                                    'action' => $action	
                                );
                            }else{
                                $data2 = array(
                                    'title' => 'Realisasi Permohonan Uang Muka',
                                    'menu_array' => $menu_array,
                                    'npk' => $session_data['npk'],
                                    'nama' => $session_data['nama'],
                                    'id' => $ID,
                                    'trkUangMuka_array' => $trkUangMuka_array,
                                    'dataUser' => $dataUserDetail,
                                    'dataNoPP' => $this->uangmuka_model->getDataPP(),
                                    'dataJenisKeterangan' => $this->uangmuka_model->getJenisKegiatanUM(),
                                    'dataOutstanding' => '0',
                                    'action' => $action	
                                );
                            }
                            
                            $this->load->helper(array('form','url'));
                            //$this->load->view('home_view', $data);
                            $this->template->load('default','UangMuka/realisasiUM_view',$data2);
                        
                    }
                }
                
             
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
        }
        catch(Exception $e)
        {
            log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
            throw new Exception( 'Something really gone wrong', 0, $e);
        }
    }

    function ajax_getTotalRealisasi()
    {
        $ID = $this->input->post('ID');
        echo json_encode($this->uangmuka_model->getTotalRealisasi($ID));
    }

    function CreateAkomodasiTiket($KodeAkomodasiTiket=''){
        if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');
            $menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);				
            if($menu){
                $menu_array = array();
                foreach($menu as $row){
                   $menu_array[] = array(
                     'menuname' => $row->menuname,
                     'url' => $row->url
                   );					   
                }
            }else{
                echo 'gagal';
            }

            $NPKGA = $this->akomodasitiket_model->getGlobalParam('AdminGA');

            $dataUser = $this->user->dataUser($session_data['npk']);
            if($dataUser){
                $dataUser_array = array();
                foreach($dataUser as $dataUser){
                    $dataUserDetail = array(
                        'title'=> 'Create Form Uang Muka Akomodasi & Tiket',
                        //'menu_array' => $menu_array,
                        'npk' => $dataUser->npk,
                        'golongan' => $dataUser->golongan,
                        'nama'=> $dataUser->nama,
                        'jabatan'=> $dataUser->jabatan,
                        'departemen'=> $dataUser->departemen,
                        'atasan'=> $dataUser->atasan,
                        'NPKAtasan'=> $dataUser->NPKAtasan			 
                    );
                }
            }else{
                echo 'gagaldatauser';
            }

            if ($KodeAkomodasiTiket=='submitUangMukaAT')
            {
                if($this->input->post('submitUangMukaAT')){
                    $DPA = $this->input->post('rbDPA');	
                    $NPKPemohon = $this->input->post('txtNPK');		
                    $NoAT = $this->input->post('txtNoAT');	
                    $noUangMuka = $this->generateNoUangMuka($DPA,$NPKPemohon);		
                    if($this->uangmuka_model->insertUangMuka($noUangMuka,$NoAT) && $this->uangmuka_model->InsertDtlUangMuka($noUangMuka) ){
                        redirect('UangMuka/cetakFormUangMuka?NoUangMuka='.$noUangMuka,'refresh');			
                    }else{
                        echo 'gagal Simpan ';
                    }
                 }
            }
            else
            {
            $trxAT = $this->akomodasitiket_model->getTrxATbyKode($KodeAkomodasiTiket);
            if($trxAT){
                $trxAT_array = array();
                foreach($trxAT as $row){
                    $trxAT_array = array(
                        'NoAkomodasiTiket' => $row->NoAkomodasiTiket,
                        'KodeAkomodasiTiket' => $row->KodeAkomodasiTiket,
                        'namapemohon' => $row->namapemohon,
                        'npkpemohon' => $row->npkpemohon,
                        'TanggalBerangkat'=> $row->TanggalBerangkat,                       
                        'TanggalKembali'=> $row->TanggalKembali,
                        'Tujuan'=> $row->Tujuan,
                        'Alasan'=> $row->Alasan,
                        'NPKAtasan'=> $row->NPKAtasan,
                        'NamaHotel'=> $row->NamaHotel,
                        'TanggalCheckIn'=> $row->TanggalCheckIn,
                        'TanggalCheckOut'=> $row->TanggalCheckOut,
                        'TiketPesawat'=> $row->TiketPesawat,
                        'TanggalBerangkatPesawat'=> $row->TanggalBerangkatPesawat,
                        'JamBerangkatPesawat'=> $row->JamBerangkatPesawat,
                        'TanggalKepulanganPesawat'=> $row->TanggalKepulanganPesawat,
                        'JamKepulanganPesawat'=> $row->JamKepulanganPesawat,
                        'DPA'=> $row->DPA,
                        'StatusApprove'=> $row->StatusApprove
                    );
                }
            }else{
                echo 'gagalAT';
            }

            $dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
            if($dataOutstanding){
                $dataOutstanding_array = array();
                foreach($dataOutstanding as $dataOutstanding){
                    $dataOutstandingDetail[] = array(
                        'title'=> 'Input Form Uang Muka Lainnya',
                        //'menu_array' => $menu_array,
                        'NoUangMuka' => $dataOutstanding->NoUangMuka,
                        'KeteranganPermohonan' => $dataOutstanding->KeteranganPermohonan,
                        'Total' => $dataOutstanding->Total,
                        'TotalSPD' => $dataOutstanding->TotalSPD,
                        'ReasonOutstanding'=> $dataOutstanding->ReasonOutStanding									 
                    );
                }
            }

            if($dataOutstanding){
                $data = array(
                    'title' => 'Create Uang Muka',
                    'trxAT_array' => $trxAT_array,
                    'npk' => $session_data['npk'],
                    'nama' => $session_data['nama'],						
                    'menu_array' => $menu_array	,
                    'dataUserDetail' => $dataUserDetail,
                    'dataOutstanding' => $dataOutstandingDetail,
                    'dataNoPP' => $this->uangmuka_model->getDataPP()	,
                    'dataJenisKeterangan' => $this->uangmuka_model->getJenisKegiatanUM(),
                    'NPKGA' => $NPKGA  
                );
            }else
            {
                $data = array(
                    'title' => 'Create Uang Muka',
                    'trxAT_array' => $trxAT_array,
                    'npk' => $session_data['npk'],
                    'nama' => $session_data['nama'],						
                    'menu_array' => $menu_array	,
                    'dataUserDetail' => $dataUserDetail,
                    'dataNoPP' => $this->uangmuka_model->getDataPP()	,
                    'dataJenisKeterangan' => $this->uangmuka_model->getJenisKegiatanUM(),
                    'NPKGA' => $NPKGA ,
                    'dataOutstanding' => 0,     
                );
            }

            $this->load->helper(array('form','url'));
            //$this->load->view('UangMuka/lihatStatusAkomodasiTiket_view',$data);
            $this->template->load('default','UangMuka/lihatStatusAkomodasiTiket_view',$data);
         }
        }else{
            redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
        }
    }

    function generateNoUangMuka($DPA,$NPKPemohon){
		$lastNo = $this->uangmuka_model->getLastNoUangMuka();
		$initdept =	$this->uangmuka_model->getInitDept($NPKPemohon);	
		$bulanFUMInt =0 ;				
		if ($lastNo != "0")
		{				
			$array = explode("/", $lastNo);					
			$bulanFUMRoman = $array[4];
			$NoUrut = (int) $array[3];
			$bulanFUMInt = $this->uangmuka_model->NumberFromRoman($bulanFUMRoman);
		}


		if($bulanFUMInt != date('m') || $lastNo == "0")
		{
			$NextNo = "001";	
		}
		else
		{
			$NextNo = (int)$NoUrut + 1;
		}

		$noUM = 'UM/DPA'. $DPA.'/'. $initdept.'/'. str_pad($NextNo,3,"0",STR_PAD_LEFT).'/'. $this->uangmuka_model->integerToRoman(date('n')).'/'.date('Y');	
		
		return $noUM;
	}
    
    function ajax_KalkulasiTotalDetilUM()
    {
        $idUangMuka = $this->input->post('idUangMuka');
        if($idUangMuka != ""){
            $totalrealisasi = $this->uangmuka_model->getTotalDetilRealisasi($idUangMuka);   
            echo $totalrealisasi;
        }
    }
}
?>