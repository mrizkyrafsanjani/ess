<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class DetailUangMuka extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('user','',TRUE);				
		$this->load->model('uangmuka_model','',TRUE);	
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Permohonan Uang Muka Lainnya") )
			{
				$this->npkLogin = $session_data['npk'];
				$this->_detailuangmuka('new','');
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	
	
	public function _detailuangmuka($mode='',$id='')
    {
		
		$crud = new grocery_crud();
		$crud->set_subject('Detail Uang Muka');
		//$crud->set_theme('datatables');
		$crud->set_model('custom_query_model');
		$crud->set_table('dtluangmuka');
		
		global $noUangMuka;
		
		if ($id<>'' && $mode=='edit' )
		{
			$noUangMuka = $this->uangmuka_model->getNoUangMukaByID($id);
			$sqlnya =" select iddtluangmuka,JenisBiayaPermohonan,KeteranganPermohonan,JumlahPermohonan,
						HargaPermohonan,format(HargaPermohonan,0) as fmtHargaPermohonan,
						TotalPermohonan,format(TotalPermohonan,0) as fmtTotalPermohonan 
						from dtluangmuka 
						where Deleted=0 and ( NoUangMuka='".$noUangMuka."' or (NoUangMuka='-' and CreatedBy='".$this->npkLogin."'))  ";
		}
		else
		if ($id<>'' && $mode=='prerealisasi')
		{
			$GLOBALS[$noUangMuka] = $this->uangmuka_model->getNoUangMukaByID($id);
			$sqlnya =" select iddtluangmuka,JenisBiayaPermohonan,KeteranganPermohonan,JumlahPermohonan,
						HargaPermohonan,format(HargaPermohonan,0) as fmtHargaPermohonan,
						TotalPermohonan,format(TotalPermohonan,0) as fmtTotalPermohonan ,
						JenisBiayaRealisasi,KeteranganRealisasi,JumlahRealisasi,HargaRealisasi,format(HargaRealisasi,0) as fmtHargaRealisasi,
						TotalRealisasi, format(TotalRealisasi,0) as fmtTotalRealisasi,NoUangMuka
						from dtluangmuka 
						where Deleted=0 and ( NoUangMuka='".$GLOBALS[$noUangMuka]."' or (NoUangMuka='-' and CreatedBy='".$this->npkLogin."'))  ";
		}
		else
		$sqlnya =" select iddtluangmuka,JenisBiayaPermohonan,KeteranganPermohonan,JumlahPermohonan,
						  HargaPermohonan,format(HargaPermohonan,0) as fmtHargaPermohonan,
						  TotalPermohonan,format(TotalPermohonan,0) as fmtTotalPermohonan 
						  from dtluangmuka 
						  where Deleted=0 and NoUangMuka='-' and CreatedBy='".$this->npkLogin."'  ";

		 $crud->basic_model->set_query_str($sqlnya); //Query text here

		 if ($id<>'' && $mode=='prerealisasi')
		 {
			$crud->columns('JenisBiayaRealisasi','KeteranganRealisasi','JumlahRealisasi','fmtHargaRealisasi','fmtTotalRealisasi');
			$crud->fields('NoUangMuka','JenisBiayaRealisasi','KeteranganRealisasi','JumlahRealisasi','HargaRealisasi');	
		 }else
		 {	 
			$crud->columns('JenisBiayaPermohonan','KeteranganPermohonan','JumlahPermohonan','fmtHargaPermohonan','fmtTotalPermohonan');
			$crud->fields('NoUangMuka','JenisBiayaPermohonan','KeteranganPermohonan','JumlahPermohonan','HargaPermohonan'); 			
		 }
	

		 if ($mode=='prerealisasi')
		 {
			$crud->display_as('JenisBiayaRealisasi','Jenis Biaya');
			$crud->display_as('KeteranganRealisasi','Keterangan');
			$crud->display_as('JumlahRealisasi','Quantity');
			$crud->display_as('fmtHargaRealisasi','Price');
			$crud->display_as('fmtTotalRealisasi','Total');
		 }
		 else
		 {
			$crud->display_as('JenisBiayaPermohonan','Jenis Biaya');
			$crud->display_as('KeteranganPermohonan','Keterangan');
			$crud->display_as('JumlahPermohonan','Quantity');
			$crud->display_as('fmtHargaPermohonan','Price');
			$crud->display_as('fmtTotalPermohonan','Total');
		 }
		
		
		 if ( $mode=='prerealisasi')
		{	
			$crud->callback_field('KeteranganRealisasi',array($this,'add_field_callback_KeteranganR'));
						$crud->unset_delete();
		}
		else
			$crud->callback_field('KeteranganPermohonan',array($this,'add_field_callback_Keterangan'));
			
		

		$crud->field_type('NoUangMuka','invisible');
		$crud->field_type('CreatedOn','invisible');
		$crud->field_type('CreatedBy','invisible');
		$crud->field_type('UpdatedOn','invisible');
		$crud->field_type('UpdatedBy','invisible');
		
		$crud->required_fields('JumlahPermohonan','HargaPermohonan','JenisBiayaPermohonan');		
	
		if ($mode=='prerealisasi')
			$crud->callback_insert(array($this,'_insertRealisasi'));	
			else
			$crud->callback_insert(array($this,'_insertPermohonan'));
		
		if ($mode=='prerealisasi')
			$crud->callback_update(array($this,'_updateRealisasi'));
		else
			$crud->callback_update(array($this,'_updatePermohonan'));
		//$crud->unset_delete();	
		$crud->callback_delete(array($this,'_delete'));
		
		//$crud->unset_back_to_list();
		
		
        $output = $crud->render();		 
        $this-> _outputview($output);
	}
	
	
 
    function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Detail Uang Muka',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));		
		$this->load->view('UangMuka/DetailUangMuka_view',$data);
		
  
    }
	


	function add_field_callback_Keterangan($value = '', $primary_key = null)
	{		
		return '<textarea name="KeteranganPermohonan" rows="10" cols="150">'.$value.'</textarea>';
	}

	function add_field_callback_KeteranganR($value = '', $primary_key = null)
	{		
		return '<textarea name="KeteranganRealisasi" rows="10" cols="150">'.$value.'</textarea>';
	}

	function add_field_callback_id($value = '', $primary_key = null)
	{		
		return '<input type="text" name="KeteranganRealisasi" readonly value='.$value.'>';
	}


	
	
	function _insertPermohonan($post_array)
	{
		$dataTransaksi = array(			
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s'),
			"JenisBiayaPermohonan" => $post_array['JenisBiayaPermohonan'],
			"KeteranganPermohonan" => $post_array['KeteranganPermohonan'],
			"JumlahPermohonan" => $post_array['JumlahPermohonan'],	
			"HargaPermohonan" => $post_array['HargaPermohonan'],
			"TotalPermohonan" => $post_array['HargaPermohonan']*$post_array['JumlahPermohonan'],
			"Deleted" => 0,
			"NoUangMuka" => '-'
		);
	
		$this->db->insert('dtluangmuka',$dataTransaksi);	
	}

	function _insertRealisasi($post_array)
	{
		//$noUangMuka = $this->uangmuka_model->getNoUangMukaByID( $post_array['id']);
		$dataTransaksiR = array(			
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s'),
			"JenisBiayaRealisasi" => $post_array['JenisBiayaRealisasi'],
			"KeteranganRealisasi" => $post_array['KeteranganRealisasi'],
			"JumlahRealisasi" => $post_array['JumlahRealisasi'],	
			"HargaRealisasi" => $post_array['HargaRealisasi'],
			"TotalRealisasi" => $post_array['HargaRealisasi']*$post_array['JumlahRealisasi'],
			"Deleted" => 0,
			"NoUangMuka" => $GLOBALS[$noUangMuka]
		);
	
		$this->db->insert('dtluangmuka',$dataTransaksiR);	
	}
	
	function _updatePermohonan($post_array, $primary_key){
		fire_print('log',"post array update: ". print_r($post_array,true));
		$data['JenisBiayaPermohonan'] = $post_array['JenisBiayaPermohonan'];
		$data['KeteranganPermohonan'] = $post_array['KeteranganPermohonan'];
		$data['JumlahPermohonan'] = $post_array['JumlahPermohonan'];
		$data['HargaPermohonan'] = $post_array['HargaPermohonan'];
		$data['TotalPermohonan'] = $post_array['HargaPermohonan']*$post_array['JumlahPermohonan'];
		$data['UpdatedOn'] = date('Y-m-d H:i:s');
		$data['UpdatedBy'] = $this->npkLogin;
		
		$this->db->trans_start();
		
		$this->db->update('dtluangmuka',$data,array('iddtluangmuka' => $primary_key));
		
		$this->db->trans_complete(); 
		return $this->db->trans_status();

	}	

	function _updateRealisasi($post_array, $primary_key){
		fire_print('log',"post array update: ". print_r($post_array,true));
		$data['JenisBiayaRealisasi'] = $post_array['JenisBiayaRealisasi'];
		$data['KeteranganRealisasi'] = $post_array['KeteranganRealisasi'];
		$data['JumlahRealisasi'] = $post_array['JumlahRealisasi'];
		$data['HargaRealisasi'] = $post_array['HargaRealisasi'];
		$data['TotalRealisasi'] = $post_array['HargaRealisasi']*$post_array['JumlahRealisasi'];
		$data['UpdatedOn'] = date('Y-m-d H:i:s');
		$data['UpdatedBy'] = $this->npkLogin;
		
		$this->db->trans_start();
		
		$this->db->update('dtluangmuka',$data,array('iddtluangmuka' => $primary_key));
		
		$this->db->trans_complete(); 
		return $this->db->trans_status();

	}
	
	
	function _delete($primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['Deleted'] = '1';
		return $this->db->update('dtluangmuka',$post_array,array('iddtluangmuka' => $primary_key));	
	}

	public function edit($ID)
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){			
				$this->npkLogin = $session_data['npk'];
				$this->_detailuangmuka('edit',$ID);			
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function prerealisasi($ID)
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){			
				$this->npkLogin = $session_data['npk'];
				$this->_detailuangmuka('prerealisasi',$ID);			
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	

}

/* End of file main.php */
/* Location: ./application/controllers/main.php */