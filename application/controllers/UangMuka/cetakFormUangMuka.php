<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start(); 
	class CetakFormUangMuka extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->helper('number');
			$this->load->model('menu','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('uangmuka_model','',TRUE);
			$this->load->model('akomodasitiket_model','',TRUE);
		}
		
		function index(){
			if($this->session->userdata('logged_in')){
				$session_data = $this->session->userdata('logged_in');
				$menu = $this->menu->getMenu($session_data['npk'],$session_data['koderole']);
				if($menu){
					$menu_array = array();
					foreach($menu as $row){
					   $menu_array[] = array(
						 'menuname' => $row->menuname,
						 'url' => $row->url
					   );					   
					}
				}else{
					echo 'gagal';
				}

				$get_NoUangMuka = $this->input->get('NoUangMuka');
				$trxuangmuka = $this->uangmuka_model->getHeaderTrxUangMukabyNoUM($get_NoUangMuka);
				if($trxuangmuka){
					$trxuangmuka_array = array();
					foreach($trxuangmuka as $row){
						$trxuangmuka_array[] = array(
							'DPA' => $row->DPA,
							'NoUangMuka' => $row->NoUangMuka,
							'TanggalUangMuka' => $row->TanggalUangMuka,
							'TanggalMulai' => $row->TanggalMulai,
							'TanggalSelesai' => $row->TanggalSelesai,
							 'NPKPemohon' => $row->NPKPemohon,
							 'namaCreatedBy'=> $row->nama,
							 'namaNPKPemohon'=> $row->atasan,
							 'TipeBayarUangMuka'=> $row->TipeBayarUangMuka,
							 'BankBayarUangMuka'=> $row->BankBayarUangMuka,
							 'NoRekBayarUangMuka'=> $row->NoRekBayarUangMuka,
							 'NmPenerimaBayarUangMuka'=> $row->NmPenerimaBayarUangMuka,
							 'WaktuBayarTercepat'=> $row->WaktuBayarTercepat,
							 'WaktuPenyelesaianTerlambat'=> $row->WaktuPenyelesaianTerlambat,
							 'NoPP'=> $row->NoPP,
							 'Keterangan'=> $row->KeteranganPermohonan,
							 'NoAkomodasiTiket' => $row->NoAkomodasiTiket,
							 'TotalPermohonan' => $row->TotalPermohonan,
							 'terbilang' => terbilang($row->TotalPermohonan),
							 'NoAkomodasiTiket' => $row->NoAkomodasiTiket
						);
					}
				}else{
					echo 'gagalheader';
				}
				
				$detailuangmuka = $this->uangmuka_model->getDetailTrxUangMukabyNoUM($get_NoUangMuka);
				if($detailuangmuka){
					$detailuangmuka_array = array();
					foreach($detailuangmuka as $row){
						$detailuangmuka_array[] = array(
							'iddtluangmuka' => $row->iddtluangmuka,
							'JenisBiayaPermohonan' => $row->JenisBiayaPermohonan,
							'KeteranganPermohonan' => $row->KeteranganPermohonan,
							'JumlahPermohonan' => $row->JumlahPermohonan,
							'HargaPermohonan' => $row->HargaPermohonan,
							 'TotalPermohonan' => $row->TotalPermohonan							 
						);
					}
				}else{
					echo 'gagaldetil';
				}

				/*data Outstanding jika ada*/
						
				$NPKAtasan = $this->uangmuka_model->getAtasan($session_data['npk']);
				$dataOutstanding = $this->uangmuka_model->getOutstandingUM($session_data['npk']);
				
				$NPKDIC = $this->uangmuka_model->getNPKDIC($NPKAtasan);
				$DICPemohon = $this->uangmuka_model->getNamaDIC($NPKDIC);
				if($DICPemohon){
					$DICPemohon_array = array();
					foreach($DICPemohon as $row){
						$DICPemohon_array[] = array(
							'NamaDIC' => $row->Nama
						);
					}
				}else{
					echo 'gagaldetil';
				}
				if ($dataOutstanding)
				{
					foreach($dataOutstanding as $row)
					{	
						$dataOutstandingDetail[] = array(
							'NoUangMuka' => $row->NoUangMuka,
							'KeteranganPermohonan' => $row->KeteranganPermohonan,
							'Total' => $row->Total,
							'TotalSPD' => $row->TotalSPD,
							'ReasonOutStanding' => $row->ReasonOutStanding
						);		
					}
				}

				//detail akomodasi tiket
				$noAT = $this->uangmuka_model->getNoAkomodasiTiket($get_NoUangMuka);	
				$trxAT_array = array();			
				if ($noAT != "0")
				{
					$trxAT = $this->akomodasitiket_model->getTrxATbyNoAK($noAT);
					
					if($trxAT){						
						foreach($trxAT as $row){
							$trxAT_array = array(
								'DPA' => $row->DPA,
								'NoAkomodasiTiket' => $row->NoAkomodasiTiket,
								'KodeAkomodasiTiket' => $row->KodeAkomodasiTiket,
								'TanggalBerangkat' => $row->TanggalBerangkat,
								'TanggalKembali' => $row->TanggalKembali,
								'TanggalCheckOut' => $row->TanggalCheckOut,
								 'TanggalCheckIn' => $row->TanggalCheckIn,
								 'TanggalBerangkatPesawat'=> $row->TanggalBerangkatPesawat,
								 'TanggalKepulanganPesawat'=> $row->TanggalKepulanganPesawat,
								 'Tujuan'=> $row->Tujuan,
								 'Alasan'=> $row->Alasan,
								 'NPKAtasan'=> $row->NPKAtasan,
								 'NamaHotel'=> $row->NamaHotel,
								 'TiketPesawat'=> $row->TiketPesawat,
								 'JamBerangkatPesawat'=> $row->JamBerangkatPesawat,
								 'JamKepulanganPesawat'=> $row->JamKepulanganPesawat,
								 'KeteranganStatus'=> $row->StatusApprove,
								 'AlasanDecline'=> $row->AlasanDecline
							);
						}
					}else{
						echo $noAT;
					
					}
				}

				
				//get head hrga, dic hrga, dan dic accounting
				
				
				$query2 = $this->db->get_where('globalparam',array('Name'=>'DIC_ACC'));
				foreach($query2->result() as $row)
				{
					$npkDICACC = $row->Value;
				}

				$query2 = $this->db->get_where('globalparam',array('Name'=>'DIC_HRGA'));
				foreach($query2->result() as $row)
				{
					$npkDICHRGA = $row->Value;
				}
				
				foreach($this->user->findUserByNPK($npkDICHRGA) as $row)
				{
					$dicHrga = $row->Nama;
				}
				foreach($this->user->findUserByNPK($npkDICACC) as $row)
				{
					$dicAccounting = $row->Nama;
				}
				$headHrga = $this->user->getSingleUserBasedJabatan("HRGA Dept Head")->Nama;
				//$dicHrga = $this->user->findUserByNPK($npkDICHRGA)->Nama;
				//$dicAccounting = $this->user->getSingleUserBasedJabatan("President Director DPA 2")->Nama;
				if($trxuangmuka && $dataOutstanding) //// UangMuka & Outstanding
				{
							$data2 = array(
								'title' => 'Cetak Form Uang Muka',
								'trxuangmuka_array' => $trxuangmuka_array,
								'detailuangmuka_array' => $detailuangmuka_array,								
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'dataOutstandingDetail'=>$dataOutstandingDetail,
								'flagoutstanding' => '1',
								'DICPemohon_array' => $DICPemohon_array,
								'trxAT_array'=>$trxAT_array								
							);
				}			
				else
				{ // Uang Muka Tanpa Outsttanding							

							$data2 = array(
								'title' => 'Cetak Form Uang Muka',
								'trxuangmuka_array' => $trxuangmuka_array,
								'detailuangmuka_array' => $detailuangmuka_array,								
								'npk' => $session_data['npk'],
								'nama' => $session_data['nama'],
								'menu_array' => $menu_array,
								'flagoutstanding' => '0',
								'DICPemohon_array' => $DICPemohon_array,
								'trxAT_array'=>$trxAT_array,
								'noAT' => $noAT
							);
				}					
				$data2['HeadHRGA'] = $headHrga;
				$data2['DICHRGA'] = $dicHrga;
				$data2['DICACC'] = $dicAccounting;
	
				

				$this->load->helper(array('form','url'));
				$this->template->load('default','UangMuka/cetakFormUangMuka_view',$data2);
				//$this->template->load('default','cetakFormUangMuka_view',$data2);
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		}
	}
?>