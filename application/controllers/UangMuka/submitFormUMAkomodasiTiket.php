<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');

class submitFormUMAkomodasiTiket extends CI_Controller {

	 function __construct()
	 {
	   parent::__construct();
	   $this->load->model('trkSPD','',TRUE);
	   $this->load->model('usertask','',TRUE);
	   $this->load->model('akomodasitiket_model','',TRUE);
	   $this->load->model('user','',TRUE);
	   $session_data = $this->session->userdata('logged_in');
	 }

	 function index()
	 {
	   //This method will have the credentials validation
	   $this->load->library('form_validation');

	   /*$this->form_validation->set_rules('rbDPA', 'DPA', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtKeterangan', 'Keterangan', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtNoPP', 'No PP', 'trim|xss_clean');	 
	   $this->form_validation->set_rules('txtTanggalMulai', 'Tanggal Mulai', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtTanggalSelesai', 'Tanggal Selesai', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtDisplayPP', 'No PP', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtKegiatan', 'Jenis Kegiatan', 'trim|required|xss_clean');
	   $this->form_validation->set_rules('txtOutstanding', 'Outstanding', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtPilihBayar', 'Tipe Bayar Uang Muka', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtWaktuBayarCepat', 'Waktu Pembayaran Tercepat', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtBank', 'Bank', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtWaktuSelesaiUM', 'Waktu Penyelesian Paling Lambat', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtNoRek', 'No Rekening', 'trim|xss_clean');
	   $this->form_validation->set_rules('txtPenerima', 'Penerima', 'trim|xss_clean');*/
	  
	   
	  
	  /* if($this->form_validation->run() == false)
	   {
		 //Field validation failed.  User redirected to login page
		 $this->load->view('UangMuka/formUMLain_view');
	   }
	   else
	   {	 */
		 //Go to private area
		 if($this->input->post('submitFormUMAkomodasiTiket')){
			$DPA = $this->input->post('rbDPA');	
			$NPKPemohon = $this->input->post('txtNPK');		
			$noAT = $this->generateNoAT($DPA,$NPKPemohon);	
			$KodeUserTask = generateNo('UT');
			$KodeAkomodasiTiket = generateNo('AK');
			if($this->akomodasitiket_model->insertAkomodasiTiket($noAT,$KodeAkomodasiTiket) && 
				$this->usertask->tambahUserTask($NPKPemohon,$KodeAkomodasiTiket,$KodeUserTask,"AK")){
				//$this->db->trans_commit();
				redirect('UangMuka/LihatStatusAkomodasiTiket?for=SAVE&KodeAkomodasiTiket='.$KodeAkomodasiTiket,'refresh');				
			}else
			{
				//$this->db->trans_rollback();				
				echo('gagal save');
			}
	/*	 }*/
	   }
	}

	

	function generateNoAT($DPA,$NPKPemohon){
		$lastNo = $this->akomodasitiket_model->getLastNoAT();
		$initdept =	$this->akomodasitiket_model->getInitDept($NPKPemohon);		
		$bulanFUMInt=0;			
		if ($lastNo != "0")
		{				
			$array = explode("/", $lastNo);					
			$bulanFUMRoman = $array[4];
			$NoUrut = (int) $array[3];
			$bulanFUMInt = $this->akomodasitiket_model->NumberFromRoman($bulanFUMRoman);
		}


		if($bulanFUMInt != date('m') || $lastNo == "0")
		{
			$NextNo = "001";	
		}
		else
		{
			$NextNo = (int)$NoUrut + 1;
		}

		$noAT = 'AT/DPA'. $DPA.'/'. $initdept.'/'. str_pad($NextNo,3,"0",STR_PAD_LEFT).'/'. $this->akomodasitiket_model->integerToRoman(date('n')).'/'.date('Y');	
		
		return $noAT;
	}
	

}
?>