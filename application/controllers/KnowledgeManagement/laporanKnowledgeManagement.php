<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class LaporanKnowledgeManagement extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		if($this->session->userdata('logged_in')){
			if(check_authorized("42"))
			{
				$this->_laporanKM();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _laporanKM()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Artikel');
		//$crud->set_theme('datatables');
		
        $crud->set_table('rwynpkartikel');
		//$crud->where('artikel.status','AP');
		$crud->set_relation('KodeArtikel','artikel','judul',array('status' => 'AP'));
		$crud->set_relation('CreatedBy','mstruser','Nama',array('deleted' => '0'));
		
		$crud-> order_by('rwynpkartikel.CreatedOn','desc');
		$crud->columns('KodeArtikel','CreatedBy', 'CreatedOn');
		$crud->fields('KodeArtikel', 'CreatedOn');
				
		
		$crud->unset_edit();
		$crud->unset_delete();
		$crud->unset_add();
		$crud->unset_read();
		$crud->display_as('KodeArtikel','Judul');
		$crud->display_as('CreatedBy','Dibaca Oleh');
		$crud->display_as('CreatedOn','Waktu Mulai Membaca');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Laporan Knowledge Management',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _delete($primary_key){
		return $this->db->update('lokasiasset',array('deleted' => '1'),array('KodeLokasiAsset' => $primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */