<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class KnowledgeManagement extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('knowledgemanagement_model','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
		$this->load->library('grocery_crud');
		$this->load->helper('date');
		
    }
	
	public function baca($KodeArtikel)
	{
		if($this->session->userdata('logged_in'))
		{
			if(check_authorized("42"))
			{
				redirect("KnowledgeManagement/KnowledgeManagement/index/read/$KodeArtikel","refresh");
			}
		}
		else
		{
			redirect("login?u=KnowledgeManagement/KnowledgeManagement/baca/$KodeArtikel","refresh");
		}
	}
	
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("42"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_KM();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	public function EditDelete()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("42"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_KM('true','true','');
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	public function Cetak()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("42"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_KM('false','true','true');
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	
	public function _KM($statusEdit='',$statusCetak ='', $page='',$bulan='')
    {
		
		try{
			
			
			$crud = new grocery_crud();
			//$crud->set_theme('datatables');
			
			$state = $crud->getState();
			
			$crud->set_subject('Artikel');
			$crud->set_table('artikel');
			$crud->columns('Judul','Artikel','Sumber');
			$crud->fields('Judul','Artikel','Sumber');
			if ($state == 'edit')
			{
				$crud->edit_fields('Judul','Artikel','Sumber','Catatan');
			}
			
				
			$crud->where('artikel.deleted','0');
			
			
			$bulan = date('n');
			if($statusEdit == "true")
			{
				$crud->set_theme('datatables');
				$crud->where('artikel.CreatedBy',$this->npkLogin);
				$crud-> order_by('artikel.UpdatedOn','desc');
				$crud->where("artikel.Status","DEP");
				//$crud->or_where("artikel.Status","PE");
				$crud->unset_read();
				$crud->unset_add();
				$crud->unset_print();
				$crud->unset_export();
				$crud->columns('Judul','Artikel','Sumber','Status');
				$crud->callback_column('Status',array($this,'_callback_status'));
				
			}else if($statusCetak == "true")
			{
				$crud->set_theme('datatables');
				$crud->where('artikel.CreatedBy',$this->npkLogin);
				$crud-> order_by('artikel.UpdatedOn','desc');
				$crud->columns('Judul','Artikel','Sumber','Status');
				$crud->where("artikel.Status","PEP");
				$crud->unset_add();
				$crud->unset_edit();
				$crud->unset_delete();
				$crud->unset_read();
				$crud->unset_print();
				$crud->unset_export();
				$crud->callback_column('Status',array($this,'_callback_status'));
				$crud->add_action('Cetak','','','ui-icon-image',array($this,'callback_action_cetak'));
					
			}else{
				$crud->where('artikel.status','APR');
				$crud->where('MONTH(artikel.UpdatedOn)', $bulan);
				$crud-> order_by('artikel.UpdatedOn','desc');
				$crud->unset_edit();
				$crud->unset_delete();
				$crud->unset_read_fields('KodeArtikel','Deleted','Status','UpdatedOn','UpdatedBy');
			}
			
			$scriptjava = "";
			if($crud->getState() == 'read'){
				
				$scriptjava = "
					var url = window.location.pathname;
					var KodeArtikel = url.substring(url.lastIndexOf('/')+1);
					console.log(url);
					//alert(KodeArtikel);
					
					$.ajax({
					 type: 'POST',
					 url: '" .base_url()."index.php/KnowledgeManagement/KnowledgeManagementController/insert_rwynpkartikel',
					 data: {KodeArtikel: KodeArtikel},
					 success:function(msg){
						//alert('Data Saved:' + KodeArtikel);
						}
					});
					
					$.ajax({
					 type: 'POST',
					 url: '" .base_url()."index.php/KnowledgeManagement/KnowledgeManagementController/updateCounterArtikel',
					 data: {KodeArtikel: KodeArtikel},
					 success:function(msg){
						//alert('Data Saved:' + KodeArtikel);
						}
					});
					
					
					
					window.onbeforeunload = confirmExit;
					function confirmExit()
					{
						$.ajax({
						 type: 'POST',
						 url: '" .base_url()."index.php/KnowledgeManagement/KnowledgeManagementController/updateEndTimeRwy',
						 data: {KodeArtikel: KodeArtikel},
						 success:function(msg){
								//alert('eksekusi ajax selesai');
							}
						}
						);
						
						/* var question = confirm('Anda ingin keluar ? ');
						if (question == true){
							
						}else{
							window.location.replace('".site_url('Lembur/LemburController/ViewLemburUser')."'); 
						} */
						
						
						/* window.location.replace('".site_url('UserTask')."'); 
						console.log('Testing'); */
					}
					
					
					
					";
					
					
			}
			
			
			/* else{
				$this->_end_Time();
			} */
			
			$crud->display_as('Judul','Judul');
			$crud->display_as('Artikel','Artikel');
			$crud->display_as('Sumber','Sumber');
			
			
			//fire_print('log','ini kode didalam try-catch fungsi KM');
			$crud->required_fields('Judul','Artikel','Sumber');
			
			$crud->callback_insert(array($this,'_insert_artikel'));
			$crud->callback_edit_field('Catatan',array($this,'_callback_catatan'));
			$crud->callback_update(array($this,'_update'));
			//$crud->unset_back_to_list();
		
			$js = " <script>			
				$scriptjava			
			</script>";
			
			
			
			$output = $crud->render();
			$output->output.= $js;
			$this-> _outputview($output,$page);  
		}
		
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
	
	function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Knowledge Management',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		if($page!='')
		{
			$this->load->view('KnowledgeManagement/KnowledgeManagementSimple_view',$data);
		}
		else
		{
			$this->template->load('default','templates/CRUD_view',$data);
		}
		
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert_artikel($post_array){
		try{
			$post_array['KodeArtikel'] = generateNo('KM');
			$post_array['Deleted'] = '0';
			$post_array['Status'] = 'PEP'; //Pending
			$post_array['CreatedOn'] = date('Y-m-d H:i:s');
			$post_array['CreatedBy'] = $this->npkLogin;
			
			$this->db->trans_begin();
			$this->db->insert('artikel',$post_array);
		
			$KodeArtikel = $post_array['KodeArtikel'];
			//fire_print('log','No Transaksi '.$KodeArtikel);
			
			$KodeUserTask = generateNo('UT');
			$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeArtikel,$KodeUserTask,"KM");
			
			if($hasilTambahUserTask){
				$this->db->trans_commit();	
				return true;
			}else{
				$this->db->trans_rollback();
				return false;
			}
			
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	
	}
	
	function _callback_catatan($value,$primary_key)
	{
		$catatan = $this->usertask->getCatatan($primary_key);
		fire_print('log',$catatan);
		return $catatan;
		
	}
	
	function _update($post_array,$primary_key){	
		$query = $this->db->get_where('artikel',array('KodeArtikel'=>$primary_key));
		$dataartikel = $query->row(1);
		if(substr($dataartikel->Status,0,2) == 'DE'){
			$post_array['Status'] = 'PEP'; //Pending Plan
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			
			$this->db->trans_start();
			
			$this->db->update('artikel',$post_array,array('KodeArtikel' => $primary_key));
			$KodeArtikel = $primary_key;
			//usertask lama di nyatakan statusnya AP
			
			$usertasklama = $this->usertask->getLastUserTask($KodeArtikel);
			$KodeUserTaskLama = '';
			if($usertasklama){
				foreach($usertasklama as $row){
					$KodeUserTaskLama = $row->KodeUserTask;
					
				}
				$this->usertask->updateStatusUserTask($KodeUserTaskLama,'AP',$this->npkLogin);
			}
			fire_print('log',$KodeUserTaskLama);
			//buat user task baru untuk proses approval artikel
			$KodeUserTask = generateNo('UT');
			
			
			if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeArtikel,$KodeUserTask,"KM"))
			{
				echo('gagal simpan usertask artikel');
				return false;
			}
			$this->db->trans_complete(); 
			return $this->db->trans_status();
		}
		else
		{
			return false;
		}	
		
	}
	
	
	
	function callback_action_cetak($KodeArtikel,$row)
	{		
		if($row->Status == "Pending Approval Rencana")
		{
			return site_url('KnowledgeManagement/KnowledgeManagementController/CetakKnowledgeManagement/'.$KodeArtikel);
		}
		else
		{
			return site_url('KnowledgeManagement/KnowledgeManagementController/tidakbisa_cetak');
		}
	}
	
	function _callback_status($value, $row){
		switch($value)
		{
			case "PEP": $text = "Pending Approval Rencana"; break;
			case "DEP": $text = "Rencana Declined"; break;
			case "APP": $text = "Rencana Approved"; break;
			case "PER": $text = "Pending Approval Realisasi"; break;
			case "DER": $text = "Realisasi Declined"; break;
			case "APR": $text = "Realisasi Approved"; break;
			
		}
		return $text;
	}
	
	

	
	
}
?>