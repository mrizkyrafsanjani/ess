<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class KnowledgeManagementController extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('knowledgemanagement_model','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('departemen','',TRUE);
		$this->load->model('mstrgolongan','',TRUE);
		$this->load->model('user','',TRUE);
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("42"))
			{
				$this->npkLogin = $session_data['npk'];
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
		
		
    }
	
	function ApprovalAwalKnowledgeManagement($KodeUserTask)
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("43"))
					{
						$dataartikel = $this->knowledgemanagement_model->getArtikel($KodeUserTask);
						foreach($dataartikel as $row){
							$historycatatan = $this->usertask->getCatatan($row->KodeArtikel);
							
							$identitas = $this->user->dataUser($row->CreatedBy);
							foreach($identitas as $datauser){
								$nama = $datauser->nama;
								$departemen = $datauser->departemen;
							}
							$artikel = array(
								'realisasi' => '0',
								'title' => 'Approval Artikel Knowledge Management',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'nama' => $nama,
								'departemen' => $departemen,
								'npk' => $row->CreatedBy,
								'judul' => $row->Judul,
								'artikel' => $row->Artikel,
								'sumber' => $row->Sumber,
								'historycatatan' => $historycatatan
							);
							
						}
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','KnowledgeManagement/knowledgeManagement_view',$artikel);
					}
				}
				else
				{
					redirect("login?u=KnowledgeManagement/knowledgeManagementController/ApprovalAwalKnowledgeManagement/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ApprovalKnowledgeManagement($KodeUserTask)
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("43"))
					{
						$dataartikel = $this->knowledgemanagement_model->getArtikel($KodeUserTask);
						foreach($dataartikel as $row){
							$historycatatan = $this->usertask->getCatatan($row->KodeArtikel);
							
							$identitas = $this->user->dataUser($row->CreatedBy);
							foreach($identitas as $datauser){
								$nama = $datauser->nama;
								$departemen = $datauser->departemen;
							}
							$artikel = array(
								'realisasi' => '1',
								'title' => 'Approval Knowledge Management',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'nama' => $nama,
								'departemen' => $departemen,
								'npk' => $row->CreatedBy,
								'judul' => $row->Judul,
								'artikel' => $row->Artikel,
								'sumber' => $row->Sumber,
								'historycatatan' => $historycatatan
							);
							
						}
						
						$this->load->helper(array('form','url'));
						$this->template->load('default','KnowledgeManagement/knowledgeManagement_view',$artikel);
					}
				}
				else
				{
					redirect("login?u=KnowledgeManagement/knowledgeManagementController/ApprovalKnowledgeManagement/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_prosesApproval()
		{
			try
			{
				$success = "Berhasil";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				
				$query = $this->knowledgemanagement_model->getArtikel($KodeUserTask);
				if($query){
					foreach($query as $row){
						$statusRencana = substr($row->Status, -1);
						$KodeArtikel = $row->KodeArtikel;
						$penulis = $row->CreatedBy;
						$statusKM = substr($row->Status, 0, 2); //dapatkan PE
					}
				}else{
					$success = "Tidak ada data knowledge management";
				}
				
				if($statusKM == 'PE')
				{
					$this->db->trans_begin();
					
					//update status artikel
					if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'KM') && $status == 'AP')
					{
						$this->knowledgemanagement_model->updateStatusKM($KodeUserTask,$status.$statusRencana,$this->npkLogin);
					}
					
					if($status == "DE")
					{
						$this->knowledgemanagement_model->updateStatusKM($KodeUserTask,$status.$statusRencana,$this->npkLogin);
						
					}
					
					//update user task sekarang
					$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan);
					
					//create user task baru untuk next proses
					$KodeUserTaskNext = generateNo('UT');	
					fire_print('log',$KodeUserTaskNext)	;
					if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeArtikel,$KodeUserTaskNext,"KM".$status,$penulis))
					{
						$success = "GAGAL USERTASK";
						$this->db->trans_rollback();
					}
					
					if ($this->db->trans_status() === FALSE)
					{
						fire_print('log','trans rollback approve KnowledgeManagement ');
						$this->db->trans_rollback();
					}
					else
					{
						fire_print('log','trans commit approve KnowledgeManagement ');
						$this->db->trans_commit();
					}
					fire_print('log','Proses Trans Approve KnowledgeManagement selesai ');
					
				}
				else
				{
					$success = "Update status tidak bisa dilakukan,karena status knowledge management bukan pending.";
				}
				
				echo $success;
				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_prosesApprovalRealisasi()
		{
			$error = 0;
			try
			{
				$success = "Berhasil";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				
				$query = $this->knowledgemanagement_model->getArtikel($KodeUserTask);
				if($query){
					foreach($query as $row){
						//$statusRencana = substr($row->Status, -1);
						$KodeArtikel = $row->KodeArtikel;
						$penulis = $row->CreatedBy;
					}
				}else{
					$success = "Tidak ada data";
				}
				$this->db->trans_begin();
				
				//update status artikel
				if($this->knowledgemanagement_model->updateStatusKM($KodeUserTask,$status."R",$this->npkLogin))
				{
					//update user task sekarang
					$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin,$catatan);
					
					if($status == "DE"){
						//create user task baru untuk next proses
						$KodeUserTaskNext = generateNo('UT');			
						if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeArtikel,$KodeUserTaskNext,"KM".$status,$penulis))
						{
							$success = "Gagal membentuk user task untuk decline";
							$error += 1;
						}
					}
					
					if($status == "AP"){
						$query = $this->db->get_where("globalparam",array("Name"=>"EmailAllKaryawan"));
						$gbEmailAllKaryawan = $query->row(1);
						if(!$this->sendEmailShare_KM($gbEmailAllKaryawan->Value,$KodeUserTask)){
							$success = "Gagal melakukan pengiriman email";
							$error += 1;
						}
					}
				}
				else
				{
					$success = "Gagal menyimpan status realisasi knowledge management";
					$error +=1;
				}
				
				if($error > 0){
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
				}
				echo $success;
				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function sendEmailShare_KM($to,$KodeUserTask)
		{
			try
			{
				$query = $this->knowledgemanagement_model->getArtikel($KodeUserTask);
				if($query){
					foreach($query as $row){
						$KodeArtikel = $row->KodeArtikel;
						$judul = $row->Judul;
						$isi = $row->Artikel;
						$penulis = $row->CreatedBy;
					}
				}
				
				$this->load->library('email');
				
				$this->email->from('ess@dpa.co.id', 'Enterprise Self Service');
				$this->email->to($to);

				$this->email->subject("Artikel Knowledge Management");
				$message = "Terdapat artikel knowledge management terbaru. Dapat dilihat pada Aplikasi Enterprise Self Service.

Cuplikan artikel:
Judul: $judul

". substr(strip_tags($isi),0,200) ."...

Lihat artikel selengkapnya pada url dibawah ini:
". base_url(). "index.php/KnowledgeManagement/KnowledgeManagement/baca/$KodeArtikel";
				$this->email->message($message);	

				if( ! $this->email->send())
				{	
					return false;
				}
				else
				{
					return true;
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function EditKM($KodeUserTask='') //user
		{
			try
			{
				if($this->session->userdata('logged_in')){
					$KodeArtikel = $this->knowledgemanagement_model->getEdit($KodeUserTask);
					
					fire_print('log',$KodeArtikel);
					redirect('KnowledgeManagement/KnowledgeManagement/EditDelete/edit/'.$KodeArtikel,'refresh');
				}
				else
				{
					redirect("login?u=KnowledgeManagement/knowledgeManagementController/EditKM/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function updateEndTimeRwy() //user
		{
			try
			{
				$KodeArtikel = $this->input->post('KodeArtikel');
				fire_print('log','EndTime Kode artikel '.$KodeArtikel );
				$EndTime = $this->knowledgemanagement_model->updaterwynpkartikelendtime($this->npkLogin,$KodeArtikel);
				fire_print('log','update End Time value'. $EndTime);
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function updateCounterArtikel() //user
		{
			try
			{
				$KodeArtikel = $this->input->post('KodeArtikel');
				fire_print('log','pengunjung Kode artikel '.$KodeArtikel );
				$Counter = $this->knowledgemanagement_model->updatecounterartikel($this->npkLogin,$KodeArtikel);
				fire_print('log','insert counter value'.$Counter);
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function insert_rwynpkartikel(){
	
			try
			{
				$KodeArtikel = $this->input->post('KodeArtikel');
				fire_print('log','EndTime Kode artikel '.$KodeArtikel );
				$EndTime = $this->knowledgemanagement_model->insert_rwynpkartikel($this->npkLogin,$KodeArtikel);
				fire_print('log','insert End Time value'.$EndTime);
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
			
		catch(Exception $e)
		{
			
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	
	}
	
	function CetakKnowledgeManagement($KodeArtikel)
	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("46"))
					{
						$dataartikel = $this->knowledgemanagement_model->cetakArtikel($KodeArtikel);
						foreach($dataartikel as $row){
						$identitas = $this->user->dataUser($row->CreatedBy);
							foreach($identitas as $dataUser){
								$nama = $dataUser->nama;
								$departemen = $dataUser->departemen;
								$atasan = $this->user->getDIC($dataUser->KodeDepartemen)->Nama;
								}
							$artikel = array(
								'title' => 'Cetak Knowledge Management',
								'nama' => $nama,
								'departemen' => $departemen,
								'atasan' => $atasan,
								'npk' => $row->CreatedBy,
								'judul' => $row->Judul,
								'artikel' => $row->Artikel,
								'sumber' => $row->Sumber
							);
						}
						
						$this->load->helper(array('form','url'));
						//$this->template->load('default','KnowledgeManagement/knowledgeManagement_cetak',$artikel);
						$this->load->view('KnowledgeManagement/knowledgeManagement_cetak',$artikel);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		
		
		function tidakbisa_cetak()
		{
			try
			{
				$this->session->set_flashdata('msg','Knowledge Management tidak dapat dicetak');
				redirect('KnowledgeManagement/KnowledgeManagement/EditDelete');
				return true;
			}
				catch(Exception $e)
			{
				throw new Exception( 'Something really gone wrong', 0, $e);
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				return false;
			}
		}
		
		
		
		function Cetak_KM() //user
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("42"))
					{	$data = array(
							'title' => 'Cetak Knowledge Management'
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','KnowledgeManagement/Cetak_KM_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		
		
}
?>