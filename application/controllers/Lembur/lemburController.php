<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class LemburController extends CI_Controller{
		var $npkLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('absensi','',TRUE);
			$this->load->model('usertask','',TRUE);
			$this->load->model('lembur_model','',TRUE);
			$this->load->model('dtltrkrwy_model','',TRUE);
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
		}
		
		function index() //untuk input
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("28"))
					{
						$kehadiran = '';
						$izin = '';
						$cuti = '';
						$sakit = '';
						$perjalanandinas = '';
						$data = array(
							'title' => 'Lembur',
							'admin' => '0',
							'kehadiran' => $kehadiran,
							'izin' => $izin,
							'cuti' => $cuti,
							'sakit' => $sakit,
							'perjalanandinas' => $perjalanandinas,
							'npk' => ''
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Lembur/lembur_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function viewLemburAdmin() //admin
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("29"))
					{
						$kehadiran = '';
						$izin = '';
						$cuti = '';
						$sakit = '';
						$perjalanandinas = '';
						$data = array(
							'title' => 'Lembur',
							'admin' => '1',
							'kehadiran' => $kehadiran,
							'izin' => $izin,
							'cuti' => $cuti,
							'sakit' => $sakit,
							'perjalanandinas' => $perjalanandinas,
							'databawahan' => $this->user->getDataBawahan($this->npkLogin),
							'npk' => ''
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Lembur/lembur_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ViewLemburUser($KodeUserTask='') //user
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("30"))
					{
						if($KodeUserTask == '')
							$KodeUserTask = 'non';
						$tul = '';
						$tunjanganmakan = '';
						$tunjangantransport = '';
						$totaljamlembur = '';
						$data = array(
							'title' => 'View Lembur User',
							'admin' => '0',
							'tul' => $tul,
							'tunjanganmakan' => $tunjanganmakan,
							'tunjangantransport' => $tunjangantransport,
							'totaljamlembur' => $totaljamlembur,
							'npk' => $this->npkLogin,
							'kodeusertask' => $KodeUserTask
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Lembur/lembur_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function Realisasi($KodeLembur)
		{
			try
			{
				//cek dulu apakah sudah bisa realisasi?
				
				//$this->db->trans_start(); KETIKA DI aktifkan malah membuat lock
				//jika ya, update status user task jadi AP
				$kodeusertasklama = $this->dtltrkrwy_model->getKodeUserTaskLast($KodeLembur);
				$this->usertask->updateStatusUserTask($kodeusertasklama,'AP',$this->npkLogin,'');
				
				//kirim user task ke Head.
				$this->session->set_flashdata('msg',"Approval realisasi sudah dikirim ke atasan");
				$KodeUserTask = generateNo('UT');
				if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeLembur,$KodeUserTask,"LBR"))
				{
					$this->session->set_flashdata('msg',"Proses realisasi mengalami kegagalan");
				}
				else
				{
					$this->lembur_model->updateStatusLembur($KodeUserTask,'PER',$this->npkLogin);
				}
				//$this->db->trans_complete();
				
				redirect('lembur/lembur/viewuser');
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function tidakbisa_ubah()
		{
			$this->session->set_flashdata('msg','Tidak bisa ubah, proses ubah hanya bisa dilakukan jika rencana lembur di decline');
			redirect('lembur/lembur/viewuser');
		}
		
		function tidakbisa_realisasi()
		{
			$this->session->set_flashdata('msg','Tidak bisa realisasi, proses realisasi hanya bisa dilakukan jika rencana lembur sudah diapprove dan jam realisasi sudah terisi');
			redirect('lembur/lembur/viewuser');
		}
		
		function ajax_GetLembur()
		{
			$tul = '0';
			$tunjanganmakan = '0';
			$tunjangantransport = '0';
			$totaljamlembur = '0';
			$nama = $this->input->post('namaKaryawan');
			$tahun = $this->input->post('tahun');
			$bulan = $this->input->post('bulan');
			$npk = $this->input->post('npk');
			$datalembur = $this->lembur_model->getUpahLembur($nama,$tahun,$bulan,$npk);
			if($datalembur){
				foreach($datalembur as $row)
				{
					$tunjanganmakan = $row->TunjanganMakan;
					$tunjangantransport = $row->TunjanganTransport;
					$tul = $row->TotalUpahLembur;
					$totaljamlembur = $row->TotalJamLembur;
				}
			}
			
			$dirArray = array(
				'tul' => $tul,
				'tunjanganmakan' => $tunjanganmakan,
				'tunjangantransport' => $tunjangantransport,
				'totaljamlembur' => $totaljamlembur
			);
			//fire_print('log',$dirArray);
			echo json_encode($dirArray);
		}
		
		function ApprovalRencanaLembur($KodeUserTask)
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("31"))
					{
						$datalembur = $this->lembur_model->getLembur($KodeUserTask);
						
						foreach($datalembur as $row){
							$akumulasilemburperbulan = $this->lembur_model->getTotalJamLemburPerBulan($row->NPK,$row->Tanggal);
							$totaljamlemburygtercatat = 0;
							if($akumulasilemburperbulan)
								$totaljamlemburygtercatat = $akumulasilemburperbulan[0]->totaljam;
							if($totaljamlemburygtercatat == '')
								$totaljamlemburygtercatat = '0';
							
							//fire_print('log',$akumulasilemburperbulan);
							//print_r($akumulasilemburperbulan[0]->totaljam);
							$historycatatan = $this->usertask->getCatatan($row->KodeLembur);
							$data = array(
								'realisasi' => '0',
								'title' => 'Approval Rencana Lembur User',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'npk' => $this->npkLogin,
								'nama' => $row->Nama,
								'hari' => $row->Hari,
								'tanggal' => $row->Tanggal,
								'jammulairencana' => $row->JamMulaiRencana,
								'jamselesairencana' => $row->JamSelesaiRencana,
								'tugaslembur' => $row->TugasLembur,
								'jammulairealisasi' => $row->JamMulaiRealisasi,
								'jamselesairealisasi' => $row->JamSelesaiRealisasi,
								'akumulasilemburperbulan' => $totaljamlemburygtercatat,
								'historycatatan' => $historycatatan
							);
						}
						$this->load->helper(array('form','url'));
						$this->template->load('default','Lembur/approvalLembur_view',$data);
					}
				}
				else
				{
					redirect("login?u=Lembur/lemburController/ApprovalRencanaLembur/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ApprovalRealisasiLembur($KodeUserTask)
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("31"))
					{
						$datalembur = $this->lembur_model->getLembur($KodeUserTask);
						foreach($datalembur as $row){
							$akumulasilemburperbulan = $this->lembur_model->getTotalJamLemburPerBulan($row->NPK,$row->Tanggal);
							$historycatatan = $this->usertask->getCatatan($row->KodeLembur);
							$data = array(
								'realisasi' => '1',
								'title' => 'Approval Realisasi Lembur User',
								'admin' => '0',
								'kodeusertask' => $KodeUserTask,
								'npk' => $this->npkLogin,
								'nama' => $row->Nama,
								'hari' => $row->Hari,
								'tanggal' => $row->Tanggal,
								'jammulairencana' => $row->JamMulaiRencana,
								'jamselesairencana' => $row->JamSelesaiRencana,
								'tugaslembur' => $row->TugasLembur,
								'jammulairealisasi' => $row->JamMulaiRealisasi,
								'jamselesairealisasi' => $row->JamSelesaiRealisasi,
								'akumulasilemburperbulan' => $akumulasilemburperbulan[0]->totaljam,
								'historycatatan' => $historycatatan
							);
						}
						$this->load->helper(array('form','url'));
						$this->template->load('default','Lembur/approvalLembur_view',$data);
					}
				}
				else
				{
					redirect("login?u=Lembur/lemburController/ApprovalRealisasiLembur/$KodeUserTask",'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_prosesApproval()
		{
			try
			{
				$success = "Berhasil";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				$catatan = $this->input->post('catatan');
				$query = $this->lembur_model->getLembur($KodeUserTask);
				if($query){
					foreach($query as $row){
						$statusRencana = substr($row->StatusLembur, -1);
						$KodeLembur = $row->KodeLembur;
						$pengajuLembur = $row->NPK;
						$statusLembur = substr($row->StatusLembur, 0, 2); //dapatkan PE
					}
				}else{
					$success = "Tidak ada data";
				}
				
				if($statusLembur == 'PE')
				{
					$this->db->trans_begin();
					$statusBaru = $status.$statusRencana;
					
					//update status lembur
					if($status == 'AP'){
						if($this->usertask->isAlreadyMaxSequence($this->npkLogin,'LB'))
						{
							$this->lembur_model->updateStatusLembur($KodeUserTask,$status.$statusRencana,$this->npkLogin);
						}
					}
					
					if($status == 'DE')
					{
						$this->lembur_model->updateStatusLembur($KodeUserTask,$status.$statusRencana,$this->npkLogin);
					}
					
					//update user task sekarang
					$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin, $catatan);
					
					//create user task baru untuk next proses
					$KodeUserTaskNext = generateNo('UT');			
					if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeLembur,$KodeUserTaskNext,"LB".$status,$pengajuLembur))
					{
						$success = "0";
					}
					
					/*
					if($this->lembur_model->updateStatusLembur($KodeUserTask,$status.$statusRencana,$this->npkLogin))
					{
						//update user task sekarang
						$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin, $catatan);
						
						//create user task baru untuk next proses
						$KodeUserTaskNext = generateNo('UT');			
						if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeLembur,$KodeUserTaskNext,"LB".$status,$pengajuLembur))
						{
							$success = "0";
						}
					}
					else
					{
						$success = "0";
					}
					*/
					//$status = $this->db->trans_complete();
					if ($this->db->trans_status() === FALSE)
					{
						fire_print('log','trans rollback approve lembur ');
						$this->db->trans_rollback();
					}
					else
					{
						fire_print('log','trans commit approve lembur ');
						$this->db->trans_commit();
					}
					fire_print('log','Proses Trans Approve Lbr selesai ');
				}
				else
				{
					$success = "Tidak dapat melakukan proses terhadap transaksi ini, karena sudah di " . ($statusLembur == "AP" ? "approve" : $statusLembur == "DE" ? "decline" : "error");
				}
				echo $success;
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
				echo "Error!";
			}
		}
		
		function ajax_prosesApprovalRealisasi()
		{
			try
			{
				$success = "Berhasil";
				$KodeUserTask = $this->input->post('kodeusertask');
				$status = $this->input->post('status');
				
				$query = $this->lembur_model->getLembur($KodeUserTask);
				if($query){
					foreach($query as $row){
						$statusRencana = substr($row->StatusLembur, -1);
						$KodeLembur = $row->KodeLembur;
						$pengajuLembur = $row->NPK;
					}
				}else{
					$success = "Tidak ada data";
				}
				$this->db->trans_begin();
				$statusBaru = $status.$statusRencana;
				
				//update status lembur
				if($this->lembur_model->updateStatusLembur($KodeUserTask,$status.$statusRencana,$this->npkLogin))
				{
					//update user task sekarang
					$this->usertask->updateStatusUserTask($KodeUserTask,$status,$this->npkLogin);
					
					if($status == "DE"){
						//create user task baru untuk next proses
						$KodeUserTaskNext = generateNo('UT');			
						if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeLembur,$KodeUserTaskNext,"LBR".$status,$pengajuLembur))
						{
							$success = "0";
						}
					}
				}
				else
				{
					$success = "0";
				}
				fire_print('log','success proses ApprovalRealisasi:'.$success);
				if($success == "0")
				{
					fire_print('log','gagal proses approval realisasi');
					$this->db->trans_rollback();
				}
				else
				{
					$this->db->trans_commit();
				}
				
				echo $success;
				
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
	}
?>