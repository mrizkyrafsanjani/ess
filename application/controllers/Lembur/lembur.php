<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Lembur extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('usertask','',TRUE);
		$this->load->model('lembur_model','',TRUE);
		$this->load->library('grocery_crud');
		$this->load->helper('date');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("28"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_lembur();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function viewUser($tahun='',$bulan='',$nama='',$NPKuser='',$KodeUserTask='')
	{
		if($KodeUserTask == 'non')
			$KodeUserTask = '';
		if($nama == 'non')
			$nama = '';
		if($tahun == 'non')
			$tahun = date('Y');
		if($bulan == 'non')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("30"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_lembur('viewUser',$nama,$tahun,$bulan,$KodeUserTask);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function viewAdmin($tahun='',$bulan='',$nama='non')
	{
		if($nama == 'non')
			$nama = 'xxx';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("29"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_lembur('viewAdmin',$nama,$tahun,$bulan);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	
	public function _lembur($page='',$nama='',$tahun='',$bulan='',$KodeUserTask='')
    {
		try{
			$crud = new grocery_crud();
			$crud->set_subject('Lembur');
			
			//$crud->set_theme('datatables');
			
			$crud->set_table('lembur');
			$crud->set_relation('NPK','mstruser','Nama',array('deleted' => '0'));			
			$crud->where('lembur.deleted','0');
			
			if($nama != '')
			{
				$crud->like('lembur.NPK',$nama);
			}
			if($tahun != '')
			{
				$crud->where('YEAR(lembur.Tanggal)',$tahun);
			}
			if($bulan != '')
			{
				$crud->where('MONTH(lembur.Tanggal)', $bulan);
			}
			
			if($KodeUserTask != '')
			{				
				$lembur = $this->lembur_model->getLembur($KodeUserTask);
				foreach($lembur as $rowLembur){
					$kodeLembur = $rowLembur->KodeLembur;
				}
				fire_print('log',$kodeLembur);
				$crud->where('lembur.KodeLembur',$kodeLembur);
			}
			
			switch($page)
			{
				case "viewAdmin":
					$crud->columns('NPK','Tanggal','Hari','JamMulaiRealisasi','JamSelesaiRealisasi','TotalJamRealisasi','TugasLembur','StatusLembur');
					$crud->unset_add();
					$crud->unset_edit();
					$crud->unset_print();
					$crud->unset_read();
					$crud->unset_delete();
					break;
				case "viewUser":
					$crud->where('lembur.NPK',$this->npkLogin);
					$crud->set_theme('datatables');
					$crud->columns('Tanggal','Hari','JamMulaiRencana','JamSelesaiRencana','JamMulaiRealisasi','JamSelesaiRealisasi','TotalJamRealisasi',		'TugasLembur','StatusLembur');
					
					$crud->unset_add();
					$crud->unset_print();
					$crud->unset_read();
					$crud->unset_export();
					$crud->add_action('Realisasi','','','ui-icon-image',array($this,'callback_action_realisasi'));
					$crud->add_action('ubah','','','ui-icon-image',array($this,'callback_action_ubah'));
					break;
				default:
					
					break;
			}	
			
			$crud->fields('Tanggal','JamMulaiRencana','JamSelesaiRencana','TugasLembur');
			$crud->edit_fields('Tanggal','JamMulaiRencana','JamSelesaiRencana','TugasLembur','Catatan');
			
			$crud->display_as('JamMulaiRencana','Rencana Dari Pukul (hh:mm)');
			$crud->display_as('JamSelesaiRencana','Sampai Pukul (hh:mm)');
			$crud->display_as('TugasLembur','Tugas Yang Dikerjakan');
			$crud->display_as('JamMulaiRealisasi','Realisasi Dari Pukul (hh:mm)');
			$crud->display_as('JamSelesaiRealisasi','Sampai Pukul (hh:mm)');
			$crud->display_as('TotalJamRealisasi','Total Lembur (Jam)');
			$crud->display_as('StatusLembur','Status Lembur');
			
			$crud->required_fields('Tanggal','JamMulaiRencana','JamSelesaiRencana','TugasLembur');
			
			$crud->set_rules('Tanggal','Tanggal Lembur','callback_validasiTanggalLembur');
			$crud->set_rules('JamSelesaiRencana','Jam Selesai','callback_validasiJamSelesai');
			/*
			$crud->set_rules('Golongan','Golongan','integer');
			$crud->set_rules('UangSaku','Uang Saku','integer');
			$crud->set_rules('UangMakan','Uang Makan','integer');
			*/
			//$crud->callback_column('UangSaku',array($this,'_callback_format_number'));
			//$crud->callback_column('UangMakan',array($this,'_callback_format_number'));
			$crud->callback_column('StatusLembur',array($this,'_callback_statuslembur'));
			$crud->callback_edit_field('Catatan',array($this,'_callback_catatan'));
			$crud->callback_insert(array($this,'_insert'));
			$crud->callback_update(array($this,'_update'));
			//$crud->callback_before_delete(array($this,'_cek_statuslembur'));
			$crud->callback_delete(array($this,'_delete_lembur'));
			
			$crud->callback_field('JamMulaiRencana',array($this,'field_callback_JamMulaiRencana'));
			$crud->callback_field('JamSelesaiRencana',array($this,'field_callback_JamSelesaiRencana'));
			
			$crud->unset_texteditor('TugasLembur');
			$crud->unset_back_to_list();
			$js = " <script>
					
					var spans = document.getElementsByTagName('span');					
					for(var i=0;i<spans.length;i++)
					{
						if(spans[i].innerHTML == '&nbsp;Edit')
							spans[i].style.display = 'none';
						if(spans[i].innerHTML == '&nbsp;Ubah')
							spans[i].style.display = 'none';
						if(spans[i].innerHTML == '&nbsp;Delete')
							spans[i].innerHTML = 'Hapus';
					}
				
				</script>";
			
			$output = $crud->render();
			$output->output.= $js;
	   
			$this-> _outputview($output,$page);  
		}
		catch(Exception $e)
		{
			fire_trace($e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
 
	function validasiTanggalLembur($value){
		try
		{
			if($value < date("Y-m-d",time() - 60 * 60 * 24 * 3)){
				fire_print('log',"nilai value : $value");
				$this->form_validation->set_message('validasiTanggalLembur',"Anda hanya bisa memasukkan rencana lembur minimal 3 hari yang lalu dari sekarang");
				return false;
			}
			return true;
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function validasiJamSelesai($value){
		$jamMulai = $this->input->post('JamMulaiRencana');
		$jamSelesai = $this->input->post('JamSelesaiRencana');
		
		if($jamSelesai < $jamMulai){
			$this->form_validation->set_message('validasiJamSelesai', 'Jam Selesai harus lebih besar dari Jam Mulai.');
			return false;
		}
	}
	
    function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Lembur',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));		
		if($page!='')
		{
			$this->load->view('Lembur/lemburSimple_view',$data);
		}
		else
		{		
			$this->template->load('default','templates/CRUD_view',$data);
		}
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function callback_action_ubah($primary_key,$row)
	{		
		if($row->StatusLembur == "Rencana Declined")
		{
			return site_url('lembur/lembur/viewuser/non/non/non/non/non/edit/'.$primary_key);
		}
		else
		{
			return site_url('lembur/LemburController/tidakbisa_ubah');
		}
	}
	
	function callback_action_realisasi($primary_key,$row)
	{
		$usertask = $this->usertask->getLastUserTask($primary_key);
		$KodeUserTask = '';
		if($usertask){
			foreach($usertask as $ut){
				$KodeUserTask = $ut->KodeUserTask;
			}			
		}
		if($row->StatusLembur == "Rencana Approved" && $row->JamMulaiRealisasi != "" && $row->JamSelesaiRealisasi != "")
		{
			return site_url('Lembur/LemburController/Realisasi/'.$primary_key);
		}
		else
		{
			return site_url('lembur/LemburController/tidakbisa_realisasi');
		}
	}
	
	function _cek_statuslembur($primary_key)
	{
		try
		{
			$this->session->set_flashdata('msg','Tidak bisa ubah, proses ubah hanya bisa dilakukan jika rencana lembur di decline');
			redirect('lembur/lembur/viewuser');
			return true;
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function _insert($post_array){
		try{
			$post_array['KodeLembur'] = generateNo('LB');
			$post_array['NPK'] = $this->npkLogin;
			$post_array['Hari'] = namaHari($post_array['Tanggal']);
			$post_array['StatusLembur'] = 'PEP'; //Pending Plan
			$post_array['CreatedOn'] = date('Y-m-d H:i:s');
			$post_array['CreatedBy'] = $this->npkLogin;
			
			$this->db->trans_begin();
			$this->db->insert('lembur',$post_array);
			
			$KodeLembur = $post_array['KodeLembur'];//$this->lembur_model->getKodeLembur($this->npkLogin);
			
			$KodeUserTask = generateNo('UT');
			$hasilTambahUserTask = $this->usertask->tambahUserTask($this->npkLogin,$KodeLembur,$KodeUserTask,"LB");
			
			fire_print('log',$hasilTambahUserTask);
			if(!$hasilTambahUserTask)
			{
				//echo('gagal simpan usertask lembur');
				fire_print('log','insert lembur rollback');
				$this->db->trans_rollback();
				return false;
			}else{
				fire_print('log','insert lembur commit');
				$this->db->trans_commit();
				return true;
			}
		}
		catch(Exception $e)
		{
			throw new Exception( 'Something really gone wrong', 0, $e);
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			return false;
		}
	}
	
	function _update($post_array,$primary_key){	
		$query = $this->db->get_where('lembur',array('KodeLembur'=>$primary_key));
		$datalembur = $query->row(1);
		if($datalembur->StatusLembur == 'DEP'){
			$post_array['Hari'] = namaHari($post_array['Tanggal']);
			$post_array['StatusLembur'] = 'PEP'; //Pending Plan
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			
			$this->db->trans_start();
			
			$this->db->update('lembur',$post_array,array('KodeLembur' => $primary_key));
			$KodeLembur = $primary_key;
			//usertask lama di nyatakan statusnya AP
			
			$usertasklama = $this->usertask->getLastUserTask($KodeLembur);
			$KodeUserTaskLama = '';
			if($usertasklama){
				foreach($usertasklama as $row){
					$KodeUserTaskLama = $row->KodeUserTask;
				}
				$this->usertask->updateStatusUserTask($KodeUserTaskLama,'AP',$this->npkLogin);
			}
			fire_print('log',$KodeUserTaskLama);
			//buat user task baru untuk proses approval rencana lembur
			$KodeUserTask = generateNo('UT');
			if(!$this->usertask->tambahUserTask($this->npkLogin,$KodeLembur,$KodeUserTask,"LB"))
			{
				echo('gagal simpan usertask lembur');
				return false;
			}
			$this->db->trans_complete(); 
			return $this->db->trans_status();
		}
		else
		{
			return false;
		}
	}
	
	function _delete_lembur($primary_key){
		$KodeLembur = $primary_key;
		$query = $this->db->get_where('lembur',array('KodeLembur'=>$KodeLembur));
		$datalembur = $query->row(1);
		if($datalembur->StatusLembur == 'DEP'){
			//usertask lama di nyatakan statusnya DE
			$query = $this->usertask->getLastUserTask($KodeLembur);
			foreach($query as $row){
				$KodeUserTaskLama = $row->KodeUserTask;
			}
			$this->usertask->updateStatusUserTask($KodeUserTaskLama,'DE',$this->npkLogin);
			
			return $this->db->update('lembur',array('deleted' => '1'),array('KodeLembur' => $primary_key));
		}
		else
		{
			return false;
		}
	}
	
	function _callback_statuslembur($value, $row){
		switch($value)
		{
			case "PEP": $text = "Pending Approval Rencana"; break;
			case "DEP": $text = "Rencana Declined"; break;
			case "APP": $text = "Rencana Approved"; break;
			case "PER": $text = "Pending Approval Realisasi"; break;
			case "DER": $text = "Realisasi Declined"; break;
			case "APR": $text = "Realisasi Approved"; break;
		}
		return $text;
	}
	
	function _callback_catatan($value,$primary_key)
	{
		$catatan = $this->usertask->getCatatan($primary_key);
		return $catatan;
	}
	
	function _callback_format_number($value, $row){
		$formatNumber = number_format($value);
		return "Rp. ". $formatNumber;
	}
	
	function field_callback_JamMulaiRencana($value='',$primary_key=null)
	{
		return "<input name='JamMulaiRencana' type='time' value='$value'>";
	}
	
	function field_callback_JamSelesaiRencana($value='',$primary_key=null)
	{
		return "<input name='JamSelesaiRencana' type='time' value='$value'>";
	}
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */