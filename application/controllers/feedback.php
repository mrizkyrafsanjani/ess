<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	class Feedback extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->library('email');
		}
		
		 function index()
		 {
		   //This method will have the credentials validation
			if($this->session->userdata('logged_in')){
				$this->template->load('cetak','feedback_view',null);				
			}else{
				redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
			}
		 }
	}
?>