<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class URFbySystemController extends CI_Controller{
		var $npkLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('user','',TRUE);
			$session_data = $this->session->userdata('logged_in');
            //require_once(APPPATH. 'libraries/phpGrid_Lite/conf.php');
			$this->npkLogin = $session_data['npk'];
			$this->load->library('grocery_crud');
			$this->load->helper('string');
			$this->load->library('email');
			$this->load->model('usertask','',TRUE);
		}
		
		function index() //untuk input
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("309"))
					{
						$data = array(
							'title' => 'Create URF',
							'departemen' => $this->user->get_departemen_by_npk($this->npkLogin)
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','URFBySystem/CreateURF_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function createURF(){
			$selectTemplate = strip_tags($this->input->post('selectTemplate'));
			$npk = $this->npkLogin;
			$createdBy = $this->npkLogin;
			$expiredPeriod = null;
			$expiredPeriod = strip_tags($this->input->post('tempDate'));
			$rdbNewUpdate = strip_tags($this->input->post('rdbNewUpdate'));
			$rdbPeriod = strip_tags($this->input->post('rdbPeriod'));
			$rdbAssistant = strip_tags($this->input->post('rdbAssistant'));
			
			$npkURFUser = strip_tags($this->input->post('npkURFUser'));
			$namaPKL = strip_tags($this->input->post('namaPKL'));
			$deptPKL = 'PKL';
			if($npkURFUser != ''){
				$npk = $npkURFUser;
			}else if($namaPKL != ''){
				$query = $this->db->get_where('globalparam',array('Name'=>'AdminHRGA'));
				foreach($query->result() as $row)
				{
					$npk = $row->Value;
				}
			}
			$user = $this->user->dataUser($npk);
			if($user != null){
				$NPKAtasan = ($user[0]->NPKAtasan);
				$atasan = $this->user->dataUser($NPKAtasan);
			}else{
				$this->session->set_flashdata('message', '
				<div class="alert alert-warning">NPK User yang ingin dibuatkan URFnya salah</div>');
				redirect('URFbySystem/URFbySystemController/');
			}
			$Deskripsi = "";
			$UserAplikasi = "";
			$AksesAplikasi = "";
			$Email = "";
			$ItemRequest = "";
			$APPLICATION = "";
			$USERID = "";
			$urfEMAIL = "";
			$OTHERS = "";
			//pastikan tidak ada urf dengan tipe yang sama user yg sama aktif
			$sqlSelect = "SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.StatusApproved = 0
						and m.StatusReceived = 0
						and m.NPK_User = '$npk'
						and d.Template = '$selectTemplate' 
						and d.Deleted = 0";
			$result = $this->db->query($sqlSelect);
			if($result->num_rows() > 0){
				$rs = $result->result();
				$URF_ID = $rs[0]->URF_ID;
				$this->session->set_flashdata('message', '
				<div class="alert alert-warning">Sudah terdapat URF dengan template yang sama yang belum di approve, untuk mengupdate urf sebelumnya, silahkan ke <a href="URFbySystemController/ViewURFUser"> sini</a></div>');
				redirect('URFbySystem/URFbySystemController/');
			}
			$URF_Number = 'URF/'.date('Y').'/'.$this->user->integerToRoman(date('n'));
			$sqlSelect = "SELECT URF_Number from mstrurf where URF_Number LIKE '%$URF_Number%' order by CreatedOn desc limit 1";
			$result = $this->db->query($sqlSelect);
			$lastNumber = 17;
			if($result->num_rows() == 0){
				$lastNumber = 17;
			}else{
				$currentURF_Number=$result->result()[0]->URF_Number;
				$lastNumber = explode('/', $currentURF_Number);
				$lastNumber = $lastNumber[3];
			}
			$lastNumber++;
			$URF_Number = $URF_Number.'/'.$lastNumber;
			$Npk_DIC_IT = "";
			$query = $this->db->get_where('globalparam',array('Name'=>'DIC_IT'));
			foreach($query->result() as $row)
			{
				$Npk_DIC_IT = $row->Value;
			}
			$dataDIC_IT = $this->user->dataUser($Npk_DIC_IT);
			$Npk_HeadIT = "";
			$query = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
			foreach($query->result() as $row)
			{
				$Npk_HeadIT = $row->Value;
			}
			$dataHeadIT = $this->user->dataUser($Npk_HeadIT);
			if($namaPKL != ""){
				$sqlInsertMstr = "INSERT INTO mstrurf
				(URF_Number,Deleted, NewOrUpdate, NPK_User, NPK_Head, NPK_HeadIT, NPK_DIC_IT, StatusReceived, StatusApproved, StatusResolved, ExpiredPeriod, CreatedOn, CreatedBy) 
				VALUES ('$URF_Number', 0, '$rdbNewUpdate', 'PKL', '$NPKAtasan', '$Npk_HeadIT', '$Npk_DIC_IT', 0, 0, 0, '$expiredPeriod', now(), '$createdBy');
				";
			}
			else{
				$sqlInsertMstr = "INSERT INTO mstrurf
				(URF_Number,Deleted, NewOrUpdate, NPK_User, NPK_Head, NPK_HeadIT, NPK_DIC_IT, StatusReceived, StatusApproved, StatusResolved, ExpiredPeriod, CreatedOn, CreatedBy) 
				VALUES ('$URF_Number', 0, '$rdbNewUpdate', '$npk', '$NPKAtasan', '$Npk_HeadIT', '$Npk_DIC_IT', 0, 0, 0, '$expiredPeriod', now(), '$createdBy');
				";
			}
			$this->db->query($sqlInsertMstr);

			$New = strip_tags($this->input->post('Deskripsi'));
			$Deskripsi = strip_quotes(strip_tags($this->input->post('Deskripsi')));
			$chkAppSiDP = strip_tags($this->input->post('chkAppSiDP'));
			$chkAppsiDAPEN1 = strip_tags($this->input->post('chkAppsiDAPEN1'));
			$chkAppsiDAPEN2 = strip_tags($this->input->post('chkAppsiDAPEN2'));
			$chkAppApisoft = strip_tags($this->input->post('chkAppApisoft'));
			$chkAppCORE = strip_tags($this->input->post('chkAppCORE'));
			$chkAppAccpac = strip_tags($this->input->post('chkAppAccpac'));
			$chkAppSIAP = strip_tags($this->input->post('chkAppSIAP'));
			$chkAppCARE = strip_tags($this->input->post('chkAppCARE'));
			$chkAppOther = strip_tags($this->input->post('chkAppOther'));
			$chkuserIDWindows = strip_tags($this->input->post('chkuserIDWindows'));
			$chkuserIDSiDP = strip_tags($this->input->post('chkuserIDSiDP'));
			$chkuserIDsiDAPEN1 = strip_tags($this->input->post('chkuserIDsiDAPEN1'));
			$chkuserIDsiDAPEN2 = strip_tags($this->input->post('chkuserIDsiDAPEN2'));
			$chkuserIDApisoft = strip_tags($this->input->post('chkuserIDApisoft'));
			$chkuserIDCORE = strip_tags($this->input->post('chkuserIDCORE'));
			$chkuserIDAccpac = strip_tags($this->input->post('chkuserIDAccpac'));
			$chkuserIDSIAP = strip_tags($this->input->post('chkuserIDSIAP'));
			$chkuserIDCARE = strip_tags($this->input->post('chkuserIDCARE'));
			$chkuserIDOther = strip_tags($this->input->post('chkuserIDOther'));
			$chkcreateUserAccount = strip_tags($this->input->post('chkcreateUserAccount'));
			$chkdeleteUserAccount = strip_tags($this->input->post('chkdeleteUserAccount'));
			$chkresetPassword = strip_tags($this->input->post('chkresetPassword'));
			$chkemail_aiastracoid = strip_tags($this->input->post('chkemail_aiastracoid'));
			$chkemail_dapenastracom = strip_tags($this->input->post('chkemail_dapenastracom'));
			$chkemail_dpacoid = strip_tags($this->input->post('chkemail_dpacoid'));
			$chkSoftware = strip_tags($this->input->post('chkSoftware'));
			$chkFile = strip_tags($this->input->post('chkFile'));
			$chkIT = strip_tags($this->input->post('chkIT'));
			$chkInternet = strip_tags($this->input->post('chkInternet'));
			$chkBackup = strip_tags($this->input->post('chkBackup'));
			$chkUpload = strip_tags($this->input->post('chkUpload'));
			$UserID = $chkcreateUserAccount.$chkdeleteUserAccount.$chkresetPassword;
			if($chkAppSiDP != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppSiDP.' ';}
			if($chkAppsiDAPEN1 != "")			{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppsiDAPEN1.' ';}
			if($chkAppsiDAPEN2 != "")			{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppsiDAPEN2.' ';}
			if($chkAppApisoft != "")			{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppApisoft.' ';}
			if($chkAppCORE != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppCORE.' ';}
			if($chkAppAccpac != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppAccpac.' ';}
			if($chkAppSIAP != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppSIAP.' ';}
			if($chkAppCARE != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppCARE.' ';}
			if($chkAppOther != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppOther;}
			if($chkuserIDWindows != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDWindows.' '; }
			if($chkuserIDSiDP != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDSiDP.' '; }
			if($chkuserIDsiDAPEN1 != "")		{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDsiDAPEN1.' '; }
			if($chkuserIDsiDAPEN2 != "")		{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDsiDAPEN2.' '; }
			if($chkuserIDApisoft != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDApisoft.' '; }
			if($chkuserIDCORE != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDCORE.' '; }
			if($chkuserIDAccpac != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDAccpac.' '; }
			if($chkuserIDSIAP != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDSIAP.' '; }
			if($chkuserIDCARE != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDCARE.' '; }
			if($chkuserIDOther != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDOther; }
			if($chkSoftware != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkSoftware.' '; }
			if($chkFile != "") 					{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkFile.' '; }
			if($chkIT != "") 					{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkIT.' '; }
			if($chkInternet != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkInternet.' '; }
			if($chkBackup != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkBackup; }
			if($chkUpload != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkUpload; }
			if($chkemail_aiastracoid != "")		{ $urfEMAIL = "EMAIL"; $Email 		 .= $chkemail_aiastracoid.' '; }
			if($chkemail_dapenastracom != "")	{ $urfEMAIL = "EMAIL"; $Email 		 .= $chkemail_dapenastracom.' '; }
			if($chkemail_dpacoid != "")			{ $urfEMAIL = "EMAIL"; $Email 		 .= $chkemail_dpacoid; }
			
			
			if($namaPKL != ""){
				$sqlInsertTrk = "INSERT INTO dtlurf
					(URF_Number, Deleted, Template, Deskripsi, AksesAplikasi, UserAplikasi, UserID, Email, ItemRequest, CreatedOn, CreatedBy, NamaPKL, DepartemenPKL) 
					VALUES ('$URF_Number', 0, '$selectTemplate', '$Deskripsi', '$AksesAplikasi', '$UserAplikasi', '$UserID', '$Email', '$ItemRequest', now(), '$createdBy', '$namaPKL', '$deptPKL');
				";
			}else{
				$sqlInsertTrk = "INSERT INTO dtlurf
					(URF_Number, Deleted, Template, Deskripsi, AksesAplikasi, UserAplikasi, UserID, Email, ItemRequest, CreatedOn, CreatedBy) 
					VALUES ('$URF_Number', 0, '$selectTemplate', '$Deskripsi', '$AksesAplikasi', '$UserAplikasi', '$UserID', '$Email', '$ItemRequest', now(), '$createdBy');
				";
			}
			$this->db->query($sqlInsertTrk);
			$New = "";
			$Update = "";
			$Permanent = "";
			$Temporary = "";
			if($rdbNewUpdate == "New"){
				$New = $rdbNewUpdate;
			}else{
				$Update = $rdbNewUpdate;
			}
			if($rdbPeriod == "Permanent"){
				$Permanent = $rdbPeriod;
			}else{
				$Temporary = $rdbPeriod;
			}
			
			$var["URF_Number"] = $URF_Number;
			$var["Permanent"] = $Permanent;
			$var["Temporary"] = $Temporary;
			$var["expiredPeriod"] = $expiredPeriod;
			$var["Deskripsi"] = $Deskripsi;
			$var["New"] = $New;
			$var["Update"] = $Update;
			$var["APPLICATION"] = $APPLICATION;
			$var["USERID"] = $USERID;
			$var["OTHERS"] = $OTHERS;
			$var["urfEMAIL"] = $urfEMAIL;
			$var["chkAppSiDP"] = $chkAppSiDP;
			$var["chkAppsiDAPEN1"] = $chkAppsiDAPEN1;
			$var["chkAppsiDAPEN2"] = $chkAppsiDAPEN2;
			$var["chkAppApisoft"] = $chkAppApisoft;
			$var["chkAppCORE"] = $chkAppCORE;
			$var["chkAppAccpac"] = $chkAppAccpac;
			$var["chkAppSIAP"] = $chkAppSIAP;
			$var["chkAppCARE"] = $chkAppCARE;
			$var["chkAppOther"] = $chkAppOther;
			$var["chkuserIDWindows"] = $chkuserIDWindows;
			$var["chkuserIDSiDP"] = $chkuserIDSiDP;
			$var["chkuserIDsiDAPEN1"] = $chkuserIDsiDAPEN1;
			$var["chkuserIDsiDAPEN2"] = $chkuserIDsiDAPEN2;
			$var["chkuserIDApisoft"] = $chkuserIDApisoft;
			$var["chkuserIDCORE"] = $chkuserIDCORE;
			$var["chkuserIDAccpac"] = $chkuserIDAccpac;
			$var["chkuserIDSIAP"] = $chkuserIDSIAP;
			$var["chkuserIDCARE"] = $chkuserIDCARE;
			$var["chkuserIDOther"] = $chkuserIDOther;
			$var["chkcreateUserAccount"] = $chkcreateUserAccount;
			$var["chkdeleteUserAccount"] = $chkdeleteUserAccount;
			$var["chkresetPassword"] = $chkresetPassword;
			$var["chkSoftware"] = $chkSoftware;
			$var["chkFile"] = $chkFile;
			$var["chkIT"] = $chkIT;
			$var["chkInternet"] = $chkInternet;
			$var["chkBackup"] = $chkBackup;
			$var["chkUpload"] = $chkUpload;
			$var["chkemail_aiastracoid"] = $chkemail_aiastracoid;
			$var["chkemail_dapenastracom"] = $chkemail_dapenastracom;
			$var["chkemail_dpacoid"] = $chkemail_dpacoid;
			$var['URL_image'] = "../../../assets/images/logoDPA.png";
			//prod : 2 .. ..
			//dev : 3.. ..
			if($namaPKL != ''){
				$var["namaUser"] = $namaPKL;
				$var["jabatanUser"] = "PKL";
				$var["departemenUser"] = 'PKL';
				$var["emailUser"] = '';
				$var["extUser"] = '';
				$var["DPA"] = '';
			}else{
				$var["namaUser"] = $user[0]->nama;
				$var["jabatanUser"] = $user[0]->jabatan;
				$var["departemenUser"] = $user[0]->departemen;
				$var["emailUser"] = $user[0]->email;
				$var["extUser"] = $user[0]->noExt;
				$var["DPA"] = $user[0]->DPA;
			}
			if($user[0]->DPA == 1){
				$var["DPA"] = 'SATU';
			}else if($user[0]->DPA == 2){
				$var["DPA"] = 'DUA';
			}else{
				$var["DPA"] = '';
			}
			$var["namaAtasan"] = $atasan[0]->nama;
			$var["jabatanAtasan"] = $atasan[0]->jabatan;
			$var["emailAtasan"] = $atasan[0]->email;
			$var["departemenAtasan"] = $atasan[0]->departemen;
			$var["extAtasan"] = $atasan[0]->noExt;
			$var["namaHeadIT"] = $dataHeadIT[0]->nama;
			$var["jabatanHeadIT"] = $dataHeadIT[0]->jabatan;
			$var["namaDICIT"] = $dataDIC_IT[0]->nama;
			$var["jabatanDICIT"] = $dataDIC_IT[0]->jabatan;
			$this->load->view('URFBySystem/CetakURF.php', $var);
			//redirect('URFbySystem/URFbySystemController/ViewURFUser/');
		}
		function ViewURFUser() //user
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("312"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						
						$sqlSelect = 
						"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.CreatedBy = '$this->npkLogin' 
						and m.Deleted = 0
						and d.Deleted = 0
						order by m.URF_Number desc
						limit 0, 5
						";
						$data = $this->db->query($sqlSelect)->result();
						$dataURF = array(
							'title'=> 'Data URF',
							'data' => $data
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','URFBySystem/URFUser_view',$dataURF);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ViewURFAdmin($Filter = 'NeedAction') //user
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("311"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						$sqlSelect = "";
						if($Filter == 'All'){
							$sqlSelect = 
							"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
							where 
							m.Deleted = 0
							and d.Deleted = 0
							";
						}else if($Filter == 'NeedAction'){
							$sqlSelect = 
							"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
							where 
							m.Deleted = 0
							and d.Deleted = 0
							and 
							(m.StatusReceived = 0
							or 
								(m.StatusReceived = 1
								and m.StatusApproved = 0)
							or
								(m.StatusReceived = 1
								and m.StatusApproved = 1
								and m.StatusResolved = 0)
							or
								(m.StatusReceived = 1
								and m.StatusApproved = 1
								and m.StatusResolved = 1)
							)
							";
						}else if($Filter == 'NotReceived'){
							$sqlSelect = 
							"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
							where 
							m.Deleted = 0
							and d.Deleted = 0
							and m.StatusReceived = 0
							";
						}else if($Filter == 'OnReview'){
							$sqlSelect = 
							"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
							where 
							m.Deleted = 0
							and d.Deleted = 0
							and m.StatusReceived = 1
							and m.StatusApproved = 0
							";
						}else if($Filter == 'Waiting'){
							$sqlSelect = 
							"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
							where 
							m.Deleted = 0
							and d.Deleted = 0
							and m.StatusReceived = 1
							and m.StatusApproved = 1
							and m.StatusResolved = 0
							";
						}else if($Filter == 'Declined'){
							$sqlSelect = 
							"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
							where 
							m.Deleted = 0
							and d.Deleted = 0
							and m.StatusReceived = 2
							";
						}else if($Filter == 'Resolved'){
							$sqlSelect = 
							"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
							where 
							m.Deleted = 0
							and d.Deleted = 0
							and m.StatusReceived = 1
							and m.StatusApproved = 1
							and m.StatusResolved != 0
							";
						}else if($Filter == 'NeedRevert'){
							$sqlSelect = 
							"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
							where 
							m.Deleted = 0
							and d.Deleted = 0
							and m.StatusReceived = 1
							and m.StatusApproved = 1
							and m.StatusResolved = 1
							";
						}else{
							$sqlSelect = 
							"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
							where 
							m.Deleted = 0
							and d.Deleted = 0
							";
						}
						$sqlSelect .= " order by m.CreatedOn desc";
						$data = $this->db->query($sqlSelect)->result();
						$dataURF = array(
							'title'=> 'Data URF',
							'data' => $data,
							'status' => $Filter
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','URFBySystem/URFAdmin_view',$dataURF);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function Ubah($URF_ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("309"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						
						$sqlSelect = 
						"SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.CreatedBy = '$this->npkLogin'
						and m.URF_ID = '$URF_ID'
						and m.Deleted = 0
						and d.Deleted = 0
						";
						$data = $this->db->query($sqlSelect);
						if($data->num_rows() == 0){
							redirect("URFbySystem/URFbySystemController/ViewURFUser",'refresh');
						}else{
							$dataURF = array(
								'title'=> 'Ubah Data URF',
								'data' => $data->result(),
								'departemen' => $this->user->get_departemen_by_npk($this->npkLogin)
							);
							$this->load->helper(array('form','url'));
							$this->template->load('default','URFBySystem/UpdateURF_view', $dataURF);
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function UpdateURF($URF_ID = ''){
			$URF_ID = $URF_ID;
			$selectTemplate = strip_tags($this->input->post('selectTemplate'));
			$createdBy = $this->npkLogin;
			$npk = $this->npkLogin;
			$npkURFUser = strip_tags($this->input->post('npkURFUser'));
			$namaPKL = strip_tags($this->input->post('namaPKL'));
			$deptPKL = 'PKL';
			if($npkURFUser != ''){
				$npk = $npkURFUser;
			}else if($namaPKL != ''){
				$query = $this->db->get_where('globalparam',array('Name'=>'AdminHRGA'));
				foreach($query->result() as $row)
				{
					$npk = $row->Value;
				}
			}
			$user = $this->user->dataUser($npk);
			$NPKAtasan = ($user[0]->NPKAtasan);
			$atasan = $this->user->dataUser($NPKAtasan);
			$expiredPeriod = null;
			$expiredPeriod = strip_tags($this->input->post('tempDate'));
			$rdbNewUpdate = strip_tags($this->input->post('rdbNewUpdate'));
			$rdbPeriod = strip_tags($this->input->post('rdbPeriod'));
			$Deskripsi = "";
			$UserAplikasi = "";
			$AksesAplikasi = "";
			$Email = "";
			$ItemRequest = "";
			$APPLICATION = "";
			$USERID = "";
			$urfEMAIL = "";
			$OTHERS = "";
			$sqlSelect = "SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.StatusApproved = 0
						and m.StatusReceived = 0
						and m.URF_ID = $URF_ID
						and m.Deleted = 0
						and d.Deleted = 0
						and m.createdBy = '$createdBy'";
			$result = $this->db->query($sqlSelect);
			if($result->num_rows() > 0){
				
				$currentURF = $result->result();
				$URF_Number = $currentURF[0]->URF_Number;
				$c = "";
				$query = $this->db->get_where('globalparam',array('Name'=>'DIC_IT'));
				foreach($query->result() as $row)
				{
					$Npk_DIC_IT = $row->Value;
				}
			$dataDIC_IT = $this->user->dataUser($Npk_DIC_IT);
				$Npk_HeadIT = "";
				$query = $this->db->get_where('globalparam',array('Name'=>'HeadIT'));
				foreach($query->result() as $row)
				{
					$Npk_HeadIT = $row->Value;
				}
			$dataHeadIT = $this->user->dataUser($Npk_HeadIT);
				$sqlUpdateMstr = 
					"UPDATE mstrurf
					SET
					   NewOrUpdate = '$rdbNewUpdate'
					  ,NPK_User = '$npk'
					  ,NPK_Head = '$NPKAtasan'
					  ,NPK_HeadIT = '$Npk_HeadIT'
					  ,NPK_DIC_IT = '$Npk_DIC_IT'
					  ,StatusReceived = 0
					  ,StatusApproved = 0
					  ,StatusResolved = 0
					  ,ExpiredPeriod = '$expiredPeriod'
					  ,UpdatedOn = now()
					  ,UpdatedBy = '$createdBy'
					WHERE URF_ID = $URF_ID
					and URF_Number = '$URF_Number'";
				$this->db->query($sqlUpdateMstr);
					
					$New = strip_tags($this->input->post('Deskripsi'));
					$Deskripsi = strip_tags($this->input->post('Deskripsi'));
					$chkAppSiDP = strip_tags($this->input->post('chkAppSiDP'));
					$chkAppsiDAPEN1 = strip_tags($this->input->post('chkAppsiDAPEN1'));
					$chkAppsiDAPEN2 = strip_tags($this->input->post('chkAppsiDAPEN2'));
					$chkAppApisoft = strip_tags($this->input->post('chkAppApisoft'));
					$chkAppCORE = strip_tags($this->input->post('chkAppCORE'));
					$chkAppAccpac = strip_tags($this->input->post('chkAppAccpac'));
					$chkAppSIAP = strip_tags($this->input->post('chkAppSIAP'));
					$chkAppCARE = strip_tags($this->input->post('chkAppCARE'));
					$chkAppOther = strip_tags($this->input->post('chkAppOther'));
					$chkuserIDWindows = strip_tags($this->input->post('chkuserIDWindows'));
					$chkuserIDSiDP = strip_tags($this->input->post('chkuserIDSiDP'));
					$chkuserIDsiDAPEN1 = strip_tags($this->input->post('chkuserIDsiDAPEN1'));
					$chkuserIDsiDAPEN2 = strip_tags($this->input->post('chkuserIDsiDAPEN2'));
					$chkuserIDApisoft = strip_tags($this->input->post('chkuserIDApisoft'));
					$chkuserIDCORE = strip_tags($this->input->post('chkuserIDCORE'));
					$chkuserIDAccpac = strip_tags($this->input->post('chkuserIDAccpac'));
					$chkuserIDSIAP = strip_tags($this->input->post('chkuserIDSIAP'));
					$chkuserIDCARE = strip_tags($this->input->post('chkuserIDCARE'));
					$chkuserIDOther = strip_tags($this->input->post('chkuserIDOther'));
					$chkcreateUserAccount = strip_tags($this->input->post('chkcreateUserAccount'));
					$chkdeleteUserAccount = strip_tags($this->input->post('chkdeleteUserAccount'));
					$chkresetPassword = strip_tags($this->input->post('chkresetPassword'));
					$chkemail_aiastracoid = strip_tags($this->input->post('chkemail_aiastracoid'));
					$chkemail_dapenastracom = strip_tags($this->input->post('chkemail_dapenastracom'));
					$chkemail_dpacoid = strip_tags($this->input->post('chkemail_dpacoid'));
					$chkSoftware = strip_tags($this->input->post('chkSoftware'));
					$chkFile = strip_tags($this->input->post('chkFile'));
					$chkIT = strip_tags($this->input->post('chkIT'));
					$chkInternet = strip_tags($this->input->post('chkInternet'));
					$chkBackup = strip_tags($this->input->post('chkBackup'));
					$chkUpload = strip_tags($this->input->post('chkUpload'));
					$UserID = $chkcreateUserAccount.$chkdeleteUserAccount.$chkresetPassword;
					if($chkAppSiDP != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppSiDP.' ';}
					if($chkAppsiDAPEN1 != "")			{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppsiDAPEN1.' ';}
					if($chkAppsiDAPEN2 != "")			{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppsiDAPEN2.' ';}
					if($chkAppApisoft != "")			{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppApisoft.' ';}
					if($chkAppCORE != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppCORE.' ';}
					if($chkAppAccpac != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppAccpac.' ';}
					if($chkAppSIAP != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppSIAP.' ';}
					if($chkAppCARE != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppCARE.' ';}
					if($chkAppOther != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppOther;}
					if($chkuserIDWindows != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDWindows.' '; }
					if($chkuserIDSiDP != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDSiDP.' '; }
					if($chkuserIDsiDAPEN1 != "")		{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDsiDAPEN1.' '; }
					if($chkuserIDsiDAPEN2 != "")		{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDsiDAPEN2.' '; }
					if($chkuserIDApisoft != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDApisoft.' '; }
					if($chkuserIDCORE != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDCORE.' '; }
					if($chkuserIDAccpac != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDAccpac.' '; }
					if($chkuserIDSIAP != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDSIAP.' '; }
					if($chkuserIDCARE != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDCARE.' '; }
					if($chkuserIDOther != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDOther; }
					if($chkSoftware != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkSoftware.' '; }
					if($chkFile != "") 					{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkFile.' '; }
					if($chkIT != "") 					{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkIT.' '; }
					if($chkInternet != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkInternet.' '; }
					if($chkBackup != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkBackup; }
					if($chkUpload != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkUpload; }
					if($chkemail_aiastracoid != "")		{ $urfEMAIL = "EMAIL"; $Email 		 .= $chkemail_aiastracoid.' '; }
					if($chkemail_dapenastracom != "")	{ $urfEMAIL = "EMAIL"; $Email 		 .= $chkemail_dapenastracom.' '; }
					if($chkemail_dpacoid != "")			{ $urfEMAIL = "EMAIL"; $Email 		 .= $chkemail_dpacoid; }
					
				$sqlUpdateDtl = "UPDATE dtlurf 
				SET Deleted = 1 , UpdatedOn = now(), UpdatedBy = '$createdBy' 
				WHERE URF_Number = '$URF_Number'";
				$this->db->query($sqlUpdateDtl);

			
				if($namaPKL != ""){
					$sqlInsertTrk = "INSERT INTO dtlurf
						(URF_Number, Deleted, Template, Deskripsi, AksesAplikasi, UserAplikasi, UserID, Email, ItemRequest, CreatedOn, CreatedBy, NamaPKL, DepartemenPKL) 
						VALUES ('$URF_Number', 0, '$selectTemplate', '$Deskripsi', '$AksesAplikasi', '$UserAplikasi', '$UserID', '$Email', '$ItemRequest', now(), '$createdBy', '$namaPKL', '$deptPKL');
					";
				}else{
					$sqlInsertTrk = "INSERT INTO dtlurf
						(URF_Number, Deleted, Template, Deskripsi, AksesAplikasi, UserAplikasi, UserID, Email, ItemRequest, CreatedOn, CreatedBy) 
						VALUES ('$URF_Number', 0, '$selectTemplate', '$Deskripsi', '$AksesAplikasi', '$UserAplikasi', '$UserID', '$Email', '$ItemRequest', now(), '$createdBy');
					";
				}
				$this->db->query($sqlInsertTrk);

				$New = "";
				$Update = "";
				$Permanent = "";
				$Temporary = "";
				if($rdbNewUpdate == "New"){
					$New = $rdbNewUpdate;
				}else{
					$Update = $rdbNewUpdate;
				}
				if($rdbPeriod == "Permanent"){
					$Permanent = $rdbPeriod;
				}else{
					$Temporary = $rdbPeriod;
				}

				$var["URF_Number"] = $URF_Number;
				$var["Permanent"] = $Permanent;
				$var["Temporary"] = $Temporary;
				$var["expiredPeriod"] = $expiredPeriod;
				$var["Deskripsi"] = $Deskripsi;
				$var["New"] = $New;
				$var["Update"] = $Update;
				$var["APPLICATION"] = $APPLICATION;
				$var["USERID"] = $USERID;
				$var["OTHERS"] = $OTHERS;
				$var["urfEMAIL"] = $urfEMAIL;
				$var["chkAppSiDP"] = $chkAppSiDP;
				$var["chkAppsiDAPEN1"] = $chkAppsiDAPEN1;
				$var["chkAppsiDAPEN2"] = $chkAppsiDAPEN2;
				$var["chkAppApisoft"] = $chkAppApisoft;
				$var["chkAppCORE"] = $chkAppCORE;
				$var["chkAppAccpac"] = $chkAppAccpac;
				$var["chkAppSIAP"] = $chkAppSIAP;
				$var["chkAppCARE"] = $chkAppCARE;
				$var["chkAppOther"] = $chkAppOther;
				$var["chkuserIDWindows"] = $chkuserIDWindows;
				$var["chkuserIDSiDP"] = $chkuserIDSiDP;
				$var["chkuserIDsiDAPEN1"] = $chkuserIDsiDAPEN1;
				$var["chkuserIDsiDAPEN2"] = $chkuserIDsiDAPEN2;
				$var["chkuserIDApisoft"] = $chkuserIDApisoft;
				$var["chkuserIDCORE"] = $chkuserIDCORE;
				$var["chkuserIDAccpac"] = $chkuserIDAccpac;
				$var["chkuserIDSIAP"] = $chkuserIDSIAP;
				$var["chkuserIDCARE"] = $chkuserIDCARE;
				$var["chkuserIDOther"] = $chkuserIDOther;
				$var["chkcreateUserAccount"] = $chkcreateUserAccount;
				$var["chkdeleteUserAccount"] = $chkdeleteUserAccount;
				$var["chkresetPassword"] = $chkresetPassword;
				$var["chkSoftware"] = $chkSoftware;
				$var["chkFile"] = $chkFile;
				$var["chkIT"] = $chkIT;
				$var["chkInternet"] = $chkInternet;
				$var["chkBackup"] = $chkBackup;
				$var["chkUpload"] = $chkUpload;
				$var["chkemail_aiastracoid"] = $chkemail_aiastracoid;
				$var["chkemail_dapenastracom"] = $chkemail_dapenastracom;
				$var["chkemail_dpacoid"] = $chkemail_dpacoid;
				$var['URL_image'] = "../../../../assets/images/logoDPA.png";
				if($namaPKL != ''){
					$var["namaUser"] = $namaPKL;
					$var["jabatanUser"] = "PKL";
					$var["departemenUser"] = 'PKL';
					$var["emailUser"] = '';
					$var["extUser"] = '';
					$var["DPA"] = '';
				}else{
					$var["namaUser"] = $user[0]->nama;
					$var["jabatanUser"] = $user[0]->jabatan;
					$var["departemenUser"] = $user[0]->departemen;
					$var["emailUser"] = $user[0]->email;
					$var["extUser"] = $user[0]->noExt;
					if($user[0]->DPA == 1){
						$var["DPA"] = 'SATU';
					}else if($user[0]->DPA == 2){
						$var["DPA"] = 'DUA';
					}else{
						$var["DPA"] = '';
					}
				}
				$var["namaAtasan"] = $atasan[0]->nama;
				$var["jabatanAtasan"] = $atasan[0]->jabatan;
				$var["emailAtasan"] = $atasan[0]->email;
				$var["departemenAtasan"] = $atasan[0]->departemen;
				$var["extAtasan"] = $atasan[0]->noExt;
				$var["namaHeadIT"] = $dataHeadIT[0]->nama;
				$var["jabatanHeadIT"] = $dataHeadIT[0]->jabatan;
				$var["namaDICIT"] = $dataDIC_IT[0]->nama;
				$var["jabatanDICIT"] = $dataDIC_IT[0]->jabatan;
				$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
				$this->email->to('gerry.gabriel@dpa.co.id'); 
				
				$this->email->subject('URF Status');
				$message = 'URF dengan Nomor '.$URF_Number.' sudah diupdate oleh User, mohon di cek kembali';
				$this->email->message($message);
				$this->email->send();
				$this->load->view('URFBySystem/CetakURF.php', $var);
			}else{
				$this->session->set_flashdata('message', '
				<label id="#error">Status URF Sudah diterima oleh IT, mohon hubungi IT untuk membatalkan URF ini</label>');
				redirect('URFbySystem/URFbySystemController/');
			}

		}
		/*function Reprint($URF_ID = ''){
			$URF_ID = $URF_ID;
			$createdBy = $this->npkLogin;
			$npk = $this->npkLogin;
			
			$sqlSelect = "SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.StatusApproved = 0
						and m.StatusReceived = 0
						and m.URF_ID = $URF_ID
						and m.Deleted = 0
						and d.Deleted = 0
						and m.createdBy = '$createdBy'";
			$result = $this->db->query($sqlSelect);
			if($result->num_rows() > 0){
				$rs = $result->result();
				$selectTemplate = $rs[0]->Template;
				$npkURFUser = $rs[0]->NPK_User;
				$namaPKL = $rs[0]->NamaPKL;
				$deptPKL = 'PKL';
				if($npkURFUser != $createdBy){
					$npk = $npkURFUser;
				}else if($namaPKL != ''){
					$query = $this->db->get_where('globalparam',array('Name'=>'AdminHRGA'));
					foreach($query->result() as $row)
					{
						$npk = $row->Value;
					}
				}
				$user = $this->user->dataUser($npk);
				$NPKAtasan = ($user[0]->NPKAtasan);
				$atasan = $this->user->dataUser($NPKAtasan);
				$expiredPeriod = null;
				$expiredPeriod = $rs[0]->ExpiredPeriod;
				$rdbNewUpdate = $rs[0]->NewOrUpdate;
				if($expiredPeriod != null){
					$rdbPeriod = "Permanent";
				}else{
					$rdbPeriod = "Temporary";
				}
				$Deskripsi = "";
				$UserAplikasi = "";
				$AksesAplikasi = "";
				$Email = "";
				$ItemRequest = "";
				$APPLICATION = "";
				$USERID = "";
				$urfEMAIL = "";
				$OTHERS = "";
				$URF_Number = $rs[0]->URF_Number;
				$New = strip_tags($this->input->post('rdbNewUpdate'));
				$Deskripsi = strip_tags($this->input->post('Deskripsi'));
				$chkAppSiDP = strip_tags($this->input->post('chkAppSiDP'));
				$chkAppsiDAPEN1 = strip_tags($this->input->post('chkAppsiDAPEN1'));
				$chkAppsiDAPEN2 = strip_tags($this->input->post('chkAppsiDAPEN2'));
				$chkAppApisoft = strip_tags($this->input->post('chkAppApisoft'));
				$chkAppCORE = strip_tags($this->input->post('chkAppCORE'));
				$chkAppAccpac = strip_tags($this->input->post('chkAppAccpac'));
				$chkAppSIAP = strip_tags($this->input->post('chkAppSIAP'));
				$chkAppCARE = strip_tags($this->input->post('chkAppCARE'));
				$chkAppOther = strip_tags($this->input->post('chkAppOther'));
				$chkuserIDWindows = strip_tags($this->input->post('chkuserIDWindows'));
				$chkuserIDSiDP = strip_tags($this->input->post('chkuserIDSiDP'));
				$chkuserIDsiDAPEN1 = strip_tags($this->input->post('chkuserIDsiDAPEN1'));
				$chkuserIDsiDAPEN2 = strip_tags($this->input->post('chkuserIDsiDAPEN2'));
				$chkuserIDApisoft = strip_tags($this->input->post('chkuserIDApisoft'));
				$chkuserIDCORE = strip_tags($this->input->post('chkuserIDCORE'));
				$chkuserIDAccpac = strip_tags($this->input->post('chkuserIDAccpac'));
				$chkuserIDSIAP = strip_tags($this->input->post('chkuserIDSIAP'));
				$chkuserIDCARE = strip_tags($this->input->post('chkuserIDCARE'));
				$chkuserIDOther = strip_tags($this->input->post('chkuserIDOther'));
				$chkcreateUserAccount = strip_tags($this->input->post('chkcreateUserAccount'));
				$chkdeleteUserAccount = strip_tags($this->input->post('chkdeleteUserAccount'));
				$chkresetPassword = strip_tags($this->input->post('chkresetPassword'));
				$chkemail_aiastracoid = strip_tags($this->input->post('chkemail_aiastracoid'));
				$chkemail_dapenastracom = strip_tags($this->input->post('chkemail_dapenastracom'));
				$chkemail_dpacoid = strip_tags($this->input->post('chkemail_dpacoid'));
				$chkSoftware = strip_tags($this->input->post('chkSoftware'));
				$chkFile = strip_tags($this->input->post('chkFile'));
				$chkIT = strip_tags($this->input->post('chkIT'));
				$chkInternet = strip_tags($this->input->post('chkInternet'));
				$chkBackup = strip_tags($this->input->post('chkBackup'));
				$chkUpload = strip_tags($this->input->post('chkUpload'));
				$UserID = $chkcreateUserAccount.$chkdeleteUserAccount.$chkresetPassword;
				if($chkAppSiDP != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppSiDP.' ';}
				if($chkAppsiDAPEN1 != "")			{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppsiDAPEN1.' ';}
				if($chkAppsiDAPEN2 != "")			{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppsiDAPEN2.' ';}
				if($chkAppApisoft != "")			{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppApisoft.' ';}
				if($chkAppCORE != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppCORE.' ';}
				if($chkAppAccpac != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppAccpac.' ';}
				if($chkAppSIAP != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppSIAP.' ';}
				if($chkAppCARE != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppCARE.' ';}
				if($chkAppOther != "")				{ $APPLICATION = "APP"; $AksesAplikasi .= $chkAppOther;}
				if($chkuserIDWindows != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDWindows.' '; }
				if($chkuserIDSiDP != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDSiDP.' '; }
				if($chkuserIDsiDAPEN1 != "")		{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDsiDAPEN1.' '; }
				if($chkuserIDsiDAPEN2 != "")		{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDsiDAPEN2.' '; }
				if($chkuserIDApisoft != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDApisoft.' '; }
				if($chkuserIDCORE != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDCORE.' '; }
				if($chkuserIDAccpac != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDAccpac.' '; }
				if($chkuserIDSIAP != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDSIAP.' '; }
				if($chkuserIDCARE != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDCARE.' '; }
				if($chkuserIDOther != "")			{ $USERID = "USERID"; $UserAplikasi  .= $chkuserIDOther; }
				if($chkSoftware != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkSoftware.' '; }
				if($chkFile != "") 					{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkFile.' '; }
				if($chkIT != "") 					{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkIT.' '; }
				if($chkInternet != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkInternet.' '; }
				if($chkBackup != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkBackup; }
				if($chkUpload != "") 				{ $OTHERS = "OTHERS"; $ItemRequest 	 .= $chkUpload; }
				if($chkemail_aiastracoid != "")		{ $urfEMAIL = "EMAIL"; $Email 		 .= $chkemail_aiastracoid.' '; }
				if($chkemail_dapenastracom != "")	{ $urfEMAIL = "EMAIL"; $Email 		 .= $chkemail_dapenastracom.' '; }
				if($chkemail_dpacoid != "")			{ $urfEMAIL = "EMAIL"; $Email 		 .= $chkemail_dpacoid; }
					
				$sqlUpdateDtl = "UPDATE dtlurf 
				SET Deleted = 1 , UpdatedOn = now(), UpdatedBy = '$createdBy' 
				WHERE URF_Number = '$URF_Number'";
				$this->db->query($sqlUpdateDtl);

			
				if($namaPKL != ""){
					$sqlInsertTrk = "INSERT INTO dtlurf
						(URF_Number, Deleted, Template, Deskripsi, AksesAplikasi, UserAplikasi, UserID, Email, ItemRequest, CreatedOn, CreatedBy, NamaPKL, DepartemenPKL) 
						VALUES ('$URF_Number', 0, '$selectTemplate', '$Deskripsi', '$AksesAplikasi', '$UserAplikasi', '$UserID', '$Email', '$ItemRequest', now(), '$createdBy', '$namaPKL', '$deptPKL');
					";
				}else{
					$sqlInsertTrk = "INSERT INTO dtlurf
						(URF_Number, Deleted, Template, Deskripsi, AksesAplikasi, UserAplikasi, UserID, Email, ItemRequest, CreatedOn, CreatedBy) 
						VALUES ('$URF_Number', 0, '$selectTemplate', '$Deskripsi', '$AksesAplikasi', '$UserAplikasi', '$UserID', '$Email', '$ItemRequest', now(), '$createdBy');
					";
				}
				$this->db->query($sqlInsertTrk);

				$New = "";
				$Update = "";
				$Permanent = "";
				$Temporary = "";
				if($rdbNewUpdate == "New"){
					$New = $rdbNewUpdate;
				}else{
					$Update = $rdbNewUpdate;
				}
				if($rdbPeriod == "Permanent"){
					$Permanent = $rdbPeriod;
				}else{
					$Temporary = $rdbPeriod;
				}

				$var["URF_Number"] = $URF_Number;
				$var["Permanent"] = $Permanent;
				$var["Temporary"] = $Temporary;
				$var["expiredPeriod"] = $expiredPeriod;
				$var["Deskripsi"] = $Deskripsi;
				$var["New"] = $New;
				$var["Update"] = $Update;
				$var["APPLICATION"] = $APPLICATION;
				$var["USERID"] = $USERID;
				$var["OTHERS"] = $OTHERS;
				$var["urfEMAIL"] = $urfEMAIL;
				$var["chkAppSiDP"] = $chkAppSiDP;
				$var["chkAppsiDAPEN1"] = $chkAppsiDAPEN1;
				$var["chkAppsiDAPEN2"] = $chkAppsiDAPEN2;
				$var["chkAppApisoft"] = $chkAppApisoft;
				$var["chkAppCORE"] = $chkAppCORE;
				$var["chkAppAccpac"] = $chkAppAccpac;
				$var["chkAppSIAP"] = $chkAppSIAP;
				$var["chkAppCARE"] = $chkAppCARE;
				$var["chkAppOther"] = $chkAppOther;
				$var["chkuserIDWindows"] = $chkuserIDWindows;
				$var["chkuserIDSiDP"] = $chkuserIDSiDP;
				$var["chkuserIDsiDAPEN1"] = $chkuserIDsiDAPEN1;
				$var["chkuserIDsiDAPEN2"] = $chkuserIDsiDAPEN2;
				$var["chkuserIDApisoft"] = $chkuserIDApisoft;
				$var["chkuserIDCORE"] = $chkuserIDCORE;
				$var["chkuserIDAccpac"] = $chkuserIDAccpac;
				$var["chkuserIDSIAP"] = $chkuserIDSIAP;
				$var["chkuserIDCARE"] = $chkuserIDCARE;
				$var["chkuserIDOther"] = $chkuserIDOther;
				$var["chkcreateUserAccount"] = $chkcreateUserAccount;
				$var["chkdeleteUserAccount"] = $chkdeleteUserAccount;
				$var["chkresetPassword"] = $chkresetPassword;
				$var["chkSoftware"] = $chkSoftware;
				$var["chkFile"] = $chkFile;
				$var["chkIT"] = $chkIT;
				$var["chkInternet"] = $chkInternet;
				$var["chkBackup"] = $chkBackup;
				$var["chkUpload"] = $chkUpload;
				$var["chkemail_aiastracoid"] = $chkemail_aiastracoid;
				$var["chkemail_dapenastracom"] = $chkemail_dapenastracom;
				$var["chkemail_dpacoid"] = $chkemail_dpacoid;
				$var['URL_image'] = "../../../../assets/images/logoDPA.png";
				if($namaPKL != ''){
					$var["namaUser"] = $namaPKL;
					$var["jabatanUser"] = "PKL";
					$var["departemenUser"] = 'PKL';
					$var["emailUser"] = '';
					$var["extUser"] = '';
					$var["DPA"] = '';
				}else{
					$var["namaUser"] = $user[0]->nama;
					$var["jabatanUser"] = $user[0]->jabatan;
					$var["departemenUser"] = $user[0]->departemen;
					$var["emailUser"] = $user[0]->email;
					$var["extUser"] = $user[0]->noExt;
					if($user[0]->DPA == 1){
						$var["DPA"] = 'SATU';
					}else if($user[0]->DPA == 2){
						$var["DPA"] = 'DUA';
					}else{
						$var["DPA"] = '';
					}
				}
				$var["namaAtasan"] = $atasan[0]->nama;
				$var["jabatanAtasan"] = $atasan[0]->jabatan;
				$var["emailAtasan"] = $atasan[0]->email;
				$var["departemenAtasan"] = $atasan[0]->departemen;
				$var["extAtasan"] = $atasan[0]->noExt;
				$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
				$this->email->to('gerry.gabriel@dpa.co.id'); 
				
				$this->email->subject('URF Status');
				$message = 'URF dengan Nomor '.$URF_Number.' sudah diupdate oleh User, mohon di cek kembali';
				$this->email->message($message);
				$this->email->send();
				$this->load->view('URFBySystem/CetakURF.php', $var);
			}else{
				$this->session->set_flashdata('message', '
				<label id="#error">Status URF Sudah diterima oleh IT, mohon hubungi IT untuk membatalkan URF ini</label>');
				redirect('URFbySystem/URFbySystemController/');
			}

		}*/
		function Batal($URF_ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("309"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						
						$sqlSelect = 
						"SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.CreatedBy = '$this->npkLogin'
						and m.URF_ID = '$URF_ID'
						and m.Deleted = 0
						and d.Deleted = 0
						and m.StatusReceived != 2
						and m.StatusApproved = 0
						and m.StatusResolved = 0
						";
						$data = $this->db->query($sqlSelect);
						if($data->num_rows() == 0){
							$this->session->set_flashdata('message', '
							<div class="alert alert-danger">URF sudah pernah di proses sebelumnya, tidak dapat dibatalkan</div>');
							redirect("URFbySystem/URFbySystemController/ViewURFUser",'refresh');
						}else{
							$URF_Number = "";
							foreach ($data->result() as $row)
							{
								$URF_Number = $row->URF_Number;
							}
							$sqlUpdate = "UPDATE mstrurf 
							SET Deleted = 1 , UpdatedOn = now(), UpdatedBy = '$this->npkLogin' 
							WHERE URF_ID = '$URF_ID'
							and URF_Number = '$URF_Number'";
							$this->db->query($sqlUpdate);
							
							$sqlUpdateDtl = "UPDATE dtlurf 
							SET Deleted = 1 , UpdatedOn = now(), UpdatedBy = '$this->npkLogin' 
							WHERE URF_Number = '$URF_Number'";
							$this->db->query($sqlUpdateDtl);
							$user = $this->user->dataUser($NPK_User);
							
							$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
							$this->email->to('it@dpa.co.id'); 
							
							$this->email->subject('URF Status');
							$message = 'URF dengan Nomor '.$URF_Number.' sudah dibatalkan oleh User.';
							$this->email->message($message);	
							$this->email->send();
							
							$this->session->set_flashdata('message', '
							<div class="alert alert-danger">URF Sudah berhasil di batalkan</div>');
							redirect('URFbySystem/URFbySystemController/ViewURFUser/');
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function Receive($URF_ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("311"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						
						$sqlSelect = 
						"SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.URF_ID = '$URF_ID'
						and m.deleted = 0
						and d.deleted = 0
						and m.StatusReceived = 0
						and m.StatusApproved = 0
						and m.StatusResolved = 0
						";
						$data = $this->db->query($sqlSelect);
						if($data->num_rows() == 0){
							$this->session->set_flashdata('message', '
							<div class="alert alert-warning">URF Sudah pernah di Receive sebelumnya</div>');
							redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
						}else{
							$URF_Number = "";
							$NPK_User = "";
							foreach ($data->result() as $row)
							{
								$URF_Number = $row->URF_Number;
								$NPK_User = $row->NPK_User;
							}
							$sqlUpdate = "UPDATE mstrurf 
							SET StatusReceived = 1 , UpdatedOn = now(), UpdatedBy = '$this->npkLogin' 
							WHERE URF_ID = '$URF_ID'
							and URF_Number = '$URF_Number'";
							$this->db->query($sqlUpdate);
							
							$user = $this->user->dataUser($NPK_User);
							if($user)
							{
								$user_target = array();
								foreach($user as $row)
								{
									$user_target = array(
										'npk' => $row->npk,
										'nama' => $row->nama,
										'email' => $row->email
									);
								}
								$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
								$this->email->to($user_target['email']); 
								
								$this->email->subject('URF Status');
								$message = 'URF Anda dengan Nomor '.$URF_Number.' sudah diterima oleh IT, harap menunggu proses review dari IT ';
								$this->email->message($message);	
								
								if($this->email->send()){
									$this->session->set_flashdata('message', '
									<div class="alert alert-success">URF Sudah berhasil di Terima</div>');
									redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
								} else {
									$this->session->set_flashdata('message', '
									<div class="alert alert-success">URF Sudah berhasil di Terima, namun gagal kirim Email</div>');
									redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
								}
							}
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function Approve($URF_ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("311"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						
						$sqlSelect = 
						"SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.URF_ID = '$URF_ID'
						and m.deleted = 0
						and d.deleted = 0
						and m.StatusReceived = 1
						and m.StatusApproved = 0
						";
						$data = $this->db->query($sqlSelect);
						if($data->num_rows() == 0){
							$this->session->set_flashdata('message', '
							<div class="alert alert-warning">URF Sudah pernah di Approve / dibatalkan sebelumnya</div>');
							redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
						}else{
							$URF_Number = "";
							$NPK_User = "";
							foreach ($data->result() as $row)
							{
								$URF_Number = $row->URF_Number;
								$NPK_User = $row->NPK_User;
							}
							$sqlUpdate = "UPDATE mstrurf 
							SET StatusApproved = 1 , UpdatedOn = now(), UpdatedBy = '$this->npkLogin' 
							WHERE URF_ID = '$URF_ID'
							and URF_Number = '$URF_Number'";
							$this->db->query($sqlUpdate);
							$user = $this->user->dataUser($NPK_User);
							if($user)
							{
								$user_target = array();
								foreach($user as $row)
								{
									$user_target = array(
										'npk' => $row->npk,
										'nama' => $row->nama,
										'email' => $row->email
									);
								}
								$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
								$this->email->to($user_target['email']); 
								
								$this->email->subject('URF Status');
								$message = 'URF Anda dengan Nomor '.$URF_Number.' sudah di Approve oleh IT, harap menunggu proses pengerjaan dari IT ';
								$this->email->message($message);	
								
								if($this->email->send()){
									$this->session->set_flashdata('message', '
									<div class="alert alert-success">URF Sudah berhasil di Approve</div>');
									redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
								} else {
									$this->session->set_flashdata('message', '
									<div class="alert alert-success">URF Sudah berhasil di Approve, namun gagal kirim Email</div>');
									redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
								}
							}
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function Decline($URF_ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("311"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						
						$sqlSelect = 
						"SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.URF_ID = '$URF_ID'
						and m.deleted = 0
						and d.deleted = 0
						and m.StatusReceived = 1
						and m.StatusApproved = 0
						";
						$data = $this->db->query($sqlSelect);
						
						if($data->num_rows() == 0){
							$this->session->set_flashdata('message', '
							<label>URF Sudah pernah di Decline sebelumnya / Di Batalkan oleh User</label>');
							redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
						}else{
							$URF_Number = "";
							$NPK_User = "";
							foreach ($data->result() as $row)
							{
								$URF_Number = $row->URF_Number;
								$NPK_User = $row->NPK_User;
							}
							$sqlUpdate = "UPDATE mstrurf 
							SET StatusApproved = 2 , UpdatedOn = now(), UpdatedBy = '$this->npkLogin' 
							WHERE URF_ID = '$URF_ID'
							and URF_Number = '$URF_Number'";
							$this->db->query($sqlUpdate);

							$user = $this->user->dataUser($NPK_User);
							if($user)
							{
								$user_target = array();
								foreach($user as $row)
								{
									$user_target = array(
										'npk' => $row->npk,
										'nama' => $row->nama,
										'email' => $row->email
									);
								}
								$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
								$this->email->to($user_target['email']); 
								
								$this->email->subject('URF Status');
								$message = 'URF Anda dengan Nomor '.$URF_Number.' sudah tolak oleh IT, harap menghubungi IT untuk informasi lebih lanjut ';
								$this->email->message($message);	
								
								
								if($this->email->send()){
									$this->session->set_flashdata('message', '
									<label>URF Sudah berhasil di Decline</label>');
									redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
								} else {
									$this->session->set_flashdata('message', '
									<label>URF Sudah berhasil di Decline, namun gagal kirim Email</label>');
									redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
								}
							}
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function Resolve($URF_ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("311"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						
						$sqlSelect = 
						"SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.URF_ID = '$URF_ID'
						and m.deleted = 0
						and d.deleted = 0
						and m.StatusReceived = 1
						and m.StatusApproved = 1
						and m.StatusResolved = 0
						";
						$data = $this->db->query($sqlSelect);
						if($data->num_rows() == 0){
							$this->session->set_flashdata('message', '
							<div class="alert alert-warning">URF Sudah pernah di Resolve sebelumnya</div>');
							redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
						}else{
							$URF_Number = "";
							$ExpiredPeriod = "";
							$NPK_User = "";
							foreach ($data->result() as $row)
							{
								$URF_Number = $row->URF_Number;
								$ExpiredPeriod = $row->ExpiredPeriod;
								$NPK_User = $row->NPK_User;
							}
							if($ExpiredPeriod != '0000-00-00'){
								$sqlUpdate = "UPDATE mstrurf 
								SET StatusResolved = 1 , UpdatedOn = now(), UpdatedBy = '$this->npkLogin' 
								WHERE URF_ID = '$URF_ID'
								and URF_Number = '$URF_Number'";
							}else{
								$sqlUpdate = "UPDATE mstrurf 
								SET StatusResolved = 2 , UpdatedOn = now(), UpdatedBy = '$this->npkLogin' 
								WHERE URF_ID = '$URF_ID'
								and URF_Number = '$URF_Number'";
							}
							$this->db->query($sqlUpdate);
							$user = $this->user->dataUser($NPK_User);
							if($user)
							{
								$user_target = array();
								foreach($user as $row)
								{
									$user_target = array(
										'npk' => $row->npk,
										'nama' => $row->nama,
										'email' => $row->email
									);
								}
								$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
								$this->email->to($user_target['email']); 
								
								$this->email->subject('URF Status');
								$message = 'URF Anda dengan Nomor '.$URF_Number.' sudah selesai dikerjakan oleh IT. Terima kasih';
								$this->email->message($message);	
								
								
								if($this->email->send()){
									$this->session->set_flashdata('message', '
									<div class="alert alert-success">URF Sudah berhasil di Resolve</div>');
									redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
								} else {
									$this->session->set_flashdata('message', '
									<div class="alert alert-success">URF Sudah berhasil di Resolve, namun gagal kirim Email</div>');
									redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
								}
							}
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function Revert($URF_ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("311"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						$sqlSelect = 
						"SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.URF_ID = '$URF_ID'
						and m.deleted = 0
						and d.deleted = 0
						and m.StatusReceived = 1
						and m.StatusApproved = 1
						and m.StatusResolved = 1
						";
						$data = $this->db->query($sqlSelect);
						if($data->num_rows() == 0){
							$this->session->set_flashdata('message', '
							<div class="alert alert-warning">URF Sudah pernah di Revert sebelumnya / URF yang ingin di proses tidak valid</div>');
							redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
						}else{		
							$URF_Number = "";
							$NPK_User = "";
							foreach ($data->result() as $row)
							{
								$URF_Number = $row->URF_Number;
								$NPK_User = $row->NPK_User;
							}
							$sqlUpdate = "UPDATE mstrurf 
							SET StatusResolved = 2 , UpdatedOn = now(), UpdatedBy = '$this->npkLogin' 
							WHERE URF_ID = '$URF_ID'
							and URF_Number = '$URF_Number'";
							$this->db->query($sqlUpdate);
							$user = $this->user->dataUser($NPK_User);
							if($user)
							{
								$user_target = array();
								foreach($user as $row)
								{
									$user_target = array(
										'npk' => $row->npk,
										'nama' => $row->nama,
										'email' => $row->email
									);
								}
								$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
								$this->email->to($user_target['email']); 
								
								$this->email->subject('URF Status');
								$message = 'URF Anda dengan Nomor '.$URF_Number.' sudah dikembalikan aksesnya oleh IT, harap menghubungi IT jika ingin melanjutkan durasi URF ini.';
								$this->email->message($message);	
								
								if($this->email->send()){
									$this->session->set_flashdata('message', '
									<div class="alert alert-danger">URF Sudah berhasil di Revert</div>');
									redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
								} else {
									$this->session->set_flashdata('message', '
									<div class="alert alert-danger">URF Sudah berhasil di Revert, namun gagal kirim Email</div>');
									redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
								}
							}
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function Reject($URF_ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("311"))
					{
						$session_data = $this->session->userdata('logged_in');
						$this->npkLogin = $session_data['npk'];
						$sqlSelect = 
						"SELECT * from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
						where 
						m.URF_ID = '$URF_ID'
						and m.deleted = 0
						and d.deleted = 0
						and m.StatusReceived = 0
						and m.StatusApproved != 1
						and m.StatusResolved != 1
						";
						$data = $this->db->query($sqlSelect);
						if($data->num_rows() == 0){
							$this->session->set_flashdata('message', '
							<div class="alert alert-warning">URF Sudah pernah di Proses sebelumnya</div>');
						}else{		
							$URF_Number = "";
							$NPK_User = "";
							foreach ($data->result() as $row)
							{
								$URF_Number = $row->URF_Number;
								$NPK_User = $row->NPK_User;
							}
							$sqlUpdate = "UPDATE mstrurf 
							SET StatusReceived = 2, UpdatedOn = now(), UpdatedBy = '$this->npkLogin' 
							WHERE URF_ID = '$URF_ID'
							and URF_Number = '$URF_Number'";
							$this->db->query($sqlUpdate);
							$user = $this->user->dataUser($NPK_User);
							if($user)
							{
								$user_target = array();
								foreach($user as $row)
								{
									$user_target = array(
										'npk' => $row->npk,
										'nama' => $row->nama,
										'email' => $row->email
									);
								}
								$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
								$this->email->to($user_target['email']); 
								
								$this->email->subject('URF Status');
								$message = 'URF Anda dengan Nomor '.$URF_Number.' sudah ditolak oleh IT, harap menghubungi IT untuk proses lebih lanjut ';
								$this->email->message($message);	
								
								if($this->email->send()){
									$this->session->set_flashdata('message', '
									<div class="alert alert-danger">URF Sudah berhasil di Reject.</div>');
								} else {
									$this->session->set_flashdata('message', '
									<div class="alert alert-danger">URF Sudah berhasil di Reject Namun gagal kirim email</div>');
								}
							}
						}
						redirect('URFbySystem/URFbySystemController/ViewURFAdmin/');
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function Checker(){
			$sqlSelect = 
			"SELECT *, DATE_FORMAT(m.ExpiredPeriod,'%d/%m/%Y') as ExpiredPeriodFmt from mstrurf m join dtlurf d on m.URF_Number = d.URF_Number
			where 
			m.Deleted = 0
			and d.Deleted = 0
			and 
				(m.StatusReceived = 0
			or 
				(m.StatusReceived = 1
				and m.StatusApproved = 0)
			or
				(m.StatusReceived = 1
				and m.StatusApproved = 1
				and m.StatusResolved = 0)
			or
				(m.StatusReceived = 1
				and m.StatusApproved = 1
				and m.StatusResolved = 1)
			)
			";
			$sqlSelect .= " order by m.CreatedOn desc";
			$data = $this->db->query($sqlSelect);
			$needRevert = false;
			$needResolved = false;
			$now = new DateTime(date('Y-m-d h:i:s'));
			$now->sub(new DateInterval('P1D'));
			if($data->num_rows() > 0){
				foreach ($data->result() as $result){
					if($result->StatusReceived == 1 &&
						$result->StatusApproved == 1 &&
						$result->StatusResolved == 1){
							if($result->ExpiredPeriod <= $now){
								echo $result->ExpiredPeriod;
								echo '<br/>';
								echo $now->format('Y-m-d') ;
								$needRevert = true;
							}
					}
					if($result->StatusReceived == 1 &&
						$result->StatusApproved == 1 &&
						$result->StatusResolved == 0){
						$needResolved = true;
					}
				}
				$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service');
				$this->email->to('it@dpa.co.id'); 
				$this->email->subject('URF Status: URF ini butuh di tangani!');
				$message = '';
				if($needRevert && $needResolved){
					$message = 'Terdapat URF yang perlu di Revert dan Resolve, harap cek ESS anda';
				}
				else if($needResolved){
					$message = 'Terdapat URF yang perlu di Resolve, harap cek ESS anda';
				}
				else if($needRevert){
					$message = 'Terdapat URF yang perlu di Revert dalam waktu kurang dari 1 hari, harap cek ESS anda';
				}
				if($message != ''){
					echo $message;
					$this->email->message($message);
					$this->email->send();
				}else{
					echo 'tidak ada urf menggantung';
				}
				
			}
		}
		
	}
?>