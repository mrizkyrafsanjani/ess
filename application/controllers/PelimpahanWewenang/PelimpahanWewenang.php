<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PelimpahanWewenang extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('user','',TRUE);	
		$this->load->model('UserPelimpahanWewenang','',TRUE);	
		$this->load->model('pelimpahanwewenang_model','',TRUE);	
		$this->load->model('cuti_model','',TRUE);	
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Input Cuti dan Izin Pribadi") || check_authorizedByName("SPD"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_pelimpahanwewenang();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	
	
	public function _pelimpahanwewenang()
    {
		
		$crud = new grocery_crud();
		$crud->set_subject('Pelimpahan Wewenang');
		//$crud->set_theme('datatables');
		
		$crud->set_table('dtlpelimpahanwewenang');		
		$crud->set_relation('NPKYangDilimpahkan','mstruser','Nama',array('Deleted' => 0,'TanggalBerhenti' => '0000-00-00'));

		$crud->where('dtlpelimpahanwewenang.deleted','0');
		$crud->where('dtlpelimpahanwewenang.CreatedBy',$this->npkLogin);
		$crud->where('dtlpelimpahanwewenang.KodePelimpahanWewenang','-');

		$crud->columns('Keterangan','NPKYangDilimpahkan');
		$crud->fields('KodeDtlPelimpahanWewenang','Keterangan','NPKYangDilimpahkan');
	

		$crud->display_as('Keterangan','Keterangan Pelimpahan');
		$crud->display_as('NPKYangDilimpahkan','Nama Dilimpahkan');

		
		$crud->callback_column('NPKYangDilimpahkan',array($this,'_column_callback_NPK'));

	//	$crud->callback_field('NPKYangDilimpahkan',array($this,'add_field_callback_NPK'));
		$crud->callback_field('Keterangan',array($this,'add_field_callback_Keterangan'));			
			
		$crud->field_type('KodeDtlPelimpahanWewenang','invisible');
		$crud->field_type('CreatedOn','invisible');
		$crud->field_type('CreatedBy','invisible');
		$crud->field_type('UpdatedOn','invisible');
		$crud->field_type('UpdatedBy','invisible');
		
		$crud->required_fields('Keterangan','NPKYangDilimpahkan');		
	
		
		$crud->callback_insert(array($this,'_insert'));		
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete'));
		
		//$crud->unset_back_to_list();
		
		
        $output = $crud->render();		 
        $this-> _outputview($output);
    }
 
    function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Pelimpahan Wewenang',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));		
		$this->load->view('PelimpahanWewenang/PelimpahanWewenang_view',$data);
		
  
    }
	
	function add_field_callback_NPK($value = '', $primary_key = null)
	{
		
		$htmlText = '<select id="NPKYangDilimpahkan" name="NPKYangDilimpahkan" class="form-control select2">';
		$npk = $this->UserPelimpahanWewenang->getNPKUser();
		$count = 0;
		if($npk){
			foreach($npk as $row){
				$htmlText .= '<option ';
				if($value == $row->NPK)
					$htmlText .= ' selected ';
				$htmlText .= ' value="'.$row->NPK.'">'.$row->Nama.'</option>';
				$count++;
			}
		}
		$htmlText .= '</select>';
		return $htmlText;
	}	

	function add_field_callback_Keterangan($value = '', $primary_key = null)
	{
		
		return '<textarea name="Keterangan" rows="10" cols="150">'.$value.'</textarea>';
	}

	function _column_callback_NPK($value,$row)
	{
		fire_print('log','row:'.print_r($row,true));
		$nama = "";
		$dataKaryawan = $this->UserPelimpahanWewenang->findUserByNPK($row->NPKYangDilimpahkan);
		foreach($dataKaryawan as $row){
			$nama = $row->Nama;
		}
		return $nama;
	}
	
	
	function _insert($post_array)
	{
		$dataTransaksi = array(			
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s'),
			"Keterangan" => $post_array['Keterangan'],
			"NPKYangDilimpahkan" => $post_array['NPKYangDilimpahkan']	
		);
	
		$this->db->insert('dtlpelimpahanwewenang',$dataTransaksi);	
	}
	
	function _update($post_array, $primary_key){		
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('dtlpelimpahanwewenang',$post_array,array('KodeDtlPelimpahanWewenang' => $primary_key));
	}	
	
	
	function _delete($primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedBy'] = '1';
		return $this->db->update('dtlpelimpahanwewenang',$post_array,array('KodeDtlPelimpahanWewenang' => $primary_key));	
	}

	function cetakFormPelimpahanWewenang($KodePelimpahanWewenang)
	{
		try
		{
			if(!$this->session->userdata('logged_in'))
			{
				redirect("login?u=PelimpahanWewenang/PelimpahanWewenang/cetakFormPelimpahanWewenang/$KodePelimpahanWewenang",'refresh');				
			}
			else
			{
				
					$dataUserYangMelimpahkanWewenang = $this->pelimpahanwewenang_model->getPWByKode($KodePelimpahanWewenang);						
					$NoTransaksi = $dataUserYangMelimpahkanWewenang['NoTransaksi'];
					$TanggalMulai = $dataUserYangMelimpahkanWewenang['TanggalMulai'];
					$TanggalSelesai = $dataUserYangMelimpahkanWewenang['TanggalSelesai'];
					$NPKSelectedUser = 	$dataUserYangMelimpahkanWewenang['NPK'];
					$NamaYangMelimpahkan = 	$dataUserYangMelimpahkanWewenang['Nama'];						
					$jumlahHariGlobal = $this->cuti_model->getHariKerja($TanggalMulai,$TanggalSelesai);
					

					/*$dataUserYangDiLimpahkanWewenang = $this->pelimpahanwewenang_model->getPWBreakDown($KodePelimpahanWewenang);						
					if($dataUserYangDiLimpahkanWewenang)
					{
						foreach($dataUserYangDiLimpahkanWewenang as $row)
						{	
							$dataUserYgDiPWDetail[] = array(
								'Keterangan' => $row->Keterangan,
								'NPKYangDilimpahkan' => $row->NPKYangDilimpahkan,
								'nama' => $row->nama
							);		
						}
					}*/
					
					//echo $NPKSelectedUser;
					
					$dataAtasanUser = $this->user->dataUser($NPKSelectedUser);	
					if($dataAtasanUser)
					{
						foreach($dataAtasanUser as $row){
						$dataAtasan = array(
							'Atasan'=> $row->atasan
						);
						}
					}
					
					

					$dataPelimpahanWewenang = $this->pelimpahanwewenang_model->getDetailPelimpahanByNoTransaksi($KodePelimpahanWewenang);						
					if($dataPelimpahanWewenang)
					{
						foreach($dataPelimpahanWewenang as $row)
						{	
							$dataPelimpahanWewenangDetail[] = array(
								'KodePelimpahanWewenang' => $row->KodePelimpahanWewenang,
								'Keterangan' => $row->Keterangan,
								'NPKYangDilimpahkan' => $row->NPKYangDilimpahkan,
								'NamaYangDilimpahkan' => $row->NamaYangDilimpahkan
							);		
						}
					}

					//email untuk di forward
					$dataEmailPelimpahanWewenang = $this->pelimpahanwewenang_model->getEmailPelimpahanByNoTransaksi($KodePelimpahanWewenang);						
					if($dataEmailPelimpahanWewenang)
					{
						foreach($dataEmailPelimpahanWewenang as $row)
						{	
							$dataEmailPelimpahanWewenangDetail[] = array(
								'Nama' => $row->Nama,
								'EmailYangDiTuju' => $row->EmailYangDiTuju
							);		
						}
					}

				
					
					if($dataPelimpahanWewenang && $dataEmailPelimpahanWewenang)
					{
						//echo '2';
						$data = array(
							'title' =>'Cetak Form Pelimpahan Wewenang'.$NPKSelectedUser,
							'NoTransaksi'=> $NoTransaksi,
							//'dataUserYgDiPWDetail'=>$dataUserYgDiPWDetail, 
							'TanggalMulai'=>$TanggalMulai,
							'TanggalSelesai'=>$TanggalSelesai,
							'jumlahHariGlobal'=>$jumlahHariGlobal,							
							'NamaYangMelimpahkan'=>$NamaYangMelimpahkan,
							'NPKYangMelimpahkan'=>$NPKSelectedUser,
							'dataAtasan'=>$dataAtasan,
							'dataPelimpahanWewenangDetail'=>$dataPelimpahanWewenangDetail,
							'dataEmailPelimpahanWewenangDetail'=>$dataEmailPelimpahanWewenangDetail,
							'KodePelimpahanWewenang'=>$KodePelimpahanWewenang,
							'flagemail' => '2'
						);
					}
					else	
					if ($dataPelimpahanWewenang)
					{
						//echo '1';
						$data = array(
							'title' =>'Cetak Form Pelimpahan Wewenang'.$NPKSelectedUser,
							'NoTransaksi'=> $NoTransaksi,
							//'dataUserYgDiPWDetail'=>$dataUserYgDiPWDetail, 
							'TanggalMulai'=>$TanggalMulai,
							'TanggalSelesai'=>$TanggalSelesai,
							'jumlahHariGlobal'=>$jumlahHariGlobal,							
							'NamaYangMelimpahkan'=>$NamaYangMelimpahkan,
							'NPKYangMelimpahkan'=>$NPKSelectedUser,
							'dataAtasan'=>$dataAtasan,
							'dataPelimpahanWewenangDetail'=>$dataPelimpahanWewenangDetail,
							'KodePelimpahanWewenang'=>$KodePelimpahanWewenang,
							'flagemail' => '1'
						);
					}	
					$this->load->view('PelimpahanWewenang/cetakFormPelimpahanWewenang_view',$data);				
					
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
		
	}
	

}

/* End of file main.php */
/* Location: ./application/controllers/main.php */