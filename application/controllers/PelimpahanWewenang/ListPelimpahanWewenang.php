<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class ListPelimpahanWewenang extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		//$this->load->model('issue','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Pelimpahan Wewenang"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_ListPelimpahanWewenang();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _ListPelimpahanWewenang()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Pelimpahan Wewenang');
		$crud->set_theme('datatables');
		
		$crud->set_model('custom_query_model');
		$crud->set_table('pelimpahanwewenang');
		//$crud->set_relation('CreatedBy','mstruser','Nama',array('Deleted' => 0));
		//$crud->where('pelimpahanwewenang.deleted','0');

		$sqlquery = " 
		select p.KodePelimpahanWewenang,p.NoTransaksi,p.TanggalMulai,p.TanggalSelesai,m.Nama , p.CreatedOn
		from pelimpahanwewenang p
		join mstruser m on m.NPK=p.CreatedBy
		where p.Deleted=0 
		";

		$crud->basic_model->set_query_str($sqlquery); 
	
		$crud->columns('KodePelimpahanWewenang','NoTransaksi','TanggalMulai','TanggalSelesai','CreatedOn','Nama');
		$crud->fields('KodePelimpahanWewenang','NoTransaksi','TanggalMulai','TanggalSelesai','CreatedOn','Nama');
		
		$crud->add_action('View Form','','','ui-icon-print',array($this,'callback_action_cetak'));
		$crud->callback_delete(array($this,'_delete_pelimpahan'));		
		
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_print();	
		$crud->unset_export();	
		$crud->unset_read();	
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Pelimpahan Wewenang',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	
	
	function _delete_pelimpahan($primary_key){
		return $this->db->update('pelimpahanwewenang',array('Deleted' => '1'),array('KodePelimpahanWewenang' => $primary_key));
	}

	function callback_action_cetak($primary_key,$row)
	{		
		
			$KodePelimpahanWewenang = $primary_key;				
			return site_url('PelimpahanWewenang/PelimpahanWewenang/cetakFormPelimpahanWewenang/' .$KodePelimpahanWewenang);
		
	}
		
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */