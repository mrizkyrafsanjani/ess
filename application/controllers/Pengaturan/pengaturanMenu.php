<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanMenu extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {		
		if($this->session->userdata('logged_in')){
			if(check_authorized("6"))
			{
				$session_data = $this->session->userdata('logged_in');
				$this->npkLogin = $session_data['npk'];
				$this->_menu();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _menu()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Menu');
		//$crud->set_theme('datatables');
		
        $crud->set_table('menu');
		$crud->where('menu.deleted','0');
		$crud->set_relation('Parent','menu','MenuName',array('deleted' => '0'));

		$crud->columns('KodeMenu','MenuName','Description','Url','Parent','MenuOrder');
		$crud->fields('KodeMenu','MenuName','Description','Url','Icon','Parent','MenuOrder');
		
		$crud->display_as('MenuName','Nama Menu');
        $crud->display_as('MenuOrder','Urutan Menu');
		
		$crud->required_fields('MenuName','Url','MenuOrder');
		
		$crud->set_rules('Parent','Parent','integer');
		$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		$crud->callback_insert(array($this,'_insert_menu'));
		$crud->callback_update(array($this,'_update_menu'));
		$crud->callback_delete(array($this,'_delete_menu'));
		$crud->unset_texteditor('Description','Url');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','Pengaturan/pengaturanMenu_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _delete_menu($primary_key){
		return $this->db->update('menu',array('deleted' => '1'),array('KodeMenu' => $primary_key));
	}
	
	function _insert_menu($post_array){
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		
		return $this->db->insert('menu',$post_array);
	}
	
	function _update_menu($post_array,$primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		return $this->db->update('menu',$post_array,array('KodeMenu'=>$primary_key));
	}
		
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */