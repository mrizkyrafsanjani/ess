<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanGolongan extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		if($this->session->userdata('logged_in')){
			if(check_authorized("5"))
			{
				$this->_golongan();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _golongan()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Golongan');
		//$crud->set_theme('datatables');
		
        $crud->set_table('mstrgolongan');
		$crud->where('mstrgolongan.deleted','0');
		
		$crud->columns('Golongan','UangSaku','UangMakan');
		$crud->fields('Golongan','UangSaku','UangMakan');
		
		$crud->display_as('UangSaku','Uang Saku');
        $crud->display_as('UangMakan','Uang Makan');
		
		$crud->required_fields('Golongan','UangSaku','UangMakan');
		
		$crud->set_rules('Golongan','Golongan','integer');
		$crud->set_rules('UangSaku','Uang Saku','integer');
		$crud->set_rules('UangMakan','Uang Makan','integer');
		
		$crud->callback_column('UangSaku',array($this,'_callback_format_number'));
		$crud->callback_column('UangMakan',array($this,'_callback_format_number'));
		$crud->callback_delete(array($this,'_delete_golongan'));
		$crud->unset_delete();
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
				'title' => 'Pengaturan Golongan',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','Pengaturan/pengaturanGolongan_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
		function _delete_golongan($primary_key){
			return $this->db->update('mstrgolongan',array('deleted' => '1'),array('Golongan' => $primary_key));
		}
		
		function _callback_format_number($value, $row){
			$formatNumber = number_format($value);
			return "Rp. ". $formatNumber;
		}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */