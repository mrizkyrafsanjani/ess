<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanTransaksiSPD extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		if($this->session->userdata('logged_in')){
			$this->_transaksiSPD();
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _transaksiSPD()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Transaksi SPD');
		//$crud->set_theme('datatables');
		
        $crud->set_table('trkspd');
		$crud->where('trkspd.deleted','0');
		
		//$crud->unset_columns('Deleted','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy');
		//$crud->unset_fields('Deleted','CreatedOn','CreatedBy','UpdatedOn','UpdatedBy');
		/*
		$crud->columns('Golongan','UangSaku','UangMakan');
		$crud->fields('Golongan','UangSaku','UangMakan');
		
		$crud->display_as('UangSaku','Uang Saku');
        $crud->display_as('UangMakan','Uang Makan');
		
		$crud->required_fields('Golongan','UangSaku','UangMakan');
		
		$crud->set_rules('Golongan','Golongan','integer');
		$crud->set_rules('UangSaku','Uang Saku','integer');
		$crud->set_rules('UangMakan','Uang Makan','integer');
		
		*/
		$crud->set_primary_key('NoSPD','trkspd');
		//$crud->fields('TotalSPD');
		$crud->callback_column('TotalSPD',array($this,'_callback_format_number'));
		$crud->callback_column('TotalLap',array($this,'_callback_format_number'));

		$crud->callback_delete(array($this,'_delete_transaksiSPD'));
		//$crud->unset_add();
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');		
		
		$data = array(
				'title' => 'Pengaturan Golongan',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','Pengaturan/pengaturanTransaksiSPD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
		function _delete_transaksiSPD($primary_key){
			return $this->db->update('trkspd',array('deleted' => '1'),array('NoSPD' => $primary_key));
		}
		
		function _callback_format_number($value, $row){
			$formatNumber = number_format($value);
			return "Rp. ". $formatNumber;
		}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */