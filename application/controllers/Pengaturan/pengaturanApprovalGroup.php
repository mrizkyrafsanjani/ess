<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanApprovalGroup extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("33"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_approvalgroup();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _approvalgroup()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Approval Group');
		//$crud->set_theme('datatables');
		
        $crud->set_table('approvalgroup');
		$crud->where('approvalgroup.deleted','0');
		
		$crud->set_relation_n_n('Anggota','approvalgroupuser','mstruser','KodeApprovalGroup','NPK','Nama');
		
		$crud->columns('NamaGroup','Anggota','Keterangan');
		$crud->fields('NamaGroup','Anggota','Keterangan');
		
		//$crud->display_as('Menu','Menu Yang Dapat Diakses');
		
		$crud->required_fields('NamaGroup','Anggota');
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		//$crud->callback_insert(array($this,'_insert')); ini di komen karena masalah relation n_n,yang menyebabkan tidak bisa di save
		//$crud->callback_update(array($this,'_update')); ini di komen karena masalah relation n_n,yang menyebabkan tidak bisa di save
		$crud->callback_delete(array($this,'_delete_approvalgroup'));
		$crud->unset_texteditor('Keterangan');
		
        $output = $crud->render();
   
        $this-> _outputview($output);
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Pengaturan Approval Group',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert($post_array)
	{
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		return $this->db->insert('approvalgroup',$post_array);
	}
	
	function _update($post_array, $primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		
		return $this->db->update('approvalgroup',$post_array,array('KodeApprovalGroup'=>$primary_key));
	}
	
	function _delete_approvalgroup($primary_key){
		return $this->db->update('approvalgroup',array('deleted' => '1'),array('KodeApprovalGroup' => $primary_key));
	}
		
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */