<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanApprovalMatrix extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("32"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_approvalmatrix();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _approvalmatrix()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Approval Matrix');
		//$crud->set_theme('datatables');
		
        $crud->set_table('approvalmatrix');
		$crud->set_relation('NPK','mstruser','Nama',array('deleted' => '0'));
		$crud->set_relation('KodeApprovalGroup','approvalgroup','NamaGroup',array('deleted' => '0'));
		$crud->where('approvalmatrix.deleted','0');
		
		
		$crud->columns('KodeApprovalMatrix', 'JenisTask','KodeApprovalGroup','Sequence','NPK', 'CreatedOn', 'CreatedBy', 'UpdatedOn', 'UpdatedBy');
		$crud->fields('JenisTask','KodeApprovalGroup','Sequence','NPK','Catatan');
				
		$crud->required_fields('JenisTask','KodeApprovalGroup','Sequence','NPK');
		
		$crud->callback_field('Catatan',array($this,'_callback_catatan'));
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete'));
		
		$js = "<script>
			$(document).ready(function() {
				var cmbKodeApprovalGroup = $('#field-KodeApprovalGroup');
				var cmbNPK = $('#NPK_input_box');
				cmbKodeApprovalGroup.change(function(){
					$.ajax({
						 type: 'POST',
						 url: '" .base_url()."index.php/Pengaturan/pengaturanApprovalMatrix/ajax_getNPKApprovalGroup',
						 data: {KodeApprovalGroup: cmbKodeApprovalGroup.val()},
					}).done(function(response){
						cmbNPK.html(response);
					});
				});
			});
		</script>";
		
        $output = $crud->render();
		$output->output.=$js;
		
        $this-> _outputview($output);        
    }
	
	function ajax_getNPKApprovalGroup(){
		$KodeApprovalGroup = $this->input->post('KodeApprovalGroup');
		$dataAGlist =$this->db->query('select * 
			from approvalgroupuser agu join mstruser u on agu.NPK = u.NPK 
			where agu.deleted = 0 and agu.KodeApprovalGroup = ?',array($KodeApprovalGroup));
		$strHTML = "<select id='NPK' name='NPK' class='chosen-select'>";
		foreach($dataAGlist->result() as $rw){
			$strHTML .= '<option value="'.$rw->NPK.'">'.$rw->Nama.'</option>';
		}
		$strHTML .= "</select>";
		echo $strHTML;
	}
	
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');		
		
		$data = array(
				'title' => 'Pengaturan Approval Matrix',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _callback_catatan($value = '', $primary_key = null)
	{
		return "Hanya dapat memasukkan NPK yang sudah terdaftar pada Approval Group";
	}
	
	function _insert($post_array)
	{
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		
		$query = $this->db->get_where('approvalgroupuser',array('KodeApprovalGroup'=>$post_array['KodeApprovalGroup'],'NPK'=>$post_array['NPK']));
		if($query->num_rows() == 0)
		{
			return false;
		}
		else
		{
			return $this->db->insert('approvalmatrix',$post_array);
		}
	}
	
	function _update($post_array, $primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		
		$query = $this->db->get_where('approvalgroupuser',array('KodeApprovalGroup'=>$post_array['KodeApprovalGroup'],'NPK'=>$post_array['NPK']));
		if($query->num_rows() == 0)
		{
			return false;
		}
		else
		{
			return $this->db->update('approvalmatrix',$post_array,array('KodeApprovalMatrix'=>$primary_key));
		}
	}
	
	function _delete($primary_key){
		return $this->db->update('approvalmatrix',array('deleted' => '1'),array('KodeApprovalMatrix' => $primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */