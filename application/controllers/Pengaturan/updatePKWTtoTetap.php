<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); 
class updatePKWTtoTetap extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
        //$this->load->database();
		$this->load->library('grocery_crud');
		$session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorizedByName("Update NPK Karyawan"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->user();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function user()
    {
		$session_data = $this->session->userdata('logged_in');
		$crud = new grocery_crud();
		$crud->set_subject('User');
		$crud->set_theme('datatables');
		
        $crud->set_table('mstruser');
		$crud->where('mstruser.deleted','0');
		$crud->where(('RIGHT(mstruser.npk,4)'),'PKWT');
		$crud->set_relation('KodeRole','role','Deskripsi',array('deleted' => '0'));
		$crud->set_relation('NPKAtasan','mstruser','Nama',array('deleted' => '0'));
		$crud->set_relation('Golongan','mstrgolongan','Golongan',array('deleted' => '0'));
		$crud->set_relation('Jabatan','jabatan','NamaJabatan',array('deleted' => '0'));
		$crud->set_relation('Departemen','departemen','NamaDepartemen',array('deleted' => '0'));
		
		$crud->add_action('Update','','Pengaturan/updatePKWTtoTetap/update','ui-icon-circle-check');
		
		$crud->columns('NPK','Nama','EmailInternal','Golongan','Jabatan','Departemen','NPKAtasan');
		$crud->unset_operations();
        $output = $crud->render();
   
        $this-> _outputview($output);       
		
    }

    /*function updatePKWTtoTetap($primary_key , $row)
	{
		return site_url('Pengaturan/updatePKWTtoTetap/update?NPK='.$row->NPK);
	}*/


	function update($NPKSelectedUser = "")
	{
		if($NPKSelectedUser == ''){
			$NPKSelectedUser = $this->npkLogin;
		}
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');				
		}
		if(check_authorizedByName("Update NPK Karyawan"))
		{
			//$NPKSelectedUser = $_GET['NPK'];
			$dataUser = $this->user->findUserByNPK($NPKSelectedUser);	
			if($dataUser){
				foreach($dataUser as $dataUser){
					$dataUserDetail = array(
						'title'=> 'Data User yang akan di Update',
						'npkLama' => $dataUser->NPK,
						'golongan' => $dataUser->Golongan,
						'nama'=> $dataUser->Nama,
						'jabatan'=> $dataUser->Jabatan,
						'dpa'=> $dataUser->DPA,
						'TanggalMulaiBekerjaBaru'=> '',
						'TanggalMulaiBekerjaLama'=> $dataUser->TanggalBekerja,
						'statuskaryawan'=> $dataUser->StatusKaryawan,
						'npkBaru' => '',

					);
				}
				$this->load->helper(array('form','url'));
				$this->template->load('default','Pengaturan/pengaturanPKWT_update_view',$dataUserDetail);
			}else{
				echo $this->npkLogin;
			}
		}
	}
	function do_updatePKWTtoTetap($NPKSelectedUser="")
	{
		if($NPKSelectedUser == ''){
			$NPKSelectedUser = $this->npkLogin;
		}
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');				
		}
		
		try{
		   $this->load->library('form_validation');			   
		   $this->form_validation->set_error_delimiters('<div id="divError">', '</div>');
		   $this->form_validation->set_rules('txtTanggalMulaiBekerjaBaru', 'Tanggal Mulai Bekerja Baru', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('txtNPKBaru', 'NPK Baru', 'trim|required|xss_clean');
		   
		   
		   if($this->form_validation->run() == false)
		   {
				$this->update($NPKSelectedUser);
		   }
		   else
		   {
		   		if($this->input->post())
				{
					$error = array();
					if(!$this->user->updateNPK($NPKSelectedUser))
					{
						$this->session->set_flashdata('msg','NPK yang di Input sudah terpakai');
						redirect('Pengaturan/updatePKWTtoTetap/update/'.$NPKSelectedUser);
					}
					else
					{
						redirect('Pengaturan/updatePKWTtoTetap','refresh');
					}
				}
			}
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');		
		
		$data = array(
				'title' => 'Update NPK PKWT',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output); 
		
		
    }
	
	function _delete_user($primary_key){
		return $this->db->update('mstruser',array('deleted' => '1'),array('NPK' => $primary_key));
	}
	
	function _insert_user($post_array){
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		return $this->db->insert('mstruser',$post_array);
	}
	
	function _update_user($post_array,$primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		return $this->db->update('mstruser',$post_array,array('NPK'=>$primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */