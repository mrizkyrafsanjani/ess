<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanJabatan extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("26"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_jabatan();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _jabatan()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Jabatan');
		//$crud->set_theme('datatables');
		
        $crud->set_table('jabatan');
		$crud->where('jabatan.deleted','0');
		
		
		$crud->columns('KodeJabatan', 'NamaJabatan');
		$crud->fields('NamaJabatan');
		
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete'));		
				
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');		
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert($post_array){
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');		
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('jabatan',$post_array);
	}
	
	function _update($post_array,$primary_key)
	{
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');		
		$post_array['UpdatedBy'] = $this->npkLogin;		
		$this->db->update('jabatan',$post_array,array('KodeJabatan' => $primary_key));
		return true;
	}
	
	function _delete($primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');		
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['Deleted'] = '1';
		return $this->db->update('jabatan',$post_array,array('KodeJabatan' => $primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */