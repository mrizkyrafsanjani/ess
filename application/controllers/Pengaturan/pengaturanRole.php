<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanRole extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("7"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_role();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _role()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Role');
		//$crud->set_theme('datatables');
		
        $crud->set_table('role');
		$crud->where('role.deleted','0');
		
		$crud->set_relation_n_n('Menu','rolemenu','menu','KodeRole','KodeMenu','MenuName');
		
		$crud->columns('KodeRole','Deskripsi','Menu');
		$crud->fields('Deskripsi','Menu');
		
		$crud->display_as('Menu','Menu Yang Dapat Diakses');
		
		$crud->required_fields('Deskripsi','Menu');
		
		//$crud->set_rules('Parent','Parent','integer');
		//$crud->set_rules('MenuOrder','Urutan Menu','integer');
		
		$crud->callback_after_insert(array($this,'_after_insert_role'));
		$crud->callback_after_update(array($this,'_after_update_role'));
		$crud->callback_delete(array($this,'_delete_role'));
		//$crud->unset_texteditor('Description','Url');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		$data = array(
			   'title' => 'Pengaturan Role',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','Pengaturan/pengaturanRole_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _after_insert_role($post_array,$primary_key)
	{
		$createDate = array(
			"CreatedBy" => $this->npkLogin,
			"CreatedOn" => date('Y-m-d H:i:s')
		);
		fire_print('log',print_r($post_array,true));
		$this->db->update('role',$createDate,array('KodeRole'=>$primary_key));
		return true;
	}
	
	function _after_update_role($post_array,$primary_key)
	{
		$updateDate = array(
			"UpdatedBy" => $this->npkLogin,
			"UpdatedOn" => date('Y-m-d H:i:s')
		);
		
		fire_print('log',print_r($post_array,true));
		fire_print('log','pk role : '.$primary_key);
		
		$this->db->update('role',$updateDate,array('KodeRole'=>$primary_key));		
		return true;
	}
	
	function _delete_role($primary_key){
		return $this->db->update('role',array('deleted' => '1'),array('KodeRole' => $primary_key));
	}
		
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */