<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanDepartemen extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		if($this->session->userdata('logged_in')){
			if(check_authorized("15"))
			{
				$this->_departemen();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _departemen()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Departemen');
		//$crud->set_theme('datatables');
		
        $crud->set_table('departemen');
		$crud->where('departemen.deleted','0');
		
		
		$crud->columns('KodeDepartemen', 'NamaDepartemen', 'CreatedOn', 'CreatedBy', 'UpdatedOn', 'UpdatedBy');
		$crud->fields('NamaDepartemen');
				
		$crud->required_fields('NamaDepartemen');
		
		$crud->callback_delete(array($this,'_delete'));
		$crud->unset_texteditor('Description','Url');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');		
		
		$data = array(
				'title' => 'Pengaturan Menu',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _delete($primary_key){
		return $this->db->update('departemen',array('deleted' => '1'),array('KodeDepartemen' => $primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */