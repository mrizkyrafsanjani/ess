<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanUser extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
        //$this->load->database();
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($this->session->userdata('logged_in')){
			if(check_authorized("4"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->user();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function user()
    {
		$session_data = $this->session->userdata('logged_in');
		$crud = new grocery_crud();
		$crud->set_subject('User');
		//$crud->set_theme('datatables');
		
        $crud->set_table('mstruser');
		$crud->where('mstruser.deleted','0');
		$crud->set_relation('KodeRole','role','Deskripsi',array('deleted' => '0'));
		$crud->set_relation('NPKAtasan','mstruser','Nama',array('deleted' => '0'));
		$crud->set_relation('Golongan','mstrgolongan','Golongan',array('deleted' => '0'));
		$crud->set_relation('Jabatan','jabatan','NamaJabatan',array('deleted' => '0'));
		$crud->set_relation('Departemen','departemen','NamaDepartemen',array('deleted' => '0'));
		
		//$crud->set_relation('NPK2','riwayatpekerjaan','NamaPerusahaan',array('deleted' => '0'));
		
		$crud->columns('NPK','Nama','EmailInternal','Golongan','Jabatan','Departemen','NPKAtasan','KodeRole');
		
		if($session_data['koderole'] == 2){ //user
			$crud->unset_edit();
			$crud->unset_add();
			$crud->unset_delete();
			$crud->where('mstruser.npk',$session_data['npk']);
			$crud->fields('NPK','Nama','EmailInternal','Golongan','Jabatan','Departemen','NPKAtasan');
		}else if($session_data['koderole'] == 3){ //HRGA
			$crud->fields('NPK','Nama','EmailInternal','Golongan','Jabatan','Departemen','NPKAtasan','KodeRole','UserIDAbsensi','WinID');
			$crud->display_as('UserIDAbsensi','User ID Absensi');
			$crud->set_rules('UserIDAbsensi','ID Absensi','callback_validasiUserIDAbsensi');
			$crud->set_rules('WinID','Win ID','callback_validasiWinID');
		}else{
			$crud->fields('NPK','Nama','EmailInternal','Golongan','Jabatan','Departemen','NPKAtasan','KodeRole','UserIDAbsensi','WinID');
			$crud->display_as('UserIDAbsensi','User ID Absensi');
			$crud->set_rules('UserIDAbsensi','ID Absensi','callback_validasiUserIDAbsensi');
			$crud->set_rules('WinID','Win ID','callback_validasiWinID');
		}
		$crud->display_as('NPKAtasan','Atasan');
        $crud->display_as('KodeRole','Role');
		$crud->display_as('Golongan','Gol');
		$crud->display_as('EmailInternal','Email Internal');
		
		$crud->required_fields('NPK','Nama','EmailInternal','Golongan','Jabatan','KodeRole');
		
		$crud->set_rules('EmailInternal','Email Internal','valid_email');
		$crud->set_rules('Golongan','Golongan','integer');
		$crud->set_rules('KodeRole','Kode Role','integer');
		
		$crud->callback_insert(array($this,'_insert_user'));
		$crud->callback_update(array($this,'_update_user'));
		$crud->callback_delete(array($this,'_delete_user'));
		
        $output = $crud->render();
   
        $this-> _outputview($output);       
		
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');		
		
		$data = array(
				'title' => 'Pengaturan User',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','Pengaturan/pengaturanUser_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output); 
		
		
    }
	
	function validasiUserIDAbsensi($value){
		$userIDAbsensi = $value;
		$nama = '';

		$query = $this->db->query('SELECT Nama FROM mstruser WHERE UserIDAbsensi = ? LIMIT 1',array($userIDAbsensi));
		$row = $query->row();
		$nama = $row->Nama;
		if( $query->num_rows() > 0 && $nama != $this->input->post('Nama'))
		{
			$this->form_validation->set_message('validasiUserIDAbsensi', 'ID Absensi '.$value.' sudah dipakai oleh '.$nama);
			return false;
		}
		else
		{
			return true;
		}
	}

	function validasiWinID($value){
		$winID = $value;
		$nama = '';

		$query = $this->db->query('SELECT Nama FROM mstruser WHERE WinID = ? LIMIT 1',array($winID));
		$row = $query->row();
		$nama = $row->Nama;
		if( $query->num_rows() > 0 && $nama != $this->input->post('Nama'))
		{
			$this->form_validation->set_message('validasiWinID', 'Win ID '.$value.' sudah dipakai oleh '.$nama);
			return false;
		}
		else
		{
			return true;
		}
	}

	function _delete_user($primary_key){
		return $this->db->update('mstruser',array('deleted' => '1'),array('NPK' => $primary_key));
	}
	
	function _insert_user($post_array){
		$post_array['CreatedBy'] = $this->npkLogin;
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		
		return $this->db->insert('mstruser',$post_array);
	}
	
	function _update_user($post_array,$primary_key)
	{
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		return $this->db->update('mstruser',$post_array,array('NPK'=>$primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */