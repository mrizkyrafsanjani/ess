<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PengaturanGlobalParam extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorized("21"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_globalparam();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _globalparam()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Global Param');
		//$crud->set_theme('datatables');
		
        $crud->set_table('globalparam');
		$crud->where('globalparam.deleted','0');
		
		
		$crud->columns('Name', 'Value', 'Keterangan');
		$crud->fields('Name', 'Value', 'Keterangan');
		
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete'));		
		
		$crud->unset_texteditor('Keterangan');
		
        $output = $crud->render();
   
        $this-> _outputview($output);        
    }
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');		
		
		$data = array(
				'title' => 'Pengaturan Global Param',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
        //$this->load->view('pengaturanUser_view',$output);    
    }
	
	function _insert($post_array){
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');		
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('globalparam',$post_array);
	}
	
	function _update($post_array,$primary_key)
	{
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');		
		$post_array['UpdatedBy'] = $this->npkLogin;		
		$this->db->update('globalparam',$post_array,array('KodeGlobalParam' => $primary_key));
		return true;
	}
	
	function _delete($primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');		
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['Deleted'] = '1';
		return $this->db->update('globalparam',$post_array,array('KodeGlobalParam' => $primary_key));
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */