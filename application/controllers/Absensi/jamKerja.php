<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class JamKerja extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->_jamKerja();
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _jamKerja()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Jam Kerja');
		//$crud->set_theme('datatables');
		
        $crud->set_table('jamkerja');
		$crud->where('jamkerja.deleted','0');
		
		$crud->columns('JamMasuk','JamKeluar');
		$crud->fields('JamMasuk','JamKeluar');
		$crud->required_fields('JamMasuk','JamKeluar');
		
		$crud->display_as('JamMasuk','Jam Masuk (hh:mm)');
		$crud->display_as('JamKeluar','Jam Keluar (hh:mm)');
		$crud->callback_field('JamMasuk',array($this,'field_callback_JamMasuk'));
		$crud->callback_field('JamKeluar',array($this,'field_callback_JamKeluar'));
		//$crud->unset_operations();
		$crud->unset_add();
		$crud->unset_delete();
		
		$crud->callback_update(array($this,'_update'));
		
        $output = $crud->render();
		
        $this-> _outputview($output);
    }
	
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		
		$data = array(
				'title' => 'Pengaturan Jam Kerja',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','Absensi/jamKerja_view',$data);
		
    }
	
	function _update($post_array, $primary_key){		
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('jamkerja',$post_array,array('KodeJamKerja' => $primary_key));
	}
	
	function field_callback_JamMasuk($value='',$primary_key=null)
	{
		return "<input name='JamMasuk' type='time' value='$value'>";
	}
	
	function field_callback_JamKeluar($value='',$primary_key=null)
	{
		return "<input name='JamKeluar' type='time' value='$value'>";
	}
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */