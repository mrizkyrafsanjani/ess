<?php
ini_set('max_execution_time', 3000);
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AbsensiTarikData extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Grocery_CRUD');
        $this->load->model('absensi', '', true);
        //$this->load->model('Kode_Model');
    }

    public function index()
    {

        //-------- Begin - Mengatur tanggal ------------//
        $timenowYmdHis = date('Y-m-d H:i:s'); //2018-08-10 11:42:22
        $timenowYmd = date('Ymd'); //20180810
        $tanggal = date('Y-m-d');
        $tanggal2hari = date('Y-m-d', strtotime($tanggal . ' - 2 days'));
        $namaharisekarang = date('D', strtotime($tanggal));
        $namahari2harilalu = date('D', strtotime($tanggal2hari));
        //-------- Begin - Mengatur tanggal ------------//

        //-------- Begin - Mengahpus data temp yang sudah lewat 2 hari ------------//
        print(" [ Deleting from table absensidata...... ");
        //$delete_temp_data = $this->absensi->deleteTempFromAbsensidata2harisebelum($tanggal2hari);
        $delete_temp_data = $this->absensi->deleteTempFromAbsensidata();
        print(" Deleted] ");
        //-------- Begin - Mengahpus data temp yang sudah lewat 2 hari ------------//

        //-------- Begin - Bagian untuk traktik absensi dari mesin absensi ------------//
        //$ipaddr1 = '10.1.25.187';
        $ipaddr2 = '10.1.25.188';
        //$ipaddr3 = '10.1.25.189';
        //$ipaddr4 = '10.1.25.190';
        $key = '0';
        //$setdataabsensi1 = $this->absensi->getDdataAbsen($ipaddr1, $key);
        $setdataabsensi2 = $this->absensi->getDataAbsen($ipaddr2, $key);
        //$setdataabsensi3 = $this->absensi->getDdataAbsen($ipaddr3, $key);
        //$setdataabsensi4 = $this->absensi->getDdataAbsen($ipaddr4, $key);
        //-------- Begin - Bagian untuk traktik absensi dari mesin absensi ------------//

        //-------- Begin - Insert or Update data into table 'absensi'  ------------//

        //1. Get data NPK, Nama, dan User ID Absensi
        $getDataNPKNamaIDAbsensi = $this->absensi->getArrayNPKNamaIDAbsensi();
        $idabsensi = array();
        $npk = array();
        foreach ($getDataNPKNamaIDAbsensi as $data) {
            $idabsensi[] = $data->UserIDAbsensi; //data disimpan dalam array
            $npk[] = $data->NPK; //data disimpan dalam array
        }
        print(" [ Array NPK: ");
        print_r($npk);
        print(" ] ");

        //2. Looping tiap NPK
        foreach ($npk as $value) {

            //3. Cek apakah data NPK tersebut sudah ada pada table absensi dengan tanggal hari ini
            $cekDataIsNpkExist = $this->absensi->checkDataNpkOnAbsensiToday($value, $tanggal);
            $KodeAbsensi = null;
            foreach ($cekDataIsNpkExist as $data2) {
                $KodeAbsensi = $data2->KodeAbsensi;
            }
            print(" | Kode Absensi : " . $KodeAbsensi . " | ");

            //4. Jika kode absensi null atau kosong, maka akan dilakukan insert data
            if (is_null($KodeAbsensi) || empty($KodeAbsensi)) {
                print(" [ Kode Absensi NULL; ");
                print(" NPK : " . $value . "; ");
                print(" Tanggal Sekarang : " . $tanggal . " ] ");
                $insertDataIntoAbsensiAutoToday = $this->absensi->insertSelectToAbsensi($value, $tanggal); //parameternya NPK dan Tanggal hari ini
            }
            //5. Jika kode absensi tidak null atau tidak kosong, maka akan melakukan update data
            else {
                print(" [ Kode Absensi: " . $KodeAbsensi . "; ");
                print(" NPK : " . $value . "; ");
                print(" Tanggal Sekarang : " . $tanggal . " ] ");

                //6. Cek apakah Waktu Masuk null atau kosong pada KodeAbsensi tertentu
                $apakahAdaWaktuMasuk = $this->absensi->checkDataWaktuMasuk($KodeAbsensi);
                $WaktuMasuk = null;
                foreach ($apakahAdaWaktuMasuk as $data3) {
                    $WaktuMasuk = $data2->WaktuMasuk;
                }
                print(" | Waktu Masuk : " . $WaktuMasuk . " | ");

                //7. Jika WaktuMasuk null atau tidak ada waktu masuk nya, maka akan update waktu masuk dan waktu keluar sekaligus
                if(is_null($WaktuMasuk) || empty($WaktuMasuk)){
                    print(" [ WaktuMasuk NULL ] ");
                    $updateWaktuKeluarPadaHariTersebut = $this->absensi->updateWaktuMasukWaktuKeluar($KodeAbsensi, $value, $tanggal); //parameternya NPK dan Tanggal hari ini
                }
                //8. Jika WaktuMasuk tidak null atau tidak kosong
                else{
                    print(" [ WaktuMasuk NOT NULL ] ");
                    $updateWaktuKeluarPadaHariTersebut = $this->absensi->updateWaktuKeluar($KodeAbsensi, $value, $tanggal); //parameternya NPK dan Tanggal hari ini
                }

            }

        }

        //-------- End - Insert or Update data into table 'absensi'  ------------//

    }

}
