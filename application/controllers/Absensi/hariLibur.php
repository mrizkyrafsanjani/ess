<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class HariLibur extends CI_Controller {
	var $npkLogin;
    function __construct()
    {
        parent::__construct();
		$this->load->library('grocery_crud');
    }
 
    public function index()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->_hariLibur();
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _hariLibur()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Hari Libur');
		//$crud->set_theme('datatables');
		
        $crud->set_table('harilibur');
		$crud->where('harilibur.deleted','0');
		
		$crud->columns('Tanggal','Hari','Keterangan');
		$crud->fields('Tanggal','Hari','Keterangan','Note');
		$crud->required_fields('Tanggal','Hari');
		//$crud->unset_operations();
		
		$crud->callback_field('Note',array($this,'callback_field_note'));
		$crud->callback_insert(array($this,'_insert'));
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete'));
		
		$crud->unset_texteditor('Keterangan');
        $output = $crud->render();
		
        $this-> _outputview($output);
    }
	
 
    function _outputview($output = null)
    {
		$session_data = $this->session->userdata('logged_in');
		
		
		$data = array(
				'title' => 'Hari Libur',
			   'body' => $output
		  );
		$this->load->helper(array('form','url'));
		$this->template->load('default','templates/CRUD_view',$data);
		
    }
	
	function _insert($post_array){		
		$post_array['CreatedOn'] = date('Y-m-d H:i:s');
		$post_array['CreatedBy'] = $this->npkLogin;
		return $this->db->insert('harilibur',$post_array);
	}
	
	function _update($post_array, $primary_key){		
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('harilibur',$post_array,array('KodeHariLibur' => $primary_key));
	}
	
	function _delete($primary_key){		
		try
		{
			$post_array['deleted'] = '1';
			$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
			$post_array['UpdatedBy'] = $this->npkLogin;
			
			return $this->db->update('harilibur',$post_array,array('KodeHariLibur' => $primary_key));
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
	}
	
	function callback_field_note()
	{
		return "Gunakan kode 'cb' (tanpa tanda petik) pada kolom Hari untuk menandakan bahwa cuti bersama.";
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */