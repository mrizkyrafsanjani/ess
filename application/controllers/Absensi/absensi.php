<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class Absensi extends CI_Controller {
	var $npkLogin;
	var $NPKSelectedUser;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index($tahun='',$bulan='',$nama='')
    {
		if($nama == 'non')
			$nama = '';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->_absensi($nama,$tahun,$bulan);
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }	
	
	public function user($tahun='',$bulan='',$nama='',$NPKuser='')
    {
		if($nama == 'non')
			$nama = '';
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->NPKSelectedUser = $NPKuser;
			$this->_absensi($nama,$tahun,$bulan);
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	
	public function _absensi($nama='',$tahun='',$bulan='')
    {
		try{
			
			$crud = new grocery_crud();			
			$crud->set_subject('Absensi');
			$crud->set_theme('datatables');
			
			$crud->set_table('absensi');
			$crud->where('absensi.deleted','0');
			$crud->set_relation('NPK','mstruser','Nama',array('deleted' => '0'));
			if($this->NPKSelectedUser != '')
			{
				//$crud->where('absensi.NPK',$this->NPKSelectedUser);
				//$crud->or_where('absensi.NPK', 'Libur'); 
				$crud->where("(absensi.NPK = '".$this->NPKSelectedUser."' OR absensi.NPK = 'Libur')");
			}
			
			if($nama != '')
			{
				$crud->like('absensi.NPK',$nama);				
			}
			if($tahun != '')
			{
				$crud->where('YEAR(absensi.Tanggal)',$tahun);
			}
			if($bulan != '')
			{
				$crud->where('MONTH(absensi.Tanggal)', $bulan);
			}
			
			$crud->order_by('Tanggal','asc');
			$crud->columns('NPK', 'Tanggal', 'Hari', 'WaktuMasuk', 'WaktuKeluar','Keterangan', 'PelimpahanWewenang');
			$crud->fields('NPK', 'Tanggal', 'WaktuMasuk', 'WaktuKeluar','Keterangan','urlFile');
			
			//$crud->required_fields('NamaTraining', 'Lembaga', 'DariTanggal', 'SampaiTanggal','SifatTraining','Sertifikasi');
			
			$crud->display_as('NPK','Nama')->display_as('WaktuMasuk','Waktu Masuk (hh:mm)')->display_as('WaktuKeluar','Waktu Keluar (hh:mm)');
			$crud->display_as('urlFile','File Attachment');
			
			$crud->callback_update(array($this,'_update'));
			$crud->callback_delete(array($this,'_delete'));		
			$crud->callback_field('NPK',array($this,'field_callback_NPK'));
			$crud->callback_field('Tanggal',array($this,'field_callback_Tanggal'));
			$crud->callback_field('Keterangan',array($this,'field_callback_Keterangan'));
			$crud->callback_column('WaktuMasuk',array($this,'column_callback_WaktuMasuk'));
			$crud->callback_column('Hari',array($this,'column_callback_Hari'));
			$crud->callback_column('WaktuKeluar',array($this,'column_callback_WaktuKeluar'));
			
			$crud->unset_add();
			$crud->unset_read();
			$crud->unset_print();
			//$crud->unset_export();
			$crud->unset_texteditor('Keterangan');
			
			$crud->set_field_upload('urlFile','assets/uploads/filelupaabsen');
			
			if($this->NPKSelectedUser == '' || $this->NPKSelectedUser == 'edit')
			{
				
			}
			else
			{
				$crud->unset_edit();
				$crud->unset_delete();
			}
			
			if ($this->user->isHead($this->npkLogin)){
				$crud->unset_edit();
				$crud->unset_delete();
			}
			
			//$crud->unset_jquery();
			//$crud->unset_jquery_ui();
			
			$js = " <script>
				
					var spans = document.getElementsByTagName('span');					
					for(var i=0;i<spans.length;i++)
					{
						if(spans[i].innerHTML == '&nbsp;Edit'){
							//alert(spans[i].innerHTML);
							spans[i].innerHTML = 'Lupa Absen';
						}
						if(spans[i].innerHTML == '&nbsp;Ubah'){
							//alert(spans[i].innerHTML);
							spans[i].innerHTML = 'Lupa Absen';
						}
					}
				
				</script>";
			
			$output = $crud->render();
			$output->output.= $js;
			$this-> _outputview($output);        
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }
 
    function _outputview($output = null)
    {
		$data = array(
			'title' => 'Absensi',
			'body' => $output
		); 
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		
        $this->load->view('Absensi/absensi_view',$data);
    }

	function field_callback_NPK($value= '', $primary_key = null)
	{
		$result = $this->user->dataUser($value);
		foreach($result as $row){
			$nama = $row->nama;
		}
		return $nama .' <input type="hidden" name="NPK" value="'.$value.'">';
	}
	
	function field_callback_Tanggal($value= '', $primary_key = null)
	{
		return date('j F Y',strtotime($value)). '<input type="hidden" name="Tanggal" value="'.$value.'">';
	}
	
	function column_callback_WaktuMasuk($value, $row)
	{
		$valTime = strtotime(date('h:i',strtotime($value)));
		fire_print('log',"valTime: $valTime");
		//$valTime = strtotime($value);
		$query = $this->db->get_where('jamkerja',array("Deleted"=>"0"));
		$dataJamKerja = $query->row(1);
		$batasTelat = strtotime($dataJamKerja->JamMasuk); //strtotime("07:30:00");
		fire_print('log',"batasTelat : $batasTelat. jam masuk: $dataJamKerja->JamMasuk");
		if($valTime > $batasTelat && strpos($row->Hari,'Sabtu') === false && strpos($row->Hari,'Minggu') === false){
			return "<font color='red'>" . substr($value,0,5) . "</font>";
		}
		return substr($value,0,5);
	}
	
	function column_callback_WaktuKeluar($value, $row)
	{
		$valTime = strtotime($value);
		$query = $this->db->get_where('jamkerja',array("Deleted"=>"0"));
		$dataJamKerja = $query->row(1);
		$batasTelat = strtotime($dataJamKerja->JamKeluar); //strtotime("16:30:00");
		fire_print('log',"batasTelat : $batasTelat");
		if($valTime < $batasTelat && strpos($row->Hari,'Sabtu') === false && strpos($row->Hari,'Minggu') === false){
			return "<font color='red'>" . substr($value,0,5) . "</font>";
		}
		return substr($value,0,5);
	}
	
	function column_callback_Hari($value, $row)
	{		
		if($value == 'Sabtu' || $value == 'Minggu' || $row->NPK == 'Libur'){
			return "<font color='red'>$value</font>";
		}
		return $value;
	}
	
	function field_callback_Keterangan($value='',$primary_key=null)
	{
		return "
		<input name='Keterangan' type='text' list='listKeterangan' value='$value'/>
		<datalist id='listKeterangan'>
		  <option value='Sakit'>
		</datalist>";
	}
	
	function _update($post_array,$primary_key){
		$post_array['StatusLupaAbsen'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		
		//update realisasi lembur jika ada perubahan jam pulang karena lupa absen
		$query = $this->db->get_where('lembur',array('Tanggal'=>$post_array['Tanggal'],'NPK'=>$post_array['NPK'],'StatusLembur'=>'PER'));
		
		$dataLembur = $query->row(1);
		$jamMulaiLembur = strtotime($dataLembur->JamMulaiRealisasi);
		$jamSelesaiLemburBaru = strtotime($post_array['WaktuKeluar']);
		$different = round(($jamSelesaiLemburBaru - $jamMulaiLembur)/3600,2);
		
		$this->db->update('lembur',
			array('JamSelesaiRealisasi'=>$post_array['WaktuKeluar'],'TotalJamRealisasi'=>$different),
			array('Tanggal'=>$post_array['Tanggal'],'NPK'=>$post_array['NPK'],'StatusLembur'=>'PER'));
		
		$new_post_array = array();
		$new_post_array['NPK'] = $post_array['NPK'];
		$new_post_array['Tanggal'] = $post_array['Tanggal'];
		$new_post_array['WaktuMasuk'] = $post_array['WaktuMasuk'];
		$new_post_array['WaktuKeluar'] = $post_array['WaktuKeluar'];
		$new_post_array['Keterangan'] = $post_array['Keterangan'];
		$new_post_array['urlFile'] = $post_array['urlFile'];
		$new_post_array['StatusLupaAbsen'] = $post_array['StatusLupaAbsen'];
		$new_post_array['UpdatedOn'] = $post_array['UpdatedOn'];
		$new_post_array['UpdatedBy'] = $post_array['UpdatedBy'];
		return $this->db->update('absensi',$new_post_array,array('KodeAbsensi' => $primary_key));
	}
	
	function _delete($primary_key){
		$post_array['deleted'] = '1';
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('absensi',$post_array,array('KodeAbsensi' => $primary_key));
	}
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */