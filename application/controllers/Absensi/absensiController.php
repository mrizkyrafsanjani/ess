<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class AbsensiController extends CI_Controller{
		var $npkLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('absensi','',TRUE);
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
		}
		
		function index()
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("25"))
					{
						$kehadiran = '';
						$izin = '';
						$cuti = '';
						$sakit = '';
						$perjalanandinas = '';
						$data = array(
							'title' => 'View Absensi Admin',
							'admin' => '1',
							'kehadiran' => $kehadiran,
							'izin' => $izin,
							'cuti' => $cuti,
							'sakit' => $sakit,
							'perjalanandinas' => $perjalanandinas,
							'databawahan' => $this->user->getDataBawahan($this->npkLogin),
							'npk' => ''
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Absensi/absensiAdmin_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ViewAbsensiUser()
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorized("22"))
					{
						$kehadiran = '';
						$izin = '';
						$cuti = '';
						$sakit = '';
						$perjalanandinas = '';
						$data = array(
							'title' => 'View Absensi Admin',
							'admin' => '0',
							'kehadiran' => $kehadiran,
							'izin' => $izin,
							'cuti' => $cuti,
							'sakit' => $sakit,
							'perjalanandinas' => $perjalanandinas,
							'npk' => $this->npkLogin
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','Absensi/absensiAdmin_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ajax_GetKehadiran()
		{
			$kehadiran = '0';
			$izin = '0';
			$cuti = '0';
			$sakit = '0';
			$perjalanandinas = '0';
			$npkKaryawan = $this->input->post('npkKaryawan');
			$tahun = $this->input->post('tahun');
			$bulan = $this->input->post('bulan');
			$npk = $this->input->post('npk');
			$dataabsensi = $this->absensi->getCountAbsen($npkKaryawan,$tahun,$bulan,$npk);
			$datakehadiran = $this->absensi->getCountKehadiran($npkKaryawan,$tahun,$bulan,$npk);
			foreach($datakehadiran as $rowKhdr)
			{
				$kehadiran = $rowKhdr->total;
			}
			if($dataabsensi){
				foreach($dataabsensi as $row)
				{
					if(strpos(strtolower($row->keterangan),"izin") !== false)
					{
						$izin += $row->total;
					}
					else if(strpos(strtolower($row->keterangan),"cuti") !== false)
					{
						$cuti += $row->total;
					}
					else if(strpos(strtolower($row->keterangan),"sakit") !== false)
					{
						$sakit += $row->total;
					}
					else if(strpos(strtolower($row->keterangan),"perjalanan dinas") !== false)
					{
						$perjalanandinas += $row->total;
					}
					
					/* switch($row->keterangan)
					{
						case "Hadir":
							$kehadiran = $row->total; break;
						case "Izin":
							$izin = $row->total; break;
						case "Cuti":
							$cuti = $row->total; break;
						case "Sakit":
							$sakit = $row->total; break;
						case "Perjalanan Dinas":
							$perjalanandinas = $row->total; break;
						default:
							$others = $row->total; break;
					} */
				}
			}
			
			/* $datacutiBersama = $this->absensi->getCutiBersama($tahun,$bulan,$npk);
			if($datacutiBersama)
			{
				foreach($datacutiBersama as $dtctBrsm)
				{
					fire_print('log',"dtctBrsm : " .$dtctBrsm->total);
					$cuti += $dtctBrsm->total;
				}
			} */
			
			$dirArray = array(
				'kehadiran' => $kehadiran,
				'izin' => $izin,
				'cuti' => $cuti,
				'sakit' => $sakit,
				'perjalanandinas' => $perjalanandinas
			);
			echo json_encode($dirArray);
		}
	}
?>