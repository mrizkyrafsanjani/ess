<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class LogMonitoringController extends CI_Controller{
		var $npkLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->library('grocery_crud');
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
		}
		
		function index() //untuk input
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Log Monitoring"))
					{
						$data = array(
							'title' => 'Log Monitoring',
							'admin' => '0'
							
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','LogMonitoring/logMonitoring_view',$data);
					}
				}
				else
				{
					redirect('login','refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		function ViewLog() //ViewUser
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Log Monitoring"))
					{
						
						$data = array(
							'title' => 'Log Monitoring',
							'admin' => '0'
						
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','LogMonitoring/logMonitoring_view',$data);
					}
				}
				else
				{
					redirect('login','refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function logDetail($idLogSelected = "") //ViewUser
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Log Monitoring"))
					{
						try{
			
							$crud = new grocery_crud();			
							$crud->set_subject('Log Monitoring Detail');
							$crud->set_theme('datatables');
							
							$crud->set_table('monitoringlog_detail');
							$crud->unset_add();
							$crud->unset_delete();
							$crud->unset_edit();
							$crud->unset_read();
							$crud->where('IdLog', $idLogSelected);
							$crud->columns('employeeNumber', 'employeeFullName', 'employeeDpaNumber', 'employeeMenuAccess','accessDate');
							$crud->order_by('accessDate','ASC');
							//
							$crud->display_as('employeeNumber','NPK')->display_as('employeeFullName','Nama Lengkap')->display_as('employeeDpaNumber','Nomor Peserta')->display_as('employeeMenuAccess','Akses Menu')->display_as('accessDate','Waktu Akses');

							$output = $crud->render();
							$data = array(
								'title' => 'Log Monitoring',
								'body' => $output
							); 
							//$this->load->helper(array('form','url'));
							//$this->template->load('default','templates/CRUD_view',$data);
							
							$this->load->view('LogMonitoring/log_view',$data); 
						}
						catch(Exception $e)
						{
							log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
							throw new Exception( 'Something really gone wrong', 0, $e);
						}
						
					}
				}
				else
				{
					redirect('login','refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		
		
	}
?>