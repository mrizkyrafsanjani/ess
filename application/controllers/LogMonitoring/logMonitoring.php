<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class LogMonitoring extends CI_Controller {
	var $npkLogin;
	var $NPKSelectedUser;
    function __construct()
    {
        parent::__construct();
		$this->load->model('menu','',TRUE);
		$this->load->model('user','',TRUE);
		$this->load->library('grocery_crud');
    }
 
    public function index($tahun='',$bulan='')
    {
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->_logMonitoring($tahun,$bulan);
		}else{
			redirect('login','refresh');
		}
    }	
	
	public function user($tahun='',$bulan='',$nama='',$NPKuser='')
    {
	
		if($tahun == '')
			$tahun = date('Y');
		if($bulan == '')
			$bulan = date('n');
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			$this->npkLogin = $session_data['npk'];
			$this->NPKSelectedUser = $NPKuser;
			$this->_logMonitoring($tahun,$bulan);
		}else{
			redirect('login','refresh');
		}
    }
	public function _logMonitoring($tahun='',$bulan='')
    {
		try{
			
			$crud = new grocery_crud();			
			$crud->set_subject('Log Monitoring');
			$crud->set_theme('datatables');
			
			$crud->set_table('monitoringlog');
			$crud->where('Count > 0');
			//$crud->set_relation('NPK','mstruser','Nama',array('deleted' => '0'));
			
			if($tahun != '')
			{
				$crud->where('YEAR(monitoringlog.DataTanggal)',$tahun);
			}
			if($bulan != '')
			{
				$crud->where('MONTH(monitoringlog.DataTanggal)', $bulan);
			}
			
			$crud->columns('DataTanggal', 'CreatedOn', 'Count');
			$crud->order_by('CreatedOn','DESC');
			$crud->display_as('DataTanggal','Data Tanggal')->display_as('CreatedOn','Waktu Tarik Log')->display_as('Count','Jumlah');
			//$crud->fields('IdLog', 'WaktuTarikLog', 'Jumlah');
			
			$crud->unset_operations();
			$crud->add_action('View Detail','','logMonitoring/logMonitoringController/logDetail');
		
			$output = $crud->render();
			$this-> _outputview($output);        
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
    }

    function _outputview($output = null)
    {
		$data = array(
			'title' => 'Log Monitoring',
			'body' => $output
		); 
		//$this->load->helper(array('form','url'));
		//$this->template->load('default','templates/CRUD_view',$data);
		
        $this->load->view('LogMonitoring/log_view',$data);
    }
	
	function view($idLog){
		try{
			
			$crud = new grocery_crud();			
			$crud->set_subject('Log Monitoring Detail');
			$crud->set_theme('datatables');
			
			$crud->set_table('monitoringlog_detail');
		
			$crud->where('IdLog', $idLog);

			
			$output = $crud->render();
			$this-> _outputview($output);        
		}
		catch(Exception $e)
		{
			log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			throw new Exception( 'Something really gone wrong', 0, $e);
		}
		

	}
	
	
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */