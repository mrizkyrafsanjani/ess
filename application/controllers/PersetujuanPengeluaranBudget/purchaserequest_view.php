<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title><?php $title ?></title>
		<script src="<?php echo $this->config->base_url(); ?>assets/js/autoNumeric.js" type="text/javascript"></script>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->		
		<style>
		td {padding:5px 5px 5px 5px;}
		</style>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Purchase Request</h3>
			</div>
			<div class="panel-body">
				<?php echo validation_errors(); ?>
				<form method="Post" onsubmit="cetakSubmitPurchaseRequest()" name="CreatePurchaseRequest" id="CreatePurchaseRequest">
					<table border=0 class="table table-condensed" >
						<tr>
							<td>No. Persetujuan Pengeluaran *</td>
							<td>
								<input class="form-control" type="text" id="txtNoPP" name="txtNoPP" value = "" readonly/>
							</td>
						</tr>
						<tr>
							<td>DPA *</td>
							<td><select class="form-control select2" style="width:100%" id="txtDPA" name="txtDPA">
								<option value='1'>1</option>
								<option value='2'>2</option>
							</select></td>
						</tr>
						<tr>
							<td>Tahun Budget</td>
							<td>
								<select class="form-control select2" id="txtTahunBudget" name="txtTahunBudget">
									<option value =''></option>
								<?php 
								foreach($dataTahunBudget as $row){
									echo "<option value='".$row->Tahun."'>".$row->Tahun."</option>";
								} ?>
							</td>
						</tr>
						<tr>
							<td>Nama Activity</td>
							<td>
								<input class="form-control" type="text" id="txtKodeActivity" name="txtKodeActivity"/>
								<select class="form-control select2" id="txtNamaActivity" name="txtNamaActivity">
									<option value =''></option>
								<?php foreach($dataActivity as $row){
									echo "<option value='".$row->KodeActivity."'>".$row->NamaActivity." (". $row->KodeActivity.")</option>";
								} ?>
								</select><div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
							</td>
						</tr>
						<tr>
							<td>Jenis Pengeluaran</td>
							<td>
							<input class="form-control" type="hidden" id="hidKodeCoa" name="hidKodeCoa" readonly/>
							<input class="form-control" type="text" id="txtOpexCapex" name="txtOpexCapex" readonly/></td>
						</tr>
						<tr>
							<td>Keterangan</td>
							<td>
							<input class="form-control" type="text" id="txtKeterangan" name="txtKeterangan"/>
							</td>
						</tr>
						<tr>
							<td>Harga Satuan</td>
							<td>
								<input class="form-control" type="text" id="txtHargaSatuan" name="txtHargaSatuan"/>
							</td>
						</tr>
						<tr>
							<td>Quantity</td>
							<td>
								<input class="form-control" type="text" id="txtQuantity" name="txtQuantity"/>
							</td>
						</tr>
						<tr>
							<td>Total Pengeluaran</td>
							<td>
								<input class="form-control" type="text" id="txtTotalPengeluaran" name="txtTotalPengeluaran" readonly/>
							</td>
						</tr>
						<tr class="">
							<td>BUDGET</td>
							<td>
								<input class="form-control" type="text" id="txtBudget" name="txtBudget" readonly/>
							</td>
						</tr>
						<tr class="">
							<td>REALISASI</td>
							<td>
								<input class="form-control" type="text" id="txtRealisasi" name="txtRealisasi" readonly/>
							</td>
						</tr>
						<tr class="">
							<td>REALISASI BERJALAN</td>
							<td>
								<input class="form-control" type="text" id="txtRealisasiBerjalan" name="txtRealisasiBerjalan" readonly/>
							</td>
						</tr>
						<tr class="">
							<td>PENGAJUAN</td>
							<td>
								<input class="form-control" type="text" id="txtPengajuan" name="txtPengajuan" readonly/>
							</td>
						</tr>
						<tr class="">
							<td>SISA BUDGET</td>
							<td>
								<input class="form-control" type="text" id="txtSisaBudget" name="txtSisaBudget" readonly/>
							</td>
						</tr>
						<tr>
							<td>STATUS</td>
							<td>
								<select class="form-control select2" id="txtSelStatus" name="txtSelStatus">
									<option value='Budgeted'>Budgeted</option>
									<option value='Unbudgeted'>Unbudgeted</option>
									<option value='Overbudget'>Overbudget</option>
								</select></td>
						</tr>
						<tr>
							<td colspan = "2">
							<input class="btn btn-primary col-sm-2" type="submit" id="btnSubmit" name="" value="Simpan dan Cetak">
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
		$( window ).load(function() {
			loadActivity();
			$('#txtKodeActivity').hide();
			$(".select2").select2();
		});
		function ambilValueTxtBudget(){
			var hargaSatuan = document.getElementById('txtHargaSatuan').value;
			var quantity = document.getElementById('txtQuantity').value;
			var Pengeluaran = document.getElementById('txtTotalPengeluaran').value;
			if(hargaSatuan == ''){
				alert("Harga satuan harus di isi!");
				flag = false;
			}
			if(quantity == ''){
				alert("Quantity harus di isi!");
				flag = false;
			}
			
			var Budget = document.getElementById('txtBudget').value;
			var Realisasi = document.getElementById('txtRealisasi').value;
			var RealisasiBerjalan = document.getElementById('txtRealisasiBerjalan').value;
		}
		function loadActivity(){
			$('#divLoadingSubmit').show();
			clear();
			var DPA = document.getElementById('txtDPA').value;
			var TahunBudget = document.getElementById('txtTahunBudget').value;

			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/BudgetMonitoring/BudgetMonitoringController/ajax_loadActivityBasedDPA',
				type: "POST",
				data: {DPA : DPA, admin: <?php echo $admin; ?>, TahunBudget: TahunBudget},
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					$('#txtKodeActivity').val('');
					$('#txtNamaActivity').empty(); //remove all child nodes
					var newOption = $('<option value=""></option>');
					$('#txtNamaActivity').append(newOption);
					for(opsi in data)
					{
						$('#txtNamaActivity').append($('<option value="'+data[opsi].KodeActivity+'">'+data[opsi].NamaActivity+' ('+ data[opsi].KodeActivity +')</option>'));
					}
					$('#txtNamaActivity').trigger("chosen:updated");
					$('#divLoadingSubmit').hide();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
		$('#txtDPA').change(function(){
			loadActivity();
		});
		$('#txtTahunBudget').change(function(){
			loadActivity();
		});
		$('#txtKodeActivity').keyup(function(){
			var KodeActivity = document.getElementById('txtKodeActivity').value;
			$('#txtNamaActivity').val(KodeActivity);
		});

		$('#txtNamaActivity').change(function(){
			$('#txtKodeActivity').val($('#txtNamaActivity').val());
			var KodeActivity = $('#txtKodeActivity').val();
			var DPA = document.getElementById('txtDPA').value;
			var TahunBudget = document.getElementById('txtTahunBudget').value;
			clear();
			$('#divLoadingSubmit').show();
			if(DPA == '1' || DPA == '2'){
				loadActivityBasedOnDPA(KodeActivity, DPA, TahunBudget);
			}
			//$('#txtDPA').val(KodeActivity.substr(KodeActivity.length - 1));
		});

		function loadActivityBasedOnDPA(KodeActivity, DPA, TahunBudget){
			var DPA = DPA;
			var kodeAcivity = KodeActivity;
			var TahunBudget = TahunBudget;
			var NoPP = document.getElementById("txtNoPP").value;
			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_kalkulasiBudgetRealisasi',
				type: "POST",
				data: { KodeActivity: KodeActivity, DPA: DPA, TahunBudget: TahunBudget },
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_getSummaryBudget',
						type: "POST",
						data: { KodeActivity: KodeActivity, DPA: DPA, NoPP: NoPP, Source: "PRST", TahunBudget: TahunBudget },
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log(data);
							if(data[2] == "NoBudget"){
								var $select = jQuery( '#txtSelStatus' );
								$select.val("Unbudgeted").change();
								alert('Tidak ada budget DPA '+DPA+' untuk activity ini');
								$('#hidKodeCoa').val(data[0].KodeCoa);
								$('#txtOpexCapex').val(data[0].JenisPengeluaran);
								$('#txtBudget').val(0);
								$('#txtRealisasi').val(0);
								$('#txtRealisasiBerjalan').val(data[1].TotalRealisasiPengajuan);
							}else{
								var $select = jQuery( '#txtSelStatus' );
								$select.val("Budgeted").change();
								if(data[0].BudgetFullYear == 0){
									$select.val("Unbudgeted").change();
								}
								$('#txtBudget').val(data[0].BudgetFullYear);
								$('#txtRealisasi').val(data[0].RealisasiYTD);
								$('#hidKodeCoa').val(data[0].KodeCoa);
								$('#txtRealisasiBerjalan').val(data[1].TotalRealisasiPengajuan);
								$('#txtOpexCapex').val(data[0].JenisPengeluaran);
							}
							$('#divLoadingSubmit').hide();
						},
						error: function (request, status, error) {
							console.log(error);
							$('#divLoadingSubmit').hide();
						}
					});
					$('#divLoadingSubmit').hide();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
	</script>
	<script type="text/javascript">
		function clear(){
			$('#txtTotalPengeluaran').val('');
			$('#txtHargaSatuan').val('');
			$('#txtQuantity').val('');
			$('#txtBudget').val('');
			$('#txtRealisasi').val('');
			$('#txtPengajuan').val('');
			$('#txtRealisasiBerjalan').val('');
			$('#txtSisaBudget').val('');
			$('#hidKodeCoa').val('');
		}
		$(function($) {
			$('#txtPengeluaran').autoNumeric('init', { lZero: 'deny', aSep: ',', mDec: 0 });
			$('#txtQuantity').autoNumeric('init', { lZero: 'deny', aSep: ',', mDec: 0 });
			$('#txtHargaSatuan').autoNumeric('init', { lZero: 'deny', aSep: ',', mDec: 0 });
			$('#txtTotalPengeluaran').autoNumeric('init', { lZero: 'deny', aSep: ',', mDec: 0 });
		});
		var nf = new Intl.NumberFormat();
		
		$("#txtHargaSatuan, #txtQuantity").on('change', function(){
			var budget = document.getElementById('txtBudget').value;
			var realisasi = document.getElementById('txtRealisasi').value;
			var realisasiBerjalan = document.getElementById('txtRealisasiBerjalan').value;
			budget = budget.replace(/,/g, "");
			realisasi = realisasi.replace(/,/g, "");
			realisasiBerjalan = realisasiBerjalan.replace(/,/g, "");
			var hargaSatuan = document.getElementById('txtHargaSatuan').value;
			var quantity = document.getElementById('txtQuantity').value;
			hargaSatuan = hargaSatuan.replace(/,/g, "");
			quantity = quantity.replace(/,/g, "");
			var totalPengeluaran = hargaSatuan * quantity;
			var sisaBudget =  budget - realisasi - realisasiBerjalan - totalPengeluaran;
			document.getElementById('txtTotalPengeluaran').value = nf.format(totalPengeluaran);
			document.getElementById('txtPengajuan').value = nf.format(totalPengeluaran);
			document.getElementById('txtSisaBudget').value = nf.format(sisaBudget);
			var $select = jQuery( '#txtSelStatus' );
			if($select.val()!='Unbudgeted'){
				if(sisaBudget < 0){
					alert('Sisa Budget kurang dari 0');
					$select.val("Overbudget").change();
				}
			}
		});
	</script>
	<script type="text/javascript">
		function cetakSubmitPurchaseRequest(){
			var DPA = document.getElementById('txtDPA').value;
			var NPK = '<?php echo $npk; ?>';
			var KodeActivity = document.getElementById('txtKodeActivity').value;
			var NamaActivity = document.getElementById('txtNamaActivity').value;
			if(NamaActivity == ''){
				alert('Harap Memilih activity yang akan di submit!');
				flag = false;
				event.preventDefault();
			}else{
				flag = true;
				ambilValueTxtBudget();
				if(flag){
					var x = window.confirm("Cetak PurchaseRequest Ini?");
					if(x){
						$("#CreatePurchaseRequest").attr("action"); //will retrieve it
						$("#CreatePurchaseRequest").attr("target", "_blank"); //will retrieve it
						$("#CreatePurchaseRequest").attr("action", "<?php echo site_url('PersetujuanPengeluaranBudget/PPBController/CreatePurchaseRequest') ?>");
						window.alert("Data sudah tersimpan, silahkan Tutup halaman ini");1
					}else{
						event.preventDefault();
					}
				}else{
					event.preventDefault();
				}
			}
		}
		</script>
</html>
