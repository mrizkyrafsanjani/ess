<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title><?php $title ?></title>
		<script src="<?php echo $this->config->base_url(); ?>assets/js/autoNumeric.js" type="text/javascript"></script>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->		
		<style>
			td {padding:5px 5px 5px 5px;}
		</style>
		
		<script type="text/javascript">
			function alertSizeDPA(pixels){
				pixels+=32;
				var DPA = document.getElementById('txtDPA').value;
				if(DPA == 1){
					document.getElementById('iFrameKartuBayarDPA1').style.height=pixels+"px";
				}else if(DPA == 2){
					document.getElementById('iFrameKartuBayarDPA2').style.height=pixels+"px";
				}else{
					document.getElementById('iFrameKartuBayarDPA1').style.height=pixels+"px";
					document.getElementById('iFrameKartuBayarDPA2').style.height=pixels+"px";
				}
			}
		</script>
		<script type="text/javascript">
			function submitKartuBayarProposal(ID, Tipe){
				flag = true;
				if(flag){
					if(Tipe == 1){
						var x = window.confirm("Submit Kartu Bayar Proposal Ini?");
					}else if(Tipe == 0){
						var x = window.confirm("Batalkan Kartu Bayar Proposal Ini?");
					}
					if(x){
						var ID = ID;
						var Tipe = Tipe;
						$('#divLoadingSubmit').show();
						$.ajax({
							url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_submitKartuBayar',
							type: "POST",
							data: { ID: ID, Tipe:Tipe},
							dataType: 'json',
							cache: false,
							success: function(data)
							{
								if(Tipe == 1){
									location.reload();
									alert('Sukses Submit');
								}else if(Tipe == 0){
									alert('Sukses Batal Submit');
									location.reload();
								}
								$('#divLoadingSubmit').hide();
							},
							error: function (request, status, error) {
								console.log(error);
								alert('Error');
							}
						});
					}else{
						event.preventDefault();
					}
				}else{
					event.preventDefault();
				}
			}
			function releaseKartuBayarPR(ID, Tipe){
				flag = true;
				if(flag){
					if(Tipe == 1){
						var x = window.confirm("Release Kartu Bayar PR Ini?");
					}else if(Tipe == 0){
						var x = window.confirm("Batalkan Kartu Bayar PR Ini?");
					}
					if(x){
						var ID = ID;
						var Tipe = Tipe;
						$('#divLoadingSubmit').show();
						$.ajax({
							url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_releaseKartuBayar',
							type: "POST",
							data: { ID: ID, Tipe:Tipe},
							dataType: 'json',
							cache: false,
							success: function(data)
							{
								console.log(data);
								if(Tipe == 1){
									alert('Sukses Release');
								}else if(Tipe == 0){
									alert('Sukses Batal Release');
								}
								location.reload();
								$('#divLoadingSubmit').hide();
							},
							error: function (request, status, error) {
								console.log(error);
								alert('Error');
							}
						});
					}else{
						event.preventDefault();
					}
				}else{
					event.preventDefault();
				}
			}
			function tutupKartuBayarPR(ID, Tipe){
				flag = true;
				if(flag){
					if(Tipe == 1){
						var x = window.confirm("Tutup Kartu Bayar Proposal Ini?");
					}else if(Tipe == 0){
						var x = window.confirm("Batalkan Tutup Kartu Bayar Proposal Ini?");
					}
					if(x){
						var ID = ID;
						var Tipe = Tipe;
						$('#divLoadingSubmit').show();
						$.ajax({
							url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_tutupKartuBayar',
							type: "POST",
							data: { ID: ID, Tipe:Tipe},
							dataType: 'json',
							cache: false,
							success: function(data)
							{
								console.log(data);
								if(Tipe == 1){
									alert('Sukses Tutup');
								}else if(Tipe == 0){
									alert('Sukses Batal Tutup');
								}
								location.reload();
								$('#divLoadingSubmit').hide();
							},
							error: function (request, status, error) {
								console.log(error);
								alert('Error');
							}
						});
					}else{
						event.preventDefault();
					}
				}else{
					event.preventDefault();
				}
			}
		</script>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Proposal | <a href="<?php echo $this->config->base_url().'assets/uploads/files/PPB/'. $data['FilePath']; ?>" download>download</a></h3>
			</div>
			<div class="panel-body">
				<?php echo validation_errors(); ?>
				<table border=0 class="table table-condensed" >
					<tr>
						<td>No. Persetujuan Pengeluaran *</td>
						<td>
							<input type="hidden" value = "<?php echo $data['isReleased']; ?>" id="isReleased" name="isReleased" readonly/>
							<input class="form-control" type="text" id="txtNoPP" name="txtNoPP" value="<?php echo $data['NoPP']; ?>" readonly />
						</td>
					</tr>
					<tr>
						<td>DPA *</td>
						<td>
							<input class="form-control" type="text" id="txtDPA" name="txtDPA" value="<?php echo $data['DPA']; ?>" readonly />
						</td>
					</tr>
					<tr>
						<td>Nama Activity</td>
						<td>
							<input type="hidden" value = "<?php echo $data['TahunBudget']; ?>" id="txtTahunBudget" name="txtTahunBudget" readonly/>
							<input class="form-control" type="text" id="txtKodeActivity" name="txtKodeActivity" value="<?php echo $data['KodeActivity'].' - '.$data['NamaActivity']; ?>" readonly />
						</td>
					</tr>
					<tr>
						<td>Jenis Pengeluaran</td>
						<td>
							<input class="form-control" type="hidden" id="hidKodeCoaDPA1" name="hidKodeCoaDPA1" readonly/>
							<input class="form-control" type="hidden" id="hidKodeCoaDPA2" name="hidKodeCoaDPA2" readonly/>
							<input class="form-control" type="text" id="txtOpexCapex" name="txtOpexCapex"value="<?php echo $data['JenisPengeluaran']; ?>" readonly/>
						</td>
					</tr>
					<tr>
						<td>Keterangan</td>
						<td><input class="form-control" type="text" id="txtKeterangan" name="txtKeterangan" value="<?php echo $data['Keterangan']; ?>"readonly/>
						</td>
					</tr>
					<tr class="rowDPA1">
						<td>SALDO PENGAJUAN DPA 1</td>
						<td>
							<input class="form-control" type="hidden" id="hidtxtPengeluaranDPA1" name="hidtxtPengeluaranDPA1" value="<?php echo $data['PengeluaranDPA1']; ?>"readonly/>
							<input class="form-control" type="text" id="txtPengeluaranDPA1" name="txtPengeluaranDPA1" value="<?php echo $data['PengeluaranDPA1']; ?>"readonly/>
						</td>
					</tr>
					<tr class="rowDPA1">
						<td>TOTAL PENGGUNAAN DPA 1</td>
						<td>
							<input class="form-control" type="hidden" id="hidtxtBudgetDPA1" name="hidtxtBudgetDPA1" readonly/>
							<input class="form-control" type="hidden" id="txtBudgetDPA1" name="txtBudgetDPA1" readonly/>
							<input class="form-control" type="hidden" id="hidtxtRealisasiDPA1" name="hidtxtRealisasiDPA1" readonly/>
							<input class="form-control" type="hidden" id="txtRealisasiDPA1" name="txtRealisasiDPA1" readonly/>
							<input class="form-control" type="hidden" id="hidtxtRealisasiBerjalanDPA1" name="hidtxtRealisasiBerjalanDPA1" readonly/>
							<input class="form-control" type="text" id="txtRealisasiBerjalanDPA1" name="txtRealisasiBerjalanDPA1" readonly/>
						</td>
					</tr>
					<tr class="rowDPA1">
						<td>SISA SALDO PENGAJUAN DPA 1</td>
						<td>
							<input class="form-control" type="hidden" id="hidtxtPengajuanDPA1" name="hidtxtPengajuanDPA1" readonly/>
							<input class="form-control bg-success" type="hidden" id="hidtxtSisaBudgetDPA1" name="hidtxtSisaBudgetDPA1" readonly/>
							<input class="form-control" type="hidden" id="txtSisaBudgetDPA1" name="txtSisaBudgetDPA1" readonly/>
							<input class="form-control" type="text" id="txtPengajuanDPA1" name="txtPengajuanDPA1" readonly/>
						</td>
					</tr>
					<tr class="rowDPA2">
						<td>SALDO PENGAJUAN DPA 2</td>
						<td>
							<input class="form-control" type="hidden" id="hidtxtPengeluaranDPA2" name="hidtxtPengeluaranDPA2"value="<?php echo $data['PengeluaranDPA2']; ?>"readonly/>
							<input class="form-control" type="text" id="txtPengeluaranDPA2" name="txtPengeluaranDPA2"value="<?php echo $data['PengeluaranDPA2']; ?>"readonly/>
						</td>
					</tr>
					<tr class="rowDPA2">
						<td>TOTAL PENGGUNAAN DPA 2</td>
						<td>
							<input class="form-control" type="hidden" id="hidtxtBudgetDPA2" name="hidtxtBudgetDPA2" readonly/>
							<input class="form-control" type="hidden" id="txtBudgetDPA2" name="txtBudgetDPA2" readonly/>
							<input class="form-control" type="hidden" id="hidtxtRealisasiDPA2" name="hidtxtRealisasiDPA2" readonly/>
							<input class="form-control" type="hidden" id="txtRealisasiDPA2" name="txtRealisasiDPA2" readonly/>
							<input class="form-control" type="hidden" id="hidtxtRealisasiBerjalanDPA2" name="hidtxtRealisasiBerjalanDPA2" readonly/>
							<input class="form-control" type="text" id="txtRealisasiBerjalanDPA2" name="txtRealisasiBerjalanDPA2" readonly/>
						</td>
					</tr>
					<tr class="rowDPA2">
						<td>SISA SALDO PENGAJUAN DPA 2</td>
						<td>
							<input class="form-control" type="hidden" id="hidtxtPengajuanDPA2" name="hidtxtPengajuanDPA2" readonly/>
							<input class="form-control bg-success" type="hidden" id="hidtxtSisaBudgetDPA2" name="hidtxtSisaBudgetDPA2" readonly/>
							<input class="form-control bg-success" type="hidden" id="txtSisaBudgetDPA2" name="txtSisaBudgetDPA2" readonly/>
							<input class="form-control" type="text" id="txtPengajuanDPA2" name="txtPengajuanDPA2"  readonly/>
						</td>
					</tr>
					<?php if($data['isReleased'] == 0 && $user[0]->departemen == "HRGA"){
					?>
					<tr>
						<td>
							<button class="btn btn-warning" onclick="releaseKartuBayarPR(<?php echo $data['ID']; ?>, 1)">Release Kartu Bayar</button>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
						<td></td>
					</tr>
					<?php
					}
					?>
					<?php if($data['isReleased'] == 1 && $user[0]->departemen == "HRGA"){
					?>
					<tr>
						<td>
							<button class="btn btn-danger" onclick="releaseKartuBayarPR(<?php echo $data['ID']; ?>, 0)">Batal Release Kartu Bayar</button>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
						<td></td>
					</tr>
					<?php
					}
					?>
					<?php if($data['isFinished'] == 0 && $user[0]->departemen == "HRGA"){
					?>
					<tr>
						<td>
							<button class="btn btn-success" onclick="tutupKartuBayarPR(<?php echo $data['ID']; ?>, 1)">Tutup Kartu Bayar</button>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
					</tr>
					<?php
					}
					?>
					<?php if($data['isFinished'] == 1 && $user[0]->departemen == "HRGA"){
					?>
					<tr>
						<td>
							<button class="btn btn-success" onclick="tutupKartuBayarPR(<?php echo $data['ID']; ?>, 0)">Batal Tutup Kartu Bayar</button>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
					</tr>
					<?php
					}
					?>
					<?php
					if($data['DPA'] == 1 && $data['isReleased'] == 1 && $data['isFinished'] == 0){
					?>
					<tr>
						<th colspan="2">
							KARTU BAYAR DPA 1
						</th>
					</tr>
					<tr>
						<td colspan="2">
							<iframe id="iFrameKartuBayarDPA1" src="<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/KartuBayar/KartuBayarDPA1/<?php echo $data['ID']; ?>" width="100%"seamless frameBorder="0">
								<p>Your browser does not support iframes.</p>
							</iframe>
						</td>
					</tr>
					<tr>
						<td>
							<button class="btn btn-success" onclick="submitKartuBayarProposal(<?php echo $data['ID']; ?>, 1)">Submit Kartu Bayar</button>
							<button class="btn btn-danger" onclick="submitKartuBayarProposal(<?php echo $data['ID']; ?>, 0)">Batal Submit Kartu Bayar</button>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
					</tr>
					<?php
					}else if($data['DPA'] == 2 && $data['isReleased'] == 1 && $data['isFinished'] == 0){
						?>
						<tr>
							<th colspan="2">
								KARTU BAYAR DPA 2
							</th>
						</tr>
						<tr>
							<td colspan="2">
								<iframe id="iFrameKartuBayarDPA2" src="<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/KartuBayar/KartuBayarDPA2/<?php echo $data['ID']; ?>" width="100%"seamless frameBorder="0">
									<p>Your browser does not support iframes.</p>
								</iframe>
							</td>
						</tr>
						<?php
					}else if($data['DPA'] == 'all'  && $data['isReleased'] == 1 && $data['isFinished'] == 0){
						?>
						<tr>
							<th colspan="2">
								KARTU BAYAR DPA 1
							</th>
						</tr>
						<tr>
							<td colspan="2">
								<iframe id="iFrameKartuBayarDPA1" src="<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/KartuBayar/KartuBayarDPA1/<?php echo $data['ID']; ?>" width="100%"seamless frameBorder="0">
									<p>Your browser does not support iframes.</p>
								</iframe>
							</td>
						</tr>
						<tr>
							<td>
								<?php if($adaYangPerluDisubmit){
								?>
									<button class="btn btn-success" onclick="submitKartuBayarProposal(<?php echo $data['ID']; ?>, 1)">Submit Kartu Bayar</button>
								<?php
								}if($adaYangPerluDiBatalkan){
								?>
									<button class="btn btn-danger" onclick="submitKartuBayarProposal(<?php echo $data['ID']; ?>, 0)">Batal Submit Kartu Bayar</button>
								<?php
								}?>
								<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
							</td>
						</tr>
						<tr>
							<th colspan="2">
								KARTU BAYAR DPA 2
							</th>
						</tr>
						<tr>
							<td colspan="2">
								<iframe id="iFrameKartuBayarDPA2" src="<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/KartuBayar/KartuBayarDPA2/<?php echo $data['ID']; ?>" width="100%"seamless frameBorder="0">
									<p>Your browser does not support iframes.</p>
								</iframe>
							</td>
						</tr>
						<?php
					}
					?>
				</table>
				<div>
					<div class="col-md-12"> 
						<iframe src="<?php echo $this->config->base_url().'assets/uploads/files/PPB/'. $data['FilePath']; ?>" width="100%" height="720px">
						This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->config->base_url().'assets/uploads/files/ppb/'. $data->FilePath; ?>">Download PDF</a>
						</iframe>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
		$( window ).load(function() {
			var TahunBudget = document.getElementById('txtTahunBudget').value;
			$('#txtPengeluaranDPA1').hide();
			$('#txtRealisasiBerjalanDPA1').hide();
			$('#txtPengajuanDPA1').hide();
			$('#txtPengeluaranDPA2').hide();
			$('#txtRealisasiBerjalanDPA2').hide();
			$('#txtPengajuanDPA2').hide();
			var isReleased = document.getElementById('isReleased').value;
			if(isReleased == 1){
				$('#txtPengeluaranDPA1').show();
				$('#txtRealisasiBerjalanDPA1').show();
				$('#txtPengajuanDPA1').show();
				$('#txtPengeluaranDPA2').show();
				$('#txtRealisasiBerjalanDPA2').show();
				$('#txtPengajuanDPA2').show();
			}
			var DPA = document.getElementById('txtDPA').value;
			if(DPA == 1){
				$('.rowDPA1').show();
				$('.rowDPA2').hide();
			}else if(DPA == 2){
				$('.rowDPA1').hide();
				$('.rowDPA2').show();
			}else{
				$('.rowDPA1').show();
				$('.rowDPA2').show();
			}
			if(DPA == '1' || DPA == '2'){
				var KodeActivity = (document.getElementById('txtKodeActivity').value).substring(0,6);
				loadActivityBasedOnDPA(KodeActivity, DPA, TahunBudget);
				if(DPA == '1'){
					$('#txtPengeluaranDPA1').prop('disabled','');
					$('#txtPengeluaranDPA2').prop('disabled','true');
					pengeluaranDPA1beforeChanged = document.getElementById('txtPengeluaranDPA1').value;
				}else{
					$('#txtPengeluaranDPA1').prop('disabled','true');
					$('#txtPengeluaranDPA2').prop('disabled','');
					pengeluaranDPA2beforeChanged = document.getElementById('txtPengeluaranDPA2').value;
				}
			}else{
				var KodeActivity = (document.getElementById('txtKodeActivity').value).substring(0,5);
				loadActivityBasedOnDPA(KodeActivity+'1', '1', TahunBudget);
				loadActivityBasedOnDPA(KodeActivity+'2', '2', TahunBudget);
				$('#txtPengeluaranDPA1').prop('disabled','');
				$('#txtPengeluaranDPA2').prop('disabled','');
				pengeluaranDPA1beforeChanged = document.getElementById('txtPengeluaranDPA1').value;
				pengeluaranDPA2beforeChanged = document.getElementById('txtPengeluaranDPA2').value;
			}
		});
		function ambilValueTxtBudgetDPA1(){
			var PengeluaranDPA1 = document.getElementById('txtPengeluaranDPA1').value;
			if(PengeluaranDPA1 == ''){
				alert("Pengeluaran DPA 1 harus di isi!");
				flag = false;
			}
			var BudgetDPA1 = document.getElementById('txtBudgetDPA1').value;
			var RealisasiDPA1 = document.getElementById('txtRealisasiDPA1').value;
			var RealisasiBerjalanDPA1 = document.getElementById('txtRealisasiBerjalanDPA1').value;
		}
		function ambilValueTxtBudgetDPA2(){
			var PengeluaranDPA2 = document.getElementById('txtPengeluaranDPA2').value;
			if(PengeluaranDPA2 == ''){
				alert("Pengeluaran DPA 2 harus di isi!");
				flag = false;
			}
			var BudgetDPA2 = document.getElementById('txtBudgetDPA2').value;
			var RealisasiDPA2 = document.getElementById('txtRealisasiDPA2').value;
			var RealisasiBerjalanDPA2 = document.getElementById('txtRealisasiBerjalanDPA2').value;
		}

		function loadActivityBasedOnDPA(KodeActivity, DPA, TahunBudget){
			var DPA = DPA;
			var KodeActivity = KodeActivity;
			var TahunBudget = TahunBudget;
			var NoPP = document.getElementById("txtNoPP").value;
			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_kalkulasiBudgetRealisasi',
				type: "POST",
				data: { KodeActivity: KodeActivity, DPA: DPA, TahunBudget: TahunBudget },
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_getSummaryBudget',
						type: "POST",
						data: { KodeActivity: KodeActivity, DPA: DPA, NoPP: NoPP, Source: "KartuBayar", TahunBudget: TahunBudget },
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log('Result getSummaryBudget');
							console.log(data);
							console.log('Result getSummaryBudget end');
							if(data.length == 1){
								alert('Tidak ada budget DPA '+DPA+' untuk activity ini');
								if(DPA == 1){
									$('#txtPengeluaranDPA1').prop('disabled','true');
								}else{
									$('#txtPengeluaranDPA2').prop('disabled','true');
								}
							}else{
								if(DPA == 1){
									$('#txtBudgetDPA1').val(data[0].BudgetFullYear);
									$('#txtRealisasiDPA1').val(data[0].RealisasiYTD);
									$('#hidKodeCoaDPA1').val(data[0].KodeCoa);
									$('#txtRealisasiBerjalanDPA1').val(data[1].TotalRealisasiPengajuan);
									$('#txtOpexCapex').val(data[0].JenisPengeluaran);
									var budgetDPA1 = document.getElementById('txtBudgetDPA1').value;
									var realisasiDPA1 = document.getElementById('txtRealisasiDPA1').value;
									var realisasiBerjalanDPA1 = document.getElementById('txtRealisasiBerjalanDPA1').value;
									var nominalPengeluaran = document.getElementById('txtPengeluaranDPA1').value;
									document.getElementById('hidtxtPengeluaranDPA1').value = nominalPengeluaran;

									budgetDPA1 = budgetDPA1.replace(/,/g, "");
									realisasiDPA1 = realisasiDPA1.replace(/,/g, "");
									realisasiBerjalanDPA1 = realisasiBerjalanDPA1.replace(/,/g, "");
									nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
									document.getElementById('hidtxtBudgetDPA1').value = budgetDPA1;
									document.getElementById('hidtxtPengajuanDPA1').value = nominalPengeluaran;
									document.getElementById('txtPengajuanDPA1').value = nf.format(nominalPengeluaran-realisasiBerjalanDPA1);

									var sisaBudgetDPA1 = budgetDPA1 - realisasiDPA1 - realisasiBerjalanDPA1;
									document.getElementById('hidtxtSisaBudgetDPA1').value = sisaBudgetDPA1;
									document.getElementById('txtSisaBudgetDPA1').value = nf.format(sisaBudgetDPA1);
									if(sisaBudgetDPA1 < 0){
										alert('Sisa Budget DPA 1 kurang dari 0');
									}
								}else if(DPA == 2){
									$('#txtBudgetDPA2').val(data[0].BudgetFullYear);
									$('#txtRealisasiDPA2').val(data[0].RealisasiYTD);
									$('#hidKodeCoaDPA2').val(data[0].KodeCoa);
									$('#txtRealisasiBerjalanDPA2').val(data[1].TotalRealisasiPengajuan);
									var budgetDPA2 = document.getElementById('txtBudgetDPA2').value;
									var realisasiDPA2 = document.getElementById('txtRealisasiDPA2').value;
									var realisasiBerjalanDPA2 = document.getElementById('txtRealisasiBerjalanDPA2').value;
									var nominalPengeluaran = document.getElementById('txtPengeluaranDPA2').value;
									budgetDPA2 = budgetDPA2.replace(/,/g, "");
									realisasiDPA2 = realisasiDPA2.replace(/,/g, "");
									realisasiBerjalanDPA2 = realisasiBerjalanDPA2.replace(/,/g, "");
									nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
									document.getElementById('hidtxtBudgetDPA2').value = budgetDPA2;
									document.getElementById('hidtxtPengajuanDPA2').value = nominalPengeluaran;
									document.getElementById('txtPengajuanDPA2').value = nf.format(nominalPengeluaran-realisasiBerjalanDPA2);

									var sisaBudgetDPA2 = budgetDPA2 - realisasiDPA2 - realisasiBerjalanDPA2;
									document.getElementById('hidtxtSisaBudgetDPA2').value = sisaBudgetDPA2;
									document.getElementById('txtSisaBudgetDPA2').value = nf.format(sisaBudgetDPA2);
									if(sisaBudgetDPA2 < 0){
										alert('Sisa Budget DPA 2 kurang dari 0');
									}
								}
							}
							$('#divLoadingSubmit').hide();
						},
						error: function (request, status, error) {
							console.log(error);
							$('#divLoadingSubmit').hide();
						}
					});
					$('#divLoadingSubmit').hide();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
	</script>
	<script type="text/javascript">
		function clear(){
			$('#txtPengeluaranDPA1').prop('disabled','true');
			$('#txtPengeluaranDPA2').prop('disabled','true');
			$('#txtPengeluaranDPA1').val('');
			$('#txtPengeluaranDPA2').val('');
			$('#txtBudgetDPA1').val('');
			$('#txtBudgetDPA2').val('');
			$('#hidtxtBudgetDPA1').val('');
			$('#hidtxtBudgetDPA2').val('');
			$('#txtRealisasiDPA1').val('');
			$('#txtRealisasiDPA2').val('');
			$('#hidtxtRealisasiDPA1').val('');
			$('#hidtxtRealisasiDPA2').val('');
			$('#txtPengajuanDPA1').val('');
			$('#txtPengajuanDPA2').val('');
			$('#hidtxtPengajuanDPA1').val('');
			$('#hidtxtPengajuanDPA2').val('');
			$('#txtRealisasiBerjalanDPA1').val('');
			$('#txtRealisasiBerjalanDPA2').val('');
			$('#hidtxtRealisasiBerjalanDPA1').val('');
			$('#hidtxtRealisasiBerjalanDPA2').val('');
			$('#txtSisaBudgetDPA1').val('');
			$('#txtSisaBudgetDPA2').val('');
			$('#hidtxtSisaBudgetDPA1').val('');
			$('#hidtxtSisaBudgetDPA2').val('');
			$('#hidtxtKodeCoaDPA1').val('');
			$('#hidtxtKodeCoaDPA2').val('');
		}
		$(function($) {
			$('#txtPengeluaranDPA1').autoNumeric('init', { lZero: 'deny', aSep: ',', mDec: 0 });
			$('#txtPengeluaranDPA2').autoNumeric('init', { lZero: 'deny', aSep: ',', mDec: 0 });
		});
		var nf = new Intl.NumberFormat();
		$("#txtPengeluaranDPA1").on('change', function() {
			var budgetDPA1 = document.getElementById('txtBudgetDPA1').value;
			var realisasiDPA1 = document.getElementById('txtRealisasiDPA1').value;
			var realisasiBerjalanDPA1 = document.getElementById('txtRealisasiBerjalanDPA1').value;
			var nominalPengeluaran = document.getElementById('txtPengeluaranDPA1').value;
			document.getElementById('hidtxtPengeluaranDPA1').value = nominalPengeluaran;
			document.getElementById('txtPengajuanDPA1').value = nominalPengeluaran;
			if(pengeluaranDPA1beforeChanged != nominalPengeluaran){
				isChangedDPA1 = true;
			}else{
				isChangedDPA1 = false;
			}
			budgetDPA1 = budgetDPA1.replace(/,/g, "");
			realisasiDPA1 = realisasiDPA1.replace(/,/g, "");
			realisasiBerjalanDPA1 = realisasiBerjalanDPA1.replace(/,/g, "");
			nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
			document.getElementById('hidtxtBudgetDPA1').value = budgetDPA1;
			document.getElementById('hidtxtPengajuanDPA1').value = nominalPengeluaran;
			var sisaBudgetDPA1 = budgetDPA1 - realisasiDPA1 - realisasiBerjalanDPA1 - nominalPengeluaran;
			document.getElementById('hidtxtSisaBudgetDPA1').value = sisaBudgetDPA1;
			if(sisaBudgetDPA1 < 0){
				alert('Sisa Budget kurang dari 0');
			}
			document.getElementById('txtSisaBudgetDPA1').value = nf.format(sisaBudgetDPA1);
		});
		$("#txtPengeluaranDPA2").on('change', function() {
			var budgetDPA2 = document.getElementById('txtBudgetDPA2').value;
			var realisasiDPA2 = document.getElementById('txtRealisasiDPA2').value;
			var realisasiBerjalanDPA2 = document.getElementById('txtRealisasiBerjalanDPA2').value;
			var nominalPengeluaran = document.getElementById('txtPengeluaranDPA2').value;
			document.getElementById('hidtxtPengeluaranDPA2').value = nominalPengeluaran;
			document.getElementById('txtPengajuanDPA2').value = nominalPengeluaran;
			if(pengeluaranPA2beforeChanged != nominalPengeluaran){
				isChangedDPA2 = true;
			}else{
				isChangedDPA2 = false;
			}
			budgetDPA2 = budgetDPA2.replace(/,/g, "");
			realisasiDPA2 = realisasiDPA2.replace(/,/g, "");
			realisasiBerjalanDPA2 = realisasiBerjalanDPA2.replace(/,/g, "");
			nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
			document.getElementById('hidtxtBudgetDPA2').value = budgetDPA2;
			document.getElementById('hidtxtPengajuanDPA2').value = nominalPengeluaran;
			var sisaBudgetDPA2 = budgetDPA2 - realisasiDPA2 - realisasiBerjalanDPA2 - nominalPengeluaran;
			document.getElementById('hidtxtSisaBudgetDPA2').value = sisaBudgetDPA2;
			if(sisaBudgetDPA2 < 0){
				alert('Sisa Budget kurang dari 0');
			}
			document.getElementById('txtSisaBudgetDPA2').value = nf.format(sisaBudgetDPA2);
		});
	</script>
</html>
