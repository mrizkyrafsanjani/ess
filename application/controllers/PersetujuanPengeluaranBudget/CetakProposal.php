<html lang="en">

<head>
    <title><?php echo $NoPP; ?></title>
    <style type="text/css">
       .wrapper{
            height:277mm;
            width:210mm;
            page-break-after:always;
            size: portrait;
            font-family: Arial;
        }
        .table th{
            text-align: center;
        }
        table > tbody > tr > th {
            border-top: 1px solid black !important;
        }
        table > tbody > tr > td {
            border-top: 1px solid black !important;
        }
        table.table-bordered{
            border:1px solid black !important;
        }
        table.table-bordered > tbody > tr > th{
            border:1px solid black !important;
        }
        table.table-bordered > tbody > tr > td{
            border:1px solid black !important;
        }
        .row{
            margin-bottom: 2%;
        }
    </style>
    <link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>bootstrap/css/bootstrap.min.css">
	<script>
		function cetak(){
            var css = '@page { size: portrait; }',
            head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');

            style.type = 'text/css';
            style.media = 'print';

            if (style.styleSheet){
            style.styleSheet.cssText = css;
            } else {
            style.appendChild(document.createTextNode(css));
            }

            head.appendChild(style);

            setTimeout(function () { window.print(); }, 500);
            setTimeout(function () { 
            alert('Klik dimanapun untuk menutup halaman ini'); }, 1000);
            //window.onfocus = function () { setTimeout(function () { window.close(); }, 0); }
		}
	</script>
</head>

<body onload = "cetak()" onclick="window.close()">
    <div class="wrapper">
        <div class="row">
            <div class="col-xs-2">
                <image id="logoDPA" src='<?php echo $this->config->base_url(); ?>assets/images/logoDPA.png'>
            </div>
            <div class="col-xs-10">
                <label><b>PROPOSAL</b><br/></label>
                <br/>
                <span for=""><?php if($Keterangan != '') echo $Keterangan;?></span>
                <br/>
                <label><b>NO : </b><?php if($NoPP != '') echo $NoPP;?></label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <label>BIAYA</label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table class="table">
                    <tr>
                        <th></th>
                        <th>Budget</th>
                        <th>Pengajuan*</th>
                        <th>Under/Over</th>
                        <th>Chart of Account</th>
                    </tr>
                    <tr>
                        <td>DPA 1</td>
                        <td><?php if($txtBudgetDPA1 != '') echo $txtBudgetDPA1;?></td>
                        <td><?php if($txtPengajuanDPA1 != '') echo $txtPengajuanDPA1;?></td>
                        <td><?php if($txtSisaBudgetDPA1 != '') echo $txtSisaBudgetDPA1 ?></td>
                        <td><?php if($KodeCoaDPA1 != '') echo $KodeCoaDPA1 ?></td>
                    </tr>
                    <tr>
                        <td>DPA 2</td>
                        <td><?php if($txtBudgetDPA2 != '') echo $txtBudgetDPA2;?></td>
                        <td><?php if($txtPengajuanDPA2 != '') echo $txtPengajuanDPA2;?></td>
                        <td><?php if($txtSisaBudgetDPA2 != '') echo $txtSisaBudgetDPA2 ?></td>
                        <td><?php if($KodeCoaDPA2 != '') echo $KodeCoaDPA2 ?></td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td><?php if($totalBudget != '') echo number_format($totalBudget); ?></td>
                        <td><?php if($totalPengajuan != '') echo number_format($totalPengajuan); ?></td>
                        <td><?php if($totalSisaBudget != '') echo number_format($totalSisaBudget); ?></td>
                        <td></td>
                    </tr>
                </table>
                <br>
                <table class="table">
                    <tr>
                        <td>STATUS: </td>
                        <td><?php if($Status != '') echo $Status;?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <label for="">Jakarta, <?php echo date('d F Y')?></label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table class="table table-bordered">
                    <tr>
                        <th>Dibuat</th>
                        <th colspan="2">Diketahui</th>
                    </tr>
                    <tr>
                        <td style="height: 100px;"></td>
                        <td style="height: 100px;"></td>
                        <td style="height: 100px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 33.33%;"><?php if($namaUser != '') echo $namaUser;?><br/>Pemohon</td>
                        <td style="width: 33.33%;"><?php if($namaAtasan != '') echo $namaAtasan;?><br/>Dept Head Pemohon</td>
                        <td style="width: 33.33%;">Ratna Ikawati<br/>Head of Acct, Tax, & Control</td>
                    </tr>
                </table>
                <br><br>
                <table class="table table-bordered">
                    <tr>
                        <th colspan="4">Disetujui</th>
                    </tr>
                    <tr>
                        <td style="height: 100px;"></td>
                        <td style="height: 100px;"></td>
                        <td style="height: 100px;"></td>
                        <td style="height: 100px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 25%;">Suheri<br/>Presiden Direktur DPA 1</td>
                        <td style="width: 25%;">Chairi Pitono<br/>Direktur DPA 1</td>
                        <td style="width: 25%;">Fredyanto Manalu<br/>Presiden Direktur DPA 2</td>
                        <td style="width: 25%;">Purwaningsih<br/>Direktur DPA 2</td>
                    </tr>
                </table>

            </div>
            
            <div class="col-xs-12">
                <p><i>Kode Activity: <?php echo $KodeActivity; ?> | Nama Activity: <?php echo $NamaActivity; ?> | Tahun Budget: <?php echo $TahunBudget; ?><i></p>
                <p><i>*Detail Proposal terlampir<i></p>
            
            </div>
        </div>

    </div>
</body>
<script type="text/javascript">

</script>

</html>