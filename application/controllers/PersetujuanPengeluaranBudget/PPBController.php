<?php if (!defined('BASEPATH')) exit('Tidak boleh mengakses dengan cara ini!');
	session_start();
	class PPBController extends CI_Controller
	{
		var $npkLogin;
		function __construct()
		{
			parent::__construct();
			$this->load->model('menu','',TRUE);
			$this->load->model('user','',TRUE);
			$this->load->model('ldarchive_model','',TRUE);
			$this->load->library('grocery_crud');
			$this->load->model('ppb_model','',TRUE);
			$this->load->model('bm_activity_model','',TRUE);
			$this->load->model('bm_summarybudget_model','',TRUE);
			$this->load->model('usertask','',TRUE);
			$this->load->model('lembur_model','',TRUE);
			$this->load->library('email');
			$this->load->model('dtltrkrwy_model','',TRUE);
			$session_data = $this->session->userdata('logged_in');
			$this->npkLogin = $session_data['npk'];
		}
		function index()
		{
			try
			{
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
        public function proposal()
    	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Proposal"))
					{
						$data = array(
							'title' => 'Proposal',
							'admin' => '1',
							'dataActivity' => $this->bm_activity_model->getDataActivity($this->npkLogin,'1'),
							'npk' => $this->npkLogin,
							'dataTahunBudget' => $this->bm_activity_model->getYear()
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','PersetujuanPengeluaranBudget/proposal_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		public function CreateProposal()
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Proposal"))
					{
						$KodeActivity = strip_tags($this->input->post('txtKodeActivity'));
						$KodeActivityForView = $KodeActivity;
						$NPKAtasan = '';
						$DPA = strip_tags($this->input->post('txtDPA'));
						$NamaActivity = "";
						$NamaActivityForView = "";
						if(strlen($KodeActivity) == 5){
							$act = $this->bm_activity_model->getActivityAll($KodeActivity);
							$KodeActivityForView = $KodeActivity.'1'.', '.$KodeActivity.'2';
							$NamaActivityForView = $act[0]->NamaActivity.', '.$act[1]->NamaActivity;
							//$NamaActivity = substr($act[0]->NamaActivity, 0,strpos($act[0]->NamaActivity,"-"));
						}else{
							$act = $this->bm_activity_model->getActivity($KodeActivity);
							$NamaActivityForView = $act[0]->NamaActivity;
						}
						$DPA = strip_tags($this->input->post('txtDPA'));
						$TahunBudget = strip_tags($this->input->post('txtTahunBudget'));
						$var["TahunBudget"] = $TahunBudget;
						$JenisPengeluaran = strip_tags($this->input->post('txtOpexCapex'));
						$Keterangan = strip_tags($this->input->post('txtKeterangan'));
						$totalBudget = 0;
						$totalPengajuan = 0;
						$totalSisaBudget = 0;
						$hidKodeCoaDPA1 = strip_tags($this->input->post('hidKodeCoaDPA1'));
						$hidKodeCoaDPA2 = strip_tags($this->input->post('hidKodeCoaDPA2'));
						$txtBudgetDPA1 = strip_tags($this->input->post('txtBudgetDPA1'));
						$txtBudgetDPA2 = strip_tags($this->input->post('txtBudgetDPA2'));
						$txtRealisasiDPA1 = strip_tags($this->input->post('txtRealisasiDPA1'));
						$txtRealisasiDPA2 = strip_tags($this->input->post('txtRealisasiDPA2'));
						$txtRealisasiBerjalanDPA1 = strip_tags($this->input->post('txtRealisasiBerjalanDPA1'));
						$txtRealisasiBerjalanDPA2 = strip_tags($this->input->post('txtRealisasiBerjalanDPA2'));
						$txtPengeluaranDPA1 = strip_tags($this->input->post('txtPengeluaranDPA1'));
						$txtPengeluaranDPA2 = strip_tags($this->input->post('txtPengeluaranDPA2'));
						$txtPengajuanDPA2 = strip_tags($this->input->post('txtPengajuanDPA2'));
						$txtPengajuanDPA1 = strip_tags($this->input->post('txtPengajuanDPA1'));
						$txtSisaBudgetDPA2 = strip_tags($this->input->post('txtSisaBudgetDPA2'));
						$txtSisaBudgetDPA1 = strip_tags($this->input->post('txtSisaBudgetDPA1'));
						$var["txtBudgetDPA1"] = $txtBudgetDPA1;
						$var["txtRealisasiDPA1"] = $txtRealisasiDPA1;
						$var["txtRealisasiBerjalanDPA1"] = $txtRealisasiBerjalanDPA1;
						$var["txtPengeluaranDPA1"] = $txtPengeluaranDPA1;
						$var["txtPengajuanDPA1"] = $txtPengajuanDPA1;
						$var["txtSisaBudgetDPA1"] = $txtSisaBudgetDPA1;
						$var["txtBudgetDPA2"] = $txtBudgetDPA2;
						$var["txtRealisasiDPA2"] = $txtRealisasiDPA2;
						$var["txtRealisasiBerjalanDPA2"] = $txtRealisasiBerjalanDPA2;
						$var["txtPengeluaranDPA2"] = $txtPengeluaranDPA2;
						$var["txtPengajuanDPA2"] = $txtPengajuanDPA2;
						$var["txtSisaBudgetDPA2"] = $txtSisaBudgetDPA2;
						$txtBudgetDPA1 = str_replace(',','',$txtBudgetDPA1);
						$txtBudgetDPA2 = str_replace(',','',$txtBudgetDPA2);
						$txtRealisasiDPA1 = str_replace(',','',$txtRealisasiDPA1);
						$txtRealisasiDPA2 = str_replace(',','',$txtRealisasiDPA2);
						$txtRealisasiBerjalanDPA1 = str_replace(',','',$txtRealisasiBerjalanDPA1);
						$txtRealisasiBerjalanDPA2 = str_replace(',','',$txtRealisasiBerjalanDPA2);
						$txtPengeluaranDPA1 = str_replace(',','',$txtPengeluaranDPA1);
						$txtPengeluaranDPA2 = str_replace(',','',$txtPengeluaranDPA2);
						$txtPengajuanDPA1 = str_replace(',','',$txtPengajuanDPA1);
						$txtPengajuanDPA2 = str_replace(',','',$txtPengajuanDPA2);
						$txtSisaBudgetDPA1 = str_replace(',','',$txtSisaBudgetDPA1);
						$txtSisaBudgetDPA2 = str_replace(',','',$txtSisaBudgetDPA2);
						$txtSelStatus = strip_tags($this->input->post('txtSelStatus'));
						if($DPA == 'all'){
							$DPA = 'DPA';
						}else{
							$DPA = 'DPA'.$DPA;
						}
						$npk = $this->npkLogin;
						$user = $this->user->dataUser($npk);
						$NPKAtasan = ($user[0]->NPKAtasan);
						if(substr($KodeActivity,0,5)=='15115'){
							$NPKAtasan = '006-96'; //kalo family day
						}
						$atasan = $this->user->dataUser($NPKAtasan);
						$NoPP = $this->ppb_model->generateNoPP($npk, $DPA);
						$this->ppb_model->insertPersetujuanPengeluaranBudget($NoPP, "PRPS", $KodeActivity, $JenisPengeluaran, $Keterangan, $txtSelStatus, $npk);

						if($DPA == 'DPA'){
							$this->ppb_model->insertDetailPersetujuanPengeluaranBudget($NoPP, 1, $txtPengeluaranDPA1, $txtBudgetDPA1, $txtRealisasiDPA1, $txtRealisasiBerjalanDPA1, $txtPengajuanDPA1, $txtSisaBudgetDPA1, $npk, $TahunBudget);
							$totalBudget+=$txtBudgetDPA1;
							$totalPengajuan+=$txtPengajuanDPA1;
							$totalSisaBudget+=$txtSisaBudgetDPA1;

							$this->ppb_model->insertDetailPersetujuanPengeluaranBudget($NoPP, 2, $txtPengeluaranDPA2, $txtBudgetDPA2, $txtRealisasiDPA2, $txtRealisasiBerjalanDPA2, $txtPengajuanDPA2, $txtSisaBudgetDPA2, $npk, $TahunBudget);
							$totalBudget+=$txtBudgetDPA2;
							$totalPengajuan+=$txtPengajuanDPA2;
							$totalSisaBudget+=$txtSisaBudgetDPA2;
						}
						else {
							if($DPA == 'DPA1'){
								$this->ppb_model->insertDetailPersetujuanPengeluaranBudget($NoPP, 1, $txtPengeluaranDPA1, $txtBudgetDPA1, $txtRealisasiDPA1, $txtRealisasiBerjalanDPA1, $txtPengajuanDPA1, $txtSisaBudgetDPA1, $npk, $TahunBudget);

								$totalBudget+=$txtBudgetDPA1;
								$totalPengajuan+=$txtPengajuanDPA1;
								$totalSisaBudget+=$txtSisaBudgetDPA1;
							}else if($DPA == 'DPA2'){
								$this->ppb_model->insertDetailPersetujuanPengeluaranBudget($NoPP, 2, $txtPengeluaranDPA2, $txtBudgetDPA2, $txtRealisasiDPA2, $txtRealisasiBerjalanDPA2, $txtPengajuanDPA2, $txtSisaBudgetDPA2, $npk, $TahunBudget);

								$totalBudget+=$txtBudgetDPA2;
								$totalPengajuan+=$txtPengajuanDPA2;
								$totalSisaBudget+=$txtSisaBudgetDPA2;
							}
						}
						$var["NoPP"] = $NoPP;
						$var["Keterangan"] = $Keterangan;
						$var["Status"] = $txtSelStatus;
						$var["KodeActivity"] = $KodeActivityForView;
						$var["NamaActivity"] = $NamaActivityForView;
						$var["DPA"] = $DPA;
						$var["JenisPengeluaran"] = $JenisPengeluaran;
						$var["KodeCoaDPA1"] = $hidKodeCoaDPA1;
						$var["KodeCoaDPA2"] = $hidKodeCoaDPA2;
						$var["namaUser"] = $user[0]->nama;
						$var["jabatanUser"] = $user[0]->jabatan;
						$var["emailUser"] = $user[0]->email;
						$var["departemenUser"] = $user[0]->departemen;
						$var["namaAtasan"] = $atasan[0]->nama;
						$var["jabatanAtasan"] = $atasan[0]->jabatan;
						$var["emailAtasan"] = $atasan[0]->email;
						$var["departemenAtasan"] = $atasan[0]->departemen;

						$var["totalBudget"] = $totalBudget;
						$var["totalPengajuan"] = $totalPengajuan;
						$var["totalSisaBudget"] = $totalSisaBudget;
						$this->load->view('PersetujuanPengeluaranBudget/CetakProposal.php', $var);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		public function CreatePurchaseRequest()
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Purchase Request"))
					{
						$KodeActivity = strip_tags($this->input->post('txtKodeActivity'));
						$KodeActivityForView = $KodeActivity;
						$NPKAtasan = '';
						$DPA = strip_tags($this->input->post('txtDPA'));
						$TahunBudget = strip_tags($this->input->post('txtTahunBudget'));
						$var['TahunBudget'] = $TahunBudget;
						$NamaActivity = "";
						$NamaActivityForView = "";
						if(strlen($KodeActivity) == 5){
							$act = $this->bm_activity_model->getActivityAll($KodeActivity);
							$KodeActivityForView = $KodeActivity.'1'.', '.$KodeActivity.'2';
							$NamaActivityForView = $act[0]->NamaActivity.', '.$act[1]->NamaActivity;
							//$NamaActivity = substr($act[0]->NamaActivity, 0,strpos($act[0]->NamaActivity,"-"));
						}else{
							$act = $this->bm_activity_model->getActivity($KodeActivity);
							$NamaActivityForView = $act[0]->NamaActivity;
						}
						$hidKodeCoa = strip_tags($this->input->post('hidKodeCoa'));
						$JenisPengeluaran = strip_tags($this->input->post('txtOpexCapex'));
						$Keterangan = strip_tags($this->input->post('txtKeterangan'));
						$txtHargaSatuan = strip_tags($this->input->post('txtHargaSatuan'));
						$txtQuantity = strip_tags($this->input->post('txtQuantity'));
						$txtTotalPengeluaran = strip_tags($this->input->post('txtTotalPengeluaran'));
						$txtBudget = strip_tags($this->input->post('txtBudget'));
						$txtRealisasi = strip_tags($this->input->post('txtRealisasi'));
						$txtRealisasiBerjalan = strip_tags($this->input->post('txtRealisasiBerjalan'));
						$txtPengajuan = strip_tags($this->input->post('txtPengajuan'));
						$txtSisaBudget = strip_tags($this->input->post('txtSisaBudget'));
						$txtSelStatus = strip_tags($this->input->post('txtSelStatus'));

						$totalBudget = 0;
						$totalPengajuan = 0;
						$totalSisaBudget = 0;

						$npk = $this->npkLogin;
						$user = $this->user->dataUser($npk);
						$NPKAtasan = ($user[0]->NPKAtasan);
						$atasan = $this->user->dataUser($NPKAtasan);
						$atasannyaatasan = $this->user->dataUser(($atasan[0]->NPKAtasan));
						if(substr($KodeActivity,0,5)=='15115'){
							$NPKAtasan = '006-96'; //kalo family day
							$atasan = $this->user->dataUser($NPKAtasan);
						}
						$var["DPA"] = $DPA;
						if($DPA == 'all'){
							$DPA = 'DPA';
						}else{
							$DPA = 'DPA'.$DPA;
						}
						$NoPP = $this->ppb_model->generateNoPP($npk, $DPA);
						$var["NoPP"] = $NoPP;
						$var["JenisPengeluaran"] = $JenisPengeluaran;
						$var["Keterangan"] = $Keterangan;
						$var["KodeCoa"] = $hidKodeCoa;
						$var["Quantity"] = $txtQuantity;
						$var["HargaSatuan"] = $txtHargaSatuan;
						$var["TotalPengeluaran"] = $txtTotalPengeluaran;
						$var["Status"] = $txtSelStatus;
						$var["namaUser"] = $user[0]->nama;
						$var["jabatanUser"] = $user[0]->jabatan;
						$var["emailUser"] = $user[0]->email;
						$var["departemenUser"] = $user[0]->departemen;
						$var["namaAtasan"] = $atasan[0]->nama;
						$var["jabatanAtasan"] = $atasan[0]->jabatan;
						$var["emailAtasan"] = $atasan[0]->email;
						$var["departemenAtasan"] = $atasan[0]->departemen;
						$var["namaAtasannyaatasan"] = $atasannyaatasan[0]->nama;
						$var["jabatanAtasannyaatasan"] = $atasannyaatasan[0]->jabatan;
						$var["emailAtasannyaatasan"] = $atasannyaatasan[0]->email;
						$var["departemenAtasannyaatasan"] = $atasannyaatasan[0]->departemen;

						$var["KodeActivity"] = $KodeActivityForView;
						$var["NamaActivity"] = $NamaActivityForView;
						$var["txtBudget"] = $txtBudget;
						$var["txtRealisasi"] = $txtRealisasi;
						$var["txtRealisasiBerjalan"] = $txtRealisasiBerjalan;
						$var["txtPengajuan"] = $txtPengajuan;
						$var["txtSisaBudget"] = $txtSisaBudget;
						$var["totalBudget"] = $totalBudget;
						$var["totalPengajuan"] = $totalPengajuan;
						$var["totalSisaBudget"] = $totalSisaBudget;
						$txtBudget = str_replace(',','', $txtBudget);
						$txtRealisasi = str_replace(',','', $txtRealisasi);
						$txtRealisasiBerjalan = str_replace(',','', $txtRealisasiBerjalan);
						$txtPengajuan = str_replace(',','', $txtPengajuan);
						$txtSisaBudget = str_replace(',','', $txtSisaBudget);
						$totalBudget = str_replace(',','', $totalBudget);
						$totalPengajuan = str_replace(',','', $totalPengajuan);
						$totalSisaBudget = str_replace(',','', $totalSisaBudget);
						$txtQuantity = str_replace(',','', $txtQuantity);
						$txtHargaSatuan = str_replace(',','', $txtHargaSatuan);
						$txtTotalPengeluaran = str_replace(',','', $txtTotalPengeluaran);
						$this->ppb_model->insertPersetujuanPengeluaranBudget($NoPP, "PRST", $KodeActivity, $JenisPengeluaran, $Keterangan, $txtSelStatus, $npk);
						if($DPA == 'DPA1'){
							$this->ppb_model->insertDetailPersetujuanPengeluaranBudget($NoPP, 1, $txtTotalPengeluaran, $txtBudget, $txtRealisasi, $txtRealisasiBerjalan, $txtPengajuan, $txtSisaBudget, $npk, $TahunBudget);
						}
						if($DPA == 'DPA2'){
							$this->ppb_model->insertDetailPersetujuanPengeluaranBudget($NoPP, 2, $txtTotalPengeluaran, $txtBudget, $txtRealisasi, $txtRealisasiBerjalan, $txtPengajuan, $txtSisaBudget, $npk, $TahunBudget);
						}
						$this->ppb_model->insertQtyPurchaseRequest($NoPP, $txtHargaSatuan, $txtQuantity, $npk);
						$TipePRST = 0;
						if($txtSelStatus =='Budgeted'){
							if($JenisPengeluaran == 'Opex'){
								if($txtTotalPengeluaran <= 1000000){
									$TipePRST = "BOPEX510";
								}else if($txtTotalPengeluaran > 1000000){
									if($DPA == 'DPA1'){
										$TipePRST = "1BOPEX100";
									}
									else if($DPA == 'DPA2'){
										$TipePRST = "2BOPEX100";
									}
								}
							}
							if($JenisPengeluaran == 'Capex'){
								if($txtTotalPengeluaran <= 2500000){
									$TipePRST = "BCAPEX525";
								}else if($txtTotalPengeluaran > 2500000){
									if($DPA == 'DPA1'){
										$TipePRST = "1BCAPEX250";
									}
									else if($DPA == 'DPA2'){
										$TipePRST = "2BCAPEX250";
									}
								}
							}
						}else if($txtSelStatus =='Unbudgeted' || $txtSelStatus == 'Overbudget'){
							if($JenisPengeluaran == 'Opex'){
								$TipePRST = "UOOPEX";
							}
							if($JenisPengeluaran == 'Capex'){
								$TipePRST = "UOCAPEX";
							}

						}
						$var["TipePRST"] = $TipePRST;
						$this->load->view('PersetujuanPengeluaranBudget/CetakPR.php', $var);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		public function purchaserequest()
    	{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Purchase Request"))
					{
						$data = array(
							'title' => 'Purchase Request',
							'admin' => '1',
							'dataActivity' => $this->bm_activity_model->getDataActivity($this->npkLogin,'1'),
							'npk' => $this->npkLogin,
							'dataTahunBudget' => $this->bm_activity_model->getYear()
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','PersetujuanPengeluaranBudget/purchaserequest_view',$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function _outputview ($output = null, $page)
		{
			$session_data = $this->session->userdata("logged_in");
			$data = array(
				"title" => "Dokumen Legal",
				"body" => $output
			);
			$this->load->helper(array("form","url"));
			if($page == "view")
			{
				$this->load->view('PersetujuanPengeluaranBudget/Proposal_iframe',$data);
			}
			else
			{
				$this->template->load("default",'templates/CRUD_view',$data);
			}
		}
		function ajax_KalkulasiBudgetRealisasi()
		{
			$DPA = $_POST["DPA"];
			$KodeActivity = $_POST["KodeActivity"];
			$TahunBudget = $_POST["TahunBudget"];

			$return = $this->bm_summarybudget_model->kalkulasiBudgetSummaryPPB($KodeActivity, $DPA, $this->npkLogin, $TahunBudget);
			echo json_encode($return);
		}
		function ajax_getSummaryBudget_old()
		{
			$DPA = $_POST["DPA"];
			$KodeActivity = $_POST["KodeActivity"];
			$NoPP = $_POST["NoPP"];
			$TahunBudget = $_POST["TahunBudget"];
			$Source = $_POST["Source"];

			$sqlWhereNoPP = "";
			if($NoPP != ''){
				$sqlWhereNoPP = " and kb.NoPP = '$NoPP' ";
			}
			if($DPA == "all")
			{
				$sqlquery = "SELECT '' as KodeSummaryBudget,LEFT(ba.KodeActivity,5) as KodeActivity, SUBSTRING(ba.NamaActivity,1,LOCATE('DPA',ba.NamaActivity)-2) as NamaActivity,ba.JenisPengeluaran,format(SUM(BudgetYTD),0) as BudgetYTD,format(SUM(RealisasiYTD),0) as RealisasiYTD,format((SUM(BudgetYTD) - SUM(RealisasiYTD)),0) as SisaBudgetYTD,format(SUM(BudgetFullYear),0) as BudgetFullYear, format((SUM(BudgetFullYear)-SUM(RealisasiYTD)),0) as SisaFullYear, MAX(TanggalLaporan) as TanggalLaporan
					FROM bm_temptotalbudget sb
						left join bm_coa c on sb.KodeCoa = c.KodeCoa
						left join bm_activity ba on sb.KodeActivity = ba.KodeActivity
					WHERE (BudgetFullYear != 0 OR RealisasiYTD != 0) and sb.CreatedBy = '". $this->npkLogin ."' ";
			}
			else
			{
				$sqlquery = "SELECT KodeSummaryBudget, sb.KodeCoa,c.NamaCoa,ba.KodeActivity,ba.NamaActivity,ba.JenisPengeluaran,
					format(BudgetYTD,0) as BudgetYTD,format(RealisasiYTD,0) as RealisasiYTD,
					format((BudgetYTD - RealisasiYTD),0) as SisaBudgetYTD,format(BudgetFullYear,0) as BudgetFullYear,
					format((BudgetFullYear-RealisasiYTD),0) as SisaFullYear, TanggalLaporan
				FROM bm_temptotalbudget sb
					left join bm_coa c on sb.KodeCoa = c.KodeCoa
					left join bm_activity ba on sb.KodeActivity = ba.KodeActivity
				WHERE (BudgetFullYear != 0 OR RealisasiYTD != 0) and sb.CreatedBy = '". $this->npkLogin ."' ";
			}
			if($DPA == "" && $KodeActivity == "")
			{
				$sqlquery = $sqlquery . " AND sb.KodeSummaryBudget = 0";
			}
			if($DPA == 'all')
			{
				$sqlquery .= " AND sb.DPA IN (1,2) ";
			}
			else if($DPA != "")
			{
				$sqlquery .= " AND sb.DPA = '$DPA'";
			}

			if($KodeActivity != "")
			{
				if($DPA == 'all')
				{
					$sqlquery .= " AND LEFT(sb.KodeActivity,5) = '$KodeActivity'";
				}else{
					$sqlquery .= " AND sb.KodeActivity = '$KodeActivity'";
				}
			}
			if($DPA == "all")
			{
				$sqlquery .= "GROUP BY LEFT(ba.KodeActivity,5),SUBSTRING(ba.NamaActivity,1,LOCATE('DPA',ba.NamaActivity)-2),ba.JenisPengeluaran";
			}
			$rs = $this->db->query($sqlquery);
			$message = "Budget";
			if($rs->num_rows() == 0){
				$selectDariActivity = "SELECT * FROM bm_activity where deleted = 0 ";
				if($DPA == 'all')
				{
					$selectDariActivity .= " and LEFT(KodeActivity,5) = '$KodeActivity'";
				}else{
					$selectDariActivity .= " and KodeActivity = '$KodeActivity'";
				}
				$rs2 = $this->db->query($selectDariActivity);
				$message = "NoBudget";
			}
			if($DPA == 1){
				if($Source == 'KartuBayar'){
					$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(kb.Nominal),0),0) as TotalRealisasiPengajuan FROM
					persetujuanpengeluaranbudget ppb
					join dtlppbkartubayar kb on ppb.NoPP = kb.NoPP
					where LEFT(KodeActivity,5) = LEFT('$KodeActivity', 5)
					and kb.CreatedBy LIKE '%DPA1%'
					".$sqlWhereNoPP."
					and ppb.Deleted = 0 and kb.Deleted = 0 and kb.isSubmitted  = 1";
				}else if ($Source == 'PRPS' || $Source == 'PRST'){
					$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(dpb.Pengajuan),0),0) as TotalRealisasiPengajuan from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
					where ppb.deleted = 0 and dpb.deleted = 0 and dpb.TahunBudget = '$TahunBudget' and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 1";
				}else if($Source == 'EditPRPS' || $Source == 'EditPRST'){
					$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(dpb.Pengajuan),0),0) as TotalRealisasiPengajuan from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
					where ppb.deleted = 0 and ppb.NoPP != '$NoPP' and dpb.deleted = 0 and dpb.TahunBudget = '$TahunBudget' and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 1";
				}
			}
			if($DPA == 2){
				if($Source == 'KartuBayar'){
					$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(kb.Nominal),0),0) as TotalRealisasiPengajuan FROM
					persetujuanpengeluaranbudget ppb
					join dtlppbkartubayar kb on ppb.NoPP = kb.NoPP
					where LEFT(KodeActivity,5) = LEFT('$KodeActivity', 5)
					and (kb.CreatedBy LIKE '%/%' OR kb.CreatedBy LIKE '%DPA2%')
					".$sqlWhereNoPP."
					and ppb.Deleted = 0 and kb.Deleted = 0 and kb.isSubmitted  = 1";
				}else if ($Source == 'PRPS' || $Source == 'PRST'){
					$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(dpb.Pengajuan),0),0) as TotalRealisasiPengajuan from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
					where ppb.deleted = 0 and dpb.deleted = 0 and dpb.TahunBudget = '$TahunBudget'  and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 2";
				}else if($Source == 'EditPRPS' || $Source == 'EditPRST'){
					$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(dpb.Pengajuan),0),0) as TotalRealisasiPengajuan from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
					where ppb.deleted = 0 and ppb.NoPP != '$NoPP' and dpb.TahunBudget = '$TahunBudget' and dpb.deleted = 0 and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 2";
				}
			}
			$nominal = $this->db->query($sqlSelectTotalNominal);
			$result = $rs->result();
			if($message == "NoBudget"){
				array_push($result, $nominal->result()[0], $rs2->result()[0], $message);
			}else if($message == "Budget"){
				array_push($result, $nominal->result()[0], $message);
			}
			echo json_encode($result);
		}
		function ajax_getSummaryBudget()
		{
			$DPA = $_POST["DPA"];
			$KodeActivity = $_POST["KodeActivity"];
			$NoPP = $_POST["NoPP"];
			$TahunBudget = $_POST["TahunBudget"];
			$Source = $_POST["Source"];

			$sqlWhereNoPP = "";
			if($NoPP != ''){
				$sqlWhereNoPP = " and kb.NoPP = '$NoPP' ";
			}
			if($DPA == "all")
			{
				$sqlquery = "SELECT '' as KodeSummaryBudget,LEFT(ba.KodeActivity,5) as KodeActivity, SUBSTRING(ba.NamaActivity,1,LOCATE('DPA',ba.NamaActivity)-2) as NamaActivity,ba.JenisPengeluaran,format(SUM(BudgetYTD),0) as BudgetYTD,format(SUM(RealisasiYTD),0) as RealisasiYTD,format((SUM(BudgetYTD) - SUM(RealisasiYTD)),0) as SisaBudgetYTD,format(SUM(BudgetFullYear),0) as BudgetFullYear, format((SUM(BudgetFullYear)-SUM(RealisasiYTD)),0) as SisaFullYear, MAX(TanggalLaporan) as TanggalLaporan
					FROM bm_temptotalbudget sb
						left join bm_coa c on sb.KodeCoa = c.KodeCoa
						left join bm_activity ba on sb.KodeActivity = ba.KodeActivity
					WHERE (BudgetFullYear != 0 OR RealisasiYTD != 0) and sb.CreatedBy = '". $this->npkLogin ."' ";
			}
			else
			{
				$sqlquery = "SELECT KodeSummaryBudget, sb.KodeCoa,c.NamaCoa,ba.KodeActivity,ba.NamaActivity,ba.JenisPengeluaran,
					format(BudgetYTD,0) as BudgetYTD,format(RealisasiYTD,0) as RealisasiYTD,
					format((BudgetYTD - RealisasiYTD),0) as SisaBudgetYTD,format(BudgetFullYear,0) as BudgetFullYear,
					format((BudgetFullYear-RealisasiYTD),0) as SisaFullYear, TanggalLaporan
				FROM bm_temptotalbudget sb
					left join bm_coa c on sb.KodeCoa = c.KodeCoa
					left join bm_activity ba on sb.KodeActivity = ba.KodeActivity
				WHERE (BudgetFullYear != 0 OR RealisasiYTD != 0) and sb.CreatedBy = '". $this->npkLogin ."' ";
			}
			if($DPA == "" && $KodeActivity == "")
			{
				$sqlquery = $sqlquery . " AND sb.KodeSummaryBudget = 0";
			}
			if($DPA == 'all')
			{
				$sqlquery .= " AND sb.DPA IN (1,2) ";
			}
			else if($DPA != "")
			{
				$sqlquery .= " AND sb.DPA = '$DPA'";
			}

			if($KodeActivity != "")
			{
				if($DPA == 'all')
				{
					$sqlquery .= " AND LEFT(sb.KodeActivity,5) = '$KodeActivity'";
				}else{
					$sqlquery .= " AND sb.KodeActivity = '$KodeActivity'";
				}
			}
			if($DPA == "all")
			{
				$sqlquery .= "GROUP BY LEFT(ba.KodeActivity,5),SUBSTRING(ba.NamaActivity,1,LOCATE('DPA',ba.NamaActivity)-2),ba.JenisPengeluaran";
			}
			$rs = $this->db->query($sqlquery);
			$message = "Budget";
			if($rs->num_rows() == 0){
				$selectDariActivity = "SELECT * FROM bm_activity where deleted = 0 ";
				if($DPA == 'all')
				{
					$selectDariActivity .= " and LEFT(KodeActivity,5) = '$KodeActivity'";
				}else{
					$selectDariActivity .= " and KodeActivity = '$KodeActivity'";
				}
				$rs2 = $this->db->query($selectDariActivity);
				$message = "NoBudget";
			}
			$totalRealisasiPengajuan = 0;
			if($DPA == 1){
				if($Source == 'KartuBayar'){
					$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(kb.Nominal),0),0) as TotalRealisasiPengajuan FROM
					persetujuanpengeluaranbudget ppb
					left join dtlppbkartubayar kb on ppb.NoPP = kb.NoPP
					where LEFT(KodeActivity,5) = LEFT('$KodeActivity', 5)
					and kb.CreatedBy LIKE '%DPA1%'
					".$sqlWhereNoPP."
					and ppb.Deleted = 0 and kb.Deleted = 0 and kb.isSubmitted  = 1";

					$nominal = $this->db->query($sqlSelectTotalNominal);
					if($nominal->num_rows()>0){
						$totalRealisasiPengajuan = $nominal->result()[0];
					}else{
						$totalRealisasiPengajuan = 0;
					}
				}else if ($Source == 'PRPS' || $Source == 'PRST'){
					$sqlSelectTotalPengajuan = "SELECT IFNULL(sum(dpb.Pengajuan),0) as Pengajuan from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
					where ppb.deleted = 0
                    and dpb.deleted = 0
					and ppb.isFinished = 0
					and dpb.TahunBudget = '$TahunBudget' and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 1";

					$sqlSelectTotalNominal = "SELECT IFNULL(sum(kb.nominal),0) as NominalKartuBayar from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
					join dtlppbkartubayar kb on kb.NoPP = ppb.NoPP
					where ppb.deleted = 0
					and kb.deleted = 0 and kb.isSubmitted = 1
					and ppb.isFinished = 0
					and dpb.deleted = 0
					and kb.createdBy like '%DPA1%' and dpb.deleted = 0
					and ppb.isFinished = 0
					and kb.Keterangan not like '%Pembulatan%'
					and dpb.TahunBudget = '$TahunBudget' and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 1";
					$pengajuan = $this->db->query($sqlSelectTotalPengajuan);
					if($pengajuan->num_rows()>0){
						$pengajuan = $pengajuan->result()[0]->Pengajuan;
					}else{
						$pengajuan = 0;
					}
					$nominal = $this->db->query($sqlSelectTotalNominal);
					if($nominal->num_rows()>0){
						$nominal = $nominal->result()[0]->NominalKartuBayar;
					}else{
						$nominal = 0;
					}

					$totalRealisasiPengajuan = number_format($pengajuan - $nominal);
					$totalRealisasiPengajuan = array("TotalRealisasiPengajuan" => $totalRealisasiPengajuan);

				}else if($Source == 'EditPRPS' || $Source == 'EditPRST'){
					$sqlSelectTotalPengajuan = "SELECT format(IFNULL(sum(dpb.Pengajuan),0),0) as Pengajuan from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
					where ppb.deleted = 0
                    and dpb.deleted = 0
					and ppb.isFinished = 0
					and ppb.NoPP != '$NoPP'
					and dpb.TahunBudget = '$TahunBudget' and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 1";

					$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(kb.nominal),0),0) as NominalKartuBayar from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
                    left join dtlppbkartubayar kb on kb.NoPP = ppb.NoPP
					where ppb.deleted = 0
					and kb.deleted = 0 and kb.isSubmitted = 1
					and ppb.isFinished = 0
                    and dpb.deleted = 0
                    and kb.createdBy like '%DPA1%' and dpb.deleted = 0
					and ppb.isFinished = 0
					and ppb.NoPP != '$NoPP'
					and kb.Keterangan not like '%Pembulatan%'
					and dpb.TahunBudget = '$TahunBudget' and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 1";
					$pengajuan = $this->db->query($sqlSelectTotalPengajuan);
					if($pengajuan->num_rows()>0){
						$pengajuan = $pengajuan->result()[0]->Pengajuan;
					}else{
						$pengajuan = 0;
					}
					$nominal = $this->db->query($sqlSelectTotalNominal);
					if($nominal->num_rows()>0){
						$nominal = $nominal->result()[0]->NominalKartuBayar;
					}else{
						$nominal = 0;
					}
					$totalRealisasiPengajuan = number_format($pengajuan - $nominal);
					$totalRealisasiPengajuan = array("TotalRealisasiPengajuan" => $totalRealisasiPengajuan);

				}
			}
			if($DPA == 2){
				if($Source == 'KartuBayar'){
					$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(kb.Nominal),0),0) as TotalRealisasiPengajuan FROM
					persetujuanpengeluaranbudget ppb
					left join dtlppbkartubayar kb on ppb.NoPP = kb.NoPP
					where LEFT(KodeActivity,5) = LEFT('$KodeActivity', 5)
					and (kb.CreatedBy LIKE '%/%' OR kb.CreatedBy LIKE '%DPA2%')
					".$sqlWhereNoPP."
					and ppb.Deleted = 0 and kb.Deleted = 0 and kb.isSubmitted  = 1";
					$nominal = $this->db->query($sqlSelectTotalNominal);
					if($nominal->num_rows()>0){
						$totalRealisasiPengajuan = $nominal->result()[0];
					}else{
						$totalRealisasiPengajuan = 0;
					}
				}else if ($Source == 'PRPS' || $Source == 'PRST'){
					$sqlSelectTotalPengajuan = "SELECT IFNULL(SUM(dpb.Pengajuan),0)as Pengajuan from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
					where ppb.deleted = 0
                    and dpb.deleted = 0
					and ppb.isFinished = 0
					and dpb.TahunBudget = '$TahunBudget' and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 2";

					$sqlSelectTotalNominal = "SELECT IFNULL(SUM(kb.nominal),0) as NominalKartuBayar from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
					join dtlppbkartubayar kb on kb.NoPP = ppb.NoPP
					where ppb.deleted = 0
					and kb.deleted = 0 and kb.isSubmitted = 1
					and ppb.isFinished = 0
					and dpb.deleted = 0
					and kb.createdBy like '%DPA2%' and dpb.deleted = 0
					and ppb.isFinished = 0
					and kb.Keterangan not like '%Pembulatan%'
					and dpb.TahunBudget = '$TahunBudget' and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 2";

					$pengajuan = $this->db->query($sqlSelectTotalPengajuan);
					if($pengajuan->num_rows()>0){
						$pengajuan = $pengajuan->result()[0]->Pengajuan;
					}else{
						$pengajuan = 0;
					}
					$nominal = $this->db->query($sqlSelectTotalNominal);
					if($nominal->num_rows()>0){
						$nominal = $nominal->result()[0]->NominalKartuBayar;
					}else{
						$nominal = 0;
					}
					$totalRealisasiPengajuan = number_format($pengajuan - $nominal);
					$totalRealisasiPengajuan = array("TotalRealisasiPengajuan" => $totalRealisasiPengajuan);

				}else if($Source == 'EditPRPS' || $Source == 'EditPRST'){
					$sqlSelectTotalPengajuan = "SELECT IFNULL(SUM(dpb.Pengajuan),0) as Pengajuan from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
					where ppb.deleted = 0
                    and dpb.deleted = 0
					and ppb.isFinished = 0
					and ppb.NoPP != '$NoPP'
					and dpb.TahunBudget = '$TahunBudget' and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 2";

					$sqlSelectTotalNominal = "SELECT IFNULL(SUM(kb.nominal),0) as NominalKartuBayar from persetujuanpengeluaranbudget ppb join
					dtlpersetujuanpengeluaranbudget dpb
					on ppb.NoPP = dpb.NoPP
                    left join dtlppbkartubayar kb on kb.NoPP = ppb.NoPP
					where ppb.deleted = 0
					and kb.deleted = 0 and kb.isSubmitted = 1
					and ppb.isFinished = 0
                    and dpb.deleted = 0
                    and kb.createdBy like '%DPA2%' and dpb.deleted = 0
					and ppb.isFinished = 0
					and ppb.NoPP != '$NoPP'
					and kb.Keterangan not like '%Pembulatan%'
					and dpb.TahunBudget = '$TahunBudget' and LEFT(ppb.KodeActivity,5) = LEFT('$KodeActivity', 5) and dpb.DPA = 2";

					$pengajuan = $this->db->query($sqlSelectTotalPengajuan);

					if($pengajuan->num_rows()>0){

						$pengajuan = $pengajuan->result()[0]->Pengajuan;
					}else{
						$pengajuan = 0;
					}
					$nominal = $this->db->query($sqlSelectTotalNominal);
					if($nominal->num_rows()>0){
						$nominal = $nominal->result()[0]->NominalKartuBayar;
					}else{
						$nominal = 0;
					}
					$totalRealisasiPengajuan = number_format($pengajuan - $nominal);
					$totalRealisasiPengajuan = array("TotalRealisasiPengajuan" => $totalRealisasiPengajuan);

				}
			}
			$result = $rs->result();
			if($message == "NoBudget"){
				array_push($result, $rs2->result()[0], $totalRealisasiPengajuan, $message);
			}else if($message == "Budget"){
				array_push($result, $totalRealisasiPengajuan, $message);
			}
			echo json_encode($result);
		}
		function listPP($searchQuery = '') //List Persetujuan Pengeluaran
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("List Persetujuan Pengeluaran Budget"))
					{
						$listPPview = array();
						$listPP = $this->ppb_model->getListPP(urldecode($searchQuery));
						if (!empty($listPP)):
							foreach ($listPP as $value){
								$detailPRPS = $this->ppb_model->getDetailPP($value->NoPP);
								$PengeluaranDPA1 = 0;
								$PengeluaranDPA2 = 0;
								$txtTotalPengeluaran = 0;

								foreach($detailPRPS as $detail){
									$txtTotalPengeluaran += $detail->Pengajuan;
									if($detail->DPA == 1){
										$PengeluaranDPA1 = number_format($detail->Pengajuan);
									}
									if($detail->DPA == 2){
										$PengeluaranDPA2 = number_format($detail->Pengajuan);
									}
								}
								$isReleased = "<b>Belum Release</b>";
								if($value->isReleased == 1){
									$isReleased = "<i>Released</i>";
								}
								$result = array("ID"=>$value->ID, "NoPP"=>$value->NoPP.'<br/> ['.$isReleased.']', "Keterangan"=>$value->Keterangan, "Tipe"=>$value->Tipe, "PengeluaranDPA1"=>$PengeluaranDPA1, "PengeluaranDPA2"=>$PengeluaranDPA2, "TotalPengeluaran"=>number_format($txtTotalPengeluaran), "FilePath"=>$value->FilePath);
								array_push($listPPview, $result);
							}
						endif;
						$data = array(
							"title" => "List Persetujuan Pengeluaran Budget",
							"admin" => "0",
							"data" => $listPPview,
							"npk" => $this->npkLogin,
							"user" => $this->user->dataUser($this->npkLogin)
						);
						$this->load->helper(array("form","url"));
						$this->template->load("default","PersetujuanPengeluaranBudget/listPP_view",$data);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ajax_loadActivity()
		{
			try
			{
				$KodeActivity = $_POST['KodeActivity'];
				$dataActivity = $this->bm_activity_model->getActivity($KodeActivity);
				//fire_print('log',print_r($dirArray,true));
				echo json_encode($dataActivity);
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ajax_loadActivityBasedDPA()
		{
			try
			{
				$DPA = $_POST['DPA'];
				$admin = $_POST['admin'];
				$dataActivity = $this->bm_activity_model->getDataActivity($this->npkLogin,$admin,$DPA);
				$dirArray = array();
				if($dataActivity)
				{
					foreach($dataActivity as $row)
					{
						$arr = array('KodeActivity' => $row->KodeActivity, 'NamaActivity' => $row->NamaActivity);
						array_push($dirArray,$arr);
					}
				}
				//fire_print('log',print_r($dirArray,true));
				echo json_encode($dirArray);
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function Upload($ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					$session_data = $this->session->userdata('logged_in');
					$this->npkLogin = $session_data['npk'];

					$user = $this->user->dataUser($this->npkLogin);
					if($user[0]->departemen == 'HRGA' || $user[0]->departemen == 'Accounting Tax & Control'){
						//klo HRGA atau ada data user tsb
						$sqlSelect =
							"SELECT * from persetujuanpengeluaranbudget where
							id = '$ID'
							";
						$data = $this->db->query($sqlSelect);
						$dataPPB = array(
							'title'=> 'Upload Persetujuan Pengeluaran Budget',
							'data' => $data->result()
						);
						$this->load->helper(array('form','url'));
						$this->template->load('default','PersetujuanPengeluaranBudget/UploadPPB_view', $dataPPB);
					}else{
						$sqlSelect =
							"SELECT * from persetujuanpengeluaranbudget where
							CreatedBy = '$this->npkLogin'
							and id = '$ID'
							";
						$data = $this->db->query($sqlSelect);
						if($data->num_rows() != 0){
							$dataPPB = array(
								'title'=> 'Upload Persetujuan Pengeluaran Budget',
								'data' => $data->result()
							);
							$this->load->helper(array('form','url'));
							$this->template->load('default','PersetujuanPengeluaranBudget/UploadPPB_view', $dataPPB);
						}else{
							redirect("PersetujuanPengeluaranBudget/PPBController/ListPP",'refresh');
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function Edit($ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{

					$session_data = $this->session->userdata('logged_in');
					$npk = $session_data['npk'];
					$editview = array();
					//$result = array("ID"=>$value->ID, "NoPP"=>$value->NoPP, "Keterangan"=>$value->Keterangan, "Tipe"=>$value->Tipe, "PengeluaranDPA1"=>$PengeluaranDPA1, "PengeluaranDPA2"=>$PengeluaranDPA2, "TotalPengeluaran"=>$txtTotalPengeluaran, "FilePath"=>$value->FilePath);
					$sqlSelect =
					"SELECT * from persetujuanpengeluaranbudget ppb where
					ppb.CreatedBy = '$this->npkLogin'
					and ppb.id = '$ID'
					and ppb.deleted = 0
					";
					if($npk == '080-17'){
						$sqlSelect =
						"SELECT * from persetujuanpengeluaranbudget ppb where
						ppb.id = '$ID'
						and ppb.deleted = 0
						";
					}
					$data = $this->db->query($sqlSelect);
					if($data->num_rows() == 0 ){
						redirect("PersetujuanPengeluaranBudget/PPBController/ListPP",'refresh');
					}else{
						//SUBSTRING(ba.NamaActivity,1,LOCATE('DPA',ba.NamaActivity)-2) as NamaActivity
						$data = $data->result();
						$Tipe = $data[0]->Tipe;
						if($Tipe == 'PRPS'){
							foreach($data as $value){
								$sqlDetail = "SELECT * FROM dtlpersetujuanpengeluaranbudget
								where NoPP = '$value->NoPP' and deleted = 0";
								$result = $this->db->query($sqlDetail)->result();
								if(count($result) == 2){
									$sqlSelectNamaActivity = "SELECT SUBSTRING(NamaActivity,1,LOCATE('DPA',NamaActivity)-2) as NamaActivity From bm_activity where left(KodeActivity,5) = left('$value->KodeActivity',5)";
								}else{
									$sqlSelectNamaActivity = "SELECT NamaActivity From bm_activity where KodeActivity = '$value->KodeActivity'";
								}
								$namaActivity = $this->db->query($sqlSelectNamaActivity)->result();
								$pengeluaranDPA1 = 0;
								$pengeluaranDPA2 = 0;
								$TahunBudget = 0;
								foreach($result as $dtl){
									if($dtl->DPA == 1){
										$DPA = 1;
										$pengeluaranDPA1 = $dtl->Pengeluaran;
										$TahunBudget = $dtl->TahunBudget;
									}else{
										$DPA = 2;
										$pengeluaranDPA2 = $dtl->Pengeluaran;
										$TahunBudget = $dtl->TahunBudget;
									}
								}
								if(count($result) == 2){
									$DPA = 'all';
								}
								$editview = array("ID"=>$value->ID,
									"NoPP"=>$value->NoPP,
									"DPA"=>$DPA,
									"Keterangan"=>$value->Keterangan,
									"NamaActivity"=>$namaActivity[0]->NamaActivity,
									"KodeActivity"=>$value->KodeActivity,
									"JenisPengeluaran"=>$value->JenisPengeluaran,
									"PengeluaranDPA1"=>$pengeluaranDPA1,
									"PengeluaranDPA2"=>$pengeluaranDPA2,
									"Status"=>$value->Status,
									"TahunBudget"=> $TahunBudget
								);
								$dataProposal = array(
									'title'=> 'Edit Proposal',
									'data' => $editview,
									'admin' => '1',
									'npk' => $npk
									);
								$this->load->helper(array('form','url'));
								$this->template->load('default','PersetujuanPengeluaranBudget/EditProposal_view', $dataProposal);
							}
						}else if($Tipe == 'PRST'){
							foreach($data as $value){
								$sqlDetail = "SELECT * FROM dtlpersetujuanpengeluaranbudget where NoPP = '$value->NoPP' and deleted = 0";
								$result = $this->db->query($sqlDetail)->result();
								$sqlDetail2 = "SELECT * FROM dtlppbpurchaserequest where NoPP = '$value->NoPP' and deleted = 0";
								$result2 = $this->db->query($sqlDetail2)->result();
								$sqlSelectNamaActivity = "SELECT NamaActivity From bm_activity where KodeActivity = '$value->KodeActivity'";
								$namaActivity = $this->db->query($sqlSelectNamaActivity)->result();
								$editview = array("ID"=>$value->ID,
									"NoPP"=>$value->NoPP,
									"DPA"=>$result[0]->DPA,
									"Keterangan"=>$value->Keterangan,
									"NamaActivity"=>$namaActivity[0]->NamaActivity,
									"KodeActivity"=>$value->KodeActivity,
									"JenisPengeluaran"=>$value->JenisPengeluaran,
									"Pengeluaran"=>$result[0]->Pengeluaran,
									"HargaSatuan"=>$result2[0]->HargaSatuan,
									"Quantity"=>$result2[0]->Quantity,
									"Status"=>$value->Status,
									"TahunBudget"=>$result[0]->TahunBudget
								);
							}
								$dataPR = array(
									'title'=> 'Edit Purchase Request',
									'data' => $editview,
									'admin' => '1',
									'npk' => $npk,
									);
								$this->load->helper(array('form','url'));
								$this->template->load('default','PersetujuanPengeluaranBudget/EditPurchaseRequest_view', $dataPR);
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		public function UpdateProposal($ID = '')
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Proposal"))
					{
						$NoPP = strip_tags($this->input->post('txtNoPP'));
						$HeaderPP = $this->ppb_model->getHeaderPP($ID);
						$KodeActivity = $HeaderPP->KodeActivity;
						$NamaActivity ="";
						$activityList = $this->bm_activity_model->getActivity($KodeActivity);
						if($activityList)
						{
							foreach($activityList as $row)
							{
								$NamaActivity = $row->NamaActivity;
							}
						}
						$DPA = strip_tags($this->input->post('txtDPA'));
						$TahunBudget = strip_tags($this->input->post('txtTahunBudget'));
						$var["TahunBudget"] = $TahunBudget;
						$JenisPengeluaran = strip_tags($this->input->post('txtOpexCapex'));
						$Keterangan = strip_tags($this->input->post('txtKeterangan'));
						$totalBudget = 0;
						$totalPengajuan = 0;
						$totalSisaBudget = 0;
						$txtPengeluaranDPA1 = strip_tags($this->input->post('txtPengeluaranDPA1'));
						$txtPengeluaranDPA2 = strip_tags($this->input->post('txtPengeluaranDPA2'));
						$hidKodeCoaDPA1 = strip_tags($this->input->post('hidKodeCoaDPA1'));
						$hidKodeCoaDPA2 = strip_tags($this->input->post('hidKodeCoaDPA2'));
						$txtBudgetDPA1 = strip_tags($this->input->post('txtBudgetDPA1'));
						$txtBudgetDPA2 = strip_tags($this->input->post('txtBudgetDPA2'));
						$txtRealisasiDPA1 = strip_tags($this->input->post('txtRealisasiDPA1'));
						$txtRealisasiDPA2 = strip_tags($this->input->post('txtRealisasiDPA2'));
						$txtRealisasiBerjalanDPA1 = strip_tags($this->input->post('txtRealisasiBerjalanDPA1'));
						$txtRealisasiBerjalanDPA2 = strip_tags($this->input->post('txtRealisasiBerjalanDPA2'));
						$txtPengajuanDPA1 = strip_tags($this->input->post('txtPengajuanDPA1'));
						$txtPengajuanDPA2 = strip_tags($this->input->post('txtPengajuanDPA2'));
						$txtSisaBudgetDPA2 = strip_tags($this->input->post('txtSisaBudgetDPA2'));
						$txtSisaBudgetDPA1 = strip_tags($this->input->post('txtSisaBudgetDPA1'));
						$var["txtBudgetDPA1"] = $txtBudgetDPA1;
						$var["txtRealisasiDPA1"] = $txtRealisasiDPA1;
						$var["txtRealisasiBerjalanDPA1"] = $txtRealisasiBerjalanDPA1;
						$var["txtPengeluaranDPA1"] = $txtPengeluaranDPA1;
						$var["txtPengajuanDPA1"] = $txtPengajuanDPA1;
						$var["txtSisaBudgetDPA1"] = $txtSisaBudgetDPA1;
						$var["txtBudgetDPA2"] = $txtBudgetDPA2;
						$var["txtRealisasiDPA2"] = $txtRealisasiDPA2;
						$var["txtRealisasiBerjalanDPA2"] = $txtRealisasiBerjalanDPA2;
						$var["txtPengeluaranDPA2"] = $txtPengeluaranDPA2;
						$var["txtPengajuanDPA2"] = $txtPengajuanDPA2;
						$var["txtSisaBudgetDPA2"] = $txtSisaBudgetDPA2;
						$txtBudgetDPA1 = str_replace(',','',$txtBudgetDPA1);
						$txtBudgetDPA2 = str_replace(',','',$txtBudgetDPA2);
						$txtRealisasiDPA1 = str_replace(',','',$txtRealisasiDPA1);
						$txtRealisasiDPA2 = str_replace(',','',$txtRealisasiDPA2);
						$txtRealisasiBerjalanDPA1 = str_replace(',','',$txtRealisasiBerjalanDPA1);
						$txtRealisasiBerjalanDPA2 = str_replace(',','',$txtRealisasiBerjalanDPA2);
						$txtPengeluaranDPA1 = str_replace(',','',$txtPengeluaranDPA1);
						$txtPengeluaranDPA2 = str_replace(',','',$txtPengeluaranDPA2);
						$txtPengajuanDPA1 = str_replace(',','',$txtPengajuanDPA1);
						$txtPengajuanDPA2 = str_replace(',','',$txtPengajuanDPA2);
						$txtSisaBudgetDPA1 = str_replace(',','',$txtSisaBudgetDPA1);
						$txtSisaBudgetDPA2 = str_replace(',','',$txtSisaBudgetDPA2);
						$txtSelStatus = strip_tags($this->input->post('txtSelStatus'));

						if($DPA == 'all'){
							$DPA = 'DPA';
						}else{
							$DPA = 'DPA'.$DPA;
						}
						$npk = $this->npkLogin;
						$user = $this->user->dataUser($npk);
						$NPKAtasan = ($user[0]->NPKAtasan);
						$atasan = $this->user->dataUser($NPKAtasan);
						$Departemen = '';
						if($user[0]->departemen == 'HRGA'){
							$Departemen = 'HRGA';
						}else if($user[0]->departemen == 'Mitra Relation & Communication'){
							$Departemen = 'MRC';
						}else if($user[0]->departemen == 'Claim & Actuary'){
							$Departemen = 'CA';
						}else if($user[0]->departemen == 'Kepesertaan'){
							$Departemen = 'KPST';
						}else if($user[0]->departemen == 'Finance & Investment'){
							$Departemen = 'FI';
						}else if($user[0]->departemen == 'IT'){
							$Departemen = 'IT';
						}else if($user[0]->departemen == 'Accounting Tax & Control'){
							$Departemen = 'ACCT';
						}else if($user[0]->departemen == 'Corporate Secretary'){
							$Departemen = 'CORSE';
						}else if($user[0]->departemen == 'Customer Relation'){
							$Departemen = 'CR';
						}else if($user[0]->departemen == 'Kepesertaan'){
							$Departemen = 'KPST';
						}

						$sqlUpdateMstr = "UPDATE persetujuanpengeluaranbudget
						SET Keterangan = ?, Status = '$txtSelStatus', UpdatedOn = now(), UpdatedBy = '$npk'
						WHERE ID = $ID;
						";
						$this->db->query($sqlUpdateMstr, array($Keterangan));
						if($DPA == 'DPA'){
							$sqlUpdateDetailDPA1 = "UPDATE dtlpersetujuanpengeluaranbudget SET Deleted = 1, UpdatedOn = now(), UpdatedBy = '$npk' where NoPP = '$NoPP' and DPA = 1 and deleted = 0";
							$this->db->query($sqlUpdateDetailDPA1);
							$sqlInsertDetailDPA1 = "INSERT INTO dtlpersetujuanpengeluaranbudget
							(Deleted, NoPP, DPA, Pengeluaran, Budget, Realisasi, RealisasiBerjalan, Pengajuan, SisaBudget, CreatedOn, CreatedBy)
							VALUES (0, ?, 1, ?, ?, ?, ?, ?, ?,?, now(), '$npk');
							";
							$totalBudget+=$txtBudgetDPA1;
							$totalPengajuan+=$txtPengajuanDPA1;
							$totalSisaBudget+=$txtSisaBudgetDPA1;
							$this->db->query($sqlInsertDetailDPA1, array($NoPP, $txtPengeluaranDPA1, $txtBudgetDPA1, $txtRealisasiDPA1, $txtRealisasiBerjalanDPA1, $txtPengajuanDPA1, $txtSisaBudgetDPA1, $TahunBudget));

							$sqlUpdateDetailDPA2 = "UPDATE dtlpersetujuanpengeluaranbudget SET Deleted = 1, UpdatedOn = now(), UpdatedBy = '$npk' where NoPP = '$NoPP' and DPA = 2 and deleted = 0";
							$this->db->query($sqlUpdateDetailDPA2);
							$sqlInsertDetailDPA2 = "INSERT INTO dtlpersetujuanpengeluaranbudget
							(Deleted, NoPP, DPA, Pengeluaran, Budget, Realisasi, RealisasiBerjalan, Pengajuan, SisaBudget, CreatedOn, CreatedBy)
							VALUES (0, ?, 2, ?, ?, ?, ?, ?, ?,?, now(), '$npk');

							";
							$totalBudget+=$txtBudgetDPA2;
							$totalPengajuan+=$txtPengajuanDPA2;
							$totalSisaBudget+=$txtSisaBudgetDPA2;
							$this->db->query($sqlInsertDetailDPA2, array($NoPP, $txtPengeluaranDPA2, $txtBudgetDPA2, $txtRealisasiDPA2, $txtRealisasiBerjalanDPA2, $txtPengajuanDPA2, $txtSisaBudgetDPA2, $TahunBudget));
						}
						else {
							if($DPA == 'DPA1'){
								$sqlUpdateDetailDPA1 = "UPDATE dtlpersetujuanpengeluaranbudget SET Deleted = 1, UpdatedOn = now(), UpdatedBy = '$npk' where NoPP = '$NoPP' and DPA = 1 and deleted = 0";
								$this->db->query($sqlUpdateDetailDPA1);
								$sqlInsertDetailDPA1 = "INSERT INTO dtlpersetujuanpengeluaranbudget
								(Deleted, NoPP, DPA, Pengeluaran, Budget, Realisasi, RealisasiBerjalan, Pengajuan, SisaBudget, CreatedOn, CreatedBy)
								VALUES (0, ?, 1, ?, ?, ?, ?, ?, ?,?, now(), '$npk');
								";
								$totalBudget+=$txtBudgetDPA1;
								$totalPengajuan+=$txtPengajuanDPA1;
								$totalSisaBudget+=$txtSisaBudgetDPA1;
								$this->db->query($sqlInsertDetailDPA1, array($NoPP, $txtPengeluaranDPA1, $txtBudgetDPA1, $txtRealisasiDPA1, $txtRealisasiBerjalanDPA1, $txtPengajuanDPA1, $txtSisaBudgetDPA1, $TahunBudget));
							}else if($DPA == 'DPA2'){
								$sqlUpdateDetailDPA2 = "UPDATE dtlpersetujuanpengeluaranbudget SET Deleted = 1, UpdatedOn = now(), UpdatedBy = '$npk' where NoPP = '$NoPP' and DPA = 2 and deleted = 0";
								$this->db->query($sqlUpdateDetailDPA2);
								$sqlInsertDetailDPA2 = "INSERT INTO dtlpersetujuanpengeluaranbudget
								(Deleted, NoPP, DPA, Pengeluaran, Budget, Realisasi, RealisasiBerjalan, Pengajuan, SisaBudget, CreatedOn, CreatedBy)
								VALUES (0, ?, 2, ?, ?, ?, ?, ?, ?,?, now(), '$npk');
								";
								$totalBudget+=$txtBudgetDPA2;
								$totalPengajuan+=$txtPengajuanDPA2;
								$totalSisaBudget+=$txtSisaBudgetDPA2;
								$this->db->query($sqlInsertDetailDPA2, array($NoPP, $txtPengeluaranDPA2, $txtBudgetDPA2, $txtRealisasiDPA2, $txtRealisasiBerjalanDPA2, $txtPengajuanDPA2, $txtSisaBudgetDPA2, $TahunBudget));
							}
						}
						$var["NoPP"] = $NoPP;
						$var["Keterangan"] = $Keterangan;
						$var["Status"] = $txtSelStatus;
						$var["KodeActivity"] = $KodeActivity;
						$var["NamaActivity"] = $NamaActivity;
						$var["DPA"] = $DPA;
						$var["JenisPengeluaran"] = $JenisPengeluaran;
						$var["KodeCoaDPA1"] = $hidKodeCoaDPA1;
						$var["KodeCoaDPA2"] = $hidKodeCoaDPA2;
						$var["namaUser"] = $user[0]->nama;
						$var["jabatanUser"] = $user[0]->jabatan;
						$var["emailUser"] = $user[0]->email;
						$var["departemenUser"] = $user[0]->departemen;
						$var["namaAtasan"] = $atasan[0]->nama;
						$var["jabatanAtasan"] = $atasan[0]->jabatan;
						$var["emailAtasan"] = $atasan[0]->email;
						$var["departemenAtasan"] = $atasan[0]->departemen;

						$var["totalBudget"] = $totalBudget;
						$var["totalPengajuan"] = $totalPengajuan;
						$var["totalSisaBudget"] = $totalSisaBudget;
						$this->load->view('PersetujuanPengeluaranBudget/CetakProposal.php', $var);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		public function UpdatePurchaseRequest($ID = '')
		{
			try
			{
				if($this->session->userdata('logged_in'))
				{
					if(check_authorizedByName("Purchase Request"))
					{
						$DPA = strip_tags($this->input->post('txtDPA'));
						$HeaderPP = $this->ppb_model->getHeaderPP($ID);
						$KodeActivity = $HeaderPP->KodeActivity;
						$NamaActivity ="";
						$activityList = $this->bm_activity_model->getActivity($KodeActivity);
						if($activityList)
						{
							foreach($activityList as $row)
							{
								$NamaActivity = $row->NamaActivity;
							}
						}
						$TahunBudget = strip_tags($this->input->post('txtTahunBudget'));
						$var["TahunBudget"] = $TahunBudget;
						//$NamaActivity = strip_tags(substr($this->input->post('txtKodeActivity'),8, strlen($this->input->post('txtKodeActivity')-8)));
						$hidKodeCoa = strip_tags($this->input->post('hidKodeCoa'));
						$JenisPengeluaran = strip_tags($this->input->post('txtOpexCapex'));
						$Keterangan = strip_tags($this->input->post('txtKeterangan'));
						$txtHargaSatuan = strip_tags($this->input->post('txtHargaSatuan'));
						$txtQuantity = strip_tags($this->input->post('txtQuantity'));
						$txtTotalPengeluaran = strip_tags($this->input->post('txtTotalPengeluaran'));
						$txtBudget = strip_tags($this->input->post('txtBudget'));
						$txtRealisasi = strip_tags($this->input->post('txtRealisasi'));
						$txtRealisasiBerjalan = strip_tags($this->input->post('txtRealisasiBerjalan'));
						$txtPengajuan = strip_tags($this->input->post('txtPengajuan'));
						$txtSisaBudget = strip_tags($this->input->post('txtSisaBudget'));
						$txtSelStatus = strip_tags($this->input->post('txtSelStatus'));
						$totalBudget = 0;
						$totalPengajuan = 0;
						$totalSisaBudget = 0;
						$npk = $this->npkLogin;
						$user = $this->user->dataUser($npk);
						$NPKAtasan = ($user[0]->NPKAtasan);
						$atasan = $this->user->dataUser($NPKAtasan);
						$atasannyaatasan = $this->user->dataUser(($atasan[0]->NPKAtasan));
						$NoPP = strip_tags($this->input->post('txtNoPP'));
						$var["DPA"] = $DPA;
						$var["NoPP"] = $NoPP;
						$var["JenisPengeluaran"] = $JenisPengeluaran;
						$var["Keterangan"] = $Keterangan;
						$var["KodeCoa"] = $hidKodeCoa;
						$var["Quantity"] = $txtQuantity;
						$var["HargaSatuan"] = $txtHargaSatuan;
						$var["TotalPengeluaran"] = $txtTotalPengeluaran;
						$var["Status"] = $txtSelStatus;
						$var["namaUser"] = $user[0]->nama;
						$var["jabatanUser"] = $user[0]->jabatan;
						$var["emailUser"] = $user[0]->email;
						$var["departemenUser"] = $user[0]->departemen;
						$var["namaAtasan"] = $atasan[0]->nama;
						$var["jabatanAtasan"] = $atasan[0]->jabatan;
						$var["emailAtasan"] = $atasan[0]->email;
						$var["departemenAtasan"] = $atasan[0]->departemen;
						$var["namaAtasannyaatasan"] = $atasannyaatasan[0]->nama;
						$var["jabatanAtasannyaatasan"] = $atasannyaatasan[0]->jabatan;
						$var["emailAtasannyaatasan"] = $atasannyaatasan[0]->email;
						$var["departemenAtasannyaatasan"] = $atasannyaatasan[0]->departemen;

						$var["KodeActivity"] = $KodeActivity;
						$var["NamaActivity"] = $NamaActivity;
						$var["txtBudget"] = $txtBudget;
						$var["txtRealisasi"] = $txtRealisasi;
						$var["txtRealisasiBerjalan"] = $txtRealisasiBerjalan;
						$var["txtPengajuan"] = $txtPengajuan;
						$var["txtSisaBudget"] = $txtSisaBudget;
						$txtBudget = str_replace(',','',$txtBudget);
						$txtRealisasi = str_replace(',','',$txtRealisasi);
						$txtRealisasiBerjalan = str_replace(',','',$txtRealisasiBerjalan);
						$txtTotalPengeluaran = str_replace(',','',$txtTotalPengeluaran);
						$txtPengajuan = str_replace(',','',$txtPengajuan);
						$txtSisaBudget = str_replace(',','',$txtSisaBudget);
						$sqlUpdateMstr = "UPDATE persetujuanpengeluaranbudget
						SET Keterangan = ?, Status = '$txtSelStatus', UpdatedOn = now(), UpdatedBy = '$npk'
						WHERE ID = $ID;
						";
						$this->db->query($sqlUpdateMstr, array($Keterangan));
						$sqlUpdateDetail = "UPDATE dtlpersetujuanpengeluaranbudget SET Deleted = 1, UpdatedOn = now(), UpdatedBy = '$npk' where NoPP = '$NoPP' and deleted = 0";
						$this->db->query($sqlUpdateDetail);

						$sqlInsertDetail = "INSERT INTO dtlpersetujuanpengeluaranbudget
						(Deleted, NoPP, DPA, Pengeluaran, Budget, Realisasi, RealisasiBerjalan, Pengajuan, SisaBudget, TahunBudget, CreatedOn, CreatedBy)
						VALUES (0, '$NoPP', $DPA, '$txtTotalPengeluaran', '$txtBudget', '$txtRealisasi', '$txtRealisasiBerjalan', '$txtPengajuan', '$txtSisaBudget', $TahunBudget, now(), '$npk');
						";
						$totalBudget+=$txtBudget;
						$totalPengajuan+=$txtPengajuan;
						$totalSisaBudget+=$txtSisaBudget;
						$this->db->query($sqlInsertDetail);
						$txtHargaSatuan = str_replace(',','',$txtHargaSatuan);
						$txtQuantity = str_replace(',','',$txtQuantity);
						$sqlUpdateQty = "UPDATE dtlppbpurchaserequest SET Deleted = 1, UpdatedOn = now(), UpdatedBy = '$npk' where NoPP = '$NoPP' and deleted = 0";
						$this->db->query($sqlUpdateQty);
						$sqlInsertQty = "INSERT INTO dtlppbpurchaserequest
						(Deleted, NoPP, HargaSatuan, Quantity, CreatedOn, CreatedBy)
						VALUES (0, '$NoPP', $txtHargaSatuan, $txtQuantity, now(), '$npk');
						";
						$this->db->query($sqlInsertQty);
						$TipePRST = 0;
						if($txtSelStatus =='Budgeted'){
							if($JenisPengeluaran == 'Opex'){
								if($txtTotalPengeluaran <= 1000000){
									$TipePRST = "BOPEX510";
								}else if($txtTotalPengeluaran > 1000000){
									if($DPA == '1'){
										$TipePRST = "1BOPEX100";
									}
									else if($DPA == '2'){
										$TipePRST = "2BOPEX100";
									}
								}
							}
							if($JenisPengeluaran == 'Capex'){
								if($txtTotalPengeluaran <= 2500000){
									$TipePRST = "BCAPEX525";
								}else if($txtTotalPengeluaran > 2500000){
									if($DPA == '1'){
										$TipePRST = "1BCAPEX250";
									}
									else if($DPA == '2'){
										$TipePRST = "2BCAPEX250";
									}
								}
							}
						}else if($txtSelStatus =='Unbudgeted' || $txtSelStatus == 'Overbudget'){
							if($JenisPengeluaran == 'Opex'){
								$TipePRST = "UOOPEX";
							}
							if($JenisPengeluaran == 'Capex'){
								$TipePRST = "UOCAPEX";
							}
						}
						$var["TipePRST"] = $TipePRST;
						$var["totalBudget"] = $totalBudget;
						$var["totalPengajuan"] = $totalPengajuan;
						$var["totalSisaBudget"] = $totalSisaBudget;
						$this->load->view('PersetujuanPengeluaranBudget/CetakPR.php', $var);
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function UploadPPB($ID = ''){
			$config['upload_path']          = './assets/uploads/files/ppb/';
			$config['allowed_types']        = 'gif|jpg|png|pdf';
			$config['max_size']             = 1000000000;
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('uploadScanPPB'))
			{
				$error = array('error' => $this->upload->display_errors());
				$session_data = $this->session->userdata('logged_in');
				$this->npkLogin = $session_data['npk'];
				$sqlSelect =
				"SELECT * from persetujuanpengeluaranbudget where
				CreatedBy = '$this->npkLogin'
				and id = '$ID'
				";
				$data = $this->db->query($sqlSelect);
				$user = $this->user->dataUser($this->npkLogin);
				if($user[0]->departemen == 'HRGA' || $user[0]->departemen == 'Accounting Tax & Control' || $data->num_rows() != 0){
					//klo HRGA atau ada data user tsb
					$dataPPB = array(
						'title'=> 'Upload Persetujuan Pengeluaran Budget',
						'data' => $data->result(),
						'error' => $error
					);
					$this->load->helper(array('form','url'));
					$this->template->load('default','PersetujuanPengeluaranBudget/UploadPPB_view', $dataPPB);
				}else{
					redirect("PersetujuanPengeluaranBudget/PPBController/ListPP",'refresh');
				}

			}
			else
			{
				$filename = $_FILES['uploadScanPPB']['name'];
				$data = array('upload_data' => $this->upload->data());
				$filename = $data['upload_data']['file_name'];
				$sqlUpdate = "UPDATE PersetujuanPengeluaranBudget SET Filepath = '$filename' where ID = $ID ";
				$this->db->query($sqlUpdate);

				$this->template->load('default','PersetujuanPengeluaranBudget/UploadSuccess', $data);
			}
		}
		function KartuBayar($ID = ''){
			try
			{
				if($this->session->userdata('logged_in'))
				{
					$session_data = $this->session->userdata('logged_in');
					$npk = $this->npkLogin;
					$user = $this->user->dataUser($npk);
					$editview = array();
					//$result = array("ID"=>$value->ID, "NoPP"=>$value->NoPP, "Keterangan"=>$value->Keterangan, "Tipe"=>$value->Tipe, "PengeluaranDPA1"=>$PengeluaranDPA1, "PengeluaranDPA2"=>$PengeluaranDPA2, "TotalPengeluaran"=>$txtTotalPengeluaran, "FilePath"=>$value->FilePath);
					$sqlSelect =
					"SELECT * from persetujuanpengeluaranbudget ppb where
					ppb.id = '$ID'
					and ppb.deleted = 0
					";
					$data = $this->db->query($sqlSelect);
					if($data->num_rows() == 0){
						redirect("PersetujuanPengeluaranBudget/PPBController/ListPP",'refresh');
					}else{
						//SUBSTRING(ba.NamaActivity,1,LOCATE('DPA',ba.NamaActivity)-2) as NamaActivity
						$data = $data->result();
						$Tipe = $data[0]->Tipe;
						if($Tipe == 'PRPS'){
							foreach($data as $value){
								$HeaderPP = $this->ppb_model->getHeaderPP($ID);
								$KodeActivity = $HeaderPP->KodeActivity;
								$NamaActivity ="";
								$activityList = $this->bm_activity_model->getActivity($KodeActivity);
								if($activityList)
								{
									foreach($activityList as $row)
									{
										$NamaActivity = $row->NamaActivity;
									}
								}
								$sqlDetail = "SELECT * FROM dtlpersetujuanpengeluaranbudget
								where NoPP = '$value->NoPP' and deleted = 0";
								$result = $this->db->query($sqlDetail)->result();
								$pengeluaranDPA1 = 0;
								$pengeluaranDPA2 = 0;
								$TahunBudget = 0;

								foreach($result as $dtl){
									if($dtl->DPA == 1){
										$DPA = 1;
										$pengeluaranDPA1 = $dtl->Pengeluaran;
										$TahunBudget = $dtl->TahunBudget;
									}else{
										$DPA = 2;
										$pengeluaranDPA2 = $dtl->Pengeluaran;
										$TahunBudget = $dtl->TahunBudget;
									}
								}
								if(count($result) == 2){
									$DPA = 'all';
								}
								$editview = array("ID"=>$value->ID,
									"NoPP"=>$value->NoPP,
									"DPA"=>$DPA,
									"Keterangan"=>$value->Keterangan,
									"NamaActivity"=>$NamaActivity,
									"KodeActivity"=>$value->KodeActivity,
									"JenisPengeluaran"=>$value->JenisPengeluaran,
									"PengeluaranDPA1"=>$pengeluaranDPA1,
									"PengeluaranDPA2"=>$pengeluaranDPA2,
									"Status"=>$value->Status,
									"FilePath"=>$value->FilePath,
									"isReleased"=>$value->isReleased,
									"isFinished"=>$value->isFinished,
									"TahunBudget"=> $TahunBudget
								);
								$selecKartuBayar = "SELECT * FROM dtlppbkartubayar where NoPP = '$value->NoPP' and Deleted = 0 and CreatedBy LIKE '%DPA1%' and IsSubmitted = 0";
								$totalKartuBayarIsNotSubmitted =$this->db->query($selecKartuBayar)->num_rows();
								if($totalKartuBayarIsNotSubmitted > 0){
									$adaYangPerluDisubmit = true;
								}else{
									$adaYangPerluDisubmit = false;
								}
								$selecKartuBayar = "SELECT * FROM dtlppbkartubayar where NoPP = '$value->NoPP' and Deleted = 0 and CreatedBy LIKE '%DPA1%' and IsSubmitted = 1";
								$totalKartuBayarIsSubmitted =$this->db->query($selecKartuBayar)->num_rows();
								if($totalKartuBayarIsSubmitted > 0){
									$adaYangPerluDiBatalkan = true;
								}else{
									$adaYangPerluDiBatalkan = false;
								}
							}
							$dataProposal = array(
								'title'=> 'Kartu Bayar Proposal',
								'data' => $editview,
								'admin' => '1',
								'npk' => $npk,
								'user' => $user,
								'adaYangPerluDisubmit' => $adaYangPerluDisubmit,
								'adaYangPerluDiBatalkan' => $adaYangPerluDiBatalkan
								);
							$this->load->helper(array('form','url'));
							$this->template->load('default','PersetujuanPengeluaranBudget/KartuBayarProposal_view', $dataProposal);
						}else if($Tipe == 'PRST'){
							foreach($data as $value){
								$sqlDetail = "SELECT * FROM dtlpersetujuanpengeluaranbudget where NoPP = '$value->NoPP' and deleted = 0";
								$result = $this->db->query($sqlDetail)->result();
								$sqlDetail2 = "SELECT * FROM dtlppbpurchaserequest where NoPP = '$value->NoPP' and deleted = 0";
								$result2 = $this->db->query($sqlDetail2)->result();
								$HeaderPP = $this->ppb_model->getHeaderPP($ID);
								$KodeActivity = $HeaderPP->KodeActivity;
								$NamaActivity ="";
								$activityList = $this->bm_activity_model->getActivity($KodeActivity);
								if($activityList)
								{
									foreach($activityList as $row)
									{
										$NamaActivity = $row->NamaActivity;
									}
								}
								$DPA = $result[0]->DPA;
								$editview = array("ID"=>$value->ID,
									"NoPP"=>$value->NoPP,
									"DPA"=>$DPA,
									"Keterangan"=>$value->Keterangan,
									"NamaActivity"=>$NamaActivity,
									"KodeActivity"=>$value->KodeActivity,
									"JenisPengeluaran"=>$value->JenisPengeluaran,
									"Pengeluaran"=>$result[0]->Pengeluaran,
									"HargaSatuan"=>$result2[0]->HargaSatuan,
									"Quantity"=>$result2[0]->Quantity,
									"Status"=>$value->Status,
									"FilePath"=>$value->FilePath,
									"isReleased"=>$value->isReleased,
									"isFinished"=>$value->isFinished,
									"TahunBudget"=>$result[0]->TahunBudget
								);
								$selecKartuBayar = "SELECT * FROM dtlppbkartubayar where NoPP = '$value->NoPP' and Deleted = 0 and CreatedBy LIKE '%DPA1%' and IsSubmitted = 0";
								$totalKartuBayarIsNotSubmitted =$this->db->query($selecKartuBayar)->num_rows();
								if($totalKartuBayarIsNotSubmitted > 0){
									$adaYangPerluDisubmit = true;
								}else{
									$adaYangPerluDisubmit = false;
								}
								$selecKartuBayar = "SELECT * FROM dtlppbkartubayar where NoPP = '$value->NoPP' and Deleted = 0 and CreatedBy LIKE '%DPA1%' and IsSubmitted = 1";
								$totalKartuBayarIsSubmitted =$this->db->query($selecKartuBayar)->num_rows();
								if($totalKartuBayarIsSubmitted > 0){
									$adaYangPerluDiBatalkan = true;
								}else{
									$adaYangPerluDiBatalkan = false;
								}
							}
							$dataPR = array(
								'title'=> 'Kartu Bayar Purchase Request',
								'data' => $editview,
								'admin' => '1',
								'npk' => $npk,
								'user' => $user,
								'adaYangPerluDisubmit' => $adaYangPerluDisubmit,
								'adaYangPerluDiBatalkan' => $adaYangPerluDiBatalkan
								);
							$this->load->helper(array('form','url'));
							$this->template->load('default','PersetujuanPengeluaranBudget/KartuBayarPR_view', $dataPR);
						}
					}
				}
				else
				{
					redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ajax_submitKartuBayar()
		{
			try
			{
				$ID = $_POST['ID'];
				$Tipe = $_POST['Tipe'];
				$HeaderPP = $this->ppb_model->getHeaderPP($ID);
				$NoPP = $HeaderPP->NoPP;
				$this->NoPP = $NoPP;
				if($Tipe == '0'){
					//membatalkan
					$sqlUpdate = "UPDATE dtlppbkartubayar
					set isSubmitted = $Tipe where NoPP = '$NoPP' and deleted = 0 and isSubmitted = 1 and CreatedBy like '%DPA1%'";
				}else{
					//mensubmit
					$sqlUpdate = "UPDATE dtlppbkartubayar
					set isSubmitted = $Tipe where NoPP = '$NoPP' and deleted = 0 and isSubmitted = 0 and CreatedBy like '%DPA1%'";
				}
				$this->db->query($sqlUpdate);
				if($this->db->affected_rows() > 0){
					if($HeaderPP)
					{
						$DetailPP = $this->ppb_model->getDetailPP($HeaderPP->NoPP);
						foreach($DetailPP as $detail){
							$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(kb.Nominal),0),0) as TotalRealisasiPengajuan FROM
									persetujuanpengeluaranbudget ppb
									join dtlppbkartubayar kb on ppb.NoPP = kb.NoPP
									where kb.NoPP = '$HeaderPP->NoPP' and kb.CreatedBy like '%DPA1%' and ppb.Deleted = 0 and kb.Deleted = 0 and kb.isSubmitted  = 1";
							$totalPenggunaan = $this->db->query($sqlSelectTotalNominal)->result();
							$totalPenggunaan = $totalPenggunaan[0]->TotalRealisasiPengajuan;
							$totalPenggunaan = str_replace(',','',$totalPenggunaan);
							$totalPengajuan = $detail->Pengajuan;
							$saldoPersetujuanPengeluaran = $totalPengajuan - $totalPenggunaan;
							if($saldoPersetujuanPengeluaran <= (0.05*$totalPengajuan)){
								$KodeActivity = $HeaderPP->KodeActivity;
								$NamaActivity = "";
								if(strlen($KodeActivity) == 5){
									$act = $this->bm_activity_model->getActivityAll($KodeActivity);
									$NamaActivity = substr($act[0]->NamaActivity, 0,strpos($act[0]->NamaActivity,"-"));

								}else{
									$act = $this->bm_activity_model->getActivity($KodeActivity);
									$NamaActivity = $act[0]->NamaActivity;
								}
								$user = $this->user->dataUser($HeaderPP->CreatedBy);
								foreach($user as $row){
									$user = $row->email;
								}
								$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service Budget Persetujuan Pengeluaran!');
								$this->email->to($user);
								$this->email->subject("Warning Sisa Saldo Persetujuan Pengeluaran");
								$message = 'Dear User, terdapat Persetujuan Pengeluaran yang saldonya akan habis, yaitu: <br/>';
								$message .= 'No. Persetujuan Pengeluaran'.$NoPP.'<br/>';
								$message .= 'Type Persetujuan Pengeluaran: '.$HeaderPP->Tipe.'<br/>';
								$message .= 'Keterangan: '.$HeaderPP->Tipe.'<br/>';
								$message .= 'Total Pengajuan: '.number_format($totalPengajuan).'<br/>';
								$message .= 'Total Penggunaan: '.number_format($totalPenggunaan).'<br/>';
								$message .= 'Sisa Saldo: '.number_format($saldoPersetujuanPengeluaran).'<br/>';
								$message .= 'Silahkan klik <a href="http://dpa2/ESS/index.php/PersetujuanPengeluaranBudget/PPBController/ListPP">link</a> ini untuk melihat detil';
								if($message != ''){
									$this->email->message($message);
									$this->email->send();
								}
							}
						}
					}
				}
				echo json_encode(array("0"=>"Success"));
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ajax_releaseKartuBayar()
		{
			try
			{
				$ID = $_POST['ID'];
				$Tipe = $_POST['Tipe'];
				$HeaderPP = $this->ppb_model->getHeaderPP($ID);
				$NoPP = $HeaderPP->NoPP;
				$this->NoPP = $NoPP;
				if($Tipe == '0'){
					//membatalkan
					$sqlUpdate = "UPDATE persetujuanpengeluaranbudget set isReleased = $Tipe where NoPP = '$NoPP' and deleted = 0 and isReleased = 1";
				}else{
					//mensubmit
					$sqlUpdate = "UPDATE persetujuanpengeluaranbudget set isReleased = $Tipe where NoPP = '$NoPP' and deleted = 0 and isReleased = 0";
				}
				$this->db->query($sqlUpdate);
				echo json_encode(array("0"=>"Success"));
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		function ajax_deleteKartuBayar(){
			try
			{
				$ID = $_POST['ID'];
				$HeaderPP = $this->ppb_model->getHeaderPP($ID);
				$NoPP = $HeaderPP->NoPP;
				$this->NoPP = $NoPP;
				$sqlUpdate = "UPDATE persetujuanpengeluaranbudget set DELETED = 1 where NoPP = '$NoPP' and deleted = 0 and isReleased = 0";
				$this->db->query($sqlUpdate);

				echo json_encode(array("0"=>"Success"));
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}

		function ajax_tutupKartuBayar()
		{
			try
			{
				$ID = $_POST['ID'];
				$Tipe = $_POST['Tipe'];
				$HeaderPP = $this->ppb_model->getHeaderPP($ID);
				$NoPP = $HeaderPP->NoPP;
				$this->NoPP = $NoPP;
				if($Tipe == '0'){
					//membatalkan
					$sqlUpdate = "UPDATE persetujuanpengeluaranbudget set isFinished = $Tipe where NoPP = '$NoPP' and deleted = 0 and isFinished = 1";
				}else{
					//mensubmit
					$sqlUpdate = "UPDATE persetujuanpengeluaranbudget set isFinished = $Tipe where NoPP = '$NoPP' and deleted = 0 and isFinished = 0";
				}
				$this->db->query($sqlUpdate);
				echo json_encode(array("0"=>"Success"));
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
				throw new Exception( 'Something really gone wrong', 0, $e);
			}
		}
		public function getAllnoPPDPA2()
		{
			try
			{
				$this->load->model('ppb_model','',TRUE);
				$result = $this->ppb_model->getNoPPDPA2('',1,2);
				if($result){
					echo json_encode($result);
				}else{
					echo "kosong";
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			}
		}
		public function getDetailPP(){
			try{
				//CORE
				$ID = $_GET["ID"];
				$DPA = $_GET["DPA"];
				$this->load->model('ppb_model','',TRUE);
				$HeaderPP = $this->ppb_model->getHeaderPP($ID);
				if($HeaderPP)
				{
					$DetailPP = $this->ppb_model->getDetailPPforCORE($HeaderPP->ID);
					foreach($DetailPP as $detail){
						if($detail->DPA == 2){
							$keterangan = $detail->NamaActivity.' | '.$HeaderPP->Keterangan;
							$totalPengajuan = $detail->Pengajuan;
							$res[] = $keterangan == null?"0":$keterangan;
							$res[] = $totalPengajuan == null?"0":$totalPengajuan;
							$KodeActivity = $HeaderPP->KodeActivity;
							$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(kb.Nominal),0),0) as TotalRealisasiPengajuan FROM
								persetujuanpengeluaranbudget ppb
								join dtlppbkartubayar kb on ppb.NoPP = kb.NoPP
								where kb.NoPP = '$HeaderPP->NoPP' and ppb.Deleted = 0 and kb.Deleted = 0 and kb.isSubmitted  = 1";
							$totalPenggunaan = $this->db->query($sqlSelectTotalNominal)->result();
							$totalPenggunaan = $totalPenggunaan[0]->TotalRealisasiPengajuan;
							$totalPenggunaan = str_replace(',','',$totalPenggunaan);
							$res[] = $totalPenggunaan == null?"0":$totalPenggunaan;
							$saldoPersetujuanPengeluaran = $totalPengajuan - $totalPenggunaan;
							$res[] = $saldoPersetujuanPengeluaran == null?"0":$saldoPersetujuanPengeluaran;
							$res[] = $HeaderPP->NoPP;
							$res[] = $detail->NamaActivity;
							echo json_encode($res);
						}
					}
				}else{
					echo "kosong";
				}
			}
			catch(Exception $e)
			{
				log_message( 'error', $e->getMessage( ) . ' in ' . $e->getFile() . ':' . $e->getLine() );
			}
		}
		public function createKartuBayarCORE(){

			//CORE
			//ubah kartu bayar
			$data = json_decode(file_get_contents('php://input'), true);
			$NoPP = $data['NoPP'];
			$Description = $data['Description'];
			$Description = str_replace("'", "\'", $Description);
			$KodeInvoiceBE = $data['KodeInvoiceBE'];
			$Nominal = $data['Nominal'];
			$Vendor = $data['Vendor'];
			$sqlInsertKartuBayar = "INSERT INTO dtlppbkartubayar(
				Deleted ,NoPP,Keterangan ,Nominal ,isSubmitted ,CreatedBy ,CreatedOn, TanggalPermintaanKas, NoPermintaanKas, Vendor) VALUES (
					0
					,'$NoPP'
					,'$Description'
					,$Nominal
					,0
					,'$KodeInvoiceBE"."DPA2'
					,now()
					,now()
					,''
					,'$Vendor'
				)";
			$this->db->query($sqlInsertKartuBayar);
			if ($this->db->_error_message()) {
				$arr = array('mesage'=>'fail');
				echo json_encode($arr);// Or do whatever you gotta do here to raise an error
			} else {
				$result = $this->db->affected_rows();
				if ($result == 0) {
					$arr = array('mesage'=>'no row updated');
					echo json_encode($arr);
				} else {
					$arr = array('mesage'=>'success');
					echo json_encode($arr);
				}
			}
		}
		public function approvedeleteKartuBayarCORE(){
			//CORE
			$data = json_decode(file_get_contents('php://input'), true);
			$NoPP = $data['NoPP'];
			$KodeInvoiceBE = $data['KodeInvoiceBE'];
			$Mode = $data['mode'];
			if($Mode == "delete"){
				$sqlUpdateKartuBayar = "UPDATE dtlppbkartubayar
				set Deleted = 1,
				UpdatedOn = now(),
				UpdatedBy = '$KodeInvoiceBE'
				WHERE CreatedBy = '$KodeInvoiceBE"."DPA2'
				and NoPP = '$NoPP'";
			}else if($Mode == "approve"){
				$KodeBEAccpac = $data['KodeBEAccpac'];
				$sqlUpdateKartuBayar = "UPDATE dtlppbkartubayar
				set isSubmitted = 1,
				UpdatedOn = now(),
				UpdatedBy = '$KodeInvoiceBE',
				NoPermintaanKas = '$KodeBEAccpac'
				WHERE CreatedBy = '$KodeInvoiceBE"."DPA2'
				and NoPP = '$NoPP'";
			}
			$this->db->query($sqlUpdateKartuBayar);
			if ($this->db->_error_message()) {
				$arr = array('mesage'=>'fail');
				echo json_encode($arr);// Or do whatever you gotta do here to raise an error
			} else {
				$result = $this->db->affected_rows();
				if ($result == 0) {
					$arr = array('mesage'=>'no row updated');
					echo json_encode($arr);
				} else {
					$HeaderPP = $this->ppb_model->getHeaderPPbyNoPP($NoPP);
					if($HeaderPP)
					{
						$DetailPP = $this->ppb_model->getDetailPP($HeaderPP->NoPP);
						foreach($DetailPP as $detail){
							$sqlSelectTotalNominal = "SELECT format(IFNULL(sum(kb.Nominal),0),0) as TotalRealisasiPengajuan FROM
									persetujuanpengeluaranbudget ppb
									join dtlppbkartubayar kb on ppb.NoPP = kb.NoPP
									where kb.NoPP = '$HeaderPP->NoPP' and (kb.CreatedBy LIKE '%/%' OR kb.CreatedBy LIKE '%DPA2%') and ppb.Deleted = 0 and kb.Deleted = 0 and kb.isSubmitted  = 1";
							$totalPenggunaan = $this->db->query($sqlSelectTotalNominal)->result();
							$totalPenggunaan = $totalPenggunaan[0]->TotalRealisasiPengajuan;
							$totalPenggunaan = str_replace(',','',$totalPenggunaan);
							$totalPengajuan = $detail->Pengajuan;
							$saldoPersetujuanPengeluaran = $totalPengajuan - $totalPenggunaan;

							if($saldoPersetujuanPengeluaran <= (0.05*$totalPengajuan)){
								$KodeActivity = $HeaderPP->KodeActivity;
								if(strlen($KodeActivity) == 5){
									$KodeActivity.='2';
								}
								$act = $this->bm_activity_model->getActivity($KodeActivity);
								$NamaActivity = $act[0]->NamaActivity;
								$user = $this->user->dataUser($HeaderPP->CreatedBy);
								foreach($user as $row){
									$user = $row->email;
								}
								$this->email->from('noreply@dpa.co.id', 'Enterprise Self Service Budget Persetujuan Pengeluaran!');
								$this->email->to($user);
								$this->email->subject("Warning Sisa Saldo Persetujuan Pengeluaran");
								$message = 'Dear User, terdapat Persetujuan Pengeluaran yang saldonya akan habis, yaitu: <br/>';
								$message .= 'No. Persetujuan Pengeluaran:'.$HeaderPP->NoPP.'<br/>';
								$message .= 'Type Persetujuan Pengeluaran: '.$HeaderPP->Tipe.'<br/>';
								$message .= 'Keterangan: '.$HeaderPP->Tipe.'<br/>';
								$message .= 'Total Pengajuan: '.number_format($totalPengajuan).'<br/>';
								$message .= 'Total Penggunaan: '.number_format($totalPenggunaan).'<br/>';
								$message .= 'Sisa Saldo: '.number_format($saldoPersetujuanPengeluaran).'<br/>';
								$message .= 'Silahkan klik <a href="http://dpa2/ESS/index.php/PersetujuanPengeluaranBudget/PPBController/ListPP">link</a> ini untuk melihat detil';
								$this->email->message($message);

								$this->email->send();
							}
						}
					}
					$arr = array('mesage'=>'success');
					echo json_encode($arr);
				}
			}
		}
	}
?>