<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class PPB extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('menu','',TRUE);
        $this->load->model('user','',TRUE);
        $this->load->model('ldarchive_model','',TRUE);
        $this->load->library('grocery_crud');
        $this->load->model('ppb_model','',TRUE);
        $this->load->model('bm_activity_model','',TRUE);
        $this->load->model('bm_summarybudget_model','',TRUE);
        $this->load->model('usertask','',TRUE);
        $this->load->model('lembur_model','',TRUE);
        $this->load->model('dtltrkrwy_model','',TRUE);
        $session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
	}
    public function index()
    {
    }
	public function PPB_admin()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Purchase Request"))
			{
				$this->npkLogin = $session_data['npk'];
				$this->_PPB();
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	public function _PPB()
    {
		$crud = new grocery_crud();
		$crud->set_subject('Persetujuan Pengeluaran Budget Admin');
		$crud->set_table('persetujuanpengeluaranbudget');
		$crud->where('persetujuanpengeluaranbudget.deleted','0');
		$crud->where('persetujuanpengeluaranbudget.isReleased','0');
		$crud->unset_add();
		$crud->unset_edit();
		$crud->columns('NoPP', 'Tipe', 'KodeActivity', 'Keterangan', 'JenisPengeluaran', 'Status', 'isReleased');
		$crud->fields('NoPP', 'Tipe', 'KodeActivity', 'Keterangan', 'JenisPengeluaran', 'Status', 'isReleased');
		$crud->callback_column('isReleased',array($this,'_column_callback_IsReleased'));
		$crud->callback_delete(array($this,'_delete'));
        $output = $crud->render();
        $this-> _outputview($output, 'list');
    }
    function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		$data = array(
			'title' => 'Persetujuan Pengeluaran Budget',
			'body' => $output
		);
		$this->load->helper(array('form','url'));
		$this->template->load('default','PersetujuanPengeluaranBudget/PPB_view',$data);
    }
	function _delete($primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		$post_array['Deleted'] = '1';
		return $this->db->update('persetujuanpengeluaranbudget',$post_array,array('ID' => $primary_key));
	}
	function _column_callback_IsReleased($value = '', $primary_key = null)
	{
		if($value == '0'){
			return 'Not Release';
		}else{
			return 'Released';
		}
	}
}

/* End of file main.php */
/* Location: ./application/controllers/main.php */