<html>
	<!--<script type="text/javascript" src="<?php echo $this->config->base_url(); ?>assets/css/bootflat/js/site.min.js"></script>-->
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
	<head>
		<title><?php $title ?></title>
		<script src="<?php echo $this->config->base_url(); ?>assets/js/autoNumeric.js" type="text/javascript"></script>
		<!--<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/bootflat/css/site.min.css">-->		
		<style>
			td {padding:5px 5px 5px 5px;}
		</style>
		<script type="text/javascript">
		function alertSizeDPA(pixels){
			pixels+=32;
			var DPA = document.getElementById('txtDPA').value;
			if(DPA == 1){
				document.getElementById('iFrameKartuBayarDPA1').style.height=pixels+"px";
			}else if(DPA == 2){
				document.getElementById('iFrameKartuBayarDPA2').style.height=pixels+"px";
			}else{
				document.getElementById('iFrameKartuBayarDPA1').style.height=pixels+"px";
				document.getElementById('iFrameKartuBayarDPA2').style.height=pixels+"px";
			}
		}
		</script>
		<script type="text/javascript">
			function submitKartuBayarPR(ID, Tipe){
				flag = true;
				if(flag){
					if(Tipe == 1){
						var x = window.confirm("Submit Kartu Bayar PR Ini?");
					}else if(Tipe == 0){
						var x = window.confirm("Batal Submit Kartu Bayar PR Ini?");
					}
					if(x){
						var ID = ID;
						var Tipe = Tipe;
						$('#divLoadingSubmit').show();
						$.ajax({
							url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_submitKartuBayar',
							type: "POST",
							data: { ID: ID, Tipe:Tipe},
							dataType: 'json',
							cache: false,
							success: function(data)
							{
								console.log(data);
								// var DPA = document.getElementById('txtDPA').value;
								// var KodeActivity = (document.getElementById('txtKodeActivity').value).substring(0,6);
								// loadActivityBasedOnDPA(KodeActivity, DPA);
								if(Tipe == 1){
									alert('Sukses Submit');
									location.reload();
								}else if(Tipe == 0){
									alert('Sukses Batal Submit');
									location.reload();
								}
								//document.getElementById('iFrameKartuBayarDPA1').contentWindow.location.reload();
								$('#divLoadingSubmit').hide();
							},
							error: function (request, status, error) {
								console.log(error);
								alert('Error');
							}
						});
					}else{
						event.preventDefault();
					}
				}else{
					event.preventDefault();
				}
			}
			function releaseKartuBayarPR(ID, Tipe){
				flag = true;
				if(flag){
					if(Tipe == 1){
						var x = window.confirm("Release Kartu Bayar PR Ini?");
					}else if(Tipe == 0){
						var x = window.confirm("Batalkan Release Kartu Bayar PR Ini?");
					}
					if(x){
						var ID = ID;
						var Tipe = Tipe;
						$('#divLoadingSubmit').show();
						$.ajax({
							url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_releaseKartuBayar',
							type: "POST",
							data: { ID: ID, Tipe:Tipe},
							dataType: 'json',
							cache: false,
							success: function(data)
							{
								console.log(data);
								if(Tipe == 1){
									alert('Sukses Release');
								}else if(Tipe == 0){
									alert('Sukses Batal Release');
								}
								location.reload();
								$('#divLoadingSubmit').hide();
							},
							error: function (request, status, error) {
								console.log(error);
								alert('Error');
							}
						});
					}else{
						event.preventDefault();
					}
				}else{
					event.preventDefault();
				}
			}

			function tutupKartuBayarPR(ID, Tipe){
				flag = true;
				if(flag){
					if(Tipe == 1){
						var x = window.confirm("Tutup Kartu Bayar PR Ini?");
					}else if(Tipe == 0){
						var x = window.confirm("Batalkan Tutup Kartu Bayar PR Ini?");
					}
					if(x){
						var ID = ID;
						var Tipe = Tipe;
						$('#divLoadingSubmit').show();
						$.ajax({
							url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_tutupKartuBayar',
							type: "POST",
							data: { ID: ID, Tipe:Tipe},
							dataType: 'json',
							cache: false,
							success: function(data)
							{
								console.log(data);
								if(Tipe == 1){
									alert('Sukses Tutup');
								}else if(Tipe == 0){
									alert('Sukses Batal Tutup');
								}
								location.reload();
								$('#divLoadingSubmit').hide();
							},
							error: function (request, status, error) {
								console.log(error);
								alert('Error');
							}
						});
					}else{
						event.preventDefault();
					}
				}else{
					event.preventDefault();
				}
			}
		</script>
	</head>
	<body>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">PURCHASE REQUEST | <a href="<?php echo $this->config->base_url().'assets/uploads/files/PPB/'. $data['FilePath']; ?>" download>download</a></h3>
			</div>
			<div class="panel-body">
				<?php echo validation_errors(); ?>
				<table border=0 class="table table-condensed" >
					<tr class="row">
						<td>No. Persetujuan Pengeluaran *</td>
						<td>
							<input type="hidden" value = "<?php echo $data['isReleased']; ?>" id="isReleased" name="isReleased" readonly/>
							<input class="form-control" type="text" id="txtNoPP" name="txtNoPP" value="<?php echo $data['NoPP']; ?>" readonly />
						</td>
					</tr>
					<tr class="row">
						<td>DPA *</td>
						<td>
							<input class="form-control" type="text" id="txtDPA" name="txtDPA" value="<?php echo $data['DPA']; ?>" readonly />
						</td>
					</tr>
					<tr class="row">
						<td>Nama Activity</td>
						<td>
							<input type="hidden" value = "<?php echo $data['TahunBudget']; ?>" id="txtTahunBudget" name="txtTahunBudget" readonly/>
							<input class="form-control" type="text" id="txtKodeActivity" name="txtKodeActivity" value="<?php echo $data['KodeActivity'].' - '.$data['NamaActivity']; ?>" readonly />
						</td>
					</tr>
					<tr class="row">
						<td>Jenis Pengeluaran</td>
						<td>
							<input class="form-control" type="hidden" id="hidKodeCoa" name="hidKodeCoa" readonly/>
							<input class="form-control" type="hidden" id="hidKodeCoaDPA2" name="hidKodeCoaDPA2" readonly/>
							<input class="form-control" type="text" id="txtOpexCapex" name="txtOpexCapex"value="<?php echo $data['JenisPengeluaran']; ?>" readonly/>
						</td>
					</tr>
					<tr class="row">
						<td>Keterangan</td>
						<td><input class="form-control" type="text" id="txtKeterangan" name="txtKeterangan" value="<?php echo $data['Keterangan']; ?>"readonly/>
						</td>
					</tr>
					<tr class="row">
						<td>SALDO PENGAJUAN</td>
						<td>
							<input class="form-control" type="hidden" id="hidtxtPengeluaran" name="hidtxtPengeluaran" value="<?php echo $data['Pengeluaran']; ?>"readonly/>
							<input class="form-control" type="text" id="txtPengeluaran" name="txtPengeluaran" value="<?php echo $data['Pengeluaran']; ?>"readonly/>
							<input class="form-control" type="hidden" id="hidtxtBudget" name="hidtxtBudget" readonly/>
							<input class="form-control" type="hidden" id="txtBudget" name="txtBudget" readonly/>
							<input class="form-control" type="hidden" id="hidtxtRealisasi" name="hidtxtRealisasi" readonly/>
							<input class="form-control" type="hidden" id="txtRealisasi" name="txtRealisasi" readonly/>
							<input class="form-control bg-success" type="hidden" id="hidtxtSisaBudget" name="hidtxtSisaBudget" readonly/>
							<input class="form-control" type="hidden" id="txtSisaBudget" name="txtSisaBudget" readonly/>
						</td>
					</tr>
					<tr class="row">
						<td>TOTAL PENGGUNAAN</td>
						<td>
							<input class="form-control" type="hidden" id="hidtxtRealisasiBerjalan" name="hidtxtRealisasiBerjalan" readonly/>
							<input class="form-control" type="text" id="txtRealisasiBerjalan" name="txtRealisasiBerjalan" readonly/>
						</td>
					</tr>
					<tr class="row">
						<td>SISA SALDO PENGAJUAN</td>
						<td>
							<input class="form-control" type="hidden" id="hidtxtPengajuan" name="hidtxtPengajuan" readonly/>
							<input class="form-control" type="text" id="txtPengajuan" name="txtPengajuan" readonly/>
						</td>
					</tr>
					<?php if($data['isReleased'] == 0 && $user[0]->departemen == "HRGA"){
					?>
					<tr class="row">
						<td>
							<button class="btn btn-warning" onclick="releaseKartuBayarPR(<?php echo $data['ID']; ?>, 1)">Release Kartu Bayar</button>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
					</tr>
					<?php
					}
					?>
					<?php if($data['isReleased'] == 1 && $user[0]->departemen == "HRGA"){
					?>
					<tr class="row">
						<td>
							<button class="btn btn-danger" onclick="releaseKartuBayarPR(<?php echo $data['ID']; ?>, 0)">Batal Release Kartu Bayar</button>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
					</tr>
					<?php
					}
					?>
					<?php if($data['isFinished'] == 0 && $user[0]->departemen == "HRGA"){
					?>
					<tr class="row">
						<td>
							<button class="btn btn-danger" onclick="tutupKartuBayarPR(<?php echo $data['ID']; ?>, 1)">Tutup Kartu Bayar</button>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
					</tr>
					<?php
					}
					?>
					<?php if($data['isFinished'] == 1 && $user[0]->departemen == "HRGA"){
					?>
					<tr class="row">
						<td>
							<button class="btn btn-danger" onclick="tutupKartuBayarPR(<?php echo $data['ID']; ?>, 0)">Batal Tutup Kartu Bayar</button>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
					</tr>
					<?php
					}
					?>
					<?php
					if($data['DPA'] == 1 && $data['isReleased'] == 1 && $data['isFinished'] == 0){
					?>
					
					<tr class="row">
						<th colspan="2">
							KARTU BAYAR DPA 1
						</th>
					</tr>
					<tr class="row">
						<td colspan="2">
							<iframe id="iFrameKartuBayarDPA1" src="<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/KartuBayar/KartuBayarDPA1/<?php echo $data['ID']; ?>" width="100%"seamless frameBorder="0">
								<p>Your browser does not support iframes.</p>
							</iframe>
						</td>
					</tr>
					<tr class="row">
						<td>
							<?php if($adaYangPerluDisubmit){
							?>
								<button class="btn btn-success" onclick="submitKartuBayarPR(<?php echo $data['ID']; ?>, 1)">Submit Kartu Bayar</button>
							<?php
							}if($adaYangPerluDiBatalkan){
							?>
								<button class="btn btn-danger" onclick="submitKartuBayarPR(<?php echo $data['ID']; ?>, 0)">Batal Submit Kartu Bayar</button>
							<?php
							}?>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
					</tr>
					<?php
					}else if($data['DPA'] == 2 && $data['isReleased'] == 1 && $data['isFinished'] == 0){
						?>
					<tr class="row">
						<th colspan="2">
							KARTU BAYAR DPA 2
						</th>
					</tr>
					<tr class="row">
							<td colspan="2">
								<iframe id="iFrameKartuBayarDPA2" src="<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/KartuBayar/KartuBayarDPA2/<?php echo $data['ID']; ?>" width="100%"seamless frameBorder="0">
									<p>Your browser does not support iframes.</p>
								</iframe>
							</td>
						</tr>
						<?php
					}else if($data['DPA'] == 'all'  && $data['isReleased'] == 1 && $data['isFinished'] == 0){
						?>
					<tr class="row">
						<th colspan="2">
							KARTU BAYAR DPA 1
						</th>
					</tr>
					<tr class="row">
							<td colspan="2">
								<iframe id="iFrameKartuBayarDPA1" src="<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/KartuBayar/KartuBayarDPA1/<?php echo $data['ID']; ?>" width="100%"seamless frameBorder="0">
									<p>Your browser does not support iframes.</p>
								</iframe>
							</td>
						</tr>
					<tr class="row">
						<td>
							<button class="btn btn-success" onclick="submitKartuBayarPR(<?php echo $data['ID']; ?>, 1)">Submit Kartu Bayar</button>
							<button class="btn btn-danger" onclick="submitKartuBayarPR(<?php echo $data['ID']; ?>, 0)">Batal Submit Kartu Bayar</button>
							<div id="divLoadingSubmit" hidden="true" class="col-sm-2" style="padding-top:7px"><i class="fa fa-refresh fa-spin"></i></div>
						</td>
					</tr>
					<tr class="row">
						<th colspan="2">
							KARTU BAYAR DPA 2
						</th>
					</tr>
					<tr class="row">
						<td colspan="2">
							<iframe id="iFrameKartuBayarDPA2" src="<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/KartuBayar/KartuBayarDPA2/<?php echo $data['ID']; ?>" width="100%"seamless frameBorder="0">
								<p>Your browser does not support iframes.</p>
							</iframe>
						</td>
					</tr>
					<?php
					}
					?>
				</table>
				<div class="row">    
					<div class="col-md-12"> 
						<iframe src="<?php echo $this->config->base_url().'assets/uploads/files/PPB/'. $data['FilePath']; ?>" width="100%" height="720px">
						This browser does not support PDFs. Please download the PDF to view it: <a href="<?php echo $this->config->base_url().'assets/uploads/files/ppb/'. $data->FilePath; ?>">Download PDF</a>
						</iframe>   
					</div>
				</div>
			</div>
		</div>
	</body>
	<script src="<?php echo $this->config->base_url(); ?>plugins/select2/select2.full.min.js"></script>
	<script type="text/javascript">
		$( window ).load(function() {
			var TahunBudget = document.getElementById('txtTahunBudget').value;
			$('#txtPengeluaran').hide();
			$('#txtRealisasiBerjalan').hide();
			$('#txtPengajuan').hide();
			var isReleased = document.getElementById('isReleased').value;
			if(isReleased == 1){
				$('#txtPengeluaran').show();
				$('#txtRealisasiBerjalan').show();
				$('#txtPengajuan').show();
			}
			var DPA = document.getElementById('txtDPA').value;
			$('.row').show();
			if(DPA == '1' || DPA == '2'){
				var KodeActivity = (document.getElementById('txtKodeActivity').value).substring(0,6);
				loadActivityBasedOnDPA(KodeActivity, DPA, TahunBudget);
				pengeluaranbeforeChanged = document.getElementById('txtPengeluaran').value;
			}
		});
		function ambilValueTxtBudget(){
			var Pengeluaran = document.getElementById('txtPengeluaran').value;
			if(Pengeluaran == ''){
				alert("Pengeluaran harus di isi!");
				flag = false;
			}
			var Budget = document.getElementById('txtBudget').value;
			var Realisasi = document.getElementById('txtRealisasi').value;
			var RealisasiBerjalan = document.getElementById('txtRealisasiBerjalan').value;
		}

		function loadActivityBasedOnDPA(KodeActivity, DPA, TahunBudget){
			var DPA = DPA;
			var TahunBudget = TahunBudget;
			var KodeActivity = KodeActivity;
			var NoPP = document.getElementById("txtNoPP").value;
			$.ajax({
				url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_kalkulasiBudgetRealisasi',
				type: "POST",
				data: { KodeActivity: KodeActivity, DPA: DPA, TahunBudget: TahunBudget },
				dataType: 'json',
				cache: false,
				success: function(data)
				{
					$.ajax({
						url: '<?php echo $this->config->base_url(); ?>index.php/PersetujuanPengeluaranBudget/PPBController/ajax_getSummaryBudget',
						type: "POST",
						data: { KodeActivity: KodeActivity, DPA: DPA, NoPP: NoPP, Source: "KartuBayar", TahunBudget: TahunBudget },
						dataType: 'json',
						cache: false,
						success: function(data)
						{
							console.log(data);
							if(data.length == 1){
								alert('Tidak ada budget DPA '+DPA+' untuk activity ini');
							}else{
								$('#txtBudget').val(data[0].BudgetFullYear);
								$('#txtRealisasi').val(data[0].RealisasiYTD);
								$('#hidKodeCoa').val(data[0].KodeCoa);
								$('#txtRealisasiBerjalan').val(data[1].TotalRealisasiPengajuan);
								$('#txtOpexCapex').val(data[0].JenisPengeluaran);
								var budget = document.getElementById('txtBudget').value;
								var realisasi = document.getElementById('txtRealisasi').value;
								var realisasiBerjalan = document.getElementById('txtRealisasiBerjalan').value;
								var nominalPengeluaran = document.getElementById('txtPengeluaran').value;
								document.getElementById('hidtxtPengeluaran').value = nominalPengeluaran;

								budget = budget.replace(/,/g, "");
								realisasi = realisasi.replace(/,/g, "");
								realisasiBerjalan = realisasiBerjalan.replace(/,/g, "");
								nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
								document.getElementById('hidtxtBudget').value = budget;
								document.getElementById('hidtxtPengajuan').value = nominalPengeluaran;
								document.getElementById('txtPengajuan').value = nf.format(nominalPengeluaran-realisasiBerjalan);

								var sisaBudget = budget - realisasi - realisasiBerjalan;
								document.getElementById('hidtxtSisaBudget').value = sisaBudget;
								document.getElementById('txtSisaBudget').value = nf.format(sisaBudget);
								if(sisaBudget < 0){
									alert('Sisa Budget kurang dari 0');
								}
							}
							$('#divLoadingSubmit').hide();
						},
						error: function (request, status, error) {
							console.log(error);
							$('#divLoadingSubmit').hide();
						}
					});
					$('#divLoadingSubmit').hide();
				},
				error: function (request, status, error) {
					console.log(error);
					$('#divLoadingSubmit').hide();
				}
			});
		}
	</script>
	<script type="text/javascript">
		function clear(){
			$('#txtPengeluaran').prop('disabled','true');
		}
		$(function($) {
			$('#txtPengeluaran').autoNumeric('init', { lZero: 'deny', aSep: ',', mDec: 0 });
		});
		var nf = new Intl.NumberFormat();
		$("#txtPengeluaran").on('change', function() {
			var budget = document.getElementById('txtBudget').value;
			var realisasi = document.getElementById('txtRealisasi').value;
			var realisasiBerjalan = document.getElementById('txtRealisasiBerjalan').value;
			var nominalPengeluaran = document.getElementById('txtPengeluaran').value;
			document.getElementById('hidtxtPengeluaran').value = nominalPengeluaran;
			document.getElementById('txtPengajuan').value = nominalPengeluaran;
			if(pengeluaranbeforeChanged != nominalPengeluaran){
				isChanged = true;
			}else{
				isChanged = false;
			}
			budget = budget.replace(/,/g, "");
			realisasi = realisasi.replace(/,/g, "");
			realisasiBerjalan = realisasiBerjalan.replace(/,/g, "");
			nominalPengeluaran = nominalPengeluaran.replace(/,/g, "");
			document.getElementById('hidtxtBudget').value = budget;
			document.getElementById('hidtxtPengajuan').value = nominalPengeluaran;
			var sisaBudget = budget - realisasi - realisasiBerjalan - nominalPengeluaran;
			document.getElementById('hidtxtSisaBudget').value = sisaBudget;
			if(sisaBudget < 0){
				alert('Sisa Budget kurang dari 0');
			}
			document.getElementById('txtSisaBudget').value = nf.format(sisaBudget);
		});
	</script>
	
</html>
