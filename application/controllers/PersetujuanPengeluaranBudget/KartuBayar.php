<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class KartuBayar extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('menu','',TRUE);
        $this->load->model('user','',TRUE);
        $this->load->model('ldarchive_model','',TRUE);
        $this->load->library('grocery_crud');
        $this->load->model('ppb_model','',TRUE);
        $this->load->model('bm_activity_model','',TRUE);
        $this->load->model('bm_summarybudget_model','',TRUE);
        $this->load->model('usertask','',TRUE);
        $this->load->model('lembur_model','',TRUE);
        $this->load->model('dtltrkrwy_model','',TRUE);
        $session_data = $this->session->userdata('logged_in');
		$this->npkLogin = $session_data['npk'];
	}
 
    public function index()
    {
    }
	public function KartuBayarDPA1($ID)
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Purchase Request"))
			{
				$this->npkLogin = $session_data['npk'];
				$HeaderPP = $this->ppb_model->getHeaderPP($ID);
				$NoPP = $HeaderPP->NoPP;
				$user = $this->user->dataUser($this->npkLogin);
				$this->NoPP = $NoPP;
				$this->_kartuBayarDPA1($NoPP, $user);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	public function _kartuBayarDPA1($NoPP, $user)
    {
		$crud = new grocery_crud();
		$crud->set_subject('Kartu Bayar DPA 1');
		$crud->set_table('dtlppbkartubayar');
		$NoPP = $this->NoPP;
		if($user[0]->departemen == "HRGA"){
		}else{
			$crud->unset_operations();
		}
		$crud->set_relation('NoPP','dtlpersetujuanpengeluaranbudget','DPA',array('Deleted'=>0, 'DPA' => 1));
		//$crud->set_relation('NPKYangDilimpahkan','mstruser','Nama',array('Deleted' => 0,'TanggalBerhenti' => '0000-00-00'));
		$crud->where('dtlppbkartubayar.deleted','0');
		$crud->where('dtlppbkartubayar.NoPP',$NoPP);
		$crud->where("dtlppbkartubayar.CreatedBy LIKE '%DPA1%'");

		$crud->columns('Keterangan', 'Vendor', 'Nominal', 'TanggalPermintaanKas', 'NoPermintaanKas', 'isSubmitted');
		$crud->fields('Keterangan', 'Vendor', 'Nominal', 'TanggalPermintaanKas', 'NoPermintaanKas');
		$crud->display_as('Keterangan','Pembayaran');
		$crud->display_as('isSubmitted','Status Submit');
		$crud->display_as('TanggalPermintaanKas','Tanggal Permintaan Kas');
		$crud->display_as('NoPermintaanKas','Nomor Permintaan Kas');
		$crud->display_as('Nominal','Jumlah Dibayar');
		$crud->callback_column('Nominal',array($this,'_column_callback_Nominal'));
		$crud->callback_column('isSubmitted',array($this,'_column_callback_IsSubmitted'));

		//$crud->callback_column('NPKYangDilimpahkan',array($this,'_column_callback_NPK'));

		//	$crud->callback_field('NPKYangDilimpahkan',array($this,'add_field_callback_NPK'));
		//$crud->callback_field('Keterangan',array($this,'add_field_callback_Keterangan'));			
		$crud->field_type('NoPP','invisible');
		$crud->field_type('CreatedOn','invisible');
		$crud->field_type('CreatedBy','invisible');
		$crud->field_type('UpdatedOn','invisible');
		$crud->field_type('UpdatedBy','invisible');
		$crud->required_fields('Keterangan','Nominal');
		$crud->callback_insert(array($this,'_insert'));		
		$crud->callback_update(array($this,'_update'));
		$crud->callback_delete(array($this,'_delete'));
        $output = $crud->render();
        $this-> _outputview($output);
    }
    function _outputview($output = null,$page='')
    {
		$session_data = $this->session->userdata('logged_in');
		$data = array(
			'title' => 'Kartu Bayar',
			'body' => $output
		);
		$this->load->helper(array('form','url'));
		if($page == ''){
			$this->load->view('PersetujuanPengeluaranBudget/KartuBayar_view',$data);
		}else if($page == 'list'){
			$this->template->load('default','PersetujuanPengeluaranBudget/KartuBayar_view',$data);
		}
    }
	function _insert($post_array)
	{
		$Nominal = str_replace(',','',$post_array['Nominal']);
		$dataTransaksi = array(
			"NoPP" => $this->NoPP,
			"Deleted" => 0,
			"CreatedBy" => $this->npkLogin.'DPA1',
			"CreatedOn" => date('Y-m-d H:i:s'),
			"Keterangan" => $post_array['Keterangan'],
			"Vendor" => $post_array['Vendor'],
			"TanggalPermintaanKas" => $post_array['TanggalPermintaanKas'],
			"NoPermintaanKas" => $post_array['NoPermintaanKas'],
			"Nominal" => $Nominal
		);
		$this->db->insert('dtlppbkartubayar',$dataTransaksi);	
	}
	function _update($post_array, $primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin.'DPA1';
		$post_array['Nominal'] = str_replace(',','',$post_array['Nominal']);
		return $this->db->update('dtlppbkartubayar',$post_array,array('ID' => $primary_key));
	}
	function _delete($primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin.'DPA1';
		$post_array['Deleted'] = '1';
		return $this->db->update('dtlppbkartubayar',$post_array,array('ID' => $primary_key));
	}
	function _column_callback_Nominal($value = '', $primary_key = null)
	{
		return number_format($value);
	}
	function _column_callback_IsSubmitted($value = '', $primary_key = null)
	{
		if($value == '0'){
			return 'Pending';
		}else{
			return 'Submitted';
		}
	}
	public function KartuBayarDPA2($ID)
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("Purchase Request"))
			{
				$this->npkLogin = $session_data['npk'];
				$HeaderPP = $this->ppb_model->getHeaderPP($ID);
				$NoPP = $HeaderPP->NoPP;
				$user = $this->user->dataUser($this->npkLogin);
				$this->NoPP = $NoPP;
				$this->_kartuBayarDPA2($NoPP, $user);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
    }
	public function _kartuBayarDPA2($NoPP, $user)
    {
		$crud = new grocery_crud();
		$crud->set_subject('Kartu Bayar DPA 2');
		$crud->set_table('dtlppbkartubayar');
		$NoPP = $this->NoPP;
		$crud->set_relation('NoPP','dtlpersetujuanpengeluaranBudget','DPA',array('Deleted'=>0, 'DPA' => 2));
		//$crud->set_relation('NPKYangDilimpahkan','mstruser','Nama',array('Deleted' => 0,'TanggalBerhenti' => '0000-00-00'));
		$crud->where('dtlppbkartubayar.deleted','0');
		$crud->where('dtlppbkartubayar.NoPP',$NoPP);
		$crud->where("(dtlppbkartubayar.CreatedBy LIKE '%/%' OR dtlppbkartubayar.CreatedBy LIKE '%DPA2%')");

		$crud->columns('Keterangan', 'Vendor', 'Nominal', 'TanggalPermintaanKas', 'NoPermintaanKas', 'isSubmitted');
		$crud->fields('Keterangan', 'Vendor', 'Nominal', 'TanggalPermintaanKas', 'NoPermintaanKas');
		$crud->display_as('Keterangan','Pembayaran');
		$crud->display_as('isSubmitted','Status Submit');
		$crud->display_as('TanggalPermintaanKas','Tanggal Permintaan Kas');
		$crud->display_as('NoPermintaanKas','Nomor Permintaan Kas');
		$crud->display_as('Nominal','Jumlah Dibayar');
		$crud->callback_column('Nominal',array($this,'_column_callback_Nominal'));
		$crud->callback_column('isSubmitted',array($this,'_column_callback_IsSubmitted'));

		//$crud->callback_column('NPKYangDilimpahkan',array($this,'_column_callback_NPK'));

		//	$crud->callback_field('NPKYangDilimpahkan',array($this,'add_field_callback_NPK'));
		//$crud->callback_field('Keterangan',array($this,'add_field_callback_Keterangan'));			
		$crud->field_type('NoPP','invisible');
		$crud->field_type('CreatedOn','invisible');
		$crud->field_type('CreatedBy','invisible');
		$crud->field_type('UpdatedOn','invisible');
		$crud->field_type('UpdatedBy','invisible');
		$crud->required_fields('Keterangan','Nominal');
		if($user[0]->departemen == "HRGA"){
		}else{
			$crud->unset_operations();
		}
		$crud->callback_insert(array($this,'_insertDPA2'));		
		$crud->callback_update(array($this,'_updateDPA2'));
		$crud->callback_delete(array($this,'_deleteDPA2'));
        $output = $crud->render();
        $this-> _outputview($output);
	}
	function _insertDPA2($post_array)
	{
		$Nominal = str_replace(',','',$post_array['Nominal']);
		$dataTransaksi = array(
			"NoPP" => $this->NoPP,
			"Deleted" => 0,
			"CreatedBy" => $this->npkLogin.'DPA2',
			"CreatedOn" => date('Y-m-d H:i:s'),
			"Keterangan" => $post_array['Keterangan'],
			"Vendor" => $post_array['Vendor'],
			"IsSubmitted" => 1,
			"TanggalPermintaanKas" => $post_array['TanggalPermintaanKas'],
			"NoPermintaanKas" => $post_array['NoPermintaanKas'],
			"Nominal" => $Nominal
		);
		$this->db->insert('dtlppbkartubayar',$dataTransaksi);	
	}
	function _updateDPA2($post_array, $primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin.'DPA2';
		$post_array['Nominal'] = str_replace(',','',$post_array['Nominal']);
		return $this->db->update('dtlppbkartubayar',$post_array,array('ID' => $primary_key));
	}
	function _deleteDPA2($primary_key){
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin.'DPA2';
		$post_array['Deleted'] = '1';
		return $this->db->update('dtlppbkartubayar',$post_array,array('ID' => $primary_key));
	}
	public function ListAllKartuBayar()
    {
		$session_data = $this->session->userdata('logged_in');
		if($session_data){
			if(check_authorizedByName("List All Kartu Bayar"))
			{
				$this->npkLogin = $session_data['npk'];
				$user = $this->user->dataUser($this->npkLogin);
				$this->_ListAllKartuBayar($user);
			}
		}else{
			redirect('login?u='.substr($_SERVER["REQUEST_URI"],stripos($_SERVER["REQUEST_URI"],"index.php/")+10),'refresh');
		}
	}
	public function _ListAllKartuBayar($user)
    {
		$crud = new grocery_crud();
		$crud->set_subject('Kartu Bayar DPA 2');
		$crud->set_table('dtlppbkartubayar');
		$crud->where('dtlppbkartubayar.deleted','0');
		if($user[0]->departemen == "Accounting Tax & Control"){
			$crud->set_relation('NoPP','PersetujuanPengeluaranBudget','{NoPP} {Keterangan}',array('deleted' => '0'));
			$crud->where('dtlppbkartubayar.NoPP','');
			$crud->unset_delete();
		}
		else if($user[0]->departemen == "HRGA"){
			$crud->unset_edit();
			$crud->where('isSubmitted','1');
		}else{
			$crud->unset_operations();
		}
		$crud->where("dtlppbkartubayar.CreatedBy LIKE '%/%'");
		$crud->columns('Keterangan','NoPP', 'Vendor', 'Nominal', 'TanggalPermintaanKas', 'NoPermintaanKas', 'isSubmitted');
		$crud->fields('Keterangan','NoPP', 'Vendor', 'Nominal', 'TanggalPermintaanKas', 'NoPermintaanKas');
		$crud->display_as('Keterangan','Pembayaran');
		$crud->display_as('NoPP','Nomor Persetujuan Pengeluaran');
		$crud->display_as('isSubmitted','Status Submit');
		$crud->display_as('TanggalPermintaanKas','Tanggal Permintaan Kas');
		$crud->display_as('NoPermintaanKas','Nomor Permintaan Kas');
		$crud->display_as('Nominal','Jumlah Dibayar');
		$crud->callback_column('Nominal',array($this,'_column_callback_Nominal'));
		$crud->callback_column('isSubmitted',array($this,'_column_callback_IsSubmitted'));

		$crud->field_type('CreatedOn','invisible');
		$crud->field_type('CreatedBy','invisible');
		$crud->field_type('UpdatedOn','invisible');
		$crud->field_type('UpdatedBy','invisible');
		$crud->required_fields('NoPP','Keterangan','Nominal');
		$crud->unset_add();
		$crud->callback_update(array($this,'_updateKartuBayar'));
        $output = $crud->render();
        $this-> _outputview($output, 'list');
	}
	function _updateKartuBayar($post_array, $primary_key)
	{
		$HeaderPP = $this->ppb_model->getHeaderPP($post_array['NoPP']);
		$post_array['NoPP']=$HeaderPP->NoPP;
		$post_array['Nominal']= str_replace(',','',$post_array['Nominal']);;
		$post_array['UpdatedOn'] = date('Y-m-d H:i:s');
		$post_array['UpdatedBy'] = $this->npkLogin;
		return $this->db->update('dtlppbkartubayar',$post_array,array('ID' => $primary_key));
	}

}

/* End of file main.php */
/* Location: ./application/controllers/main.php */