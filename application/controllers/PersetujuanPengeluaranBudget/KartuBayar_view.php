<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <script src="<?php echo $this->config->base_url(); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?php echo $this->config->base_url(); ?>assets/js/autoNumeric.js" type="text/javascript"></script>
    
    <script>

    $(function($) {
        $('#field-Nominal').autoNumeric('init', { vMin: '-2142423123.00', lZero: 'deny', aSep: ',', mDec: 0 });
    });

    </script>
<?php 
foreach($body->css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<?php foreach($body->js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>



</head>
<body onload="parent.alertSizeDPA(document.body.clientHeight);" >
<!-- Beginning header -->

<!-- End of header-->
    <div style='height:20px;'></div>  
    <div>
        <?php echo $body->output; ?>
    </div>
<!-- Beginning footer -->
<div></div>
<!-- End of Footer -->
</body>
</html>