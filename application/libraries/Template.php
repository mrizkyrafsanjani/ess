<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
   class Template
	{
		var $ci;
		function __construct()
		{
			$this->ci =& get_instance();
			$this->ci->load->model('menu','',TRUE);
		}
   

		function load($tpl_view, $body_view = null, $data = null)
		{
		   if ( ! is_null( $body_view ) )
		   {
			  if ( file_exists( APPPATH.'views/'.$tpl_view.'/'.$body_view ) )
			  {
				 $body_view_path = $tpl_view.'/'.$body_view;
			  }
			  else if ( file_exists( APPPATH.'views/'.$tpl_view.'/'.$body_view.'.php' ) )
			  {
				 $body_view_path = $tpl_view.'/'.$body_view.'.php';
			  }
			  else if ( file_exists( APPPATH.'views/'.$body_view ) )
			  {
				 $body_view_path = $body_view;
			  }
			  else if ( file_exists( APPPATH.'views/'.$body_view.'.php' ) )
			  {
				 $body_view_path = $body_view.'.php';
			  }
			  else
			  {
				 show_error('Unable to load the requested file: ' . $tpl_name.'/'.$view_name.'.php');
			  }
			  $body = $this->ci->load->view($body_view_path, $data, TRUE);
			  if ( is_null($data) )
			  {
				 $data = array('body' => $body);
			  }
			  else if ( is_array($data) )
			  {
				 $data['body'] = $body;
			  }
			  else if ( is_object($data) )
			  {
				 $data->body = $body;
			  }
		   }
		   
		   $session_data = $this->ci->session->userdata('logged_in');
		   $menu = $this->ci->menu->getMenu($session_data['npk'],$session_data['koderole']);
			if($menu){
				$menu_array = array();
				foreach($menu as $row){
				   $menu_array[] = array(
					 'menuname' => $row->menuname,
					 'url' => $row->url,
					 'KodeMenu' => $row->KodeMenu,
					 'ParentMenu' => $row->ParentMenu,
					 'MenuOrder' => $row->MenuOrder,
					 'Icon' => $row->Icon
				   );					   
				}
			}else{
				echo 'gagal';
			}
		   $data['menu_array'] = $menu_array;
		   $data['npk'] = $session_data['npk'];
		   $data['nama'] = $session_data['nama'];
		   
		   $this->ci->load->view('templates/'.$tpl_view, $data);
		}  
	}
?>